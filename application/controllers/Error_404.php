<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Error_404 extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['kode'] = "404";
		$data['msg'] = "Page Not Found";
		$this->load->view("errors/error_handler", $data);
	}
}