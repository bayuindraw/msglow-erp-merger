<?php
function helper_log($param=[]){
    $CI =& get_instance();

    // get location users
    $ip = $_SERVER['REMOTE_ADDR']; // get client's IP
    $getloc = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));// Send to ipinfo
    $coordinates = explode(",", $getloc->loc); // -> '32,-72' becomes'32','-72'
    $latitude = $coordinates[0];
    $longitude = $coordinates[1];
    // $city = $getloc->city; // Gives you the city of the client.
    // $country = $getloc->country; // Gives you the country of the client.
 
    // parameter
    $param['user_id'] = $CI->session->userdata('user_id');
    $param['account_detail_real_log_ip'] = $_SERVER['REMOTE_ADDR'];
    $param['account_detail_real_longitude'] = $longitude;
    $param['account_detail_real_latitude'] = $latitude;
 
    //load model log
    $CI->load->model('m_log');
 
    //save to database
    $CI->m_log->save_log($param);
}
?>