<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relasi extends CI_Model {


	public function v_Kategori() {
		$Query = $this->db->query("SELECT * FROM  v_kategori ");
		return $Query->result_array();	
	}
	public function v_SubKategori() {
		$Query = $this->db->query("SELECT * FROM  v_subkategori ");
		return $Query->result_array();	
	}

	public function v_Barang($cKondisi="") {
		$Query = $this->db->query("SELECT * FROM  v_barang ".$cKondisi." ");
		return $Query->result_array();	
	}

	public function v_Blog($cKondisi="") {
		$Query = $this->db->query("SELECT * FROM  v_blog  ".$cKondisi." ");
		return $Query->result_array();	
	}

	public function v_Cart() {
		$Query = $this->db->query("SELECT * FROM  v_cart ORDER BY id_user DESC ");
		return $Query->result_array();	
	}

	public function v_Cart_Detail($cIdUser) {
		$Query = $this->db->query("SELECT * FROM v_cart_detail WHERE id_user = '$cIdUser'  ");
		return $Query->result_array();	
	}

	public function v_Cart_Detail_pesanan($cIdUser) {
		$Query = $this->db->query("SELECT * FROM v_cart_detail WHERE no_pesanan = '$cIdUser'  ");
		return $Query->result_array();	
	}

	public function v_Cart_Detail_status($cIdUser) {
		$Query = $this->db->query("SELECT * FROM  v_cart_detail WHERE no_pesanan = '$cIdUser'  ");
		return $Query;	
	}

	public function v_Chekout() {
		$Query = $this->db->query("SELECT * FROM  v_chekout ORDER BY id_user DESC ");
		return $Query->result_array();	
	}

	public function v_Chekout_Where($cIdUser) {
		$Query = $this->db->query("SELECT * FROM  v_chekout WHERE id_user = '".$cIdUser."' ");
		return $Query;	
	}

	public function v_Chekout_Where_2($cIdUser) {
		$Query = $this->db->query("SELECT * FROM  v_chekout WHERE id_user = '".$cIdUser."' ");
		return $Query->result_array();	
	}

	public function laporan_bulanan($Bulan="",$Tahun="") {
		$Query = $this->db->query("SELECT *  FROM v_cart_detail WHERE MONTH(tanggal) = '$Bulan' AND YEAR(tanggal) = '$Tahun' ");
		return $Query;	
	}

	public function v_edit_user($cIdUser) {
		$Query = $this->db->query("SELECT c.*,u.username,u.password ,u.nama 
									FROM checkout c , username u
									WHERE c.id_user = u.id_user AND u.id_user = '$cIdUser' ");
		return $Query->result_array();	
	}

	

}