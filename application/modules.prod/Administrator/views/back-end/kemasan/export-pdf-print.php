<!DOCTYPE html>
<?php
$bln = array(
    '01' => 'Januari',
    '02' => 'February',
    '03' => 'Maret',
    '04' => 'April',
    '05' => 'Mei',
    '06' => 'Juni',
    '07' => 'July',
    '08' => 'Agustus',
    '09' => 'September',
    '10' => 'Oktober',
    '11' => 'November',
    '12' => 'Desember'
);
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="https://msglow.app/upload/nitrogen.png" />
    <title>Print Purchase Order</title>

    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 5px;
        }

        table {
            width: 100%;
        }

        .right {
            text-align: right;
        }

        td {
            text-align: center;
        }
    </style>
</head>

<body id="ignorePDF">
    <div class="ttd2" style="float:left;margin-top: -10px;">
        <img src="<?php echo base_url('assets/pt.PNG'); ?>" width="200px" height="120px" style="margin: 10px;">
    </div>
    <div class="ttd2" style="float:right;margin-top: -10px;">
        <h2 style="margin: 10px 0px 0px 0px">FORM PERMINTAAN PO SUPPLIER</h2>
        <h3 style="margin: 0px; text-align: right">Tanggal: <?php $arrtgl = explode('-', $row1[0]['tanggal']);
                                                            echo (int)$arrtgl[2] . ' ' . $bln[$arrtgl[1]] . ' ' . $arrtgl[0];
                                                            ?></h3>
        <h3 style="margin: 0px; text-align: right">Nomor: <?php echo $row1[0]['kode_pb']; ?>
    </div>
    <table>
        <tr>
            <th>NO</th>
            <th>Nama Kemasan</th>
            <th>QTY(PCS)</th>
            <th>SUPPLIER</th>
        </tr>
        <tr>
            <?php
            $i = 1;
            foreach ($row1 as $key) {
            ?>
        <tr>
            <td width="10px"><?= $i ?></td>
            <td style="text-align: left;"><?= $key['nama_kemasan'] ?></td>
            <td><?= number_format($key['jumlah']) ?></td>
            <td><?= $key['nama_supplier'] ?></td>
        </tr>
    <?php
                $i++;
            } ?>
    </tr>
    </table>
    <div class="ttd2" style="float:left; margin-left: 20%;">
        <p><b>
                <center>Dibuat oleh,</center>
            </b></p>
        <br><br><br>
        <p style="font-weight: bold;">_________________</p>
        <p><b>
                <center>Rizhaldi Adhitya</center>
            </b></p>
        <p><i>
                <center>SPV Kemasan</center>
            </i></p>
    </div>
    <div class="ttd2" style="float:right; margin-right: 20%;">
        <p><b>
                <center>Disetujui oleh,</center>
            </b></p>
        <br><br><br>
        <p style="font-weight: bold;">_________________</p>
        <p><b>
                <center>Chandra Mahardhika</center>
            </b></p>
        <p><i>
                <center>Manager Produk</center>
            </i></p>
    </div>
</body>
<script>
    var doc = new jsPDF();
    var elementHandler = {
        '#ignorePDF': function(element, renderer) {
            return true;
        }
    };
    var source = window.document.getElementsByTagName("body")[0];
    doc.fromHTML(
        source,
        15,
        15, {
            'width': 180,
            'elementHandlers': elementHandler
        });

    doc.output("dataurlnewwindow");
</script>

<script>
    window.print();
</script>

</html>