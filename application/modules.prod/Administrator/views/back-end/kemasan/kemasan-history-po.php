							<div class="kt-portlet kt-portlet--mobile">
							  <div class="kt-portlet__head kt-portlet__head--lg">
							    <div class="kt-portlet__head-label">
							      <!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
							      <h3 class="kt-portlet__head-title">
							        History Pengajuan PO
							      </h3>
							    </div>
							  </div>
							  <div class="kt-portlet__body">

							    <!--begin: Datatable -->
							    <div dir id="dir" content="table">
							      <table id="multi-colum-dt" class="table table-striped table-bordered nowrap">
							        <thead>
							          <tr>
							            <th>No</th>
							            <th>Kode</th>
							            <th>Tanggal</th>
							            <th>Suplier</th>
							            <th>User</th>
							            <th>Action</th>
							          </tr>
							        </thead>
							        <tbody>
							          <?php
                        $no = 0;
                        foreach ($row as $key => $vaData) {
                        ?>
							            <tr>
							              <td><?= ++$no ?></td>
							              <td><?= $vaData['kode_po'] ?></td>
							              <td><?= $vaData['tanggal'] ?></td>
							              <td><?= $vaData['nama_supplier'] ?></td>
							              <td><?= $vaData['user'] ?></td>
							              <td>
							                <?php if ($_SESSION['role_id'] == "4") { ?>
							                  <?php if ($vaData['status_approve'] == "1") { ?>
							                    <button class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" 
                                   data-placement="top" onclick="showHistoryApprove('<?=$vaData['id_po_kemasan']?>')">
                                   <i class="icofont icofont-ui-edit flaticon2-edit"></i>
                                    </button>
							                  <?php } else { ?>
							                    <a href="<?= base_url() ?>Administrator/Stock/edit_po_kemasan/<?= $vaData['id_po_kemasan'] ?>" class="btn btn-primary waves-effect waves-light"><i class="icofont icofont-ui-edit flaticon2-edit"></i></a>
							                  <?php } ?>
							                <?php } ?>
							                <a href="<?= base_url() ?>Administrator/Stock/po_kemasan_history_detail/<?= $vaData['kode_po'] ?>" class="btn btn-danger waves-effect waves-light"><i class="icofont icofont-ui-edit flaticon2-list"></i></a>
							              </td>
							            </tr>
							          <?php } ?>
							        </tbody>
							      </table>
							    </div>

							    <!--end: Datatable -->
							  </div>
							</div>

							<div class="modal fade" id="modal-paket" tabindex="-1" role="dialog">
							  <div class="modal-dialog modal-lg" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							        <h4 class="modal-title"></h4>
							      </div>
							      <div class="modal-body">

							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

							      </div>
							    </div>
							  </div>
							</div>

							<script type="text/javascript">
							  function showHistoryApprove($id_po_kemasan) {

							    $("#modal-paket").modal('show');
							    $.ajax({
							      type: "POST",

							      url: "<?php echo base_url() ?>Administrator/Stock/tampil_history_approve/" + $id_po_kemasan,
							      cache: false,
							      success: function(msg) {
							        $(".modal-body").html(msg);

							      }
							    });
							  }
							</script>