							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											Draft Purchase Order
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<div dir  id="dir" content="table">
                     <table id="multi-colum-dt" class="table table-striped table-bordered nowrap">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Tanggal</th>
                            <th>Suplier</th>
                            <th>User</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['kode_po']?></td>
                            <td><?=$vaData['tanggal']?></td>
                            <td><?=$vaData['nama_supplier']?></td>
                            <td><?=$vaData['user']?></td>
                            <td>
                                <a href="<?=base_url()?>Administrator/Stock/list_po_draft/<?=$vaData['id_po_kemasan']?>" class="btn btn-primary waves-effect waves-light" ><i class="icofont icofont-ui-edit flaticon2-edit"></i></a>
                                <!--<a href="<?= base_url() ?>Administrator/Stock_Act/hapus_po_kemasan/<?= $vaData['id_po_kemasan'] ?><?= $vaData['kode_po'] ?>" type="button" class="btn btn-danger waves-effect waves-light"><i class="icofont icofont-ui-delete flaticon2-trash"></i></a>-->
                            </td>
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                   </div>

									<!--end: Datatable -->
								</div>
							</div> 
							
					