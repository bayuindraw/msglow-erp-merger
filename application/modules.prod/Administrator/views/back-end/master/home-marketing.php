    <div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4>ERP MSGLOW</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Dashboard</a>
                    </li>
                    
                </ol>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-xl-4 col-lg-6 grid-item">
                <div class="card">
                    <div class="row">
                        <div class="col-sm-12 d-flex">
                            <div class="col-sm-5 bg-primary">
                                <div class="p-20 text-center">
                                   <i class="icofont icofont-chart-arrows-axis f-64"></i>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="text-center">
                                    <h1 class="txt-warning">0</h1>
                                    <span>Penjualan</span>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xl-4 col-lg-6 grid-item">
                <div class="card">
                    <div class="row">
                        <div class="col-sm-12 d-flex">
                            <div class="col-sm-5 bg-danger">
                                <div class="p-20 text-center">
                                    <i class="icofont icofont-cart-alt f-64"></i>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="text-center">
                                    <h1 class="txt-warning">0</h1>
                                    <span>Belum Packing</span>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xl-4 col-lg-6 grid-item">
                <div class="card">
                    <div class="row">
                        <div class="col-sm-12 d-flex">
                            <div class="col-sm-5 bg-warning">
                                <div class="p-20 text-center">
                                    <i class="icofont icofont-id-card f-64"></i>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="text-center">
                                    <h1 class="txt-warning">0</h1>
                                    <span>Pending Pengiriman</span>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
          <div class="col-sm-4">
            <div class="card">
              <div class="card-header">
                <h5>STOCK BARANG JADI <?=date('d-m-Y')?></h5>
              </div>
              <div class="card-block">
                <table class="table" style="font-size: 13px">
                  <thead>
                    <th>Nama Produk</th>
                    <th>Jumlah Baik</th>
                    <th>Jumlah Rusak</th>
                  </thead>
                  <tbody>
                    <?php 
                      foreach ($kemasan as $key => $vaData) {
                    ?>
                    <tr>
                      <td><?=$vaData['nama_produk']?></td>
                      <td><?=number_format($vaData['jumlah'])?></td>
                      <td><?=number_format($vaData['rusak'])?></td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
              <div class="card-header">
                <h5>DATA PENJUALAN HARI INI</h5>
              </div>
              <div class="card-block">
                <table class="table" style="font-size: 13px">
                  <thead>
                    <th>Tgl Penjualan</th>
                    <th>Pembeli</th>
                    <th>Total Pembelian</th>
                  </thead>
                  <tbody>
                    <?php 
                      foreach ($penjualan as $key => $vaData) {
                    ?>
                    <tr>
                      <td><?=$vaData['tgl_jual']?></td>
                      <td><?=$vaData['kode_seller']?></td>
                      <td><?=number_format($vaData['total_bayar'])?></td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
              <div class="card-header">
                <h5>DATA PENJUALAN BELUM TERKIRIM</h5>
              </div>
              <div class="card-block">
                <table class="table" style="font-size: 13px">
                  <thead>
                    <th>Tgl Penjualan</th>
                    <th>Pembeli</th>
                    <th>Total Pembelian</th>
                  </thead>
                  <tbody>
                    <?php 
                      foreach ($penjualan as $key => $vaData) {
                    ?>
                    <tr>
                      <td><?=$vaData['tgl_jual']?></td>
                      <td><?=$vaData['kode_seller']?></td>
                      <td><?=number_format($vaData['total_bayar'])?></td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
      </div>
