<div class="row">
  <div class="col-xl-12 p-0">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h4>ERP MSGLOW</h4>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-header">
          <h5>STOCK KEMASAN <?= date('d-F-Y') ?></h5>
        </div>
        <div class="card-block">
          <table class="table" style="font-size: 13px">
            <thead>
              <th>Nama Kemasan</th>
              <th>Kategori</th>
              <th>Jumlah Baik</th>
              <th>Jumlah Rusak</th>
            </thead>
            <tbody>
              <?php
              foreach ($kemasan as $key => $vaData) {
              ?>
                <tr>
                  <td><?= $vaData['nama_kemasan'] ?> <label style="font-weight: 800;color: red;">(<?= $vaData['kode_sup'] ?>)</label></td>
                  <td><?= $vaData['kategori'] ?></td>
                  <td><?= number_format($vaData['jumlah']) ?></td>
                  <td><?= number_format($vaData['rusak']) ?></td>
                </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="card">
        <div class="card-header">
          <h5>KEMASAN KELUAR <?= date('d-F-Y') ?></h5>
        </div>
        <div class="card-block">
          <table class="table" style="font-size: 13px">
            <thead>
              <th>Tanggal</th>
              <th>Nama Kemasan</th>
              <th>Qty</th>
              <th>Factory</th>
            </thead>
            <tbody>
              <?php
              foreach ($produk as $key => $vaData) {
              ?>
                <tr>
                  <td><?= $vaData['tanggal'] ?></td>
                  <td><?= $vaData['nama_kemasan'] ?></td>
                  <td><?= $vaData['jumlah'] ?></td>
                  <td><?= $vaData['nama_factory'] ?></td>

                </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>