    <div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4>ERP MSGLOW</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Dashboard</a>
                    </li>
                    
                </ol>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xl-3 col-lg-6 grid-item">
                <div class="card">
                    <div class="row">
                        <div class="col-sm-12 d-flex">
                            <div class="col-sm-5 bg-primary">
                                <div class="p-20 text-center">
                                   <i class="icofont icofont-chart-arrows-axis f-64"></i>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="text-center">
                                    <h1 class="txt-warning">0</h1>
                                    <span>Penjualan</span>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xl-3 col-lg-6 grid-item">
                <div class="card">
                    <div class="row">
                        <div class="col-sm-12 d-flex">
                            <div class="col-sm-5 bg-success">
                                <div class="p-20 text-center">
                                    <i class="icofont icofont-chart-line-alt f-64"></i>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="text-center">
                                    <h1 class="txt-warning">0</h1>
                                    <span>Approve Payment</span>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xl-3 col-lg-6 grid-item">
                <div class="card">
                    <div class="row">
                        <div class="col-sm-12 d-flex">
                            <div class="col-sm-5 bg-danger">
                                <div class="p-20 text-center">
                                    <i class="icofont icofont-cart-alt f-64"></i>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="text-center">
                                    <h1 class="txt-warning">0</h1>
                                    <span>Approve Packing</span>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xl-3 col-lg-6 grid-item">
                <div class="card">
                    <div class="row">
                        <div class="col-sm-12 d-flex">
                            <div class="col-sm-5 bg-warning">
                                <div class="p-20 text-center">
                                    <i class="icofont icofont-id-card f-64"></i>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="text-center">
                                    <h1 class="txt-warning">0</h1>
                                    <span>Pending Pengiriman</span>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="card">
              <div class="card-header">
                <h5>STOCK KEMASAN <?=date('d-m-Y')?></h5>
              </div>
              <div class="card-block">
                <table class="table" style="font-size: 13px">
                  <thead>
                    <th>Nama Kemasan</th>
                    <th>Qty</th>
                   
                  </thead>
                  <tbody>
                    <?php 
                      foreach ($kemasan as $key => $vaData) {
                    ?>
                    <tr>
                      <td><?=$vaData['nama_kemasan']?></td>
                      <td><?=$vaData['jumlah']?></td>
                      
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
              <div class="card-header">
                <h5>STOCK BARANG JADI <?=date('d-m-Y')?></h5>
              </div>
              <div class="card-block">
                <table class="table" style="font-size: 13px">
                  <thead>
                    <th>Nama Produk</th>
                    <th>Qty</th>
                   
                  </thead>
                  <tbody>
                    <?php 
                      foreach ($produk as $key => $vaData) {
                    ?>
                    <tr>
                      <td><?=$vaData['nama_produk']?></td>
                      <td><?=$vaData['jumlah']?></td>
                      
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
              <div class="card-header">
                <h5>BARANG KLUAR <?=date('d-m-Y')?></h5>
              </div>
              <div class="card-block">
                <table class="table" style="font-size: 13px">
                  <thead>
                    <th>Nama Produk</th>
                    <th>Qty</th>
                   
                  </thead>
                  <tbody>
                    <?php 
                      foreach ($produk as $key => $vaData) {
                    ?>
                    <tr>
                      <td><?=$vaData['nama_produk']?></td>
                      <td><?=$vaData['jumlah']?></td>
                      
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
      </div>
