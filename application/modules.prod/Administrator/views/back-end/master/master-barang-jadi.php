<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$file?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Master</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Barang Jadi</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">

          <!-- Row start -->
          <div class="row">

            <div class="col-lg-12">

              <div class="card">

                <!-- Radio-Button start -->
                <div class="card-header"><h5 class="card-header-text">Form Input & Data Produk</h5></div>
                <div class="card-block ">
                  <!-- Row start -->
                  <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                      <!-- <h6 class="sub-title">Tab With Icon</h6> -->

                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs md-tabs " role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" data-toggle="tab" href="#data" role="tab">
                            <i class="icon-grid"></i> &nbsp;&nbsp; Data Produk</a>
                            <div class="slide">
                              
                            </div>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#input" role="tab">
                            <i class="icofont icofont-pencil-alt-5 "></i> &nbsp;&nbsp; Form Input Data Produk 
                          </a>
                          <div class="slide"></div>
                        </li>
                      </ul>
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <br/> <br/>
                        <div class="tab-pane active" id="data" role="tabpanel">
                        
                         <div dir  id="dir" content="table">
                           <table id="multi-colum-dt" class="table table-striped table-bordered nowrap">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Nama Produk</th>
                                  <th>Harga</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 
                                  $no=0;
                                  foreach ($row as $key => $vaData) {
                                ?>
                                <tr>
                                  <td><?=++$no?></td>
                                  <td><?=$vaData['nama_produk']?></td>
                                  <td>Rp.<?=number_format($vaData['harga'])?></td>
                                  <td>
                                      <button type="button" class="btn btn-primary waves-effect waves-light"><i class="icofont icofont-ui-edit"></i></button>
                                      <button type="button" class="btn btn-danger waves-effect waves-light"><i class="icofont icofont-ui-delete"></i></button>
                                  </td>
                                </tr>
                               <?php } ?>
                              </tbody>
                           </table>
                         </div> 
                         <span ng-bind="msg"></span>
                        </div>
                        <div class="tab-pane" id="input" role="tabpanel">
                         <form id="main" class="form-horizontal" action="/" method="post" novalidate>
                          <div class="md-group-add-on p-relative">
                           <span class="md-add-on">
                            <i class="icofont icofont-bar-code"></i>
                          </span>
                          <div class="md-input-wrapper">
                            <input type="text" class="md-form-control md-static"  name="KodeBarang" id="cKodeBarang">
                            <input type="hidden" class="md-form-control"  name="cIdBarang" id="cIdBarang" >
                            <label for="KodeBarang">Kode Produk</label>
                            <span class="messages"></span>
                          </div>
                        </div>
                        <div class="md-group-add-on p-relative">
                             <span class="md-add-on">
                              <i class="icofont icofont-ui-tag"></i>
                            </span>
                           <div class="md-input-wrapper">
                              <input type="text" class="md-form-control md-static"  name="NamaBarang" id="cNamaBarang">
                              <label for="NamaBarang">Nama Produk</label>
                              <span class="messages"></span>
                            </div>
                          </div>
                          <div class="md-group-add-on p-relative">
                             <span class="md-add-on">
                              <i class="icofont icofont-cur-dollar"></i>
                            </span>
                           <div class="md-input-wrapper">
                              <input type="text"   class="md-form-control md-static"  id="nHargaBeli" name="HargaBeli">
                              <label for="HargaBeli">Harga</label>
                              <span class="messages"></span>
                            </div>
                          </div>
                         

                        <div class="md-input-wrapper">     
                         <button type="submit" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" 
                         data-placement="top" title="{{cValueButton}}">
                         <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
                       </button>
                     </div>
                      </form>
                      
                      </div>
                    </div>

                  </div>
                  <!-- Row end -->
                </div>
              </div>
            </div>
          </div>
          <!-- Row end -->
        </div>
      </div>
      <!-- loader ends -->
      </div>
    </div>
</div>