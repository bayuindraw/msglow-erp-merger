<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$file?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">UI Elements</a>
                    </li>
                    <li class="breadcrumb-item"><a href="panels-wells.html">Panels Wells</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-lg-12">

    <!-- Row start -->
    <div class="row">

      <div class="col-lg-12">

        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">Form Input & Data Pabrik</h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->

                <!-- Nav tabs -->
                <ul class="nav nav-tabs md-tabs " role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#data" role="tab">
                      <i class="icon-grid"></i> &nbsp;&nbsp; Data Seller Msglow</a>
                      <div class="slide">
                        
                      </div>
                  </li>
                  
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                  <br/> <br/>
                  <div class="tab-pane active" id="data" role="tabpanel">
                  
                   <div dir  id="dir" content="table">
                     <table id="multi-colum-dt" class="table table-striped table-bordered nowrap">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Kota</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['kode']?></td>
                            <td><?=$vaData['nama']?></td>
                            <td><?=$vaData['kota']?></td>
                            <td><?=$vaData['alamat']?></td>
                            <td><?=$vaData['telepon']?></td>
                            <td>
                                <button type="button" class="btn btn-primary waves-effect waves-light" ><i class="icofont icofont-ui-edit"></i></button>
                                <button type="button" class="btn btn-danger waves-effect waves-light" ><i class="icofont icofont-ui-delete"></i></button>
                            </td>
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                   </div> 
                   <span ng-bind="msg"></span>
                  </div>

              </div>

            </div>
            <!-- Row end -->
          </div>
        </div>
      </div>
    </div>
    <!-- Row end -->
  </div>
</div>
<!-- loader ends -->
</div>
