<div class="kt-portlet kt-portlet--mobile">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Data Penjualan
      </h3>
    </div>
  </div>
  <div class="kt-portlet__body">

    <!--begin: Datatable -->
    <div dir id="dir" content="table">
      <table class="table table-striped table-bordered nowrap">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Penjualan</th>
            <th>Kode Seller</th>
            <th>Nama Seller</th>
            <th>Tanggal</th>
            <th>Total Penjualan</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 0;
          foreach ($row as $key => $vaData) {
          ?>
            <tr>
              <td><?= ++$no ?></td>
              <td><?= $vaData['kode_penjualan'] ?></td>
              <td><?= $vaData['kode'] ?></td>
              <td><?= $vaData['nama'] ?></td>
              <td><?= $vaData['tgl_jual'] ?></td>
              <td>Rp. <?= number_format($vaData['total_bayar']) ?></td>
              <td>
                <?php

                if ($vaData['approved_by']  == '') {
                  $kemasan  = 'PENDING';
                  $label    = 'btn btn-danger';
                } else {
                  $kemasan  = 'DITERIMA';
                  $label    = 'btn btn-success';
                }

                ?>
                <strong class="label <?= $label ?>"><?= $kemasan ?></strong>
              </td>

              <td>
                <a href="<?= base_url() ?>Administrator/Penjualan/detail_penjualan/<?= $vaData['kode_penjualan'] ?>" class="btn btn-primary waves-effect waves-light"><i class="fas fa-info-circle"></i> Detail</a>
              </td>

            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <!--end: Datatable -->
  </div>
</div>

<script type="text/javascript">
  function hapus(id) {
    swal({
        title: "Apakah Yakin Konfirmasi Penjualan Ini  ?",
        text: "Pastikan data yang di Konfirmasi benar",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya , Konfirmasi Penjualan",
        closeOnConfirm: false
      }, function() {
        $.ajax({
          type: "GET",
          url: "<?= base_url() ?>Administrator/Penjualan_Act/penjualan_konfirmasi/" + id,
          cache: false,
          success: function(data) {
            swal.close();
            $('#hapus').val('true');
            toastr.success('Sukses!', 'Berhasil Konfirmasi Penjualan');
            setTimeout(function() { // wait for 5 secs(2)
              location.reload(); // then reload the page.(3)
            }, 1000);
          },
          complete: function() {
            $('#aksi').val('simpan');
            //window.alert('sukses');
          }
        });
      },
      function() {});

  }
</script>