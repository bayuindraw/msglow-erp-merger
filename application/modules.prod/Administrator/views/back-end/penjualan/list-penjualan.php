<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$file?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">UI Elements</a>
                    </li>
                    <li class="breadcrumb-item"><a href="panels-wells.html">Panels Wells</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-lg-12">

    <!-- Row start -->
    <div class="row">

      <div class="col-lg-12">

        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">DATA PENJUALAN</h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->
                <div class="tab-content">
                  
                  <div class="tab-pane active" id="data" role="tabpanel">
                  
                   <div dir  id="dir" content="table">
                     <table class="table table-striped table-bordered nowrap" style="font-size: 14px">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Kode Penjualan</th>
                            <th>Kode Seller</th>
                            <th>Nama Seller</th>
                            <th>Tanggal</th>
                            <th>Total Penjualan</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['kode_penjualan']?></td> 
                            <td><?=$vaData['kode']?></td> 
                            <td><?=$vaData['nama']?></td>
                            <td><?=String2Date($vaData['tgl_jual'])?></td>
                            <td>Rp. <?=number_format($vaData['total_bayar'])?></td>
                            <td>
                              <?php 

                                  if($vaData['approved_by']  == ''){
                                    $kemasan  = 'PENDING';
                                    $label    = 'label-danger';
                                  }else{
                                    $kemasan  = 'DITERIMA';
                                    $label    = 'label-success';
                                  }

                              ?>
                              <strong class="label <?=$label?>"><?=$kemasan?></strong>
                            </td>
                            <td><button type="button" class="btn btn-primary waves-effect waves-light" title="Terima Order" onclick="hapus(<?=$vaData['id_penjualan']?>)"><i class="icofont icofont-ui-check"></i></button></td>
                            
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                   </div> 
                   <span ng-bind="msg"></span>
                  </div>

              </div>

            </div>
            <!-- Row end -->
          </div>
        </div>
      </div>
    </div>
    <!-- Row end -->
  </div>
</div>
<!-- loader ends -->
</div>
<script type="text/javascript">
  function hapus(id){
  swal({
      title: "Apakah Yakin Konfirmasi Penjualan Ini  ?",
      text: "Pastikan data yang di Konfirmasi benar",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Ya , Konfirmasi Penjualan",
      closeOnConfirm: false
    },function () {
      $.ajax({
        type: "GET",  
        url: "<?=base_url()?>Administrator/Penjualan_Act/penjualan_konfirmasi/"+id,
        cache: false,
        success:function(data){
          swal.close();
          $('#hapus').val('true');
          toastr.success('Sukses!','Berhasil Konfirmasi Penjualan');
          setTimeout(function(){// wait for 5 secs(2)
           location.reload(); // then reload the page.(3)
            }, 1000); 
        },
        complete: function() {
          $('#aksi').val('simpan');
          //window.alert('sukses');
        }
      });
   },
   function(){});
  
}
</script>