<!--<div class="main-header">
              <h4><?= $menu ?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Stock</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Stock Opname Kemasan</a>
                    </li>
                </ol>
            </div>-->
<div class="kt-portlet kt-portlet--mobile">


  <!--<div class="card-header"><h5 class="card-header-text">STOCK REAL KEMASAN</h5></div>-->
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <!--<span class="kt-portlet__head-icon">
                      <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>-->
      <h3 class="kt-portlet__head-title">

       TOTAL DELIVERY PRODUCT BULAN <?=strtoupper(date('M'))?> UNTUK PRODUCT : <?=$action?>
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
          
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">

    <!--begin: Datatable -->
    <div dir id="dir" content="table">
      
      <table class="table table-striped table-bordered nowrap" id="mytable_barang" style="font-size: 12px">
        <thead>
          <tr>
            <th>No</th>
            <th>Sales Code</th>
            <th>DO Code</th>
            <<!-- th>Kode Seller</th> -->
            <th>Nama Seller</th>
            <th>Kode Produk</th>
            <th>Nama Produk</th>
            <th>Total Pengiriman</th>

          </tr>
        </thead>
        <tbody>
          <?php
          $no = 0;
          
          foreach ($row as $key => $vaData) {
           
          ?>
            <tr>
              <td><?= ++$no ?></td>
              <td><?= $vaData['sales_code'] ?></td>
              <td><?= $vaData['package_code'] ?></td>
              <!-- <td><?= $vaData['seller_code'] ?></td> -->
              <td><?= $vaData['seller_name'] ?></td>
              <td><?= $vaData['product_code'] ?></td>
              <td><?= $vaData['sku_name'] ?></td>
              <td><?= number_format($vaData['send_quantity']) ?></td>

            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <!--end: Datatable -->
  </div>
</div>