<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            DAFTAR LIST PO PRODUK ACTIVE
          </h3>
        </div>
      </div>
    </div>
  </div>
  <?php
  foreach ($row as $key => $vaData) {

    $query = $this->model->code("SELECT count(id_pokemasan) as totalproduk, SUM(jumlah) as totaljumlah FROM  v_detail_po_produk WHERE kode_pb = '" . $vaData['kode_po'] . "'");
    foreach ($query as $key => $vaTotal) {
      $totalp =  $vaTotal['totalproduk'];
    }
  ?>
    <div class="col-lg-4 col-xl-4">
      <!--begin::Portlet-->
      <div class="kt-portlet">


        <div class="kt-portlet__head">
          <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
              <?= $vaData['kode_po'] ?>
            </h3>
          </div>
        </div>
        <div class="kt-portlet__body">

          <table class="table" width="100%">
            <tr>
              <td>Supplier</td>
              <td>:</td>
              <td><?= substr($vaData['nama_factory'], 0, 15) ?></td>
            </tr>
            <tr>
              <td>Total Produk</td>
              <td>:</td>
              <td><?= $vaTotal['totaljumlah'] ?> Pcs / <?= $totalp ?> Item</td>
            </tr>
            <tr>
              <td>Total Harga</td>
              <td>:</td>
              <td>RP <?= $vaData['total_biaya'] ?></td>
            </tr>
          </table>
          <div class="kt-form__actions">
            <button class="btn btn-primary waves-effect waves-light"><?= $vaData['tanggal'] ?></button>
            <button class="btn btn-warning"><?='PO BERJALAN'?></button><br>
			<br>
			<a href="<?= base_url() ?>Administrator/Stock_Produk/terima_produk/<?= $vaData['kode_po'] ?>"><button class="btn btn-success">Penerimaan PO</button></a>
            
          </div>
		  <div class="side-box bg-<?=$warna?>">
                                  <i class="icon-wallet"></i>
                                  </div>
        </div>
      </div>
    </div>

<?php } ?>


</div>