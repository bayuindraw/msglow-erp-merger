<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Aset_lainnyaModel extends CI_Model
{

	var $table_ = "t_asset";
	var $id_ = "asset_id";
	var $eng_ = "asset";
	var $url_ = "Aset_lainnya";


	public function insert_coa_beban($id3_beban, $asset_name, $kode)
	{
		$coaBiaya = $this->db->get_where('coa_3', ['id' => $id3_beban])->row_array();
		$coaBeban = ([
			'nama' => str_replace("Biaya", "Beban", $coaBiaya['nama']) . " " . $asset_name,
			'kode' => $coaBiaya['kode'] . "." . $kode,
			'coa3_id' => $id3_beban
		]);
		$this->db->insert('coa_4', $coaBeban);
		$coaBeban['id'] = $this->db->insert_id();

		$total = ([
			'coa_id' => $coaBeban['id'],
			'coa_code' => $coaBeban['kode'],
			'coa_level' => 4,
			'date_create' => date('Y-m-d H:i:s'),
			'coa_name' => $coaBeban['nama'],
		]);
		$this->db->insert('t_coa_total', $total);
		$total['coa_total_date'] = date('Y-m') . '-01';
		$this->db->insert('t_coa_total_history', $total);
		return $coaBeban;
	}
	public function insert_coa_aset($coa3_id, $asset_name)
	{
		$coaAset = $this->db->get_where('coa_4', ['nama' => "Aset Lainnya " . $asset_name, 'coa3_id' => $coa3_id])->row_array();
		if ($coaAset == null) {
			$coas = $this->db->get_where('coa_4', ['coa3_id' => $coa3_id])->result_array();
			$coaAset = ([
				'nama' => "Aset Lainnya " . $asset_name,
				'kode' => "100.3.1." . count($coas),
				'coa3_id' => $coa3_id
			]);
			$this->db->insert('coa_4', $coaAset);
			$coaAset['id'] = $this->db->insert_id();
			$total = ([
				'coa_id' => $coaAset['id'],
				'coa_code' => $coaAset['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $coaAset['nama'],
			]);
			$this->db->insert('t_coa_total', $total);
			$total['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $total);
		}
		return $coaAset;
	}
	public function insert_coa_akum($asset_name, $kode)
	{
		$coaAkum = ([
			'nama' => "Akumulasi Penyusutan " . $asset_name,
			'kode' => "100.2.2." . $kode,
			'coa3_id' => 22
		]);
		$this->db->insert('coa_4', $coaAkum);
		$coaAkum['id'] = $this->db->insert_id();

		$total = ([
			'coa_id' => $coaAkum['id'],
			'coa_code' => $coaAkum['kode'],
			'coa_level' => 4,
			'date_create' => date('Y-m-d H:i:s'),
			'coa_name' => $coaAkum['nama'],
		]);
		$this->db->insert('t_coa_total', $total);
		$total['coa_total_date'] = date('Y-m') . '-01';
		$this->db->insert('t_coa_total_history', $total);
		return $coaAkum;
	}
	public function insert_coa_utang($name)
	{
		$coa3 = $this->db->get_where('coa_3', ['id' => 27])->row_array();
		$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 27])->result_array();
		$coaUtang = ([
			'nama' => "Utang " . $name,
			'kode' => $coa3['kode'] . "." . (count($coa4) + 1),
			'coa3_id' => 27
		]);
		$this->db->insert('coa_4', $coaUtang);
		$coaUtang['id'] = $this->db->insert_id();

		$total = ([
			'coa_id' => $coaUtang['id'],
			'coa_code' => $coaUtang['kode'],
			'coa_level' => 4,
			'date_create' => date('Y-m-d H:i:s'),
			'coa_name' => $coaUtang['nama'],
		]);
		$this->db->insert('t_coa_total', $total);
		$total['coa_total_date'] = date('Y-m') . '-01';
		$this->db->insert('t_coa_total_history', $total);
		return $coaUtang;
	}

	public function input_old_coa($date, $t_asset, $coaAkum, $coaBeban, $idHeader)
	{
		$akunAkum = ([
			'coa_name' => $coaAkum['nama'],
			'coa_code' => $coaAkum['kode'],
			'coa_date' => $date,
			'coa_level' => 4,
			'coa_debit' => ($t_asset['asset_value'] / $t_asset['asset_span']),
			'coa_transaction_note' => 'Pembelian Aset By ' . $_SESSION['user_fullname'],
			'date_create' => date('Y-m-d'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_source' => 13,
			'coa_transaction_source_id' => $t_asset['asset_id'],
			'coa_group_id' => $idHeader,
			'coa_id' => $coaAkum['id'],
			'coa_transaction_realization' => 1,
		]);
		$this->db->insert('t_coa_transaction', $akunAkum);
		$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $akunAkum[coa_debit] WHERE coa_id = '$akunAkum[coa_id]' AND coa_level = '$akunAkum[coa_level]'");
		$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $akunAkum[coa_debit] WHERE coa_id = '$akunAkum[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunAkum['coa_date'] . "', '-', '') AND coa_level = '$akunAkum[coa_level]'");

		$akunBeban = ([
			'coa_name' => $coaBeban['nama'],
			'coa_code' => $coaBeban['kode'],
			'coa_date' => $date,
			'coa_level' => 4,
			'coa_credit' => ($t_asset['asset_value'] / $t_asset['asset_span']),
			'coa_transaction_note' => 'Pembelian Aset By ' . $_SESSION['user_fullname'],
			'date_create' => date('Y-m-d'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_source' => 13,
			'coa_transaction_source_id' => $t_asset['asset_id'],
			'coa_group_id' => $idHeader,
			'coa_id' => $coaBeban['id'],
			'coa_transaction_realization' => 1,
		]);
		$this->db->insert('t_coa_transaction', $akunBeban);
		$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $akunBeban[coa_credit] WHERE coa_id = '$akunBeban[coa_id]' AND coa_level = '$akunBeban[coa_level]'");
		$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $akunBeban[coa_credit] WHERE coa_id = '$akunBeban[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunBeban['coa_date'] . "', '-', '') AND coa_level = '$akunBeban[coa_level]'");
	}

	function get_data()
	{
		$table = $this->table_;
		$id = $this->id_;
		$field = array('asset_name', 'asset_date', 'FORMAT(asset_value, 0)');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/detail/xid") . '" class="btn btn-info"> <i class="fa fa-list"></i></a>';
		$jfield = join(', ', $field);
		$arrorder[] = "asset_id DESC";
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}
	function get_data_detail($kode)
	{
		$table = "t_asset A";
		$arrjoin[] = "JOIN t_coa_transaction B ON B.coa_id = A.coa_shrink_id";
		$id = "A.asset_id";
		$field = array('B.coa_date', 'FORMAT(B.coa_credit, 0)');
		$fields = array('coa_date', 'FORMAT(B.coa_credit, 0)');
		$url = $this->url_;
		$jfield = join(', ', $field);
		$arrorder[] = "B.coa_date DESC";
		$arrwhere[] = "A.asset_id = " . $kode . "";
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($fields as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}
}
