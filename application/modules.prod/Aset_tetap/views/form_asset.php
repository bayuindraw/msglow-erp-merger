<div class="row">
	<div class="col-lg-12 col-xl-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" onsubmit="return confirm('Are you sure?')" enctype="multipart/form-data">
				<div class="kt-portlet__body">
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Nama Aset</th>
								<th width="10%">Akun</th>
								<th>Tanggal Mulai Pemakaian</th>
								<th width="8%">Umur Ekonomis (Bulan)</th>
								<th width="10%">Harga</th>
								<th width="10%">Total Pembayaran</th>
								<th>Akun Pembayaran</th>
								<th>Pilihan Akun</th>
								<th>Nama Pemilik</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="x">
							<?php
							$totalx = 0;
							$i = 0;
							?>
							<tr id="tr0">
								<td><input type="text" class="form-control" placeholder="Nama Aset" name="input2[0][asset_name]" required></td>
								<td><select name="input2[0][coa3_id]" class="pilihAkun form-control md-static" required><?= $arrakun ?></select></td>
								<td><input type="text" class="form-control date_picker" placeholder="Tanggal" autocomplete="off" name="input2[0][asset_date]" required></td>
								<td><input type="number" class="form-control" placeholder="Umur Ekonomis" name="input2[0][asset_span]"></td>
								<td><input type="text" class="form-control numeric" id="harga0" placeholder="Harga" name="input2[0][asset_value]"></td>
								<td><input type="text" class="form-control numeric" id="totalbayar0" placeholder="Total Bayar" name="input2[0][asset_paid]"></td>
								<td>
									<select id="asset_type0" name="input2[0][asset_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(0);" required>
										<option value=""></option>
										<option value="1">KAS</option>
										<option value="2">BANK</option>
										<option value="3">Modal Disetor</option>
										<option value="4">Aset Lainnya</option>
									</select>
								</td>
								<td>
									<select onchange="chkcode(0);" id="arrakuncoa40" name="input2[0][coa4_pakai]" class="pilihAkun form-control md-static" required></select>
								</td>
								<td><input type="text" class="form-control" placeholder="Nama Pemilik" name="input2[0][asset_seller]"></td>
								<td><button onclick="myDeleteFunction(0);chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
							</tr>
							<?php $i++; ?>
						</tbody>
						<tbody>
							<tr>
								<td colspan="3">Total Nominal : <span id="total"><?= $totalx ?></span></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<button type="submit" name="simpan" value="simpan" id="btn_submit" class="btn btn-primary">Simpan</button>
							<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
						</div>
					</div>
			</form>
		</div>
	</div>
</div>

<script>
	var count = <?= $i; ?>;

	function myCreateFunction() {
		count = count + 1;
		var html = '<tr id="tr' + count + '"><td><input type="text" class="form-control" placeholder="Nama Aset" name="input2[' + count + '][asset_name]" required></td><td><select name="input2[' + count + '][coa3_id]" class="pilihAkun' + count + ' form-control md-static" required><?= $arrakun ?></select></td><td><input type="text" class="form-control date_picker" placeholder="Tanggal" name="input2[' + count + '][asset_date]" required></td><td><input type="number" class="form-control" placeholder="Umur Ekonomis" name="input2[' + count + '][asset_span]"></td><td><input type="text" class="form-control numeric" id="harga' + count + '" placeholder="Harga" name="input2[' + count + '][asset_value]"></td><td><input type="text" class="form-control numeric" id="totalbayar' + count + '" placeholder="Total Bayar" name="input2[' + count + '][asset_paid]"></td><td><select id="asset_type' + count + '" name="input2[' + count + '][asset_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(' + count + ');" required><option value=""></option><option value="1">KAS</option><option value="2">BANK</option><option value="3">Modal Disetor</option><option value="4">Aset Lainnya</option></select></td><td> <select onchange = "chkcode(' + count + ');"id = "arrakuncoa4' + count + '"name = "input2[' + count + '][coa4_pakai]"class = "pilihAkun form-control md-static"required > </select> </td > <td><input type = "text"class = "form-control"placeholder = "Nama Pemilik"name = "input2[' + count + '][asset_seller]"></td><td><button onclick = "myDeleteFunction(' + count + ');chk_total();" class= "btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$('.pilihAkun' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('#asset_type' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('#arrakuncoa4' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('.date_picker, #kt_datepicker_1_validate').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			format: "yyyy-mm-dd"
		});
		$(".numeric").mask("#,##0", {
			reverse: true
		});
	}

	function myDeleteFunction(id) {
		$('#tr' + id).remove();
	}

	function cekCoa4(id) {
		$.ajax({
			url: '<?= base_url() ?>Aset_tetap/get_coa4/' + $('#asset_type' + id + ' option:selected').val(),
			success: function(result) {
				$("#arrakuncoa4" + id + "").html(result);
			}
		});
	}

	function chkcode(code) {
		if ($("#asset_type" + code).find(":selected").val() == 4) {
			$('#harga' + code).val(commafy($('#arrakuncoa4' + code + ' option:selected').attr('total')));
			$('#totalbayar' + code).val(commafy($('#arrakuncoa4' + code + ' option:selected').attr('total')));
			$('#harga' + code).attr('readonly', 'true');
			$('#totalbayar' + code).attr('readonly', 'true');
		} else {
			$('#harga' + code).removeAttr('readonly');
			$('#totalbayar' + code).removeAttr('readonly');
		}
	}
</script>