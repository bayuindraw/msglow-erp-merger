<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


// Syarat  :  

// 1 . Select  = View 
// 2 . Insert  = Ins
// 3 . Update  = Updt
// 4 . Delete  = Del

class Coa_model extends CI_Model
{

    public function get_coa1()
    {
        return $this->db->get_where('coa_1', ['is_delete' => 0])->result_array();
    }
    public function get_coa($id)
    {
        // return $this->db->get_where('coa_' . $id . '', ['is_delete' => 0])->result_array();
        return $this->db->query("SELECT * FROM coa_" . $id . " A LEFT JOIN (SELECT coa_id, COUNT(coa_total_id) jum, coa_total_debit, coa_total_credit, coa_total_date FROM t_coa_total_history WHERE coa_level = $id GROUP BY coa_id) B ON B.coa_id = A.id AND B.jum = 1 WHERE is_delete = 0 LIMIT 800")->result_array();
    }
    public function get_coa1_where($data = "")
    {
        return $this->db->get_where('coa_1', ['is_delete' => 0, 'id' => $data])->result_array();
    }
    public function get_coa2()
    {
        return $this->db->get_where('coa_2', ['is_delete' => 0])->result_array();
    }
    public function get_coa2_where($data = "")
    {
        return $this->db->get_where('coa_2', ['is_delete' => 0, 'id' => $data])->result_array();
    }
    public function get_coa3()
    {
        return $this->db->get_where('coa_3', ['is_delete' => 0])->result_array();
    }
    public function get_coa3_where($data = "")
    {
        return $this->db->get_where('coa_3', ['is_delete' => 0, 'id' => $data])->result_array();
    }
    public function get_coa4()
    {
        return $this->db->get('coa_4')->result_array();
    }
    public function get_coa4_where($data = "")
    {
        return $this->db->get_where('coa_4', ['id' => $data])->result_array();
    }
    public function get_coa1_detail($id)
    {
        return $this->db->get_where('coa_1', ['id' => $id])->row_array();
    }
    public function get_coa2_detail($id)
    {
        return $this->db->get_where('coa_2', ['id' => $id])->row_array();
    }
    public function get_coa3_detail($id)
    {
        return $this->db->get_where('coa_3', ['id' => $id])->row_array();
    }
    public function get_coa1_count()
    {
        return $this->db->get_where('coa_1', ['is_delete' => 0])->result_array();
    }
    public function get_coa2_count($id)
    {
        return $this->db->get_where('coa_2', ['is_delete' => 0, 'coa1_id' => $id])->result_array();
    }
    public function get_coa3_count($id)
    {
        return $this->db->get_where('coa_3', ['is_delete' => 0, 'coa2_id' => $id])->result_array();
    }
    public function get_coa4_count($id)
    {
        return $this->db->get_where('coa_4', ['is_delete' => 0, 'coa3_id' => $id])->result_array();
    }

    public function get_data_coa4()
    {
        $table = "coa_4";
        $id = "id";
        $field = array('kode', 'nama');
        $url = 'COA';
        //$action = '';
        $jfield = join(', ', $field);
        $start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
        $length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
        $limit = "LIMIT $start, $length";
        $action = '<a href="' . base_url() . 'COA/coa4/edit/xid" class="btn btn-info btn-sm waves-effect waves-light del-product" type="submit"><span class="btn-label"><i class="fas fa-edit"></i></span></a>';
        $where = " WHERE is_delete = 0";
        $where2 = "is_delete = 0";
        $search = $_GET['search']['value'];
        $arrwhere2 = @$arrwhere;
        if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
        if (@$search != "") {
            foreach ($field as $key => $value) {
                $arrfield[] = "$value LIKE '%$search%'";
            }
            $arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
        }
        if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
        foreach (@$_GET['order'] as $key2 => $value2) {
            $arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
        }
        $order = 'ORDER BY ' . join(', ', $arrorder);
        $data = array();
        $jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table WHERE $where2")->row_array();
        $jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $where")->row_array();
        $result = $this->db->query("SELECT $id, $jfield FROM $table $where $order $limit")->result_array();
        $dataxy = array();
        foreach ($result as $keyr => $valuer) {
            $datax = array();
            foreach ($field as $keyfield) {
                if ($keyfield == "jum1" || $keyfield == "jum2") {
                    $datax[] = number_format($valuer[$keyfield]);
                } else {
                    $datax[] = $valuer[$keyfield];
                }
            }
            $datax[] = str_replace('xid', $valuer[$id], $action);
            $dataxy[] = $datax;
        }
        $data = array(
            'draw' => $_GET['draw'],
            'recordsTotal' => (int)$jum_all['jum'],
            'recordsFiltered' => (int)$jum_filter['jum'],
            'data' => @$dataxy
        );

        echo json_encode($data);
    }
}
