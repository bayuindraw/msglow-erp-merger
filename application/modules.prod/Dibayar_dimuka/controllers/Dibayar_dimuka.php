<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Dibayar_dimuka extends CI_Controller
{

	var $url_ = "Dibayar_dimuka";
	var $id_ = "activities_id";
	var $eng_ = "Dibayar Dimuka";
	var $ind_ = "Dibayar Dimuka";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function form()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['arrakun'] = "<option></option>";
		$datacontent['arrakuncoa4'] = "";
		$arrakuns = $this->db->query("SELECT * FROM coa_3 WHERE nama LIKE '%Biaya%' AND id != 19")->result_array();
		foreach ($arrakuns as $arrakun) {
			$datacontent['arrakun'] .= "<option value=" . $arrakun['id'] . ">" . $arrakun['nama'] . "</option>";
		}
		$arrbrands = $this->db->get('m_brand')->result_array();
		$datacontent['arrbrand'] = "<option></option>";
		foreach ($arrbrands as $arrbrand) {
			$datacontent['arrbrand'] .= "<option coa_id=" . $arrbrand['coa_id'] . " value=" . $arrbrand['brand_id'] . ">" . $arrbrand['brand_name'] . "</option>";
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_coa4($id)
	{
		$data = '';
		if ($id == 1) {
			foreach ($this->db->get_where('coa_4', ['coa3_id' => 10])->result_array() as $value) {
				$data .= "<option value=" . $value['id'] . ">" . $value['nama'] . "</option>";
			}
		} else if ($id == 2) {
			foreach ($this->db->get_where('coa_4', ['coa3_id' => 11])->result_array() as $value) {
				$data .= "<option value=" . $value['id'] . ">" . $value['nama'] . "</option>";
			}
		} else if ($id == 4) {
			foreach ($this->db->get_where('coa_4', ['coa3_id' => 180])->result_array() as $value) {
				$data .= "<option value=" . $value['id'] . ">" . $value['nama'] . "</option>";
			}
		}
		echo $data;
	}

	public function get_form($id)
	{
		if ($id == 2) {
			$data = 1;
		} else $data = 0;
		echo $data;
	}
	public function get_produk($id)
	{
		$produks = $this->db->query("SELECT * FROM produk_global WHERE brand_id = $id")->result_array();
		$data = '<option><option>';
		foreach ($produks as $produk) {
			$data .= '<option value="' . $produk['id'] . '">' . $produk['nama_produk'] . '</option>';
		}
		echo $data;
	}

	public function simpan()
	{
		$this->db->trans_begin();
		foreach ($_POST['input2'] as $input) {
			$kode = $this->db->get_where('coa_4', ['coa3_id' => $input['coa3_id']])->result_array();
			$kode = count($kode) + 1;
			$coaBiaya = $this->Model->insert_coa_biaya($input['coa3_id'], $input['activities_name'], $kode);

			$cekMarketing = $input['type_biaya'];
			$coaAkumulasi = $this->Model->insert_coa_akumulasi($input['activities_name'], $coaBiaya['id']);
			$coaAmortisasi = $this->Model->insert_coa_amortisasi($input['activities_name'], $coaBiaya['id'], $cekMarketing);
			$coaBank = $this->db->get_where('coa_4', ['id' => $input['coa4_pakai']])->row_array();

			$t_activities = ([
				'activities_name' => $input['activities_name'],
				'activities_date' => $input['activities_date'],
				'activities_span' => $input['activities_span'],
				'activities_value' => str_replace(",", "", $input['activities_value']),
				'activities_paid' => str_replace(",", "", $input['activities_paid']),
				'activities_type' => $input['coa3_id'],
				'pic_id' => $_SESSION['user_id'],
				'activities_update_date' => date('Y-m-d H:i:s'),
				'activities_create_user' => $_SESSION['user_id'],
				'coa_last_code' => $kode,
				'coa_id' => $coaBiaya['id'],
				'coa_amortitation_id' => $coaAmortisasi['id'],
				'coa_amortitation_accumulation_id' => $coaAkumulasi['id'],
			]);
			$this->db->insert('t_activities', $t_activities);
			$t_activities['activities_id'] = $this->db->insert_id();

			$header = ([
				'coa_transaction_date' => $input['activities_date'],
				'date_create' => date('Y-m-d'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_debit' => $t_activities['activities_value'],
				'coa_transaction_credit' => $t_activities['activities_value'],
				'coa_transaction_payment' => $t_activities['activities_value'],
				'coa_transaction_realization' => 1,
				'coa_transaction_realization_date' => date('Y-m-d'),
			]);
			$this->db->insert('t_coa_transaction_header', $header);
			$idHeader = $this->db->insert_id();

			$akunBiaya = ([
				'coa_name' => $coaBiaya['nama'],
				'coa_code' => $coaBiaya['kode'],
				'coa_date' => $input['activities_date'],
				'coa_level' => 4,
				'coa_debit' => $t_activities['activities_value'],
				'coa_transaction_note' => 'Dibayar Dimuka By ' . $_SESSION['user_fullname'],
				'date_create' => date('Y-m-d'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 13,
				'coa_transaction_source_id' => $t_activities['activities_id'],
				'coa_group_id' => $idHeader,
				'coa_id' => $coaBiaya['id'],
				'coa_transaction_realization' => 1,
			]);
			$this->db->insert('t_coa_transaction', $akunBiaya);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $akunBiaya[coa_debit] WHERE coa_id = '$akunBiaya[coa_id]' AND coa_level = '$akunBiaya[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $akunBiaya[coa_debit] WHERE coa_id = '$akunBiaya[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunBiaya['coa_date'] . "', '-', '') AND coa_level = '$akunBiaya[coa_level]'");

			if ($t_activities['activities_paid'] > 0) {
				$akunBank = ([
					'coa_name' => $coaBank['nama'],
					'coa_code' => $coaBank['kode'],
					'coa_date' => $input['activities_date'],
					'coa_credit' => $t_activities['activities_paid'],
					'coa_transaction_note' => 'Dibayar Dimuka By ' . $_SESSION['user_fullname'],
					'date_create' => date('Y-m-d'),
					'user_create' => $_SESSION['user_id'],
					'coa_transaction_source' => 13,
					'coa_transaction_source_id' => $t_activities['activities_id'],
					'coa_group_id' => $idHeader,
					'coa_id' => $coaBank['id'],
					'coa_transaction_realization' => 1,
				]);
				if ($input['activities_type'] == 3) {
					$akunBank['coa_level'] = 3;
				} else {
					$akunBank['coa_level'] = 4;
				}
				$this->db->insert('t_coa_transaction', $akunBank);
				$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $akunBank[coa_credit] WHERE coa_id = '$akunBank[coa_id]' AND coa_level = '$akunBank[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $akunBank[coa_credit] WHERE coa_id = '$akunBank[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunBank['coa_date'] . "', '-', '') AND coa_level = '$akunBank[coa_level]'");
			}

			if ($t_activities['activities_value'] > $t_activities['activities_paid']) {
				$coaUtang = $this->Model->insert_coa_utang($input['activities_name']);
				$akunUtang = ([
					'coa_name' => $coaUtang['nama'],
					'coa_code' => $coaUtang['kode'],
					'coa_date' => $input['activities_date'],
					'coa_level' => 4,
					'coa_credit' => $t_activities['activities_value'] - $t_activities['activities_paid'],
					'coa_transaction_note' => 'Dibayar Dimuka By ' . $_SESSION['user_fullname'],
					'date_create' => date('Y-m-d'),
					'user_create' => $_SESSION['user_id'],
					'coa_transaction_source' => 13,
					'coa_transaction_source_id' => $t_activities['activities_id'],
					'coa_group_id' => $idHeader,
					'coa_id' => $coaUtang['id'],
					'coa_transaction_realization' => 1,
				]);
				$this->db->insert('t_coa_transaction', $akunUtang);
				$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $akunUtang[coa_credit] WHERE coa_id = '$akunUtang[coa_id]' AND coa_level = '$akunUtang[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $akunUtang[coa_credit] WHERE coa_id = '$akunUtang[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunUtang['coa_date'] . "', '-', '') AND coa_level = '$akunUtang[coa_level]'");
			}

			if ($cekMarketing == 2) {
				foreach ($input['id_brand'] as $index => $brand) {
					$this->db->insert('t_activities_brand', ['activities_id' => $t_activities['activities_id'], 'brand_id' => $brand]);
					if (@$input['id_product'][$index] == null) {
						$this->Model->insertCoaBrand($input['activities_name'], $brand, $coaBiaya['id'], $coaAmortisasi['id']);
					} else {
						$produk = $input['id_product'][$index];
						$this->db->insert('t_activities_brand_product', ['activities_id' => $t_activities['activities_id'], 'brand_id' => $brand, 'product_id' => $produk]);
						$this->Model->insertCoaProduk($input['activities_name'], $brand, $produk, $coaBiaya['id'], $coaAmortisasi['id']);
					}
				}
			}
			if (date('Y-m') != substr($input['activities_date'], 0, 7)) {
				$this->input_old_coa($input['activities_date'], $t_activities, $coaAkumulasi, $coaAmortisasi, $idHeader, $input['presentase'], $input['activities_span']);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(base_url() . 'Dibayar_dimuka/form');
	}

	public function input_old_coa($date, $t_activities, $coaAkum, $coaAmortisasi, $idHeader, $presentase, $span)
	{
		$tgl1 = $date;
		$tgl2 = date('Y-m-d');
		$awal1 = substr(str_replace('-', '', $tgl1), 0, 6);
		$akhir1 = substr(str_replace('-', '', $tgl1), 6, 8);
		$awal2 = substr(str_replace('-', '', $tgl2), 0, 6);
		$akhir2 = substr(str_replace('-', '', $tgl2), 6, 8);
		$tgl_akhir1 = date("t", strtotime($tgl1));
		$cek = 0;
		$cekspan = 1;
		for ($i = ($awal1 + 1); $i <= $awal2; $i++) {
			if ($cekspan <= $span) {
				if (substr($i, 4, 6) == 13) {
					$i = (substr($i, 0, 4) + 1) . '01';
				}
				$tgl_akhir = date("Y-m-t", strtotime(substr($i, 0, 4) . '-' . substr($i, 4, 6) . '-01'));
				if ($awal2 == $i) {
					if ($akhir1 > substr($tgl_akhir, 8, 10)) {
						if ($akhir1 <= $akhir2 || ($akhir1 == $tgl_akhir1 && $akhir2 == substr($tgl_akhir, 8, 10))) {
							$cek++;
							$this->Model->input_old_coa($tgl_akhir, $t_activities, $coaAkum, $coaAmortisasi, $idHeader, $presentase, $cek);
						}
					} else {
						if ($akhir1 <= $akhir2) {
							$cek++;
							$this->Model->input_old_coa(substr($tgl_akhir, 0, 8) . $akhir1, $t_activities, $coaAkum, $coaAmortisasi, $idHeader, $presentase, $cek);
						}
					}
				} else {
					if ($akhir1 > substr($tgl_akhir, 8, 10)) {
						$cek++;
						$this->Model->input_old_coa(substr($tgl_akhir, 0, 8) . substr($tgl_akhir, 8, 10), $t_activities, $coaAkum, $coaAmortisasi, $idHeader, $presentase, $cek);
					} else {
						$cek++;
						$this->Model->input_old_coa(substr($tgl_akhir, 0, 8) . $akhir1, $t_activities, $coaAkum, $coaAmortisasi, $idHeader, $presentase, $cek);
					}
				}
				echo "<br>";
				if (substr($i, 4, 6) == 12) $i += 88;
				$cekspan++;
			}
		}
		return '';
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function get_data_detail($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_detail($id);
	}

	public function detail($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['id'] = $id;
		$datacontent['title'] = 'Data Penyusutan';
		$datacontent['card'] = $this->db->query("SELECT A.asset_value, A.asset_date, sum(B.coa_credit) as coa_credit FROM t_activities A left JOIN t_coa_transaction B ON B.coa_id = A.coa_shrink_id WHERE A.asset_id = '$id' group by asset_id")->row_array();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
}
