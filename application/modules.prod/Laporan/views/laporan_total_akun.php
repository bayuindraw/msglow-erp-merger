<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Laporan Total Akun
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
        <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1" style="font-size: 10px">
          <thead>
            <tr style="text-align: center; vertical-align: middle;">
              <th width="7%" rowspan="2" >Kode</th>
              <th rowspan="2">Akun</th>
              <th colspan="2">Jan</th>
              <th colspan="2">Feb</th>
              <th colspan="2">Mar</th>
              <th colspan="2">Apr</th>
              <th colspan="2">Mei</th>
              <th colspan="2">Jun</th>
              <th colspan="2">Jul</th>
              <th colspan="2">Agu</th>
              <th colspan="2">Sept</th>
              <th colspan="2">Okt</th>
              <th colspan="2">Nov</th>
              <th colspan="2">Des</th>
            </tr>
            <tr>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
              <th>D</th>
              <th>K</th>
            </tr>
          </thead>
        <tbody>
          <?php foreach($akuns as $akun){
             ?>
            <tr>
              <td><strong><?= $akun['kode1'] ?><strong></td>
              <td><strong><?= $akun['nama1'] ?></strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
              <?php 
              if(@count(@$akun['coa2']) > 0){
              
              foreach($akun['coa2'] as $akun2){ ?>
              <tr>
                <td>&nbsp;&nbsp;<?= $akun2['kode2'] ?></td>
                <td>&nbsp;&nbsp;<?= $akun2['nama2'] ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <?php if(@count(@$akun2['coa3']) > 0){
              
              foreach($akun2['coa3'] as $akun3){ ?>
                <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;<?= $akun3['kode3'] ?></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;<?= $akun3['nama3'] ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['01']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['01']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['02']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['02']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['03']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['03']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['04']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['04']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['05']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['05']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['06']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['06']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['07']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['07']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['08']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['08']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['09']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['09']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['10']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['10']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['11']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['11']['coa_total_credit']) ?></td>
                <td>Rp <?= str_replace(".00","",$akun3['history4']['12']['coa_total_debit']) ?></td>
                <td>Rp <?= str_replace(".00", "", $akun3['history4']['12']['coa_total_credit']) ?></td>
              </tr>
              <?php }
              } ?>
              <?php }
              } ?>
          <?php } ?>
        </tbody>
      </table>
    </div>

    </div>
  </div>


</div>