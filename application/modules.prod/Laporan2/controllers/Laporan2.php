<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Laporan2 extends CI_Controller
{

	var $url_ = "Laporan2";
	var $ind_ = "Laporan2";

	public function list_boleh_kirim_vs_kirim()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data List Boleh Kirim & Kirim';
		$data['file'] = 'List Boleh Kirim & Kirim';
		$data['content'] = $this->load->view($this->url_ . '/list_boleh_kirim_vs_kirim', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_list_boleh_kirim_vs_kirim()
	{
		$datacontent['datatable'] = $this->Model->get_data_list_boleh_kirim_vs_kirim();
	}

	public function list_boleh_kirim_vs_kirim_seller($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['id'] = $id;
		$datacontent['title'] = 'Data List Boleh Kirim & Kirim Seller';
		$data['file'] = 'List Boleh Kirim & Kirim Seller';
		$data['content'] = $this->load->view($this->url_ . '/list_boleh_kirim_vs_kirim_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_list_boleh_kirim_vs_kirim_seller($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_list_boleh_kirim_vs_kirim_seller($id);
	}

	public function String2Date($dTgl)
	{
		list($cMonth, $cDate, $cYear)	= explode("/", $dTgl);
		if (strlen($cYear) == 4) {
			$dTgl	= $cYear . "-" . $cMonth . "-" . $cDate;
		}
		return $dTgl;
	}

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
		error_reporting(0);
	}

	public function test_print()
	{
		$this->load->view('test_print');
	}

	public function DateStampIndo()
	{
		date_default_timezone_set("Asia/Jakarta");
		$tanggal = date("m Y");

		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);

		$split = explode(' ', $tanggal);
		$Data = $bulan[(int) $split[0]] . ' ' . $split[1];

		return $Data;
	}

	public function DateStampIndo2()
	{
		date_default_timezone_set("Asia/Jakarta");
		$tanggal = date("d m Y");

		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);

		$split = explode(' ', $tanggal);
		$Data = $split[0] . ' ' . $bulan[(int) $split[1]] . ' ' . $split[2];

		return $Data;
	}

	public function chart()
	{
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$bulan = date('m');
		$tahun = date('Y');
		if (@$_POST['cBulan'] != "") $bulan = $_POST['cBulan'];
		if (@$_POST['cTahun'] != "") $tahun = $_POST['cTahun'];
		$datacontent['data_pendingan'] = $this->db->query("SELECT SUM(pending_quantity) as sum FROM t_sales_detail")->row_array();
		$datacontent['data_pembagian'] = $this->db->query("SELECT SUM(package_detail_quantity) as sum FROM t_package_trial_detail_draft WHERE package_id IS NULL")->row_array();
		$datacontent['po_belumselesai'] = $this->db->query("SELECT COUNT(id_po_produk) as sum FROM po_produk WHERE active = 'Y'")->row_array();
		$datacontent['barang_hari_ini'] = $this->db->query("SELECT jumlah FROM terima_produk WHERE REPLACE(tgl_terima, '-', '') = SUBSTR(REPLACE(NOW(), '-', ''), 1, 8)")->row_array();
		$datacontent['barang_keluar_ini'] = $this->db->query("SELECT SUM(package_detail_quantity) as sum FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE  REPLACE(A.package_date, '-', '') = SUBSTR(REPLACE(NOW(), '-', ''), 1, 8)")->row_array();
		$datacontent['stok_barang_menipis'] = $this->db->query("SELECT COUNT(id_barang) as sum FROM tb_stock_produk B WHERE SUBSTR(REPLACE(B.tanggal, '-', ''), 1, 6) = SUBSTR(REPLACE(NOW(), '-', ''), 1, 6) AND jumlah <= min_jumlah")->row_array();
		$datacontent['tahun'] = $tahun;
		$datacontent['bulan'] = $bulan;
		$datacontent['ind_tanggal'] = $arrbln[$bulan] . " " . $tahun;
		$datacontent['url'] = $this->url_;
		if ($_SESSION['role_id'] == "5") {
			$datacontent['title'] = 'Laporan Harian Barang Keluar';
			$data['file'] = 'Laporan';
			$tgl_terima2 = $tahun . '-' . $bulan;
			$arrdat_masuk = $this->db->query("SELECT SUM(jumlah) as jum, RIGHT(tgl_terima, 2) as tgl FROM terima_produk WHERE LEFT(tgl_terima, 7) = '$tgl_terima2' GROUP BY tgl_terima")->result_array();
			foreach ($arrdat_masuk as $index_masuk => $value_masuk) {
				$datacontent['arrdat_masuk'][$value_masuk['tgl']] = $value_masuk['jum'];
			}
			$arrdat_keluar = $this->db->query("SELECT SUM(A.package_detail_quantity) as jum, RIGHT(B.package_date, 2) as tgl FROM t_package_trial_detail A LEFT JOIN t_package_trial B ON B.package_id = A.package_id WHERE LEFT(B.package_date, 7) = '$tgl_terima2' GROUP BY B.package_date")->result_array();
			foreach ($arrdat_keluar as $index_keluar => $value_keluar) {
				$datacontent['arrdat_keluar'][$value_keluar['tgl']] = $value_keluar['jum'];
			}
			$datacontent['arrout_topten'] = $this->db->query("SELECT SUM(A.package_detail_quantity) as jum, nama_produk FROM t_package_trial_detail A LEFT JOIN t_package_trial B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = A.product_id WHERE LEFT(B.package_date, 7) = '$tgl_terima2' GROUP BY A.product_id ORDER BY SUM(A.package_detail_quantity) DESC LIMIT 10")->result_array();
			$datacontent['arrout_bottomten'] = $this->db->query("SELECT SUM(A.package_detail_quantity) as jum, nama_produk FROM t_package_trial_detail A LEFT JOIN t_package_trial B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = A.product_id WHERE LEFT(B.package_date, 7) = '$tgl_terima2' GROUP BY A.product_id ORDER BY SUM(A.package_detail_quantity) ASC LIMIT 10")->result_array();
			$datacontent['arrin_factory'] = $this->db->query("SELECT SUM(A.jumlah) as jum, C.nama_factory FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po LEFT JOIN factory C ON C.id_factory = B.factory WHERE LEFT(A.tgl_terima, 7) = '$tgl_terima2' GROUP BY B.factory")->result_array();
			$data['content'] = $this->load->view($this->url_ . '/chart_barang_jadi', $datacontent, TRUE);
		} else if ($_SESSION['role_id'] == "17" || $_SESSION['role_id'] == "7" || $_SESSION['role_id'] == "12" || $_SESSION['role_id'] == "10" || $_SESSION['role_id'] == "0" || $_SESSION['role_id'] == "23") {
			$datacontent['title'] = 'Laporan Harian Barang Keluar';
			$data['file'] = 'Laporan';
			$tgl_terima2 = $tahun . '-' . $bulan;
			$arrdat_masuk = $this->db->query("SELECT SUM(account_detail_sales_product_allow) as jum, RIGHT(A.account_detail_sales_date, 2) as tgl FROM t_account_detail_sales A LEFT JOIN t_account_detail_sales_product B ON B.account_detail_sales_id = A.account_detail_sales_id WHERE LEFT(account_detail_sales_date, 7) = '$tgl_terima2' GROUP BY A.account_detail_sales_date")->result_array();
			foreach ($arrdat_masuk as $index_masuk => $value_masuk) {
				$datacontent['arrdat_masuk'][$value_masuk['tgl']] = $value_masuk['jum'];
			}
			$arrdat_keluar = $this->db->query("SELECT SUM(A.package_detail_quantity) as jum, RIGHT(B.package_date, 2) as tgl FROM t_package_trial_detail A LEFT JOIN t_package_trial B ON B.package_id = A.package_id WHERE LEFT(B.package_date, 7) = '$tgl_terima2' GROUP BY B.package_date")->result_array();
			foreach ($arrdat_keluar as $index_keluar => $value_keluar) {
				$datacontent['arrdat_keluar'][$value_keluar['tgl']] = $value_keluar['jum'];
			}
			$arrdat_pending = $this->db->query("SELECT * FROM r_pending WHERE REPLACE(pending_date, '-', '') BETWEEN SUBSTR(REPLACE(DATE_SUB(NOW(), INTERVAL 30 DAY), '-', ''),1 , 8) AND SUBSTR(REPLACE(NOW(), '-', ''),1 , 8);")->result_array();
			$datacontent['arrdat_pending'] = "";
			$datacontent['arrdat_pending_date'] = "";
			foreach ($arrdat_pending as $index_pending => $value_pending) {
				if ($index_pending > 0) {
					$datacontent['arrdat_pending'] .= ", ";
					$datacontent['arrdat_pending_date'] .= ", ";
				}
				$datacontent['arrdat_pending'] .= $value_pending['pending_quantity'];
				$datacontent['arrdat_pending_date'] .= '"' . $value_pending['pending_date'] . '"';
			}
			$datacontent['arrout_topten'] = $this->db->query("SELECT SUM(B.account_detail_sales_product_allow) as jum, C.nama_produk FROM t_account_detail_sales A JOIN t_account_detail_sales_product B ON B.account_detail_sales_id = A.account_detail_sales_id JOIN produk C ON C.id_produk = B.product_id WHERE LEFT(A.account_detail_sales_date, 7) = '$tgl_terima2' GROUP BY B.product_id ORDER BY SUM(B.account_detail_sales_product_allow) DESC LIMIT 10")->result_array();
			$arrdat_masuk = $this->db->query("SELECT B.factory, A.tgl_terima, SUM(jumlah) as jumlah, C.nama_factory FROM terima_produk A JOIN po_produk B ON B.kode_po = A.kode_po JOIN factory C ON C.id_factory = B.factory WHERE REPLACE(A.tgl_terima, '-', '') BETWEEN SUBSTR(REPLACE(DATE_SUB(NOW(), INTERVAL 30 DAY), '-', ''),1 , 8) AND SUBSTR(REPLACE(NOW(), '-', ''),1 , 8) GROUP BY B.factory, A.tgl_terima")->result_array();
			$arrmasuktgl = array();
			$arrmasukfactory = array();
			foreach ($arrdat_masuk as $index => $value) {
				$arrdata_masuk2[$value['factory']][$value['tgl_terima']] = $value;
				$arrmasuktgl[$value['tgl_terima']] = $value['tgl_terima'];
				$arrmasukfactory[$value['factory']] = $value['nama_factory'];
			}
			$datacontent['arrdat_barang_masuk'] = array();
			foreach ($arrmasukfactory as $index2 => $value2) {
				$datacontent['arrdat_barang_masuk'][$index2] = array();
				$val = "[";
				$tgl = "[";
				$i = 0;
				foreach ($arrmasuktgl as $index3 => $value3) {
					if ($i > 0) {
						$val .= ", ";
						$tgl .= ", ";
					}
					if (@$arrdata_masuk2[$index2][$index3] == "") $val .= 0;
					else $val .= $arrdata_masuk2[$index2][$index3]['jumlah'];
					$tgl .= '"' . $value3 . '"';
					$i++;
				}
				$val .= "]";
				$tgl .= "]";
				$datacontent['arrdat_barang_masuk'][$index2] += ['name' => $value2, 'data' => $val];
			}
			$datacontent['tgl_barang_masuk'] = @$tgl;
			$data['content'] = $this->load->view($this->url_ . '/chart_penjualan', $datacontent, TRUE);
		}
		//$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', isset($data) ? $data : "");
	}

	public function monthly_all()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_all', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function report_all()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/report-all', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function journal()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Jurnal';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_journal', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function format_db($tgl)
	{
		$arrtgl = explode('/', $tgl);
		$new_format = $arrtgl['2'] . '-' . $arrtgl['0'] . '-' . $arrtgl['1'];
		return $new_format;
	}

	public function print_journal()
	{
		$from = $this->format_db($_POST['from']);
		$to = $this->format_db($_POST['to']);

		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);



		//$sheet->getStyle('C4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f8cbad');
		$arrdata = $this->db->query("SELECT A.*, B.kode as  kode_4, C.kode as  kode_3, D.kode as  kode2, E.kode as  kode_1 FROM t_coa_transaction A LEFT JOIN coa_4 B ON B.id = A.coa_id LEFT JOIN coa_3 C ON C.id = A.coa_id  LEFT JOIN coa_2 D ON D.id = A.coa_id  LEFT JOIN coa_1 E ON E.id = A.coa_id WHERE (REPLACE(A.coa_date, '-', '') BETWEEN REPLACE('$from', '-', '') AND REPLACE('$to', '-', ''))")->result_array();

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:F2')->applyFromArray($style);
		$sheet->getStyle('B2:F2')->getFont()->setSize(20);
		$sheet->getStyle('B2:F2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'JURNAL UMUM ' . $_POST['from'] . ' s/d ' . $_POST['to'] . '');
		$sheet->mergeCells('B2:F2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		//$sheet->mergeCells($abjad.$awal.':'.$abjad.($awal+1));
		$sheet->setCellValue($abjad++ . $awal, 'NO AKUN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'TANGGAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'DEBIT');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KREDIT');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'KETERANGAN');
		for ($x = $abjad_awal;; $x++) {
			$sheet->getStyle($x . ($awal) . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($x == $abjad) break;
		}
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		//$sheet->getStyle('E5:I1')->getAlignment()->setWrapText(true);
		$nawal = $awal;
		$i = 1;
		foreach ($arrdata as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			if ($value1['coa_level'] == 1) $coa_id = $value1['kode_1'];
			else if ($value1['coa_level'] == 2) $coa_id = $value1['kode_2'];
			else if ($value1['coa_level'] == 3) $coa_id = $value1['kode_3'];
			else if ($value1['coa_level'] == 4) $coa_id = $value1['kode_4'];
			$sheet->setCellValue($nabjad++ . $nawal, $coa_id);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['coa_date']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['coa_debit']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['coa_credit']);
			$sheet->setCellValue($nabjad . $nawal, $value1['coa_transaction_note']);
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
		}


		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"JURNAL UMUM " . $_POST['from'] . " s/d " . $_POST['to'] . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function search_member($search)
	{
		$search = str_replace('%20', ' ', $search);
		//echo "SELECT nama, kode, kota FROM member WHERE nama LIKE '%$search%' OR kode LIKE '%$search%'";
		//sdie(); 
		/*$data2 = $this->db->query("SELECT B.seller_id, SUM(A.sales_detail_quantity-A.sales_detail_quantity_send) AS total FROM t_sales_detail A LEFT JOIN t_sales B ON B.sales_id = A.sales_id WHERE A.sales_detail_quantity > A.sales_detail_quantity_send AND A.product_id = '$product_id' GROUP BY B.seller_id")->result_array(); 
		foreach($data2 as $index2 => $value2){
			$pending[$value2['seller_id']] = $value2['total'];  
		}*/
		$whereand = "";
		if ($_SESSION['role_id'] == "7") {
			$whereand = " AND (user_pic_id = '" . $_SESSION['user_id'] . "' OR user_pic_id IS NULL)";
		}
		$data = $this->db->query("SELECT nama, kode, kota FROM member WHERE kode NOT LIKE '%.%' AND (nama LIKE '%$search%' OR kode LIKE '%$search%')  AND schema_id NOT IN(2, 3) $whereand")->result_array();
		$dat = array();
		foreach ($data as $index => $value) {
			$dat[$index] = array();
			$dat[$index]['id'] = $value["kode"];
			$dat[$index]['text'] = $value["nama"] . ' (' . $value["kode"] . ') (' . $value["kota"] . ')';
			//echo json_encode($dat[$index]);
		}
		$array = array(
			'results' => $dat,
			'pagination' => array('more' => true)
		);
		echo json_encode($array);
	}

	public function daily_distribution()
	{
		$datacontent['url'] = $this->url_;
		// $datacontent['title'] = 'Laporan Bulanan';
		$datacontent['title'] = 'Laporan Pengiriman';
		$datacontent['product_type_list'] = "";
		/*foreach ($this->Model->get_product_type() as $key => $value) {
			$datacontent['product_type_list'] = $datacontent['product_type_list'] . '<option value="' . $value['product_type_name'] . '">' . $value['product_type_name'] . '</option>';
		}*/
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_daily_distribution', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function excel_daily_distribution()
	{

		//SELECT A.sales_code, A.sales_date, D.nama_produk, B.sales_detail_id, B.sales_detail_quantity - IFNULL(SUM(bridge_beta_quantity), 0) as kurang_order, B.sales_detail_price FROM t_sales A JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN produk C ON C.id_produk = B.product_id JOIN produk_global D ON D.kode = C.kd_pd LEFT JOIN t_bridge_beta E ON E.sales_detail_id = B.sales_detail_id LEFT JOIN t_package_trial F ON F.package_id = E.package_id AND DATE_FORMAT(F.package_date, '%Y%m') < '202109' WHERE A.seller_id = 'MSGLOW066' GROUP BY B.sales_detail_id
		//SELECT SUM(E.bridge_beta_quantity), F.package_date, B.sales_detail_id FROM t_sales A JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN produk C ON C.id_produk = B.product_id JOIN produk_global D ON D.kode = C.kd_pd JOIN t_bridge_beta E ON E.sales_detail_id = B.sales_detail_id JOIN t_package_trial F ON F.package_id = E.package_id WHERE A.seller_id = 'MSGLOW066' AND DATE_FORMAT(F.package_date, '%Y%m') = '202109' GROUP BY B.sales_detail_id, F.package_date
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$tgl_awal = str_replace('-', '', $_POST['tgl_awal']);
		$tgl_akhir = str_replace('-', '', $_POST['tgl_akhir']);
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);



		$arrpr = $this->db->query("SELECT A.sales_id, A.sales_code, A.sales_date, D.nama_produk, B.sales_detail_id, B.sales_detail_quantity - IFNULL(SUM(bridge_beta_quantity), 0) as kurang_order, B.sales_detail_price FROM t_sales A JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN produk C ON C.id_produk = B.product_id JOIN produk_global D ON D.kode = C.kd_pd LEFT JOIN t_bridge_beta E ON E.sales_detail_id = B.sales_detail_id LEFT JOIN t_package_trial F ON F.package_id = E.package_id WHERE A.seller_id = '" . $_POST['seller_id'] . "' GROUP BY B.sales_detail_id")->result_array();
		foreach ($arrpr as $indexpr => $valuepr) {
			$dataprmain[$valuepr['sales_id']] = $valuepr;
			$datapr[$valuepr['sales_id']][$valuepr['sales_detail_id']] = $valuepr;
		}
		$arrkirim = $this->db->query("SELECT SUM(E.bridge_beta_quantity) as jum, F.package_date, B.sales_detail_id FROM t_sales A JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN produk C ON C.id_produk = B.product_id JOIN produk_global D ON D.kode = C.kd_pd JOIN t_bridge_beta E ON E.sales_detail_id = B.sales_detail_id JOIN t_package_trial F ON F.package_id = E.package_id WHERE A.seller_id = 'MSGLOW066' AND DATE_FORMAT(F.package_date, '%Y%m%d') BETWEEN $tgl_awal AND $tgl_akhir GROUP BY B.sales_detail_id, F.package_date")->result_array();
		foreach ($arrkirim as $indexkirim => $valuekirim) {
			$datakirim[$valuekirim['package_date']][$valuekirim['sales_detail_id']] = $valuekirim['jum'];
		}
		$abjad = 'A';
		$awal = 5;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');
		$sheet->setCellValue($abjad++ . $awal, 'TGL ORDER');
		$sheet->setCellValue($abjad++ . $awal, 'KEKURANGAN ORDER');

		$sheet->setCellValue($abjad . $awal, 'TANGGAL PENGIRIMAN');

		foreach ($datakirim as $indexxxxkirim => $valuexxxkirim) {
			$sheet->setCellValue($abjad++ . ($awal + 1), $indexxxxkirim);
		}
		$sheet->setCellValue($abjad++ . $awal, 'Total Kirim');
		$sheet->setCellValue($abjad++ . $awal, 'Kurang Kirim');
		$sheet->setCellValue($abjad++ . $awal, 'Harga');
		$sheet->setCellValue($abjad++ . $awal, 'Total Penjualan');
		$nawal = $awal;
		$no = 1;
		$nawal++;
		foreach ($dataprmain as $key => $value) {
			$nabjad = 'A';
			$paling_awal = $nabjad;
			$nawal++;
			if (@$produk_merge_awal == "") {
				$produk_merge_awal = $nawal;
			}
			$sheet->setCellValue($nabjad . $nawal++, $value['sales_code']);
			foreach ($datapr[$key] as $prkey => $prvalue) {
				$xnabjad = $paling_awal;
				$sheet->setCellValue($xnabjad++ . $nawal, $prvalue['nama_produk']);
				$sheet->setCellValue($xnabjad++ . $nawal, $prvalue['sales_date']);
				$sheet->setCellValue($xnabjad++ . $nawal, 0);
				$tot = 0;
				foreach ($datakirim as $indexxxxkirim => $valuexxxkirim) {
					//echo $prvalue['sales_detail_id'];
					//print_r(@$datakirim[$indexxxxkirim]);
					$sheet->setCellValue($xnabjad . $nawal, @$datakirim[$indexxxxkirim][$prvalue['sales_detail_id']]);
					$xnabjad++;
					$tot += (@$datakirim[$indexxxxkirim][$prvalue['sales_detail_id']] == "") ? 0 : @$datakirim[$indexxxxkirim][$prvalue['sales_detail_id']];
				}
				$sheet->setCellValue($xnabjad++ . $nawal, $tot);
				$sheet->setCellValue($xnabjad++ . $nawal, 0);
				$sheet->setCellValue($xnabjad++ . $nawal, $prvalue['sales_detail_price']);
				$sheet->setCellValue($xnabjad++ . $nawal, 0);
				$nawal++;
			}
			$tgl_xawal = $_POST['tgl_awal'];
			$tgl_xakhir = $_POST['tgl_akhir'];
		}
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"STOCK BARANG JADI MSGLOW.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function sample()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Sample';
		$datacontent['product_type_list'] = "";
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_sample', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function excel_sample()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$tgl_awal = str_replace('-', '', $_POST['tgl_awal']);
		$tgl_akhir = str_replace('-', '', $_POST['tgl_akhir']);
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$datas = $this->db->query("SELECT * FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN produk C ON C.id_produk = B.product_id WHERE A.member_code = 'MSGLOWXX102' AND DATE_FORMAT(A.package_date, '%Y%m%d') BETWEEN $tgl_awal AND $tgl_akhir")->result_array();

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "LIST BARANG SAMPLE");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SJ');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'TANGGAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BATCH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'JUMLAH');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'D';
		$xxxabjad = 'C';
		$i = 1;
		foreach ($datas as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			//$value1['debit'] = (float) $value1['debit'];
			$sheet->setCellValue($nabjad++ . $nawal, $i++);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['package_delivery_number']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['package_date']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['package_detail_batch']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['package_detail_quantity']);
		}
		$nawal++;
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN SAMPLE.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function rusak()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Rusak';
		$datacontent['product_type_list'] = "";
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_rusak', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function excel_rusak()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$tgl_awal = str_replace('-', '', $_POST['tgl_awal']);
		$tgl_akhir = str_replace('-', '', $_POST['tgl_akhir']);
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$datas = $this->db->query("SELECT * FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN produk C ON C.id_produk = B.product_id WHERE A.member_code = 'MSGLOWXX101' AND A.warehouse_id = '".$_SESSION['warehouse_id']."' AND DATE_FORMAT(A.package_date, '%Y%m%d') BETWEEN $tgl_awal AND $tgl_akhir")->result_array();

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "LIST BARANG RUSAK");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SJ');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'TANGGAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BATCH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'JUMLAH');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'D';
		$xxxabjad = 'C';
		$i = 1;
		foreach ($datas as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			//$value1['debit'] = (float) $value1['debit'];
			$sheet->setCellValue($nabjad++ . $nawal, $i++);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['package_delivery_number']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['package_date']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['package_detail_batch']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['package_detail_quantity']);
		}
		$nawal++;
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN RUSAK.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function daily_stock()
	{
		$datacontent['url'] = $this->url_;
		// $datacontent['title'] = 'Laporan Bulanan';
		$datacontent['title'] = 'Laporan Pembagian';
		$datacontent['product_type_list'] = "";
		/*foreach ($this->Model->get_product_type() as $key => $value) {
			$datacontent['product_type_list'] = $datacontent['product_type_list'] . '<option value="' . $value['product_type_name'] . '">' . $value['product_type_name'] . '</option>';
		}*/
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_daily_stock', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function factory_in_difference()
	{
		$datacontent['url'] = $this->url_;
		// $datacontent['title'] = 'Laporan Bulanan';
		$datacontent['title'] = 'Laporan Pembagian';
		$datacontent['product_type_list'] = "";
		$datacontent['factory_list'] = "";
		foreach ($this->Model->get_factory() as $key => $value) {
			$datacontent['factory_list'] = $datacontent['factory_list'] . '<option value="' . $value['id_factory'] . '">' . $value['nama_factory'] . '</option>';
		}
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_factory_in_difference', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function excel_daily_stock()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$tgl_awal = str_replace('-', '', $_POST['tgl_awal']);
		$tgl_akhir = str_replace('-', '', $_POST['tgl_akhir']);
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);



		$arrproduct = $this->db->query("SELECT *, A.nama_produk FROM produk A JOIN m_product_type B ON B.product_type_name = A.klasifikasi JOIN tb_stock_produk C ON C.id_barang = A.id_produk WHERE A.status = 1 AND C.warehouse_id = '" . $_SESSION['warehouse_id'] . "' ORDER BY C.order_x")->result_array();
		
		$arrout = $this->db->query("SELECT SUM(B.package_detail_quantity) as jum, A.package_date, B.product_id FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE A.member_code NOT IN('MSGLOWXX101', 'MSGLOWXX102') AND REPLACE(A.package_date, '-', '') BETWEEN $tgl_awal AND $tgl_akhir AND A.warehouse_id = '" . $_SESSION['warehouse_id'] . "' GROUP BY B.product_id, A.package_date")->result_array();
		foreach ($arrout as $keyout => $valueout) {
			$dataout[$valueout['package_date']][$valueout['product_id']] = $valueout['jum'];
		}
		$arroutrs = $this->db->query("SELECT SUM(B.package_detail_quantity) as jum, A.package_date, B.product_id, A.member_code FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE A.member_code IN('MSGLOWXX101', 'MSGLOWXX102') AND REPLACE(A.package_date, '-', '') BETWEEN $tgl_awal AND $tgl_akhir AND A.warehouse_id = '" . $_SESSION['warehouse_id'] . "' GROUP BY A.member_code, B.product_id, A.package_date")->result_array();

		foreach ($arroutrs as $keyoutrs => $valueoutrs) {
			if ($valueoutrs['member_code'] == 'MSGLOWXX101') {

				$datarusak[$valueoutrs['package_date']][$valueoutrs['product_id']] = $valueoutrs['jum'];
			} else if ($valueoutrs['member_code'] == 'MSGLOWXX102') {
				$datasample[$valueoutrs['package_date']][$valueoutrs['product_id']] = $valueoutrs['jum'];
			}
		}
		$arrin = $this->db->query("SELECT SUM(A.jumlah) as jum, A.tgl_terima, A.id_barang FROM terima_produk A WHERE REPLACE(A.tgl_terima, '-', '') BETWEEN $tgl_awal AND $tgl_akhir AND A.warehouse_id = '" . $_SESSION['warehouse_id'] . "' GROUP BY A.tgl_terima, A.id_barang")->result_array();
		foreach ($arrin as $keyin => $valuein) {
			$datain[$valuein['tgl_terima']][$valuein['id_barang']] = $valuein['jum'];
		}
		/*$arrrepair = $this->db->query("SELECT SUM(jumlah) jumlah, id_barang, tgl_terima FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po WHERE LEFT(tgl_terima, 7) = '" . $tahun . "-" . $bulan . "' AND factory = '19' GROUP BY tgl_terima, id_barang")->result_array();
		foreach ($arrrepair as $keyterima => $valueterima) {
			$datarepair[$valueterima['id_barang']][$valueterima['tgl_terima']] = $valueterima;
		}

		$arrkeluar = $this->db->query("SELECT SUM(package_detail_quantity) jumlah, product_id, package_date FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE LEFT(package_date, 7) = '" . $tahun . "-" . $bulan . "' AND member_code NOT IN('MSGLOWXX101', 'MSGLOWXX102') GROUP BY package_date, product_id")->result_array();
		foreach ($arrkeluar as $keykeluar => $valuekeluar) {
			$datakeluar[$valuekeluar['product_id']][$valuekeluar['package_date']] = $valuekeluar;
		}

		$arrrusak = $this->db->query("SELECT SUM(package_detail_quantity) jumlah, product_id, package_date FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE LEFT(package_date, 7) = '" . $tahun . "-" . $bulan . "' AND member_code = 'MSGLOWXX101' GROUP BY package_date, product_id")->result_array();
		foreach ($arrrusak as $keykeluar => $valuekeluar) {
			$datarusak[$valuekeluar['product_id']][$valuekeluar['package_date']] = $valuekeluar;
		}

		$arrsample = $this->db->query("SELECT SUM(package_detail_quantity) jumlah, product_id, package_date FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE LEFT(package_date, 7) = '" . $tahun . "-" . $bulan . "' AND member_code = 'MSGLOWXX102' GROUP BY package_date, product_id")->result_array();
		foreach ($arrsample as $keykeluar => $valuekeluar) {
			$datasample[$valuekeluar['product_id']][$valuekeluar['package_date']] = $valuekeluar;
		}*/
		$abjad = 'A';
		$awal = 5;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->setCellValue($abjad++ . $awal, 'KLASIFIKASI');
		$sheet->setCellValue($abjad++ . $awal, 'KODE BARANG');
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');

		$nawal = $awal;
		$tgl_xawal = $_POST['tgl_awal'];
		$tgl_xakhir = $_POST['tgl_akhir'];

		for ($x = $tgl_xawal; str_replace('-', '', $x) <= str_replace('-', '', $tgl_xakhir); $x = date('Y-m-d', strtotime($x . "+1 days"))) {
			$sheet->setCellValue($abjad . ($awal), $x);
			$x2 = date('Y-m-d', strtotime($x . "-1 days"));
			$tabjadawal = $abjad;
			$sheet->setCellValue($abjad++ . ($awal + 1), 'Stok Awal');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'Barang Datang');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'Retur Gudang');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'Barang Keluar');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'Sampel');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'Reject');
			$sheet->mergeCells($tabjadawal . ($awal) . ':' . $abjad . ($awal));
			$sheet->setCellValue($abjad++ . ($awal + 1), 'Stok Akhir');
			$arrquery1 = $this->db->query("SELECT A.id_barang, jumlah + IFNULL(jumlah_masuk, 0) - IFNULL(jumlah_keluar, 0) as stock_awal FROM tb_stock_produk_history A LEFT JOIN (SELECT ZA.id_barang, SUM(ZA.jumlah) as jumlah_masuk FROM terima_produk ZA WHERE REPLACE(ZA.tgl_terima, '-', '') BETWEEN " . substr(str_replace('-', '', $x), 0, 6) . "01 AND REPLACE('" . $x2 . "', '-', '') AND ZA.warehouse_id = '" . $_SESSION['warehouse_id'] . "' GROUP BY ZA.id_barang) B ON B.id_barang = A.id_barang LEFT JOIN (SELECT XB.product_id, SUM(XB.package_detail_quantity) as jumlah_keluar FROM t_package_trial XA JOIN t_package_trial_detail XB ON XB.package_id = XA.package_id WHERE REPLACE(XA.package_date, '-', '') BETWEEN " . substr(str_replace('-', '', $x), 0, 6) . "01 AND REPLACE('" . $x2 . "', '-', '') AND XA.warehouse_id = '" . $_SESSION['warehouse_id'] . "' GROUP BY XB.product_id) C ON C.product_id = A.id_barang WHERE REPLACE(LEFT(A.tanggal, 7), '-', '') = REPLACE(LEFT('" . $x . "', 7), '-', '') AND A.warehouse_id = '" . $_SESSION['warehouse_id'] . "'")->result_array();
			foreach ($arrquery1 as $indexarr1 => $valuearr1) {
				$arrstockawal[$x][$valuearr1['id_barang']] = $valuearr1['stock_awal'];
			}
		}
		$no = 1;
		$nawal++;
		foreach ($arrproduct as $key => $value) {

			$nabjad = 'A';
			$paling_awal = $nabjad;
			$nawal++;
			if (@$produk_merge_awal == "") {
				$produk_merge_awal = $nawal;
			}
			$sheet->setCellValue($nabjad++ . $nawal, $no++);
			$sheet->setCellValue($nabjad++ . $nawal, $value['klasifikasi']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['kode_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk']);

			$tgl_xawal = $_POST['tgl_awal'];
			$tgl_xakhir = $_POST['tgl_akhir'];

			for ($x = $tgl_xawal; str_replace('-', '', $x) <= str_replace('-', '', $tgl_xakhir); $x = date('Y-m-d', strtotime($x . "+1 days"))) {
				$sheet->setCellValue($nabjad++ . $nawal, ((@$arrstockawal[$x][$value['id_produk']] == "") ? 0 : $arrstockawal[$x][$value['id_produk']]));
				$sheet->setCellValue($nabjad++ . $nawal, ((@$datain[$x][$value['id_produk']] == "") ? 0 : @$datain[$x][$value['id_produk']]));
				$sheet->setCellValue($nabjad++ . $nawal, 0);
				$sheet->setCellValue($nabjad++ . $nawal, ((@$dataout[$x][$value['id_produk']] == "") ? 0 : @$dataout[$x][$value['id_produk']]));
				$sheet->setCellValue($nabjad++ . $nawal, ((@$datasample[$x][$value['id_produk']] == "") ? 0 : @$datasample[$x][$value['id_produk']]));
				$sheet->setCellValue($nabjad++ . $nawal, ((@$datarusak[$x][$value['id_produk']] == "") ? 0 : @$datarusak[$x][$value['id_produk']]));
				$sheet->setCellValue($nabjad++ . $nawal, ((@$arrstockawal[$x][$value['id_produk']] == "") ? 0 : $arrstockawal[$x][$value['id_produk']]) + ((@$datain[$x][$value['id_produk']] == "") ? 0 : @$datain[$x][$value['id_produk']]) - ((@$dataout[$x][$value['id_produk']] == "") ? 0 : @$dataout[$x][$value['id_produk']]) - ((@$datasample[$x][$value['id_produk']] == "") ? 0 : @$datasample[$x][$value['id_produk']]) - ((@$datarusak[$x][$value['id_produk']] == "") ? 0 : @$datarusak[$x][$value['id_produk']]));
			}
		}
		/*foreach ($arrdata as $key => $value) {
			$nabjad = 'B';
			$paling_awal = $nabjad;
			$nawal++;
			if (@$produk_merge_awal == "") {
				$produk_merge_awal = $nawal;
			}

			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk']);
			//$xx = 1;
			if (@$rgb == "") $rgb = 'e9f7df';
			if ($produk_global != $value['nama_produk']) {
				//$xx = 0;
				$xabjad = $nabjad;
				if ($produk_merge_akhir != "") {

					$sheet->mergeCells($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_akhir);
					$sheet->getStyle($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_awal)->applyFromArray($style);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
					//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
				}
				$produk_merge_awal = $nawal;
			}
			$produk_global = $value['klasifikasi'];
			$sheet->setCellValue($nabjad++ . $nawal, $value['klasifikasi']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['jumlah']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['rusak']);
			$jumlah = $value['jumlah'];
			$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$jmlterima = 0;
			$jmlrepair = 0;
			$jmlmasuk = 0;
			$jmlkirim = 0;
			$jmlrusak = 0;
			$jmlsample = 0;
			$jmlkeluar = 0;
			for ($i = 1; $i <= $nJumlahHari; $i++) {
				if ($i > 9) {
					$cNol = "";
				} else {
					$cNol = "0";
				}
				$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
				$no = 0;

				$sheet->setCellValue($nabjad++ . $nawal, @$dataterima[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datarepair[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datakeluar[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datarusak[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datasample[$value['id_barang']][$date]['jumlah']);
				$jmlterima += ((@$dataterima[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_barang']][$date]['jumlah']);
				$jmlrepair += ((@$datarepair[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarepair[$value['id_barang']][$date]['jumlah']);
				$jmlmasuk += ((@$dataterima[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_barang']][$date]['jumlah']) + ((@$datarepair[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarepair[$value['id_barang']][$date]['jumlah']);
				$jmlkirim += ((@$datakeluar[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datakeluar[$value['id_barang']][$date]['jumlah']);
				$jmlrusak += ((@$datarusak[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarusak[$value['id_barang']][$date]['jumlah']);
				$jmlsample += ((@$datasample[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datasample[$value['id_barang']][$date]['jumlah']);
				$jmlkeluar += ((@$datakeluar[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datakeluar[$value['id_barang']][$date]['jumlah']) + ((@$datarusak[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarusak[$value['id_barang']][$date]['jumlah']) + ((@$datasample[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datasample[$value['id_barang']][$date]['jumlah']);
				//$jmlkeluar += $jmlkeluar + (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']);
				//$jumlah = $jumlah + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima[$value['id_kemasan']][$date]['jumlah']) - (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']); 
			}
			$jumlah = (int)$jumlah + (int)$jmlmasuk - (int)$jmlkeluar;
			$sheet->setCellValue($nabjad++ . $nawal, $jmlterima);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlrepair);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlmasuk);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlkirim);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlrusak);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlsample);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlkeluar);
			$sheet->setCellValue($nabjad . $nawal, $jumlah);
			$produk_merge_akhir = $nawal;
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
			$rgb = @$value['category_product_rgb'];
			if ($rgb == "") $rgb = 'e9f7df';
			if ($jumlah < 20000) {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
			} else {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
			}
		}*/

		/*$sheet->mergeCells($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_akhir);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray2);
		$sheet->getStyle($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_awal)->applyFromArray($style);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
		

		$nawal++;
		$nawal++;
		$sheet->setCellValue($abjad_awal . $nawal++, 'NB:');
		$abjad_awal2 = $abjad_awal;
		$sheet->getStyle($abjad_awal2 . $nawal . ':' . $abjad_awal2++ . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
		$sheet->setCellValue($abjad_awal2 . $nawal++, 'STOK DIBAWAH 20.000 PCS');
		*/

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"STOCK BARANG JADI MSGLOW.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function daily_pr()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Harian PR';
		$datacontent['product_type_list'] = "";
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_daily_pr', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function excel_kartu_stock($bulan, $tahun, $product_id)
	{

		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);




		$arrstock_awal = $this->db->query("SELECT  A.*,B.nama_produk as produk FROM tb_stock_produk_history A LEFT JOIN produk B ON B.id_produk = A.id_barang WHERE REPLACE(LEFT(tanggal, 7), '-', '') = " . $tahun . $bulan . " AND id_barang = '$product_id'")->row_array();
		$arrdat_in = $this->db->query("SELECT * FROM v_terima_produk WHERE REPLACE(LEFT(tgl_terima, 7), '-', '') = '" . $tahun . $bulan . "' AND id_barang = '" . $product_id . "'")->result_array();
		foreach ($arrdat_in as $index_in => $value_in) {
			$dat_in[$value_in['tgl_terima']] = $value_in;
			if (@$dat_all_in_sj[$value_in['tgl_terima']] == "") {
				$dat_all_in_sj[$value_in['tgl_terima']] = $value_in['kode_factory'] . " : " . number_format($value_in['jumlah_sj']);
			} else {
				$dat_all_in_sj[$value_in['tgl_terima']] .= '
				' . $value_in['kode_factory'] . " : " . number_format($value_in['jumlah_sj']);
			}
			if (@$dat_all_in[$value_in['tgl_terima']] == "") {
				$in_value[$value_in['tgl_terima']] = $value_in['jumlah'];
				$dat_all_in[$value_in['tgl_terima']] = $value_in['kode_factory'] . " : " . number_format($value_in['jumlah']);
			} else {
				$in_value[$value_in['tgl_terima']] += $value_in['jumlah'];
				$dat_all_in[$value_in['tgl_terima']] .= '
				' . $value_in['kode_factory'] . " : " . number_format($value_in['jumlah']);
			}
		}
		$arrdat_out = $this->db->query("SELECT * FROM v_trial_out A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE REPLACE(LEFT(A.package_date, 7), '-', '') = '" . $tahun . $bulan . "' AND product_id = '" . $product_id . "'")->result_array();
		foreach ($arrdat_out as $index_out => $value_out) {
			$dat_out[$value_out['package_date']] = $value_out;
			if (@$dat_all_out[$value_out['package_date']] == "") {
				$out_value[$value_out['package_date']] = $value_out['package_detail_quantity'];
				$dat_all_out[$value_out['package_date']] = $value_out['member_code'] . " " . $value_out['nama'] . " " . $value_out['package_dropship'] . " : " . number_format($value_out['package_detail_quantity']);
			} else {
				$out_value[$value_out['package_date']] += $value_out['package_detail_quantity'];
				$dat_all_out[$value_out['package_date']] .= '
				' . $value_out['member_code'] . " " . $value_out['nama'] . " " . $value_out['package_dropship'] . " : " . number_format($value_out['package_detail_quantity']);
			}
		}
		$abjad = 'B';
		$xawal = 2;
		$abjad_awal = $abjad;
		$no = 1;

		$awal = $xawal;
		$xabjad = $abjad_awal;
		$xabjad++;
		$xabjad++;
		$sheet->setCellValue($xabjad . $awal++, 'KARTU STOCK ' . $arrstock_awal['produk']);
		$sheet->setCellValue($xabjad . $awal++, $arrbln[$bulan] . ' ' . $tahun);
		$awal++;

		$xabjad = $abjad_awal;
		$awalalpha = $awal;
		$sheet->getColumnDimension($xabjad)->setWidth(15);
		$sheet->setCellValue($xabjad++ . $awal, 'Tanggal');
		$sheet->setCellValue($xabjad++ . $awal, 'Jumlah Awal');
		$sheet->getColumnDimension($xabjad)->setWidth(20);
		$sheet->setCellValue($xabjad++ . $awal, 'Draft Pemasukan');
		$sheet->getColumnDimension($xabjad)->setWidth(30);
		$sheet->setCellValue($xabjad++ . $awal, 'Realisasi Pemasukan');
		$sheet->getColumnDimension($xabjad)->setWidth(30);
		$sheet->setCellValue($xabjad++ . $awal, 'Pengeluaran');
		$sheet->setCellValue($xabjad++ . $awal++, 'Jumlah Akhir');
		$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		$jum_awal = "";
		for ($i = 1; $i <= $nJumlahHari; $i++) {
			if ($i == 1) $jum_awal = $arrstock_awal['jumlah'];
			$nabjad = $abjad_awal;
			if ($i > 9) {
				$cNol = "";
			} else {
				$cNol = "0";
			}

			$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
			$no = 0;
			$sheet->setCellValue($nabjad++ . $awal, $date);
			if (str_replace('-', '', date('Y-m-d')) >= str_replace('-', '', $date)) {
				$sheet->setCellValue($nabjad++ . $awal, $jum_awal);
				$sheet->getStyle($nabjad . $awal)->getAlignment()->setWrapText(true);
				$sheet->setCellValue($nabjad++ . $awal, (@$dat_all_in_sj[$date] == "") ? 0 : $dat_all_in_sj[$date]);
				$sheet->getStyle($nabjad . $awal)->getAlignment()->setWrapText(true);
				$sheet->setCellValue($nabjad++ . $awal, (@$dat_all_in[$date] == "") ? 0 : $dat_all_in[$date]);
				$sheet->getStyle($nabjad . $awal)->getAlignment()->setWrapText(true);
				$sheet->setCellValue($nabjad++ . $awal, (@$dat_all_out[$date] == "") ? 0 : $dat_all_out[$date]);
				if (@$in_value[$date] != "") $jum_awal = $jum_awal + $in_value[$date];
				if (@$out_value[$date] != "") $jum_awal = $jum_awal - $out_value[$date];
				$sheet->setCellValue($nabjad++ . $awal, $jum_awal);
			}
			$awal++;
		}
		/*$xabjad++;
			$xabjadbeta = $xabjad;  
			$xabjad = $abjad_awal;
			$sheet->setCellValue($xabjad++ . $awal, 'Tanggal');
			$sheet->setCellValue($xabjad++ . $awal, 'Jumlah Awal');
			$sheet->setCellValue($xabjad++ . $awal, 'Draft Pemasukan');
			$sheet->setCellValue($xabjad++ . $awal, 'Realisasi Pemasukan');
			$sheet->setCellValue($xabjad++ . $awal, 'Pengeluaran');
			$sheet->setCellValue($xabjad++ . $awal++, 'Jumlah Akhir');
			$xabjad = $abjad_awal;
			$awal++;
			$akhiralpha = $awal;

			$xabjadbetax = $xabjadbeta;
			$awalbeta = $awalalpha;
			$awal = $awalbeta;
			$xawal = $akhiralpha;
			$sheet->setCellValue($xabjadbetax++ . $awal, 'NO');
			$sheet->setCellValue($xabjadbetax++ . $awal, 'Nama Produk');
			$sheet->setCellValue($xabjadbetax++ . $awal, 'Jumlah Pesan');
			$sheet->setCellValue($xabjadbetax++ . $awal, 'Terbayar');
			$sheet->setCellValue($xabjadbetax++ . $awal++, 'Terkirim');
			$xno = 1;
			foreach ($datadetail[$valuepr['sales_id']] as $indexdetail => $valuedetail) {
				$xabjadbetax = $xabjadbeta;
				$sheet->setCellValue($xabjadbetax++ . $awal, $xno++);
				$sheet->setCellValue($xabjadbetax++ . $awal, $valuedetail['nama_produk']);
				$sheet->setCellValue($xabjadbetax++ . $awal, $valuedetail['sales_detail_quantity']);
				$sheet->setCellValue($xabjadbetax++ . $awal, (($valuedetail['connected_quantity'] == "") ? 0 : $valuedetail['connected_quantity']) + (($valuedetail['pending_quantity'] == "") ? 0 : $valuedetail['pending_quantity']));
				$sheet->setCellValue($xabjadbetax++ . $awal++, $valuedetail['connected_quantity']);
			}
			$awal++;
			$akhirbeta = $awal;
			if ($akhirbeta > $akhiralpha) {
				$xawal = $akhirbeta;
			} else {
				$xawal = $akhiralpha;
			}
			$no++;*/

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"KARTU STOCK " . $arrstock_awal['produk'] . " " . $arrbln[$bulan] . " $tahun.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function excel_daily_pr()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$tgl_awal = str_replace('-', '', $_POST['tgl_awal']);
		$tgl_akhir = str_replace('-', '', $_POST['tgl_akhir']);
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);



		$arrpr = $this->db->query("SELECT * FROM t_sales A JOIN member B ON B.kode = A.seller_id WHERE REPLACE(LEFT(sales_date_create, 10), '-', '') BETWEEN 20211022 AND 20211022 AND A.user_id = 61")->result_array();
		$arrdetail = $this->db->query("SELECT B.*, D.nama_produk FROM t_sales A JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN produk C ON C.id_produk = B.product_id JOIN produk_global D ON D.kode = C.kd_pd WHERE REPLACE(LEFT(A.sales_date_create, 10), '-', '') BETWEEN 20211022 AND 20211022 AND A.user_id = 61")->result_array();
		foreach ($arrdetail as $indexdetail => $valuedetail) {
			$datadetail[$valuedetail['sales_id']][$valuedetail['sales_detail_id']] = $valuedetail;
		}
		$abjad = 'B';
		$xawal = 5;
		$abjad_awal = $abjad;
		$no = 1;
		foreach ($arrpr as $indexpr => $valuepr) {
			$awal = $xawal;
			$xabjad = $abjad_awal;
			$xabjad++;
			$xabjad++;
			$sheet->setCellValue($xabjad . $awal++, 'SURAT ORDER');
			$sheet->setCellValue($xabjad . $awal++, $valuepr['sales_date']);
			$awal++;

			$xabjad = $abjad_awal;
			$awalalpha = $awal;
			$sheet->setCellValue($xabjad++ . $awal, 'NO');
			$sheet->setCellValue($xabjad++ . $awal, 'CUSTOMER');
			$sheet->setCellValue($xabjad++ . $awal++, 'KETERANGAN');
			$xabjad++;
			$xabjadbeta = $xabjad;
			$xabjad = $abjad_awal;
			$sheet->setCellValue($xabjad++ . $awal, $no);
			$sheet->setCellValue($xabjad++ . $awal, 'ID AGEN :');
			$sheet->setCellValue($xabjad++ . $awal++, 'DETAIL ALAMAT PENGIRIMAN');
			$xabjad = $abjad_awal;
			$xabjad++;
			$sheet->setCellValue($xabjad++ . $awal, $valuepr['seller_id']);
			$sheet->mergeCells($xabjad . $awal . ':' . $xabjad . ($awal + 3));
			$sheet->getStyle($xabjad . $awal)->getAlignment()->setWrapText(true);
			$sheet->setCellValue($xabjad++ . $awal++, $valuepr['sales_address']);
			$xabjad = $abjad_awal;
			$xabjad++;
			$sheet->setCellValue($xabjad++ . $awal++, 'NAMA :');
			$xabjad = $abjad_awal;
			$xabjad++;
			$sheet->setCellValue($xabjad++ . $awal++, $valuepr['nama']);
			$xabjad = $abjad_awal;
			$xabjad++;
			$sheet->setCellValue($xabjad++ . $awal++, $valuepr['kota']);
			$xabjad = $abjad_awal;
			$xabjad++;
			$sheet->setCellValue($xabjad++ . $awal, 'No PO');
			$sheet->setCellValue($xabjad++ . $awal++, 'Nama Cargo:');
			$xabjad = $abjad_awal;
			$xabjad++;
			$xabjad_reverse = $xabjad;
			$sheet->setCellValue($xabjad++ . $awal++, $valuepr['sales_code']);
			$sheet->mergeCells($xabjad_reverse . ($awal - 1) . ':' . $xabjad . ($awal - 1));
			$awal++;
			$akhiralpha = $awal;

			$xabjadbetax = $xabjadbeta;
			$awalbeta = $awalalpha;
			$awal = $awalbeta;
			$xawal = $akhiralpha;
			$sheet->setCellValue($xabjadbetax++ . $awal, 'NO');
			$sheet->setCellValue($xabjadbetax++ . $awal, 'Nama Produk');
			$sheet->setCellValue($xabjadbetax++ . $awal, 'Jumlah Pesan');
			$sheet->setCellValue($xabjadbetax++ . $awal, 'Terbayar');
			$sheet->setCellValue($xabjadbetax++ . $awal++, 'Terkirim');
			$xno = 1;
			foreach ($datadetail[$valuepr['sales_id']] as $indexdetail => $valuedetail) {
				$xabjadbetax = $xabjadbeta;
				$sheet->setCellValue($xabjadbetax++ . $awal, $xno++);
				$sheet->setCellValue($xabjadbetax++ . $awal, $valuedetail['nama_produk']);
				$sheet->setCellValue($xabjadbetax++ . $awal, $valuedetail['sales_detail_quantity']);
				$sheet->setCellValue($xabjadbetax++ . $awal, (($valuedetail['connected_quantity'] == "") ? 0 : $valuedetail['connected_quantity']) + (($valuedetail['pending_quantity'] == "") ? 0 : $valuedetail['pending_quantity']));
				$sheet->setCellValue($xabjadbetax++ . $awal++, $valuedetail['connected_quantity']);
			}
			$awal++;
			$akhirbeta = $awal;
			if ($akhirbeta > $akhiralpha) {
				$xawal = $akhirbeta;
			} else {
				$xawal = $akhiralpha;
			}
			$no++;
		}
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN ORDER HARIAN.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function division_daily()
	{
		$datacontent['url'] = $this->url_;
		// $datacontent['title'] = 'Laporan Bulanan';
		$datacontent['title'] = 'Laporan Pembagian';
		$datacontent['product_type_list'] = "";
		foreach ($this->Model->get_product_type() as $key => $value) {
			$datacontent['product_type_list'] = $datacontent['product_type_list'] . '<option value="' . $value['product_type_name'] . '">' . $value['product_type_name'] . '</option>';
		}
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_division_daily', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function division_daily_to_excel()
	{
		$product_type = $_POST['product_type'];
		$tgl_awal = str_replace('-', '', $_POST['tgl_awal']);
		$tgl_akhir = str_replace('-', '', $_POST['tgl_akhir']);
		$draft = isset($_POST['draft']) ? $_POST['draft'] : null;
		$realisasi = isset($_POST['realisasi']) ? $_POST['realisasi'] : null;

		if (empty($product_type)) {
			$where_klasifikasi = "";
		} else {
			$where_klasifikasi = "AND klasifikasi = '$product_type'";
		}

		if (is_null($draft)) {
			//realisasi
			$arrreturn['type'] = 'Realisasi';
			$arrdata = $this->db->query("SELECT C.nama, C.kode, D.nama_produk, SUM(B.package_detail_quantity) AS package_detail_quantity, A.package_date, B.product_id FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN member C ON C.kode = A.member_code JOIN produk D ON D.id_produk = B.product_id WHERE A.package_status_id = '1' AND (REPLACE(A.package_date, '-', '') BETWEEN '$tgl_awal' AND '$tgl_akhir') $where_klasifikasi GROUP BY C.kode, D.id_produk, A.package_date")->result_array();
		} else {
			// draft
			$arrreturn['type'] = 'Draft';
			$arrdata = $this->db->query("SELECT B.product_id, B.seller_id, D.nama, C.nama_produk, B.package_detail_quantity FROM t_package_trial_detail_draft B LEFT JOIN produk C ON C.id_produk = B.product_id AND (C.is_delete = 0 OR C.is_delete IS NULL) LEFT JOIN member D ON D.kode = B.seller_id WHERE B.package_id IS NULL $where_klasifikasi AND B.seller_id NOT IN('MSGLOWXX101', 'MSGLOWXX102')")->result_array();
		}
		foreach ($arrdata as $index => $value) {
			$arrreturn['arrdata'][$value['product_id']][] = $value;
			$arrreturn['arrproduk'][$value['product_id']] = $value['nama_produk'];
		}
		$data['content'] = $this->load->view($this->url_ . '/excel_pembagian', $arrreturn);
	}

	public function pembagian_outstanding()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Total Pembagian';
		$datacontent['product_type_list'] = "";
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/laporan_pembagian_outstanding', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function pembagian_outstanding_to_excel()
	{
		$tgl_awal = str_replace('-', '', $_POST['tgl_awal']);
		$tgl_akhir = str_replace('-', '', $_POST['tgl_akhir']);
		$draft = isset($_POST['draft']) ? $_POST['draft'] : null;
		$realisasi = isset($_POST['realisasi']) ? $_POST['realisasi'] : null;

		if (is_null($draft)) {
			//realisasi
			$arrdata['pembagian'] = $this->db->query("SELECT SUM(B.package_detail_quantity) as jum_tot, C.nama_produk, A.package_date FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN produk C ON C.id_produk = B.product_id WHERE REPLACE(A.package_date, '-', '') BETWEEN " . $tgl_awal . " AND " . $tgl_akhir . " GROUP BY B.product_id, A.package_date order by A.package_date")->result_array();
			$data['content'] = $this->load->view($this->url_ . '/excel_pembagian_outstanding', $arrdata);
		} else {
			// draft
			$arrdata['pembagian'] = $this->db->query("SELECT SUM(A.package_detail_quantity) as jum_tot, B.nama_produk FROM t_package_trial_detail_draft A JOIN produk B ON B.id_produk = A.product_id WHERE A.package_detail_quantity <> 0 GROUP BY A.product_id")->result_array();
			$data['content'] = $this->load->view($this->url_ . '/excel_pembagian_outstanding_draft', $arrdata);
		}
	}

	public function datatable_draft_total_pembagian()
	{
		$this->Model->get_data_draft_total_pembagian();
	}

	public function datatable_realisasi_total_pembagian()
	{
		$tgl_awal = str_replace('-', '', $_GET['tgl_awal']);
		$tgl_akhir = str_replace('-', '', $_GET['tgl_akhir']);
		$this->Model->get_data_realisasi_total_pembagian($tgl_awal, $tgl_akhir);
	}

	public function mpdf_division_daily()
	{
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		// $tanggal = $_POST['cTanggal'];
		// if (empty($tanggal)) {
		// 	$tgl_temp = date("Y-m-d");
		// 	$tgl_formater = (int)substr($tgl_temp, 8, 2) . " " . $arrbln[substr($tgl_temp, 5, 2)] . " " . substr($tgl_temp, 0, 4);
		// } else {
		// 	$tgl_formater = (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4);
		// }
		$product_type = $_POST['product_type'];
		if (empty($product_type)) {
			$WhereAndDraft = "";
			$WhereAndRealisasi = "";
		} else {
			$WhereAndDraft = "AND C.klasifikasi = '$product_type'";
			$WhereAndRealisasi = "AND D.klasifikasi = '$product_type'";
			// $WhereAndRealisasi = "D.klasifikasi = '$product_type'";
		}

		if (@$_POST['draft']) {
			$arrdata = $this->db->query("SELECT B.product_id, B.seller_id, D.nama, C.nama_produk, B.package_detail_quantity FROM t_package_trial_detail_draft B LEFT JOIN produk C ON C.id_produk = B.product_id AND (C.is_delete = 0 OR C.is_delete IS NULL) LEFT JOIN member D ON D.kode = B.seller_id WHERE B.package_id IS NULL $WhereAndDraft AND B.seller_id NOT IN('MSGLOWXX101', 'MSGLOWXX102')")->result_array();
			$arrreturn['type'] = 'draft';
			$arrreturn['detail_type'] = 'DRAFT';
			$arrreturn['cek_data'] = $this->db->query("SELECT B.product_id, B.seller_id, D.nama, C.nama_produk, B.package_detail_quantity FROM t_package_trial_detail_draft B LEFT JOIN produk C ON C.id_produk = B.product_id AND (C.is_delete = 0 OR C.is_delete IS NULL) LEFT JOIN member D ON D.kode = B.seller_id WHERE B.package_id IS NULL $WhereAndDraft AND B.seller_id NOT IN('MSGLOWXX101', 'MSGLOWXX102')");
		} else {
			// $arrdata = $this->db->query("SELECT C.nama, C.kode, D.nama_produk, SUM(B.package_detail_quantity) AS package_detail_quantity, A.package_date, B.product_id FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN member C ON C.kode = A.member_code JOIN produk D ON D.id_produk = B.product_id WHERE package_date = '" . $tanggal . "' $WhereAndRealisasi GROUP BY C.kode, D.id_produk, A.package_date")->result_array();
			// $arrreturn['type'] = 'realisasi';
			// $arrreturn['cek_data'] = $this->db->query("SELECT C.nama, C.kode, D.nama_produk, SUM(B.package_detail_quantity) AS package_detail_quantity, A.package_date, B.product_id FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN member C ON C.kode = A.member_code JOIN produk D ON D.id_produk = B.product_id WHERE package_date = '" . $tanggal . "' $WhereAndRealisasi GROUP BY C.kode, D.id_produk, A.package_date");

			$arrdata = $this->db->query("SELECT C.nama, C.kode, D.nama_produk, SUM(B.package_detail_quantity) AS package_detail_quantity, A.package_date, B.product_id FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN member C ON C.kode = A.member_code JOIN produk D ON D.id_produk = B.product_id WHERE A.package_status_id = '1' $WhereAndRealisasi GROUP BY C.kode, D.id_produk, A.package_date")->result_array();
			$arrreturn['type'] = 'realisasi';
			$arrreturn['cek_data'] = $this->db->query("SELECT C.nama, C.kode, D.nama_produk, SUM(B.package_detail_quantity) AS package_detail_quantity, A.package_date, B.product_id FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN member C ON C.kode = A.member_code JOIN produk D ON D.id_produk = B.product_id WHERE A.package_status_id = '1' $WhereAndRealisasi GROUP BY C.kode, D.id_produk, A.package_date");
		}
		foreach ($arrdata as $index => $value) {
			$arrreturn['arrdata'][$value['product_id']][] = $value;
			$arrreturn['arrproduk'][$value['product_id']] = $value['nama_produk'];
		}
		// $arrreturn['tgl_format'] = $tgl_formater;
		$arrreturn['product_type'] = @$product_type;
		$data['content'] = $this->load->view($this->url_ . '/excel_division_daily', $arrreturn);
	}

	public function preview_division_daily()
	{

		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		// $tanggal = $_POST['cTanggal'];
		// if (empty($tanggal)) {
		// 	$tgl_temp = date("Y-m-d");
		// 	$tgl_formater = (int)substr($tgl_temp, 8, 2) . " " . $arrbln[substr($tgl_temp, 5, 2)] . " " . substr($tgl_temp, 0, 4);
		// } else {
		// 	$tgl_formater = (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4);
		// }

		$product_type = $_POST['product_type'];
		if ($product_type == "all") {
			$WhereAndDraft = " ";
			$WhereAndRealisasi = " ";
		} else {
			$WhereAndDraft = "AND C.klasifikasi = '$product_type'";
			$WhereAndRealisasi = "AND D.klasifikasi = '$product_type'";
			// $WhereAndRealisasi = "D.klasifikasi = '$product_type'";
		}

		if (@$_POST['draft'] == "draft") {
			$arrdata = $this->db->query("SELECT B.product_id, B.seller_id, D.nama, C.nama_produk, B.package_detail_quantity FROM t_package_trial_detail_draft B LEFT JOIN produk C ON C.id_produk = B.product_id AND (C.is_delete = 0 OR C.is_delete IS NULL) LEFT JOIN member D ON D.kode = B.seller_id WHERE B.package_id IS NULL $WhereAndDraft AND B.seller_id NOT IN('MSGLOWXX101', 'MSGLOWXX102')")->result_array();

			$arrreturn['type'] = 'draft';
			$arrreturn['detail_type'] = 'DRAFT';
			$arrreturn['cek_data'] = $this->db->query("SELECT B.product_id, B.seller_id, D.nama, C.nama_produk, B.package_detail_quantity FROM t_package_trial_detail_draft B LEFT JOIN produk C ON C.id_produk = B.product_id AND (C.is_delete = 0 OR C.is_delete IS NULL) LEFT JOIN member D ON D.kode = B.seller_id WHERE B.package_id IS NULL $WhereAndDraft AND B.seller_id NOT IN('MSGLOWXX101', 'MSGLOWXX102')");
		} else {
			// $arrdata = $this->db->query("SELECT C.nama, C.kode, D.nama_produk, SUM(B.package_detail_quantity) AS package_detail_quantity, A.package_date, B.product_id FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN member C ON C.kode = A.member_code JOIN produk D ON D.id_produk = B.product_id WHERE package_date = '" . $tanggal . "' $WhereAndRealisasi GROUP BY C.kode, D.id_produk, A.package_date")->result_array();
			// $arrreturn['type'] = 'realisasi';
			// $arrreturn['cek_data'] = $this->db->query("SELECT C.nama, C.kode, D.nama_produk, SUM(B.package_detail_quantity) AS package_detail_quantity, A.package_date, B.product_id FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN member C ON C.kode = A.member_code JOIN produk D ON D.id_produk = B.product_id WHERE package_date = '" . $tanggal . "' $WhereAndRealisasi GROUP BY C.kode, D.id_produk, A.package_date");
			if (isset($_POST['tgl_awal'])) {
				$tgl_awal = $_POST['tgl_awal'];
			} else $tgl_awal = date('Y-m-d');

			if (isset($_POST['tgl_akhir'])) {
				$tgl_akhir = $_POST['tgl_akhir'];
			} else $tgl_akhir = date('Y-m-d');

			$arrdata = $this->db->query("SELECT C.nama, C.kode, D.nama_produk, SUM(B.package_detail_quantity) AS package_detail_quantity, A.package_date, B.product_id FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN member C ON C.kode = A.member_code JOIN produk D ON D.id_produk = B.product_id WHERE A.package_status_id = '1' AND A.package_date BETWEEN '$tgl_awal' AND '$tgl_akhir' $WhereAndRealisasi GROUP BY C.kode, D.id_produk, A.package_date")->result_array();
			$arrreturn['type'] = 'realisasi';
			$arrreturn['cek_data'] = $this->db->query("SELECT C.nama, C.kode, D.nama_produk, SUM(B.package_detail_quantity) AS package_detail_quantity, A.package_date, B.product_id FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN member C ON C.kode = A.member_code JOIN produk D ON D.id_produk = B.product_id WHERE A.package_status_id = '1' AND A.package_date BETWEEN '$tgl_awal' AND '$tgl_akhir' $WhereAndRealisasi GROUP BY C.kode, D.id_produk, A.package_date");
		}
		foreach ($arrdata as $index => $value) {
			$arrreturn['arrdata'][$value['product_id']][] = $value;
			$arrreturn['arrproduk'][$value['product_id']] = $value['nama_produk'];
		}
		// $arrreturn['tgl_format'] = $tgl_formater;
		$arrreturn['product_type'] = @$product_type;
		$data['content'] = $this->load->view($this->url_ . '/preview_division_daily', $arrreturn);
	}

	public function mpdf_pembagian()
	{
		$data['content'] = $this->load->view($this->url_ . '/mpdf_pembagian');
	}

	public function mpdf_test()
	{
		$data['content'] = $this->load->view($this->url_ . '/mpdf_v');
	}


	public function print_monthly_all()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);



		//$sheet->getStyle('C4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f8cbad');
		$arrdata = $this->db->query("SELECT * FROM tb_stock_produk_history A JOIN produk B ON B.id_produk = A.id_barang AND B.is_delete IS NULL LEFT JOIN m_product_type C ON C.product_type_name = B.klasifikasi WHERE LEFT(tanggal, 7) = '" . $tahun . "-" . $bulan . "' AND A.warehouse_id = '" . $_SESSION['warehouse_id'] . "' ORDER BY C.product_type_order, B.id_produk")->result_array();
		$arrterima = $this->db->query("SELECT SUM(jumlah) jumlah, id_barang, tgl_terima FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po WHERE LEFT(tgl_terima, 7) = '" . $tahun . "-" . $bulan . "' AND factory <> '19' AND A.warehouse_id = '" . $_SESSION['warehouse_id'] . "' GROUP BY tgl_terima, id_barang")->result_array();
		foreach ($arrterima as $keyterima => $valueterima) {
			$dataterima[$valueterima['id_barang']][$valueterima['tgl_terima']] = $valueterima;
		}

		$arrrepair = $this->db->query("SELECT SUM(jumlah) jumlah, id_barang, tgl_terima FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po WHERE LEFT(tgl_terima, 7) = '" . $tahun . "-" . $bulan . "' AND factory = '19' AND A.warehouse_id = '" . $_SESSION['warehouse_id'] . "' GROUP BY tgl_terima, id_barang")->result_array();
		foreach ($arrrepair as $keyterima => $valueterima) {
			$datarepair[$valueterima['id_barang']][$valueterima['tgl_terima']] = $valueterima;
		}

		$arrkeluar = $this->db->query("SELECT SUM(package_detail_quantity) jumlah, product_id, package_date FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE LEFT(package_date, 7) = '" . $tahun . "-" . $bulan . "' AND member_code NOT IN('MSGLOWXX101', 'MSGLOWXX102') AND A.warehouse_id = '" . $_SESSION['warehouse_id'] . "' GROUP BY package_date, product_id")->result_array();
		foreach ($arrkeluar as $keykeluar => $valuekeluar) {
			$datakeluar[$valuekeluar['product_id']][$valuekeluar['package_date']] = $valuekeluar;
		}

		$arrrusak = $this->db->query("SELECT SUM(package_detail_quantity) jumlah, product_id, package_date FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE LEFT(package_date, 7) = '" . $tahun . "-" . $bulan . "' AND member_code = 'MSGLOWXX101' AND A.warehouse_id = '" . $_SESSION['warehouse_id'] . "' GROUP BY package_date, product_id")->result_array();
		foreach ($arrrusak as $keykeluar => $valuekeluar) {
			$datarusak[$valuekeluar['product_id']][$valuekeluar['package_date']] = $valuekeluar;
		}

		$arrsample = $this->db->query("SELECT SUM(package_detail_quantity) jumlah, product_id, package_date FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE LEFT(package_date, 7) = '" . $tahun . "-" . $bulan . "' AND member_code = 'MSGLOWXX102' AND A.warehouse_id = '" . $_SESSION['warehouse_id'] . "' GROUP BY package_date, product_id")->result_array();
		foreach ($arrsample as $keykeluar => $valuekeluar) {
			$datasample[$valuekeluar['product_id']][$valuekeluar['package_date']] = $valuekeluar;
		}
		$abjad = 'B';
		$awal = 5;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');
		$sheet->setCellValue($abjad++ . $awal, 'KLASIFIKASI');
		$sheet->setCellValue($abjad++ . $awal, 'STOK AWAL');
		$sheet->setCellValue($abjad++ . $awal, 'STOK BARANG RUSAK AWAL');

		$nawal = $awal;

		$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		for ($i = 1; $i <= $nJumlahHari; $i++) {
			if ($i > 9) {
				$cNol = "";
			} else {
				$cNol = "0";
			}
			$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
			$no = 0;
			$titik_awal = $abjad;

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG MASUK');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG REPAIR');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG KELUAR');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG RUSAK');
			$sheet->getStyle($titik_awal . $awal . ':' . $abjad . $awal)->applyFromArray($styleArray);
			$sheet->mergeCells($titik_awal . $awal . ':' . $abjad . $awal);
			$sheet->setCellValue($titik_awal . $awal, substr($arrbln[$bulan], 0, 3) . '-' . (int)($cNol . $i));

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG SAMPLE');
		}
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG DITERIMA');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG REPAIR');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG MASUK');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG TERKIRIM');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG RUSAK');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG SAMPLE');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG KELUAR');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad . $awal, 'SISA STOCK BAGUS');


		$sheet->mergeCells($abjad_awal . ($awal - 3) . ':' . $abjad . ($awal - 3));
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$sheet->setCellValue($abjad_awal . ($awal - 3), 'LAPORAN STOK BARANG JADI MS GLOW');
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setSize(20);
		$sheet->mergeCells($abjad_awal . ($awal - 2) . ':' . $abjad . ($awal - 2));
		$sheet->setCellValue($abjad_awal . ($awal - 2), 'PERIODE ' . strtoupper($arrbln[$bulan]));
		$sheet->setCellValue($abjad . ($awal - 1), date('d/m/Y H.i'));


		//$sheet->getStyle('A1:B2')->applyFromArray($styleArray);
		//$sheet ->getStyle($abjad_awal.$awal.':'.$abjad.($awal+1))->applyFromArray($styleArray);
		$sheet->freezePane('F7');
		$sheet->getSheetView()->setZoomScale(70);
		$sheet->getRowDimension($awal + 1)->setRowHeight(30);
		$irow = 1;
		$wrap_awal = "";
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal + 1))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = @$wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}
		$sheet->getStyle($wrap_awal . '5:' . $wrap_akhir . '6')->getAlignment()->setWrapText(true);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$nawal++;
		$produk_global = "";
		$produk_merge_akhir = "";

		foreach ($arrdata as $key => $value) {
			$nabjad = 'B';
			$paling_awal = $nabjad;
			$nawal++;
			if (@$produk_merge_awal == "") {
				$produk_merge_awal = $nawal;
			}

			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk']);
			//$xx = 1;
			if (@$rgb == "") $rgb = 'e9f7df';
			if ($produk_global != $value['nama_produk']) {
				//$xx = 0;
				$xabjad = $nabjad;
				if ($produk_merge_akhir != "") {

					$sheet->mergeCells($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_akhir);
					$sheet->getStyle($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_awal)->applyFromArray($style);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
					//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
				}
				$produk_merge_awal = $nawal;
			}
			$produk_global = $value['klasifikasi'];
			$sheet->setCellValue($nabjad++ . $nawal, $value['klasifikasi']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['jumlah']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['rusak']);
			$jumlah = $value['jumlah'];
			$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$jmlterima = 0;
			$jmlrepair = 0;
			$jmlmasuk = 0;
			$jmlkirim = 0;
			$jmlrusak = 0;
			$jmlsample = 0;
			$jmlkeluar = 0;
			for ($i = 1; $i <= $nJumlahHari; $i++) {
				if ($i > 9) {
					$cNol = "";
				} else {
					$cNol = "0";
				}
				$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
				$no = 0;

				$sheet->setCellValue($nabjad++ . $nawal, @$dataterima[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datarepair[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datakeluar[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datarusak[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datasample[$value['id_barang']][$date]['jumlah']);
				$jmlterima += ((@$dataterima[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_barang']][$date]['jumlah']);
				$jmlrepair += ((@$datarepair[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarepair[$value['id_barang']][$date]['jumlah']);
				$jmlmasuk += ((@$dataterima[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_barang']][$date]['jumlah']) + ((@$datarepair[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarepair[$value['id_barang']][$date]['jumlah']);
				$jmlkirim += ((@$datakeluar[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datakeluar[$value['id_barang']][$date]['jumlah']);
				$jmlrusak += ((@$datarusak[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarusak[$value['id_barang']][$date]['jumlah']);
				$jmlsample += ((@$datasample[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datasample[$value['id_barang']][$date]['jumlah']);
				$jmlkeluar += ((@$datakeluar[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datakeluar[$value['id_barang']][$date]['jumlah']) + ((@$datarusak[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarusak[$value['id_barang']][$date]['jumlah']) + ((@$datasample[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datasample[$value['id_barang']][$date]['jumlah']);
				//$jmlkeluar += $jmlkeluar + (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']);
				//$jumlah = $jumlah + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima[$value['id_kemasan']][$date]['jumlah']) - (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']); 
			}
			$jumlah = (int)$jumlah + (int)$jmlmasuk - (int)$jmlkeluar;
			$sheet->setCellValue($nabjad++ . $nawal, $jmlterima);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlrepair);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlmasuk);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlkirim);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlrusak);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlsample);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlkeluar);
			$sheet->setCellValue($nabjad . $nawal, $jumlah);
			$produk_merge_akhir = $nawal;
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
			$rgb = @$value['category_product_rgb'];
			if ($rgb == "") $rgb = 'e9f7df';
			if ($jumlah < 20000) {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
			} else {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
			}
		}

		//if($xx == 1){
		$sheet->mergeCells($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_akhir);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray2);
		$sheet->getStyle($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_awal)->applyFromArray($style);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
		//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
		//}

		$nawal++;
		$nawal++;
		$sheet->setCellValue($abjad_awal . $nawal++, 'NB:');
		$abjad_awal2 = $abjad_awal;
		$sheet->getStyle($abjad_awal2 . $nawal . ':' . $abjad_awal2++ . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
		$sheet->setCellValue($abjad_awal2 . $nawal++, 'STOK DIBAWAH 20.000 PCS');
		/*foreach($arrcategoryproduct as $keyproduct => $valueproduct){
			$abjad_awal2 = $abjad_awal;
			$sheet->getStyle($abjad_awal2.$nawal.':'.$abjad_awal2++.$nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($valueproduct['category_product_rgb']);
			$sheet->setCellValue($abjad_awal2.$nawal++, $valueproduct['category_product_name']);
		}*/

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"STOCK BARANG JADI MSGLOW " . strtoupper($arrbln[$bulan]) . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}


	public function print_monthly_all2()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$arrpackage_trial = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN member B ON B.kode = A.member_code WHERE A.package_date = '$tanggal'")->result_array();
		$arrpackage_trial_detail = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = B.product_id WHERE A.package_date = '$tanggal'")->result_array();
		$arrpackage_trial_employee = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_employee B ON B.package_id = A.package_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.package_date = '$tanggal'")->result_array();
		foreach ($arrpackage_trial_detail as $index2 => $value2) {
			$datpackage_trial_detail[$value2['package_id']][] = $value2;
		}
		foreach ($arrpackage_trial_employee as $index3 => $value3) {
			$datpackage_trial_employee[$value3['package_id']][] = $value3;
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:F2')->applyFromArray($style);
		$sheet->getStyle('B2:F2')->getFont()->setSize(20);
		$sheet->getStyle('B2:F2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'LAPORAN PENGIRIMAN ' . (int)substr($tanggal, 8, 2) . ' ' . $arrbln[substr($tanggal, 5, 2)] . ' ' . substr($tanggal, 0, 4));
		$sheet->mergeCells('B2:F2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PRODUK');
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PEGAWAI');

		$nawal = $awal;
		foreach ($arrpackage_trial as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value1['member_code']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama'] . " (" . $value1['kode'] . ")");
			$nawalx = $nawal;
			$abjadxawal = $nabjad;
			foreach ($datpackage_trial_detail[$value1['package_id']] as $indexd2 => $valued2) {
				$x = $nawalx;
				$abjadx = $abjadxawal;
				$sheet->setCellValue($abjadx++ . $nawalx, $valued2['nama_produk']);
				$sheet->setCellValue($abjadx . $nawalx++, $valued2['package_detail_quantity']);
			}
			$abjadx++;
			$nabjad = $abjadx;
			$nawaly = $nawal;
			foreach ($datpackage_trial_employee[$value1['package_id']] as $indexd3 => $valued3) {
				$y = $nawaly;
				$abjadx = $abjadxawal;
				$sheet->setCellValue($nabjad . $nawaly++, $valued3['nama_phl']);
			}
			if ($x > $y) $nawal = $x;
			else $nawal = $y;
		}

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENGIRIMAN " . (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4) . " .xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function print_monthly_all3()
	{
		ob_end_clean();
		$from = $this->String2Date($_POST['from']);
		$to = $this->String2Date($_POST['to']);
		$arrdata = $this->db->query("SELECT A.*, B.kode as  kode_4, C.kode as  kode_3, D.kode as  kode2, E.kode as  kode_1 FROM t_coa_transaction A LEFT JOIN coa_4 B ON B.id = A.coa_id LEFT JOIN coa_3 C ON C.id = A.coa_id  LEFT JOIN coa_2 D ON D.id = A.coa_id  LEFT JOIN coa_1 E ON E.id = A.coa_id WHERE A.coa_date BETWEEN '" . $from . "' AND '" . $to . "'")->result_array();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Hello World !');

		$writer = new Xlsx($spreadsheet);
		$writer->save('hello world.xlsx');
	}

	public function daily_out()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Harian Barang Keluar';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_daily_out', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_daily_out()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$tanggal = $_POST['cTanggal'];
		$arrpackage_trial = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN member B ON B.kode = A.member_code WHERE A.package_date = '$tanggal'")->result_array();
		$arrpackage_trial_detail = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = B.product_id WHERE A.package_date = '$tanggal'")->result_array();
		$arrpackage_trial_employee = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_employee B ON B.package_id = A.package_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.package_date = '$tanggal'")->result_array();
		foreach ($arrpackage_trial_detail as $index2 => $value2) {
			$datpackage_trial_detail[$value2['package_id']][] = $value2;
		}
		foreach ($arrpackage_trial_employee as $index3 => $value3) {
			$datpackage_trial_employee[$value3['package_id']][] = $value3;
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:F2')->applyFromArray($style);
		$sheet->getStyle('B2:F2')->getFont()->setSize(20);
		$sheet->getStyle('B2:F2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'LAPORAN PENGIRIMAN ' . (int)substr($tanggal, 8, 2) . ' ' . $arrbln[substr($tanggal, 5, 2)] . ' ' . substr($tanggal, 0, 4));
		$sheet->mergeCells('B2:F2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PRODUK');
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PEGAWAI');

		$nawal = $awal;
		foreach ($arrpackage_trial as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value1['member_code']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama'] . " (" . $value1['kode'] . ")");
			$nawalx = $nawal;
			$abjadxawal = $nabjad;
			foreach ($datpackage_trial_detail[$value1['package_id']] as $indexd2 => $valued2) {
				$x = $nawalx;
				$abjadx = $abjadxawal;
				$sheet->setCellValue($abjadx++ . $nawalx, $valued2['nama_produk']);
				$sheet->setCellValue($abjadx . $nawalx++, $valued2['package_detail_quantity']);
			}
			$abjadx++;
			$nabjad = $abjadx;
			$nawaly = $nawal;
			foreach ($datpackage_trial_employee[$value1['package_id']] as $indexd3 => $valued3) {
				$y = $nawaly;
				$abjadx = $abjadxawal;
				$sheet->setCellValue($nabjad . $nawaly++, $valued3['nama_phl']);
			}
			if ($x > $y) $nawal = $x;
			else $nawal = $y;
		}

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENGIRIMAN " . (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4) . " .xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function daily_all()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Harian Stok';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_daily_all', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_daily_all()
	{

		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$tanggal = $_POST['cTanggal'];
		//$tanggal = '2020-11-05';
		$tgl_before = (date('Y-m-d', strtotime($tanggal . ' - 1 days')) < '2020-11-01') ? substr($tanggal, 0, 7) . '-01' : date('Y-m-d', strtotime($tanggal . ' - 1 days'));
		$arrstock_produk_history = $this->db->query("SELECT * FROM tb_stock_produk_history A LEFT JOIN produk B ON B.id_produk = A.id_barang LEFT JOIN m_product_type C ON C.product_type_name = B.klasifikasi WHERE LEFT(tanggal, 7) = '" . substr($tanggal, 0, 7) . "' ORDER BY C.product_type_order")->result_array();
		$arrin_past = $this->db->query("SELECT SUM(jumlah) as jumlah, id_barang as id_produk FROM terima_produk WHERE tgl_terima BETWEEN '" . substr($tanggal, 0, 7) . '-01' . "' AND '$tgl_before' GROUP BY id_barang")->result_array();
		foreach ($arrin_past as $indexin => $valuein) {
			$datin_past[$valuein['id_produk']] = $valuein['jumlah'];
		}
		$arrout_past = $this->db->query("SELECT SUM(B.package_detail_quantity) as jumlah, B.product_id as id_produk FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE A.package_date BETWEEN '" . substr($tanggal, 0, 7) . '-01' . "' AND '$tgl_before' GROUP BY B.product_id")->result_array();
		foreach ($arrout_past as $indexout => $valueout) {
			$datout_past[$valueout['id_produk']] = $valueout['jumlah'];
		}
		$arrin_new = $this->db->query("SELECT SUM(jumlah) as jumlah, id_barang as id_produk FROM terima_produk WHERE tgl_terima = '" . $tanggal . "' GROUP BY id_barang")->result_array();
		foreach ($arrin_new as $indexin_new => $valuein_new) {
			$datin_new[$valuein_new['id_produk']] = $valuein_new['jumlah'];
		}
		$arrout_new = $this->db->query("SELECT SUM(B.package_detail_quantity) as jumlah, B.product_id as id_produk FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE A.package_date = '" . $tanggal . "' GROUP BY B.product_id")->result_array();
		foreach ($arrout_new as $indexout_new => $valueout_new) {
			$datout_new[$valueout_new['id_produk']] = $valueout_new['jumlah'];
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:I2')->applyFromArray($style);
		$sheet->getStyle('B2:I2')->getFont()->setSize(20);
		$sheet->getStyle('B2:I2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'LAPORAN HARIAN ' . (int)substr($tanggal, 8, 2) . ' ' . $arrbln[substr($tanggal, 5, 2)] . ' ' . substr($tanggal, 0, 4));
		$sheet->mergeCells('B2:I2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'KLASIFIKASI');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');
		$sheet->mergeCells($abjad . $awal . ':I' . $awal);
		$sheet->getStyle($abjad . $awal . ':I' . $awal)->applyFromArray($styleArray);
		$sheet->setCellValue($abjad . $awal++, (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4));
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STOCK AWAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BARANG DATANG');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BARANG KELUAR');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STOCK AKHIR');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'KETERANGAN');
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . ($awal - 1) . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($x == $abjad) break;
		}
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		//$sheet->getStyle('E5:I1')->getAlignment()->setWrapText(true);
		$nawal = $awal;
		$i = 1;
		$klasifikasi_chk = "";
		foreach ($arrstock_produk_history as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			$stockawal = ($value1['jumlah'] + ((@$datin_past[$value1['id_barang']] == "") ? 0 : @$datin_past[$value1['id_barang']]) - ((@$datout_past[$value1['id_barang']] == "") ? 0 : @$datout_past[$value1['id_barang']]));
			$sheet->setCellValue($nabjad++ . $nawal, $i++);
			if ($klasifikasi_chk != $value1['klasifikasi']) {
				if ($klasifikasi_chk != "") {
					$sheet->mergeCells($nabjad . $nawal_awal . ':' . $nabjad . $nawal);
				}
				$nawal_awal = $nawal;
			}
			$sheet->setCellValue($nabjad++ . $nawal, $value1['klasifikasi']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $stockawal);
			$sheet->setCellValue($nabjad++ . $nawal, @$datin_new[$value1['id_barang']]);
			$sheet->setCellValue($nabjad++ . $nawal, @$datout_new[$value1['id_barang']]);
			$sheet->setCellValue($nabjad++ . $nawal, ($stockawal + ((@$datin_new[$value1['id_barang']] == "") ? 0 : @$datin_new[$value1['id_barang']]) - ((@$datout_new[$value1['id_barang']] == "") ? 0 : $datout_new[$value1['id_barang']])));
			$klasifikasi_chk = $value1['klasifikasi'];
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
		}
		if ($klasifikasi_chk != $value1['klasifikasi']) {
			if ($klasifikasi_chk != "") {
				$sheet->mergeCells($nabjad . $nawal_awal . ':' . $nabjad . $nawal);
			}
			$klasifikasi_awal = $value1['klasifikasi'];
		}
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN HARIAN " . (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4) . " .xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function monthly_factory_in()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Terima Barang Bulanan';
		$datacontent['factory_list'] = "";
		foreach ($this->Model->get_factory() as $key => $value) {
			$datacontent['factory_list'] = $datacontent['factory_list'] . '<option value="' . $value['id_factory'] . '">' . $value['nama_factory'] . '</option>';
		}
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_factory_in', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_monthly_factory_in()
	{

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$factory_id = $_POST['factory_id'];
		$tgl_query = "$tahun-$bulan";

		date_default_timezone_set("Asia/Jakarta");
		$tanggal = date("$bulan $tahun");

		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);

		$split = explode(' ', $tanggal);
		$datacontent['tanggal'] = $bulan[(int) $split[0]] . ' ' . $split[1];

		$datacontent['nama_supplier'] = $this->db->query("SELECT nama_factory FROM factory WHERE id_factory = '$factory_id' ")->result_array();
		$datacontent['data_terima_barang'] = $this->db->query("SELECT id_barang, C.nama_produk, nomor_surat_jalan, SUM(jumlah) as jumlah, tgl_terima FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po LEFT JOIN produk C ON C.id_produk = A.id_barang WHERE B.factory = '" . $factory_id . "' AND LEFT(A.tgl_terima, 7) = '" . $tgl_query . "' GROUP BY id_barang, tgl_terima ORDER BY tgl_terima")->result_array();
		$datacontent['cek_data'] = $this->db->query("SELECT id_barang, C.nama_produk, nomor_surat_jalan, SUM(jumlah) as jumlah, tgl_terima FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po LEFT JOIN produk C ON C.id_produk = A.id_barang WHERE B.factory = '" . $factory_id . "' AND LEFT(A.tgl_terima, 7) = '" . $tgl_query . "' GROUP BY id_barang, tgl_terima ORDER BY tgl_terima");
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Terima Barang Bulanan';
		$datacontent['file'] = 'Laporan Terima Barang Bulanan';
		$this->load->view('Laporan2/export_excel_terima_barang', $datacontent);
	}

	public function preview_monthly_factory_in()
	{

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$factory_id = $_POST['factory_id'];
		$tgl_query = "$tahun-$bulan";

		date_default_timezone_set("Asia/Jakarta");
		$tanggal = date("$bulan $tahun");

		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);

		$split = explode(' ', $tanggal);
		$datacontent['tanggal'] = $bulan[(int) $split[0]] . ' ' . $split[1];

		$datacontent['nama_supplier'] = $this->db->query("SELECT nama_factory FROM factory WHERE id_factory = '$factory_id' ")->result_array();
		$datacontent['data_terima_barang'] = $this->db->query("SELECT id_barang, C.nama_produk, nomor_surat_jalan, SUM(jumlah) as jumlah, tgl_terima FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po LEFT JOIN produk C ON C.id_produk = A.id_barang WHERE B.factory = '" . $factory_id . "' AND LEFT(A.tgl_terima, 7) = '" . $tgl_query . "' GROUP BY id_barang, tgl_terima ORDER BY tgl_terima")->result_array();
		$datacontent['cek_data'] = $this->db->query("SELECT id_barang, C.nama_produk, nomor_surat_jalan, SUM(jumlah) as jumlah, tgl_terima FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po LEFT JOIN produk C ON C.id_produk = A.id_barang WHERE B.factory = '" . $factory_id . "' AND LEFT(A.tgl_terima, 7) = '" . $tgl_query . "' GROUP BY id_barang, tgl_terima ORDER BY tgl_terima");
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Terima Barang Bulanan';
		$datacontent['file'] = 'Laporan Terima Barang Bulanan';
		$this->load->view('Laporan2/preview_terima_barang', $datacontent);
	}

	public function monthly_out()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Barang Keluar';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_out', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_monthly_out()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$product_id = $_POST['product_id'];
		$arrpackage_trial = $this->db->query("SELECT SUM(B.package_detail_quantity) AS jumlah, A.package_date, A.member_code, D.nama, C.nama_produk FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = B.product_id LEFT JOIN member D ON D.kode = A.member_code WHERE LEFT(A.package_date, 7) = '" . $tahun . "-" . $bulan . "' AND B.product_id = '$product_id' GROUP BY A.package_date, A.member_code ORDER BY A.package_date")->result_array();
		$tanggalx = array();
		$sellerx = array();
		$arrdata = array();
		foreach ($arrpackage_trial as $index => $value) {
			$tanggalx[$value['package_date']] = $value['package_date'];
			$sellerx[$value['member_code']] = $value['nama'];
			$arrdata[$value['member_code']][$value['package_date']] = $value['jumlah'];
			$barang = $value['nama_produk'];
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->setCellValue('A1', 'Laporan Bulanan');
		$sheet->setCellValue('A2', @$barang);
		$sheet->getStyle('A1:A2')->getFont()->setSize(20);
		$sheet->getStyle('A1:A2')->getFont()->setBold(true);
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$awal = 3;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NAMA');
		foreach (@$tanggalx as $index2 => $value2) {
			$sheet->setCellValue($abjad++ . $awal, (int)substr($value2, 8, 2) . "-" . substr($arrbln[$bulan], 0, 3) . "-" . substr($tahun, 2, 4));
			$total[$index2] = 0;
		}

		$nawal = $awal;
		foreach (@$sellerx as $index3 => $value3) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value3);
			foreach ($tanggalx as $index2 => $value2) {
				$sheet->setCellValue($nabjad++ . $nawal, @$arrdata[$index3][$index2]);
				$total[$index2] = $total[$index2] + ((@$arrdata[$index3][$index2] != "") ? @$arrdata[$index3][$index2] : 0);
			}
		}
		$nawal++;
		$nabjad = $abjad_awal;
		$sheet->setCellValue($nabjad++ . $nawal, 'Grand Total');
		foreach ($tanggalx as $index2 => $value2) {
			$abjad_akhir = $nabjad;
			$sheet->setCellValue($nabjad++ . $nawal, $total[$index2]);
		}
		$sheet->getStyle('A1:' . ((@$abjad_akhir == "") ? 'A' : $abjad_akhir) . '2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F4B084');
		$sheet->getStyle($abjad_awal . $awal . ':' . ((@$abjad_akhir == "") ? 'A' : $abjad_akhir) . $nawal)->applyFromArray($styleArray_thin);
		$writer = new Xlsx($spreadsheet);
		$sheet->getStyle($abjad_awal . $nawal . ':' . ((@$abjad_akhir == "") ? 'A' : $abjad_akhir) . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('D9D9D9');
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENGIRIMAN " . @$barang . " BULAN " . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function export_stock_monthly($bulan, $tahun)
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		//$sheet->getStyle('C4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f8cbad');
		$arrdata = $this->model->Query("SELECT *, C.jumlah, C.rusak FROM kemasan A LEFT JOIN produk B ON B.id_produk = A.produk LEFT JOIN tb_stock_kemasan_history C ON C.id_barang = A.id_kemasan AND C.tanggal = '" . $tahun . "-" . $bulan . "-01' LEFT JOIN m_category_product D ON D.category_product_id = B.produk_kategori ORDER BY B.produk_kategori, A.produk");
		$arrcategoryproduct = $this->model->Query("SELECT * FROM m_category_product");
		$arrterima = $this->model->Query("SELECT SUM(jumlah) as jumlah, SUM(rusak) as rusak, tgl_terima, id_barang FROM terima_kemasan  GROUP BY id_barang, tgl_terima");
		foreach ($arrterima as $keyterima => $valueterima) {
			$dataterima[$valueterima['id_barang']][$valueterima['tgl_terima']] = $valueterima;
		}
		$arrterima_print = $this->model->Query("SELECT SUM(jumlah) as jumlah, SUM(rusak) as rusak, tgl_terima, id_barang FROM terima_print  GROUP BY id_barang, tgl_terima");
		foreach ($arrterima_print as $keyterima_print => $valueterima_print) {
			$dataterima_print[$valueterima_print['id_barang']][$valueterima_print['tgl_terima']] = $valueterima_print;
		}
		$arrkeluar = $this->model->Query("SELECT SUM(jumlah) as jumlah, tanggal, id_barang FROM tb_detail_kluar_kemasan GROUP BY id_barang, tanggal");
		foreach ($arrkeluar as $keykeluar => $valuekeluar) {
			$datakeluar[$valuekeluar['id_barang']][$valuekeluar['tanggal']] = $valuekeluar;
		}

		$abjad = 'B';
		$awal = 5;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NAMA AKUN');
		$sheet->setCellValue($abjad++ . $awal, 'TANGGAL TRANSAKSI');
		$sheet->setCellValue($abjad++ . $awal, 'DEBIT');

		$nawal = $awal;

		$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		for ($i = 1; $i <= $nJumlahHari; $i++) {
			if ($i > 9) {
				$cNol = "";
			} else {
				$cNol = "0";
			}
			$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
			$no = 0;
			$titik_awal = $abjad;

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG DATANG');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG KELUAR');
			$sheet->getStyle($titik_awal . $awal . ':' . $abjad . $awal)->applyFromArray($styleArray);
			$sheet->mergeCells($titik_awal . $awal . ':' . $abjad . $awal);
			$sheet->setCellValue($titik_awal . $awal, substr($arrbln[$bulan], 0, 3) . '-' . (int)($cNol . $i));

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG RUSAK');
		}

		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG DATANG');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG KELUAR');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad . $awal, 'SISA STOCK KEMASAN BAGUS');

		$sheet->mergeCells($abjad_awal . ($awal - 3) . ':' . $abjad . ($awal - 3));
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$sheet->setCellValue($abjad_awal . ($awal - 3), 'LAPORAN STOK BAHAN KEMAS MS GLOW');
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setSize(20);
		$sheet->mergeCells($abjad_awal . ($awal - 2) . ':' . $abjad . ($awal - 2));
		$sheet->setCellValue($abjad_awal . ($awal - 2), 'PERIODE ' . strtoupper($arrbln[$bulan]));
		$sheet->setCellValue($abjad . ($awal - 1), date('d/m/Y H.i'));


		//$sheet->getStyle('A1:B2')->applyFromArray($styleArray);
		//$sheet ->getStyle($abjad_awal.$awal.':'.$abjad.($awal+1))->applyFromArray($styleArray);
		$sheet->freezePane('F7');
		$sheet->getSheetView()->setZoomScale(70);
		$sheet->getRowDimension($awal + 1)->setRowHeight(30);
		$irow = 1;
		$wrap_awal = "";
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal + 1))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = $wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}
		$sheet->getStyle($wrap_awal . '5:' . $wrap_akhir . '6')->getAlignment()->setWrapText(true);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$nawal++;
		$produk_global = "";
		$produk_merge_akhir = "";

		foreach ($arrdata as $key => $value) {
			$nabjad = 'B';
			$paling_awal = $nabjad;
			$nawal++;
			if ($produk_merge_awal == "") {
				$produk_merge_awal = $nawal;
			}

			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_kemasan']);
			//$xx = 1;
			if ($rgb == "") $rgb = 'e9f7df';
			if ($produk_global != $value['nama_produk']) {
				//$xx = 0;
				$xabjad = $nabjad;
				if ($produk_merge_akhir != "") {

					$sheet->mergeCells($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_akhir);
					$sheet->getStyle($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_awal)->applyFromArray($style);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
					//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
				}
				$produk_merge_awal = $nawal;
			}
			$produk_global = $value['nama_produk'];
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['jumlah']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['rusak']);
			$jumlah = $value['jumlah'];
			$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$jmldatang = 0;
			$jmlkeluar = 0;
			for ($i = 1; $i <= $nJumlahHari; $i++) {
				if ($i > 9) {
					$cNol = "";
				} else {
					$cNol = "0";
				}
				$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
				$no = 0;

				$sheet->setCellValue($nabjad++ . $nawal, ($dataterima[$value['id_kemasan']][$date]['jumlah'] != "" || $dataterima_print[$value['id_kemasan']][$date]['jumlah'] != "") ? ((($dataterima[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_kemasan']][$date]['jumlah']) + (($dataterima_print[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima_print[$value['id_kemasan']][$date]['jumlah'])) : "");
				$sheet->setCellValue($nabjad++ . $nawal, $datakeluar[$value['id_kemasan']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, $dataterima[$value['id_kemasan']][$date]['rusak']);
				$jmldatang = $jmldatang + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_kemasan']][$date]['jumlah']) + (($dataterima_print[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima_print[$value['id_kemasan']][$date]['jumlah']);
				$jmlkeluar = $jmlkeluar + (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $datakeluar[$value['id_kemasan']][$date]['jumlah']);
				//$jumlah = $jumlah + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima[$value['id_kemasan']][$date]['jumlah']) - (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']); 
			}
			$jumlah = $jumlah + $jmldatang - $jmlkeluar;
			$sheet->setCellValue($nabjad++ . $nawal, $jmldatang);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlkeluar);
			$sheet->setCellValue($nabjad . $nawal, $jumlah);
			$produk_merge_akhir = $nawal;
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
			$rgb = $value['category_product_rgb'];
			if ($rgb == "") $rgb = 'e9f7df';
			if ($jumlah < 20000) {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
			} else {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
			}
		}
		//if($xx == 1){
		$sheet->mergeCells($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_akhir);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray2);
		$sheet->getStyle($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_awal)->applyFromArray($style);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
		//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
		//}

		$nawal++;
		$nawal++;
		$sheet->setCellValue($abjad_awal . $nawal++, 'NB:');
		$abjad_awal2 = $abjad_awal;
		$sheet->getStyle($abjad_awal2 . $nawal . ':' . $abjad_awal2++ . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
		$sheet->setCellValue($abjad_awal2 . $nawal++, 'STOK DIBAWAH 20.000 PCS');
		foreach ($arrcategoryproduct as $keyproduct => $valueproduct) {
			$abjad_awal2 = $abjad_awal;
			$sheet->getStyle($abjad_awal2 . $nawal . ':' . $abjad_awal2++ . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($valueproduct['category_product_rgb']);
			$sheet->setCellValue($abjad_awal2 . $nawal++, $valueproduct['category_product_name']);
		}

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"STOCK KEMASAN MSGLOW " . strtoupper($arrbln[$bulan]) . ".xls\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function laporan_kas()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Kas';
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_laporan_kas', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function excel_laporan_kas()
	{
		ob_end_clean();

		$from = $this->String2Date($_POST['from']);
		$to = $this->String2Date($_POST['to']);
		$arr = $this->db->query("SELECT coa_transaction_header_id, A.coa_transaction_realization, B.coa_transaction_id, nama, coa_transaction_date, coa_debit FROM t_coa_transaction_header A JOIN t_coa_transaction B ON B.coa_transaction_source_id = A.coa_transaction_header_id AND B.coa_transaction_source = 4 AND B.coa_debit > 0 JOIN coa_4 C ON C.id = B.coa_id WHERE REPLACE(coa_transaction_date, '-', '') BETWEEN REPLACE('$from', '-', '') AND REPLACE('$to', '-', '')")->result_array();

		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "Periode : " . $from . " s/d " . $to . " - LAPORAN KAS");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA AKUN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'TGL TRANSAKSI');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'DEBIT');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'C';

		$total = 0;
		foreach ($arr as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;

			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['coa_transaction_date']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['coa_debit']);
			$total += $value1['coa_debit'];
		}
		$nawal++;
		$nabjad = $abjad_awal;
		$sheet->setCellValue($nabjad++ . $nawal, 'Total Debit');
		$sheet->setCellValue($xxabjad . $nawal, $total);

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN KAS.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function laporan_terima_produk()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Terima Produk';
		$data['file'] = 'Laporan';
		$datacontent['factories'] = $this->db->get('factory')->result_array();
		$data['content'] = $this->load->view($this->url_ . '/form_laporan_terima_produk', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function laporan_fifo()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan FIFO';
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_laporan_fifo', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function excel_laporan_terima_produk()
	{
		ob_end_clean();

		$from = $this->String2Date($_POST['from']);
		$to = $this->String2Date($_POST['to']);
		$heads = $this->db->query("SELECT nomor_surat_jalan, nama_factory, tgl_terima FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po LEFT JOIN factory C ON C.id_factory = B.factory WHERE B.factory = $_POST[factory_id] GROUP BY A.nomor_surat_jalan, B.factory ORDER BY tgl_terima DESC")->result_array();
		foreach ($heads as $index => $head) {
			$data[$index] = $head;
			$sj[$index] = $head['nomor_surat_jalan'];
		}
		$strsj = join("', '", $sj);
		$childs = $this->db->query("SELECT A.nomor_surat_jalan, A.id_barang, A.jumlah, A.tgl_terima, nama_produk, A.kode_po, A.id_terima_kemasan FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po LEFT JOIN produk C ON C.id_produk = A.id_barang WHERE B.factory = '$_POST[factory_id]' AND A.nomor_surat_jalan IN('$strsj') AND A.realisasi_date IS NULL")->result_array();
		foreach ($childs as $child) {
			$datachild[$child['nomor_surat_jalan']][$child['id_barang']][$child['id_terima_kemasan']] = $child;
			//$datay[$index][$child['id_barang']][$child['id_terima_kemasan']] = $child;
		}
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "Periode : " . $from . " s/d " . $to . " - LAPORAN KAS");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA AKUN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'TGL TRANSAKSI');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'DEBIT');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'C';

		$total = 0;
		foreach ($arr as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;

			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['coa_transaction_date']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['coa_debit']);
			$total += $value1['coa_debit'];
		}
		$nawal++;
		$nabjad = $abjad_awal;
		$sheet->setCellValue($nabjad++ . $nawal, 'Total Debit');
		$sheet->setCellValue($xxabjad . $nawal, $total);

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN KAS.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}
	public function excel_laporan_fifo()
	{
		ob_end_clean();
		$from = $this->String2Date($_POST['from']);
		$to = $this->String2Date($_POST['to']);
		$datas = $this->db->query("SELECT nama_produk, bridge_quantity, bridge_hpp, bridge_price, ((bridge_price - bridge_hpp)*bridge_quantity) as total FROM t_bridge A JOIN produk B ON B.id_produk = A.product_id JOIN terima_produk C ON C.id_terima_kemasan = A.id_terima_kemasan JOIN t_sales D ON D.sales_id = A.sales_id  JOIN t_package_trial E ON E.package_id = A.package_id WHERE REPLACE(E.package_date, '-', '') BETWEEN REPLACE('$from', '-', '') AND REPLACE('$to', '-', '')")->result_array();

		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "Periode : " . $from . " s/d " . $to . " - LAPORAN FIFO");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'HPP');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'HARGA JUAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'TOTAL KEUNTUNGSN');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'C';

		$total = 0;
		foreach ($datas as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;

			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['bridge_quantity']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['bridge_hpp']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['bridge_price']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['total']);
			$total += $value1['total'];
		}
		$nawal++;
		$nabjad = $abjad_awal;
		$sheet->setCellValue($nabjad++ . $nawal, 'Total Harga');
		$sheet->setCellValue($xxabjad . $nawal, $total);

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN FIFO.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}
	public function excel_laporan_pendingan()
	{
		ob_end_clean();
		$datas = $this->db->query("SELECT C.nama, C.kode, E.nama_produk, SUM(pending_quantity) as pending FROM t_sales A JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN member C ON C.kode = A.seller_id JOIN produk D ON D.id_produk = B.product_id JOIN produk_global E ON E.kode = D.kd_pd WHERE B.pending_quantity > 0 GROUP BY A.seller_id, D.kd_pd")->result_array();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "LAPORAN PENDINGAN");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'PENDING');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'D';

		$total = 0;
		foreach ($datas as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			if ($index1 > 0) {
				if ($value1['kode'] == $datas[$index1 - 1]['kode']) {
					$value1['kode'] = "";
					$value1['nama'] = "";
				}
			}
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['kode']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['pending']);
			$total += $value1['pending'];
		}
		$nawal++;
		$nabjad = $abjad_awal;
		$sheet->setCellValue($nabjad++ . $nawal, 'Total Pendingan');
		$sheet->setCellValue($xxabjad . $nawal, $total);

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENDINGAN.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function excel_laporan_laba_rugi()
	{
		ob_end_clean();
		$datas = $this->db->query("SELECT A.kode, A.nama, IFNULL(credit, 0) AS credit, IFNULL(debit, 0) AS debit FROM `coa_3` A LEFT JOIN (SELECT coa3_id, SUM(coa_total_credit + IFNULL(tot_credit5, 0)) as credit, SUM(coa_total_debit + IFNULL(tot_debit5, 0)) as debit FROM coa_4 JOIN t_coa_total ON t_coa_total.coa_id = coa_4.id AND t_coa_total.coa_level = 4 LEFT JOIN (SELECT coa4_id, SUM(coa_total_debit) as tot_debit5, SUM(coa_total_credit) as tot_credit5 FROM coa_5 JOIN t_coa_total ON t_coa_total.coa_id = coa_5.id AND t_coa_total.coa_level = 5 GROUP BY coa4_id) C ON C.coa4_id =coa_4.id  GROUP BY coa3_id) B ON B.coa3_id = A.id JOIN coa_2 C ON C.id = A.coa2_id WHERE A.`is_delete` = 0 AND C.coa1_id IN(1, 2, 3, 4, 5)")->result_array();
		//$datas = $this->db->query("SELECT A.kode, A.nama, IFNULL(credit, 0) AS credit, IFNULL(debit, 0) AS debit FROM `coa_3` A LEFT JOIN (SELECT coa3_id, CASE WHEN (SUM(coa_total_debit) + IFNULL(tot_debit5, 0)) < (SUM(coa_total_credit) + IFNULL(tot_credit5, 0)) THEN (SUM(coa_total_credit) + IFNULL(tot_credit5, 0)) - (SUM(coa_total_debit) + IFNULL(tot_debit5, 0)) ELSE 0 END credit, CASE WHEN (SUM(coa_total_debit) + IFNULL(tot_debit5, 0)) < (SUM(coa_total_credit) + IFNULL(tot_credit5, 0)) THEN 0 ELSE (SUM(coa_total_debit) + IFNULL(tot_debit5, 0)) - (SUM(coa_total_credit) + IFNULL(tot_credit5, 0)) END debit FROM coa_4 JOIN t_coa_total ON t_coa_total.coa_id = coa_4.id LEFT JOIN (SELECT coa4_id, SUM(coa_total_debit) as tot_debit5, SUM(coa_total_credit) as tot_credit5 FROM coa_5 JOIN t_coa_total ON t_coa_total.coa_id = coa_5.id GROUP BY coa4_id) C ON C.coa4_id =coa_4.id  GROUP BY coa3_id) B ON B.coa3_id = A.id JOIN coa_2 C ON C.id = A.coa2_id WHERE A.`is_delete` = 0 AND C.coa1_id IN(1, 2, 3, 4, 5)")->result_array();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "LAPORAN LABA RUGI");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KREDIT');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'DEBIT');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'D';
		$xxxabjad = 'C';

		$totalDebit = 0;
		$totalCredit = (float)0;
		foreach ($datas as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			$value1['debit'] = (float) $value1['debit'];
			$sheet->setCellValue($nabjad++ . $nawal, $value1['kode']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['credit']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['debit']);
			$totalDebit += (float)$value1['debit'];
			$totalCredit += (float)$value1['credit'];
		}
		$nawal++;
		$nabjad = $abjad_awal;
		$sheet->setCellValue($nabjad++ . $nawal, 'Total');
		$sheet->setCellValue($xxxabjad . $nawal, $totalCredit);
		$sheet->setCellValue($xxabjad . $nawal, $totalDebit);
		$sheet->setCellValue('E' . $nawal, round(($totalCredit - $totalDebit), 2) == -0 ? 0 : round(($totalCredit - $totalDebit), 2));

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN LABA RUGI.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function excel_list_pendingan()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$datas = $this->db->query("SELECT a.sales_id, d.kode, d.nama_produk, FORMAT(SUM(b.sales_detail_quantity), 0) as 'jumlah_pesanan', e.pembagian_qty as 'jumlah_pembagian', FORMAT((SUM(b.pending_quantity) + SUM(b.connected_quantity)), 0) as 'boleh_dikirim', FORMAT(SUM(b.pending_quantity), 0) as 'pending', FORMAT(SUM(b.connected_quantity), 0) as 'terkirim' FROM t_sales a JOIN t_sales_detail b ON b.sales_id = a.sales_id JOIN produk c ON c.id_produk = b.product_id JOIN produk_global d ON d.kode = c.kd_pd left join (SELECT FORMAT(SUM(package_detail_quantity), 0) as pembagian_qty, seller_id, kd_pd FROM t_package_trial_detail_draft JOIN produk ON produk.id_produk = t_package_trial_detail_draft.product_id GROUP BY kd_pd) e ON e.kd_pd = d.kode WHERE b.pending_quantity > 0 GROUP BY d.kode")->result_array();

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "LIST PENDINGAN");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH PESANAN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH PEMBAGIAN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BOLEH DIKIRIM');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PENDING');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'TERKIRIM');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'D';
		$xxxabjad = 'C';
		$i = 1;
		foreach ($datas as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			//$value1['debit'] = (float) $value1['debit'];
			$sheet->setCellValue($nabjad++ . $nawal, $i++);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['kode']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['jumlah_pesanan']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['jumlah_pembagian']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['boleh_dikirim']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['pending']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['terkirim']);
		}
		$nawal++;
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN LABA RUGI.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}
	
	public function excel_detail_list_pendingan_seller($seller_id)
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$datas = $this->db->query("SELECT a.seller_id, d.kode, d.nama_produk, SUM(b.sales_detail_quantity) as 'jumlah_pesanan', ifnull(SUM(e.package_detail_quantity), 0) as 'jumlah_pembagian', SUM(b.pending_quantity) + SUM(b.connected_quantity) as 'boleh_dikirim', SUM(b.pending_quantity) as 'pending', SUM(b.connected_quantity) as 'terkirim' FROM t_sales a JOIN t_sales_detail b ON b.sales_id = a.sales_id JOIN produk c ON c.id_produk = b.product_id JOIN produk_global d ON d.kode = c.kd_pd LEFT JOIN t_package_trial_detail_draft e ON e.seller_id = a.seller_id and e.product_id = c.id_produk WHERE b.pending_quantity > 0 AND a.seller_id = '$seller_id' GROUP BY d.kode, a.seller_id")->result_array();

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "LIST DETAIL PENDINGAN SELLER");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH PESANAN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH PEMBAGIAN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BOLEH DIKIRIM');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PENDING');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'TERKIRIM');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'D';
		$xxxabjad = 'C';
		$i = 1;
		foreach ($datas as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			//$value1['debit'] = (float) $value1['debit'];
			$sheet->setCellValue($nabjad++ . $nawal, $i++);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['kode']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['jumlah_pesanan']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['jumlah_pembagian']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['boleh_dikirim']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['pending']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['terkirim']);
		}
		$nawal++;
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN DETAIL LIST PENDINGAN SELLER.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function excel_factory_in_difference()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$tgl_awal = str_replace('-', '', $_POST['tgl_awal']);
		$tgl_akhir = str_replace('-', '', $_POST['tgl_akhir']);
		$factory_id = $_POST['factory_id'];
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$datas = $this->db->query("SELECT * FROM terima_produk A JOIN po_produk B ON B.kode_po = A.kode_po WHERE B.factory = '$factory_id' AND DATE_FORMAT(A.tgl_terima, '%Y%m%d') BETWEEN $tgl_awal AND $tgl_akhir")->result_array();

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "LIST PENERIMAAN BARANG");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SJ');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH DRAFT');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'JUMLAH REALISASI');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'D';
		$xxxabjad = 'C';
		$i = 1;
		foreach ($datas as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			//$value1['debit'] = (float) $value1['debit'];
			$sheet->setCellValue($nabjad++ . $nawal, $i++);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nomor_surat_jalan']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['jumlah_sj']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['jumlah']);
		}
		$nawal++;
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENERIMAAN BARANG.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function excel_detail_list_pendingan($kode)
	{

		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$datas = $this->db->query("SELECT A.id_member, B.seller_id, A.nama, FORMAT(SUM(C.sales_detail_quantity),0) as 'jumlah_pesanan', FORMAT(pembagian_qty,0) as 'jumlah_pembagian', FORMAT(SUM(C.pending_quantity) + SUM(C.connected_quantity),0) as 'boleh_dikirim', FORMAT(SUM(C.pending_quantity),0) as 'pending', FORMAT(SUM(C.connected_quantity),0) as 'terkirim' FROM member A JOIN t_sales B ON B.seller_id = A.kode JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd left join (SELECT ifnull(SUM(package_detail_quantity), 0) as pembagian_qty, seller_id, kd_pd FROM t_package_trial_detail_draft JOIN produk ON produk.id_produk = t_package_trial_detail_draft.product_id WHERE produk.kd_pd = '" . $kode . "' GROUP BY seller_id, kd_pd) F on F.seller_id = B.seller_id and F.kd_pd = E.kode WHERE C.pending_quantity > 0 AND E.kode = '" . $kode . "' GROUP BY E.kode, B.seller_id")->result_array();

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "LIST PENDINGAN");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH PESANAN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH PEMBAGIAN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BOLEH DIKIRIM');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PENDING');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'TERKIRIM');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'D';
		$xxxabjad = 'C';
		$i = 1;
		foreach ($datas as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			//$value1['debit'] = (float) $value1['debit'];
			$sheet->setCellValue($nabjad++ . $nawal, $i++);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['seller_id']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['jumlah_pesanan']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['jumlah_pembagian']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['boleh_dikirim']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['pending']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['terkirim']);
		}
		$nawal++;
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN LABA RUGI.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function excel_detail_account_list_pendingan($product, $kode)
	{

		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$datas = $this->db->query("SELECT B.sales_id, B.sales_code, B.sales_date, C.sales_detail_quantity as 'jumlah_pesanan', C.pending_quantity + C.connected_quantity as 'boleh_dikirim', C.pending_quantity as 'pending', C.connected_quantity as 'terkirim' FROM t_sales B JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id WHERE D.kd_pd = '$product' AND B.seller_id = '$kode' AND C.pending_quantity > '0'")->result_array();

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "LIST PENDINGAN");
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE PR');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'TANGGAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH PESANAN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BOLEH DIKIRIM');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PENDING');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'TERKIRIM');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'D';
		$xxxabjad = 'C';
		$i = 1;
		foreach ($datas as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			//$value1['debit'] = (float) $value1['debit'];
			$sheet->setCellValue($nabjad++ . $nawal, $i++);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['sales_code']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['sales_date']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['jumlah_pesanan']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['boleh_dikirim']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['pending']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['terkirim']);
		}
		$nawal++;
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN LABA RUGI.xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function laporan_laba_rugi()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Laba Rugi';
		$data['file'] = 'Laporan';
		$data['content'] = $this->load->view($this->url_ . '/form_laporan_laba_rugi', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	public function laporan_pendingan()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Rekap Pendingan PT. KOSMETIKA CANTIK INDONESIA';
		$data['file'] = 'Laporan Pendingan';
		$datacontent['tanggal'] = $this->DateStampIndo2();
		$datacontent['data_list_pending'] = $this->db->query("SELECT B.sales_id, F.kode AS kode_tempat, E.kode, F.team, F.status, E.nama_produk, D.product_type, FORMAT(SUM(C.sales_detail_quantity), 0) as Jumlah_Pesanan, FORMAT((SUM(C.pending_quantity) + SUM(C.connected_quantity)), 0) as Boleh_Dikirim, SUM(C.pending_quantity) as Pending, FORMAT(SUM(C.connected_quantity), 0) as Terkirim FROM t_sales B JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 AND D.product_type != 'MSGLOW FOR MEN' AND F.status != 'KLINIK' GROUP BY E.kode")->result_array();
		$datacontent['data_list_pending_formen'] = $this->db->query("SELECT B.sales_id, F.kode AS kode_tempat, E.kode, F.team, F.status, E.nama_produk, D.product_type, FORMAT(SUM(C.sales_detail_quantity), 0) as Jumlah_Pesanan, FORMAT((SUM(C.pending_quantity) + SUM(C.connected_quantity)), 0) as Boleh_Dikirim, SUM(C.pending_quantity) as Pending, FORMAT(SUM(C.connected_quantity), 0) as Terkirim FROM t_sales B JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 AND D.product_type = 'MSGLOW FOR MEN' GROUP BY E.kode")->result_array();
		$datacontent['data_list_pending_klinik'] = $this->db->query("SELECT B.sales_id, F.kode AS kode_tempat, E.kode, F.team, F.status, E.nama_produk, D.product_type, FORMAT(SUM(C.sales_detail_quantity), 0) as Jumlah_Pesanan, FORMAT((SUM(C.pending_quantity) + SUM(C.connected_quantity)), 0) as Boleh_Dikirim, SUM(C.pending_quantity) as Pending, FORMAT(SUM(C.connected_quantity), 0) as Terkirim FROM t_sales B JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 AND F.status = 'KLINIK' GROUP BY E.kode")->result_array();
		$data['content'] = $this->load->view($this->url_ . '/form_laporan_pendingan', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	public function get_data_list_pendingan()
	{
		$datacontent['datatable'] = $this->Model->get_data_list_pendingan();
	}
	public function export_data_pendingan($tanggal = "")
	{
		$datacontent['tanggal'] = $this->input->post("cTanggal");
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Rekap Pendingan PT. KOSMETIKA CANTIK INDONESIA';
		$datacontent['file'] = 'Laporan Pendingan';
		$datacontent['tanggal'] = $tanggal;
		$datacontent['data_list_pending'] = $this->db->query("SELECT B.sales_id, F.kode AS kode_tempat, E.kode, F.team, F.status, E.nama_produk, D.product_type, FORMAT(SUM(C.sales_detail_quantity), 0) as Jumlah_Pesanan, FORMAT((SUM(C.pending_quantity) + SUM(C.connected_quantity)), 0) as Boleh_Dikirim, SUM(C.pending_quantity) as Pending, FORMAT(SUM(C.connected_quantity), 0) as Terkirim FROM t_sales B JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 AND D.product_type != 'MSGLOW FOR MEN' AND F.status != 'KLINIK' GROUP BY E.kode")->result_array();
		$datacontent['data_list_pending_formen'] = $this->db->query("SELECT B.sales_id, F.kode AS kode_tempat, E.kode, F.team, F.status, E.nama_produk, D.product_type, FORMAT(SUM(C.sales_detail_quantity), 0) as Jumlah_Pesanan, FORMAT((SUM(C.pending_quantity) + SUM(C.connected_quantity)), 0) as Boleh_Dikirim, SUM(C.pending_quantity) as Pending, FORMAT(SUM(C.connected_quantity), 0) as Terkirim FROM t_sales B JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 AND D.product_type = 'MSGLOW FOR MEN' GROUP BY E.kode")->result_array();
		$datacontent['data_list_pending_klinik'] = $this->db->query("SELECT B.sales_id, F.kode AS kode_tempat, E.kode, F.team, F.status, E.nama_produk, D.product_type, FORMAT(SUM(C.sales_detail_quantity), 0) as Jumlah_Pesanan, FORMAT((SUM(C.pending_quantity) + SUM(C.connected_quantity)), 0) as Boleh_Dikirim, SUM(C.pending_quantity) as Pending, FORMAT(SUM(C.connected_quantity), 0) as Terkirim FROM t_sales B JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 AND F.status = 'KLINIK' GROUP BY E.kode")->result_array();
		$this->load->view('Laporan2/export_excel_pendingan', $datacontent);
	}

	public function laporan_pendingan_per_seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Rekap Pendingan Seller PT. KOSMETIKA CANTIK INDONESIA';
		$data['file'] = 'Laporan Pendingan Seller';
		$datacontent['tanggal'] = $this->DateStampIndo2();
		$datacontent['data_detail_product'] = $this->db->query("SELECT	B.sales_id,	F.kode AS kode_tempat,	E.kode,	F.nama,	F.team,	F.STATUS,	E.nama_produk,	D.product_type,	FORMAT( SUM( C.sales_detail_quantity ), 0 ) AS Jumlah_Pesanan, FORMAT(( SUM( C.pending_quantity ) + SUM( C.connected_quantity )), 0 ) AS Boleh_Dikirim,	SUM( C.pending_quantity ) AS Pending, FORMAT( SUM( C.connected_quantity ), 0 ) AS Terkirim FROM	t_sales B	JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 GROUP BY	E.Kode")->result_array();

		$datacontent['data_seller'] = $this->db->query("SELECT	B.sales_id,	F.kode AS kode_tempat,	E.kode,	F.nama,	F.team,	F.STATUS,	E.nama_produk,	D.product_type,	SUM( C.sales_detail_quantity ) AS Jumlah_Pesanan, ( SUM( C.pending_quantity ) + SUM( C.connected_quantity )) AS Boleh_Dikirim,	SUM( C.pending_quantity ) AS Pending,  SUM( C.connected_quantity ) AS Terkirim FROM	t_sales B	JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 GROUP BY	C.sales_id")->result_array();

		$datacontent['header_seller'] = $this->db->query("SELECT F.nama FROM t_sales B JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 GROUP BY F.nama")->result_array();
		$data['content'] = $this->load->view($this->url_ . '/form_pending_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function export_data_pendingan_seller($sales_id = "")
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Rekap Pendingan Seller PT. KOSMETIKA CANTIK INDONESIA';
		$data['file'] = 'Laporan Pendingan Seller';
		$datacontent['tanggal'] = $this->DateStampIndo2();
		$datacontent['data_detail_product'] = $this->db->query("SELECT	B.sales_id,	F.kode AS kode_tempat,	E.kode,	F.nama,	F.team,	F.STATUS,	E.nama_produk,	D.product_type,	SUM( C.sales_detail_quantity ) AS Jumlah_Pesanan,  SUM( C.pending_quantity ) + SUM( C.connected_quantity ) AS Boleh_Dikirim,	SUM( C.pending_quantity ) AS Pending, SUM( C.connected_quantity ) AS Terkirim FROM	t_sales B	JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 AND C.sales_id = '" . $sales_id . "' GROUP BY E.Kode")->result_array();
		$datacontent['data_seller'] = $this->db->query("SELECT	B.sales_id,	F.kode AS kode_tempat,	E.kode,	F.nama,	F.team,	F.STATUS,	E.nama_produk,	D.product_type,	SUM( C.sales_detail_quantity ) AS Jumlah_Pesanan, ( SUM( C.pending_quantity ) + SUM( C.connected_quantity )) AS Boleh_Dikirim,	SUM( C.pending_quantity ) AS Pending, SUM( C.connected_quantity ) AS Terkirim FROM	t_sales B	JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 GROUP BY C.sales_id")->result_array();
		$datacontent['header_seller'] = $this->db->query("SELECT F.nama FROM t_sales B JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd JOIN member F ON F.kode = B.seller_id WHERE C.pending_quantity > 0 AND C.sales_id = '" . $sales_id . "' GROUP BY F.nama")->result_array();

		$this->load->view('Laporan2/export_pending_seller', $datacontent);
	}
}
