<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<div class="row">

	<div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet kt-portlet--collapsed" data-ktportlet="true" id="kt_portlet_tools_4">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Filter Bulan
												</h3>
											</div>
											<div class="kt-portlet__head-toolbar">
												<div class="kt-portlet__head-group">
													<a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-brand btn-icon-md"><i class="la la-angle-down"></i></a>
												</div>
											</div>
										</div>
										<div class="kt-portlet__body">
											<form id="main" class="form-horizontal" action="<?= site_url($url . '/chart'); ?>" method="Post">
          <div class="form-group">
            <label>Bulan</label>
            <select name="cBulan" id="pilihBulan" ng-model="cBulan" class="form-control">
				<option></option>
				<option value="01" <?= ($bulan=='01')?'selected':''; ?>>Januari</option>
				<option value="02" <?= ($bulan=='02')?'selected':''; ?>>Februari</option>
				<option value="03" <?= ($bulan=='03')?'selected':''; ?>>Maret</option>
				<option value="04" <?= ($bulan=='04')?'selected':''; ?>>April</option>
				<option value="05" <?= ($bulan=='05')?'selected':''; ?>>Mei</option>
				<option value="06" <?= ($bulan=='06')?'selected':''; ?>>Juni</option>
				<option value="07" <?= ($bulan=='07')?'selected':''; ?>>Juli</option>
				<option value="08" <?= ($bulan=='08')?'selected':''; ?>>Agustus</option>
				<option value="09" <?= ($bulan=='09')?'selected':''; ?>>September</option>
				<option value="10" <?= ($bulan=='10')?'selected':''; ?>>Oktober</option>
				<option value="11" <?= ($bulan=='11')?'selected':''; ?>>November</option>
				<option value="12" <?= ($bulan=='12')?'selected':''; ?>>Desember</option>
			</select> 
          </div>
		  <div class="form-group">
            <label>Tahun</label>
            <select name="cTahun" id="pilihTahun" class="form-control">
				<option></option>
				<option value="2019" <?= ($tahun=='2019')?'selected':''; ?>>2019</option>
				<option value="2020" <?= ($tahun=='2020')?'selected':''; ?>>2020</option>
				<option value="2021" <?= ($tahun=='2021')?'selected':''; ?>>2021</option>
			</select> 
          </div>
		  <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                <i class="flaticon2-search"></i><span class="m-l-10">Search</span>
            </div>
          </div>
        </form>
										</div>
									</div>
	

  </div>
	
	<div class="col-xl-12">

									<!--begin:: Widgets/Activity-->
									<div class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--skin-solid kt-portlet--height-fluid">
										<div class="kt-portlet__head kt-portlet__head--noborder kt-portlet__space-x">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Penjualan <?= $ind_tanggal ?>
												</h3>
											</div> 
										</div>
										<div class="kt-portlet__body kt-portlet__body--fit">
											<div class="kt-widget17">
												<div class="kt-widget17__visual kt-widget17__visual--chart kt-portlet-fit--top kt-portlet-fit--sides" style="background-color: #fd397a">
													<div class="kt-widget17__chart" style="height:150px;">
														<!--<canvas id="kt_chart_activities"></canvas>-->
													</div>
												</div>
												<div class="kt-widget17__stats">
													<div class="kt-widget17__items">
														<a href="<?= base_url() ?>T_package_trial2/list_pendingan" class="kt-widget17__item">
															<span class="kt-widget17__icon">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000" />
																		<rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1" />
																	</g>
																</svg> </span>
															<span class="kt-widget17__subtitle">
																Pendingan Sales Order
															</span>
															<span class="kt-widget17__desc">
																<?= number_format($data_pendingan['sum']) ?>
															</span>
														</a>
														
														<a href="<?= base_url() ?>T_package_trial2/table_produk" class="kt-widget17__item">
															<span class="kt-widget17__icon">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M12.7037037,14 L15.6666667,10 L13.4444444,10 L13.4444444,6 L9,12 L11.2222222,12 L11.2222222,14 L6,14 C5.44771525,14 5,13.5522847 5,13 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,13 C19,13.5522847 18.5522847,14 18,14 L12.7037037,14 Z" fill="#000000" opacity="0.3" />
																		<path d="M9.80428954,10.9142091 L9,12 L11.2222222,12 L11.2222222,16 L15.6666667,10 L15.4615385,10 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9.80428954,10.9142091 Z" fill="#000000" />
																	</g>
																</svg> </span>
															<span class="kt-widget17__subtitle">
																Pembagian
															</span>
															<span class="kt-widget17__desc">
																<?= number_format($data_pembagian['sum']) ?> 
															</span>
														</a>
														<a href="<?= base_url() ?>Po_produk" class="kt-widget17__item">
															<span class="kt-widget17__icon">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--send">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M12.7037037,14 L15.6666667,10 L13.4444444,10 L13.4444444,6 L9,12 L11.2222222,12 L11.2222222,14 L6,14 C5.44771525,14 5,13.5522847 5,13 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,13 C19,13.5522847 18.5522847,14 18,14 L12.7037037,14 Z" fill="#000000" opacity="0.3" />
																		<path d="M9.80428954,10.9142091 L9,12 L11.2222222,12 L11.2222222,16 L15.6666667,10 L15.4615385,10 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9.80428954,10.9142091 Z" fill="#000000" />
																	</g>
																</svg> </span>
															<span class="kt-widget17__subtitle">
																PO Belum Selesai
															</span>
															<span class="kt-widget17__desc">
																<?= number_format($po_belumselesai['sum']) ?>
															</span>
														</a>
													</div>
													
													<div class="kt-widget17__items">
                            <a href="<?= base_url() ?>Po_produk" class="kt-widget17__item">
															<span class="kt-widget17__icon">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
																		<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
																		
																	</g>
																</svg> </span>
															<span class="kt-widget17__subtitle">
																Barang Masuk Hari Ini
															</span>
															<span class="kt-widget17__desc">
																<?= ($barang_hari_ini['jumlah'] == null)? 0 : number_format($barang_hari_ini['jumlah'])?>
															</span>
														</a>
														<a href="<?= base_url() ?>T_package_trial2" class="kt-widget17__item">
															<span class="kt-widget17__icon">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
																		<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
																	</g>
																</svg> </span>
															<span class="kt-widget17__subtitle">
																Barang Keluar Hari Ini
															</span>
															<span class="kt-widget17__desc">
																<?= number_format($barang_keluar_ini['sum']) ?>
															</span>
														</a>
														<a href="<?= base_url() ?>Administrator/Stock_Produk/stock_opname_produk" class="kt-widget17__item">
															<span class="kt-widget17__icon">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--error">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
																		
																		<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
																		<polygon points="0 0 24 0 24 24 0 24" />
																	</g>
																</svg> </span>
															<span class="kt-widget17__subtitle">
                                Stok Barang Menipis
															</span>
															<span class="kt-widget17__desc">
																<?= $stok_barang_menipis['sum'] ?>
															</span>
														</a>
													</div>
													<br>
													<br>
													<br>
												</div>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Activity-->
								</div>
								
	
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Axis Pemesanan & Pengiriman Barang <?= $ind_tanggal ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/print_daily_out'); ?>" method="Post" target="_blank">
          <div class="form-group">
            <div class="kt-portlet">
										<div class="kt-portlet__body">
											<div id="axisinout" style="height: 300px;"></div>
										</div>
									</div>
          </div>
        </form>
      </div>

    </div>
  </div>
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Pendingan
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
          <div class="form-group">
            <div class="kt-portlet">
										<div class="kt-portlet__body">
											<div id="chart_pendingan" style="height: 300px;"></div>
										</div>
									</div>
          </div>
      </div>

    </div>
  </div>
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Barang Masuk
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
          <div class="form-group">
            <div class="kt-portlet">
										<div class="kt-portlet__body">
											<div id="chart_supp_masuk" style="height: 300px;"></div>
										</div>
									</div>
          </div>
      </div>

    </div>
  </div>
  
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Top 10 Barang Telaris <?= $ind_tanggal ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/print_daily_out'); ?>" method="Post" target="_blank">
          <div class="form-group">
            <div class="kt-portlet">
										<div class="kt-portlet__body">
											<div id="toptenout" style="height: 300px;"></div>
										</div>
									</div>
          </div>
        </form>
      </div>

    </div>
  </div>
  
  <div class="col-lg-6 col-xl-6" style="display:none;">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Kiriman Pabrik <?= $ind_tanggal ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/print_daily_out'); ?>" method="Post" target="_blank">
          <div class="form-group">
            <div class="kt-portlet">
										<div class="kt-portlet__body">
											<div id="infactory" style="height: 300px;"></div>
										</div>
									</div>
          </div>
        </form>
      </div>

    </div>
  </div>
  
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Kiriman Pabrik <?= $ind_tanggal ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/print_daily_out'); ?>" method="Post" target="_blank">
          <div class="form-group">
            <div class="kt-portlet">
										<div class="kt-portlet__body">
											<div id="charttest" style="height: 300px;"></div>
										</div>
									</div> 
          </div>
        </form>
      </div>

    </div>
  </div>


</div>
<script>

    var options = {
          series: [
            <?php foreach($arrdat_barang_masuk as $index => $data){ ?>
            {
              name: "<?= $data['name'] ?>",
              data: <?= $data['data'] ?>
            },
          <?php } ?>
        ],
          chart: {
          height: 350,
          type: 'line',
          zoom: {
            enabled: true
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: [5, 7, 5],
          curve: 'straight',
          dashArray: [0, 8, 5]
        },
        title: {
          text: 'Page Statistics',
          align: 'left'
        },
        legend: {
          tooltipHoverFormatter: function(val, opts) {
            return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
          }
        },
        markers: {
          size: 0,
          hover: {
            sizeOffset: 6
          }
        },
        xaxis: {
          type: 'datetime',
          categories: <?= $tgl_barang_masuk ?>,
        },
        tooltip: {
          y: [
            {
              title: {
                formatter: function (val) {
                  return val;
                }
              }
            }
          ]
        },
        grid: {
          borderColor: '#f1f1f1',
        }
        };

        var chart = new ApexCharts(document.querySelector("#chart_supp_masuk"), options);
        chart.render();

		var options = {
          series: [{
          name: 'Pemesanan Barang',
          data: [<?= ((@$arrdat_masuk['01'] == "")?0:$arrdat_masuk['01']) ?>, <?= ((@$arrdat_masuk['02'] == "")?0:$arrdat_masuk['02']) ?>, <?= ((@$arrdat_masuk['03'] == "")?0:$arrdat_masuk['03']) ?>, <?= ((@$arrdat_masuk['04'] == "")?0:$arrdat_masuk['04']) ?>, <?= ((@$arrdat_masuk['05'] == "")?0:$arrdat_masuk['05']) ?>, <?= ((@$arrdat_masuk['06'] == "")?0:$arrdat_masuk['06']) ?>, <?= ((@$arrdat_masuk['07'] == "")?0:$arrdat_masuk['07']) ?>, <?= ((@$arrdat_masuk['08'] == "")?0:$arrdat_masuk['08']) ?>, <?= ((@$arrdat_masuk['09'] == "")?0:$arrdat_masuk['09']) ?>, <?= ((@$arrdat_masuk['10'] == "")?0:$arrdat_masuk['10']) ?>, <?= ((@$arrdat_masuk['11'] == "")?0:$arrdat_masuk['11']) ?>, <?= ((@$arrdat_masuk['12'] == "")?0:$arrdat_masuk['12']) ?>, <?= ((@$arrdat_masuk['13'] == "")?0:$arrdat_masuk['13']) ?>, <?= ((@$arrdat_masuk['14'] == "")?0:$arrdat_masuk['14']) ?>, <?= ((@$arrdat_masuk['15'] == "")?0:$arrdat_masuk['15']) ?>, <?= ((@$arrdat_masuk['16'] == "")?0:$arrdat_masuk['16']) ?>, <?= ((@$arrdat_masuk['17'] == "")?0:$arrdat_masuk['17']) ?>, <?= ((@$arrdat_masuk['18'] == "")?0:$arrdat_masuk['18']) ?>, <?= ((@$arrdat_masuk['19'] == "")?0:$arrdat_masuk['19']) ?>, <?= ((@$arrdat_masuk['20'] == "")?0:$arrdat_masuk['20']) ?>, <?= ((@$arrdat_masuk['21'] == "")?0:$arrdat_masuk['21']) ?>, <?= ((@$arrdat_masuk['22'] == "")?0:$arrdat_masuk['22']) ?>, <?= ((@$arrdat_masuk['23'] == "")?0:$arrdat_masuk['23']) ?>, <?= ((@$arrdat_masuk['24'] == "")?0:$arrdat_masuk['24']) ?>, <?= ((@$arrdat_masuk['25'] == "")?0:$arrdat_masuk['25']) ?>, <?= ((@$arrdat_masuk['26'] == "")?0:$arrdat_masuk['26']) ?>, <?= ((@$arrdat_masuk['27'] == "")?0:$arrdat_masuk['27']) ?>, <?= ((@$arrdat_masuk['28'] == "")?0:$arrdat_masuk['28']) ?>, <?= ((@$arrdat_masuk['29'] == "")?0:$arrdat_masuk['29']) ?>, <?= ((@$arrdat_masuk['30'] == "")?0:$arrdat_masuk['30']) ?>]
        }, {
          name: 'Pengiriman Barang',
          data: [<?= ((@$arrdat_keluar['01'] == "")?0:$arrdat_keluar['01']) ?>, <?= ((@$arrdat_keluar['02'] == "")?0:$arrdat_keluar['02']) ?>, <?= ((@$arrdat_keluar['03'] == "")?0:$arrdat_keluar['03']) ?>, <?= ((@$arrdat_keluar['04'] == "")?0:$arrdat_keluar['04']) ?>, <?= ((@$arrdat_keluar['05'] == "")?0:$arrdat_keluar['05']) ?>, <?= ((@$arrdat_keluar['06'] == "")?0:$arrdat_keluar['06']) ?>, <?= ((@$arrdat_keluar['07'] == "")?0:$arrdat_keluar['07']) ?>, <?= ((@$arrdat_keluar['08'] == "")?0:$arrdat_keluar['08']) ?>, <?= ((@$arrdat_keluar['09'] == "")?0:$arrdat_keluar['09']) ?>, <?= ((@$arrdat_keluar['10'] == "")?0:$arrdat_keluar['10']) ?>, <?= ((@$arrdat_keluar['11'] == "")?0:$arrdat_keluar['11']) ?>, <?= ((@$arrdat_keluar['12'] == "")?0:$arrdat_keluar['12']) ?>, <?= ((@$arrdat_keluar['13'] == "")?0:$arrdat_keluar['13']) ?>, <?= ((@$arrdat_keluar['14'] == "")?0:$arrdat_keluar['14']) ?>, <?= ((@$arrdat_keluar['15'] == "")?0:$arrdat_keluar['15']) ?>, <?= ((@$arrdat_keluar['16'] == "")?0:$arrdat_keluar['16']) ?>, <?= ((@$arrdat_keluar['17'] == "")?0:$arrdat_keluar['17']) ?>, <?= ((@$arrdat_keluar['18'] == "")?0:$arrdat_keluar['18']) ?>, <?= ((@$arrdat_keluar['19'] == "")?0:$arrdat_keluar['19']) ?>, <?= ((@$arrdat_keluar['20'] == "")?0:$arrdat_keluar['20']) ?>, <?= ((@$arrdat_keluar['21'] == "")?0:$arrdat_keluar['21']) ?>, <?= ((@$arrdat_keluar['22'] == "")?0:$arrdat_keluar['22']) ?>, <?= ((@$arrdat_keluar['23'] == "")?0:$arrdat_keluar['23']) ?>, <?= ((@$arrdat_keluar['24'] == "")?0:$arrdat_keluar['24']) ?>, <?= ((@$arrdat_keluar['25'] == "")?0:$arrdat_keluar['25']) ?>, <?= ((@$arrdat_keluar['26'] == "")?0:$arrdat_keluar['26']) ?>, <?= ((@$arrdat_keluar['27'] == "")?0:$arrdat_keluar['27']) ?>, <?= ((@$arrdat_keluar['28'] == "")?0:$arrdat_keluar['28']) ?>, <?= ((@$arrdat_keluar['29'] == "")?0:$arrdat_keluar['29']) ?>, <?= ((@$arrdat_keluar['30'] == "")?0:$arrdat_keluar['30']) ?>]
        }],
          chart: {
          height: 350,
          type: 'area'
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'smooth'
        },
        xaxis: {
          type: 'datetime',
          categories: ["2020-<?= $bulan ?>-01", "2020-<?= $bulan ?>-02", "2020-<?= $bulan ?>-03", "2020-<?= $bulan ?>-04", "2020-<?= $bulan ?>-05", "2020-<?= $bulan ?>-06", "2020-<?= $bulan ?>-07", "2020-<?= $bulan ?>-08", "2020-<?= $bulan ?>-09", "2020-<?= $bulan ?>-10", "2020-<?= $bulan ?>-11", "2020-<?= $bulan ?>-12", "2020-<?= $bulan ?>-13", "2020-<?= $bulan ?>-14", "2020-<?= $bulan ?>-15", "2020-<?= $bulan ?>-16", "2020-<?= $bulan ?>-17", "2020-<?= $bulan ?>-18", "2020-<?= $bulan ?>-19", "2020-<?= $bulan ?>-20", "2020-<?= $bulan ?>-21", "2020-<?= $bulan ?>-22", "2020-<?= $bulan ?>-23", "2020-<?= $bulan ?>-24", "2020-<?= $bulan ?>-25", "2020-<?= $bulan ?>-26", "2020-<?= $bulan ?>-27", "2020-<?= $bulan ?>-28", "2020-<?= $bulan ?>-29", "2020-<?= $bulan ?>-30"]
        },
        tooltip: {
          x: {
            format: 'dd/MM/yy HH:mm'
          },
        },
        };

        var chart = new ApexCharts(document.querySelector("#axisinout"), options);
        chart.render();
		
        var options = {
          series: [{
          name: 'Pendingan',
          data: [<?= $arrdat_pending ?>]
        }],
          chart: {
          type: 'area',
          stacked: false,
          height: 350,
          zoom: {
            type: 'x',
            enabled: true,
            autoScaleYaxis: true
          },
          toolbar: {
            autoSelected: 'zoom'
          }
        },
        dataLabels: {
          enabled: false
        },
        markers: {
          size: 0,
        },
        title: {
          text: 'Data Pendingan',
          align: 'left'
        },
        fill: {
          type: 'gradient',
          gradient: {
            shadeIntensity: 1,
            inverseColors: false,
            opacityFrom: 0.5,
            opacityTo: 0,
            stops: [0, 90, 100]
          },
        },
        yaxis: {
          title: {
            text: 'Jumlah'
          },
        },
        xaxis: {
          type: 'datetime',
          categories: [<?= $arrdat_pending_date ?>]
        },
        tooltip: {
          shared: false,
        }
        };

      var chart = new ApexCharts(document.querySelector("#chart_pendingan"), options);
      chart.render();
		
		 var options = {
          series: [{
          name: 'Quantitas',
          data: [<?php if(@$arrout_topten != ""){
			  $i= 0;
				foreach(@$arrout_topten as $index => $value){ 
				$i++; ?>
				<?= $value['jum'] ?><?= ($i==10)?"":"," ?>
			<?php }
			}			?>]
        }],
        chart: {
          height: 350,
          type: 'bar',
        },
        plotOptions: {
          bar: {
            columnWidth: '50%',
            endingShape: 'rounded'  
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: 2
        },
        
        grid: {
          row: {
            colors: ['#fff', '#f2f2f2']
          }
        },
        xaxis: {
          labels: {
            rotate: -45
          },
          categories: [<?php if(@$arrout_topten != ""){
				$i = 0;
				foreach(@$arrout_topten as $index => $value){ 
				$i++; ?>'<?= $value["nama_produk"] ?>'<?= ($i==10)?"":"," ?><?php }
			}?>],
          tickPlacement: 'on'
        },
        yaxis: {
          title: {
            text: 'Quantitas',
          },
        },
        fill: {
          type: 'gradient',
          gradient: {
            shade: 'light',
            type: "horizontal",
            shadeIntensity: 0.25,
            gradientToColors: undefined,
            inverseColors: true,
            opacityFrom: 0.85,
            opacityTo: 0.85,
            stops: [50, 0, 100]
          },
        }
        };

        var chart2 = new ApexCharts(document.querySelector("#toptenout"), options);
        chart2.render();
		
		
		
		

		
		
		
		 var options = {
          series: [<?php if(@$arrin_factory != ""){
				foreach(@$arrin_factory as $index => $value){ ?><?= $value["jum"] ?>, <?php }
			}?>],
          chart: {
          type: 'polarArea',
        },
        stroke: {
          colors: ['#fff']
        },
        fill: {
          opacity: 0.8
        },
        labels: [<?php if(@$arrin_factory != ""){
				foreach(@$arrin_factory as $index => $value){ ?>'<?= $value["nama_factory"] ?> (<?= $value["jum"] ?>)', <?php }
			}?>],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 600
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart3 = new ApexCharts(document.querySelector("#infactory"), options);
        chart3.render();
		
		
		
		
		var options = {
          series: [{
          data: [44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21, 44, 55, 41, 64, 22, 43, 21]
        }, {
          data: [53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32, 53, 32, 33, 52, 13, 44, 32]
        }],
          chart: {
          type: 'bar',
          height: 8000
        },
        plotOptions: {
          bar: {
            horizontal: true,
            dataLabels: {
              position: 'top', 
            }, 
          }
        },
        dataLabels: {
          enabled: true,
          offsetX: -6,
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        xaxis: {
          categories: [2001, 2002, 2003, 2004, 2005, 2006, 2007],
        },
        };

        var chart4 = new ApexCharts(document.querySelector("#charttest"), options);
        chart4.render();
</script>
