<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
            <h3 class="kt-portlet__head-title">
                <?= $title ?>
            </h3>
        </div>
        <!--<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<a href="<?= site_url() . $url . "/form/tambah" ?>" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Tambah
												</a>
											</div>
										</div>
									</div>-->
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-12 text-warning text-center">
                <!-- <h2> <?= strtoupper($tanggal) ?> </h2> -->
            </div>
            <!-- <div class="col-4">
                <form id="main" class="form-horizontal" action="<?= site_url($url . '/' . 'export_data_pendingan/' . $tanggal) ?>" method="Post" target="_blank">

                    <div class="kt-form__actions pull-right">
                        <button type="submit" class="btn btn-primary waves-effect waves-light btn-sm" name="pend" value="pend" title="Cetak Laporan Pendingan">
                            <i class="flaticon2-print"></i>&nbsp;&nbsp;<span class="m-l-10">Cetak Laporan Pendingan</span>
                        </button>
                    </div>

                </form>
            </div> -->
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <table class="table table-striped table-bordered text-center" id='DataTable_list_pendingan'>
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Kode Seller</td>
                                <td>Nama Seller</td>
                                <td>Nama Produk</td>
                                <td>Boleh Kirim</td>
                                <td>Realisasi/Kirim</td>
                                <td>Sisa Pendingan</td>
                                <td>Cetak</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0;
                            foreach ($data_seller as $vaData) {
                            ?>
                                <tr>
                                    <td><?= ++$no ?></td>
                                    <td> <?= $vaData['kode_tempat'] ?> </td>
                                    <td> <?= $vaData['nama'] ?> </td>
                                    <td> <?= $vaData['nama_produk'] ?> </td>
                                    <td> <?= number_format($vaData['Boleh_Dikirim'], 0, ',', ',') ?> </td>
                                    <td> <?= number_format($vaData['Terkirim'], 0, ',', ',') ?> </td>
                                    <td>
                                        <?php
                                        $total_pending = $vaData['Boleh_Dikirim'] - $vaData['Terkirim'];
                                        echo number_format($total_pending, 0, ',', ',');
                                        ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-outline-primary btn-elevate btn-icon" title="View Data" href="<?= site_url('Laporan2/export_data_pendingan_seller/' . $vaData['sales_id'] . '') ?>">
                                            <i class="la la-print"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>


        <!-- =======================================DATA TABLES SCRIPT=========================================================== -->
        <script src="<?= base_url() ?>assets2/plugins/general/jquery/dist/jquery.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/js/global/integration/plugins/datatables.init.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-autofill/js/dataTables.autoFill.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-buttons/js/buttons.colVis.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-buttons/js/buttons.flash.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-buttons/js/buttons.html5.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-buttons/js/buttons.print.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-colreorder/js/dataTables.colReorder.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-keytable/js/dataTables.keyTable.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-rowgroup/js/dataTables.rowGroup.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-rowreorder/js/dataTables.rowReorder.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-scroller/js/dataTables.scroller.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets2/plugins/custom/datatables.net-select/js/dataTables.select.min.js" type="text/javascript"></script>
        <!-- =======================================END DATA TABLES SCRIPT=========================================================== -->

        <script type="text/javascript">
            $("#DataTable_list_pendingan").dataTable({
                scrollY: '50vh',
                scrollX: 'true',
                scrollCollapse: true,
                "oLanguage": {
                    "sLengthMenu": "Tampilkan _MENU_ data per halaman",
                    "sSearch": "Pencarian: ",
                    "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
                    "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
                    "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
                    "sInfoFiltered": "(di filter dari _MAX_ total data)",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sLast": "Akhir",
                        "sPrevious": "Sebelumnya",
                        "sNext": "Selanjutnya"
                    }
                }
            });

            $("#DataTable_list_pendingan_formen").dataTable({
                scrollY: '50vh',
                scrollX: 'true',
                scrollCollapse: true,
                "oLanguage": {
                    "sLengthMenu": "Tampilkan _MENU_ data per halaman",
                    "sSearch": "Pencarian: ",
                    "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
                    "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
                    "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
                    "sInfoFiltered": "(di filter dari _MAX_ total data)",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sLast": "Akhir",
                        "sPrevious": "Sebelumnya",
                        "sNext": "Selanjutnya"
                    }
                }
            });

            $("#DataTable_list_pendingan_klinik").dataTable({
                scrollY: '50vh',
                scrollX: 'true',
                scrollCollapse: true,
                "oLanguage": {
                    "sLengthMenu": "Tampilkan _MENU_ data per halaman",
                    "sSearch": "Pencarian: ",
                    "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
                    "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
                    "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
                    "sInfoFiltered": "(di filter dari _MAX_ total data)",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sLast": "Akhir",
                        "sPrevious": "Sebelumnya",
                        "sNext": "Selanjutnya"
                    }
                }
            });
        </script>