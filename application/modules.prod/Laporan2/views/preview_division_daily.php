<?php
if ($cek_data->num_rows() > 0) {
?>
    <div class="center">
        <table>
            <thead>
                <tr>
                    <td rowspan="2" colspan="7">
                        <h5> LAPORAN <?= strtoupper($type) ?> BARANG KELUAR PT. KOSMETIKA CANTIK INDONESIA </h5>
                    </td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <br />
    <?php
    $x = 1;
    foreach ($arrdata as $index => $value) {
    ?>
        <table class="table table-striped table-bordered text-center" id='DataTable_preview_tb1'>
            <thead>
                <tr>
                    <td colspan="2" style="background-color:#FFE4C4"> <?= $arrproduk[$index] ?></td>
                </tr>
                <tr>
                    <td style="background-color:#BAB86C"> Nama Seller </td>
                    <td style="background-color:#BAB86C"> Quantity</td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($value as $index2 => $value2) {
                ?>
                    <tr>
                        <td> <?= $value2['nama'] ?> </td>
                        <td> <?= $value2['package_detail_quantity'] ?> </td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <br />
        <br />
    <?php

    }
} else {
    ?>
    <table class="table table-striped table-bordered text-center" id='DataTable_preview_tb2'>
        <thead>
            <?php
            if ($type == "draft") {
            ?>
                <tr>
                    <td rowspan="2" colspan="7" style="background-color:#8CD3FF;"> LAPORAN <?= strtoupper($type) ?> BARANG KELUAR PT. KOSMETIKA CANTIK INDONESIA </td>
                </tr>
            <?php
            } else {
            ?>
                <tr>
                    <td colspan="7" style="background-color:#8CD3FF;"> LAPORAN <?= strtoupper($type) ?> BARANG KELUAR PT. KOSMETIKA CANTIK INDONESIA </td>
                </tr>
                <tr>
                    <!-- <td colspan="7" style="background-color:#8CD3FF;"> <?= $tgl_format ?> </td> -->
                </tr>
            <?php
            }
            ?>

        </thead>
        <tbody>
        </tbody>
    </table>
    <br />
    <table class="table table-striped table-bordered text-center" id='DataTable_preview_tb3'>
        <thead>
            <tr>
                <td colspan="2" style="background-color:#FFE4C4"> DATA NOT FOUND</td>
            </tr>
            <tr>
                <td style="background-color:#BAB86C"> Nama Seller </td>
                <td style="background-color:#BAB86C"> Quantity</td>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
<?php
}
?>


<script type="text/javascript">
    $("#DataTable_preview_tb1").dataTable({
        scrollY: '50vh',
        scrollX: 'true',
        scrollCollapse: true,
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data per halaman",
            "sSearch": "Pencarian: ",
            "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
            "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
            "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
            "sInfoFiltered": "(di filter dari _MAX_ total data)",
            "oPaginate": {
                "sFirst": "Awal",
                "sLast": "Akhir",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya"
            }
        }
    });

    $("#DataTable_preview_tb2").dataTable({
        scrollY: '50vh',
        scrollX: 'true',
        scrollCollapse: true,
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data per halaman",
            "sSearch": "Pencarian: ",
            "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
            "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
            "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
            "sInfoFiltered": "(di filter dari _MAX_ total data)",
            "oPaginate": {
                "sFirst": "Awal",
                "sLast": "Akhir",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya"
            }
        }
    });

    $("#DataTable_preview_tb3").dataTable({
        scrollY: '50vh',
        scrollX: 'true',
        scrollCollapse: true,
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data per halaman",
            "sSearch": "Pencarian: ",
            "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
            "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
            "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
            "sInfoFiltered": "(di filter dari _MAX_ total data)",
            "oPaginate": {
                "sFirst": "Awal",
                "sLast": "Akhir",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya"
            }
        }
    });
</script>