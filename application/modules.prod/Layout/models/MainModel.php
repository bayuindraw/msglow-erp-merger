<?php
defined('BASEPATH') or exit('No direct script access allowed');
class MainModel extends CI_Model
{
	function get()
	{
		die('xx');
		if($_SESSION['role_id'] == '2') '';
		else $this->db->where("m_role.role_id NOT IN ('2', '1')");
		$this->db->join('m_role', 'm_role.role_id = m_user.role_id');
		$data = $this->db->get('m_user');
		return $data;
	}
	
	function get_list_role()
	{
		if($_SESSION['role_id'] == '2') '';
		else $this->db->where("role_id NOT IN ('2', '1')");
		return $this->db->get('m_role')->result();
	}
	
	function get_list_users_propinsi($id = ""){
		$this->db->where('users_id', $id);
		$data = $this->db->get('users_provinsi')->result_array();
		foreach($data as $index => $value){
			$newdata[$value['provinsi_id']] = $value['users_id']; 
		}
		return @$newdata;
	}
	
	function insert($data = array())
	{
		$this->db->insert('m_user', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function update($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update('m_user', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete('m_user');
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
}
