<?php
$xx = @$this->uri->segment(3, 0);
if($xx == "stock_opname_produk" && $xx != 0){ 
}else if (@!$_SESSION['logged_in'] && $allow != 1) {
	
	redirect(site_url() . "Login");
}
?>
<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->

<head>
	<base href="../../">
	<meta charset="utf-8" />
	<title>MSGLOW OFFICE</title>
	<meta name="description" content="Page with empty content">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--begin::Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

	<!--end::Fonts -->

	<!--begin::Page Vendors Styles(used by this page) -->
	<link href="<?= base_url() ?>web/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<!--end::Page Vendors Styles -->

	<!--begin::Global Theme Styles(used by all pages) -->
	<link href="<?= base_url() ?>web/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/css/style.bundle.css" rel="stylesheet" type="text/css" />

	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->
	<link href="<?= base_url() ?>web/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/css/skins/brand/dark.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/css/skins/aside/dark.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/plugins/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/plugins/select2/css/s2-docs.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Layout Skins -->
	<link rel="shortcut icon" href="https://msglow.app/upload/nitrogen.png" />
</head>
<?php if ($_SESSION['logged_in']) { ?>
	<script src="<?= base_url() ?>web/plugins/jquery.js" type="text/javascript"></script>
	<!--<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" type="text/javascript"></script>-->
	<script src="<?= base_url() ?>web/plugins/custom/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
	<script src="<?= base_url() ?>web/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>

<?php } ?>

<script src="<?= base_url() ?>web/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

<script src="<?= base_url() ?>web/plugins/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?= base_url() ?>web/plugins/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>web/plugins/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>
<link rel="stylesheet" href="<?= base_url('web/css/lightbox.css'); ?>">

<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

	<!-- begin:: Page -->

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="#">
				<img alt="Logo" src="https://msglow.app/upload/logo_nitromerah.png" style="max-height: 20px;" />
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
		</div>
	</div>

	<!-- end:: Header Mobile -->
	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

			<!-- begin:: Aside -->

			<!-- Uncomment this to display the close button of the panel
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
-->
			<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

				<!-- begin:: Aside -->
				<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand" style="background-color: rgb(0 76 153)">
					<div class="kt-aside__brand-logo">
						<a href="<?= site_url() ?>">
							<!-- <img alt="Logo" src="<?= base_url('assets/logo-01.gif'); ?>" style="width: 192px;" /> -->
							<img src="https://msglow.app/upload/logo_nitromerah.png" style="width: 192px;" alt="Logo">
						</a>
					</div>
					<div class="kt-aside__brand-tools">
					</div>
				</div>

				<!-- end:: Aside -->
<?php if(@$this->uri->segment(3, 0) != 'stock_opname_produk' || $_SESSION['logged_in']) { ?>
				<!-- begin:: Aside Menu -->
				
				<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
					<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
						<ul class="kt-menu__nav ">

							<?php if ($_SESSION['role_id'] == "4") { ?>
								<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Master/index_pic" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-tachometer-alt"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
							<?php } ?>
							<?php if ($_SESSION['role_id'] == "5"  || $_SESSION['role_id'] == "10"|| $_SESSION['role_id'] == "23") { ?>
								<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-tachometer-alt"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
							<?php } ?>
							<?php if ($_SESSION['role_id'] == "7" || $_SESSION['role_id'] == "17" || $_SESSION['role_id'] == "12") { ?>
								<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-tachometer-alt"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
							<?php } ?>
							<?php if ($_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "11") { ?>
								<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Dashboard" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-tachometer-alt"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
							<?php } ?>
							<li class="kt-menu__section ">
								<h4 class="kt-menu__section-text"><?= $_SESSION['role_name'] ?></h4>
								<i class="kt-menu__section-icon flaticon-more-v2"></i>
							</li>
							<?php if ($_SESSION['role_id'] == "98") { ?>
								<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_salesd" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-home"></i><span class="kt-menu__link-text">INPUT PENDINGAN</span></a></li>
								<!-- <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">DATA PENDINGAN</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_salesd/table_pending_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Pendingan Per Produk</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_salesd/table_pending_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Pendingan Per Seller</span></a></li>
										</ul>
									</div>
								</li> -->
							<?php } ?>
							<?php if ($_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "0") {
								$this->load->view('administrator_menu');
							}
							if ($_SESSION['role_id'] == "11") {
								$this->load->view('sidemenu2');
							}
							if ($_SESSION['role_id'] == "3" || $_SESSION['role_id'] == 0) {
								$this->load->view('sidemenu3');
							}
							if ($_SESSION['role_id'] == "4" || $_SESSION['role_id'] == 0) {
								$this->load->view('sidemenu4');
							}
							if ($_SESSION['role_id'] == "24") {
								$this->load->view('product_menu');
							}
							if ($_SESSION['role_id'] == "5" || $_SESSION['role_id'] == "10" || $_SESSION['role_id'] == "16") {
								$this->load->view('product_menu');
							}
							if ($_SESSION['role_id'] == "10") {
								$this->load->view('sidemenu6');
							}
							if ($_SESSION['role_id'] == "6") {
								$this->load->view('sidemenu7');
							}
							if ($_SESSION['role_id'] == 12 || $_SESSION['role_id'] == 17 || $_SESSION['role_id'] == 7 || $_SESSION['role_id'] == 0) {
								$this->load->view('sales_menu');
							};
							if ($_SESSION['role_id'] == "15") {
								$this->load->view('admin_ongkir_menu');
							}
							if ($_SESSION['role_id'] == "26" || $_SESSION['role_id'] == "12") { ?>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-dolly"></i><span class="kt-menu__link-text">Inventory Transfer</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_inventory/transfer" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Inventory Transfer Order</span></a></li>
										</ul>
									</div>
								</li>

							<?php }
							if ($_SESSION['role_id'] == "21") { ?>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>R_barcode_pwa_generator" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-users"></i><span class="kt-menu__link-text">Barcode</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_delivery_order" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-users"></i><span class="kt-menu__link-text">Surat Jalan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
							<?php }
							if ($_SESSION['role_id'] == "23") { ?>
								<li class="kt-menu__section ">
									<h5 class="kt-menu__section-text">Sales Feature</h5>
									<i class="kt-menu__section-icon flaticon-more-v2"></i>
								</li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-hand-holding-usd"></i><span class="kt-menu__link-text">Penjualan Lunas</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('T_sales/Form/Tambah') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Input Penjualan</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('T_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Penjualan</span></a></li>
										</ul>
									</div>
								</li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('Laporan2/list_boleh_kirim_vs_kirim') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-alt"></i><span class="kt-menu__link-text">List Pengiriman</span></a></li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('T_package_trial2/list_pendingan') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-alt"></i><span class="kt-menu__link-text">List Pendingan Produk</span></a></li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('T_package_trial2/list_pendingan_seller') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-alt"></i><span class="kt-menu__link-text">List Pendingan Seller</span></a></li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cart-arrow-down"></i><span class="kt-menu__link-text">Pembayaran</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
								    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
								        <ul class="kt-menu__subnav">
								            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_account/seller') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pembayaran</span></a></li>
								            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_account/seller_parsial') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pembayaran Parsial</span></a></li>
								            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_account/edit_verif_pembayaran') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Perubahan Verifikasi Pembayaran </span></a></li>
								        </ul>
								    </div>
								</li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cubes"></i><span class="kt-menu__link-text">Laporan Inventory</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
								    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
								        <ul class="kt-menu__subnav">
								            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Administrator/Stock_Produk/stock_opname_produk') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Stock Hari Ini</span></a></li>
								            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Packing</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
								                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
								                    <ul class="kt-menu__subnav">
								                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('T_package_trial2') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History Pengiriman</span></a></li>
								                    </ul>
								                </div>
								            </li>
								            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Gudang</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
								                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
								                    <ul class="kt-menu__subnav">
								                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/monthly_all') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Bulanan</span></a></li>
								                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/division_daily') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
								                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/monthly_factory_in') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pabrik Bulanan</span></a></li>
								                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/daily_all') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
								                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/daily_out') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li>
								                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/daily_product_out') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Produk Harian</span></a></li>
								                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_out') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Bulanan</span></a></li>
								                        <?php if ($_SESSION['role_id'] == "16") { ?>
								                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/chart') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Chart</span></a></li>
								                        <?php } ?>
								                    </ul>
								                </div>
								            </li>
								        </ul>
								    </div>
								</li>
								
									<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Laporan Sales</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
									        <ul class="kt-menu__subnav">
									            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Produk</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
									                    <ul class="kt-menu__subnav">
									                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Bulanan</span></a></li>
									                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Sales</span></a></li>
									                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_payment') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembayaran Produk</span></a></li>
									                    </ul>
									                </div>
									            </li>
									            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Sales</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
									                    <ul class="kt-menu__subnav">
									                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_seller') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller Bulanan</span></a></li>
									                        <!--<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_paid') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Mutasi Perorangan</span></a></li>-->
									                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_user') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Sales</span></a></li>
									                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Bulanan</span></a></li>
									                    </ul>
									                </div>
									            </li>
									        </ul>
									    </div>
									</li>
													<li class="kt-menu__section ">
														<h5 class="kt-menu__section-text">Inventory Feature</h5>
														<i class="kt-menu__section-icon flaticon-more-v2"></i>
													</li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-undo"></i><span class="kt-menu__link-text">Line Quality Control</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Adjustment</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/form_masuk_opname" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Masuk</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/form_keluar_opname" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Keluar</span></a></li>
					                    </ul>
					                </div>
					            </li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Tidak Sesuai SJ</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_retur" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pengembalian Retur</span></a></li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_factory" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Retur Ke Pabrik</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_repair" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Repair</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_sample" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Sample</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_rusak" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Rusak</span></a></li>
					        </ul>
					    </div>
					</li>
					
					
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cubes"></i><span class="kt-menu__link-text">Managemen Finish Good</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock/po_produk_pic" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Verifikasi PO Barang Jadi</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/stock_opname_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Stock Barang Jadi</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/kartu_stock_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Kartu Stock</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/po_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Input PO Barang Jadi</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Produk/table_slow_moving" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>List Dead Stock</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>List PO</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Active</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk/index2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Selesai</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Trial PO</span></a></li>
					                    </ul>
					                </div>
					            </li>
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Pengemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <!-- <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Pendingan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_sales/table_pending_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pendingan Per Produk</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_sales/table_pending_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pendingan Per Seller</span></a></li>
					                    </ul>
					                </div>
					            </li> -->
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/list_pendingan" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pendingan</span></a></li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Laporan2/list_boleh_kirim_vs_kirim" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pengiriman</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Keluar</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pembagian Produk</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Konfirmasi Pengiriman</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History</span></a></li>
					                    </ul>
					                </div>
					            </li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_safeconduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Surat Jalan</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_safeconduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Surat Jalan Selesai</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package/table_postage_finish" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Ongkir</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>M_account2/fifoProduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Incomplete Sales Data</span></a></li>
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="<?= base_url() ?>Laporan2/report_all" class="kt-menu__link"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span></div>
					</li>
					<li class="kt-menu__section ">
														<h5 class="kt-menu__section-text">Accounting Feature</h5>
														<i class="kt-menu__section-icon flaticon-more-v2"></i>
													</li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_postage" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-shipping-fast"></i><span class="kt-menu__link-text">Ongkir</span></a></li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-check-square"></i><span class="kt-menu__link-text">Verifikasi</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/verification_transfer_seller") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Pembayaran Seller 1 </span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/verification_transfer_seller2") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Pembayaran Seller 2 </span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/get_finish_rev") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Revisi Pembayaran </span></a></li>
					           
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Laporan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("T_coa_transaction") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Jurnal Umum</span></a></li>
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-chart-line"></i><span class="kt-menu__link-text">Laporan Keuangan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_sales_product_payment_acc") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount SO</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_pr") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount PR</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_do") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount DO</span></a></li>
        </ul>
    </div>
</li>
												<?php }
												if ($_SESSION['role_id'] == "14" || $_SESSION['role_id'] == 0) { ?>

													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/po_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-tag"></i><span class="kt-menu__link-text">Input PO Barang Jadi</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">List PO</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">PO Active</span></a></li>
																<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk/index2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">PO Selesai</span></a></li>
																<li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Retur Barang Jadi</span></a></li>
															</ul>
														</div>
													</li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon  fas fa-gift"></i><span class="kt-menu__link-text">Barang Tidak Sesuai SJ</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_retur" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-backward"></i><span class="kt-menu__link-text">Pengembalian Retur</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_sample" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon  fas fa-gift"></i><span class="kt-menu__link-text">Sample</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">

																<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_factory_in" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Pabrik Bulanan</span></a></li>

															</ul>
														</div>
													</li>
												<?php }
												if ($_SESSION['role_id'] == "18" || $_SESSION['role_id'] == 0) { ?>

													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">Barang Keluar</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Pembagian Produk</span></a></li>
																<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Konfirmasi Pengiriman</span></a></li>
																<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">History</span></a></li>
															</ul>
														</div>
													</li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/division_daily" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
																<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li>
																<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_product_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Keluar Produk Harian</span></a></li>
																<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Keluar Bulanan</span></a></li>
															</ul>
														</div>
													</li>
												<?php }
												if ($_SESSION['role_id'] == "19" || $_SESSION['role_id'] == 0) { ?>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-archive"></i><span class="kt-menu__link-text">Master</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_login_pwa" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">User PWA</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_line" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Line</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Factory" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pabrik</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Produk</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk_global" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_product_global_group" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU Group</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_brand" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Brand</span></a></li>
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Bulanan1</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/division_daily" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_factory_in" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Terima Barang Bulanan</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
					            <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li> -->
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_product_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Harian</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Bulanan</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Pendingan</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan_per_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Pendingan Per Seller</span></a></li>
					            <?php if ($_SESSION['role_id'] == "16") { ?>
					                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Chart</span></a></li>
					            <?php } ?>
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cubes"></i><span class="kt-menu__link-text">Managemen Barang Jadi</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/stock_opname_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Stock Barang Jadi</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/kartu_stock_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Kartu Stock</span></a></li>
					           
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Pengemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <!-- <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Pendingan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_sales/table_pending_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pendingan Per Produk</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_sales/table_pending_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pendingan Per Seller</span></a></li>
					                    </ul>
					                </div>
					            </li> -->
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/list_pendingan" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pendingan</span></a></li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Laporan2/list_boleh_kirim_vs_kirim" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pengiriman</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Keluar</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pembagian Produk</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Konfirmasi Pengiriman</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_transfer" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Konfirmasi Inventory Transfer</span></a></li> 
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History</span></a></li>
					                    </ul>
					                </div>
					            </li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_safeconduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Surat Jalan</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_safeconduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Surat Jalan Selesai</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package/table_postage_finish" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Ongkir</span></a></li>
					        </ul>
					    </div>
					</li> 
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"><a href="<?= base_url() ?>T_sales/table_member" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Penjualan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-handshake"></i><span class="kt-menu__link-text">Sales</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Produk</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Bulanan</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Sales</span></a></li>
					                    </ul>
					                </div>
					            </li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Seller</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller Bulanan</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_paid" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Mutasi Perorangan</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Bulanan</span></a></li>
					                    </ul>
					                </div>
					            </li>
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-chart-line"></i><span class="kt-menu__link-text">Laporan Keuangan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_sales_product_payment_acc") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount SO</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan2/monthly_pr") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount PR</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan2/monthly_do") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount DO</span></a></li>
        </ul>
    </div>
</li>
													
																<?php if ($_SESSION['role_id'] == "16") { ?>
																	<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Chart</span></a></li>
																<?php } ?>
															</ul>
														</div>
													</li>
												<?php }
												if ($_SESSION['role_id'] == "22" || $_SESSION['role_id'] == 0) { ?>
												
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-archive"></i><span class="kt-menu__link-text">Master</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_login_pwa" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">User PWA</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_line" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Line</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Factory" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pabrik</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Produk</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk_global" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_product_global_group" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU Group</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_brand" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Brand</span></a></li>
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Bulanan</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/division_daily" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_factory_in" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Terima Barang Bulanan</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
					            <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li> -->
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_product_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Harian</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Bulanan</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Pendingan</span></a></li>
					            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan_per_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Pendingan Per Seller</span></a></li>
					            <?php if ($_SESSION['role_id'] == "16") { ?>
					                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Chart</span></a></li>
					            <?php } ?>
							
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/sample" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Sample</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/rusak" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Rusak</span></a></li>
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cubes"></i><span class="kt-menu__link-text">Managemen Barang Jadi</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock/po_produk_pic" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Verifikasi PO Barang Jadi</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/stock_opname_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Stock Barang Jadi</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/kartu_stock_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Kartu Stock</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/po_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Input PO Barang Jadi</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Produk/table_slow_moving" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>List Dead Stock</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>List PO</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Active</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk/index2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Selesai</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Trial PO</span></a></li>
					                    </ul>
					                </div>
					            </li>           
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-undo"></i><span class="kt-menu__link-text">Line Quality Control</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Adjustment</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/form_masuk_opname" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Masuk</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/form_keluar_opname" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Keluar</span></a></li>
					                    </ul>
					                </div>
					            </li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Tidak Sesuai SJ</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_retur" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pengembalian Retur</span></a></li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_factory" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Retur Ke Pabrik</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_repair" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Repair</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_sample" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Sample</span></a></li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_rusak" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Rusak</span></a></li>
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Pengemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <!-- <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Pendingan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_sales/table_pending_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pendingan Per Produk</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_sales/table_pending_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pendingan Per Seller</span></a></li>
					                    </ul>
					                </div>
					            </li> -->
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/list_pendingan" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pendingan</span></a></li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Laporan2/list_boleh_kirim_vs_kirim" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pengiriman</span></a></li>
					            
					        </ul>
					    </div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"><a href="<?= base_url() ?>T_sales" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Penjualan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
					<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-handshake"></i><span class="kt-menu__link-text">Sales</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					        <ul class="kt-menu__subnav">
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Produk</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/sample" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Sample</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/rusak" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Rusak</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Bulanan</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Sales</span></a></li>
					                    </ul>
					                </div>
					            </li>
					            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Seller</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
					                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
					                    <ul class="kt-menu__subnav">
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller Bulanan</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_paid" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Mutasi Perorangan</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller</span></a></li>
					                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Bulanan</span></a></li>
					                    </ul>
					                </div>
					            </li>
					        </ul>
					    </div>
					</li>
													
																<?php if ($_SESSION['role_id'] == "16") { ?>
																	<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Chart</span></a></li>
																<?php } ?>
															</ul>
														</div>
													</li>
												<?php } ?>
											</ul>
										</div>
									</div>
						<?php } ?>
				<!-- end:: Aside Menu -->
			</div>

			<!-- end:: Aside -->
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

				<!-- begin:: Header -->
				<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " style="background-color: #399bff;">


					<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
						<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">

						</div>
					</div>

					<!-- end:: Header Menu -->

					<!-- begin:: Header Topbar -->
					<?php $this->load->view('header_topbar'); ?>

					<!-- end:: Header Topbar -->
				</div>
				<script src="<?= base_url() ?>web/plugins/general/raphael/raphael.js" type="text/javascript"></script>
				<script src="<?= base_url() ?>web/plugins/general/morris.js/morris.js" type="text/javascript"></script>
				<script src="<?= base_url() ?>web/plugins/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
				<script src="<?= base_url() ?>web/js/scripts.bundle.js" type="text/javascript"></script>
				<script src="<?= base_url() ?>web/js/pages/dashboard.js" type="text/javascript"></script>
