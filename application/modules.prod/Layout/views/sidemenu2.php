<?php
$verifikasi_pembayaran_seller_1 = $this->db->query("select * from v_count_verifikasi_pembayaran_seller_1_open")->num_rows();

$verifikasi_pembayaran_seller_2 = $this->db->query("select * from v_count_verifikasi_pembayaran_seller_2_open")->num_rows();

$verifikasi_revisi_pembayaran_open = $this->db->query("select * from v_count_verifikasi_revisi_pembayaran_open")->num_rows();

$count_verifikasi = $verifikasi_pembayaran_seller_1 + $verifikasi_pembayaran_seller_2 + $verifikasi_revisi_pembayaran_open;
?>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-chart-area"></i><span class="kt-menu__link-text">COA</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">COA 1</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA/coa2" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">COA 2</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA/coa3" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">COA 3</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA/coa4" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">COA 4</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-piggy-bank"></i><span class="kt-menu__link-text">Kas</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_coa_user" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Kas Pengeluaran</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_coa_user/table_realization" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">List Realisasi</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Transfer_account" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Transfer Antar Akun</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_coa_user/table_history" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Riwayat Pengeluaran Kas</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-briefcase"></i><span class="kt-menu__link-text">Aset</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Aset_tetap/form" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Input Aset Tetap</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Aset_tetap" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">History Aset Tetap</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Aset_lainnya/form" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Input Aset Lainnya</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Aset_lainnya" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">History Aset Lainnya</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-dollar-sign"></i><span class="kt-menu__link-text">Biaya Dibayar Dimuka</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Dibayar_dimuka/form" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Input Beban Dibayar Dimuka</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Dibayar_dimuka" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">History Beban Dibayar Dimuka</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-chart-line"></i><span class="kt-menu__link-text">Laporan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("T_coa_transaction") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Jurnal Umum</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/laporan_total_akun") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Laporan Total Akun</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan2/laporan_kas") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Laporan Kas</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan2/laporan_fifo") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Laporan FIFO</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan2/laporan_laba_rugi") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Laporan Laba Rugi</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-chart-line"></i><span class="kt-menu__link-text">Laporan Keuangan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_sales_product_payment_acc") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount SO</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_pr") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount PR</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_do") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount DO</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-check-square"></i><span class="kt-menu__link-text">Verifikasi</span><span class="badge badge-warning badge-pill" style="padding-left: 1em;padding-right: 1em; padding-top: 0.7em;"><?php if ($count_verifikasi > 0) echo $count_verifikasi; ?></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/verification_transfer_seller") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Pembayaran Seller 1 <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_pembayaran_seller_1 > 0) echo $verifikasi_pembayaran_seller_1; ?></span></span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/verification_transfer_seller2") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Pembayaran Seller 2 <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_pembayaran_seller_2 > 0) echo $verifikasi_pembayaran_seller_2; ?></span></span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/get_finish_rev") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Revisi Pembayaran <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_revisi_pembayaran_open > 0) echo $verifikasi_revisi_pembayaran_open; ?></span></span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Tb_Stock_Produk_Detail") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Barang Masuk</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("T_postage") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Ongkir</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Laporan Sales</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Sales</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Produk</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Bulanan</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Sales</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_payment') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembayaran Produk</span></a></li>
                    </ul>
                </div>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Sales</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_seller') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller Bulanan</span></a></li>
                        <!--<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_paid') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Mutasi Perorangan</span></a></li>-->
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_user') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Sales</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Bulanan</span></a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Laporan Inventory</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">Inventory</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Bulanan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/division_daily" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_factory_in" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Terima Barang Bulanan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
            <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li> -->
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_product_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Harian</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Bulanan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Pendingan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan_per_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Pendingan Per Seller</span></a></li>
            <?php if ($_SESSION['role_id'] == "16") { ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Chart</span></a></li>
            <?php } ?>
        </ul>
    </div>
</li>