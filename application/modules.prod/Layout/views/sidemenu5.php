<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">Master</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_login_pwa" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">User PWA</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_line" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Line</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Factory" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Pabrik</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Produk</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk_global" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">SKU</span></a></li>
			<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_product_global_group" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">SKU GROUP</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_brand" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Brand</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock/po_produk_pic" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Verifikasi PO Barang Jadi</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/stock_opname_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box-open"></i><span class="kt-menu__link-text">Stock Barang Jadi</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/kartu_stock_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon  fas fa-id-card"></i><span class="kt-menu__link-text">Kartu Stock</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/po_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-tag"></i><span class="kt-menu__link-text">Input PO Barang Jadi</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Produk/table_slow_moving" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-tag"></i><span class="kt-menu__link-text">List Dead Stock</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">List PO</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">PO Active</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk/index2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">PO Selesai</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Trial PO</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Retur</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">Adjustment</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/form_masuk_opname" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Barang Masuk</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/form_keluar_opname" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Barang Keluar</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon  fas fa-gift"></i><span class="kt-menu__link-text">Barang Tidak Sesuai SJ</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_retur" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-backward"></i><span class="kt-menu__link-text">Pengembalian Retur</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_repair" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-hammer"></i><span class="kt-menu__link-text">Repair</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_sample" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon  fas fa-gift"></i><span class="kt-menu__link-text">Sample</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_rusak" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-trash"></i><span class="kt-menu__link-text">Rusak</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Pengemasan</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<!-- <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">Data Pendingan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_sales/table_pending_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Pendingan Per Produk</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_sales/table_pending_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Pendingan Per Seller</span></a></li>
        </ul>
    </div>
</li> -->
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/list_pendingan" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">List Pendingan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">Barang Keluar</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Pembagian Produk</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Konfirmasi Pengiriman</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">History</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_safeconduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-forward"></i><span class="kt-menu__link-text">Surat Jalan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_safeconduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-forward"></i><span class="kt-menu__link-text">Surat Jalan Selesai</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package/table_postage_finish" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-shopping-cart"></i><span class="kt-menu__link-text">Data Ongkir</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Laporan</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Bulanan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/division_daily" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_factory_in" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Terima Barang Bulanan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
            <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li> -->
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_product_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Harian</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Bulanan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Pendingan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan_per_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Pendingan Per Seller</span></a></li>
            <?php if ($_SESSION['role_id'] == "16") { ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Chart</span></a></li>
            <?php } ?>
        </ul>
    </div>
</li>