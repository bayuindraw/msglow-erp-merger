<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
    <thead>
        <tr>
            <th>Rekening Tujuan</th>
            <th>Tanggal</th>
            <th>Nama Pengirim</th>
            <th>Bukti Transfer</th>
            <th>Catatan</th>
            <th>Nominal (sales)</th>
            <th>Verif Nominal</th>
            <th>Verification</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($mutasis as $key => $mutasi) { ?>
            <input type="hidden" name="input[<?= $key ?>][source_id]" value="<?= $mutasi['account_detail_real_id'] ?>" />
            <input type="hidden" name="input[<?= $key ?>][group_id]" value="<?= $mutasi['coa_transaction_source_id'] ?>" />
            <tr>
                <td><input type="hidden" value="<?= $mutasi['account_name'] ?>" name="input[<?= $key ?>][account_name]" /><?= $mutasi['account_name'] ?></td>
                <td><input type="hidden" value="<?= $mutasi['account_detail_date'] ?>" name="input[<?= $key ?>][account_detail_date]" /><?= $mutasi['account_detail_date'] ?></td>
                <td><?= $mutasi['account_detail_transfer_name'] ?></td>
                <td><img src="<?= base_url() . $mutasi['proof'] ?>" style="max-width: 90%;" /></td>
                <td><input type="text" value="<?= $mutasi['account_detail_note_accounting'] ?>" name="input[<?= $key ?>][account_detail_note_accounting]" /></td>
                <td><input type="hidden" value="<?= $mutasi['account_detail_debit'] ?>" name="input[<?= $key ?>][account_detail_debit]" /><?= $mutasi['account_detail_debit'] ?></td>
                <td><input type="number" name="input[<?= $key ?>][nominal]" value="<?= str_replace(".00", "", $mutasi['coa_credit']) ?>" readonly /></td>
                <td><input type="checkbox" name="input[<?= $key ?>][is_verification]" checked></td>
            </tr>
        <?php } ?>
    </tbody>
</table>