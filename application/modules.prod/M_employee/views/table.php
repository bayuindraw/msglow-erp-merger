							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											<?= $title ?>
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<a href="<?= site_url().$url."/form/tambah" ?>" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Tambah
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama</th>
												<th>Alamat</th>
												<th>Telephone</th>
												<th>Email</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div> 
							
<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
		"processing": true,
		"serverSide": true,
		"ajax": "<?= site_url() ?>/<?= $url ?>/get_data"
		});
	});
</script>