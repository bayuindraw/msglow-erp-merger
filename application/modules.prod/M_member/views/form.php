<?php
if ($parameter == 'ubah' && $id != '') {
	$query = "SELECT * FROM m_member A LEFT JOIN mst_kota B ON B.id_kota = A.city_id WHERE A.member_id = '$id'";
	$data = $this->db->query($query)->row_array();
	$query = "SELECT * FROM mst_kota WHERE id_provinsi = '$data[province_id]'";
	$arrkota = $this->db->query($query)->result_array();
	foreach($arrkota as $key => $value){
		$kota[$value['id_kota']] = $value['nama_kota'];
	}
	$query = "SELECT * FROM mst_kecamatan WHERE id_kota = '$data[city_id]'";
	$arrkecamatan = $this->db->query($query)->result_array();
	foreach($arrkecamatan as $key => $value){
		$kecamatan[$value['id_kecamatan']] = $value['nama_kecamatan'];
	}
}
?>
<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
<div class="row">
								<div class="col-lg-8 col-xl-6">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
												<?php if(@$_SESSION['logged_in'] != ""){ ?>
		<label>ID MEMBER</label>
		<?= input_text('input[member_code]', @$data['member_code'], '', 'placeholder = "ID Member"') ?>
	</div>
	<?php } ?>
	<div class="form-group">
		<label>NAMA LENGKAP</label>
		<?= input_text('input[member_name]', @$data['member_name'], '', 'placeholder = "Nama Lengkap" required') ?>
	</div>
	<div class="form-group">
		<label>ALAMAT</label>
		<?= input_text('input[member_address]', @$data['member_address'], '', 'placeholder = "Alamat" required') ?>
	</div>
	<div class="form-group">
		<label>PROVINSI</label><br>
		<select class="custom-select form-control" name="input[province_id]" id="province_id" required onchange="chk_provinsi()">
			<option value="">Pilih Provinsi</option>
			<?php  
				foreach ($provinsi as $key2 => $value2) { // Lakukan looping pada variabel siswa dari controller
					echo "<option value='" . $value2->id_provinsi . "'".(($value2->id_provinsi == @$data['province_id'])?'selected':'').">" . $value2->nama_provinsi . "</option>";
				}
			 ?>
		</select>
		<div id="loading" style="margin-top: 15px;" style="">
			<!-- <img src="images/loading.gif" width="18"> <small>Loading...</small> -->
		</div>
	</div>
	<!--<div class="form-group">
		<label>KOTA</label>
		<?= input_text('city', @$data['nama_kota'], '', 'id="city"  url="'.site_url("autocomplete/city_new").'" placeholder="Pilih Kota" onBlur="$(this).val($(\'#city_chk\').val());" required') ?>
		<input type="hidden" name="input[city_id]" value="<?= @$data['city_id']; ?>" id="city_id">
		<input type="hidden" name="city_chk" value="<?= @$data['nama_kota']; ?>" id="city_chk">
	</div><br>-->
	<div class="form-group">
		<label>KOTA</label><br>
		<select class="custom-select form-control" name="input[city_id]" id="city" required onchange="chk_kota()">
			<option value="">Pilih Kota</option>
			<?php if(@$data['city_id'] != ""){ 
				foreach ($kota as $key2 => $value2) { // Lakukan looping pada variabel siswa dari controller
					echo "<option value='" . $key2 . "'".(($key2 == $data['city_id'])?'selected':'').">" . $value2 . "</option>";
				}
			} ?>
		</select>
		<div id="loading" style="margin-top: 15px;">
			<!-- <img src="images/loading.gif" width="18"> <small>Loading...</small> -->
		</div>
	</div>
	<div class="form-group">
		<label>KECAMATAN</label><br>
		<select class="custom-select form-control" name="input[district_id]" id="district" required>
			<option value="">Pilih Kecamatan</option>
			<?php if(@$data['district_id'] != ""){ 
				foreach ($kecamatan as $key2 => $value2) { // Lakukan looping pada variabel siswa dari controller
					echo "<option value='" . $key2 . "'".(($key2 == $data['district_id'])?'selected':'').">" . $value2 . "</option>";
				}
			} ?>
		</select>
		<div id="loading" style="margin-top: 15px;">
			<!-- <img src="images/loading.gif" width="18"> <small>Loading...</small> -->
		</div>
	</div>
	<div class="form-group">
		<label>NO REKENING</label>
		<?= input_text('input[member_bank_account]', @$data['member_bank_account'], '', 'placeholder = "No Rekening"') ?>
	</div>
	<div class="form-group">
		<label>NO TELEPON</label>
		<?= input_text('input[member_phone]', @$data['member_phone'], '', 'placeholder = "No Telepon" required') ?>
	</div>
	<div class="form-group">
		<label>UPLOAD SCAN KTP</label>
		<?php if(@$data['member_ktp']){ ?>
			<br><span id="spanKTP"><img src="<?= base_url('upload/KTP/'.@$data['member_ktp']) ?>" alt="KTP" width="250" height="250"> <a class="btn btn-danger" onclick="$('#updKTP').show();$('#spanKTP').hide();"><i class="fa fa-remove"></i></a></span>
			<?= input_file('upd[KTP]', '', '', 'id="updKTP" style="display:none;" onchange="loadKTP(event)"') ?>
			<?= input_hidden('KTP', @$data['member_ktp']) ?>
		<?php }else{ ?>
			<?= input_file('upd[KTP]', '', '', 'required onchange="loadKTP(event)"') ?>
		<?php } ?>
		<div id="divKPT"style="display:none;"><br><img id="outputKTP" width="250" height="250" /></div>
	</div>
	<div class="form-group">
		<label>UPLOAD BUKTI TRANSFER</label>
		<?php if(@$data['member_transfer']){ ?>
			<br><span id="spanTRANSFER"><img src="<?= base_url('upload/TRANSFER/'.@$data['member_transfer']) ?>" alt="TRANSFER" width="250" height="250"> <a class="btn btn-danger" onclick="$('#updTRANSFER').show();$('#spanTRANSFER').hide();"><i class="fa fa-remove"></i></a></span>
			<?= input_file('upd[TRANSFER]', '', '', 'id="updTRANSFER" style="display:none;" onchange="loadTRANSFER(event)"') ?>
			<?= input_hidden('TRANSFER', @$data['member_transfer']) ?>
		<?php }else{ ?>
			<?= input_file('upd[TRANSFER]', '', '', 'required onchange="loadTRANSFER(event)"') ?>
		<?php } ?>
		<div id="divTRANSFER"style="display:none;"><br><img id="outputTRANSFER" width="250" height="250" /></div>
	</div>
	<div class="form-group">
		<label>UPLOAD BUKTI TRANSFER 2</label>
		<?php if(@$data['member_transfer2']){ ?>
			<br><span id="spanTRANSFER2"><img src="<?= base_url('upload/TRANSFER/'.@$data['member_transfer2']) ?>" alt="TRANSFER2" width="250" height="250"> <a class="btn btn-danger" onclick="$('#updTRANSFER2').show();$('#spanTRANSFER2').hide();"><i class="fa fa-remove"></i></a></span>
			<?= input_hidden('TRANFER2', @$data['member_transfer2']) ?>
			<?= input_file('upd[TRANSFER2]', '', '', 'id="updTRANSFER2" style="display:none;" onchange="loadTRANSFER2(event)"') ?>
		<?php }else{ ?>
			<?= input_file('upd[TRANSFER2]', '', '', 'onchange="loadTRANSFER2(event)"') ?>
		<?php } ?>
		<div id="divTRANSFER2"style="display:none;"><br><img id="outputTRANSFER2" width="250" height="250" /></div>
	</div>
	<div class="form-group">
		<label>FOTO SELLER</label>
		<?php if(@$data['member_photo']){ ?>
			<br><span id="spanPHOTO"><img src="<?= base_url('upload/PHOTO/'.@$data['member_photo']) ?>" alt="PHOTO" width="250" height="250"> <a class="btn btn-danger" onclick="$('#updPHOTO').show();$('#spanPHOTO').hide();"><i class="fa fa-remove"></i></a></span>
			<?= input_file('upd[PHOTO]', '', '', 'id="updPHOTO" style="display:none;" onchange="loadPHOTO(event)"') ?>
			<?= input_hidden('PHOTO', @$data['member_photo']) ?>
		<?php }else{ ?>
			<?= input_file('upd[PHOTO]', '', '', 'required onchange="loadPHOTO(event)"') ?>
		<?php } ?>
		<div id="divPHOTO"style="display:none;"><br><img id="outputPHOTO" width="250" height="250" /></div>
	</div>
	<div class="form-group">
		<label>STATUS</label>
	</div>
	<div class="radio">
		<label><input type="radio" name="input[member_status_id]" value="1" <?= (@$data['member_status_id']=='1')?'checked':''; ?><?= (@$data['member_status_id']=='')?'checked':''; ?>> &nbsp;DISTRIBUTOR</label>
	</div>
	<div class="radio">
		<label><input type="radio" name="input[member_status_id]" value="2" <?= (@$data['member_status_id']=='2')?'checked':''; ?>> &nbsp;AGEN</label>
	</div>
	<div class="radio">
		<label><input type="radio" name="input[member_status_id]" value="3" <?= (@$data['member_status_id']=='3')?'checked':''; ?>> &nbsp;MEMBER</label>
	</div>
	<div class="radio">
		<label><input type="radio" name="input[member_status_id]" value="4" <?= (@$data['member_status_id']=='4')?'checked':''; ?>> &nbsp;RESELLER</label>
	</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										


</div>
</div>
<div class="col-lg-4 col-xl-6">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
			
	<div class="form-group">
		<label>ACCOUNT INSTAGRAM</label>
		<?= input_text('input[member_instagram]', @$data['member_instagram'], '', 'placeholder = "Account Instagram" required') ?>
	</div>
	<div class="form-group">
		<label>ACCOUNT FACEBOOK</label>
		<?= input_text('input[member_facebook]', @$data['member_facebook'], '', 'placeholder = "Account Facebook"') ?>
	</div>
	<div class="form-group">
		<label>ACCOUNT TWITTER</label>
		<?= input_text('input[member_twitter]', @$data['member_twitter'], '', 'placeholder = "Account Twitter"') ?>
	</div>
											</div>
										


</div>
</div>
</div>
</form>

<script>
	var loadKTP = function(event) {
		$('#divKPT').show();
		var image = document.getElementById('outputKTP');
		image.src = URL.createObjectURL(event.target.files[0]);
	};
	var loadTRANSFER = function(event) {
		$('#divTRANSFER').show();
		var image = document.getElementById('outputTRANSFER');
		image.src = URL.createObjectURL(event.target.files[0]);
	};
	var loadTRANSFER2 = function(event) {
		$('#divTRANSFER2').show();
		var image = document.getElementById('outputTRANSFER2');
		image.src = URL.createObjectURL(event.target.files[0]);
	};
	var loadPHOTO = function(event) {
		$('#divPHOTO').show();
		var image = document.getElementById('outputPHOTO');
		image.src = URL.createObjectURL(event.target.files[0]);
	};
	function chk_provinsi(){
		$.ajax({
			type: "POST", // Method pengiriman data bisa dengan GET atau POST
			url: "<?= base_url(); ?>M_member/list_kota", // Isi dengan url/path file php yang dituju
			data: {
				id_provinsi: $("#province_id"). children("option:selected"). val()
			}, // data yang akan dikirim ke file yang dituju
			dataType: "json",
			beforeSend: function(e) {
				if (e && e.overrideMimeType) {
					e.overrideMimeType("application/json;charset=UTF-8");
				}
			},
			success: function(response) { // Ketika proses pengiriman berhasil
				$("#loading").hide(); // Sembunyikan loadingnya

				// set isi dari combobox kota
				// lalu munculkan kembali combobox kotanya
				$("#city").html(response.list_stasiun).show();
			},
			error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
				alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
			}
		});
	}
	function chk_kota(){
		$.ajax({
			type: "POST", // Method pengiriman data bisa dengan GET atau POST
			url: "<?= base_url(); ?>M_member/list_kecamatan", // Isi dengan url/path file php yang dituju
			data: {
				id_kota: $("#city"). children("option:selected"). val()
			}, // data yang akan dikirim ke file yang dituju
			dataType: "json",
			beforeSend: function(e) {
				if (e && e.overrideMimeType) {
					e.overrideMimeType("application/json;charset=UTF-8");
				}
			},
			success: function(response) { // Ketika proses pengiriman berhasil
				$("#loading").hide(); // Sembunyikan loadingnya

				// set isi dari combobox kota
				// lalu munculkan kembali combobox kotanya
				$("#district").html(response.list_stasiun).show();
			},
			error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
				alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
			}
		});
	}
</script>

