<?php
//if ($parameter == 'ubah' && $id != '') {
	//$this->db->where('A.id_kemasan', $id);
	$row = $this->Model->get($id);
//}
?>
<div class="row">
	<div class="col-lg-12 col-xl-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<table>
							<tr>
								<td>Produk</td>
								<td>&nbsp;&nbsp;:</td>
								<td>&nbsp;&nbsp;<?= @$row['nama_produk'] ?></td>
							</tr>
							<!--<tr>
								<td>Seller</td>
								<td>&nbsp;&nbsp;:</td>
								<td>&nbsp;&nbsp;<?= $arrsales_member['nama'] . ' (' . $arrsales_member['kode'] . ')' ?></td>
							</tr>-->
						</table>
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<form method="post" action="<?= site_url($url . '/simpan_price'); ?>" enctype="multipart/form-data">
										
											<?= input_hidden('id', $id) ?>
											<?= input_hidden('input[produk_global_id]', $id) ?>
											<?= input_hidden('input[produk_global_kd]', @$row['kode']) ?>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Skema</th>
								<th>Jumlah Minimal</th>
								<th>Harga Satuan</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="x">
							<?php if (@count(@$arrprice2) > 0) {
								$i = 0;
								$total = 0;
								foreach ($arrprice2 as $indexx => $valuex) {

							?>
									<tr id="<?= $i; ?>">
										<?php if(@$valuex['schema_type'] == '2'){ ?>
											<td><input type="text" value="<?= $valuex['schema_name'] ?>" class="form-control static" readonly><input type="hidden"  value="<?= $valuex['schema_id'] ?>" name="input2[<?= $i ?>][schema_id]"></td>
											<td><input type="text" id="price_quantity<?= $i ?>" name="input2[<?= $i ?>][price_quantity]" value="1" min="0" class="form-control static numeric" readonly></td>
										<?php }else{ ?>
											<td><select class="form-control" name="input2[<?= $i ?>][schema_id]" required>
											<?php 
												foreach($arrschema_tier as $indexy => $valuexy){
													echo '<option value="'.$valuexy['schema_id'].'" '.((@$valuex['schema_id']==$valuexy['schema_id'])?'selected':'').'>'.$valuexy['schema_name'].'</option>';
												}
											?>
											</select></td>
											<td><input type="text" id="price_quantity<?= $i ?>" name="input2[<?= $i ?>][price_quantity]" value="<?= $valuex['price_quantity'] ?>" min="0" class="form-control static numeric"></td>
										<?php } ?>
										
										<td><input type="text" id="price_unit_price<?= $i ?>" name="input2[<?= $i ?>][price_unit_price]" value="<?= ($valuex['price_unit_price']=="")?0:$valuex['price_unit_price']; ?>" min="0" class="form-control static numeric"></td>
										<td><button onclick="myDeleteFunction('<?= $i ?>')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>			
									<?php $i++;
								}
							} else { ?>
									<tr id="0">
										<td><select class="form-control" name="input2[0][schema_id]" required><?= $schema_tier ?></select></td>
										<td><input type="text" id="price_unit_price0" name="input2[0][price_unit_price]" value="0" min="0" class="form-control static numeric"></td>
										<td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									</tr>
								<?php } ?>
						</tbody>
					</table>
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
					<div class="kt-portlet__foot" style="margin-top: 20px;">
						<div class="kt-form__actions">
							<button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;">
								<i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
							</button>
						</div>
					</div>
				</form>


			</div>
		</div>
	</div>
</div>


<script>
	it = <?= ((@count(@$arrprice2) > 0) ? count(@$arrprice2) : 1); ?>;

	function myCreateFunction() {
		it++;
		var html = '<tr id="' + it + '"><td><select class="form-control" name="input2[' + it + '][schema_id]" required><?= $schema_tier ?></select></td><td><input type="text" id="price_quantity' + it + '" name="input2[' + it + '][price_quantity]" value="0" min="0" class="form-control static numeric"></td><td><input type="text" id="price_unit_price' + it + '" name="input2[' + it + '][price_unit_price]" value="0" min="0" class="form-control static numeric"></td><td><button onclick="myDeleteFunction(' + it + ')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$(".numeric").mask("#,##0", {
			reverse: true
		});
	}

	function myDeleteFunction(id) {
		$('#' + id).remove();
	}

</script>