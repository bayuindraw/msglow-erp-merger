<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_role extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_roleModel', 'Model');
	}
	
	public function index()
	{
		$dataHeader['title']		= "HoMe | Web Administrator";
		$dataHeader['menu']   		= 'Hak Akses';
		$dataHeader['file']   		= 'Index' ;
		$dataHeader['link']   		= 'index' ;
		$datacontent['url'] = 'M_role';
		$datacontent['title'] = 'Role';
		$datacontent['datatable'] = $this->Model->get();
		$data['content'] = $this->load->view('M_role/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Administrator/back-eend/container/header',$dataHeader);
		$this->load->view('Administrator/back-eend/master/home',$data);
		$this->load->view('Administrator/back-eend/container/footer');
	}
	
	public function form($parameter = '', $id = '')
	{
		$datacontent['title'] = 'Role';
		$datacontent['parameter'] = $parameter;
		$datacontent['id'] = $id;
		$datacontent['url'] = 'm_role';
		$data['content'] = $this->load->view('m_role/m_roleForm', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('layouts/html', $data);
	}
	
	public function simpan()
	{

		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$exec = $this->Model->insert($data);
			} else {

				$this->Model->update($data, ['role_id' => $this->input->post('id')]);
			}
		}

		redirect(site_url('m_role'));
	}
	public function hapus($id = '')
	{
		$this->Model->delete(["role_id" => $id]);
		redirect('m_role');
	}
}
