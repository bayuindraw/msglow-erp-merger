<?= content_open($title) ?>


<a href="<?= site_url($url . '/form/tambah') ?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
<hr>
<?= $this->session->flashdata('info'); ?>

<body>
	<table id="example" class="table table-striped table-bordered" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th>Role</th>
				<th>Aksi</th>

			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;

			foreach ($datatable->result() as $row) {
			?>
				<tr>
					<td><?= $no ?></td>
					<td><?= $row->role_name ?></td>

					<td>
						<a href="<?= site_url($url . '/form/ubah/' . $row->role_id) ?>" class="btn btn-info"> <i class="fa fa-edit"></i></a>
						<a href="<?= site_url($url . '/hapus/' . $row->role_id) ?>" class="btn btn-danger" onclick="return confirm('Hapus data?')"> <i class="fa fa-trash"></i></a>
					</td>

				</tr>
			<?php
				$no++;
			}
			?>
		</tbody>
	</table>
</body>
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable({
			"pagingType": "full_numbers"
		});
	});
</script>
<?= content_close() ?>
