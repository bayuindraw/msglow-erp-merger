<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <table>
												<tr>
													<td>Nomor SO</td>
													<td>&nbsp;&nbsp;:</td>
													<td>&nbsp;&nbsp;<?= $kodeSO = $arrsales_member['sales_code'] ?></td>
												</tr>
												<tr>
													<td>Seller</td>
													<td>&nbsp;&nbsp;:</td>
													<td>&nbsp;&nbsp;<?= $arrsales_member['nama'].' ('.$arrsales_member['kode'].')' ?></td>
												</tr>
											</table>                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_detail'); ?>" method="Post">
						<?= input_hidden('id', $id) ?>
						<?= input_hidden('account_id', @$arrseller['account_id']) ?>
						<?= input_hidden('tanggal', $arrseller['sales_date']) ?>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id="x"> 
								<?php if(@count(@$arrsales_detail) > 0){ 
									$i = 0;
									foreach($arrsales_detail as $indexx => $valuex){
									
								?>
									<tr id="<?= $i; ?>">
										
										<td><select name="input2[<?= $i; ?>][product_id]" class="PilihBarang form-control md-static" required><option></option>
								<?php
									foreach($arrproduct as $indexl => $valuel){
											echo '<option value="'.$valuel['id_produk'].'" '.(($valuex['product_id'] == $valuel['id_produk'])?'selected':'').'>'.$valuel['nama_produk'].' ('.$valuel['klasifikasi'].') (stock: '.$valuel['jumlah'].')</option>';
											//echo '<option value="'.$valuel['id_phl'].'" '.(($valuex['employee_id'] == $valuel['id_phl'])?'selected':'').'>'.$valuel['nama_phl'].'</option>';
								?>
								<?php	
										} ?>
									<td><input type="text" class="form-control numeric" placeholder="Jumlah" name="input2[<?= $i ?>][sales_detail_quantity]" required value="<?= $valuex['sales_detail_quantity']; ?>"></td>
									<td><input type="text" class="form-control numeric" placeholder="Harga" name="input2[<?= $i ?>][sales_detail_price]" required value="<?= $valuex['sales_detail_price']; ?>"></td>
																				
										<td><button onclick="myDeleteFunction('<?= $i ?>')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									<?php $i++;
									}
									
								}else{ ?>
									<tr id="0">
										<td><select name="input2[0][product_id]" class="PilihBarang form-control md-static" required><option></option><?= $product_list ?></select></td>
										<td><input type="text" id="cJumlah" name="input2[0][sales_detail_quantity]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static numeric"></td>
										<td><input type="text" id="cJumlah" name="input2[0][sales_detail_price]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static numeric"></td>										
										<td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									</tr> 
								<?php } ?>
                            </tbody>    
                         </table> 
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
					<div class="kt-portlet__foot" style="margin-top: 20px;">
            <div class="kt-form__actions">
              <button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;">
                <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
              </button>
            </div>
          </div>
                </form>


            </div>
        </div>
    </div>
</div>


<script>
it = <?= ((@count(@$arrsales_detail) > 0)?count(@$arrsales_detail):1); ?>;
function myCreateFunction() {
	it++;
	var html = '<tr id="'+it+'"><td><select name="input2['+it+'][product_id]" class="PilihBarang form-control md-static" required><option></option><?= $product_list ?></select></td><td><input type="text" id="cJumlah" name="input2['+it+'][sales_detail_quantity]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static numeric"></td><td><input type="text" id="cJumlah" name="input2['+it+'][sales_detail_price]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control numeric static"></td><td><button onclick="myDeleteFunction('+it+')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
	$('#x').append(html);
	$('.PilihBarang').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Barang',
	});
	$(".numeric").mask("#,##0", {reverse: true});
}	
function myDeleteFunction(id) {
	$('#'+id).remove();
}

</script>