<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											Seller : <?= $arrmember['nama'].' ('.$arrmember['kode'].')' ?>
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
										<thead>
											<tr>
											<th>No</th>
											<th>Kode</th>
											<th>Nama Produk</th>
											<th>Jumlah Pesanan</th> 
											<th>Boleh Dikirim</th> 
											<th>Pending</th>  
											<th>Terkirim</th>  
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 0;
											foreach ($arrproducts as $key => $vaData) {
											?>
											<tr>
												<td><?= ++$no ?></td>
												<td><?= $vaData['kode'] ?></td>
												<td><?= $vaData['nama_produk']  ?></td>
												<td><?= number_format($vaData['Jumlah Pesanan']) ?></td>
												<td><?= number_format($vaData['Boleh Dikirim']) ?></td>
												<td><?= number_format($vaData['Pending']) ?></td>
												<td><?= number_format($vaData['Terkirim']) ?></td>
											</tr>
											<?php } ?>
								
										</tbody>
									</table>
								</div>
							</div> 
							