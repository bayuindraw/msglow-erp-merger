<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	public function __construct(){
		
        parent::__construct();
		      
        $this->load->model('model');
        $this->load->model('relasi');
        $this->load->library('session');
        $this->load->database();
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download'); 
		 
    }
		public  function Date2String($dTgl){
			//return 2012-11-22
			list($cDate,$cMount,$cYear)	= explode("-",$dTgl) ;
			if(strlen($cDate) == 2){
				$dTgl	= $cYear . "-" . $cMount . "-" . $cDate ;
			}
			return $dTgl ; 
		}
		
		public  function String2Date($dTgl){
			//return 22-11-2012  
			list($cYear,$cMount,$cDate)	= explode("-",$dTgl) ;
			if(strlen($cYear) == 4){
				$dTgl	= $cDate . "-" . $cMount . "-" . $cYear ;
			} 
			return $dTgl ; 	
		}
		
		public function TimeStamp() {
			date_default_timezone_set("Asia/Jakarta");
			$Data = date("H:i:s");
			return $Data ;
		}
			
		public function DateStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("d-m-Y");
			return $Data ;
		}  
			
		public function DateTimeStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("Y-m-d h:i:s");
			return $Data ;
		} 


		public function index($Aksi=""){
			
			$dataHeader['title']		= "Ms Slim By Ms Glow | Official Register By  Ms Glow";
			$dataHeader['menu']   		= 'Master';
			$dataHeader['file']   		= 'Dashboard' ;
			$dataHeader['link']   		= 'index' ;

			$data['action'] 			= $Aksi ;
			
			$this->load->view('front/container/header',$dataHeader);
			$this->load->view('front/dashboard');
			$this->load->view('front/container/footer');
		}

		public function list_seller($Aksi=""){
			
			$datacontent['title']		= "Ms Slim By Ms Glow | Official Register By  Ms Glow";
			$datacontent['menu']   		= 'Master';
			$datacontent['file']   		= 'Data Seller' ;
			$datacontent['link']   		= 'List Data Seller' ;
			$datacontent['title'] = 'Data Seller';
			$datacontent['action'] = $Aksi;
			$datacontent['seller'] = $this->model->code("SELECT * FROM member LIMIT 1000");
			$data['file'] = 'Master Seller';	
			$data['content'] = $this->load->view('front/list-seller', $datacontent, TRUE);
			$data['title'] = $datacontent['title']; 
			$this->load->view('Layout/home',$data);
		}

		public function edit_seller($Aksi=""){
			
			$datacontent['title']		= "Ms Slim By Ms Glow | Official Register By  Ms Glow";
			$datacontent['menu']   		= 'Master';
			$datacontent['file']   		= 'Data Seller' ;
			$datacontent['link']   		= 'List Data Seller' ;

			$datacontent['action'] 			= $Aksi ;
			$datacontent['row']				= $this->model->ViewWhere('member','kode',$Aksi);
			$datacontent['penerima']			= $this->model->ViewWhere('member_alamat','kode',$Aksi);
			$data['file'] = 'Master Seller';	
			$data['content'] = $this->load->view('front/edit-seller', $datacontent, TRUE);
			$data['title'] = $datacontent['title']; 
			$this->load->view('Layout/home',$data);
		}

		public function AlamatKirim($Type="",$id=""){
			

				$data = array (
				'kode' 				=> $this->input->post('cMember'),
				'nama_terima' 		=> $this->input->post('cNamaKirim'),
				'telepon' 			=> $this->input->post('cTelepKirim'),
				'alamat' 			=> $this->input->post('cAlamatKirim'),
				);

				$this->model->Insert('member_alamat',$data);
				redirect(site_url('Penjualan/edit_seller/'.$this->input->post('cMember')));

		}
		
		public function generate_code(){

		 $cFormatTahun  = substr(date('Y'),2,2);
	     $cFormatBulan  = date('m');
	     $cBsdpnp 		= "MSGLOWSALES-";
	     $dbDate	=	$this->db->query("SELECT COUNT(sales_id) as JumlahTransaksi FROM t_sales WHERE LEFT(sales_date, 7) = '".date('Y-m')."'");
	     if($dbDate->num_rows() > 0){
	     	foreach ($dbDate->result_array() as $key => $vaDbDate) {
	     		$nJumlahTransaksi = $vaDbDate['JumlahTransaksi']+1;
	     	}
	     }else{
	     		$nJumlahTransaksi = 1;
	     }
	     $panjang = strlen($nJumlahTransaksi);
	     if($panjang == 1){
	     	$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'0000'.$nJumlahTransaksi;
	     }elseif($panjang == 2){
	     	$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'000'.$nJumlahTransaksi;
	     }elseif($panjang == 3){
	     	$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'00'.$nJumlahTransaksi;
		 }elseif($panjang == 4){
	     	$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'0'.$nJumlahTransaksi;
		 }elseif($panjang == 5){
	     	$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.$nJumlahTransaksi;
	     }
	     return $cKode;     
		}

		public function penjualan($Aksi=""){
			
			$dataHeader['title']		= "Ms Slim By Ms Glow | Official Register By  Ms Glow";
			$dataHeader['menu']   		= 'Master';
			$dataHeader['file']   		= 'Data Penjualan' ;
			$dataHeader['link']   		= 'Penjualan Form' ;
			$data['kodejual']			= $this->generate_code();
			$data['action'] 			= $Aksi ;
			$data['seller']				= $this->model->code("SELECT * FROM member LIMIT 1000");
				
			$this->load->view('front/container/header',$dataHeader);
			$this->load->view('front/penjualan',$data);
			$this->load->view('front/container/footer');
		}

	
	
		
}
