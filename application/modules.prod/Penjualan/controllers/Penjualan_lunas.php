<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Penjualan_lunas extends CI_Controller
{
	
	var $url_ = "Penjualan/Penjualan_lunas";
	var $id_ = "sales_id";
	var $eng_ = "Penjualan Utang";
	var $ind_ = "Penjualan Utang";
	
	public function __construct()
	{
		parent::__construct();
		ob_start();
		$this->load->library('m_pdf');
		$this->load->model('model');

		$this->load->model(array(
			'Penjualan_lunasModel'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}
	
	public function search_member($search = '')
	{
		$this->Model->search_member($search);
	}
	
	public function index()
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data '.$this->ind_;
		$datacontent['parameter'] 	= $parameter;
		$datacontent['id']		 	= $id;
		$datacontent['code'] 		= $this->generate_code();
		// $datacontent['arrseller'] 	= $this->Model->get_seller();
		$data['file'] 				= $this->ind_ ;
		$data['content'] 			= $this->load->view('Penjualan/form_penjualan_lunas', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}

	public function generate_code(){

		$cFormatTahun  = substr(date('Y'),2,2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWSO-";
		$dbDate	=	$this->db->query("SELECT COUNT(sales_id) as JumlahTransaksi FROM t_sales WHERE LEFT(sales_date_create, 7) = '".date('Y-m')."'");
		if($dbDate->num_rows() > 0){
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi']+2;
			}
		}else{
				$nJumlahTransaksi = 1;
		}
		$panjang = strlen($nJumlahTransaksi);
		if($panjang == 1){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'0000'.$nJumlahTransaksi;
		}elseif($panjang == 2){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'000'.$nJumlahTransaksi;
		}elseif($panjang == 3){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'00'.$nJumlahTransaksi;
		}elseif($panjang == 4){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'0'.$nJumlahTransaksi;
		}elseif($panjang == 5){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.$nJumlahTransaksi;
		}
		return $cKode;     
   }

   public function simpan()
   {
	   redirect('Penjualan/Penjualan_lunas/list');
   }

   public function list()
   {
	$datacontent['url'] 		= $this->url_;
	$datacontent['title'] 		= 'Data '.$this->ind_;
	$datacontent['parameter'] 	= $parameter;
	$datacontent['id']		 	= $id;
	$datacontent['code'] 		= $this->generate_code();
	// $datacontent['arrseller'] 	= $this->Model->get_seller();
	$data['file'] 				= $this->ind_ ;
	$data['content'] 			= $this->load->view('Penjualan/table_penjualan_lunas', $datacontent, TRUE);
	$data['title'] 				= $datacontent['title'];
	$this->load->view('Layout/home',$data);
   }

   public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	
}
