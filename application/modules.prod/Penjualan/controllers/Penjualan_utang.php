<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Penjualan_utang extends CI_Controller
{

	var $url_ = "Penjualan/Penjualan_utang";
	var $id_ = "sales_id";
	var $eng_ = "Penjualan Utang";
	var $ind_ = "Penjualan Utang";

	public function __construct()
	{
		parent::__construct();
		ob_start();
		$this->load->library('m_pdf');
		$this->load->model('model');

		$this->load->model(array(
			'Penjualan_utangModel'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function search_member($search = '')
	{
		$this->Model->search_member($search);
	}

	public function index()
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['parameter'] 	= $parameter;
		$datacontent['id']		 	= $id;
		$datacontent['code'] 		= $this->generate_code();
		// $datacontent['arrseller'] 	= $this->Model->get_seller();
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view('Penjualan/form_penjualan_utang', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function generate_code()
	{

		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWSO-";
		$dbDate	=	$this->db->query("SELECT (MAX(RIGHT(sales_code, 5)) + 1) as JumlahTransaksi FROM t_sales WHERE LEFT(sales_date_create, 7) = '" . date('Y-m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 2;
			}
		} else {
			$nJumlahTransaksi = 1;
		}
		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0000' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '000' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 4) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 5) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}
		return $cKode;
	}

	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$data['user_id'] = $this->session->userdata('user_id');
			$data['sales_date_create'] = date('Y-m-d H:i:s');
			$data['sales_category_id'] = $_SESSION['sales_category_id'];
			//if($data['sales_address'] == "") $data['sales_address'] = $this->Model->get_seller_address($data['seller_id']); 
			$this->Model->chk_account($data['seller_id']);
			$data['sales_type'] = 1;
			$exec = $this->Model->insert($data);
			$id = $this->Model->insert_id();
		}
		redirect(site_url('Penjualan/Penjualan_utang/form_detail/' . $id));
	}

	public function form_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		$datacontent['arrseller'] = $this->Model->get_seller($id);
		$datacontent['limit'] = $this->db->query("SELECT SUM(C.account_detail_sales_product_allow) as minim, C.product_id FROM t_account_detail_sales A LEFT JOIN t_account_detail B ON B.account_detail_real_id = A.account_detail_real_id JOIN t_account_detail_sales_product C ON C.account_detail_sales_id = A.account_detail_sales_id WHERE A.sales_id = '$id' AND B.coa_transaction_confirm = 1 GROUP BY C.product_id")->result_array();
		$datacontent['arrproduct'] = $this->Model->get_produk();
		$datacontent['product_list'] = "";
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
			//$datacontent['product_list'] = $datacontent['product_list'].'<option value="'.$value['id_produk'].'">'.$value['nama_produk'].' ('.(($value['tipe']=="1")?"Barang Jadi":"Gudang").')</option>';
		}
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view('Penjualan/form_detail_penjualan_utang', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function list()
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['parameter'] 	= $parameter;
		$datacontent['id']		 	= $id;
		$datacontent['code'] 		= $this->generate_code();
		// $datacontent['arrseller'] 	= $this->Model->get_seller();
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view('Penjualan/table_penjualan_utang', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	public function simpan_detail()
	{
		$this->db->trans_begin();
		//$data = $this->input->post('input');
		$data2 = $this->input->post('input2');
		$id = $_POST['id'];

		$pending = $this->Model->reset_detail($_POST['id']);
		foreach ($data2 as $index => $value) {
			$value['sales_detail_price'] = str_replace(',', '', $value['sales_detail_price']);
			$value['sales_detail_quantity'] = str_replace(',', '', $value['sales_detail_quantity']);
			$data = $value;
			$data['sales_id'] = $_POST['id'];
			$data['user_id'] = $this->session->userdata('user_id');
			$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
			if (@$pending[$value['product_id']]['sales_detail_id'] != "") {
				if ($pending[$value['product_id']]['sales_detail_quantity_send'] < $value['sales_detail_quantity']) {
					$exec = $this->Model->update_detail_quantity($pending[$value['product_id']]['sales_detail_id'], $value['sales_detail_quantity']);
				}
				$dataxx['sales_detail_price'] = $value['sales_detail_price'];
				$this->Model->update_detail2($dataxx, ['sales_detail_id' => $pending[$value['product_id']]['sales_detail_id']]);
			} else {
				$arrprod = $this->db->query("SELECT * FROM produk WHERE id_produk = '" . $data['product_id'] . "'")->row_array();
				$ecc = $this->Model->insert_detail($data);
				$arrdetail_kode[$arrprod['kd_pd']][$ecc] = $ecc;
			}
			$this->Model->check_fifo_alpha($value['product_id']);
			$this->Model->check_fifo_beta($value['product_id']);
			$this->Model->check_fifo_main($value['product_id']);
			$this->Model->set_sales_detail();
		}
		$total_price = $this->Model->get_sales_detail_total_price($id);
		$chk_account_detail = $this->Model->get_account_detail($id);
		if ($chk_account_detail['account_id'] != "") {
			$arrsales = $this->Model->get_sales($id);
			$account_id = $this->input->post('account_id');
			$id2 = $this->Model->get_max_id();

			$this->db->where('account_id', $account_id);
			$row = $this->Model->get_m_account($account_id)->row_array();
			$type = "debit";
			if (@$row['account_date_reset'] > $chk_account_detail['account_detail_date']) {
				$this->Model->reset_balance($account_id, (int)$chk_account_detail['account_detail_' . $type], $type, @$row['account_type_id'], $chk_account_detail['account_detail_date']);
			} else {
				$this->Model->reset_balance($account_id, (int)$chk_account_detail['account_detail_' . $type], $type);
			}
			//$this->Model->delete_account_detail($chk_account_detail['account_detail_real_id']);

			//$datax['account_id'] = $account_id;
			//$datax['account_detail_id'] = $id2;
			//$datax['account_detail_type_id'] = 3;
			//$datax['account_detail_user_id'] = $_SESSION['user_id'];
			//$datax['account_detail_pic'] = $_SESSION['user_fullname'];
			$datax['account_detail_date'] = $arrsales['sales_date'];
			//$datax['account_detail_category_id'] = '81';
			//$datax['account_detail_realization'] = 1;
			//$datax['account_detail_quantity'] = ;
			//$datax['sales_id'] = $id;
			$datax['account_detail_price'] = $total_price;
			//$datax['account_detail_note'] = 'Pembelian dengan sales order : '.$arrsales['sales_code'];
			//$type = "debit";
			$datax['account_detail_debit'] = $total_price;
			//$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
			$id_account_detail = $this->Model->update_account_detail($datax, ['account_detail_real_id' => $chk_account_detail['account_detail_real_id']]);

			if (@$row['account_date_reset'] > $arrsales['sales_date']) {
				$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
			} else {
				$this->Model->update_balance($account_id,  $total_price, $type);
			}
			//$data['account_detail_real_id'] = $id_account_detail;
		} else {
			$arrsales = $this->Model->get_sales($id);
			$account_id = $this->input->post('account_id');
			$id2 = $this->Model->get_max_id();
			$datax['account_id'] = $account_id;
			$datax['account_detail_id'] = $id2;
			$datax['account_detail_type_id'] = 3;
			$datax['account_detail_user_id'] = $_SESSION['user_id'];
			$datax['account_detail_pic'] = $_SESSION['user_fullname'];
			$datax['account_detail_date'] = $arrsales['sales_date'];
			$datax['account_detail_category_id'] = '81';
			$datax['account_detail_realization'] = 1;
			//$datax['account_detail_quantity'] = ;
			$datax['sales_id'] = $id;
			$datax['account_detail_price'] = $total_price;
			$datax['account_detail_note'] = 'Pembelian dengan sales order : ' . $arrsales['sales_code'];
			$type = "debit";
			$datax['account_detail_debit'] = $total_price;
			$datax['account_detail_date_create'] = date('Y-m-d H:i:s');
			$datax['account_detail_paid_type'] = 2;
			$id_account_detail = $this->Model->insert_account_detail($datax);
			// $dataxx = ([
			// 	'account_detail_real_id' => $id_account_detail,
			// 	'sales_id' => $id,
			// 	'account_detail_target_real_id' => $id_account_detail,
			// 	'account_detail_sales_amount' => $total_price,
			// 	'account_detail_sales_date' => date('Y-m-d'),
			// 	'account_detail_id' => $id2,
			// ]);
			// $id_account_detail_sales = $this->Model->insert_account_detail_sales($dataxx);
			foreach ($_POST['input2'] as $input2) {
				$datay = ([
					'sales_id' => $id,
					'product_id' => $input2['product_id'],
					'sales_detail_quantity' => str_replace(",", "", $input2['sales_detail_quantity']),
					'user_id' => $_SESSION['user_id'],
					'sales_detail_date_create' => date('Y-m-d H:i:s'),
					'sales_detail_approve_user_id' => $_SESSION['user_id'],
					'sales_detail_approve_date' => date('Y-m-d H:i:s'),
					'sales_detail_price' => str_replace(',', '', $input2['sales_detail_price']),
					'account_detail_real_id' => $id_account_detail,
					'pending_quantity' => str_replace(",", "", $input2['sales_detail_quantity']),
				]);
				$this->db->update('t_sales_detail', $datay, ['sales_id' => $id, 'product_id' => $input2['product_id']]);
				$sales_detail = $this->db->get_where('t_sales_detail', ['sales_id' => $id, 'product_id' => $input2['product_id']])->row_array();
				$id_sales_detail = $sales_detail['sales_detail_id'];
				$dataxxx = ([
					'account_detail_real_id' => $id_account_detail,
					'sales_id' => $id,
					'product_id' => $input2['product_id'],
					'account_detail_sales_product_allow' => str_replace(",", "", $input2['sales_detail_quantity']),
					// 'account_detail_sales_id' => $id_account_detail_sales,
					'sales_detail_id' => $id_sales_detail,
					'account_detail_id' => $id_account_detail,
				]);
				$this->db->insert('t_account_detail_sales_product', $dataxxx);
			}
			$this->db->where('account_id', $account_id);
			$row = $this->Model->get_m_account($account_id)->row_array();
			if (@$row['account_date_reset'] > $arrsales['sales_date']) {
				$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
			} else {
				$this->Model->update_balance($account_id,  $total_price, $type);
			}
			$data['account_detail_real_id'] = $id_account_detail;
		}
		if (isset($_POST['discount_id'])) {
			$discounts = array();
			foreach ($_POST['discount_id'] as $discount_id) {
				$discounts[$discount_id] = $discount_id;
			}
			foreach ($discounts as $discount_id) {
				$discount = $this->db->query("SELECT sum(B.discount_reward_product_deduction_cost) as discount_reward_product_deduction_cost, SUM(B.discount_reward_product_percentage) as discount_reward_product_percentage, SUM(B.discount_reward_product_quantity) as discount_reward_product_quantity FROM m_discount_reward A JOIN m_discount_reward_product B ON A.discount_reward_id = B.discount_reward_id WHERE A.discount_id = $discount_id GROUP BY A.discount_id")->row_array();
				$total_price -= $discount['discount_reward_product_deduction_cost'];
				$total_price -= ($discount['discount_reward_product_percentage'] / 100 * $total_price);
				$salesDisc = ([
					'sales_id' => $id,
					'discount_id' => $discount_id,
					'sales_discount_date' => $arrsales['sales_date'],
					'sales_discount_create_date' => date('Y-m-d H:i:s'),
					'sales_discount_create_user_id' => $_SESSION['user_id'],
					'sales_discount_percentage_active' => 0,
					'sales_discount_price' => $discount['discount_reward_product_deduction_cost'],
					'sales_discount_percentage' => $discount['discount_reward_product_percentage'],
					'sales_discount_price_total_accumulated' => 0,
					'sales_discount_product_price_target' => $total_price,
				]);
				if ($discount['discount_reward_product_quantity'] > 0 && $discount['discount_reward_product_quantity'] !== '') {
					$salesDisc['sales_discount_product'] = 1;
				} else {
					$salesDisc['sales_discount_product'] = 0;
				}
				$this->db->insert('t_sales_discount', $salesDisc);
				$salesDisc['sales_discount_id'] = $this->db->insert_id();
				if ($discount['discount_reward_product_quantity'] > 0 && $discount['discount_reward_product_quantity'] !== '') {
					$discountProds = $this->db->query("SELECT * FROM m_discount_reward A JOIN m_discount_reward_product B ON A.discount_reward_id = B.discount_reward_id AND B.product_kd IS NOT NULL WHERE A.discount_id = $discount_id")->result_array();
					foreach ($discountProds as $discountProd) {
						$produk = $this->db->get_where('produk', ['kd_pd' => $discountProd['product_kd']])->row_array();
						$price = $this->db->query("SELECT * FROM m_price WHERE produk_global_kd = '$discountProd[product_kd]' ORDER BY price_unit_price ASC LIMIT 1")->row_array();
						$sales_detail = ([
							'product_id' => $produk['id_produk'],
							'sales_detail_quantity' => $_POST['promo_product'][$discountProd['product_kd']],
							'sales_detail_price' => 0,
							'sales_id' => $_POST['id'],
							'user_id' => $_SESSION['user_id'],
							'sales_detail_date_create' => date('Y-m-d H:i:s'),
							'sales_detail_discount' => 1,
						]);
						$this->Model->insert_detail($sales_detail);
						$sales_detail['sales_detail_id'] = $this->db->insert_id();

						$salesDiscProd = ([
							'product_kd' => $discountProd['product_kd'],
							'sales_discount_product_quantity' => $discountProd['discount_reward_product_quantity'],
							'sales_discount_product_accumulated' => 0,
							'sales_discount_product_target' => $_POST['promo_product'][$discountProd['product_kd']],
							'sales_discount_id' => $salesDisc['sales_discount_id'],
							'discount_reward_product_id' => $discountProd['discount_reward_product_id'],
							'sales_discount_product_draft' => 0,
							'sales_detail_id' => $sales_detail['sales_detail_id'],
							'sales_discount_product_price' => $price['price_unit_price'],
						]);
						$this->db->insert('t_sales_discount_product', $salesDiscProd);
						$datax = ([
							// 'account_detail_id' => $id,
							// 'account_detail_real_id' => $_POST['real_id'][$index][$index2],
							'sales_id' => $_POST['id'],
							'sales_detail_id' => $sales_detail['sales_detail_id'],
							'product_id' => $produk['id_produk'],
							'account_detail_sales_product_allow' => $_POST['promo_product'][$discountProd['product_kd']],
							// 'account_detail_sales_id' => $ids[$index][$index2],
							// 'region_id' => $_POST['input2']['kecamatan'],
							// 'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
							// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
							// 'account_detail_sales_product_address' => $_POST['input2']['full_address'],
							// 'account_detail_sales_product_delivery_cost' => '',
							'account_detail_sales_product_discount' => $price['price_unit_price'],
							'account_detail_sales_product_discount_total' => ($price['price_unit_price'] *  $_POST['promo_product'][$discountProd['product_kd']]),
							// 'sales_discount_terms_id' => $sales_discount_terms['sales_discount_terms_id']
						]);
						$this->db->insert("t_account_detail_sales_product", $datax);
						$discountx = $this->db->get_where('t_discount_total', ['account_detail_id' => $datax['account_detail_id']])->row_array();
						if ($discountx['discount_total_price'] == null) $discountx['discount_total_price'] = 0;
						$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
						$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow], discount_price = discount_price + $discountx[discount_total_price] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
						$this->Model->check_fifo_alpha($datax['product_id']);
						$this->Model->check_fifo_beta($datax['product_id']);
						$this->Model->check_fifo_main($datax['product_id']);
						$this->Model->set_sales_detail();
						$discountx = '';
					}
				}
				$discTerms = $this->db->query("SELECT * FROM m_discount_terms A JOIN m_discount_terms_product B ON A.discount_terms_id = B.discount_terms_id WHERE A.discount_id = $discount_id")->result_array();
				foreach ($discTerms as $discTerm) {
					$salesDiscTerm = ([
						'sales_discount_id' => $salesDisc['sales_discount_id'],
						'product_kd' => $discTerm['product_kd'],
						'sales_discount_terms_quantity' => $discTerm['discount_terms_product_quantity'],
						'discount_terms_product_id' => $discTerm['discount_terms_product_id'],
						'sales_discount_terms_type' => $discTerm['discount_terms_product_type'],
						'sales_discount_terms_price' => $discTerm['discount_terms_product_price'],
					]);
					$this->db->insert('t_sales_discount_terms', $salesDiscTerm);
					$sales_discount_terms_id = $this->db->insert_id();
					foreach ($arrdetail_kode[$discTerm['product_kd']] as $index_terms => $value_terms) {
						$data = ([
							'sales_discount_id' => $salesDisc['sales_discount_id'],
							'sales_detail_id' => $value_terms,
							'sales_discount_terms_id' => $sales_discount_terms_id,
							'sales_id' => $_POST['id'],
						]);
						$this->db->insert('t_sales_discount_detail', $data);
					}
				}
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('Penjualan/Penjualan_utang/table_detail/' . $id));
	}

	public function table_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail2($id);
		$datacontent['deposit'] 	= $this->Model->get_deposit($id);
		$datacontent['arraccount_detail_sales'] = $this->Model->get_account_detail_sales($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view('Penjualan/table_detail_penjualan_utang', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
}
