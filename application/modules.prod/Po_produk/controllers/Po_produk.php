<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Po_produk extends CI_Controller
{

	var $url_ = "Po_produk";
	var $id_ = "id_po_produks";
	var $eng_ = "package";
	var $ind_ = "PO Produk";
	var $url2_ = "Administrator/Stock_Produk";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['url2'] = $this->url2_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function index2()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['url2'] = $this->url2_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data2()
	{
		$datacontent['datatable'] = $this->Model->get_data2();
	}

	public function get_data_sales()
	{
		$datacontent['datatable'] = $this->Model->get_data_sales();
	}

	public function table_sales()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Penjualan';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_sales', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function generate_code()
	{

		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWPACKAGE-";
		$dbDate	=	$this->db->query("SELECT COUNT(package_id) as JumlahTransaksi FROM t_package WHERE LEFT(package_date, 7) = '" . date('Y-m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 1;
			}
		} else {
			$nJumlahTransaksi = 1;
		}
		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0000' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '000' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 4) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 5) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}
		return $cKode;
	}

	public function get_data_postage()
	{
		$datacontent['datatable'] = $this->Model->get_data_postage();
	}

	public function table_postage()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Ongkir';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_postage', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_postage($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Ongkir' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		$datacontent['arrpackage'] = $this->Model->get_package($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_postage', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail($id);
		$datacontent['arrpackage_detail'] = $this->Model->get_package_detail($id);
		$datacontent['arrpackage_detail_date'] = $this->Model->get_package_detail_date($id);
		$datacontent['arrpackage_detail_sum'] = $this->Model->get_package_detail_sum($id);

		$datacontent['product_list'] = "";
		foreach ($datacontent['arrsales_detail'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$datacontent['arrsales'] = $this->Model->get_sales($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail($id = '')
	{

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrproduct'] = $this->Model->get_product();
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {

			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$data['user_id'] = $this->session->userdata('user_id');
				$data[$this->eng_ . '_date_create'] = date('Y-m-d H:i:s');

				if ($data['package_postage'] != "" && $data['package_postage'] != "0") {
					$account_id = $this->Model->get_account_id($_POST['seller_id']);
					$id2 = $this->Model->get_max_id();
					$datax['account_id'] = $account_id;
					$datax['account_detail_id'] = $id2;
					$datax['account_detail_type_id'] = 3;
					$datax['account_detail_user_id'] = $_SESSION['user_id'];
					$datax['account_detail_pic'] = $_SESSION['user_fullname'];
					$datax['account_detail_date'] = $data['package_date'];
					$datax['account_detail_category_id'] = '82';
					$datax['account_detail_realization'] = 1;
					$datax['account_detail_price'] = $data['package_postage'];
					$datax['account_detail_note'] = 'Ongkos Kirim ' . $data['package_code'];
					$type = "debit";
					$datax['account_detail_debit'] = $data['package_postage'];
					$datax['account_detail_date_create'] = date('Y-m-d H:i:s');
					$id_account_detail = $this->Model->insert_account_detail($datax);
					$this->db->where('account_id', $account_id);
					$row = $this->Model->get_m_account()->row_array();
					if (@$row['account_date_reset'] > $_POST['package_date']) {
						$this->Model->update_balance($account_id,  $data['package_postage'], $type, @$row['account_type_id'], $datax['account_detail_date']);
					} else {
						$this->Model->update_balance($account_id,  $data['package_postage'], $type);
					}
					$data['account_detail_real_id'] = $id_account_detail;
				}
				$exec = $this->Model->insert($data);
				$id = $this->Model->insert_id();
				$data2 = $this->input->post('input2');
				foreach ($data2 as $key => $value) {
					$value['package_id'] = $id;
					$exec = $this->Model->insert_detail($value);

					$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_package/form/' . $data['sales_id']));
	}

	public function simpan_detail()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$data['sales_id'] = $_POST['id'];
			$data['user_id'] = $this->session->userdata('user_id');
			$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
			$exec = $this->Model->insert_detail($data);
		}
		redirect(site_url('T_sales/form_detail/' . $_POST['id']));
	}

	public function simpan_postage()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$id = $this->input->post('id');
			$data = $this->input->post('input');
			if (@$data['package_delivery_method'] == "") {
				$arrdata = $this->Model->get_package_all($id);
				$account_id = $arrdata['account_id'];
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrdata['package_date'];
				$datax['account_detail_category_id'] = '82';
				$datax['account_detail_realization'] = 1;
				$datax['account_detail_price'] = $data['package_postage'];
				$datax['account_detail_note'] = 'Ongkos Kirim ' . $data['package_code'];
				$type = "debit";
				$datax['account_detail_debit'] = $data['package_postage'];
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');
				$id_account_detail = $this->Model->insert_account_detail($datax);
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account()->row_array();
				if (@$row['account_date_reset'] > $arrdata['package_date']) {
					$this->Model->update_balance($account_id,  $data['package_postage'], $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $data['package_postage'], $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			}
			$exec = $this->Model->update($data, [$this->id_ => $this->input->post('id')]);;
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_sales/table_postage'));
	}

	public function simpan_detail_all()
	{
		redirect(site_url('T_sales'));
	}

	public function report()
	{
		if (@$_POST['export_pdf']) {
			$this->export_pdf();
		} else {
			$datacontent['url'] = $this->url_;
			$datacontent['search'] = $this->input->post('search');
			$datacontent['url2'] = '/' . $datacontent['search']['member_status'] . '|' . $datacontent['search']['all'];
			$datacontent['title'] = 'Member';
			$data['file'] = $this->ind_;
			$data['content'] = $this->load->view($this->url_ . '/table_report', $datacontent, TRUE);
			$data['title'] = $datacontent['title'];
			$this->load->view('Layout/home', $data);
		}
	}

	public function export_pdf()
	{
		ob_start();
		$datacontent['data'] = $this->Model->get_report()->result_array();
		$this->load->view('M_member/export_pdf', $datacontent);
		//$data['content'] = $this->load->view('m_member/table_report', $datacontent);
		//$data['siswa'] = $this->siswa_model->view_row();
		//$this->load->view('print', $data);
		$html = ob_get_contents();
		ob_end_clean();

		require './assets/html2pdf/autoload.php';

		$pdf = new Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
		$pdf->WriteHTML($html);
		$pdf->Output(date('Y-m-d') . '.pdf', 'D');
	}



	public function get_data_report($search = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_report($search);
	}

	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}

	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}

	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}



	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {

				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_credit'] = $from['account_detail_debit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_credit'], 'credit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_send()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function non_active($id = '')
	{
		$data['active'] = 'T';
		$data['active_user'] = $this->session->userdata('user_id');
		$this->Model->update($data, ['kode_po' => $id]);
		redirect($this->url_);
	}

	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}

	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();


		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();

		if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
		} else {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
		}



		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_ . "/$id"));
	}

	public function list_kecamatan()
	{
		$id_kota = $this->input->post('id_kota');
		$stasiun = $this->Model->get_list_kecamatan($id_kota);
		$lists = "<option value=''>Pilih Kecamatan</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kecamatan . "'>" . $data->nama_kecamatan . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function list_kota()
	{
		$id_provinsi = $this->input->post('id_provinsi');
		$stasiun = $this->Model->get_list_kota($id_provinsi);
		$lists = "<option value=''>Pilih Kota</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kota . "'>" . $data->nama_kota . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}
}
