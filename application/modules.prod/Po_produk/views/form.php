
<div class="row">
  <div class="col-lg-12 col-xl-12">
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            PENGEMASAN PRODUK KODE PENJUALAN : <?= $arrsales['sales_code'] ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
        <h6>Detail Pembelian Produk : </h6>
        <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Produk</th>
              <th>Order</th>
              <?php
              foreach ($arrpackage_detail_date as $key => $vaTanggal) {

              ?>
                <th><?= ($vaTanggal['package_date']) ?></th>
              <?php } ?>
              <th>Total Kirim</th>
              <th>Kurang Kirim</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 0;
            foreach ($arrsales_detail as $key => $vaData) {
            ?>
              <tr>
                <td><?= ++$no ?></td> 
                <td><?= $vaData['nama_produk'] ?></td>
                <td><?= number_format($vaData['total']) ?> Pcs</td>
                <?php
				$totalKirim = 0;
              foreach ($arrpackage_detail_date as $key => $vaTanggal) {
              ?>
				<td><?= @$arrpackage_detail_sum[$vaTanggal['package_date']][$vaData['id_produk']] ?></td>
              <?php
				$totalKirim = $totalKirim + @$arrpackage_detail_sum[$vaTanggal['package_date']][$vaData['id_produk']];
			  } ?>
                <td><b><?= number_format($totalKirim) ?></b></td>
                <td><b><?= number_format($vaData['total'] - $totalKirim) ?></b></td>
              </tr>
            <?php } ?>

          </tbody>
        </table>
        <hr>
        <h6>Detail Pengiriman Termin Penjualan : <?= $arrsales['sales_code'] ?></h6>
        <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Pengiriman</th>
              <th>Tanggal Pengiriman</th>
              <th>Nama Produk</th>
              <th>Jumlah</th>
              <th>User</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 0;
            foreach ($arrpackage_detail as $key => $vaData) {
            ?>
              <tr>
                <td><?= ++$no ?></td>
                <td><?= $vaData['package_code'] ?></td>
                <td><?= $vaData['package_date'] ?></td>
                <td><?= $vaData['nama_produk'] ?></td>
                <td><?= number_format($vaData['package_detail_quantity']) ?> Pcs</td>
                <td><?= $vaData['user_fullname'] ?></td>
              </tr>
            <?php } ?>

          </tbody>
        </table>
        <hr>
        <h5>Form Pengiriman Produk : <?= $arrsales['sales_code'] ?></h5>
        <hr>
        <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan'); ?>" method="post" novalidate>
          <?= input_hidden('input[sales_id]', $id) ?>
          <?= input_hidden('seller_id', $arrsales['seller_id']) ?>
          <?= input_hidden('parameter', 'tambah') ?>
		  <div class="form-group">
            <label>Kode</label>
            <input type="text" id="cKodePo" name="input[package_code]" class="form-control md-form-control md-static" value="<?= $code ?>">
          </div>
		  <div class="form-group">
            <label>Tanggal Pengemasan</label>
            <input type="text" id="dTglPo" name="input[package_date]" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required> 
          </div>
		  <div class="form-group">
            <label>Ongkos Kirim</label>
            <input type="text" name="input[package_postage]" class="form-control md-form-control md-static" value="0">
          </div>
		 <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id="x">
								
									<tr id="0">
										
										<td><select name="input2[0][product_id]" id="cIdStock" class="form-control md-static" required><option></option><?= $product_list ?></select></td>
										<td><input type="text" class="form-control" placeholder="Jumlah" name="input2[0][package_detail_quantity]" required></td> 
										<td><button onclick="myDeleteFunction('0')"></button></td>
									</tr> 
                            </tbody>
                         </table>
					<button type="button" onclick="myCreateFunction();return false();">Create row</button>
          <!--<div class="form-group">
            <label>Nama Barang</label>
            <select name="NamaBarang" id="cIdStock" class="form-control md-static" required>

              <option></option>
              <?php
              /*$query = $this->model->ViewWhere('v_detail_produk_jual', 'kode_jual', $action);
              foreach ($query as $key => $vaKemasan) {
              ?>
                <option value="<?= $vaKemasan['id_barang'] ?>"><?= $vaKemasan['nama_produk'] ?></option>
              <?php }*/ ?>
            </select>
          </div>-->
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk">
                <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
it = 1;
function myCreateFunction() {
	it++;
	var html = '<tr id="'+it+'"><td><select name="input2['+it+'][product_id]" class="cIdStock form-control md-static" required><option></option><?= $product_list ?></select></td><td><input type="text" class="form-control" placeholder="Jumlah" name="input2['+it+'][package_detail_quantity]" required></td><td><button onclick="myDeleteFunction('+it+')"></button></td></tr>';
	$('#x').append(html);
	$('.cIdStock').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Nama Stock Barang',
	});
}

function myDeleteFunction(id) {
	$('#'+id).remove();
}
</script>

