<?php
if ($parameter == 'ubah' && $id != '') {
	//$this->db->where('A.id_kemasan', $id);
	$row = $this->Model->get($id);
}
?>
<div class="row"
								<div class="col-lg-12 col-xl-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Nama</label>
													<input type="text" class="form-control" placeholder="Nama" name="input[nama_produk]" value="<?= @$row['nama_produk'] ?>" required>
												</div>
												<div class="form-group">
													<label>Kode</label>
													<input type="text" autocomplete="off" id="kode_unik_produk" class="form-control" placeholder="Kode" name="input[kode_produk]" value="<?= @$row['kode_produk'] ?>" required onkeyup="chk_kode_produk();">
												</div>
												<div class="form-group">
													<label>Tipe Produk</label>
													<input id="dataListProduk" name="nama_produk" placeholder="Type to search..." list="datalistOptions" class="form-control md-static" value="<?= @$row['tipe_produk'] ?>" required>
													<datalist id="datalistOptions">
														<?php foreach($produks as $produk){ ?>
															<option data-value="<?= $produk['kode'] ?>"><?= $produk['nama_produk'] ?></option>
														<?php } ?>
													</datalist> 
													<input type="hidden" name="input[kd_pd]" id="dataListProduk-hidden" value="<?= @$row['kd_pd'] ?>">
												</div>
												<div class="form-group" id="kodeTipe"></div>
												<div class="form-group">
													<label>Klasifikasi</label>
													<input type="text" class="form-control" placeholder="Klasifikasi" name="input[klasifikasi]" value="<?= @$row['klasifikasi'] ?>" required>
												</div>
												<?php if($_SESSION['role_id'] == 0 || $_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 2) { ?>
												<div class="form-group">
													<label>Harga</label>
													<input type="text" class="form-control numeric" placeholder="Harga" name="input[harga]" value="<?= @$row['harga'] ?>" required>
												</div>
												<?php } ?>
												<?php if($id == ""){ ?>
												<div class="form-group">
													<label>Tanggal Mulai Stok Awal</label>
													<input type="text" id="cTanggal" name="cTanggal" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= date('Y-m-d'); ?>" placeholder="Pilih Tanggal Penjualan">
													<input type="hidden" id="cIdOutlet" name="NamaOutlet" class="md-form-control md-static floating-label" value="1">
												</div>
												<div class="form-group">
													<label>Stok Awal</label>
													<input type="text" class="form-control" placeholder="Stok Awal" name="stock_awal" value="0" required>
												</div>
												<?php } ?>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
<script>
	$('#PilihProduk').select2({  
	   allowClear: true,
	   placeholder: 'Pilih Produk',
	  });

	document.querySelector('input[list]').addEventListener('input', function(e) {
		var input = e.target,
			list = input.getAttribute('list'),
			options = document.querySelectorAll('#' + list + ' option'),
			hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden'),
			inputValue = input.value;

		hiddenInput.value = "";

		for(var i = 0; i < options.length; i++) {
			var option = options[i];
			console.log(option);

			if(option.innerText === inputValue) {
				hiddenInput.value = option.getAttribute('data-value');
				break;
			}
		}
		if(hiddenInput.value === ""){
			if($("#plus").length) {
				
			}else{
				var html = '<div id="plus"><label>Kode Tipe Produk</label><input type="text" autocomplete="off" class="form-control" placeholder="Kode Tipe Produk" name="kode_produk" id="kode_produk" required onkeyup="this.value=removeSpaces(this.value);chk_kode();"></div><br><div id="plus2"><label>Brand</label><select class="form-control" name="brand_id" id="brand_id" required><?=$brand_list?></select></div>';
				$('#kodeTipe').append(html);
				
			}
		}else{
			$('#plus').remove();
			$('#plus2').remove();
		}
	});
	function removeSpaces(string) {
		return string.split(' ').join('');
	}
	function chk_kode(){
		$.ajax({
			url: '<?= base_url() ?>/Produk/chk_kode/'+$('#kode_produk').val(),
			success: function(result) {
				if(result === 'false'){
					document.getElementById('kode_produk').style.color = 'red'
					document.getElementById("simpan").disabled = true;
					document.getElementById('simpan').style.display = 'none'
				}else{
					document.getElementById('kode_produk').style.color = 'black'
					document.getElementById("simpan").disabled = false;
					document.getElementById('simpan').style.display = 'block'
				}
			}
		});
	}
	function chk_kode_produk(){
		$.ajax({
			url: '<?= base_url() ?>/Produk/chk_kode_produk/'+$('#kode_unik_produk').val(),
			success: function(result) {
				if(result === 'false'){
					document.getElementById('kode_unik_produk').style.color = 'red'
					document.getElementById("simpan").disabled = true;
					document.getElementById('simpan').style.display = 'none'
				}else{
					document.getElementById('kode_unik_produk').style.color = 'black'
					document.getElementById("simpan").disabled = false;
					document.getElementById('simpan').style.display = 'block'
				}
			}
		});
	}
</script>