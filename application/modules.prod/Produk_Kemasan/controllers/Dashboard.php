<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct(){
		
        parent::__construct();
		      
        $this->load->model('model');
        $this->load->model('relasi');
        $this->load->library('session');
        $this->load->database();
        $this->load->helper('url');
		$this->load->helper('form');
		 
	 }
	 
		public function index(){
			$dataHeader['title'] 	= "Index";
			$dataHeader['menu'] 	= '';
			$dataHeader['file'] 	= 'Index' ;
			$dataHeader['link']  	= 'index' ;

			$data['data'] = $this->model->tampil_produk();

			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/container/dashboard',$data);
			$this->load->view('back-end/container/footer');
        }
    }