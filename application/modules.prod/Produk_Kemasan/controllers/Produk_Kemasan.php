<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk_Kemasan extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();

		$this->load->model('model');
		$this->load->model('relasi');
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
	}

	public function index()
	{
		$dataHeader['title'] 	= "Produk & Kemasan";
		$dataHeader['menu'] 	= 'Master';
		$dataHeader['file'] 	= 'Produk & Kemasan';
		$dataHeader['link']  	= 'index';

		if (isset($_GET['dari_tanggal']) && isset($_GET['sampai_tanggal'])) {
			$data['data'] = $this->model->tampil_produk($_GET['dari_tanggal'], $_GET['sampai_tanggal']);
		} else {
			$data['data'] = $this->model->tampil_produk(null, null);
		}

		$data['file'] 	= $dataHeader['file'];
		$dataHeader['content'] = $this->load->view('back-end/master/home', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);

		// $this->load->view('back-end/container/header',$dataHeader);
		// $this->load->view('back-end/master/home',$data);
		// $this->load->view('back-end/container/footer');
	}

	public function export_pdf()
	{
		if (isset($_GET['dari_tanggal']) && isset($_GET['sampai_tanggal'])) {
			$data['data'] = $this->model->tampil_produk($_GET['dari_tanggal'], $_GET['sampai_tanggal']);
		} else {
			$data['data'] = $this->model->tampil_produk(null, null);
		}

		$data['nomor_cetak'] = $this->db->query("SELECT * FROM tb_nomorcetak")->row_array();

		$this->load->view('back-end/master/export_pdf', $data);
	}

	public function tambah()
	{
		$dataHeader['title']		  	= "Tambah Produk & Kemasan";
		$dataHeader['menu']   			= 'Master';
		$dataHeader['file']   			= 'Tambah Produk & Kemasan';


		$data['file'] 	= $dataHeader['file'];
		$dataHeader['content'] = $this->load->view('back-end/master/tambah', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);

		// $this->load->view('back-end/container/header',$dataHeader);
		// $this->load->view('back-end/master/tambah');
		// $this->load->view('back-end/container/footer');
	}

	public function tambah_act()
	{
		$Value = array(
			'nama_barang' => $this->input->post('nama_barang'),
			'qty' => $this->input->post('qty'),
			'supplier' => $this->input->post('supplier'),
			'keterangan' => $this->input->post('keterangan'),
			'tanggal_input' => $this->input->post('tanggal_input')
		);
		$this->model->tambah('tb_produkkemasan', $Value);
		Redirect("Produk_Kemasan");
	}

	public function edit()
	{
		$dataHeader['title']		= "Edit Produk & Kemasan";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Edit Produk & Kemasan';
		$no = $_GET['no'];
		$data['data'] = $this->db->query("SELECT * FROM tb_produkkemasan WHERE no = $no")->row_array();

		$data['file'] 	= $dataHeader['file'];
		$dataHeader['content'] = $this->load->view('back-end/master/edit', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);

		// $this->load->view('back-end/container/header',$dataHeader);
		// $this->load->view('back-end/master/edit',$data);
		// $this->load->view('back-end/container/footer');
	}

	public function edit_act()
	{
		$no = $this->input->post('no');
		$Value = array(
			'nama_barang' => $this->input->post('nama_barang'),
			'qty' => $this->input->post('qty'),
			'supplier' => $this->input->post('supplier'),
			'keterangan' => $this->input->post('keterangan'),
			'tanggal_input' => $this->input->post('tanggal_input')
		);
		$this->model->edit("tb_produkkemasan", "no", $no, $Value);
		Redirect("Produk_Kemasan");
	}

	public function hapus()
	{
		$no = $_GET['no'];
		$this->model->hapus("tb_produkkemasan", "no", $no);
		Redirect("Produk_Kemasan");
	}
}
