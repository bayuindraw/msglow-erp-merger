<?php
if (date_format(date_create($nomor_cetak['tanggal']), "m") != date('m')) {
   $query = $this->db->query("UPDATE `tb_nomorcetak` SET `no`=1,`tanggal`=CURDATE() WHERE id=1");
} else {
   $query = $this->db->query("UPDATE `tb_nomorcetak` SET `no`=`no`+1,`tanggal`=CURDATE() WHERE id=1");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="shortcut icon" href="https://msglow.app/upload/nitrogen.png" />
   <title>Print Produk & Kemasan</title>

   <style>
      table,
      th,
      td {
         border: 1px solid black;
         border-collapse: collapse;
         padding: 5px;
      }

      table {
         width: 100%;
      }

      .right {
         text-align: right;
      }

      td {
         text-align: center;
      }
   </style>
</head>

<body id="ignorePDF">
   <div class="ttd2" style="float:left;margin-top: -10px;">
      <img src="<?php echo base_url('assets/pt.PNG'); ?>" width="200px" height="120px" style="margin: 10px;">
   </div>
   <div class="ttd2" style="float:right;margin-top: -10px;">
      <h2 style="margin: 10px 0px 0px 0px">FORM PERMINTAAN PO SUPPLIER</h2>
      <h3 style="margin: 0px; text-align: right">Tanggal: <?= date('d F Y'); ?></h3>
      <h3 style="margin: 0px; text-align: right">Nomor: <?php echo $nomor_cetak['no']; ?>-PO.SUP/<?php echo date("m"); ?>/<?php echo date("y"); ?></h3>
   </div>
   <table>
      <tr>
         <th>NO</th>
         <th>Nama Barang</th>
         <th>QTY(PCS)</th>
         <th>SUPPLIER</th>
         <th>KETERANGAN</th>
      </tr>
      <tr>
         <?php
         $no = 1;
         foreach ($data as $key) {
         ?>
      <tr>
         <td width="10px"><?= $no++ ?></td>
         <td style="text-align: left;"><?= $key->nama_barang ?></td>
         <td><?= $key->qty ?></td>
         <td><?= $key->supplier ?></td>
         <td><?= $key->keterangan ?></td>
      </tr>
   <?php } ?>
   </tr>
   </table>
   <div class="ttd2" style="float:left; margin-left: 20%;">
      <p><b>
            <center>Dibuat oleh,</center>
         </b></p>
      <br><br><br>
      <p style="font-weight: bold;">_________________</p>
      <p><b>
            <center>Rizhaldi Adhitya</center>
         </b></p>
      <p><i>
            <center>SPV Kemasan</center>
         </i></p>
   </div>
   <div class="ttd2" style="float:right; margin-right: 20%;">
      <p><b>
            <center>Disetujui oleh,</center>
         </b></p>
      <br><br><br>
      <p style="font-weight: bold;">_________________</p>
      <p><b>
            <center>Chandra Mahardhika</center>
         </b></p>
      <p><i>
            <center>Manager Produk</center>
         </i></p>
   </div>
</body>
<script>
   var doc = new jsPDF();
   var elementHandler = {
      '#ignorePDF': function(element, renderer) {
         return true;
      }
   };
   var source = window.document.getElementsByTagName("body")[0];
   doc.fromHTML(
      source,
      15,
      15, {
         'width': 180,
         'elementHandlers': elementHandler
      });

   doc.output("dataurlnewwindow");
</script>

</html>