<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_Jalan_Barang extends CI_Controller {
	public function __construct(){
		
        parent::__construct();
		      
        $this->load->model('model');
        $this->load->model('relasi');
        $this->load->library('session');
        $this->load->database();
        $this->load->helper('url');
		$this->load->helper('form');
		 
	 }
	 
		public function index(){
			$dataHeader['title'] 	= "Surat Jalan Barang Jadi";
			$dataHeader['menu'] 	= 'Master';
			$dataHeader['file'] 	= 'Surat Jalan Barang Jadi' ;
			$dataHeader['link']  	= 'index' ;

			$data['data'] = $this->model->tampil_barang();

			$data['file'] 	= $dataHeader['file'];
			$dataHeader['content'] = $this->load->view('back-end/master_barang/home', $data, TRUE);
			$this->load->view('Layout/home',$dataHeader);

			// $this->load->view('back-end/container/header',$dataHeader);
			// $this->load->view('back-end/master_barang/home',$data);
			// $this->load->view('back-end/container/footer');
		}

		public function export_pdf(){
			$data['data'] = $this->model->tampil_barang();
			$this->load->view('back-end/master_barang/export_pdf',$data);
		}

		public function tambah(){
			$dataHeader['title']		  	= "Tambah Surat Jadi Barang Jadi";
			$dataHeader['menu']   			= 'Master';
			$dataHeader['file']   			= 'Tambah Surat Jadi Barang Jadi' ;
			

			$data['file'] 	= $dataHeader['file'];
			$dataHeader['content'] = $this->load->view('back-end/master_barang/tambah', $data, TRUE);
			$this->load->view('Layout/home',$dataHeader);

			// $this->load->view('back-end/container/header',$dataHeader);
			// $this->load->view('back-end/master_barang/tambah');
			// $this->load->view('back-end/container/footer');
		}

		public function tambah_act(){
			$Value = array(
				'kode' => $this->input->post('kode'),
				'nama_barang' => $this->input->post('nama_barang'),
				'jumlah' => $this->input->post('jumlah'),
				'unit' => $this->input->post('unit')
			 );
			$this->model->tambah('tb_suratjalanbarang',$Value);
			Redirect("Surat_Jalan/Surat_Jalan_Barang");
		}

		public function edit(){
			$dataHeader['title']		= "Edit Surat Jalan Barang Jadi";
			$dataHeader['menu']   		= 'Master';
			$dataHeader['file']   		= 'Edit Surat Jalan Barang Jadi' ;
			$no = $_GET['no'];
			$data['data'] = $this->db->query("SELECT * FROM tb_suratjalanbarang WHERE no = $no")->row_array();
			
			$data['file'] 	= $dataHeader['file'];
			$dataHeader['content'] = $this->load->view('back-end/master_barang/edit', $data, TRUE);
			$this->load->view('Layout/home',$dataHeader);

			// $this->load->view('back-end/container/header',$dataHeader);
			// $this->load->view('back-end/master_barang/edit',$data);
			// $this->load->view('back-end/container/footer');
		}

		public function edit_act(){
			$no = $this->input->post('no');
			$Value = array(
				'kode' => $this->input->post('kode'),
				'nama_barang' => $this->input->post('nama_barang'),
				'jumlah' => $this->input->post('jumlah'),
				'unit' => $this->input->post('unit')
			 );
			$this->model->edit("tb_suratjalanbarang","no",$no,$Value);
			Redirect("Surat_Jalan/Surat_Jalan_Barang");
		}

		public function hapus(){
			$no = $_GET['no'];
			$this->model->hapus("tb_suratjalanbarang","no",$no);
			Redirect("Surat_Jalan/Surat_Jalan_Barang");
		}	
}