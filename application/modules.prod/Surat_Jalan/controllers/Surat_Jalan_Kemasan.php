<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_Jalan_Kemasan extends CI_Controller {
	public function __construct(){
		
        parent::__construct();
		      
        $this->load->model('model');
        $this->load->model('relasi');
        $this->load->library('session');
        $this->load->database();
        $this->load->helper('url');
		$this->load->helper('form');
		date_default_timezone_set("Asia/Jakarta");
	 }
	 
		public function index(){
			$dataHeader['title'] 	= "Surat Jalan Kemasan";
			$dataHeader['menu'] 	= 'Master';
			$dataHeader['file'] 	= 'Surat Jalan Kemasan';
			$dataHeader['link']  	= 'index' ;

			$data['data'] = $this->model->tampil_kemasan();

			$data['file'] 	= $dataHeader['file'];
			$dataHeader['content'] = $this->load->view('back-end/master_kemasan/home', $data, TRUE);
			$this->load->view('Layout/home',$dataHeader);
			
			// $this->load->view('back-end/container/header',$dataHeader);
			// $this->load->view('back-end/master_kemasan/home',$data);
			// $this->load->view('back-end/container/footer');
		}

		public function export_pdf(){
			$data['data'] = $this->model->tampil_kemasan();
			$this->load->view('back-end/master_kemasan/export_pdf',$data);
		}

		public function tambah(){
			$dataHeader['title']		  	= "Tambah Surat Kemasan";
			$dataHeader['menu']   			= 'Master';
			$dataHeader['file']   			= 'Tambah Surat Kemasan' ;
			
			$data['file'] 	= $dataHeader['file'];
			$dataHeader['content'] = $this->load->view('back-end/master_kemasan/tambah', $data, TRUE);
			$this->load->view('Layout/home',$dataHeader);

			// $this->load->view('back-end/container/header',$dataHeader);
			// $this->load->view('back-end/master_kemasan/tambah');
			// $this->load->view('back-end/container/footer');
		}

		public function tambah_act(){
			$Value = array(
				'jumlah' => $this->input->post('jumlah'),
				'nama' => $this->input->post('nama'),
				'jumlah_koli' => $this->input->post('jumlah_koli')
			 );
			$this->model->tambah('tb_suratjalankemasan',$Value);
			Redirect("Surat_Jalan/Surat_Jalan_Kemasan");
		}

		public function edit(){
			$dataHeader['title']		= "Edit Surat Jalan Kemasan";
			$dataHeader['menu']   		= 'Master';
			$dataHeader['file']   		= 'Edit Surat Jalan Kemasan' ;
			$no = $_GET['no'];
			$data['data'] = $this->db->query("SELECT * FROM tb_suratjalankemasan WHERE no = $no")->row_array();
			
			$data['file'] 	= $dataHeader['file'];
			$dataHeader['content'] = $this->load->view('back-end/master_kemasan/edit', $data, TRUE);
			$this->load->view('Layout/home',$dataHeader);

			// $this->load->view('back-end/container/header',$dataHeader);
			// $this->load->view('back-end/master_kemasan/edit',$data);
			// $this->load->view('back-end/container/footer');
		}

		public function edit_act(){
			$no = $this->input->post('no');
			$Value = array(
				'jumlah' => $this->input->post('jumlah'),
				'nama' => $this->input->post('nama'),
				'jumlah_koli' => $this->input->post('jumlah_koli')
			 );
			$this->model->edit("tb_suratjalankemasan","no",$no,$Value);
			Redirect("Surat_Jalan/Surat_Jalan_Kemasan");
		}

		public function hapus(){
			$no = $_GET['no'];
			$this->model->hapus("tb_suratjalankemasan","no",$no);
			Redirect("Surat_Jalan/Surat_Jalan_Kemasan");
		}
}