<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

            <!-- begin:: Subheader -->
            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
              <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                  <h3 class="kt-subheader__title">
                    <?=$file?> </h3>
                  <span class="kt-subheader__separator kt-hidden"></span>
                  
                </div>
                
              </div>
            </div>

            <!-- end:: Subheader -->

            <!-- begin:: Content -->
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
              <form action="<?=base_url()?>Surat_Jalan/Surat_Jalan_Kemasan/tambah_act" method="post">
                    <label for="jumlah">Jumlah</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" name="jumlah" id="jumlah" class="form-control" placeholder="Jumlah" required="" maxlength="50">
                        </div>
                    </div>

                    <label for="nama">Nama</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" required="" maxlength="50">
                        </div>
                    </div>

                    <label for="jumlah_koli">Jumlah Koli</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="number" name="jumlah_koli" id="jumlah_koli" class="form-control" placeholder="Jumlah Koli" required="">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">Save</button>
                    <a href="<?php echo base_url('Surat_Jalan/Surat_Jalan_Kemasan') ?>"  class="btn btn-danger">Back</button></a>
               </form>
            </div>

            <!-- end:: Content -->
          </div>