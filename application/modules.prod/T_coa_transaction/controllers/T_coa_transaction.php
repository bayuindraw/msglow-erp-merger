<?php
defined('BASEPATH') or exit('No direct script access allowed');


class T_coa_transaction extends CI_Controller
{

	var $url_ = "T_coa_transaction";
	var $id_ = "coa_transaction_id";
	var $eng_ = "journal";
	var $ind_ = "Jurnal";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function table_produk()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_ . ' Produk';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_produk', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_produk()
	{
		$datacontent['datatable'] = $this->Model->get_data_produk();
	}

	public function form_return_retur($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Pengembalian Retur';
		$datacontent['id'] = $id;
		$datacontent['factory_list'] = "";
		foreach ($this->Model->get_factory() as $key => $value) {
			$datacontent['factory_list'] = $datacontent['factory_list'] . '<option value="' . $value['id_factory'] . '">' . $value['nama_factory'] . '</option>';
		}
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Repair';
		$data['content'] = $this->load->view($this->url_ . '/form_return_retur', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_repair($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Repair';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Repair';
		$data['content'] = $this->load->view($this->url_ . '/form_repair', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_masuk_opname($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Adjustment Barang Masuk';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Adjustment';
		$data['content'] = $this->load->view($this->url_ . '/form_masuk_opname', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_keluar_opname($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Adjustment Barang Keluar';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Adjustment';
		$data['content'] = $this->load->view($this->url_ . '/form_keluar_opname', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_sample($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Sample';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Sample';
		$data['content'] = $this->load->view($this->url_ . '/form_sample', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_rusak($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Rusak';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Rusak';
		$data['content'] = $this->load->view($this->url_ . '/form_rusak', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_produk_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrproduk_row'] = $this->Model->get_produk_row($id);
		$datacontent['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail3($id);
		$datacontent['arrpackage_trial_employee'] = $this->Model->get_package_trial_employee3($id);
		foreach ($datacontent['arrpackage_trial_employee'] as $indeemp => $valueemp) {
			$datacontent['data_trial_employee'][$valueemp['package_detail_id']][] = $valueemp;
		}
		$datacontent['seller_list'] = "";
		$datacontent['arrmember'] = $this->Model->get_member();
		foreach ($datacontent['arrmember'] as $key => $value) {
			$datacontent['seller_list'] = $datacontent['seller_list'] . '<option value="' . $value['kode'] . '">' . $value['nama'] . ' (' . $value['kode'] . ')</option>';
		}
		$datacontent['product_list'] = "";
		$datacontent['arrproduk'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduk'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$datacontent['employee_list'] = "";
		$datacontent['arrphl'] = $this->Model->get_phl();
		foreach ($datacontent['arrphl'] as $key => $value) {
			$datacontent['employee_list'] = $datacontent['employee_list'] . '<option value="' . $value['id_phl'] . '">' . $value['nama_phl'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_produk_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan_repair()
	{
		$data = $this->input->post('input2');
		$data['user'] = $_SESSION['user_id'];
		$data['kode_po'] = 'MSGLOWPOFACTORY-2012016';
		$data['date_create'] = date('Y-m-d H:i:s');
		$data['factory_id'] = '19';
		$data['nomor_surat_jalan'] = '';
		$exec = $this->Model->insert_terima_produk($data);
		$this->Model->update_masuk($data['jumlah'], $data['tgl_terima'], $data['id_barang']);
		redirect(site_url('T_package_trial2/form_repair'));
	}

	public function simpan_masuk_opname()
	{
		$data = $this->input->post('input2');
		$data['user'] = $_SESSION['user_id'];
		$data['kode_po'] = 'MSGLOWPOFACTORY-2102013';
		$data['date_create'] = date('Y-m-d H:i:s');
		$data['factory_id'] = '29';
		$data['nomor_surat_jalan'] = '';
		$exec = $this->Model->insert_terima_produk($data);
		$this->Model->update_masuk($data['jumlah'], $data['tgl_terima'], $data['id_barang']);
		redirect(site_url('T_package_trial2/form_masuk_opname'));
	}

	public function simpan_return_retur()
	{
		$data = $this->input->post('input2');
		$data['user'] = $_SESSION['user_id'];
		$data['kode_po'] = 'MSGLOWPOFACTORY-2101114';
		$data['date_create'] = date('Y-m-d H:i:s');
		$data['factory_id'] = '28';
		$data['nomor_surat_jalan'] = '';
		$exec = $this->Model->insert_terima_produk($data);
		$this->Model->update_masuk($data['jumlah'], $data['tgl_terima'], $data['id_barang']);
		redirect(site_url('T_package_trial2/form_repair'));
	}

	public function simpan_sample()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$value = $this->input->post('input2');
			//$id2 = $_POST['id'];
			$data['package_code'] = $this->generate_code();
			$data['package_status_id'] = '1';
			$data['member_code'] = 'MSGLOWXX102';
			$data['user_id'] = $_SESSION['user_id'];
			$data['package_date_create'] = date('Y-m-d H:i:s');

			$exec = $this->Model->insert($data);
			$id = $this->Model->insert_id();

			//foreach($data2 as $key => $value){
			if (@$value['product_id'] != "") {
				$value['package_id'] = $id;
				$exec = $this->Model->insert_detail($value);
				$id2 = $this->Model->insert_id();

				$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
			}
			//}
		}
		redirect(site_url('T_package_trial2/form_sample'));
	}

	public function simpan_keluar_opname()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$value = $this->input->post('input2');
			//$id2 = $_POST['id'];
			$data['package_code'] = $this->generate_code();
			$data['package_status_id'] = '1';
			$data['member_code'] = 'MSGLOWXX104';
			$data['user_id'] = $_SESSION['user_id'];
			$data['package_date_create'] = date('Y-m-d H:i:s');

			$exec = $this->Model->insert($data);
			$id = $this->Model->insert_id();

			//foreach($data2 as $key => $value){
			if (@$value['product_id'] != "") {
				$value['package_id'] = $id;
				$exec = $this->Model->insert_detail($value);
				$id2 = $this->Model->insert_id();

				$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
			}
			//}
		}
		redirect(site_url('T_package_trial2/form_sample'));
	}

	public function simpan_rusak()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$value = $this->input->post('input2');
			//$id2 = $_POST['id'];
			$data['package_code'] = $this->generate_code();
			$data['package_status_id'] = '1';
			$data['member_code'] = 'MSGLOWXX101';
			$data['user_id'] = $_SESSION['user_id'];
			$data['package_date_create'] = date('Y-m-d H:i:s');

			$exec = $this->Model->insert($data);
			$id = $this->Model->insert_id();

			//foreach($data2 as $key => $value){
			if (@$value['product_id'] != "") {
				$value['package_id'] = $id;
				$exec = $this->Model->insert_detail($value);
				$id2 = $this->Model->insert_id();

				$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
			}
			//}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_package_trial2/form_rusak'));
	}

	public function simpan_safeconduct()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$id = $this->input->post('id');
			$data2 = $this->input->post('input2');
			$this->Model->delete_package_safeconduct(['package_id' => $id]);
			foreach ($data2 as $key => $value) {
				$value['package_id'] = $id;
				$exec = $this->Model->insert_package_safeconduct($value);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_package_trial2/table_safeconduct'));
	}

	public function simpan_produk_detail()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$id = $this->input->post('id');
			$data2 = $this->input->post('input2');
			$data3 = $this->input->post('input3');
			$exec = $this->Model->insert_reset3($id);
			foreach ($data2 as $key => $value) {
				$value['product_id'] = $id;
				$exec = $this->Model->insert_detail_draft($value);
				$id2 = $this->Model->insert_id();
				foreach ($data3[$key] as $key3 => $value3) {
					$value3['product_id'] = $id;
					$value3['package_detail_id'] = $id2;
					$exec = $this->Model->insert_employee($value3);
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_package_trial2/table_produk'));
	}

	public function table_seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_ . ' seller';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_seller2()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_ . ' seller';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_seller2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_seller();
	}

	public function get_data_seller2()
	{
		$datacontent['datatable'] = $this->Model->get_data_seller2();
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_safeconduct()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_safeconduct', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail_draft($id)
	{
		$datacontent['url'] = $this->url_;
		$data['file'] = $this->ind_;
		$datacontent['arrpackage_trial_detail_draft'] = $this->Model->get_package_trial_detail_draft($id);
		$datacontent['arrdata'] = $this->Model->get_package_trial($id);
		$datacontent['title'] = 'Data ' . $this->ind_ . ' Detail : ' . $datacontent['arrdata']['package_code'];
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail2($id)
	{
		$datacontent['url'] = $this->url_;
		$data['file'] = $this->ind_;
		$datacontent['arrpackage_trial_detail_draft'] = $this->Model->get_package_trial_detail($id);
		$datacontent['arrdata'] = $this->Model->get_package_trial($id);
		$datacontent['title'] = 'Data ' . $this->ind_ . ' Detail : ' . $datacontent['arrdata']['nama'] . ' (' . $datacontent['arrdata']['package_code'] . ')';
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function cetaksuratjalankemasan($id = "")
	{
		$data['kode']	= $id;
		$data['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail($id);
		$data['arrpackage_trial'] = $this->Model->get_package_trial($id);

		$this->load->view('sjkemasan', $data);
	}

	public function cetaksuratjalankemasan2($id = "")
	{
		$data['kode']	= $id;
		$data['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail($id);
		$data['arrpackage_trial'] = $this->Model->get_package_trial($id);

		$this->load->view('test_print', $data);
	}

	public function cetaksuratjalandriver($id = "")
	{
		$data['kode']	= $id;
		$data['arrpackage_trial_detail'] = $this->Model->get_package_safeconduct($id);
		$data['arrpackage_trial'] = $this->Model->get_package_trial($id);

		$this->load->view('sjdriver2', $data);
	}

	public function cetaksuratjalankemasan3($id = "")
	{
		$data['kode']	= $id;
		$data['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail($id);
		$data['arrpackage_trial'] = $this->Model->get_package_trial($id);

		$this->load->view('sjdriver', $data);
	}


	public function get_data_sales()
	{
		$datacontent['datatable'] = $this->Model->get_data_sales();
	}

	public function table_sales()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Penjualan';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_sales', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function generate_code()
	{

		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWOUT-";
		$dbDate	=	$this->db->query("SELECT (COUNT(package_id) + 1) as JumlahTransaksi FROM t_package_trial WHERE LEFT(package_date_create, 7) = '" . date('Y-m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 1;
			}
		} else {
			$nJumlahTransaksi = 1;
		}
		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0000' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '000' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 4) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 5) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}
		return $cKode;
	}

	public function get_data_postage()
	{
		$datacontent['datatable'] = $this->Model->get_data_postage();
	}

	public function table_postage()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Ongkir';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_postage', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_postage($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Ongkir' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		//$datacontent['arrpackage'] = $this->Model->get_package($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_postage', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_safeconduct($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Ongkir' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		$datacontent['arrpackage_safeconduct'] = $this->Model->get_package_safeconduct($id);
		$datacontent['arrsafeconduct'] = $this->Model->get_safeconduct();
		$datacontent['safeconduct_list'] = "";
		foreach ($datacontent['arrsafeconduct'] as $key => $value) {
			$datacontent['safeconduct_list'] = $datacontent['safeconduct_list'] . '<option value="' . $value['safeconduct_id'] . '">' . $value['safeconduct_name'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_safeconduct', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function search_member($search = '')
	{
		$this->Model->search_member($search);
		/*echo '{
  "results": [
    {
      "id": 1,
      "text": "Option 1"
    },
    {
      "id": 2,
      "text": "Option 2"
    }
  ],
  "pagination": {
    "more": true
  }
}';*/
	}

	public function form($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		$datacontent['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail($id);
		$datacontent['seller_list'] = "";
		/*foreach ($this->Model->get_member() as $key => $value) {
			$datacontent['seller_list'] = $datacontent['seller_list'].'<option value="'.$value['kode'].'">'.$value['nama'].' ('.$value['kode'].')</option>';
		}*/
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . (($value['tipe'] == "1") ? "Barang Jadi" : "Gudang") . ')</option>';
			//$datacontent['product_list'] = $datacontent['product_list'].'<option value="'.$value['id_produk'].'">'.$value['nama_produk'].' ('.(($value['tipe']=="1")?"Barang Jadi":"Gudang").')</option>';
		}
		$datacontent['employee_list'] = "";
		$datacontent['arrphl'] = $this->Model->get_phl();
		foreach ($datacontent['arrphl'] as $key => $value) {
			$datacontent['employee_list'] = $datacontent['employee_list'] . '<option value="' . $value['id_phl'] . '">' . $value['nama_phl'] . '</option>';
		}
		$datacontent['arrsales'] = $this->Model->get_sales($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_confirmation($id = '')
	{
		$id = str_replace('%20', ' ', $id);
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		$datacontent['arrmember'] = $this->Model->get_member2($id);
		$datacontent['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail_draft2($id);
		$datacontent['arrpackage_trial_employee'] = $this->Model->get_package_trial_employee4($id);
		foreach ($datacontent['arrpackage_trial_employee'] as $indeemp => $valueemp) {
			$datacontent['data_trial_employee'][$valueemp['package_detail_id']][] = $valueemp;
		}
		$datacontent['seller_list'] = "";
		foreach ($this->Model->get_member() as $key => $value) {
			$datacontent['seller_list'] = $datacontent['seller_list'] . '<option value="' . $value['kode'] . '">' . $value['nama'] . ' (' . $value['kode'] . ')</option>';
		}
		$datacontent['product_list'] = "";
		$datacontent['arrproduk'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduk'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$datacontent['employee_list'] = "";
		$datacontent['arrphl'] = $this->Model->get_phl();
		foreach ($datacontent['arrphl'] as $key => $value) {
			$datacontent['employee_list'] = $datacontent['employee_list'] . '<option value="' . $value['id_phl'] . '">' . $value['nama_phl'] . '</option>';
		}
		$datacontent['arrsales'] = $this->Model->get_sales($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_confirmation', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_confirmation_adjustment($id = '')
	{

		//$datacontent['title'] = 'Data '.$this->ind_.' Detail : '.$datacontent['arrdata']['nama'].' ('.$datacontent['arrdata']['package_code'].')';
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;


		$datacontent['arrpackage_trial_detail_draft'] = $this->Model->get_package_trial_detail($id);
		$datacontent['arrdata'] = $this->Model->get_package_trial($id);


		//$datacontent['arrmember'] = $this->Model->get_member2($id); 



		$datacontent['product_list'] = "";
		$datacontent['arrproduk'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduk'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}


		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_adjustment', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_confirmation2($id = '')
	{
		$id = str_replace('%20', ' ', $id);
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		$datacontent['arrmember'] = $this->Model->get_member2($id);
		$datacontent['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail_draft2($id);
		$datacontent['arrpackage_trial_employee'] = $this->Model->get_package_trial_employee4($id);
		foreach ($datacontent['arrpackage_trial_employee'] as $indeemp => $valueemp) {
			$datacontent['data_trial_employee'][$valueemp['package_detail_id']][] = $valueemp;
		}
		$datacontent['seller_list'] = "";
		foreach ($this->Model->get_member() as $key => $value) {
			$datacontent['seller_list'] = $datacontent['seller_list'] . '<option value="' . $value['kode'] . '">' . $value['nama'] . ' (' . $value['kode'] . ')</option>';
		}
		$datacontent['product_list'] = "";
		$datacontent['arrproduk'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduk'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$datacontent['employee_list'] = "";
		$datacontent['arrphl'] = $this->Model->get_phl();
		foreach ($datacontent['arrphl'] as $key => $value) {
			$datacontent['employee_list'] = $datacontent['employee_list'] . '<option value="' . $value['id_phl'] . '">' . $value['nama_phl'] . '</option>';
		}
		$datacontent['arrsales'] = $this->Model->get_sales($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_confirmation2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_employee($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail($id);
		$datacontent['arrpackage_trial_employee'] = $this->Model->get_package_trial_employee($id);
		foreach ($datacontent['arrpackage_trial_employee'] as $indeemp => $valueemp) {
			$datacontent['data_trial_employee'][$valueemp['package_detail_id']][] = $valueemp;
		}
		$datacontent['product_list'] = "";
		$datacontent['arrproduk'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduk'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$datacontent['employee_list'] = "";
		$datacontent['arrphl'] = $this->Model->get_phl();
		foreach ($datacontent['arrphl'] as $key => $value) {
			$datacontent['employee_list'] = $datacontent['employee_list'] . '<option value="' . $value['id_phl'] . '">' . $value['nama_phl'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_employee', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail($id = '')
	{

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrproduct'] = $this->Model->get_product();
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan()
	{
		if ($this->input->post('simpan')) {

			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$data['user_id'] = $this->session->userdata('user_id');
				$data[$this->eng_ . '_date_create'] = date('Y-m-d H:i:s');

				/*if($data['package_postage'] != "" && $data['package_postage'] != "0"){
						$account_id = $this->Model->get_account_id($_POST['seller_id']);
						$id2 = $this->Model->get_max_id();
						$datax['account_id'] = $account_id;
						$datax['account_detail_id'] = $id2;
						$datax['account_detail_type_id'] = 3;
						$datax['account_detail_user_id'] = $_SESSION['user_id'];
						$datax['account_detail_pic'] = $_SESSION['user_fullname'];
						$datax['account_detail_date'] = $data['package_date'];
						$datax['account_detail_category_id'] = '82';
						$datax['account_detail_realization'] = 1;
						$datax['account_detail_price'] = $data['package_postage'];
						$datax['account_detail_note'] = 'Ongkos Kirim '.$data['package_code'];
						$type = "debit";
						$datax['account_detail_debit'] = $data['package_postage'];
						$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
						$id_account_detail = $this->Model->insert_account_detail($datax); 
						$this->db->where('account_id', $account_id);
						$row = $this->Model->get_m_account()->row_array();
						if(@$row['account_date_reset'] > $_POST['package_date']){
							$this->Model->update_balance($account_id,  $data['package_postage'], $type, @$row['account_type_id'], $datax['account_detail_date']);
						}else{
							$this->Model->update_balance($account_id,  $data['package_postage'], $type);
						}
						$data['account_detail_real_id'] = $id_account_detail;
				}*/
				$data['package_date_draft'] = $data['package_date'];
				$exec = $this->Model->insert($data);
				$id = $this->Model->insert_id();
			}
		}
		redirect(site_url('T_package_trial/form_employee/' . $id));
	}

	public function simpan_confirmation()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$id2 = $_POST['id'];
				$data['package_status_id'] = '1';
				$data['member_code'] = $id2;
				$data['user_id'] = $_SESSION['user_id'];
				$data['package_date_create'] = date('Y-m-d H:i:s');

				$exec = $this->Model->insert($data);
				$id = $this->Model->insert_id();


				$data2 = $this->input->post('input2');
				$data3 = $this->input->post('input3');
				$exec = $this->Model->update_confirm($id, $id2);
				/*foreach($data2 as $key => $value){
					$value['package_id'] = $id;
					$exec = $this->Model->insert_detail_draft($value);
					
				}*/

				$data2 = $this->input->post('input2');
				foreach ($data2 as $key => $value) {
					if (@$value['product_id'] != "") {
						$value['package_id'] = $id;
						$exec = $this->Model->insert_detail($value);
						$id2 = $this->Model->insert_id();
						foreach ($data3[$key] as $key3 => $value3) {
							$value3['package_id'] = $id;
							$value3['package_detail_id'] = $id2;
							$value3['product_id'] = $value['product_id'];
							$exec = $this->Model->insert_employee_real($value3);
						}
						$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
					}
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_package_trial2'));
	}

	public function simpan_confirmation2()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {


				$this->db->trans_begin();
				$data['update_user_id'] = $_SESSION['user_id'];
				$exec = $this->Model->update($data, [$this->id_ => $this->input->post('id')]);;

				$date_previous = $_POST['date_previous'];
				$arrdata = $this->Model->get_package_trial_detail_pure($this->input->post('id'));
				foreach ($arrdata as $key2 => $value2) {
					$this->Model->update_masuk($value2['package_detail_quantity'], $date_previous, $value2['product_id']);
				}


				$data2 = $this->input->post('input2');
				foreach ($data2 as $key => $value) {
					if (@$value['product_id'] != "") {
						$exec = $this->Model->update_table('t_package_trial_detail', $value, ['package_detail_id' => $value['package_detail_id']]);
						$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
					}
				}
				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
			}
		}
		redirect(site_url('T_package_trial2'));
	}

	public function simpan_employee()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$id = $this->input->post('id');
			$data2 = $this->input->post('input2');
			$data3 = $this->input->post('input3');
			$exec = $this->Model->insert_reset($id);
			foreach ($data2 as $key => $value) {
				if (@$value['product_id'] != "") {
					$value['package_id'] = $id;
					$exec = $this->Model->insert_detail_draft($value);
					$id2 = $this->Model->insert_id();
					foreach ($data3[$key] as $key3 => $value3) {
						$value3['package_id'] = $id;
						$value3['package_detail_id'] = $id2;
						$exec = $this->Model->insert_employee($value3);
					}
				}
			}
			//$id = $this->input->post('id');
			//$data2 = $this->input->post('input2');
			//foreach($data2 as $key => $value){
			//	$value['package_id'] = $id;
			//	$exec = $this->Model->insert_employee($value);
			//	//$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
			//}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_package_trial'));
	}

	public function simpan_detail()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$data['sales_id'] = $_POST['id'];
			$data['user_id'] = $this->session->userdata('user_id');
			$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
			$exec = $this->Model->insert_detail($data);
		}
		redirect(site_url('T_sales/form_detail/' . $_POST['id']));
	}

	public function simpan_postage()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$id = $this->input->post('id');
			$data = $this->input->post('input');
			if (@$data['package_delivery_method'] == "") {
				$arrdata = $this->Model->get_package_all($id);
				$account_id = $arrdata['account_id'];
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrdata['package_date'];
				$datax['account_detail_category_id'] = '82';
				$datax['account_detail_realization'] = 1;
				$datax['account_detail_price'] = $data['package_postage'];
				$datax['account_detail_note'] = 'Ongkos Kirim ' . $data['package_code'];
				$type = "debit";
				$datax['account_detail_debit'] = $data['package_postage'];
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');
				$id_account_detail = $this->Model->insert_account_detail($datax);
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account()->row_array();
				if (@$row['account_date_reset'] > $arrdata['package_date']) {
					$this->Model->update_balance($account_id,  $data['package_postage'], $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $data['package_postage'], $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			}
			$exec = $this->Model->update($data, [$this->id_ => $this->input->post('id')]);;
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_sales/table_postage'));
	}

	public function simpan_detail_all()
	{
		redirect(site_url('T_sales'));
	}

	public function report()
	{
		if (@$_POST['export_pdf']) {
			$this->export_pdf();
		} else {
			$datacontent['url'] = $this->url_;
			$datacontent['search'] = $this->input->post('search');
			$datacontent['url2'] = '/' . $datacontent['search']['member_status'] . '|' . $datacontent['search']['all'];
			$datacontent['title'] = 'Member';
			$data['file'] = $this->ind_;
			$data['content'] = $this->load->view($this->url_ . '/table_report', $datacontent, TRUE);
			$data['title'] = $datacontent['title'];
			$this->load->view('Layout/home', $data);
		}
	}

	public function export_pdf()
	{
		ob_start();
		$datacontent['data'] = $this->Model->get_report()->result_array();
		$this->load->view('M_member/export_pdf', $datacontent);
		//$data['content'] = $this->load->view('m_member/table_report', $datacontent);
		//$data['siswa'] = $this->siswa_model->view_row();
		//$this->load->view('print', $data);
		$html = ob_get_contents();
		ob_end_clean();

		require './assets/html2pdf/autoload.php';

		$pdf = new Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
		$pdf->WriteHTML($html);
		$pdf->Output(date('Y-m-d') . '.pdf', 'D');
	}

	public function get_data($tgl_awal = '', $tgl_akhir = '')
	{
		$datacontent['datatable'] = $this->Model->get_data($tgl_awal, $tgl_akhir);
		// $datacontent['datatable'] = $this->Model->get_data_jurnal($tgl_awal, $tgl_akhir);
	}

	public function get_data_safeconduct()
	{
		$datacontent['datatable'] = $this->Model->get_data_safeconduct();
	}

	public function get_data_report($search = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_report($search);
	}

	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}

	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}

	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}



	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {

				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_credit'] = $from['account_detail_debit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_credit'], 'credit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_send()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}

	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();


		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();

		if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
		} else {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
		}



		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_ . "/$id"));
	}

	public function list_kecamatan()
	{
		$id_kota = $this->input->post('id_kota');
		$stasiun = $this->Model->get_list_kecamatan($id_kota);
		$lists = "<option value=''>Pilih Kecamatan</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kecamatan . "'>" . $data->nama_kecamatan . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function list_kota()
	{
		$id_provinsi = $this->input->post('id_provinsi');
		$stasiun = $this->Model->get_list_kota($id_provinsi);
		$lists = "<option value=''>Pilih Kota</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kota . "'>" . $data->nama_kota . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}
}
