
<div class="row">
  <div class="col-lg-12 col-xl-12">
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            FORM DETAIL KEMAS <?= $arrproduk_row['nama_produk'] ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
        <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_produk_detail'); ?>" method="post" novalidate>
          <?= input_hidden('id', $id) ?>
		 <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>Seller</th>
                                <th>Jumlah</th>
                                <th>Pegawai</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id="x"> 
								<?php 
								$totalx = 0;
								if(count(@$arrpackage_trial_detail) > 0){ 
									$i = 0;
									foreach($arrpackage_trial_detail as $indexx => $valuex){
									
								?>
									<tr id="<?= $i; ?>">
										
										<td><select name="input2[<?= $i; ?>][seller_id]" class="PilihDistributor2 form-control md-static" required><option></option>
								<?php
									echo '<option value="'.$valuex['seller_id'].'" selected>'.$valuex['nama'].' ('.$valuex['seller_id'].') ('.$valuex['kota'].')</option>';
									foreach($arrmember as $indexl => $valuel){
											echo '<option value="'.$valuel['kode'].'" '.(($valuex['seller_id'] == $valuel['kode'])?'selected':'').'>'.$valuel['nama'].' ('.$valuel['kode'].')</option>';
											//echo '<option value="'.$valuel['id_phl'].'" '.(($valuex['employee_id'] == $valuel['id_phl'])?'selected':'').'>'.$valuel['nama_phl'].'</option>';
								?>
								<?php	
										}
										$totalx += $valuex['package_detail_quantity'];
										?>
									<td><input type="number" class="form-control sum_total" onkeyup="chk_total($(this).val())" placeholder="Jumlah" name="input2[<?= $i ?>][package_detail_quantity]" required value="<?= $valuex['package_detail_quantity']; ?>"></td>
										<td><div id="td<?= $i ?>">
										<?php $i2 = 0;
										if(@count(@$data_trial_employee[@$valuex['package_detail_id']]) > 0){
									foreach($data_trial_employee[$valuex['package_detail_id']] as $indexxx => $valuexxx){ 
									?>
								
											<div class="form-group row" id="employee<?= $i; ?>x<?= $i2; ?>">
												<div class="col-10">
													<select name="input3[<?= $i; ?>][<?= $i2; ?>][employee_id]" class="PilihPegawai form-control" required><option></option><?php
													foreach($arrphl as $indexl => $valuel){
														echo '<option value="'.$valuel['id_phl'].'" '.(($valuexxx['employee_id'] == $valuel['id_phl'])?'selected':'').'>'.$valuel['nama_phl'].'</option>'; 
													}
													?></select>
												</div>
												<label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee(<?= $i ?>, <?= $i2; ?>);">-</label>
											</div>
										<?php 
											$i2++;
										}
										}else{ ?>
											<div id="td0"><div class="form-group row" id="employee0x0">
												<div class="col-10">
													<select name="input3[<?= $i ?>][0][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select>
												</div>
												<label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee(<?= $i ?>, 0);">-</label>
											</div>
											</div>
										<?php } ?>
										</div>
										<label onclick="add_employee(<?= $i ?>);">+</label>
										</td>										
										<td><button onclick="myDeleteFunction('<?= $i ?>')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									<?php $i++;
									}
									
								}else{ ?>
									<tr id="0">
										<td><select name="input2[0][seller_id]" class="PilihDistributor2 form-control md-static" required><option></option><?= $seller_list ?></select></td>
										<td><input type="number" class="form-control sum_total" onkeyup="chk_total($(this).val())" placeholder="Jumlah" name="input2[0][package_detail_quantity]" required></td>
										<td><div id="td0"><div class="form-group row" id="employee0x0">
											<div class="col-10">
												<select name="input3[0][0][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select>
											</div>
											<label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee(0, 0);">-</label>
										</div>
										</div>
										<label onclick="add_employee(0);">+</label>
										</td>										
										<td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									</tr> 
								<?php } ?>
                            </tbody>    
							<tbody>
								<tr>
									<td>Stok : <?= $arrproduk_row['jumlah'] ?></td>
									<td>Total : <span id="total"><?= $totalx ?></span></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
                         </table>
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();return false();">Tambah Barang</button>
          <!--<div class="form-group">
            <label>Nama Barang</label>
            <select name="NamaBarang" id="cIdStock" class="form-control md-static" required>

              <option></option>
              <?php
              /*$query = $this->model->ViewWhere('v_detail_produk_jual', 'kode_jual', $action);
              foreach ($query as $key => $vaKemasan) {
              ?>
                <option value="<?= $vaKemasan['id_barang'] ?>"><?= $vaKemasan['nama_produk'] ?></option>
              <?php }*/ ?>
            </select>
          </div>-->
          <div class="kt-portlet__foot" style="margin-top: 20px;">
            <div class="kt-form__actions">
              <button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;">
                <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
it = <?= ((count(@$arrpackage_trial_detail) > 0)?count(@$arrpackage_trial_detail):0); ?>;
let fruits = [];
var total = 0;
<?php if(count(@$arrpackage_trial_detail) > 0){
	$i = 0;
	foreach($arrpackage_trial_detail as $indexx => $valuex){	
			?>
				fruits.push([0]);
			<?php
			$i2 = 1;
			if(@count(@$data_trial_employee[@$valuex['package_detail_id']]) > 0){
		foreach(@$data_trial_employee[@$valuex['package_detail_id']] as $indexxx => $valuexxx){
			?>
				fruits[<?= $i ?>]++;
			<?php 
			$i2++;
		}
			}
		$i++;
	}
}else{ ?>
	
<?php } ?>
fruits.push([0]);
function myCreateFunction() {
	it++;
	fruits.push([0]);
	var html = '<tr id="'+it+'"><td><select name="input2['+it+'][seller_id]" class="PilihDistributor2 form-control md-static" required><option></option><?= $seller_list ?></select></td><td><input type="number" class="form-control sum_total" onkeyup="chk_total($(this).val())" placeholder="Jumlah" name="input2['+it+'][package_detail_quantity]" required></td><td><div id="td'+it+'"><div class="form-group row" id="employee'+it+'x0"><div class="col-10"><select name="input3['+it+'][0][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select></div><label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee('+it+', 0);">-</label></div></div><label onclick="add_employee('+it+');">+</label></td><td><button onclick="myDeleteFunction('+it+')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
	$('#x').append(html);
	$('.PilihBarang').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Barang',
	});
	$('.PilihPegawai').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Pegawai', 
	});
	$('.PilihDistributor2').select2({		 
	   minimumInputLength: 3,
      allowClear: true,
	  placeholder: 'Input Nama Seller',
      ajax: {
        type: 'POST',
		url: function (params) {
			return '<?= $url ?>/search_member/' + params.term+ '/<?= $id ?>';
		},
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
       
        processResults: function (data, params) {
    params.page = params.page || 1;

    return {
        results: data.results,
        pagination: {
            more: (params.page * 10) < data.count_filtered
        }
    };
}
	 }});
}
function add_employee(id) { 
	fruits[id]++;
	var html = '<div class="form-group row" id="employee'+id+'x'+fruits[id]+'"><div class="col-10"><select name="input3['+id+']['+fruits[id]+'][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select></div><label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee('+id+', '+fruits[id]+');">-</label></div>';
	$('#td'+id).append(html);
	$('.PilihPegawai').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Pegawai', 
	});
}

function myDeleteFunction(id) {
	$('#'+id).remove();
}

function delete_employee(id, id2) {
	$('#employee'+id+'x'+id2).remove();
}

function chk_total(val){
	var sum = 0;
	$('.sum_total').each(function(){
		    //if(parseFloat(this.value) > 0)(
				sum += parseFloat(this.value);
		//}
	});
	$('#total').html(sum); 
}
$('.PilihDistributor2').select2({		 
	   minimumInputLength: 3,
      allowClear: true,
	  placeholder: 'Input Nama Seller',
      ajax: {
        type: 'POST',
		url: function (params) {
			return '<?= $url ?>/search_member/' + params.term+ '/<?= $id ?>';
		},
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
       
        processResults: function (data, params) {
    params.page = params.page || 1;

    return {
        results: data.results,
        pagination: {
            more: (params.page * 10) < data.count_filtered
        }
    };
}
	 }});
</script>

