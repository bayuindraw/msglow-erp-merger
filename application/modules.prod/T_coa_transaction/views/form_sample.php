
<div class="row"
								<div class="col-lg-12 col-xl-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan_sample'); ?>" enctype="multipart/form-data">
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Tanggal</label>
													<input type="text" id="dTglPo" name="input[package_date]" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= date('Y-m-d') ?>" required>
												</div>
												<div class="form-group">
													<label>Barang</label>
													<select name="input2[product_id]" class="PilihBarang form-control md-static" required><option></option>
								<?php
									foreach($arrproduct as $indexl => $valuel){
											echo '<option value="'.$valuel['id_produk'].'" '.(($valuex['product_id'] == $valuel['id_produk'])?'selected':'').'>'.$valuel['nama_produk'].' ('.$valuel['klasifikasi'].') (stock: '.$valuel['jumlah'].')</option>';
											//echo '<option value="'.$valuel['id_phl'].'" '.(($valuex['employee_id'] == $valuel['id_phl'])?'selected':'').'>'.$valuel['nama_phl'].'</option>';
								?>
								<?php	
										} ?>
										</select>
												</div>
												<div class="form-group">
													<label>Jumlah</label>
													<input type="text" id="cJumlah" name="input2[package_detail_quantity]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static">
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>