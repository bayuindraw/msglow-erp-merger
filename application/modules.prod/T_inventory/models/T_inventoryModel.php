<?php
defined('BASEPATH') or exit('No direct script access allowed');

class T_inventoryModel extends CI_Model
{
	
	var $table_ = "t_transfer";
	var $id_ = "transfer_id";
	var $eng_ = "action";
	var $url_ = "T_inventory";
	
	function get()
	{
		$data = $this->db->get($this->table_);
		return $data;
	}

	function get_data($status)
	{
		$table = $this->table_." a";
		$id = $this->id_;
		$field = array('transfer_code','transfer_date', 'transfer_date_send', 'b.warehouse_name', 'c.warehouse_name as wh_target');
		$field2 = array('transfer_code','transfer_date', 'transfer_date_send', 'warehouse_name', 'wh_target');
		$arrjoin[] = "JOIN m_warehouse b ON a.warehouse_id = b.warehouse_id";
		$arrjoin[] = "JOIN m_warehouse c ON a.warehouse_target_id = c.warehouse_id";
		$url = $this->url_;

		if ($status==0) {
			$action = '<a href="'.site_url($url."/edit/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . '/hapus/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a> <a href="'.site_url($url . '/download_pdf/xid').'" class="btn btn-default" style="margin-top: 3px;"> <i class="fa fa-download"></i> Download PDF</a> <a href="'.site_url($url . '/verifikasi/xid').'" class="btn btn-success" style="margin-top: 3px;"> <i class="fa fa-check"></i> Verifikasi</a>';
		} else {
			$action = '<a href="'.site_url($url . '/download_pdf_verifikasi/xid').'" class="btn btn-default" style="margin-top: 3px;"> <i class="fa fa-download"></i> Download PDF</a>';
		}

		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$arrwhere[] = "transfer_status = '$status'";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field2 as $keyfield){
				$datax[] = $valuer[$keyfield];	
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);
		
		echo json_encode($data);
	}
	
	function get_detail_wh($wh_id)
	{
		$query = "SELECT 
		(SELECT c.name FROM m_region c WHERE c.REGION_ID = RPAD(LEFT(a.REGION_ID, 2), 10, 0)) as prov,
		(SELECT c.name FROM m_region c WHERE c.REGION_ID = RPAD(LEFT(a.REGION_ID, 4), 10, 0)) as kota,
		(SELECT c.name FROM m_region c WHERE c.REGION_ID = RPAD(LEFT(a.REGION_ID, 6), 10, 0)) as kec,
		a.name as kel, b.warehouse_address as alamat, a.region_id
		FROM m_region a
		JOIN m_warehouse b ON a.region_id = b.warehouse_region_id
		WHERE b.warehouse_id = '$wh_id'";
		$data = $this->db->query($query);
		return $data;
	}
	
	function get_table_where($table, $where)
	{
		$this->db->where($where);
		$data = $this->db->get($table);
		return $data;
	}
	
	function get_table($table)
	{
		$data = $this->db->get($table);
		return $data;
	}

	function get_produk()
	{
		$data = $this->db->query("SELECT A.* FROM produk A WHERE A.produk_status = '1'");
		return $data->result_array();
	}


	function get_brand($id)
	{
		$data = $this->db->query("SELECT b.brand_id, c.brand_name FROM produk a 
			JOIN produk_global b ON b.kode = a.kd_pd 
			JOIN m_brand c ON b.brand_id = c.brand_id
			WHERE a.id_produk = '$id'");
		return $data->result_array();
	}
	
	function insert($table, $data = array())
	{
		$this->db->insert($table, $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function update($table, $data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($table, $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function delete($table, $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($table);
		$info = '<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}

	//PDF
	function get_header($id)
	{
		$query = "SELECT a.transfer_code, a.transfer_date, a.transfer_date_send, b.warehouse_name, c.warehouse_id, c.warehouse_name as wh_target, c.warehouse_address, d.delivery_instance_name, e.user_fullname, f.role_name
		FROM t_transfer a
		JOIN m_warehouse b ON a.warehouse_id = b.warehouse_id
		JOIN m_warehouse c ON a.warehouse_target_id = c.warehouse_id
		JOIN m_delivery_instance d ON a.delivery_instance_id = d.delivery_instance_id
		JOIN m_user e ON a.transfer_create_user_id = e.user_id
		LEFT JOIN m_role f ON e.role_id = f.role_id
		WHERE a.transfer_id = '$id'";
		$data = $this->db->query($query)->result_array();
		return $data;
	}

	function get_wh_alamat($id)
	{
		$query = "SELECT 
		(SELECT c.name FROM m_region c WHERE c.REGION_ID = RPAD(LEFT(a.REGION_ID, 2), 10, 0)) as prov,
		(SELECT c.name FROM m_region c WHERE c.REGION_ID = RPAD(LEFT(a.REGION_ID, 4), 10, 0)) as kota,
		(SELECT c.name FROM m_region c WHERE c.REGION_ID = RPAD(LEFT(a.REGION_ID, 6), 10, 0)) as kec,
		a.name as kel, b.warehouse_address as alamat
		FROM m_region a
		JOIN m_warehouse b ON a.region_id = b.warehouse_region_id
		WHERE b.warehouse_id = '$id'";
		$data = $this->db->query($query)->result_array();
		return $data;
	}
}
