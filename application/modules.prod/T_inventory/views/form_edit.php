<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan_edit/'.$dt_t[0]['transfer_id']); ?>" enctype="multipart/form-data">
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Kode Transfer</label>
								<input type="text" id="transfer_code" name="input[transfer_code]" class="md-form-control md-static floating-label form-control" value="<?php echo $dt_t[0]['transfer_code'] ?>" readonly>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal Order</label>
								<input type="text" id="transfer_date" name="input[transfer_date]" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?php echo $dt_t[0]['transfer_date'] ?>" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal Permintaan Kirim</label>
								<input type="text" id="transfer_date_send" name="input[transfer_date_send]" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?php echo $dt_t[0]['transfer_date_send'] ?>" required>
							</div>
						</div>
						<div class="col-md-6">
							<label>Gudang Asal</label>
							<select class="form-control kt-select2 select_gudang_asal" id="warehouse_id" name="input[warehouse_id]" onchange="ganti_asal(this.value)">
								<?php foreach ($wh as $d) { ?>
								<option value="<?php echo $d['warehouse_id'] ?>" <?php if($dt_t[0]['warehouse_id']==$d['warehouse_id']) echo 'selected'; ?>><?php echo $d['warehouse_name'] ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-6">
							<label>Gudang Tujuan</label>
							<select class="form-control kt-select2 select_gudang_tujuan" id="warehouse_target_id" name="input[warehouse_target_id]" onchange="detail_wh()">
								<?php foreach ($wh as $d) { ?>
								<option value="<?php echo $d['warehouse_id'] ?>" <?php if($dt_t[0]['warehouse_target_id']==$d['warehouse_id']) echo 'selected'; ?>><?php echo $d['warehouse_name'] ?></option>
								<?php } ?>
							</select>

							<input type="hidden" name="input[warehouse_region_id]" id="warehouse_region_id" value="<?php echo $dt_t[0]['warehouse_region_id'] ?>">
						</div>
						<div class="col-md-6" style="margin-top: 30px;">
							<div class="form-group">
								<label>Provinsi Tujuan</label>
								<input type="text" name="prov" id="prov" placeholder="Provinsi.." class="form-control" readonly="">
							</div>
						</div>
						<div class="col-md-6" style="margin-top: 30px;">
							<div class="form-group">
								<label>Kota Tujuan</label>
								<input type="text" name="kota" id="kota" placeholder="Kota.." class="form-control" readonly="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Kecamatan Tujuan</label>
								<input type="text" name="kec" id="kec" placeholder="Kecamatan.." class="form-control" readonly="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Kelurahan Tujuan</label>
								<input type="text" name="kel" id="kel" placeholder="Kelurahan.." class="form-control" readonly="">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Alamat Tujuan</label>
								<textarea class="form-control" id="alamat" name="input[warehouse_address]" rows="3" placeholder="Alamat.." readonly=""></textarea>
							</div>
						</div>
						<div class="col-md-12">
							<label>Ekspedisi</label>
							<select class="form-control kt-select2 select_ekspedisi" name="input[ekspedisi]">
								<option value=""></option>
								<?php foreach ($deliveries as $delivery) { ?>
								<option value="<?= $delivery['delivery_instance_id'] ?>" <?php if($dt_t[0]['delivery_instance_id']==$delivery['delivery_instance_id']) echo 'selected'; ?>><?= $delivery['delivery_instance_name'] ?></option>
								<?php } ?>
							</select>
						</div>


						<div class="col-md-12" style="margin-top: 30px;">
							<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
								<thead>
									<tr>
										<th>Produk</th>
										<th style="width: 20%;">Jumlah</th>
										<th style="width: 10%;">Aksi</th>
									</tr>
								</thead>
								<tbody id="x">
									<?php for ($i=0; $i < count($dt_t_d); $i++) {?>
									<tr id="<?= $i; ?>">
										<td>
											<select name="input2[<?= $i; ?>][product_id]" class="PilihBarang form-control md-static" required>
												<option></option>
												<?php foreach ($produk as $d) { ?>
												<option value="<?php echo $d['id_produk'] ?>" <?php if($dt_t_d[$i]['product_id']==$d['id_produk']) echo 'selected'; ?>><?php echo $d['nama_produk'] ?></option>
												<?php } ?>
											</select>
										</td>
										<td>
											<input type="number" class="form-control" placeholder="Jumlah" name="input2[<?= $i ?>][transfer_detail_quantity]" id="quantity<?= $i ?>" value="<?php echo $dt_t_d[$i]['transfer_detail_quantity'] ?>" required>
										</td>
										<td>
											<center><button onclick="myDeleteFunction('<?= $i ?>');" class="btn btn-danger"><i class="fas fa-trash"></i></button></center>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
						</div>


					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var wh_id = document.getElementById('warehouse_id').value;
		ganti_asal(wh_id);

		$('.select_ekspedisi').select2({
			allowClear: true,
			placeholder: 'Pilih Ekspedisi',
		});

		$('.select_gudang_asal').select2({
			allowClear: true,
			placeholder: 'Pilih Gudang Asal',
		});

		$('.select_gudang_tujuan').select2({
			allowClear: true,
			placeholder: 'Pilih Gudang Tujuan',
		});
	});

	function ganti_asal(wh_id) {
		$('#frm-tujuan').hide();

		$.ajax({
			type:'post',
			url: '<?php echo site_url('T_inventory/get_wh') ?>',
			data: {
				"warehouse_id":wh_id
			},
			success: function(data){
				var json = $.parseJSON(data);
				if(json.status){
					$('#warehouse_target_id').find('option').remove().end();
					for(var i = 0; i<json.data.length; i++){
						var o = new Option(json.data[i].warehouse_name, json.data[i].warehouse_id);
						$("#warehouse_target_id").append(o);
					}
					detail_wh();
				}
				$("#frm-tujuan").show();
			}
		});
	}

	function detail_wh() {
		var wh_tujuan = document.getElementById("warehouse_target_id").value;
		$.ajax({
			type:'post',
			url: '<?php echo site_url('T_inventory/get_detail_wh') ?>',
			data: {
				"warehouse_id":wh_tujuan
			},
			success: function(data){
				var json = $.parseJSON(data);
				if (json.status) {
					document.getElementById("warehouse_region_id").value = json.data[0]['region_id'];
					document.getElementById("prov").value = json.data[0]['prov'];
					document.getElementById("kota").value = json.data[0]['kota'];
					document.getElementById("kec").value = json.data[0]['kec'];
					document.getElementById("kel").value = json.data[0]['kel'];
					document.getElementById("alamat").value = json.data[0]['alamat'];
				}
			}
		});
	}

	var it = '<?php echo count($dt_t_d); ?>';
	function myCreateFunction() {
		var html = '<tr id="'+it+'">'+
		'<td><select name="input2['+it+'][product_id]" class="PilihBarang form-control md-static" required><option></option><?php foreach ($produk as $d) { ?><option value="<?php echo $d['id_produk'] ?>"><?php echo $d['nama_produk'] ?></option><?php } ?></select></td>'+
		'<td><input type="number" class="form-control" placeholder="Jumlah" name="input2['+ it +'][transfer_detail_quantity]" id="quantity'+ it +'" value="" required></td>'+
		'<td><center><button onclick="myDeleteFunction('+ it +');" class="btn btn-danger"><i class="fas fa-trash"></i></button></center></td>'+
		'<tr>';

		$('#x').append(html);
		$('.PilihBarang').select2({
			allowClear: true,
			placeholder: 'Pilih Barang',
		});
		
		it++;
	}

	function myDeleteFunction(id) {
		$('#' + id).remove();
	}
</script>