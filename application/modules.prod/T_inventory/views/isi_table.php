<table class="table table-striped- table-bordered table-hover" id="kt_table_1">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode Transfer Order</th>
			<th>Tgl. Order</th>
			<th>Tgl. Req. Kirim</th>
			<th>Gudang Asal</th>
			<th>Gudang Tujuan</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",
			scrollY: "300px",
			scrollX: true,
			scrollCollapse: true,
			"processing": true,
			"serverSide": true,
			"ajax": "<?= site_url().'/'.$url.'/get_data/'.$status ?>"
		});
	});
</script>