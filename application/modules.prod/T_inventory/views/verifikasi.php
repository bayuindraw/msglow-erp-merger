<div class="row">
	<div class="col-lg-6 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan_verifikasi/'.$dt_t[0]['transfer_id']); ?>" enctype="multipart/form-data">
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Kode Transfer</label>
								<input type="text" id="transfer_code" name="input[transfer_code]" class="md-form-control md-static floating-label form-control" value="<?php echo $dt_t[0]['transfer_code'] ?>" readonly>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Upload File</label>
								<?php if(count($dt_t)>0 && $dt_t[0]['transfer_file']!=""){ ?>
								<a class="btn btn-default btn-lg" href="<?php echo base_url().'upload/transfer_order/'.$dt_t[0]['transfer_file'] ?>" target="_blank"><i class="fa fa-file"></i> <?php echo $dt_t[0]['transfer_file'] ?></a><br><br>
								<?php } ?>
								<input type="file" id="file" name="file" class="md-form-control md-static floating-label form-control" value="">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Keterangan</label>
								<textarea class="form-control" id="ket" name="ket" rows="3" placeholder="Keterangan.." required=""><?php if(count($dt_t)>0 && $dt_t[0]['transfer_note']!=""){ echo $dt_t[0]['transfer_note']; } ?></textarea>
							</div>
						</div>


					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="setuju" value="setuju" class="btn btn-success">Disetujui</button>
						<button type="submit" name="tolak" value="tolak" class="btn btn-danger pull-right">Ditolak</button>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var wh_id = document.getElementById('warehouse_id').value;
		ganti_asal(wh_id);

		$('.select_ekspedisi').select2({
			allowClear: true,
			placeholder: 'Pilih Ekspedisi',
		});

		$('.select_gudang_asal').select2({
			allowClear: true,
			placeholder: 'Pilih Gudang Asal',
		});

		$('.select_gudang_tujuan').select2({
			allowClear: true,
			placeholder: 'Pilih Gudang Tujuan',
		});
	});

	function ganti_asal(wh_id) {
		$('#frm-tujuan').hide();

		$.ajax({
			type:'post',
			url: '<?php echo site_url('T_inventory/get_wh') ?>',
			data: {
				"warehouse_id":wh_id
			},
			success: function(data){
				var json = $.parseJSON(data);
				if(json.status){
					$('#warehouse_target_id').find('option').remove().end();
					for(var i = 0; i<json.data.length; i++){
						var o = new Option(json.data[i].warehouse_name, json.data[i].warehouse_id);
						$("#warehouse_target_id").append(o);
					}
					detail_wh();
				}
				$("#frm-tujuan").show();
			}
		});
	}

	function detail_wh() {
		var wh_tujuan = document.getElementById("warehouse_target_id").value;
		$.ajax({
			type:'post',
			url: '<?php echo site_url('T_inventory/get_detail_wh') ?>',
			data: {
				"warehouse_id":wh_tujuan
			},
			success: function(data){
				var json = $.parseJSON(data);
				if (json.status) {
					document.getElementById("warehouse_region_id").value = json.data[0]['region_id'];
					document.getElementById("prov").value = json.data[0]['prov'];
					document.getElementById("kota").value = json.data[0]['kota'];
					document.getElementById("kec").value = json.data[0]['kec'];
					document.getElementById("kel").value = json.data[0]['kel'];
					document.getElementById("alamat").value = json.data[0]['alamat'];
				}
			}
		});
	}

	var it = '<?php echo count($dt_t_d); ?>';
	function myCreateFunction() {
		var html = '<tr id="'+it+'">'+
		'<td><select name="input2['+it+'][product_id]" class="PilihBarang form-control md-static" required><option></option><?php foreach ($produk as $d) { ?><option value="<?php echo $d['id_produk'] ?>"><?php echo $d['nama_produk'] ?></option><?php } ?></select></td>'+
		'<td><input type="number" class="form-control" placeholder="Jumlah" name="input2['+ it +'][transfer_detail_quantity]" id="quantity'+ it +'" value="" required></td>'+
		'<td><center><button onclick="myDeleteFunction('+ it +');" class="btn btn-danger"><i class="fas fa-trash"></i></button></center></td>'+
		'<tr>';

		$('#x').append(html);
		$('.PilihBarang').select2({
			allowClear: true,
			placeholder: 'Pilih Barang',
		});
		
		it++;
	}

	function myDeleteFunction(id) {
		$('#' + id).remove();
	}
</script>