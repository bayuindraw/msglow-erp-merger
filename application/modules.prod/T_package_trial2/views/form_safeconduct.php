<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">Form Surat Jalan Driver</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_safeconduct'); ?>" method="Post">
						<?= input_hidden('id', $id) ?>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>KEMASAN</th>
                                <th>JUMLAH KOLI</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id="x"> 
								<?php if(@count(@$arrpackage_safeconduct) > 0){ 
									$i = 0;
									foreach($arrpackage_safeconduct as $indexx => $valuex){
									
								?>
								
								
									<tr id="<?= $i; ?>">
										
										<td><select name="input2[<?= $i; ?>][safeconduct_id]" class="PilihKemasan form-control md-static" required><option></option>
								<?php
									foreach($arrsafeconduct as $indexl => $valuel){
											echo '<option value="'.$valuel['safeconduct_id'].'" '.(($valuex['safeconduct_id'] == $valuel['safeconduct_id'])?'selected':'').'>'.$valuel['safeconduct_name'].'</option>';
									}
								?>
									<td><input type="text" name="input2[<?= $i; ?>][package_safeconduct_quantity]" value="<?= $valuex['package_safeconduct_quantity']; ?>" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static"></td>
										<td><button onclick="myDeleteFunction('<?= $i ?>')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									<?php $i++;
									}
									
								}else{ ?>
									<tr id="0">
										<td><select name="input2[0][safeconduct_id]" class="PilihKemasan form-control md-static" required><option></option><?= $safeconduct_list; ?></select></td>
										<td><input type="text" id="cJumlah" name="input2[0][package_safeconduct_quantity]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static"></td>										
										<td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									</tr> 
								<?php } ?>
                            </tbody>    
                         </table> 
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Kemasan</button>
					<div class="kt-portlet__foot" style="margin-top: 20px;">
            <div class="kt-form__actions">
              <button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;">
                <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
              </button>
            </div>
          </div>
                </form>


            </div>
        </div>
    </div>
</div>


<script>
it = <?= ((@count(@$arrpackage_safeconduct) > 0)?count(@$arrpackage_safeconduct):1); ?>;
function myCreateFunction() { 
	it++;
	var html = '<tr id="'+it+'"><td><select name="input2['+it+'][safeconduct_id]" class="PilihKemasan form-control md-static" required><option></option><?= $safeconduct_list; ?></select></td><td><input type="text" id="cJumlah" name="input2['+it+'][package_safeconduct_quantity]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static"></td><td><button onclick="myDeleteFunction('+it+')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
	$('#x').append(html);
	$('.PilihKemasan').select2({  
	   allowClear: true,
	   placeholder: 'Pilih Kemasan',
	  });
	$(".numeric").mask("#,##0", {reverse: true});
}	
function myDeleteFunction(id) {
	$('#'+id).remove();
}

</script>