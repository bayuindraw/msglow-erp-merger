						<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											Filter
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">

									
        <form id="main" class="form-horizontal" action="<?= site_url($url ); ?>" method="post" novalidate>
         <div class="row">
		  <div class="col-sm-6">
            <label>Tanggal Awal</label>
            <input type="text" id="tgl_awal" name="input[package_date]" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d', strtotime(date('Y-m-d'). ' - 1 days')) ?>" required> 
          </div>
		  <div class="col-sm-6">
            <label>Tanggal Akhir</label>
            <input type="text" id="tgl_akhir" name="input[package_date]" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required> 
          </div>
		 </div>
          <div class="kt-portlet__foot" style="margin-top: 20px;">
            <div class="kt-form__actions">
              <button type="button" onclick="table_search()" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;">
                <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Cari</span>
              </button>
            </div>
          </div>
        </form>
								</div>
							</div>
						<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											<?= $title ?>
										</h3>
									</div>
									<!--<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<a href="<?= site_url().$url."/form/tambah" ?>" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Tambah
												</a>
											</div>
										</div>
									</div>-->
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>No</th>
												<th>Kode</th>
												<th>Seller</th>
												<th>Tanggal</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div> 
							
<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
		"processing": true,
		"serverSide": true,
		"ajax": "<?= site_url() ?>/<?= $url ?>/get_data"
		});
		
		
	});
	function table_search(){
			var tgl_awal = $('#tgl_awal').val();
			var tgl_akhir = $('#tgl_akhir').val();
			var table = $('#kt_table_1').DataTable();
			table.destroy();
			$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",scrollY:        "300px",
			scrollX:        true,
			scrollCollapse: true,
			"processing": true,
			"serverSide": true,
			"ajax": "<?= site_url() ?>/<?= $url ?>/get_data/"+tgl_awal+"/"+tgl_akhir
			});
	}
</script>