<?php
if ($parameter == 'ubah' && $id != '') {
	$this->db->where('account_id', $id);
	$row = $this->Model->get()->row_array();
}
?>
							<div class="row">
								<div class="col-lg-12 col-xl-6">
									<div class="kt-portlet">
										<form method="post" action="<?= site_url($url . '/set_transfer'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Pengirim 
												</h3>  
											</div>
										</div>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Akun</label>
													<select class="form-control" id="exampleSelect1" name="from[account_id]" required>
														<option value="">Pilih Akun</option>
														<?php
														foreach ($m_account as $data) { 
															echo "<option value='" . $data->account_id . "'".(($data->account_id == $role_id)?'selected':'').">" . $data->account_name . "</option>";
														}
														?>
													</select>
												</div>
												<div class="form-group">
													<label>PIC</label>
													<input type="text" class="form-control" placeholder="PIC" name="from[account_detail_pic]" value="<?= @$row['account_detail_pic'] ?>" required>
												</div>
												<div class="form-group">
													<label>Nominal</label> 
													<input type="number" class="form-control" placeholder="Nominal" name="from[account_detail_debit]" value="<?= @$row['account_detail_debit'] ?>" required>
												</div>
												<div class="form-group">
													<label>Catatan</label>
													<textarea class="summernote" name="from[account_detail_note]"><?= @$row['account_detail_note'] ?></textarea>
												</div>
												<div class="form-group">
													<label>Tanggal</label>
													<input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="from[account_detail_date]">
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>