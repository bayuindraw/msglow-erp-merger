<div class="row">
	<div class="col-lg-12 col-xl-12">
		<div class="kt-portlet">
			<form method="post" action="<?= site_url($url . '/simpan_pay_deposit'); ?>" enctype="multipart/form-data">

				<?= input_hidden('id', $id) ?>
				<?= input_hidden('account_detail_real_id', $arraccount_detail['account_detail_real_id']) ?>
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Pembayaran Deposit (Rp <?= @number_format(@$deposit); ?>)
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">


					<input type="hidden" id="max" name="max" value="<?= (@$deposit == "") ? 0 : $deposit; ?>">
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>NO</th>
								<th>RESI</th>
								<th>TANGGAL</th>
								<th>TOTAL TAGIAN</th>
								<th>KURANG BAYAR</th>
								<th>BAYAR</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 0;
							$depolas = @$deposit;
							foreach ($arraccount_detail as $key => $vaData) {
							?>
								<tr>
									<td><input type="hidden" name="account_detail_real_id[<?= $vaData['postage_id'] ?>]" value="<?= $vaData['account_detail_real_id'] ?>" /><?= ++$no ?></td>
									<td><input type="hidden" name="postage_receipt[<?= $vaData['postage_id'] ?>]" value="<?= $vaData['postage_receipt'] ?>" /><?= $vaData['postage_receipt'] ?></td>
									<td><?= $vaData['postage_date'] ?></td>
									<td>Rp <?= number_format($vaData['account_detail_debit']) ?></td>
									<td>Rp <?= number_format($vaData['account_detail_debit'] - $vaData['account_detail_paid']) ?></td>
									<td>
										<?php if ($depolas > 0) { ?>
											<?php if ($depolas >= $vaData['account_detail_debit'] - $vaData['account_detail_paid']) { ?>
												<input type="text" name="paid[<?= $vaData['postage_id'] ?>]" max="<?= $vaData['account_detail_debit'] - $vaData['account_detail_paid'] ?>" class="form-control md-static sum_total numeric" value="<?= $vaData['account_detail_debit'] - $vaData['account_detail_paid'] ?>" onkeyup="chk_total()">
											<?php } else { ?>
												<input type="text" name="paid[<?= $vaData['postage_id'] ?>]" max="<?= $vaData['account_detail_debit'] - $vaData['account_detail_paid'] ?>" class="form-control md-static sum_total numeric" value="<?= $depolas - $vaData['account_detail_debit'] - $vaData['account_detail_paid'] ?>" onkeyup="chk_total()">
											<?php } ?>
										<?php $depolas -= $vaData['account_detail_debit'] - $vaData['account_detail_paid'];
										} else { ?>
											<input type="text" name="paid[<?= $vaData['postage_id'] ?>]" max="<?= $vaData['account_detail_debit'] - $vaData['account_detail_paid'] ?>" class="form-control md-static sum_total numeric" value="0" onkeyup="chk_total()">
										<?php } ?>
									</td>
								</tr>

							<?php } ?>

						</tbody>
					</table>

				</div>




				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary" style="<?= (@$deposit > 0) ? '' : 'display:none;' ?>">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>


		</div>
	</div>
</div>

<script>
	function chk_total(val) {
		var paid = $('#max').val();
		var sum = 0;
		var nilai_awal = 0;
		$('.sum_total').each(function() {
			nilai_awal = $(this).val();
			if (parseFloat(nilai_awal.replaceAll(",", "")) > parseFloat($(this).attr("max"))) {
				$(this).val(commafy($(this).attr("max")));
				sum += parseFloat($(this).attr("max"));
			} else {
				sum += parseFloat(nilai_awal.replaceAll(",", ""));
			}
			//if(parseFloat(this.value) > 0)(

			//}
		});
		if ((paid - sum) < 0) {
			$('#simpan').hide();
		} else {
			$('#simpan').show();
		}
	}
</script>

<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
			<h3 class="kt-portlet__head-title">
				History Ongkos Kirim
			</h3>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
			<thead>
				<tr>
					<th>No</th>
					<th>Resi</th>
					<th>Tanggal</th>
					<th>Total Tagihan</th>
					<th>Kurang Bayar</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<!--end: Datatable -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",
			scrollY: "300px",
			scrollX: true,
			scrollCollapse: true,
			"processing": true,
			"serverSide": true,
			"ajax": "<?= site_url() ?>/<?= $url ?>/get_data_postage/<?= $id ?>"
		});
	});
</script>