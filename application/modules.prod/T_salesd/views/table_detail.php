							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											<table>
												<tr>
													<td>Nomor SO</td>
													<td>&nbsp;&nbsp;:</td>
													<td>&nbsp;&nbsp;<?= $kodeSO = $arrsales_member['sales_code'] ?></td>
												</tr>
												<tr>
													<td>Seller</td>
													<td>&nbsp;&nbsp;:</td>
													<td>&nbsp;&nbsp;<?= $arrsales_member['nama'].' ('.$arrsales_member['kode'].')' ?></td>
												</tr>
											</table>
											
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
										<thead>
											<tr>
											<th>No</th>
											<th>Nama Produk</th>
											<th>Jumlah</th>
											<th>Harga Satuan</th>  
											<th>Sub Total</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$total[] = 0;
											$no = 0;
											foreach ($arrsales_detail as $key => $vaData) {
											?>
											<tr>
												<td><?= ++$no ?></td>
												<td><?= $vaData['nama_produk'] ?></td>
												<td><?= $vaData['sales_detail_quantity'] ?> Pcs</td>
												<td align="right">Rp. <?= number_format($vaData['sales_detail_price']) ?> </td>
												<td align="right">Rp. <?= number_format($total[] = $vaData['sales_detail_price']*$vaData['sales_detail_quantity']) ?></td>
											</tr>
											<?php } ?>
									
										</tbody>
										<tfoot>
											<tr>
												<td colspan="4" align="right" style="font-weight: 900">Total Nominal Sales Order</td>
												<td style="font-weight: 900" align="right">Rp. <?=number_format(array_sum($total))?></td>
											</tr>
										</tfoot>
									</table>
									
								</div>
							</div> 
							
							
							<script>
								<?php if((array_sum($total)-array_sum($total2)) == 0){ ?>
									$('.xhide').hide();
								<?php } ?>
							</script>