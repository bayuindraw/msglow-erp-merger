							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											Seller : <?= $id ?>
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
										<thead>
											<tr>
											<th>No</th>
											<th>Tanggal</th>
											<th>Nama Produk</th>
											<th>Kode SJ</th> 
											<th>User Create</th>
											<th>Total Kirim</th> 
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 0;
											$arrJumlah = array();
											foreach ($arrpending_seller_detail as $key => $vaData) {
											?>
											<tr>
												<td><?= ++$no ?></td>
												<td><?= $vaData['package_date'] ?></td>
												<td><?= $vaData['nama_produk'] ?></td>
												<td><?= $vaData['package_code'] ?></td>
												<td><?= $vaData['user_name']?> </td>
												<td>
													<?php echo number_format($arrJumlah[] = $vaData['package_detail_quantity']) ?>
												</td>
											</tr>
											<?php } ?>
											<tr>
												<td colspan="5" align="right"> Total Kirim</td>
												<td><?=number_format(array_sum($arrJumlah))?></td>
											</tr>
								
										</tbody>
									</table>
								</div>
							</div> 
							