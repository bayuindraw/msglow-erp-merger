
							<div class="row">
							
							<div class="col-lg-12 col-xl-12">
									<div class="kt-portlet">
										
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Form Transfer <br>
													(<?= $factory['nama_factory']; ?>)
												</h3>  
											</div>
										</div>
											<div class="kt-portlet__body">
												
												<div class="form-group">
																						<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>No</th>
												<th>Tanggal</th>
												<th>Jumlah Transfer</th>
												<th>Rekening Tujuan</th>
											</tr>
										</thead>
										<tbody>
                                            <?php for($i = 0; $i <= 10; $i++){ ?>
                                            <tr>
                                                <td><?= $i+1 ?></td>
                                                <td>2020-03-30</td>
                                                <td>Rp <?= number_format(rand(1000000, 1000000000)) ?></td>
                                                <td>BCA (Penjualan)</td>
                                            </tr>
                                            <?php } ?>
										</tbody>
									</table>

												</div>
											</div>
										</div>
									</div>
														

							
<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
		"processing": true,
		// "serverSide": true,
		// "ajax": "<?= site_url() ?>/<?= $url ?>/get_data_transfer/<?= $id ?>"
		});
	});
</script>
								<div class="col-lg-12 col-xl-12">
									<div class="kt-portlet">
										<form method="post" action="<?= site_url($url . '/set_pembayaran'); ?>" enctype="multipart/form-data">
											<?= input_hidden('id', $id) ?>
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Transfer 
												</h3>  
											</div>
										</div>
											<div class="kt-portlet__body">
												<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
					                            <thead>
					                              <tr>
					                                <th>Rekening Tujuan</th>
					                                <th>Nama Pengirim</th>
					                                <th>Tanggal</th>
					                                <th>Nominal</th>
					                                <th>Bukti Transfer</th>
													<th></th>
					                              </tr>
					                            </thead>
					                            <tbody id="x"> 
													
														<tr id="0">
															<td><select class="form-control" id="exampleSelect1" name="account_id[0]" required>
																			<option value="">Pilih Akun</option>
																			<?php
																			foreach ($m_account as $data) { 
																				echo "<option value='" . $data->account_id . "'".(($data->account_id == $role_id)?'selected':'').">" . $data->account_name . "</option>";
																			}
																			?>
																		</select></td>
															
															<td><input type="text" class="form-control" placeholder="Nama Pengirim" name="from[0][account_detail_transfer_name]"></td>										
															<td><input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="from[0][account_detail_date]" required></td>
															
															<td><input type="text" class="form-control numeric paid_total" placeholder="Nominal" name="from[0][account_detail_credit]" value="0" required onkeyup="chk_total()"></td>		
															<td><input type="file" class="form-control" name="transfer_0"></td>										
															<td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
														</tr> 
					                            </tbody>    
					                         </table> 
											 <div class="kt-form__actions">
																		<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Transaksi</button>
																	</div>
										
																	
															<br>
															<br>
															<div class="form-group">
																		<label>Nominal</label>
																		<input type="text" class="form-control numeric" placeholder="Nominal" name="nominal" value="0" id="paid" required onkeyup="chk_total()" readonly>
																	</div>
															<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
											<thead>
												<tr>
												<th>No</th>
												<th>INVOICE</th>
												<th>TANGGAL</th>
												<th>TOTAL TAGIAN</th>
												<th>BAYAR</th>
												<th>KURANG BAYAR</th>
												<th>BAYAR</th> 
												</tr>
											</thead>
											<tbody>
												<?php
												$no = 0;
												foreach ($arrlist_account_sales as $key => $vaData) {
												?>
												<tr>
													<td><?= ++$no ?></td>
													<td onclick="$('#<?= $key ?>x').toggle();"><?= $vaData['account_detail_transfer_name'] ?></td>
													<td><?= $vaData['account_detail_date'] ?></td>
													<td>Rp <?= number_format($vaData['account_detail_price']) ?></td>
													<td>Rp <?= number_format($vaData['account_detail_paid']) ?></td>
													<td><input type="hidden" name="sisa[<?= $vaData['account_detail_real_id'] ?>]" value="<?= $vaData['account_detail_price'] - $vaData['account_detail_paid'] ?>" />Rp <?= number_format($vaData['account_detail_price'] - $vaData['account_detail_paid']) ?></td>	
													<td><input type="text" name="paid[<?= $vaData['account_detail_real_id'] ?>]" max="<?= $vaData['account_detail_price'] - $vaData['account_detail_paid'] ?>" class="form-control md-static sum_total numeric bayar" value="0" onkeyup="chk_total()"><input type="hidden" name="sales[<?= $vaData['account_detail_real_id'] ?>]" value="<?= $vaData['sales_id']; ?>"></td>
												</tr>
												<tr id="<?= $key ?>x" style="display:none;">
														<td></td>
														<td colspan="6">
															<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
														<thead>
															<tr>
															<th>Nama Produk</th>
															<th>Jumlah Pesanan</th>  
															</tr>
														</thead>
														<tbody>
															<?php
															foreach ($arrdetail_sales[$vaData['sales_id']] as $keyx => $valuex) {
															?>
															<tr>
																<td><?= $valuex['nama_produk'] ?></td>
																<td><?= number_format($valuex['sales_detail_quantity']) ?></td>
															</tr>
															<?php } ?>
												
														</tbody>
												</table></td>
											</tr>
												<?php } ?>
									
											</tbody>
										</table>
										<div class="form-group">
																		<label>Deposit</label>
																		<input type="text" name="deposit" id="deposit" readonly class="form-control numeric" value="0" required>
																	</div>
										</div>
										<!--<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Akun Tujuan
												</h3>  
											</div>
										</div>-->
										

											
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>

<script>
	it = 0;
	function myCreateFunction() {
		it++;  
		var html = '<tr id="'+it+'"><td><select class="form-control" id="exampleSelect1" name="account_id['+it+']" required><option value="">Pilih Akun</option><?php foreach ($m_account as $data) { echo "<option value=\'" . $data->account_id . "\'>" . $data->account_name . "</option>";}?></select></td><td><input type="text" class="form-control" placeholder="Nama Pengirim" name="from['+it+'][account_detail_transfer_name]"></td><td><input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="from['+it+'][account_detail_date]" required></td><td><input type="text" class="form-control numeric paid_total" placeholder="Nominal" name="from['+it+'][account_detail_credit]" value="0" required onkeyup="chk_total()"></td><td><input type="file" class="form-control" name="transfer_'+it+'"></td><td><button onclick="myDeleteFunction('+it+')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$(".numeric").mask("#,##0", {reverse: true});
		$('.date_picker, #kt_datepicker_1_validate').datepicker({            
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",            
			format: "yyyy-mm-dd"     
		});
	}
	function myDeleteFunction(id) {
		$('#'+id).remove();
	}
	function chk_total(val){
		var paid_all = 0;
		var val_x = "";	
		$('.paid_total').each(function(){
			val_x = $(this).val();
			val_x = val_x.replaceAll(",", "");
			if(val_x == "") val_x = 0;
			paid_all += parseFloat(val_x);
		});
		$('#paid').val(commafy(paid_all));
		var paid = $('#paid').val();
		paid = paid.replaceAll(",", "");
		var sum = 0;
		var nilai_awal = 0;
		$('.sum_total').each(function(){
			nilai_awal = $(this).val();
			if(parseFloat(paid) > parseFloat($(this).attr("max"))){
				$(this).val(commafy($(this).attr("max")));
				sum += parseFloat($(this).attr("max"));
				$(this).val(commafy($(this).attr("max")));
				paid -= parseFloat($(this).attr("max"));
			}else{
				sum += parseFloat(nilai_awal.replaceAll(",", ""));
				$(this).val(commafy(paid));
				paid -= paid;
			}
				//if(parseFloat(this.value) > 0)(
					
			//}
		});
		$('#deposit').val(commafy(paid));
		// if((paid - sum) < 0){
		// 	$('#simpan').hide();
		// }else{
			$('#simpan').show();
		// }
	}
</script>