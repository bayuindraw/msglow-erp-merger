<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relasi extends CI_Model {


public function PoSOAllMonthly($Bulan="",$Tahun="") {
		$Query = $this->db->query("SELECT 
		v.tanggal,SUM(total_payment) as total_so,(SELECT SUM(s.sales_detail_price * s.sales_detail_quantity) FROM t_sales_detail s where s.sales_id = v.sales_id) as total_po
		FROM v_dashboard_sales_order_up v where month(v.tanggal) = '".$Bulan."' and 
		year(v.tanggal) = '".$Tahun."' GROUP by v.tanggal");
		return $Query;	
}

public function PoSOAllYearly($Tahun="") {
	$Query = $this->db->query("SELECT 
	v.tanggal,SUM(total_payment) as total_so,(SELECT SUM(s.sales_detail_price * s.sales_detail_quantity) FROM t_sales_detail s where s.sales_id = v.sales_id) as total_po
	FROM v_dashboard_sales_order_up v where year(v.tanggal) = '".$Tahun."' GROUP by month(v.tanggal)");
	return $Query;	
}

public function PoUnpaidMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT account_detail_sales_date,
		SUM(account_detail_sales_amount) as amount FROM 
		t_account_detail_sales where month(account_detail_sales_date) = '".$Bulan."' and 
		year(account_detail_sales_date) = '".$Tahun."' and account_detail_sales_amount IS NOT
		NULL group by account_detail_sales_date");
	return $Query;	
}

public function PoPaidMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT account_detail_sales_date,
		SUM(account_detail_sales_amount) as amount FROM 
		t_account_detail_sales where month(account_detail_sales_date) = '".$Bulan."' and 
		year(account_detail_sales_date) = '".$Tahun."' and account_detail_sales_amount IS 
		NOT NULL group by account_detail_sales_date");
	return $Query;	
}

public function SoByQtyMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT ads.account_detail_sales_date,COUNT(ads.account_detail_sales_id) as so_qty 
		FROM t_account_detail_sales ads, t_account_detail_sales_product adsp  where ads.sales_id = adsp.sales_id and 
		adsp.account_detail_sales_product_allow > 0 and 
		month(ads.account_detail_sales_date) = '".$Bulan."' and year(ads.account_detail_sales_date) = '".$Tahun."' 
		group by ads.account_detail_sales_date");
	return $Query;	
}

public function SoByBrandMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT p.brand_name,COUNT(ads.account_detail_sales_id) as so_by_brand 
	  FROM t_account_detail_sales ads, t_account_detail_sales_product adsp,v_produk_dashboard_arga p  where ads.sales_id = adsp.sales_id and 
		p.id_produk = adsp.product_id		and
		adsp.account_detail_sales_product_allow > 0 and month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."' group by p.brand_name");
	return $Query;	
}

public function SoByKategoriMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT p.kategori,COUNT(ads.account_detail_sales_id) as so_by_kategori 
	  FROM t_account_detail_sales ads, t_account_detail_sales_product adsp,v_produk_dashboard_arga p  where ads.sales_id = adsp.sales_id and 
		p.id_produk = adsp.product_id		and
		adsp.account_detail_sales_product_allow > 0 and month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."' group by p.kategori;");
	return $Query;	
}

public function SoByMemberMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT m.`status`,COUNT(ads.account_detail_sales_id) as member 
		FROM t_account_detail_sales ads, t_account_detail_sales_product adsp ,t_sales s,member m
		where 
		ads.sales_id = adsp.sales_id and s.seller_id = m.kode and s.sales_id = ads.sales_id and
		adsp.account_detail_sales_product_allow > 0 and 
		month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."'  and 
		`status` = 'MEMBER' group by m.`status`  ");
	return $Query;	
}

public function SoByResellerMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT m.`status`,COUNT(ads.account_detail_sales_id) as member 
		FROM t_account_detail_sales ads, t_account_detail_sales_product adsp ,t_sales s,member m
		where 
		ads.sales_id = adsp.sales_id and s.seller_id = m.kode and s.sales_id = ads.sales_id and
		adsp.account_detail_sales_product_allow > 0 and 
		month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."'  and 
		`status` = 'RESELLER' group by m.`status`");
	return $Query;	
}

public function SoByAgenMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT m.`status`,COUNT(ads.account_detail_sales_id) as member 
		FROM t_account_detail_sales ads, t_account_detail_sales_product adsp ,t_sales s,member m
		where 
		ads.sales_id = adsp.sales_id and s.seller_id = m.kode and s.sales_id = ads.sales_id and
		adsp.account_detail_sales_product_allow > 0 and 
		month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."'  and 
		`status` = 'AGEN' group by m.`status`");
	return $Query;	
}

public function SoByDistributorMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT m.`status`,COUNT(ads.account_detail_sales_id) as member 
		FROM t_account_detail_sales ads, t_account_detail_sales_product adsp ,t_sales s,member m
		where 
		ads.sales_id = adsp.sales_id and s.seller_id = m.kode and s.sales_id = ads.sales_id and
		adsp.account_detail_sales_product_allow > 0 and 
		month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."'  and 
		`status` = 'DISTRIBUTOR' group by m.`status`");
	return $Query;	
}


public function PoUnpaidYearly($Tahun="") {
	$Query = $this->db->query("SELECT account_detail_sales_date,
		SUM(account_detail_sales_amount) as amount FROM 
		t_account_detail_sales where 
		year(account_detail_sales_date) = '".$Tahun."' and account_detail_sales_amount IS 
		NULL group by month(account_detail_sales_date)");
	return $Query;	
}

public function PoPaidYearly($Tahun="") {
	$Query = $this->db->query("SELECT account_detail_sales_date,
		SUM(account_detail_sales_amount) as amount FROM 
		t_account_detail_sales where 
		year(account_detail_sales_date) = '".$Tahun."' and account_detail_sales_amount IS NOT
		NULL group by month(account_detail_sales_date)");
	return $Query;	
}

public function SoByQtyYearly($Tahun="") {
	$Query = $this->db->query("
		SELECT ads.account_detail_sales_date,COUNT(ads.account_detail_sales_id) as so_qty 
		FROM t_account_detail_sales ads, t_account_detail_sales_product adsp  where ads.sales_id = adsp.sales_id and 
		adsp.account_detail_sales_product_allow > 0 and year(ads.account_detail_sales_date) = '".$Tahun."' 
		group by month(account_detail_sales_date)");
	return $Query;	
}

public function SoByBrandYearly($Tahun="") {
	$Query = $this->db->query("SELECT brand_name,COUNT(account_detail_sales_id) as so_by_brand  FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and year(account_detail_sales_date) = '".$Tahun."' group by brand_name");
	return $Query;	
}

public function SoByKategoriYearly($Tahun="") {
	$Query = $this->db->query("SELECT kategori,COUNT(account_detail_sales_id) as so_by_kategori FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and year(account_detail_sales_date) = '".$Tahun."' group by kategori");
	return $Query;	
}

public function SoBySellerLine($Bulan,$Tahun,$Seller) {
	$Query = $this->db->query("SELECT ads.account_detail_sales_date,`status`,COUNT(ads.account_detail_sales_id) as member 
		FROM t_account_detail_sales ads, t_account_detail_sales_product adsp ,t_sales s,member m
		where 
		ads.sales_id = adsp.sales_id and s.seller_id = m.kode and s.sales_id = ads.sales_id and
		adsp.account_detail_sales_product_allow > 0 and 
		month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."'  and 
		m.`status` = '".$Seller."' group by m.`status`,ads.account_detail_sales_date");
	return $Query;	
}

public function FastMoving($Bulan,$Tahun) {
	$Query = $this->db->query("SELECT p.nama_produk,SUM(adsp.account_detail_sales_product_allow) as jumlah_terjual 
	  FROM t_account_detail_sales ads, t_account_detail_sales_product adsp,v_produk_dashboard_arga p  where ads.sales_id = adsp.sales_id and 
		p.id_produk = adsp.product_id		and
		adsp.account_detail_sales_product_allow > 0 and month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."'  group by p.nama_produk ORDER BY jumlah_terjual DESC LIMIT 10 ");
	return $Query;	
}

public function SlowMoving($Bulan,$Tahun) {
	$Query = $this->db->query("SELECT p.nama_produk,SUM(adsp.account_detail_sales_product_allow) as jumlah_terjual 
	  FROM t_account_detail_sales ads, t_account_detail_sales_product adsp,v_produk_dashboard_arga p  where ads.sales_id = adsp.sales_id and 
		p.id_produk = adsp.product_id		and
		adsp.account_detail_sales_product_allow > 0 and month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."'  group by p.nama_produk ORDER BY jumlah_terjual DESC  ");
	return $Query;	
}

public function TopSeller($Bulan,$Tahun,$Seller) {
	$Query = $this->db->query("SELECT ads.account_detail_sales_date,s.seller_id,m.nama as nama_member,m.`status`,SUM(ads.account_detail_sales_amount) as amount
		FROM t_account_detail_sales ads, t_account_detail_sales_product adsp ,t_sales s,member m
		where 
		ads.sales_id = adsp.sales_id and s.seller_id = m.kode and s.sales_id = ads.sales_id and
		month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."'  and 
		m.`status` = '".$Seller."' group by m.`nama` limit 0,10
");
	return $Query;	
}


public function TopSeller2($Bulan,$Tahun) {
	$Query = $this->db->query("SELECT m.`status` as nama_member,SUM(ads.account_detail_sales_amount) as amount
		FROM t_account_detail_sales ads, t_account_detail_sales_product adsp ,t_sales s,member m
		where 
		ads.sales_id = adsp.sales_id and s.seller_id = m.kode and s.sales_id = ads.sales_id and
		month(ads.account_detail_sales_date) = '".$Bulan."' and 
		year(ads.account_detail_sales_date) = '".$Tahun."'   group by m.`status`
");
	return $Query;	
}

}