<?php
function convertDate($tgl){
    $cDate = explode("-", $tgl);

    $nTahun = $cDate[0];
    $nBulan = $cDate[1];
    $nHari  = $cDate[2];

    if($nBulan == "01"){
        $cBulan = "Jan";
    }else if($nBulan == "02"){
        $cBulan = "Feb";
    }else if($nBulan == "03"){
        $cBulan = "Mar";
    }else if($nBulan == "04"){
        $cBulan = "Apr";
    }else if($nBulan == "05"){
        $cBulan = "Mei";
    }else if($nBulan == "06"){
        $cBulan = "Juni";
    }else if($nBulan == "07"){
        $cBulan = "Juli";
    }else if($nBulan == "08"){
        $cBulan = "Agst";
    }else if($nBulan == "09"){
        $cBulan = "Spt";
    }else if($nBulan == "10"){
        $cBulan = "Okt";
    }else if($nBulan == "11"){
        $cBulan = "Nov";
    }else if($nBulan == "12"){
        $cBulan = "Des";
    }

    $cFix = $nHari." ".$cBulan;
    return $cFix;

}

function convertDate2($tgl){
    $cDate = explode("-", $tgl);

    $nTahun = $cDate[0];
    $nBulan = $cDate[1];
    $nHari  = $cDate[2];

    if($nBulan == "01"){
        $cBulan = "Jan";
    }else if($nBulan == "02"){
        $cBulan = "Feb";
    }else if($nBulan == "03"){
        $cBulan = "Mar";
    }else if($nBulan == "04"){
        $cBulan = "Apr";
    }else if($nBulan == "05"){
        $cBulan = "Mei";
    }else if($nBulan == "06"){
        $cBulan = "Juni";
    }else if($nBulan == "07"){
        $cBulan = "Juli";
    }else if($nBulan == "08"){
        $cBulan = "Agst";
    }else if($nBulan == "09"){
        $cBulan = "Spt";
    }else if($nBulan == "10"){
        $cBulan = "Okt";
    }else if($nBulan == "11"){
        $cBulan = "Nov";
    }else if($nBulan == "12"){
        $cBulan = "Des";
    }

    $cFix = $cBulan;
    return $cFix;

}
?>


<link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />

<div id="kt_morris_2" style="height:500px;"></div>


<script src="<?=base_url()?>web/plugins/jquery.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/plugins/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<!--begin::Page Vendors(used by this page)-->
        <script src="//www.amcharts.com/lib/3/amcharts.js"></script>
        <script src="//www.amcharts.com/lib/3/serial.js"></script>
        <script src="//www.amcharts.com/lib/3/radar.js"></script>
        <script src="//www.amcharts.com/lib/3/pie.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
        <script src="//www.amcharts.com/lib/3/themes/light.js"></script>
        <!--end::Page Vendors-->


<script type="text/javascript">

"use strict";

// Class definition
var KTamChartsChartsDemo = function() {

  

    var demoALL = function() {
        var chart = AmCharts.makeChart("kt_morris_2", {
            "theme": "light",
            "type": "serial",
            "dataProvider": [
		
        
			<?php 
             if($row->num_rows() > 0){
                     foreach ($row->result_array() as $key => $vaDataPoSOAll1) {               
    		?>
            {
                "country": "<?=convertDate2($vaDataPoSOAll1['tanggal'])?>",
                "year2004": <?=$vaDataPoSOAll1['total_po']?>,
                "year2005": <?=$vaDataPoSOAll1['total_so']?>
            },
           <?php } ?>
		   <?php }else{ ?>
		    {
                "country": "<?=date("Y-m-d")?>",
                "year2004": 0,
                "year2005": 0
            },
		   <?php } ?>
            

            ],
            "valueAxes": [{
                "stackType": "3d",
                "unit": "%",
                "position": "left",
                "title": "Purchase Requisition & Sales Order",
            }],
            "startDuration": 1,
            

            "graphs": [

            {
                "id": "graph2",
                "balloonText": "<span style='font-size:12px;'>SO : [[category]]:<br><span style='font-size:20px;'>Amount : [[value]]</span></span>",
                "bullet": "round",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "SO",
                "valueField": "year2005",
                "dashLengthField": "dashLengthLine"
            },
            {

                "balloonText": "<span style='font-size:12px;'>PO : [[category]]:<br><span style='font-size:20px;'>Amount : [[value]]</span></span>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "PO",
                "type": "column",
                "fillColors": "#0abb87",
                "valueField": "year2004"
            }

            ],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 60,
            "angle": 30,
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "export": {
                "enabled": true
            }
        });
    }

   

    return {
        // public functions
        init: function() {
           
            demoALL();
           
        }
    };
}();

jQuery(document).ready(function() {
    KTamChartsChartsDemo.init();
});
</script>