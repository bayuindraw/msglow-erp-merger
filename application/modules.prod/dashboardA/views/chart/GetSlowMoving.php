<?php
function convertDate($tgl){
    $cDate = explode("-", $tgl);

    $nTahun = $cDate[0];
    $nBulan = $cDate[1];
    $nHari  = $cDate[2];

    if($nBulan == "01"){
        $cBulan = "Jan";
    }else if($nBulan == "02"){
        $cBulan = "Feb";
    }else if($nBulan == "03"){
        $cBulan = "Mar";
    }else if($nBulan == "04"){
        $cBulan = "Apr";
    }else if($nBulan == "05"){
        $cBulan = "Mei";
    }else if($nBulan == "06"){
        $cBulan = "Juni";
    }else if($nBulan == "07"){
        $cBulan = "Juli";
    }else if($nBulan == "08"){
        $cBulan = "Agst";
    }else if($nBulan == "09"){
        $cBulan = "Spt";
    }else if($nBulan == "10"){
        $cBulan = "Okt";
    }else if($nBulan == "11"){
        $cBulan = "Nov";
    }else if($nBulan == "12"){
        $cBulan = "Des";
    }

    $cFix = $nHari." ".$cBulan;
    return $cFix;

}
?>


<link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />

<div id="kt_morris_11" style="height:500px;"></div>


<script src="<?=base_url()?>web/plugins/jquery.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/plugins/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<!--begin::Page Vendors(used by this page)-->
        <script src="//www.amcharts.com/lib/3/amcharts.js"></script>
        <script src="//www.amcharts.com/lib/3/serial.js"></script>
        <script src="//www.amcharts.com/lib/3/radar.js"></script>
        <script src="//www.amcharts.com/lib/3/pie.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
        <script src="//www.amcharts.com/lib/3/themes/light.js"></script>
        <!--end::Page Vendors-->


<script type="text/javascript">

"use strict";

// Class definition
var KTamChartsChartsDemo = function() {

  

    var demo9 = function() {
        var chart = AmCharts.makeChart("kt_morris_11", {
            "type": "serial",
            "theme": "light",
            "handDrawn": true,
            "handDrawScatter": 3,
            "legend": {
                "useGraphSettings": true,
                "markerSize": 12,
                "valueWidth": 0,
                "verticalGap": 0
            },
            "dataProvider": [

            <?php 
            	$no = 1 ;
                if($row->num_rows() > 0){
                	
                     foreach ($row->result_array() as $key => $vaData8) {
                     $nTerjual = (!empty($vaData['jumlah_terjual'])) ? $vaData['jumlah_terjual'] : "0";    
                      if($vaData8['jumlah_terjual'] < 5000 and $vaData8['jumlah_terjual'] != 0){            
                ?>
            
            <?php 
            if($no < 11){
            ?>
            {
                "year": "<?=$vaData8['nama_produk']?>",
                "income": <?=$vaData8['jumlah_terjual']?>,
                "expenses": <?=$vaData8['jumlah_terjual']?>
            }, 
        <?php ++$no;} ?>
             <?php }} ?>
            <?php }else{ ?>
            	{
                "year": 0,
                "income": 0,
                "expenses": 0
            }, 
            <?php  } ?>


            ],
            "valueAxes": [{
                "minorGridAlpha": 0.08,
                "minorGridEnabled": true,
                "position": "top",
                "axisAlpha": 0
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b></span>",
                "title": "Terjual",
                "type": "column",
                "fillAlphas": 0.8,

                "valueField": "income"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b></span>",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "fillAlphas": 0,
                "lineThickness": 2,
                "lineAlpha": 1,
                "bulletSize": 7,
                "title": "Terjual",
                "valueField": "expenses"
            }],
            "rotate": true,
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "export": {
                "enabled": true
            }

        });
    }

   

    return {
        // public functions
        init: function() {
           
            demo9();
           
        }
    };
}();

jQuery(document).ready(function() {
    KTamChartsChartsDemo.init();
});
</script>