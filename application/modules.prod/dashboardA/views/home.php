<?php
//$xx = @$this->uri->segment(3, 0);
//if($xx == "stock_opname_produk" && $xx != 0){ 
//}else if (@!$_SESSION['logged_in'] && $allow != 1) {
	
//	redirect(site_url() . "Login");
//}

function convertDate($tgl){
	$cDate = explode("-", $tgl);

	$nTahun = $cDate[0];
	$nBulan	= $cDate[1];
	$nHari  = $cDate[2];

	if($nBulan == "01"){
		$cBulan = "Jan";
	}else if($nBulan == "02"){
		$cBulan = "Feb";
	}else if($nBulan == "03"){
		$cBulan = "Mar";
	}else if($nBulan == "04"){
		$cBulan = "Apr";
	}else if($nBulan == "05"){
		$cBulan = "Mei";
	}else if($nBulan == "06"){
		$cBulan = "Juni";
	}else if($nBulan == "07"){
		$cBulan = "Juli";
	}else if($nBulan == "08"){
		$cBulan = "Agst";
	}else if($nBulan == "09"){
		$cBulan = "Spt";
	}else if($nBulan == "10"){
		$cBulan = "Okt";
	}else if($nBulan == "11"){
		$cBulan = "Nov";
	}else if($nBulan == "12"){
		$cBulan = "Des";
	}

	$cFix = $nHari." ".$cBulan;
	return $cFix;

}
?>
<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
	<base href="../../../">
	<meta charset="utf-8" />
	<title>MSGLOW OFFICE</title>
	<meta name="description" content="Amcharts charts examples">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--begin::Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

	<!--end::Fonts -->

	<!--begin::Page Vendors Styles(used by this page) -->
	<link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />

	<!--end::Page Vendors Styles -->

	<!--begin::Global Theme Styles(used by all pages) -->

	<!--begin:: Vendor Plugins -->
	
	<link href="<?=base_url()?>web/dashboard/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />

	<link href="<?=base_url()?>web/dashboard/plugins/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>web/dashboard/plugins/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>web/dashboard/plugins/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>web/dashboard/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />

	<!--end:: Vendor Plugins -->
	<link href="<?=base_url()?>web/dashboard/style.bundle.dashboard.css" rel="stylesheet" type="text/css" />

	<!--begin:: Vendor Plugins for custom pages -->
	<link href="<?=base_url()?>web/dashboard/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	

	<!--end:: Vendor Plugins for custom pages -->

	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->

	<!--end::Layout Skins -->
	<link rel="shortcut icon" href="https://msglow.app/upload/nitrogen.png" />
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">

	<!-- begin:: Page -->

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="#">
				<img alt="Logo" src="https://msglow.app/upload/logo_nitromerah.png" style="max-height: 20px;" />
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
		</div>
	</div>

	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

			<!-- begin:: Aside -->
			<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
			<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

				<!-- begin:: Aside Menu -->
				<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
					<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
						<ul class="kt-menu__nav ">
							<li class="kt-menu__item " aria-haspopup="true"><a href="<?=base_url()?>" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-protection"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
							
						</ul>
					</div>
				</div>

				<!-- end:: Aside Menu -->
			</div>

			<!-- end:: Aside -->
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

				<!-- begin:: Header -->
				<div id="kt_header" class="kt-header kt-grid kt-grid--ver  kt-header--fixed ">

					<!-- begin:: Aside -->
					<div class="kt-header__brand kt-grid__item  " id="kt_header_brand">
						<div class="kt-header__brand-logo">
							<a href="index.html">
								<img alt="Logo" src="<?=base_url()?>web/media/logos/logo-6.png" />
							</a>
						</div>
					</div>

					<!-- end:: Aside -->

					<!-- begin:: Title -->
					<h3 class="kt-header__title kt-grid__item">
						Dashboard
					</h3>

					<!-- end:: Title -->

					<!-- begin: Header Menu -->
					<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
					<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
						<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
							<ul class="kt-menu__nav ">
								<li class="kt-menu__item " aria-haspopup="true"><a href="<?=base_url()?>" class="kt-menu__link "><span class="kt-menu__link-text">Home</span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<!-- end:: Header -->
				<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
					<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
					
					<br/>
					<div class="row">

					<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							  Purchase Requisition & Sales Order; By Amount
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 <div class="col-lg-12">		
						  <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							<select name="cKategoriPoSoJoin" id="cKategoriPoSoJoin" onChange="return filterkategoriposojoin(this.value);" class="form-control">
								<option></option>
								<option value="bulan" selected>Monthly</option>
								<option value="tahun" >Yearly</option>
							</select> 
							<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-cogs"></i></span></span>
						   </div>
						   </div>
						   <div class="col-lg-12" id="bulan_po_so">
						    <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							<br/>
							 <select name="cBulanPoSoJoin" id="cBulanPoSoJoin" class="form-control">
								<option></option>
								<option value="01" >Januari</option>
								<option value="02" >Februari</option>
								<option value="03" >Maret</option>
								<option value="04" >April</option>
								<option value="05" >Mei</option>
								<option value="06" >Juni</option>
								<option value="07" >Juli</option>
								<option value="08" >Agustus</option>
								<option value="09" >September</option>
								<option value="10" >Oktober</option>
								<option value="11" >November</option>
								<option value="12" selected>Desember</option>
							</select> 
							<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
							</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunPoSoJoin" id="cTahunPoSoJoin" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>						
						  <div class="col-lg-12"><br/>
						   <button onclick="return GetPoSoJoin();" 
						   type="button" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
						    </div>
					 		<div style="overflow-x:auto;"  class="box-body">
							<div id="div_po_so_join">
								<div id="kt_morris_2" style="height:500px;"></div>
							</div>
						</div>
				    	</div>
						</div>
					</div>
				

<script type="text/javascript">
							function GetPoSoJoin(){
								var cBulanPoSoJoin = $('#cBulanPoSoJoin').val();
								var cTahunPoSoJoin = $('#cTahunPoSoJoin').val();
								var cKategoriPoSoJoin = $('#cKategoriPoSoJoin').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetPoSoJoin')?>/"+cBulanPoSoJoin+'/'+cTahunPoSoJoin+'/'+cKategoriPoSoJoin,
									cache: false,
									beforeSend:function(){
										$('#div_po_so_join').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
										$('#div_po_so_join').html(msg);
									}
								});  
								
							}

							function filterkategoriposojoin(){
								var cKategoriPoSoJoin = $('#cKategoriPoSoJoin').val();

								if(cKategoriPoSoJoin == 'tahun'){
								document.getElementById("bulan_po_so").style.display = "none";
								}else if(cKategoriPoSoJoin == 'bulan'){
									document.getElementById("bulan_po_so").style.display = "block";
								}
							}
						</script>


						<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							 How many Purchase Requisition (unpaid)
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 <div class="col-lg-12">		
						  <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
						  <select onchange="filterkategoriunpaid()" name="cKategoriPoUnpaid" id="cKategoriPoUnpaid" class="form-control">
								<option></option>
								<option value="bulan" selected>Monthly</option>
								<option value="tahun" >Yearly</option>
							</select> 
							<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-cogs"></i></span></span>
						   </div>
						   </div>
						   <div class="col-lg-12" id="bulan_unpaid">
						    <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							<br/>
							<select name="cBulanPoUnpaid" id="cBulanPoUnpaid" class="form-control">
								<option></option>
								<option value="01" >Januari</option>
								<option value="02" >Februari</option>
								<option value="03" >Maret</option>
								<option value="04" >April</option>
								<option value="05" >Mei</option>
								<option value="06" >Juni</option>
								<option value="07" >Juli</option>
								<option value="08" >Agustus</option>
								<option value="09" >September</option>
								<option value="10" >Oktober</option>
								<option value="11" >November</option>
								<option value="12" selected>Desember</option>
							</select> 
							<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
							</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunPoUnpaid" id="cTahunPoUnpaid" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								<option value="2022" >2022</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>						
				
						  <div class="col-lg-12"><br/>
						  <button onclick="return GetPoUnpaidMonthly();" type="button" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button> 
						    </div>
							<div style="overflow-x:auto;"  class="box-body">
							<div id="div_po_unpaid">
								<div id="kt_morris_3" style="height:500px;"></div>
							</div>
						</div>
				    	</div>
						</div>
					</div>
					<script type="text/javascript">
							function GetPoUnpaidMonthly(){
								var cBulanPoUnpaid = $('#cBulanPoUnpaid').val();
								var cTahunPoUnpaid = $('#cTahunPoUnpaid').val();
								var cKategoriPoUnpaid = $('#cKategoriPoUnpaid').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetPoUnpaidMonthly')?>/"+cBulanPoUnpaid+'/'+cTahunPoUnpaid+'/'+cKategoriPoUnpaid,
									cache: false,
									beforeSend:function(){
										$('#div_po_unpaid').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
										$('#div_po_unpaid').html(msg);
									}
								});  
								
							}

							function filterkategoriunpaid(){
								var cKategoriPoUnpaid = $('#cKategoriPoUnpaid').val();

								if(cKategoriPoUnpaid == 'tahun'){
								document.getElementById("bulan_unpaid").style.display = "none";
								}else if(cKategoriPoUnpaid == 'bulan'){
									document.getElementById("bulan_unpaid").style.display = "block";
								}
							}
						</script>



						<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							  How many PR become Sales Order (paid)
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 <div class="col-lg-12">		
						  <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
						  <select onchange="return filterkategoripaid();" name="cKategoriPopaid" id="cKategoriPopaid" class="form-control">
								<option></option>
								<option value="bulan" selected>Monthly</option>
								<option value="tahun" >Yearly</option>
																
						</select>  
							<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-cogs"></i></span></span>
						   </div>
						   </div>
						   <div class="col-lg-12" id="bulan_paid">
							<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							<br/>
							<select name="cBulanPopaid" id="cBulanPopaid" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" >November</option>
																<option value="12" selected>Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunPoPaid" id="cTahunPoPaid" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								<option value="2022" >2022</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>						
					
						  <div class="col-lg-12"><br/>
						  <button onclick="return GetPopaidMonthly();" type="button" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search  Report</button>
						    </div>
							<div style="overflow-x:auto;"  class="box-body">
						   <div id="div_po_paid">
							<div id="kt_morris_4" style="height:500px;"></div>
						   </div>
						</div>
				    	</div>
						</div>
					</div>
					<script type="text/javascript">
							function GetPopaidMonthly(){
								var cBulanPopaid = $('#cBulanPopaid').val();
								var cTahunPopaid = $('#cTahunPoPaid').val();
								var cKategoriPopaid = $('#cKategoriPopaid').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetPoPaidMonthly')?>/"+cBulanPopaid+'/'+cTahunPopaid+'/'+cKategoriPopaid,
									cache: false,
									beforeSend:function(){
									$('#div_po_paid').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
										$('#div_po_paid').html(msg);
									}
								});  
							}

							function filterkategoripaid(){
								var cKategoriPopaid = $('#cKategoriPopaid').val();

								if(cKategoriPopaid == 'tahun'){
								document.getElementById("bulan_paid").style.display = "none";
								}else if(cKategoriPopaid == 'bulan'){
									document.getElementById("bulan_paid").style.display = "block";
								}
							}

							
						</script>



<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							  How many Sales Order based on Quantity 
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 <div class="col-lg-12">		
						  <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
						  <select name="cKategoriPoSo" id="cKategoriPoSo" onchange="return filterkategoriposo();" class="form-control">
								<option></option>
								<option value="bulan" selected>Monthly</option>
								<option value="tahun" >Yearly</option>
							</select> 
							<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-cogs"></i></span></span>
						   </div>
						   </div>
						   <div class="col-lg-12" id="bulan_poso">
						    <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							<br/>
							<select name="cBulanPoSo" id="cBulanPoSo" class="form-control">
								<option></option>
								<option value="01" >Januari</option>
								<option value="02" >Februari</option>
								<option value="03" >Maret</option>
								<option value="04" >April</option>
								<option value="05" >Mei</option>
								<option value="06" >Juni</option>
								<option value="07" >Juli</option>
								<option value="08" >Agustus</option>
								<option value="09" >September</option>
								<option value="10" >Oktober</option>
								<option value="11" >November</option>
								<option value="12" selected>Desember</option>
							</select> 
							<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
							</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunPoSo" id="cTahunPoSo" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								<option value="2022" >2022</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>						
					
						  <div class="col-lg-12"><br/>
						  <button onclick="return GetPoSo();" type="button" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
						    </div>
							<div style="overflow-x:auto;"  class="box-body">
							<div id="div_po_so">
								<div id="kt_morris_5" style="height:500px;"></div>
							</div>
						</div>
				    	</div>
						</div>
					</div>
					<script type="text/javascript">
							function GetPoSo(){
								var cBulanPoSo = $('#cBulanPoSo').val();
								var cTahunPoSo = $('#cTahunPoSo').val();
								var cKategoriPoSo = $('#cKategoriPoSo').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByQtyMonthly')?>/"+cBulanPoSo+'/'+cTahunPoSo+'/'+cKategoriPoSo,
									cache: false,
									beforeSend:function(){
									$('#div_po_so').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_po_so').html(msg);
									}
								});  
							}

							function filterkategoriposo(){
								var cKategoriPoSo = $('#cKategoriPoSo').val();

								if(cKategoriPoSo == 'tahun'){
								document.getElementById("bulan_poso").style.display = "none";
								}else if(cKategoriPoSo == 'bulan'){
									document.getElementById("bulan_poso").style.display = "block";
								}
							}
						</script>


<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							  Sales Order Quantity, By Category and Brand - Bar Chart
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 <div class="col-lg-12">		
						  <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
						  <select name="cKategoriBrandKategori" id="cKategoriBrandKategori"  class="form-control">
								<option></option>
								<option value="kategori" selected>Category</option>
								<option value="brand" >Brand</option>
							</select> 
							<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-cogs"></i></span></span>
						   </div>
						   </div>
						   <div class="col-lg-12">
						    <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							<br/>
							<select name="cBulanKategori1" id="cBulanKategori1" class="form-control">
								<option></option>
								<option value="01" >Januari</option>
								<option value="02" >Februari</option>
								<option value="03" >Maret</option>
								<option value="04" >April</option>
								<option value="05" >Mei</option>
								<option value="06" >Juni</option>
								<option value="07" >Juli</option>
								<option value="08" >Agustus</option>
								<option value="09" >September</option>
								<option value="10" >Oktober</option>
								<option value="11" >November</option>
								<option value="12" selected>Desember</option>
							</select> 
							<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
							</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunKategori1" id="cTahunKategori1" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								<option value="2022" >2022</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>						
					
						  <div class="col-lg-12"><br/>
						  <button  type="button" onclick="return GetSoKategori1();" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
						    </div>
							<div style="overflow-x:auto;"  class="box-body">
							<div id="kt_morris_7_div">
								<div id="kt_morris_7" style="height:500px;"></div>
							</div>
						</div>
				    	</div>
						</div>
					</div>
					<script type="text/javascript">
							function GetSoKategori1(){
								var cKategoriBrandKategori = $('#cKategoriBrandKategori').val();
								var cBulanKategori1 = $('#cBulanKategori1').val();
								var cTahunKategori1 = $('#cTahunKategori1').val();

								if(cKategoriBrandKategori == "kategori"){
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByCategoryMonthlyBar')?>/"+cBulanKategori1+'/'+cTahunKategori1,
									cache: false,
									beforeSend:function(){
									$('#kt_morris_7_div').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#kt_morris_7_div').html(msg);
									}
								});  
							   }else{
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByBrandMonthlyBar')?>/"+cBulanKategori1+'/'+cTahunKategori1,
									cache: false,
									beforeSend:function(){
									$('#kt_morris_7_div').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#kt_morris_7_div').html(msg);
									}
								});  
							   }
							}
							

							
						</script>


<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							  Sales Order Quantity, By Category and Brand - Line Chart 
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 <div class="col-lg-12">		
						  <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
						  <select name="cKategoriBrandKategori2" id="cKategoriBrandKategori2"  class="form-control">
								<option></option>
								<option value="kategori" selected>Category</option>
								<option value="brand" >Brand</option>
							</select> 
							<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-cogs"></i></span></span>
						   </div>
						   </div>
						   <div class="col-lg-12">
						    <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							<br/>
							<select name="cBulanKategori2" id="cBulanKategori2" class="form-control">
								<option></option>
								<option value="01" >Januari</option>
								<option value="02" >Februari</option>
								<option value="03" >Maret</option>
								<option value="04" >April</option>
								<option value="05" >Mei</option>
								<option value="06" >Juni</option>
								<option value="07" >Juli</option>
								<option value="08" >Agustus</option>
								<option value="09" >September</option>
								<option value="10" >Oktober</option>
								<option value="11" >November</option>
								<option value="12" selected>Desember</option>
							</select> 
							<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
							</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunKategori2" id="cTahunKategori2" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								<option value="2022" >2022</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>						
					
						  <div class="col-lg-12"><br/>
						  <button  type="button" onclick="return GetSoKategori2();" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
						    </div>
							<div style="overflow-x:auto;"  class="box-body">
							<div id="div_so_brand_kategori_2">
							<div id="kt_morris_6" style="height:500px;"></div>
						 </div>
						</div>
				    	</div>
						</div>
					</div>
					<script type="text/javascript">
							function GetSoKategori2(){
								var cKategoriBrandKategori2 = $('#cKategoriBrandKategori2').val();
								var cBulanKategori2 = $('#cBulanKategori2').val();
								var cTahunKategori2 = $('#cTahunKategori2').val();

								if(cKategoriBrandKategori2 == "kategori"){
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByCategoryMonthlyLine')?>/"+cBulanKategori2+'/'+cTahunKategori2,
									cache: false,
									beforeSend:function(){
									$('#div_so_brand_kategori_2').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_so_brand_kategori_2').html(msg);
									}
								});  
							   }else{
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByBrandMonthlyLine')?>/"+cBulanKategori2+'/'+cTahunKategori2,
									cache: false,
									beforeSend:function(){
									$('#div_so_brand_kategori_2').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_so_brand_kategori_2').html(msg);
									}
								});  
							   }
							}
							

							
						</script>

<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							  Sales Order Quantity, By Seller - Bar Chart 
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 
						   <div class="col-lg-12">
						    <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							
							<select name="cBulanSeller1" id="cBulanSeller1" class="form-control">
								<option></option>
								<option value="01" >Januari</option>
								<option value="02" >Februari</option>
								<option value="03" >Maret</option>
								<option value="04" >April</option>
								<option value="05" >Mei</option>
								<option value="06" >Juni</option>
								<option value="07" >Juli</option>
								<option value="08" >Agustus</option>
								<option value="09" >September</option>
								<option value="10" >Oktober</option>
								<option value="11" >November</option>
								<option value="12" selected>Desember</option>
							</select> 
							<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
							</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunSeller1" id="cTahunSeller1" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								<option value="2022" >2022</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>	
						  <div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cMemberSeller1" id="cMemberSeller1" class="form-control">
																<option>All</option>
																
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>					
					
						  <div class="col-lg-12"><br/>
						  <button onclick="return GetSoSeller1();" type="button" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
						    </div>
							<div style="overflow-x:auto;"  class="box-body">
							<div id="div_so_seller_1">
							<div id="kt_morris_8" style="height:500px;"></div>
						   </div>
						</div>
				    	</div>
						</div>
					</div>
					<script type="text/javascript">
							function GetSoSeller1(){
								var cBulanSeller1 = $('#cBulanSeller1').val();
								var cTahunSeller1 = $('#cTahunSeller1').val();
								

								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoSellerBar')?>/"+cBulanSeller1+'/'+cTahunSeller1,
									cache: false,
									beforeSend:function(){
									$('#div_so_seller_1').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_so_seller_1').html(msg);
									}
								});  

								
							}
						</script>
<!-- --->

<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							  Sales Order Quantity, By Seller - Line Chart
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 
						   <div class="col-lg-12">
						    <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
						
							<select name="cBulanSeller2" id="cBulanSeller2" class="form-control">
								<option></option>
								<option value="01" >Januari</option>
								<option value="02" >Februari</option>
								<option value="03" >Maret</option>
								<option value="04" >April</option>
								<option value="05" >Mei</option>
								<option value="06" >Juni</option>
								<option value="07" >Juli</option>
								<option value="08" >Agustus</option>
								<option value="09" >September</option>
								<option value="10" >Oktober</option>
								<option value="11" >November</option>
								<option value="12" selected>Desember</option>
							</select> 
							<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
							</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunSeller2" id="cTahunSeller2" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								<option value="2022" >2022</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>	
						  <div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
														<select name="cMemberSeller2" id="cMemberSeller2" class="form-control">
																<option>--Pilih Seller--</option>
																
																<option value="Member" >Member</option>
																<option value="RESELLER" >RESELLER</option>
																<option value="AGEN" >AGEN</option>
																<option value="DISTRIBUTOR" >DISTRIBUTOR</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>					
					
						  <div class="col-lg-12"><br/>
						  <button onclick="return GetSoSeller2();" type="button" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
						    </div>
							<div style="overflow-x:auto;"  class="box-body">
							<div id="div_so_seller_2">
								<div id="kt_morris_9" style="height:500px;"></div>
							</div>
						</div>
				    	</div>
						</div>
					</div>
					<script type="text/javascript">
							function GetSoSeller2(){
								var cBulanSeller2 = $('#cBulanSeller2').val();
								var cTahunSeller2 = $('#cTahunSeller2').val();
								var cMemberSeller2 = $('#cMemberSeller2').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoSellerLine')?>/"+cBulanSeller2+'/'+cTahunSeller2+'/'+cMemberSeller2,
									cache: false,
									beforeSend:function(){
									$('#div_so_seller_2').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_so_seller_2').html(msg);
									}
								});  
								
							}
						</script>

						
<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							  Top 10 Best Seller Products  
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 
						   <div class="col-lg-12">
						    <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
						
							<select name="cBulanFastMoving" id="cBulanFastMoving" class="form-control">
								<option></option>
								<option value="01" >Januari</option>
								<option value="02" >Februari</option>
								<option value="03" >Maret</option>
								<option value="04" >April</option>
								<option value="05" >Mei</option>
								<option value="06" >Juni</option>
								<option value="07" >Juli</option>
								<option value="08" >Agustus</option>
								<option value="09" >September</option>
								<option value="10" >Oktober</option>
								<option value="11" >November</option>
								<option value="12" selected>Desember</option>
							</select> 
							<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
							</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunFastMoving" id="cTahunFastMoving" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								<option value="2022" >2022</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>	
						 			
					
						  <div class="col-lg-12"><br/>
						  <button onclick="return FastMoving();" type="button" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
						    </div>
							<div style="overflow-x:auto;"  class="box-body">
							<div id="div_fast_moving">
							<div id="kt_morris_10" style="height:500px;"></div>
						  </div>
						</div>
				    	</div>
						</div>
					</div>
					<script type="text/javascript">
							function FastMoving(){
								var cBulanFastMoving = $('#cBulanFastMoving').val();
								var cTahunFastMoving = $('#cTahunFastMoving').val();
				
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetFastMoving')?>/"+cBulanFastMoving+'/'+cTahunFastMoving,
									cache: false,
									beforeSend:function(){
									$('#div_fast_moving').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_fast_moving').html(msg);
									}
								});  
								
							}
						</script>

<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							  Top 10 Slow Moving Products 
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 
						   <div class="col-lg-12">
						    <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
						
							<select name="cBulanSlowMoving" id="cBulanSlowMoving" class="form-control">
								<option></option>
								<option value="01" >Januari</option>
								<option value="02" >Februari</option>
								<option value="03" >Maret</option>
								<option value="04" >April</option>
								<option value="05" >Mei</option>
								<option value="06" >Juni</option>
								<option value="07" >Juli</option>
								<option value="08" >Agustus</option>
								<option value="09" >September</option>
								<option value="10" >Oktober</option>
								<option value="11" >November</option>
								<option value="12" selected>Desember</option>
							</select> 
							<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
							</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunSlowMoving" id="cTahunSlowMoving" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								<option value="2022" >2022</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>	
						 			
					
						  <div class="col-lg-12"><br/>
						  <button onclick="return SlowMoving();" type="button" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
						    </div>
							<div style="overflow-x:auto;"  class="box-body">
							<div id="div_slow_moving">
							<div id="kt_morris_11" style="height:500px;"></div>
						  </div>
						</div>
				    	</div>
						</div>
					</div>
					<script type="text/javascript">
							function SlowMoving(){
								var cBulanSlowMoving = $('#cBulanSlowMoving').val();
								var cTahunSlowMoving = $('#cTahunSlowMoving').val();
				
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSlowMoving')?>/"+cBulanSlowMoving+'/'+cTahunSlowMoving,
									cache: false,
									beforeSend:function(){
									$('#div_slow_moving').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_slow_moving').html(msg);
									}
								});  
								
							}
						</script>
						
<div class="col-lg-12">
					 	<div class="kt-portlet">
					   	<div class="kt-portlet__head">
						 	<div class="kt-portlet__head-label">
						  	<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
						  	</span>
						  	<h3 class="kt-portlet__head-title">
							  Top Seller - Monthly
						  	 </h3>
							</div>
						</div>
						<div class="kt-portlet__body">
						 
						   <div class="col-lg-12">
						    <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
						
							<select name="cBulanTopSeller" id="cBulanTopSeller" class="form-control">
								<option></option>
								<option value="01" >Januari</option>
								<option value="02" >Februari</option>
								<option value="03" >Maret</option>
								<option value="04" >April</option>
								<option value="05" >Mei</option>
								<option value="06" >Juni</option>
								<option value="07" >Juli</option>
								<option value="08" >Agustus</option>
								<option value="09" >September</option>
								<option value="10" >Oktober</option>
								<option value="11" >November</option>
								<option value="12" selected>Desember</option>
							</select> 
							<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
							</div>
							<div class="col-lg-12">
						     <div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
							   <br/>
							   <select name="cTahunTopSeller" id="cTahunTopSeller" class="form-control">
								<option></option>
								<option value="2019" >2019</option>
								<option value="2020" >2020</option>
								<option value="2021" selected>2021</option>
								<option value="2022" >2022</option>
								</select> 
								<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
							</div>
						  </div>	
						  <div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
														<select name="cTopSeller" id="cTopSeller" class="form-control">
																<option>--Choose Seller--</option>
																<option value="All" >All</option>
																<option value="Member" >Member</option>
																<option value="RESELLER" >RESELLER</option>
																<option value="AGEN" >AGEN</option>
																<option value="DISTRIBUTOR" >DISTRIBUTOR</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>					
					
						  <div class="col-lg-12"><br/>
						  <button onclick="return TopSeller();" type="button" class="btn btn-primary btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
						    </div>
							<div style="overflow-x:auto;"  class="box-body">
							<div id="div_top_seller">
								<div id="kt_morris_12" style="height:500px;"></div>
							</div>
						</div>
				    	</div>
						</div>
					</div>
					<script type="text/javascript">
							function TopSeller(){
								var cBulanTopSeller = $('#cBulanTopSeller').val();
								var cTahunTopSeller = $('#cTahunTopSeller').val();
								var cTopSeller 		= $('#cTopSeller').val();
				
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/TopSeller')?>/"+cBulanTopSeller+'/'+cTahunTopSeller+'/'+cTopSeller,
									cache: false,
									beforeSend:function(){
									$('#div_top_seller').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_top_seller').html(msg);
									}
								});  
								
							}
						</script>


			
					</div>
				</div>

										<!-- begin:: Footer -->
										<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
											<div class="kt-container  kt-container--fluid ">
												<div class="kt-footer__copyright">
													2019&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
												</div>
												
											</div>
										</div>

										<!-- end:: Footer -->
									</div>
								</div>
							</div>

							<!-- end:: Page -->

							

							<!-- begin::Scrolltop -->
							<div id="kt_scrolltop" class="kt-scrolltop">
								<i class="fa fa-arrow-up"></i>
							</div>

							<!-- end::Scrolltop -->



							<!-- begin::Global Config(global config for global JS sciprts) -->
							<script>
								var KTAppOptions = {
									"colors": {
										"state": {
											"brand": "#22b9ff",
											"light": "#ffffff",
											"dark": "#282a3c",
											"primary": "#5867dd",
											"success": "#34bfa3",
											"info": "#36a3f7",
											"warning": "#ffb822",
											"danger": "#fd3995"
										},
										"base": {
											"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
											"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
										}
									}
								};
							</script>

							<!-- end::Global Config -->

							<!--begin::Global Theme Bundle(used by all pages) -->

							<!--begin:: Vendor Plugins -->
							<script src="<?=base_url()?>web/plugins/jquery.js" type="text/javascript"></script>

							<script src="<?=base_url()?>web/plugins/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>

							<script src="<?=base_url()?>web/plugins/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
							<script src="<?=base_url()?>web/plugins/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>

							

						

							<!--end:: Vendor Plugins -->
							<script src="<?=base_url()?>web/js/scripts.bundle.js" type="text/javascript"></script>

							<!--begin:: Vendor Plugins for custom pages -->
							<script src="<?=base_url()?>web/dashboard/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>


						 <script src="//www.amcharts.com/lib/3/amcharts.js"></script>
        <script src="//www.amcharts.com/lib/3/serial.js"></script>
        <script src="//www.amcharts.com/lib/3/radar.js"></script>
        <script src="//www.amcharts.com/lib/3/pie.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
        <script src="//www.amcharts.com/lib/3/themes/light.js"></script>

							<!--end::Page Vendors -->

							<!--begin::Page Scripts(used by this page) -->
							<script type="text/javascript">
								

"use strict";

// Class definition
var KTamChartsChartsDemo = function() {

 	var demo1 = function() {
        var chart = AmCharts.makeChart("kt_morris_3", {
            "type": "serial",
            "theme": "light",
            "marginRight": 40,
            "marginLeft": 40,
            "autoMarginOffset": 20,
            "mouseWheelZoomEnabled": true,
            "dataDateFormat": "YYYY-MM-DD",
            "valueAxes": [{
                "id": "v1",
                "axisAlpha": 0,
                "position": "left",
                "ignoreAxisWidth": true
            }],
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [{
                "id": "g1",
                "balloon": {
                    "drop": true,
                    "adjustBorderColor": false,
                    "color": "#ffffff"
                },
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 5,
                "hideBulletsCount": 50,
                "lineThickness": 2,
                "title": "red line",
                "useLineColorForBulletBorder": true,
                "valueField": "value",
                "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 80,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 1,
                "cursorColor": "#258cbb",
                "limitToGraph": "g1",
                "valueLineAlpha": 0.2,
                "valueZoomable": true
            },
            "valueScrollbar": {
                "oppositeAxis": false,
                "offset": 50,
                "scrollbarHeight": 10
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true
            },
            "export": {
                "enabled": true
            },
            "dataProvider": [

            <?php 
                if($PoUnpaidMonthly->num_rows() > 0){
                   foreach ($PoUnpaidMonthly->result_array() as $key => $vaData1) {               
                ?>
            {
                "date": "<?=$vaData1['account_detail_sales_date']?>",
                "value": <?=$vaData1['amount']?>
            }, 
        <?php } ?>
        <?php }else{ ?>
        	{
                "date": "<?=date("Y-m-D")?>",
                "value": 0
            }, 
        <?php } ?>


            ]
        });

        chart.addListener("rendered", zoomChart);

        zoomChart();

        function zoomChart() {
            chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
        }
    }
    var demo2 = function() {
        var chart = AmCharts.makeChart("kt_morris_4", {
            "type": "serial",
            "theme": "light",
            "marginRight": 40,
            "marginLeft": 40,
            "autoMarginOffset": 20,
            "mouseWheelZoomEnabled": true,
            "dataDateFormat": "YYYY-MM-DD",
            "valueAxes": [{
                "id": "v1",
                "axisAlpha": 0,
                "position": "left",
                "ignoreAxisWidth": true
            }],
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [{
                "id": "g1",
                "balloon": {
                    "drop": true,
                    "adjustBorderColor": false,
                    "color": "#ffffff"
                },
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 5,
                "hideBulletsCount": 50,
                "lineThickness": 2,
                "title": "red line",
                "useLineColorForBulletBorder": true,
                "valueField": "value",
                "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 80,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 1,
                "cursorColor": "#258cbb",
                "limitToGraph": "g1",
                "valueLineAlpha": 0.2,
                "valueZoomable": true
            },
            "valueScrollbar": {
                "oppositeAxis": false,
                "offset": 50,
                "scrollbarHeight": 10
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true
            },
            "export": {
                "enabled": true
            },
            "dataProvider": [

            <?php 
                if($PoPaidMonthly->num_rows() > 0){
                   foreach ($PoPaidMonthly->result_array() as $key => $vaData2) {               
                ?>
            {
                "date": "<?=$vaData2['account_detail_sales_date']?>",
                "value": <?=$vaData2['amount']?>
            }, 
        <?php } ?>
        <?php }else{ ?>
        	{
                "date": "<?=date("Y-m-D")?>",
                "value": 0
            }, 
        <?php } ?>


            ]
        });

        chart.addListener("rendered", zoomChart);

        zoomChart();

        function zoomChart() {
            chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
        }
    }

    var demo3 = function() {
        var chart = AmCharts.makeChart("kt_morris_5", {
            "type": "serial",
            "theme": "light",
            "marginRight": 40,
            "marginLeft": 40,
            "autoMarginOffset": 20,
            "mouseWheelZoomEnabled": true,
            "dataDateFormat": "YYYY-MM-DD",
            "valueAxes": [{
                "id": "v1",
                "axisAlpha": 0,
                "position": "left",
                "ignoreAxisWidth": true
            }],
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [{
                "id": "g1",
                "balloon": {
                    "drop": true,
                    "adjustBorderColor": false,
                    "color": "#ffffff"
                },
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 5,
                "hideBulletsCount": 50,
                "lineThickness": 2,
                "title": "red line",
                "useLineColorForBulletBorder": true,
                "valueField": "value",
                "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 80,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 1,
                "cursorColor": "#258cbb",
                "limitToGraph": "g1",
                "valueLineAlpha": 0.2,
                "valueZoomable": true
            },
            "valueScrollbar": {
                "oppositeAxis": false,
                "offset": 50,
                "scrollbarHeight": 10
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true
            },
            "export": {
                "enabled": true
            },
            "dataProvider": [

            <?php 
                if($SoByQtyMonthly->num_rows() > 0){
                   foreach ($SoByQtyMonthly->result_array() as $key => $vaData3) {               
                ?>
            {
                "date": "<?=$vaData3['account_detail_sales_date']?>",
                "value": <?=$vaData3['so_qty']?>
            }, 
        <?php } ?>
        <?php }else{ ?>
        	{
                "date": "<?=date("Y-m-D")?>",
                "value": 0
            }, 
        <?php } ?>


            ]
        });

        chart.addListener("rendered", zoomChart);

        zoomChart();

        function zoomChart() {
            chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
        }
    }

    var demo4 = function() {
        var chart = AmCharts.makeChart("kt_morris_6", {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "theme": "light",
            "dataProvider": [

             <?php 
                if($SoByKategoriMonthlyLine->num_rows() > 0){
                     foreach ($SoByKategoriMonthlyLine->result_array() as $key => $vaData4) {               
                ?>
            {
                "country": "<?=$vaData4['kategori']?>",
                "visits": <?=$vaData4['so_by_kategori']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>
		

		],
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [
             {
                "id": "graph2",
                "balloonText": "<span style='font-size:12px;'>[[category]]:<br><span style='font-size:20px;'>[[value]]</span></span>",
                "bullet": "round",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Qty",
                "valueField": "visits",
                "dashLengthField": "dashLengthLine"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": true
            }

        });
    }

    var demo5 = function() {
        var chart = AmCharts.makeChart("kt_morris_7", {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "theme": "light",
            "dataProvider": [

             <?php 
                if($SoByKategoriMonthlyBar->num_rows() > 0){
                     foreach ($SoByKategoriMonthlyBar->result_array() as $key => $vaData5) {               
                ?>
            {
                "country": "<?=$vaData5['kategori']?>",
                "visits": <?=$vaData5['so_by_kategori']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>
		

		],
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": true
            }

        });
    }

    var demo7 = function() {
        var chart = AmCharts.makeChart("kt_morris_8", {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "theme": "light",
            "dataProvider": [

            <?php 
                if($member->num_rows() > 0){
                     foreach ($member->result_array() as $key => $vaDataMember) {               
                ?>
            {
                "country": "<?=$vaDataMember['status']?>",
                "visits": <?=$vaDataMember['member']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>


	<?php 
                if($reseller->num_rows() > 0){
                     foreach ($reseller->result_array() as $key => $vaDataReseller) {               
                ?>
            {
                "country": "<?=$vaDataReseller['status']?>",
                "visits": <?=$vaDataReseller['member']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>


	<?php 
                if($agen->num_rows() > 0){
                     foreach ($agen->result_array() as $key => $vaDataAgen) {               
                ?>
            {
                "country": "<?=$vaDataAgen['status']?>",
                "visits": <?=$vaDataAgen['member']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>

	<?php 
                if($distributor->num_rows() > 0){
                     foreach ($distributor->result_array() as $key => $vaDataDistributor) {               
                ?>
            {
                "country": "<?=$vaDataDistributor['status']?>",
                "visits": <?=$vaDataDistributor['member']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>
		

		],
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": true
            }

        });
    }


	var demo6 = function() {
        var chart = AmCharts.makeChart("kt_morris_9", {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "theme": "light",
            "dataProvider": [

            <?php 
                if($member->num_rows() > 0){
                     foreach ($member->result_array() as $key => $vaDataMember) {               
                ?>
            {
                "country": "<?=$vaDataMember['status']?>",
                "visits": <?=$vaDataMember['member']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>


	<?php 
                if($reseller->num_rows() > 0){
                     foreach ($reseller->result_array() as $key => $vaDataReseller) {               
                ?>
            {
                "country": "<?=$vaDataReseller['status']?>",
                "visits": <?=$vaDataReseller['member']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>


	<?php 
                if($agen->num_rows() > 0){
                     foreach ($agen->result_array() as $key => $vaDataAgen) {               
                ?>
            {
                "country": "<?=$vaDataAgen['status']?>",
                "visits": <?=$vaDataAgen['member']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>

	<?php 
                if($distributor->num_rows() > 0){
                     foreach ($distributor->result_array() as $key => $vaDataDistributor) {               
                ?>
            {
                "country": "<?=$vaDataDistributor['status']?>",
                "visits": <?=$vaDataDistributor['member']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>
		

		],
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [
             {
                "id": "graph2",
                "balloonText": "<span style='font-size:12px;'>[[category]]:<br><span style='font-size:20px;'>[[value]]</span></span>",
                "bullet": "round",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Qty",
                "valueField": "visits",
                "dashLengthField": "dashLengthLine"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": true
            }

        });
    }

    


    var demo8 = function() {
        var chart = AmCharts.makeChart("kt_morris_10", {
            "type": "serial",
            "theme": "light",
            "handDrawn": true,
            "handDrawScatter": 3,
            "legend": {
                "useGraphSettings": true,
                "markerSize": 12,
                "valueWidth": 0,
                "verticalGap": 0
            },
            "dataProvider": [

            <?php 
                if($FastMoving->num_rows() > 0){
                     foreach ($FastMoving->result_array() as $key => $vaData7) {
                     if($vaData7['jumlah_terjual'] >= 5000){                
                ?>
            
            {
                "year": "<?=$vaData7['nama_produk']?>",
                "income": <?=$vaData7['jumlah_terjual']?>,
                "expenses": <?=$vaData7['jumlah_terjual']?>
            }, 
             <?php }} ?>
            <?php }else{ ?>
            	{
                "year": 0,
                "income": 0,
                "expenses": 0
            }, 
            <?php  } ?>


            ],
            "valueAxes": [{
                "minorGridAlpha": 0.08,
                "minorGridEnabled": true,
                "position": "top",
                "axisAlpha": 0
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b></span>",
                "title": "Terjual",
                "type": "column",
                "fillAlphas": 0.8,

                "valueField": "income"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b></span>",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "fillAlphas": 0,
                "lineThickness": 2,
                "lineAlpha": 1,
                "bulletSize": 7,
                "title": "Terjual",
                "valueField": "expenses"
            }],
            "rotate": true,
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "export": {
                "enabled": true
            }

        });
    }

    var demo9 = function() {
        var chart = AmCharts.makeChart("kt_morris_11", {
            "type": "serial",
            "theme": "light",
            "handDrawn": true,
            "handDrawScatter": 3,
            "legend": {
                "useGraphSettings": true,
                "markerSize": 12,
                "valueWidth": 0,
                "verticalGap": 0
            },
            "dataProvider": [

            <?php 
            	$no = 1 ;
                if($SlowMoving->num_rows() > 0){
                	
                     foreach ($SlowMoving->result_array() as $key => $vaData8) {
                     $nTerjual = (!empty($vaData['jumlah_terjual'])) ? $vaData['jumlah_terjual'] : "0";    
                      if($vaData8['jumlah_terjual'] < 5000 and $vaData8['jumlah_terjual'] != 0){            
                ?>
            
            <?php 
            if($no < 11){
            ?>
            {
                "year": "<?=$vaData8['nama_produk']?>",
                "income": <?=$vaData8['jumlah_terjual']?>,
                "expenses": <?=$vaData8['jumlah_terjual']?>
            }, 
        <?php ++$no;} ?>
             <?php }} ?>
            <?php }else{ ?>
            	{
                "year": 0,
                "income": 0,
                "expenses": 0
            }, 
            <?php  } ?>


            ],
            "valueAxes": [{
                "minorGridAlpha": 0.08,
                "minorGridEnabled": true,
                "position": "top",
                "axisAlpha": 0
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b></span>",
                "title": "Terjual",
                "type": "column",
                "fillAlphas": 0.8,

                "valueField": "income"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b></span>",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "fillAlphas": 0,
                "lineThickness": 2,
                "lineAlpha": 1,
                "bulletSize": 7,
                "title": "Terjual",
                "valueField": "expenses"
            }],
            "rotate": true,
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "export": {
                "enabled": true
            }

        });
    }

    var demo10 = function() {
        var chart = AmCharts.makeChart("kt_morris_12", {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "theme": "light",
            "dataProvider": [

             
   
    	{
                "country": "None",
                "visits": 0
            }, 
  
		

		],
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": true
            }

        });
    }

    var demoALL = function() {
        var chart = AmCharts.makeChart("kt_morris_2", {
            "theme": "light",
            "type": "serial",
            "dataProvider": [
			
			<?php 
             if($PoSOAll->num_rows() > 0){
                     foreach ($PoSOAll->result_array() as $key => $vaDataPoSOAll) {               
    		?>
            {
                "country": "<?=convertDate($vaDataPoSOAll['tanggal'])?>",
                "year2004": <?=$vaDataPoSOAll['total_po']?>,
                "year2005": <?=$vaDataPoSOAll['total_so']?>
            },
           <?php } ?>
		   <?php }else{ ?>
		    {
                "country": "<?=date("Y-m-d")?>",
                "year2004": 0,
                "year2005": 0
            },
		   <?php } ?>
            

            ],
            "valueAxes": [{
                "stackType": "3d",
                "unit": "%",
                "position": "left",
                "title": "Purchase Requisition & Sales Order",
            }],
            "startDuration": 1,
            

            "graphs": [

            {
                "id": "graph2",
                "balloonText": "<span style='font-size:12px;'>SO : [[category]]:<br><span style='font-size:20px;'>Amount : [[value]]</span></span>",
                "bullet": "round",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "SO",
                "valueField": "year2005",
                "dashLengthField": "dashLengthLine"
            },
            {

                "balloonText": "<span style='font-size:12px;'>PO : [[category]]:<br><span style='font-size:20px;'>Amount : [[value]]</span></span>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "PO",
                "type": "column",
                "fillColors": "#0abb87",
                "valueField": "year2004"
            }

            ],
            "plotAreaFillAlphas": 0.1,
            "depth3D": 60,
            "angle": 30,
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start"
            },
            "export": {
                "enabled": true
            }
        });
    }



   

    return {
        // public functions
        init: function() {
           
            demo1();
            demo2();
            demo3();
            demo4();
            demo5();
            demo6();
            demo7();
            demo8();
            demo9();
            demo10();
            demoALL();
           
        }
    };
}();

jQuery(document).ready(function() {
    KTamChartsChartsDemo.init();
});
</script>
						

							<!--end::Page Scripts -->
						</body>

						<!-- end::Body -->
						</html>