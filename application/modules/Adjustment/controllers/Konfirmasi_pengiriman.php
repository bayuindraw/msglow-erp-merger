<?php

// require 'vendor/autoload.php';

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Style\Border;

defined('BASEPATH') or exit('No direct script access allowed');

class Konfirmasi_pengiriman extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('Global_model', 'gm');
		$this->load->library('session');
		$this->load->database();
		// $this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}

	public function index($Aksi = "")
	{
		$title = "Konfirmasi Pengiriman";
		$cont = "Konfirmasi_pengiriman";
		$dataHeader['title']		= $title;
		$dataHeader['menu']   		= 'Adjustment Outbound';
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont;
		$dataHeader['url']			= $title;
		$data['title']				= "Log Adjustment - ".date('m/Y');
		$data['action'] 			= $Aksi;
		$data['row']				= $this->db->query("SELECT a.*, b.*, c.user_fullname, d.nama_produk FROM l_outbound_log a 
			JOIN t_package_trial_detail_draft b ON a.package_detail_id = b.package_detail_id 
			JOIN m_user c ON a.user_id = c.user_id
			JOIN produk d ON b.product_id = d.id_produk
			WHERE MONTH(a.outbound_date_create) = '".date('m')."' and YEAR(a.outbound_date_create) = '".date('Y')."' and a.outbound_type > 1 and a.outbound_log_detail LIKE '%Konfirmasi pengiriman%'")->result_array();

		$dataHeader['content'] = $this->load->view('table_konfirmasi_pengiriman', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function form($Aksi = "")
	{
		$title = "Konfirmasi Pengiriman";
		$cont = "Konfirmasi_pengiriman";
		$dataHeader['title']		= "Adjustment ".$title;
		$dataHeader['menu']   		= $title;
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont.'/form';
		$dataHeader['url']			= $cont;
		$data['title']				= "Form ".$title;
		$data['action'] 			= $Aksi;
		$data['tp']					= $this->db->query("SELECT a.*, c.nama_produk, d.package_code, e.nama
			FROM t_package_trial_detail_draft a
			JOIN l_outbound_log b ON a.package_detail_id = b.package_detail_id
			JOIN produk c ON a.product_id = c.id_produk
			JOIN member e ON a.seller_id = e.kode
			LEFT JOIN t_package_trial d ON a.package_id = d.package_id
			WHERE b.outbound_log_detail LIKE '%Outbound Konfirmasi Pengiriman%'")->result_array();

		$dataHeader['content'] = $this->load->view('form_konfirmasi_pengiriman', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	function simpan()
	{
		$this->db->trans_begin();
		if (!is_dir(BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d'))) {
			mkdir(BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d'), 0777);
		}

		$this->load->library('upload');
		$config['upload_path'] = BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d');
		$config['allowed_types'] = 'pdf|jpg|jpeg|png';
		$config['encrypt_name'] = TRUE;

		$this->upload->initialize($config);
		if($this->upload->do_upload("file")){
			$table = "t_package_trial_detail_draft";
			$where = "package_detail_id = '".$this->input->post('package_detail_id')."'";
			$dt = $this->gm->get_table_where("*", $table, $where);
			if (count($dt)>0 && $dt[0]['package_id']!="") {
				$table = "t_package_trial";
				$where = "package_id = '".$dt[0]['package_id']."'";
				$dt_package = $this->gm->get_table_where("*", $table, $where);
				$tanggal = $dt_package[0]['package_date'];

				$qty_confirm = $this->input->post('qty');
				$qty_akum = $dt[0]['package_detail_confirm_quantity']-$this->input->post('qty');
				$qty_log = $this->input->post('qty')-$dt[0]['package_detail_confirm_quantity'];
				$qty_detail = $dt[0]['package_detail_quantity']+$qty_akum;

				$du = array(
					'package_detail_quantity' => $qty_detail,
					'package_detail_confirm_quantity' => $qty_confirm
				);
				$this->gm->update_table("t_package_trial_detail_draft",$du,$where);
				$lq = $this->db->last_query();

				$this->db->query("UPDATE tb_stock_produk SET jumlah = (jumlah + " . $qty_akum . ") WHERE id_barang = '" . $dt[0]['product_id'] . "' AND warehouse_id = '".$_SESSION['warehouse_id']."'");

				$this->db->query("UPDATE tb_stock_produk_history SET jumlah = (jumlah + " . $qty_akum . ") 
					WHERE id_barang = '" . $dt[0]['product_id'] . "' AND REPLACE(tanggal, '-', '') > REPLACE('" . $tanggal . "', '-', '') 
					AND warehouse_id = '".$_SESSION['warehouse_id']."'");

				$dt_root = $this->db->query("SELECT * FROM l_outbound_log WHERE package_id = '".$dt_package[0]['package_id']."' ORDER BY outbound_log_id DESC LIMIT 1")->result_array();
				if (count($dt_root)>0) {
					$id_log = $dt_root[0]['outbound_log_id'];
				} else {
					$id_log = 0;
				}

				$datalog = array(
					'package_id' => $dt_package[0]['package_id'],
					'user_id' => $_SESSION['user_id'],
					'outbound_log_status' => 1,
					'outbound_log_detail' => "Adjustment Outbound Konfirmasi Pengiriman",
					'outbound_log_note' => $this->input->post('note'),
					'outbound_log_query' => $lq,
					'outbound_date_create' => date('Y-m-d H:i:s'),
					'package_detail_id' => $this->input->post('package_detail_id'),
					'outbound_quantity' => $qty_log,
					'adjustment_target' => $this->input->post('package_detail_id'),
					'outbound_date' => date('Y-m-d H:i:s'),
					'outbound_quantity_before' => $dt[0]['package_detail_confirm_quantity'],
					'outbound_quantity_current' => $qty_confirm,
					'outbound_before_log_id' => $id_log,
					'outbound_type' => 2,
					'outbound_url' => base_url().'/Adjustment/Konfirmasi_pengiriman/simpan',
					'outbound_form_url' => base_url().'/Adjustment/Konfirmasi_pengiriman/form',
					'outbound_file' => date('Y-m-d').'/'.$this->upload->data('file_name')
				);
				$this->gm->insert_table('l_outbound_log', $datalog);
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Adjustment/Konfirmasi_pengiriman','refresh');
	}

	function batal($id)
	{
		$this->db->trans_begin();
		$table = "l_inbound_log";
		$where = "inbound_log_id = '$id'";
		$dt_log = $this->gm->get_table_where("*", $table, $where);

		$table = "terima_produk";
		$where = "id_terima_kemasan = '".$dt_log[0]['terima_produk_id']."'";
		$dt = $this->gm->get_table_where("*", $table, $where);
		
		$qty_akum = $dt_log[0]['inbound_quantity_current']-$dt_log[0]['inbound_quantity_before'];

		$du = array(
			'jumlah' => $dt_log[0]['inbound_quantity_before'],
			'jumlah_sj' => $dt_log[0]['inbound_quantity_before']
		);
		$this->gm->update_table($table,$du,$where);
		$lq = $this->db->last_query();

		$datalog = array(
			'user_id' => $_SESSION['user_id'],
			'inbound_log_status' => 1,
			'inbound_log_detail' => 'Batal Adjustment Terima Produk',
			'inbound_log_query' => $lq,
			'inbound_date_create' => date('Y-m-d H:i:s'),
			'terima_produk_id' => $dt_log[0]['terima_produk_id'],
			'adjustment_target' => $dt_log[0]['terima_produk_id'],
			'inbound_quantity' => $qty_akum,
			'inbound_quantity_before' => $dt_log[0]['inbound_quantity_current'],
			'inbound_quantity_current' => $dt_log[0]['inbound_quantity_before'],
			'inbound_date' => $dt[0]['tgl_terima'],
			'inbound_type' => 2,
			'inbound_before_log_id' => $dt_log[0]['inbound_log_id'],
			'inbound_url' => base_url().'/Adjustment/Terima_produk/batal',
			'inbound_form_url' => base_url().'/Adjustment/Terima_produk'
		);
		$this->gm->insert_table('l_inbound_log', $datalog);

		$this->db->query("UPDATE tb_stock_produk SET jumlah = (jumlah + $qty_akum) WHERE id_barang = '" . $dt[0]['id_barang'] . "' AND warehouse_id = '".$dt[0]['warehouse_id']."'");
		$this->db->query("UPDATE tb_stock_produk_history SET jumlah = (jumlah + $qty_akum) WHERE id_barang = '" . $dt[0]['id_barang'] . "' AND REPLACE(tanggal, '-', '') > REPLACE('" . $dt[0]['tgl_terima'] . "', '-', '') AND warehouse_id = '".$dt[0]['warehouse_id']."'");

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Adjustment/Terima_produk','refresh');
	}
}
