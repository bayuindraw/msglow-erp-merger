<?php

// require 'vendor/autoload.php';

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Style\Border;

defined('BASEPATH') or exit('No direct script access allowed');

class Payment extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('Global_model', 'gm');
		$this->load->library('session');
		$this->load->database();
		// $this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}

	public function index($Aksi = "")
	{
		$title = "Payment";
		$cont = "Payment";
		$dataHeader['title']		= $title;
		$dataHeader['menu']   		= 'Adjustment';
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont;
		$dataHeader['url']			= $title;
		$data['title']				= "Log Adjustment - ".date('m/Y');
		$data['action'] 			= $Aksi;
		$data['row']				= $this->db->query("SELECT a.*, b.*, c.user_fullname,
			(SELECT g.sales_code FROM t_account_detail f JOIN t_sales g ON f.sales_id = g.sales_id WHERE f.account_id = b.account_id and f.sales_id != '' LIMIT 1) as sales_code
			FROM l_account_detail_log a 
			JOIN t_account_detail b ON a.account_detail_header_id = b.account_detail_id 
			JOIN m_user c ON a.user_id = c.user_id
			WHERE MONTH(a.account_detail_log_date_create) = '".date('m')."' and YEAR(a.account_detail_log_date_create) = '".date('Y')."' and a.account_detail_log_type = 4 and (SELECT g.sales_code FROM t_account_detail f JOIN t_sales g ON f.sales_id = g.sales_id WHERE f.account_id = b.account_id and f.sales_id != '' LIMIT 1)!=''")->result_array();

		$dataHeader['content'] = $this->load->view('table_payment', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function form($Aksi = "")
	{
		$title = "Payment";
		$cont = "Payment";
		$dataHeader['title']		= "Adjustment ".$title;
		$dataHeader['menu']   		= $title;
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont.'/form';
		$dataHeader['url']			= $cont;
		$data['title']				= "Form ".$title;
		$data['action'] 			= $Aksi;
		$data['tp']					= $this->db->query("SELECT a.*, c.nama,
			(SELECT g.sales_code FROM t_account_detail f JOIN t_sales g ON f.sales_id = g.sales_id WHERE f.account_id = a.account_id and f.sales_id != '' LIMIT 1) as sales_code
			FROM t_account_detail a
			JOIN m_account b ON a.account_id = b.account_id
			JOIN member c ON b.seller_id = c.kode
			WHERE a.account_detail_debit = 0")->result_array();

		$dataHeader['content'] = $this->load->view('form_payment', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	function cek_data()
	{
		$account_detail_id = $this->input->post('account_detail_id');
		$dt = $this->db->query("SELECT a.*
			FROM t_account_detail a
			WHERE a.account_detail_id = '$account_detail_id'")->result_array();

		if (count($dt)>0) {
			echo json_encode(array('status' => true, 'data' => $dt));
		} else {
			echo json_encode(array('status' => false, 'message' => 'Data tidak ditemukan!'));
		}
	}

	function simpan()
	{
		$this->db->trans_begin();
		if (!is_dir(BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d'))) {
			mkdir(BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d'), 0777);
		}

		$this->load->library('upload');
		$config['upload_path'] = BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d');
		$config['allowed_types'] = 'pdf|jpg|jpeg|png';
		$config['encrypt_name'] = TRUE;

		$this->upload->initialize($config);
		if($this->upload->do_upload("file")){
			$table = "t_account_detail";
			$where = "account_detail_id = '".$this->input->post('account_detail_id')."'";
			$dt = $this->gm->get_table_where("*", $table, $where);

			if (count($dt)>0) {
				$id = $dt[0]['account_id'];

				$du = array(
					'account_detail_credit' => $this->input->post('payment')
				);
				$where = "account_detail_id = '".$this->input->post('account_detail_id')."'";
				$this->gm->update_table("t_account_detail",$du,$where);
				$lq = $this->db->last_query();

				$table = "t_account_detail_sales";
				$where = "account_detail_id = '".$this->input->post('account_detail_id')."'";
				$dt2 = $this->gm->get_table_where("account_detail_sales_amount", $table, $where);
				if (count($dt2)>0) {
					$amount = $dt2[0]['account_detail_sales_amount'];
				} else {
					$amount = 0;
				}

				$table = "t_account_detail_sales_product";
				$where = "account_detail_id = '".$this->input->post('account_detail_id')."'";
				$dt3 = $this->gm->get_table_where("*", $table, $where);

				if ($this->input->post('payment')>=$amount) {
						//UPDATE DEPOSIT
					$selisih = $this->input->post('payment') - $dt[0]['account_detail_credit'];

					$table = "m_account";
					$where = "account_id = '".$dt[0]['account_id']."'";
					$dt4 = $this->gm->get_table_where("account_deposit", $table, $where);
					$du = array(
						'account_deposit' => $dt4[0]['account_deposit'] + $selisih
					);
					$this->gm->update_table("m_account",$du,$where);
				} else {
						//CARI MAKSIMAL QTY SESUAI YG DIBAYAR
						//cari harga
					$table = "t_sales_detail";
					$where = "sales_detail_id = '".$dt3[0]['sales_detail_id']."'";
					$dt_harga = $this->gm->get_table_where("sales_detail_price", $table, $where);
						//cari maksimal qty boleh kirim
					$jml_qty = $this->input->post('payment')/$dt_harga[0]['sales_detail_price'];
					$du = array(
						'account_detail_sales_product_allow' => $jml_qty
					);
					$where = "account_detail_id = '".$this->input->post('account_detail_id')."'";
					$this->gm->update_table("t_account_detail_sales_product",$du,$where);
						//cari bayar pr
					$bayar_pr = $jml_qty*$dt_harga[0]['sales_detail_price'];
					$du = array(
						'account_detail_sales_amount' => 0
					);
					$this->gm->update_table("t_account_detail_sales",$du,$where);
					$sisa_amount = $bayar_pr - $amount;
					if ($selisih_amount > 0) {
						$table = "t_account_detail_sales";
						$where = "sales_id = '".$dt3[0]['sales_id']."' and account_detail_id != '".$this->input->post('account_detail_id')."'";
						$dt_amount = $this->gm->get_table_where("account_detail_sales_amount", $table, $where);
						foreach ($dt_amount as $d) {
							$sisa_amount = $d['account_detail_sales_amount']-$sisa_amount;
							if ($d['account_detail_sales_amount']>=$sisa_amount) {
								$du = array(
									'account_detail_sales_amount' => $sisa_amount
								);
								$this->gm->update_table("t_account_detail_sales",$du,$where);
							} else {
								$du = array(
									'account_detail_sales_amount' => 0
								);
								$this->gm->update_table("t_account_detail_sales",$du,$where);
							}
							if ($sisa_amount<1) {
								break;
							}
						}
					}

						//UPDATE DEPOSIT
					$selisih_tf_bayar = $dt[0]['account_detail_credit']-$amount;

					$table = "m_account";
					$where = "account_id = '".$dt[0]['account_id']."'";
					$dt4 = $this->gm->get_table_where("account_deposit", $table, $where);
					$du = array(
						'account_deposit' => $dt4[0]['account_deposit'] - $selisih_tf_bayar
					);
					$this->gm->update_table("m_account",$du,$where);
				}

				$datalog = array(
					'account_detail_header_id' => $this->input->post('account_detail_id'),
					'user_id' => $_SESSION['user_id'],
					'account_detail_log_status' => 1,
					'account_detail_log_detail' => 'Adjustment Payment',
					'account_detail_log_note' => $this->input->post('note'),
					'account_detail_log_query' => $lq,
					'account_detail_log_date_create' => date('Y-m-d H:i:s'),
					'account_detail_log_type' => 4,
					'account_detail_log_credit' => $this->input->post('payment'),
					'account_detail_log_credit_past' => $dt[0]['account_detail_credit'],
					'account_detail_log_credit_current' => $this->input->post('payment'),
					'account_detail_file' => date('Y-m-d').'/'.$this->upload->data('file_name')
				);
				$this->gm->insert_table('l_account_detail_log', $datalog);
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Adjustment/Payment','refresh');
	}
}
