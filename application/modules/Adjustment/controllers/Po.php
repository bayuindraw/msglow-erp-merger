<?php

// require 'vendor/autoload.php';

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Style\Border;

defined('BASEPATH') or exit('No direct script access allowed');

class Po extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('Global_model', 'gm');
		$this->load->library('session');
		$this->load->database();
		// $this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}

	public function index($Aksi = "")
	{
		$title = "PO";
		$cont = "Po";
		$dataHeader['title']		= $title;
		$dataHeader['menu']   		= 'Adjustment PO';
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont;
		$dataHeader['url']			= $title;
		$data['title']				= "Log Adjustment - ".date('m/Y');
		$data['action'] 			= $Aksi;
		$data['row']				= $this->db->query("SELECT a.*, b.*, c.user_fullname, d.nama_produk, f.nama_produk as nama_produk_log
			FROM l_po_goods_detail_log a 
			JOIN tb_detail_po_produk b ON a.po_goods_detail_id = b.id_pokemasan 
			JOIN m_user c ON a.user_id = c.user_id
			JOIN produk d ON b.id_barang = d.id_produk
			JOIN l_po_goods_log e ON a.po_goods_id = e.po_goods_id
			JOIN produk f ON a.product_id = f.id_produk
			WHERE MONTH(a.po_goods_detail_log_date_create) = '".date('m')."' and YEAR(a.po_goods_detail_log_date_create) = '".date('Y')."' and a.po_goods_detail_log_type > 1")->result_array();

		$dataHeader['content'] = $this->load->view('table_po', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function form($Aksi = "")
	{
		$title = "PO";
		$cont = "Po";
		$dataHeader['title']		= "Adjustment ".$title;
		$dataHeader['menu']   		= $title;
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont.'/form';
		$dataHeader['url']			= $cont;
		$data['title']				= "Form ".$title;
		$data['action'] 			= $Aksi;
		$data['tp']					= $this->db->query("SELECT a.*, c.nama_produk
			FROM tb_detail_po_produk a
			JOIN po_produk d ON a.kode_pb = d.kode_po
			-- JOIN l_po_goods_detail_log b ON b.po_goods_detail_id = a.id_pokemasan
			JOIN produk c ON a.id_barang = c.id_produk
			WHERE d.active = 'Y'")->result_array();
		$data['produk']				= $this->gm->get_table_where("*","produk","id_produk != 0");

		$dataHeader['content'] = $this->load->view('form_po', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	function simpan()
	{
		$this->db->trans_begin();
		if (!is_dir(BASEPATH.'../upload/INBOUND/'.date('Y-m-d'))) {
			mkdir(BASEPATH.'../upload/INBOUND/'.date('Y-m-d'), 0777);
		}

		$this->load->library('upload');
		$config['upload_path'] = BASEPATH.'../upload/INBOUND/'.date('Y-m-d');
		$config['allowed_types'] = 'pdf|jpg|jpeg|png';
		$config['encrypt_name'] = TRUE;

		$this->upload->initialize($config);
		if($this->upload->do_upload("file")){
			$table = "tb_detail_po_produk";
			$where = "id_pokemasan = '".$this->input->post('id_pokemasan')."'";
			$dt = $this->gm->get_table_where("*", $table, $where);
			if (count($dt)>0) {

				$table = "l_po_goods_detail_log";
				$where = "po_goods_detail_id = '".$this->input->post('id_pokemasan')."'";
				$dt2 = $this->gm->get_table_where("*", $table, $where);

				$qty_akum = $this->input->post('qty')-$dt[0]['jumlah'];

				if ($this->input->post('id_produk')!="") {
					$id_produk = $this->input->post('id_produk');
				} else {
					$id_produk = $dt[0]['id_barang'];
				}

				$du = array(
					'id_barang' => $id_produk,
					'jumlah' => $this->input->post('qty')
				);
				$where = "id_pokemasan = '".$this->input->post('id_pokemasan')."'";
				$this->gm->update_table("tb_detail_po_produk",$du,$where);
				$lq = $this->db->last_query();

				$dt_p_global = $this->db->query("SELECT a.id, a.kode, a.nama_produk FROM produk_global a JOIN produk b ON a.kode = b.kd_pd WHERE b.id_produk = '".$id_produk."'")->result_array();
				$product_global_id = 0;
				$product_global_code = "";
				$product_global_name = "";
				if (count($dt_p_global)>0) {
					$product_global_id = $dt_p_global[0]['id'];
					$product_global_code = $dt_p_global[0]['kode'];
					$product_global_name = $dt_p_global[0]['nama_produk'];
				}

				$dt_root = $this->db->query("SELECT * FROM l_po_goods_detail_log WHERE po_goods_detail_id = '".$this->input->post('id_pokemasan')."' ORDER BY po_goods_detail_log_id DESC LIMIT 1")->result_array();
				if (count($dt_root)>0) {
					$id_log = $dt_root[0]['po_goods_detail_log_id'];
				} else {
					$id_log = 0;
				}

				$datalog = array(
					'po_goods_id' => $dt2[0]['po_goods_id'],
					'user_id' => $_SESSION['user_id'],
					'po_goods_detail_log_status' => 1,
					'po_goods_detail_log_detail' => "Adjustment PO Detail",
					'po_goods_detail_log_note' => $this->input->post('note'),
					'po_goods_detail_log_query' => $lq,
					'po_goods_detail_log_date_create' => date('Y-m-d H:i:s'),
					'po_goods_detail_id' => $dt2[0]['po_goods_detail_id'],
					'po_goods_detail_log_price' => $dt2[0]['po_goods_detail_log_price'],
					'po_goods_detail_log_quantity' => $qty_akum,
					'po_goods_detail_log_quantity_before' => $dt[0]['jumlah'],
					'po_goods_detail_log_quantity_current' => $this->input->post('qty'),
					'product_id' => $id_produk,
					'product_global_id' => $product_global_id,
					'product_global_code' => $product_global_code,
					'product_global_name' => $product_global_name,
					'po_goods_detail_log_type' => 2,
					'po_goods_detail_log_url' => base_url().'/Po/simpan',
					'po_goods_detail_log_form_url' => base_url().'/Po/form'
				);
				$this->gm->insert_table('l_po_goods_detail_log', $datalog);
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Adjustment/Po','refresh');
	}
}
