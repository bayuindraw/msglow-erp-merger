<?php

// require 'vendor/autoload.php';

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Style\Border;

defined('BASEPATH') or exit('No direct script access allowed');

class Pr extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('Global_model', 'gm');
		$this->load->model('T_salesModel', 'Model');
		$this->load->library('session');
		$this->load->database();
		// $this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}

	public function index($Aksi = "")
	{
		$title = "Purchase Request";
		$cont = "Pr";
		$dataHeader['title']		= $title;
		$dataHeader['menu']   		= 'Adjustment PR';
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont;
		$dataHeader['url']			= $title;
		$data['title']				= "Log Adjustment - ".date('m/Y');
		$data['action'] 			= $Aksi;
		$data['row']				= $this->db->query("SELECT a.*, b.*, c.user_fullname, d.nama_produk, e.sales_code, f.nama_produk as nama_produk_log
			FROM l_sales_detail_log a 
			JOIN t_sales_detail b ON a.sales_detail_id = b.sales_detail_id 
			JOIN m_user c ON a.user_id = c.user_id
			JOIN produk d ON b.product_id = d.id_produk
			JOIN t_sales e ON b.sales_id = e.sales_id
			JOIN produk f ON a.product_id = f.id_produk
			WHERE MONTH(a.sales_detail_log_date_create) = '".date('m')."' and YEAR(a.sales_detail_log_date_create) = '".date('Y')."' and a.sales_detail_log_type > 1")->result_array();

		$dataHeader['content'] = $this->load->view('table_pr', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function form($Aksi = "")
	{
		$title = "Purchase Request";
		$cont = "Pr";
		$dataHeader['title']		= "Adjustment ".$title;
		$dataHeader['menu']   		= $title;
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont.'/form';
		$dataHeader['url']			= $cont;
		$data['title']				= "Form ".$title;
		$data['action'] 			= $Aksi;
		$data['tp']					= $this->db->query("SELECT a.*, c.nama_produk, d.sales_code
			FROM t_sales_detail a
			JOIN produk c ON a.product_id = c.id_produk
			JOIN t_sales d ON a.sales_id = d.sales_id
			WHERE d.sales_status != 2")->result_array();
		$data['produk']				= $this->gm->get_table_where("*","produk","id_produk != 0");

		$dataHeader['content'] = $this->load->view('form_pr', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	function cek_sales_detail()
	{
		$sales_detail_id = $this->input->post('sales_detail_id');
		$dt = $this->db->query("SELECT a.*, IFNULL(b.account_detail_sales_product_allow,0) as account_detail_sales_product_allow 
			FROM t_sales_detail a
			LEFT JOIN t_account_detail_sales_product b ON a.sales_detail_id = b.sales_detail_id
			WHERE a.sales_detail_id = '$sales_detail_id'")->result_array();
		// $dt = $this->gm->get_table_where("*","t_sales_detail","sales_detail_id = '$sales_detail_id'");
		if (count($dt)>0) {
			echo json_encode(array('status' => true, 'data' => $dt));
		} else {
			echo json_encode(array('status' => false, 'message' => 'Data tidak ditemukan!'));
		}
	}

	function simpan()
	{
		$this->db->trans_begin();
		if (!is_dir(BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d'))) {
			mkdir(BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d'), 0777);
		}

		$this->load->library('upload');
		$config['upload_path'] = BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d');
		$config['allowed_types'] = 'pdf|jpg|jpeg|png';
		$config['encrypt_name'] = TRUE;

		$this->upload->initialize($config);
		if($this->upload->do_upload("file")){
			$table = "t_sales_detail";
			$where = "sales_detail_id = '".$this->input->post('sales_detail_id')."'";
			$dt = $this->gm->get_table_where("*", $table, $where);
			if (count($dt)>0) {
				$id = $dt[0]['sales_id'];

				$table = "l_sales_detail_log";
				$where = "sales_detail_id = '".$this->input->post('sales_detail_id')."'";
				$dt2 = $this->gm->get_table_where("*", $table, $where);

				$qty_akum = $this->input->post('qty')-$dt[0]['sales_detail_quantity'];

				$dt_p_global = $this->db->query("SELECT a.id, a.kode, a.nama_produk FROM produk_global a JOIN produk b ON a.kode = b.kd_pd WHERE b.id_produk = '".$this->input->post('id_produk')."'")->result_array();
				$product_global_id = 0;
				$product_global_code = "";
				$product_global_name = "";
				if (count($dt_p_global)>0) {
					$product_global_id = $dt_p_global[0]['id'];
					$product_global_code = $dt_p_global[0]['kode'];
					$product_global_name = $dt_p_global[0]['nama_produk'];
				}

				//UPDATE PR
				$du = array(
					'product_id' => $this->input->post('id_produk'),
					'product_global_id' => $product_global_id,
					'product_global_code' => $product_global_code,
					'product_global_name' => $product_global_name,
					'sales_detail_quantity' => $this->input->post('qty'),
					'sales_detail_price' => $this->input->post('harga')
				);
				$where = "sales_detail_id = '".$this->input->post('sales_detail_id')."'";
				$this->gm->update_table("t_sales_detail",$du,$where);
				$lq = $this->db->last_query();

				//UPDATE by Table excel
				if ($dt[0]['sales_detail_price'] < $this->input->post('harga') && $dt[0]['sales_detail_quantity'] == $this->input->post('qty') && $dt[0]['product_id'] == $this->input->post('id_produk')) {
					$du = array(
						'account_detail_sales_product_allow' => 0
					);
					$this->gm->update_table("t_account_detail_sales_product",$du,"sales_detail_id = '".$this->input->post('sales_detail_id')."'");
					
					$total_acc = $this->Model->get_account_detail_sales_product_total($id);

					$du = array(
						'account_detail_sales_amount' => $total_acc[0]['total']
					);
					$this->gm->update_table("t_account_detail_sales",$du,"account_detail_sales_id = '".$total_acc[0]['account_detail_sales_id']."'");

					//UPDATE DEPOSIT
					$cek_deposit = $this->db->query("SELECT * FROM m_account a JOIN t_account_detail b ON a.account_id = b.account_id WHERE b.sales_id = '$id'")->result_array();
					if (count($cek_deposit)>0) {
						$du = array(
							'account_deposit' => $this->input->post('qty')*$this->input->post('harga')
						);
						$this->gm->update_table("m_account",$du,"account_id = '".$cek_deposit[0]['account_id']."'");
					}
				}
				if ($dt[0]['sales_detail_price'] == $this->input->post('harga') && $dt[0]['sales_detail_quantity'] < $this->input->post('qty') && $dt[0]['product_id'] == $this->input->post('id_produk')) {
					$du = array(
						'sales_detail_quantity' => $this->input->post('qty')
					);
					$this->gm->update_table("t_sales_detail",$du,"sales_detail_id = '".$this->input->post('sales_detail_id')."'");
				}
				if ($dt[0]['sales_detail_price'] == $this->input->post('harga') && $dt[0]['sales_detail_quantity'] > $this->input->post('qty') && $dt[0]['product_id'] == $this->input->post('id_produk')) {
					$du = array(
						'sales_detail_quantity' => $this->input->post('qty')
					);
					$this->gm->update_table("t_sales_detail",$du,"sales_detail_id = '".$this->input->post('sales_detail_id')."'");

					$total_acc = $this->Model->get_account_detail_sales_product_total($id);
					if (count($total_acc)>0) {
						$du = array(
							'account_detail_sales_amount' => $total_acc[0]['total']
						);
						$this->gm->update_table("t_account_detail_sales",$du,"account_detail_sales_id = '".$total_acc[0]['account_detail_sales_id']."'");
					}

					//UPDATE DEPOSIT
					$cek_deposit = $this->db->query("SELECT * FROM m_account a JOIN t_account_detail b ON a.account_id = b.account_id WHERE b.sales_id = '$id'")->result_array();
					if (count($cek_deposit)>0) {
						$du = array(
							'account_deposit' => $this->input->post('qty')*$this->input->post('harga')
						);
						$this->gm->update_table("m_account",$du,"account_id = '".$cek_deposit[0]['account_id']."'");
					}
				}
				if ($dt[0]['sales_detail_price'] == $this->input->post('harga') && $dt[0]['sales_detail_quantity'] == $this->input->post('qty') && $dt[0]['product_id'] != $this->input->post('id_produk')) {
					$du = array(
						'product_id' => $this->input->post('id_produk')
					);
					$this->gm->update_table("t_account_detail_sales_product",$du,"sales_detail_id = '".$this->input->post('sales_detail_id')."'");
				}
				if ($dt[0]['sales_detail_price'] == $this->input->post('harga') && $dt[0]['sales_detail_quantity'] > $this->input->post('qty') && $dt[0]['product_id'] != $this->input->post('id_produk')) {
					$du = array(
						'product_id' => $this->input->post('id_produk')
					);
					$this->gm->update_table("t_account_detail_sales_product",$du,"sales_detail_id = '".$this->input->post('sales_detail_id')."'");
				}
				if ($dt[0]['sales_detail_price'] != $this->input->post('harga') && $dt[0]['sales_detail_quantity'] == $this->input->post('qty') && $dt[0]['product_id'] != $this->input->post('id_produk')) {
					$du = array(
						'account_detail_sales_product_allow' => 0,
						'product_id' => $this->input->post('id_produk')
					);
					$this->gm->update_table("t_account_detail_sales_product",$du,"sales_detail_id = '".$this->input->post('sales_detail_id')."'");
					
					$total_acc = $this->Model->get_account_detail_sales_product_total($id);
					if (count($total_acc)>0) {
						$du = array(
							'account_detail_sales_amount' => $total_acc[0]['total']
						);
						$this->gm->update_table("t_account_detail_sales",$du,"account_detail_sales_id = '".$total_acc[0]['account_detail_sales_id']."'");
					}

					//UPDATE DEPOSIT
					$cek_deposit = $this->db->query("SELECT * FROM m_account a JOIN t_account_detail b ON a.account_id = b.account_id WHERE b.sales_id = '$id'")->result_array();
					if (count($cek_deposit)>0) {
						$du = array(
							'account_deposit' => $this->input->post('qty')*$this->input->post('harga')
						);
						$this->gm->update_table("m_account",$du,"account_id = '".$cek_deposit[0]['account_id']."'");
					}
				}
				if ($dt[0]['sales_detail_price'] == $this->input->post('harga') && $dt[0]['sales_detail_quantity'] < $this->input->post('qty') && $dt[0]['product_id'] != $this->input->post('id_produk')) {
					$du = array(
						'account_detail_sales_product_allow' => 0,
						'product_id' => $this->input->post('id_produk')
					);
					$this->gm->update_table("t_account_detail_sales_product",$du,"sales_detail_id = '".$this->input->post('sales_detail_id')."'");
					
					$total_acc = $this->Model->get_account_detail_sales_product_total($id);
					if (count($total_acc)>0) {
						$du = array(
							'account_detail_sales_amount' => $total_acc[0]['total']
						);
						$this->gm->update_table("t_account_detail_sales",$du,"account_detail_sales_id = '".$total_acc[0]['account_detail_sales_id']."'");
					}

					//UPDATE DEPOSIT
					$cek_deposit = $this->db->query("SELECT * FROM m_account a JOIN t_account_detail b ON a.account_id = b.account_id WHERE b.sales_id = '$id'")->result_array();
					if (count($cek_deposit)>0) {
						$du = array(
							'account_deposit' => $this->input->post('qty')*$this->input->post('harga')
						);
						$this->gm->update_table("m_account",$du,"account_id = '".$cek_deposit[0]['account_id']."'");
					}
				}

				//FIFO
				if ($dt[0]['connected_quantity']>0) {
					$qty_connect = $dt[0]['connected_quantity'];
					$dtbrd = $this->db->query("SELECT * FROM t_bridge_beta WHERE sales_detail_id = '".$this->input->post('sales_detail_id')."'")->result_array();
					foreach ($dtbrd as $d) {
						$dtbrd2 = $this->db->query("SELECT bridge_beta_id, bridge_beta_quantity FROM t_bridge_beta WHERE sales_detail_id = '".$this->input->post('sales_detail_id')."' AND bridge_beta_quantity <= '$qty_connect' ORDER BY bridge_beta_quantity DESC LIMIT 1")->result_array();
						if (count($dtbrd2)>0) {
							$this->gm->delete_table("t_bridge_beta","bridge_beta_id = '".$dtbrd2[0]['bridge_beta_id']."'");
							$qty_connect -= $dtbrd2[0]['bridge_beta_quantity'];
							if ($qty_connect==0) {
								break;
							}
						} else {
							$dtbrd3 = $this->db->query("SELECT bridge_beta_id, bridge_beta_quantity FROM t_bridge_beta WHERE sales_detail_id = '".$this->input->post('sales_detail_id')."' AND bridge_beta_quantity > '$qty_connect' ORDER BY bridge_beta_quantity ASC LIMIT 1")->result_array();
							if (count($dtbrd3)>0) {
								$du = array(
									'bridge_beta_quantity' => $dtbrd3[0]['bridge_beta_quantity']-$qty_connect
								);
								$this->gm->update_table("t_bridge_beta",$du,"bridge_beta_id = '".$dtbrd3[0]['bridge_beta_id']."'");
								break;
							}
						}
					}
				}
				//END FIFO

				$id = $dt[0]['sales_id'];
				$total_price = $this->Model->get_sales_detail_total_price($id);
				$chk_account_detail = $this->Model->get_account_detail($id);
				if ($chk_account_detail['account_id'] != "") {
					//UPDATE BAYU dikurangi dulu total m_account
					$subtotal_price = $this->Model->get_sales_detail_subtotal_price($id);
					$dtmacc = $this->gm->get_table_where("account_debit","m_account","account_id = '".$chk_account_detail['account_id']."'");
					$du = array(
						'account_debit' => $dtmacc[0]['account_debit']-$subtotal_price
					);
					$this->gm->update_table("m_account",$du,"account_id = '".$chk_account_detail['account_id']."'");
					//END

					$arrsales = $this->Model->get_sales($id);
					$account_id = $chk_account_detail['account_id'];
					$id2 = $this->Model->get_max_id();

					$this->db->where('account_id', $account_id);
					$row = $this->Model->get_m_account($account_id)->row_array();
					$type = "debit";
					if (@$row['account_date_reset'] > $chk_account_detail['account_detail_date']) {
						$this->Model->reset_balance($account_id, (int)$chk_account_detail['account_detail_' . $type], $type, @$row['account_type_id'], $chk_account_detail['account_detail_date']);
					} else {
						$this->Model->reset_balance($account_id, (int)$chk_account_detail['account_detail_' . $type], $type);
					}
					$datax['account_detail_date'] = $arrsales['sales_date'];
					$datax['account_detail_price'] = $total_price;
					$datax['account_detail_debit'] = $total_price;  
					$datax['account_detail_paid'] = $this->Model->get_paid($id);  
					$id_account_detail = $this->Model->update_account_detail($datax, ['account_detail_real_id' => $chk_account_detail['account_detail_real_id']]);

					if (@$row['account_date_reset'] > $arrsales['sales_date']) {
						$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
					} else {
						$this->Model->update_balance($account_id,  $total_price, $type);
					}
				}

				//UPDATE SO
				$dt_account_detail_sales = $this->gm->get_table_where("*","t_account_detail_sales","sales_id = '$id'");
				if (count($dt_account_detail_sales)>0) {
					$du = array(
						'account_detail_sales_amount' => $total_price
					);
					$where = "sales_id = '$id'";
					$this->gm->update_table("t_account_detail_sales",$du,$where);

					$du = array(
						'product_id' => $this->input->post('id_produk'),
						'account_detail_sales_product_allow' => $this->input->post('boleh_kirim')
					);
					$where = "sales_detail_id = '".$this->input->post('sales_detail_id')."'";
					$this->gm->update_table("t_account_detail_sales_product", $du, $where);
				}

				$dt_root = $this->db->query("SELECT * FROM l_sales_detail_log WHERE sales_detail_id = '".$this->input->post('sales_detail_id')."' ORDER BY sales_detail_log_id DESC LIMIT 1")->result_array();
				if (count($dt_root)>0) {
					$id_log = $dt_root[0]['sales_detail_log_id'];
				} else {
					$id_log = 0;
				}

				$datalog = array(
					'sales_id' => $dt[0]['sales_id'],
					'user_id' => $_SESSION['user_id'],
					'sales_detail_log_status' => 1,
					'sales_detail_log_detail' => "Adjustment Detail Purchase Request",
					'sales_detail_log_note' => $this->input->post('note'),
					'sales_detail_log_query' => $lq,
					'sales_detail_log_date_create' => date('Y-m-d H:i:s'),
					'sales_detail_id' => $this->input->post('sales_detail_id'),
					'sales_detail_log_price' => $this->input->post('harga'),
					'sales_detail_log_quantity' => $this->input->post('qty'),
					'product_id' => $this->input->post('id_produk'),
					'product_global_id' => $product_global_id,
					'product_global_code' => $product_global_code,
					'product_global_name' => $product_global_name,
					'sales_detail_log_price_before' => $dt[0]['sales_detail_price'],
					'sales_detail_log_quantity_before' => $dt[0]['sales_detail_quantity'],
					'sales_detail_log_total_price_current' => $this->input->post('qty')*$this->input->post('harga'),
					'sales_detail_log_total_price_before' => $dt[0]['sales_detail_quantity']*$dt[0]['sales_detail_price'],
					'sales_detail_log_total_price_difference' => ($dt[0]['sales_detail_quantity']*$dt[0]['sales_detail_price']) - ($this->input->post('qty')*$this->input->post('harga')),
					'sales_detail_log_type' => 2,
					'sales_detail_log_url' => base_url().'/Adjustment/Pr/simpan',
					'sales_detail_log_form_url' => base_url().'/Adjustment/Pr/form',
					'sales_detail_file' => date('Y-m-d').'/'.$this->upload->data('file_name')
				);
				$this->gm->insert_table('l_sales_detail_log', $datalog);
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Adjustment/Pr','refresh');
	}
}
