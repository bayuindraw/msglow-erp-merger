<?php

// require 'vendor/autoload.php';

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Style\Border;

defined('BASEPATH') or exit('No direct script access allowed');

class Retur_pabrik extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('Global_model', 'gm');
		$this->load->library('session');
		$this->load->database();
		// $this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}

	public function index($Aksi = "")
	{
		$title = "Retur Pabrik";
		$cont = "Retur_pabrik";
		$dataHeader['title']		= $title;
		$dataHeader['menu']   		= 'Adjustment Outbound';
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $title;
		$dataHeader['url']			= $title;
		$data['title']				= "Log Adjustment - ".date('m/Y');
		$data['action'] 			= $Aksi;
		$data['row']				= $this->db->query("SELECT a.*, b.*, c.user_fullname, d.nama_produk, e.package_code 
			FROM l_outbound_log a 
			JOIN t_package_trial_detail b ON a.package_detail_id = b.package_detail_id 
			JOIN m_user c ON a.user_id = c.user_id
			JOIN produk d ON b.product_id = d.id_produk
			LEFT JOIN t_package_trial e ON b.package_id = e.package_id
			WHERE MONTH(a.outbound_date_create) = '".date('m')."' and YEAR(a.outbound_date_create) = '".date('Y')."' and a.outbound_type > 1 and a.outbound_log_detail LIKE '%Retur Pabrik%'")->result_array();

		$dataHeader['content'] = $this->load->view('table_retur_pabrik', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function form($Aksi = "")
	{
		$title = "Retur Pabrik";
		$cont = "Retur_pabrik";
		$dataHeader['title']		= "Adjustment ".$title;
		$dataHeader['menu']   		= $title;
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont.'/form';
		$dataHeader['url']			= $cont;
		$data['title']				= "Form ".$title;
		$data['action'] 			= $Aksi;
		$data['tp']					= $this->db->query("SELECT a.*, c.nama_produk, d.package_code
			FROM t_package_trial_detail a
			JOIN l_outbound_log b ON a.package_detail_id = b.package_detail_id
			JOIN produk c ON a.product_id = c.id_produk
			LEFT JOIN t_package_trial d ON a.package_id = d.package_id
			WHERE b.outbound_log_detail LIKE '%Outbound Retur Pabrik%'")->result_array();

		$dataHeader['content'] = $this->load->view('form_retur_pabrik', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	function simpan()
	{
		$this->db->trans_begin();
		if (!is_dir(BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d'))) {
			mkdir(BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d'), 0777);
		}

		$this->load->library('upload');
		$config['upload_path'] = BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d');
		$config['allowed_types'] = 'pdf|jpg|jpeg|png';
		$config['encrypt_name'] = TRUE;

		$this->upload->initialize($config);
		if($this->upload->do_upload("file")){
			$table = "t_package_trial_detail";
			$where = "package_detail_id = '".$this->input->post('package_detail_id')."'";
			$dt = $this->gm->get_table_where("*", $table, $where);
			if (count($dt)>0 && $dt[0]['package_id']!="") {
				$table = "t_package_trial";
				$where = "package_id = '".$dt[0]['package_id']."'";
				$dt_package = $this->gm->get_table_where("*", $table, $where);
				$tanggal = $dt_package[0]['package_date'];

				$qty_akum = $this->input->post('qty')-$dt[0]['package_detail_quantity'];

				$du = array(
					'package_detail_quantity' => $this->input->post('qty')
				);
				$this->gm->update_table("t_package_trial_detail",$du,$where);
				$lq = $this->db->last_query();

				$this->db->query("UPDATE tb_stock_produk SET jumlah = (jumlah + " . $qty_akum . ") WHERE id_barang = '" . $dt[0]['product_id'] . "' AND warehouse_id = '".$_SESSION['warehouse_id']."'");

				$this->db->query("UPDATE tb_stock_produk_history SET jumlah = (jumlah + " . $qty_akum . ") 
					WHERE id_barang = '" . $dt[0]['product_id'] . "' AND REPLACE(tanggal, '-', '') > REPLACE('" . $tanggal . "', '-', '') 
					AND warehouse_id = '".$_SESSION['warehouse_id']."'");

				$dt_root = $this->db->query("SELECT * FROM l_outbound_log WHERE package_id = '".$dt_package[0]['package_id']."' ORDER BY outbound_log_id DESC LIMIT 1")->result_array();
				if (count($dt_root)>0) {
					$id_log = $dt_root[0]['outbound_log_id'];
				} else {
					$id_log = 0;
				}

				$datalog = array(
					'package_id' => $dt_package[0]['package_id'],
					'user_id' => $_SESSION['user_id'],
					'outbound_log_status' => 1,
					'outbound_log_detail' => "Adjustment Outbound Retur Pabrik",
					'outbound_log_note' => $this->input->post('note'),
					'outbound_log_query' => $lq,
					'outbound_date_create' => date('Y-m-d H:i:s'),
					'package_detail_id' => $this->input->post('package_detail_id'),
					'outbound_quantity' => $qty_akum,
					'adjustment_target' => $this->input->post('package_detail_id'),
					'outbound_date' => date('Y-m-d H:i:s'),
					'outbound_quantity_before' => $dt[0]['package_detail_quantity'],
					'outbound_quantity_current' => $this->input->post('qty'),
					'outbound_before_log_id' => $id_log,
					'outbound_type' => 2,
					'outbound_url' => base_url().'/Adjustment/Retur_pabrik/simpan',
					'outbound_form_url' => base_url().'/Adjustment/Retur_pabrik/form',
					'outbound_file' => date('Y-m-d').'/'.$this->upload->data('file_name')
				);
				$this->gm->insert_table('l_outbound_log', $datalog);
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Adjustment/Retur_pabrik','refresh');
	}
}
