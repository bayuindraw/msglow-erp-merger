<?php

// require 'vendor/autoload.php';

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Style\Border;

defined('BASEPATH') or exit('No direct script access allowed');

class So extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('Global_model', 'gm');
		$this->load->model('T_salesModel', 'Model');
		$this->load->library('session');
		$this->load->database();
		// $this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}

	public function index($Aksi = "")
	{
		$title = "Sales Order";
		$cont = "So";
		$dataHeader['title']		= $title;
		$dataHeader['menu']   		= 'Adjustment SO';
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont;
		$dataHeader['url']			= $title;
		$data['title']				= "Log Adjustment - ".date('m/Y');
		$data['action'] 			= $Aksi;
		$data['row']				= $this->db->query("SELECT a.*, c.user_fullname, d.sales_code
			FROM l_account_detail_sales_log a 
			JOIN m_user c ON a.user_id = c.user_id
			JOIN t_sales d ON a.sales_id = d.sales_id
			WHERE MONTH(a.account_detail_sales_date_create) = '".date('m')."' and YEAR(a.account_detail_sales_date_create) = '".date('Y')."' and a.account_detail_sales_type > 1")->result_array();

		$dataHeader['content'] = $this->load->view('table_so', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function form($Aksi = "")
	{
		$title = "Sales Order";
		$cont = "So";
		$dataHeader['title']		= "Adjustment ".$title;
		$dataHeader['menu']   		= $title;
		$dataHeader['file']   		= 'Adjustment '.$title;
		$dataHeader['link']			= $cont.'/form';
		$dataHeader['url']			= $cont;
		$data['title']				= "Form ".$title;
		$data['action'] 			= $Aksi;
		$data['tp']					= $this->db->query("SELECT a.*, d.sales_code, e.nama
			FROM t_account_detail_sales a
			JOIN t_sales d ON a.sales_id = d.sales_id
			JOIN member e ON d.seller_id = e.kode
			WHERE d.sales_status != 2")->result_array();
		$data['produk']				= $this->gm->get_table_where("*","produk","id_produk != 0");

		$dataHeader['content'] = $this->load->view('form_so', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	function cek_detail()
	{
		$account_detail_sales_id = $this->input->post('account_detail_sales_id');
		$dt = $this->db->query("SELECT a.*
			FROM t_account_detail_sales a
			WHERE a.account_detail_sales_id = '$account_detail_sales_id'")->result_array();
		// $dt = $this->gm->get_table_where("*","t_sales_detail","sales_detail_id = '$sales_detail_id'");
		if (count($dt)>0) {
			echo json_encode(array('status' => true, 'data' => $dt));
		} else {
			echo json_encode(array('status' => false, 'message' => 'Data tidak ditemukan!'));
		}
	}

	function simpan()
	{
		$this->db->trans_begin();
		if (!is_dir(BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d'))) {
			mkdir(BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d'), 0777);
		}

		$this->load->library('upload');
		$config['upload_path'] = BASEPATH.'../upload/OUTBOUND/'.date('Y-m-d');
		$config['allowed_types'] = 'pdf|jpg|jpeg|png';
		$config['encrypt_name'] = TRUE;

		$this->upload->initialize($config);
		if($this->upload->do_upload("file")){
			$table = "t_account_detail_sales";
			$where = "account_detail_sales_id = '".$this->input->post('account_detail_sales_id')."'";
			$dt = $this->gm->get_table_where("*", $table, $where);

			if ($this->input->post('bayar') < $dt[0]['account_detail_sales_amount'] && $this->input->post('bayar')!=0) {
				$this->session->set_flashdata("gagal","Bayar harus lebih dari bayar sebelumnya (".$dt[0]['account_detail_sales_amount'].")");
				redirect('Adjustment/So/form');
			}

			if (count($dt)>0) {
				$id = $dt[0]['sales_id'];

				if ($this->input->post('bayar')==0) {
					//UPDATE BAYAR PR
					$du = array(
						'account_detail_sales_amount' => 0
					);
					$where = "account_detail_sales_id = '".$this->input->post('account_detail_sales_id')."'";
					$dtamount = $this->gm->get_table_where("*", "t_account_detail_sales", $where);
					$this->gm->update_table("t_account_detail_sales",$du,$where);
					$lq = $this->db->last_query();

					$datalog = array(
						'account_detail_header_id' => $dt[0]['account_detail_id'],
						'user_id' => $_SESSION['user_id'],
						'account_detail_sales_status' => 1,
						'account_detail_sales_log_detail' => "Adjustment SO Detail",
						'account_detail_sales_log_note' => $this->input->post('note'),
						'account_detail_sales_log_query' => $lq,
						'account_detail_sales_type' => 2,
						'account_detail_sales_date_create' => date('Y-m-d H:i:s'),
						'account_detail_sales_amount' => 0,
						'account_detail_sales_amount_current' => 0,
						'account_detail_sales_amount_past' => $dtamount[0]['account_detail_sales_amount'],
						'sales_id' => $id,
						'account_detail_sales_url' => base_url().'/Adjustment/So/simpan',
						'account_detail_sales_form_url' => base_url().'/Adjustment/So/form',
						'account_detail_sales_file' => date('Y-m-d').'/'.$this->upload->data('file_name')
					);
					$this->gm->insert_table('l_account_detail_sales_log', $datalog);

					//UPDATE BOLEH KIRIM
					$du = array(
						'account_detail_sales_product_allow' => 0,
						'pending_quantity' => 0,
						'connected_quantity' => 0
					);
					$where = "account_detail_sales_id = '".$this->input->post('account_detail_sales_id')."'";
					$dtp = $this->gm->get_table_where("*", "t_account_detail_sales_product", $where);
					$this->gm->update_table("t_account_detail_sales_product",$du,$where);
					$lq = $this->db->last_query();

					$dtsd = $this->gm->get_table_where("*", "t_sales_detail", "sales_detail_id = '".$dtp[0]['sales_detail_id']."'");

					$dt_p_global = $this->db->query("SELECT a.id, a.kode, a.nama_produk FROM produk_global a JOIN produk b ON a.kode = b.kd_pd WHERE b.id_produk = '".$dtsd[0]['product_id']."'")->result_array();
					$product_global_id = 0;
					$product_global_code = "";
					$product_global_name = "";
					if (count($dt_p_global)>0) {
						$product_global_id = $dt_p_global[0]['id'];
						$product_global_code = $dt_p_global[0]['kode'];
						$product_global_name = $dt_p_global[0]['nama_produk'];
					}

					$datalog = array(
						'account_detail_sales_id' => $this->input->post('account_detail_sales_id'),
						'user_id' => $_SESSION['user_id'],
						'account_detail_sales_product_status' => 1,
						'account_detail_sales_product_log_detail' => 'Adjustment SO Produk',
						'account_detail_sales_product_log_query' => $lq,
						'account_detail_sales_product_date_create' => date('Y-m-d H:i:s'),
						'account_detail_sales_product_type' => 2,
						'product_id' => $dtsd[0]['product_id'],
						'product_global_id' => $product_global_id,
						'product_global_code' => $product_global_code,
						'product_global_name' => $product_global_name,
						'account_detail_sales_product_allow' => 0,
						'account_detail_sales_product_allow_past' => $dtsd[0]['account_detail_sales_product_allow'],
						'account_detail_sales_product_allow_current' => 0,
						'account_detail_sales_product_price' => $dtsd[0]['sales_detail_price'],
						'account_detail_sales_product_price_past' => $dtsd[0]['sales_detail_price'],
						'account_detail_sales_product_total_price' => $dtsd[0]['sales_detail_price']*$dtsd[0]['sales_detail_quantity'],
						'account_detail_sales_product_total_price_past' => $dtsd[0]['sales_detail_price']*$dtsd[0]['sales_detail_quantity']
					);
					$this->gm->insert_table('l_account_detail_sales_product_log', $datalog);

					//FIFO
					if ($dtp[0]['connected_quantity']>0) {
						$qty_connect = $dtp[0]['connected_quantity'];
						$dtbrd = $this->db->query("SELECT * FROM t_bridge_beta WHERE sales_detail_id = '".$dt[0]['sales_detail_id']."'")->result_array();
						foreach ($dtbrd as $d) {
							$dtbrd2 = $this->db->query("SELECT bridge_beta_id, bridge_beta_quantity FROM t_bridge_beta WHERE sales_detail_id = '".$dt[0]['sales_detail_id']."' AND bridge_beta_quantity <= '$qty_connect' ORDER BY bridge_beta_quantity DESC LIMIT 1")->result_array();
							if (count($dtbrd2)>0) {
								$this->gm->delete_table("t_bridge_beta","bridge_beta_id = '".$dtbrd2[0]['bridge_beta_id']."'");
								$qty_connect -= $dtbrd2[0]['bridge_beta_quantity'];
								if ($qty_connect==0) {
									break;
								}
							} else {
								$dtbrd3 = $this->db->query("SELECT bridge_beta_id, bridge_beta_quantity FROM t_bridge_beta WHERE sales_detail_id = '".$dt[0]['sales_detail_id']."' AND bridge_beta_quantity > '$qty_connect' ORDER BY bridge_beta_quantity ASC LIMIT 1")->result_array();
								if (count($dtbrd3)>0) {
									$du = array(
										'bridge_beta_quantity' => $dtbrd3[0]['bridge_beta_quantity']-$qty_connect
									);
									$this->gm->update_table("t_bridge_beta",$du,"bridge_beta_id = '".$dtbrd3[0]['bridge_beta_id']."'");
									break;
								}
							}
						}
					}
					//END FIFO

					//UPDATE DEPOSIT
					$table = "t_account_detail";
					$where = "account_detail_real_id = '".$dt[0]['account_detail_real_id']."'";
					$dt_account_id = $this->gm->get_table_where("account_id", $table, $where);
					if (count($dt_account_id)>0) {
						$table = "m_account";
						$where = "account_id = '".$dt_account_id[0]['account_id']."'";
						$dt_m_account = $this->gm->get_table_where("account_deposit", $table, $where);
						if (count($dt_m_account)>0) {
							$du = array(
								'account_deposit' => $dt_m_account[0]['account_deposit']+$dt[0]['account_detail_sales_amount']
							);
							$this->gm->update_table("m_account",$du,$where);
						}
					}

				} else {
					$selisih = $this->input->post('bayar')-$dt[0]['account_detail_sales_amount'];
					//UPDATE DEPOSIT
					$deposit = 0;
					$table = "t_account_detail";
					$where = "account_detail_real_id = '".$dt[0]['account_detail_real_id']."'";
					$dt_account_id = $this->gm->get_table_where("account_id", $table, $where);
					if (count($dt_account_id)>0) {
						$table = "m_account";
						$where = "account_id = '".$dt_account_id[0]['account_id']."'";
						$dt_m_account = $this->gm->get_table_where("account_deposit", $table, $where);
						if (count($dt_m_account)>0) {
							$deposit = $dt_m_account[0]['account_deposit'];
							if ($deposit>=$selisih) {
								$du = array(
									'account_deposit' => $dt_m_account[0]['account_deposit']-$selisih
								);
								$this->gm->update_table("m_account",$du,$where);
							} else {
								$this->session->set_flashdata("gagal","Deposit tidak mencukupi");
								redirect('Adjustment/So/form');
							}
						} else {
							$this->session->set_flashdata("gagal","Deposit tidak ada");
							redirect('Adjustment/So/form');
						}
					}

					//UPDATE BAYAR PR
					$du = array(
						'account_detail_sales_amount' => $this->input->post('bayar')
					);
					$where = "account_detail_sales_id = '".$this->input->post('account_detail_sales_id')."'";
					$dtamount = $this->gm->get_table_where("*", "t_account_detail_sales", $where);
					$this->gm->update_table("t_account_detail_sales",$du,$where);
					$lq = $this->db->last_query();

					$datalog = array(
						'account_detail_header_id' => $dt[0]['account_detail_id'],
						'user_id' => $_SESSION['user_id'],
						'account_detail_sales_status' => 1,
						'account_detail_sales_log_detail' => "Adjustment SO Detail",
						'account_detail_sales_log_note' => $this->input->post('note'),
						'account_detail_sales_log_query' => $lq,
						'account_detail_sales_type' => 2,
						'account_detail_sales_date_create' => date('Y-m-d H:i:s'),
						'account_detail_sales_amount' => 0,
						'account_detail_sales_amount_current' => 0,
						'account_detail_sales_amount_past' => $dtamount[0]['account_detail_sales_amount'],
						'sales_id' => $id,
						'account_detail_sales_url' => base_url().'/Adjustment/So/simpan',
						'account_detail_sales_form_url' => base_url().'/Adjustment/So/form',
						'account_detail_sales_file' => date('Y-m-d').'/'.$this->upload->data('file_name')
					);
					$this->gm->insert_table('l_account_detail_sales_log', $datalog);
				}

			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Adjustment/So','refresh');
	}
}
