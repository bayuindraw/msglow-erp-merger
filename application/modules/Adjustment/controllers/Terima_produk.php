<?php

// require 'vendor/autoload.php';

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Style\Border;

defined('BASEPATH') or exit('No direct script access allowed');

class Terima_produk extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('Global_model', 'gm');
		$this->load->library('session');
		$this->load->database();
		// $this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}

	public function index($Aksi = "")
	{

		$dataHeader['title']		= "Terima Produk";
		$dataHeader['menu']   		= 'Adjustment';
		$dataHeader['file']   		= 'Adjustment Terima produk';
		$dataHeader['link']			= 'Terima_produk';
		$dataHeader['url']			= 'Terima produk';
		$data['title']				= "Log Adjustment - ".date('m/Y');
		$data['action'] 			= $Aksi;
		$data['row']				= $this->db->query("SELECT a.*, b.*, c.user_fullname FROM l_inbound_log a 
			JOIN terima_produk b ON a.adjustment_target = b.id_terima_kemasan 
			JOIN m_user c ON a.user_id = c.user_id
			WHERE MONTH(a.inbound_date_create) = '".date('m')."' and YEAR(a.inbound_date_create) = '".date('Y')."' and a.inbound_type > 1 and inbound_log_detail LIKE '%Terima Produk%'")->result_array();

		$dataHeader['content'] = $this->load->view('table', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function form($Aksi = "")
	{

		$dataHeader['title']		= "Adjustment Terima Produk";
		$dataHeader['menu']   		= 'Terima Produk';
		$dataHeader['file']   		= 'Adjustment Terima Produk';
		$dataHeader['link']			= 'terima_produk/form';
		$dataHeader['url']			= 'terima_produk';
		$data['title']				= "Form Terima Produk";
		$data['action'] 			= $Aksi;
		$data['tp']					= $this->db->query("SELECT a.*, c.nama_produk
			FROM terima_produk a
			JOIN produk_global c ON a.id_barang = c.id
			JOIN po_produk d ON a.kode_po = d.kode_po
			JOIN tb_detail_po_produk e ON a.kode_po = e.kode_pb
			WHERE a.status = 1 and d.active = 'Y'")->result_array();

		$dataHeader['content'] = $this->load->view('form', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	function simpan()
	{
		$this->db->trans_begin();
		if (!is_dir(BASEPATH.'../upload/INBOUND/'.date('Y-m-d'))) {
			mkdir(BASEPATH.'../upload/INBOUND/'.date('Y-m-d'), 0777);
		}

		$this->load->library('upload');
		$config['upload_path'] = BASEPATH.'../upload/INBOUND/'.date('Y-m-d');
		$config['allowed_types'] = 'pdf|jpg|jpeg|png';
		$config['encrypt_name'] = TRUE;

		$this->upload->initialize($config);
		if($this->upload->do_upload("file")){
			$table = "terima_produk";
			$where = "id_terima_kemasan = '".$this->input->post('id_terima_kemasan')."'";
			$dt = $this->gm->get_table_where("*", $table, $where);

			$qty_akum = $this->input->post('qty') - $dt[0]['jumlah'];

			$du = array(
				'jumlah' => $this->input->post('qty'),
				'jumlah_sj' => $this->input->post('qty')
			);
			$this->gm->update_table($table,$du,$where);
			$lq = $this->db->last_query();

			$dt_root = $this->db->query("SELECT * FROM l_inbound_log WHERE terima_produk_id = '".$this->input->post('id_terima_kemasan')."' ORDER BY inbound_log_id DESC LIMIT 1")->result_array();
			if (count($dt_root)>0) {
				$id_log = $dt_root[0]['inbound_log_id'];
			} else {
				$id_log = 0;
			}

			$datalog = array(
				'user_id' => $_SESSION['user_id'],
				'inbound_log_status' => 1,
				'inbound_log_detail' => 'Adjustment Terima Produk',
				'inbound_log_note' => $this->input->post('note'),
				'inbound_log_query' => $lq,
				'inbound_date_create' => date('Y-m-d H:i:s'),
				'terima_produk_id' => $this->input->post('id_terima_kemasan'),
				'adjustment_target' => $this->input->post('id_terima_kemasan'),
				'inbound_quantity' => $qty_akum,
				'inbound_quantity_before' => $dt[0]['jumlah'],
				'inbound_quantity_current' => $this->input->post('qty'),
				'inbound_date' => $dt[0]['tgl_terima'],
				'inbound_type' => 2,
				'inbound_before_log_id' => $id_log,
				'inbound_url' => base_url().'/Adjustment/Terima_produk/simpan',
				'inbound_form_url' => base_url().'/Adjustment/Terima_produk/form',
				'inbound_file' => date('Y-m-d').'/'.$this->upload->data('file_name')
			);
			$this->gm->insert_table('l_inbound_log', $datalog);

			$this->db->query("UPDATE tb_stock_produk SET jumlah = (jumlah + $qty_akum) WHERE id_barang = '" . $dt[0]['id_barang'] . "' AND warehouse_id = '".$dt[0]['warehouse_id']."'");
			$this->db->query("UPDATE tb_stock_produk_history SET jumlah = (jumlah + $qty_akum) WHERE id_barang = '" . $dt[0]['id_barang'] . "' AND REPLACE(tanggal, '-', '') > REPLACE('" . $dt[0]['tgl_terima'] . "', '-', '') AND warehouse_id = '".$dt[0]['warehouse_id']."'");
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Adjustment/Terima_produk','refresh');
	}

	function batal($id)
	{
		$this->db->trans_begin();
		$table = "l_inbound_log";
		$where = "inbound_log_id = '$id'";
		$dt_log = $this->gm->get_table_where("*", $table, $where);

		$table = "terima_produk";
		$where = "id_terima_kemasan = '".$dt_log[0]['terima_produk_id']."'";
		$dt = $this->gm->get_table_where("*", $table, $where);
		
		$qty_akum = $dt_log[0]['inbound_quantity_current']-$dt_log[0]['inbound_quantity_before'];

		$du = array(
			'jumlah' => $dt_log[0]['inbound_quantity_before'],
			'jumlah_sj' => $dt_log[0]['inbound_quantity_before']
		);
		$this->gm->update_table($table,$du,$where);
		$lq = $this->db->last_query();

		$datalog = array(
			'user_id' => $_SESSION['user_id'],
			'inbound_log_status' => 1,
			'inbound_log_detail' => 'Batal Adjustment Terima Produk',
			'inbound_log_query' => $lq,
			'inbound_date_create' => date('Y-m-d H:i:s'),
			'terima_produk_id' => $dt_log[0]['terima_produk_id'],
			'adjustment_target' => $dt_log[0]['terima_produk_id'],
			'inbound_quantity' => $qty_akum,
			'inbound_quantity_before' => $dt_log[0]['inbound_quantity_current'],
			'inbound_quantity_current' => $dt_log[0]['inbound_quantity_before'],
			'inbound_date' => $dt[0]['tgl_terima'],
			'inbound_type' => 2,
			'inbound_before_log_id' => $dt_log[0]['inbound_log_id'],
			'inbound_url' => base_url().'/Adjustment/Terima_produk/batal',
			'inbound_form_url' => base_url().'/Adjustment/Terima_produk'
		);
		$this->gm->insert_table('l_inbound_log', $datalog);

		$this->db->query("UPDATE tb_stock_produk SET jumlah = (jumlah + $qty_akum) WHERE id_barang = '" . $dt[0]['id_barang'] . "' AND warehouse_id = '".$dt[0]['warehouse_id']."'");
		$this->db->query("UPDATE tb_stock_produk_history SET jumlah = (jumlah + $qty_akum) WHERE id_barang = '" . $dt[0]['id_barang'] . "' AND REPLACE(tanggal, '-', '') > REPLACE('" . $dt[0]['tgl_terima'] . "', '-', '') AND warehouse_id = '".$dt[0]['warehouse_id']."'");

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Adjustment/Terima_produk','refresh');
	}
}
