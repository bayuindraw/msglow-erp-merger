<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            <?php echo $title ?>
          </h3>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">

      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= base_url() ?>Adjustment/Payment/simpan" method="POST" enctype='multipart/form-data'>

          <div class="form-group">
            <i class="far fa-calendar-alt"></i>
            <label>Tanggal Adjustment</label>
            <br>
            <span style="font-weight: bold;"><?php echo date('Y-m-d') ?></span>
            <input type="hidden" id="tgl" name="tgl" value="<?php echo date('Y-m-d') ?>">
          </div>
          <div class="form-group">
            <i class="fas fa-tag"></i>
            <label>Kode Payment</label>
            <select name="account_detail_id" id="pilihData" class="md-form-control md-static" required onchange="cek_account_detail()">
              <option></option>
              <?php foreach ($tp as $d) { ?>
                <option value="<?= $d['account_detail_id'] ?>"><?= $d['sales_code'].' - '.$d['nama'].' (Payment: '.$d['account_detail_credit'].')' ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <i class="fas fa-plus-circle"></i>
            <label>Jumlah Payment Seharusnya</label>
            <input type="number" id="payment" name="payment" class="form-control" value="0" min="1" class="md-form-control md-static" required>
          </div>
          <div class="form-group">
            <i class="fas fa-exclamation-circle"></i>
            <label>Note</label>
            <input type="text" id="note" name="note" class="form-control" value="" class="md-form-control md-static" required placeholder="Tambahkan note..">
          </div>
          <div class="form-group">
            <i class="fas fa-file"></i>
            <label>File</label>
            <input type="file" id="file" name="file" class="form-control" value="" class="md-form-control md-static" required><br>
            <span style="font-size: 8pt; font-weight: bold;">Type: PDF / JPG / PNG</span>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light pull-right" title="Tambah Adjustment">
                <i class="fas fa-save"></i><span class="m-l-10"> Simpan</span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function cek_account_detail() {
    var account_detail_id = document.getElementById('pilihData').value;

    $.ajax({
      type:'post',
      data:{
        "account_detail_id":account_detail_id
      },
      url: '<?php echo site_url('Adjustment/Payment/cek_data') ?>',
      success: function(data){
        var json = $.parseJSON(data);
        if(json.status){
          document.getElementById("payment").value = json.data[0]['account_detail_credit'];
        }
      }
    });
  }
</script>