<div class="kt-portlet kt-portlet--mobile">

  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        <?php echo $title ?>
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
          <!-- <a href="<?= base_url() ?>Laporan2/daily_stock" class="btn btn-primary waves-effect waves-light" target="_blank"> <i class="la la-print"></i> <span class="m-l-10">Cetak Stock</span></a> -->
          <a href="<?= base_url() ?>Adjustment/Po/form" class="btn btn-primary waves-effect waves-light"> <i class="fa fa-edit"></i> <span class="m-l-10">Ajukan Adjustment</span></a>
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">

    <!--begin: Datatable -->
    <div content="table">
      <table class="table table-striped table-bordered nowrap" id="kt_table_1">
        <thead>
          <tr>
            <th style="text-align: center;">Kode</th>
            <th style="text-align: center;">Inbound Detail</th>
            <th style="text-align: center;">Produk</th>
            <th style="text-align: center;">Qty Adjust</th>
            <th style="text-align: center;">Qty Before</th>
            <th style="text-align: center;">Qty Current</th>
            <th style="text-align: center;">File</th>
            <th style="text-align: center;">Yang mengajukan</th>
            <!-- <th style="text-align: center;">Action</th> -->
          </tr>
        </thead>
        <tbody>
          <?php $no = 0; foreach ($row as $d) { ?>
            <tr>
              <td><?= $d['kode_pb'] ?></td>
              <td><?= $d['po_goods_detail_log_detail'] ?></td>
              <td><?= $d['nama_produk_log'] ?></td>
              <td style="text-align: center;"><?= $d['po_goods_detail_log_quantity'] ?></td>
              <td style="text-align: center;"><?= $d['po_goods_detail_log_quantity_before'] ?></td>
              <td style="text-align: center;"><?= $d['po_goods_detail_log_quantity_current'] ?></td>
              <td style="text-align: center;"><a href="<?php echo base_url().'upload/INBOUND/'.$d['po_goods_detail_log_file'] ?>" target="_blank"><i class="fa fa-file" style="font-size: 30pt;"></i></a></td>
              <td><?= $d['user_fullname'] ?></td>
              <!-- <td style="text-align: center;">
                <a href="<?php echo site_url('terima_produk/batal') ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Batal</a>
              </td> -->
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#kt_table_1').DataTable({
      "pagingType": "full_numbers",
      scrollY: "300px",
      scrollX: true,
      scrollCollapse: true,
      "processing": true,
      "paging": false,
      "ordering": false,
      "serverSide": false
    });
  });
</script>