<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting extends CI_Controller {

	public function __construct(){
		
        parent::__construct();
        ob_start();
		/*error_reporting(0);*/
        $this->load->model('model');
        $this->load->model('relasi');
        $this->load->library('session');
        $this->load->database();
        $this->load->library('m_pdf');
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download'); 
		 
    }
		public  function Date2String($dTgl){
			//return 2012-11-22
			list($cDate,$cMount,$cYear)	= explode("-",$dTgl) ;
			if(strlen($cDate) == 2){
				$dTgl	= $cYear . "-" . $cMount . "-" . $cDate ;
			}
			return $dTgl ; 
		}
		
		public  function String2Date($dTgl){
			//return 22-11-2012  
			list($cYear,$cMount,$cDate)	= explode("-",$dTgl) ;
			if(strlen($cYear) == 4){
				$dTgl	= $cDate . "-" . $cMount . "-" . $cYear ;
			} 
			return $dTgl ; 	
		}
		
		public function TimeStamp() {
			date_default_timezone_set("Asia/Jakarta");
			$Data = date("H:i:s");
			return $Data ;
		}
			
		public function DateStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("d-m-Y");
			return $Data ;
		}  
			
		public function DateTimeStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("Y-m-d h:i:s");
			return $Data ;
		} 

		public function signin($Action=""){
		$data = "" ;
		if($Action == "error"){
			$data['notif'] = "Username / Password Salah" ;
			}
			$msg 			 = 'My secret message';
			$this->load->view('back-end/login-secure',$data);
		
		}

		public function signinsecure($Action=""){
		$data = "" ;
		if($Action == "error"){
			$data['notif'] = "Username / Password Salah" ;
			}
			$msg 			 = 'My secret message';
			$this->load->view('back-end/login',$data);
		
		}

		public function list_penjualan_day($Aksi="",$Id=""){

			$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Penjualan Seller Msglow' ;
			$dataHeader['link']		= 'kemasan';
			$data['action'] 		= $Aksi ; 
			$data['row']			= $this->model->code("SELECT * FROM v_penjualan WHERE tgl_jual = '".date('Y-m-d')."' ORDER BY tgl_jual DESC");
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/penjualan/list-penjualan-marketing',$data);
			$this->load->view('back-end/container/footer');

		}

		public function list_penjualan_selectday($Aksi="",$Id=""){

			$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Penjualan Seller Msglow' ;
			$dataHeader['link']		= 'kemasan';
			$data['action'] 		= $Aksi ;
			if(empty($Aksi)){
				$data['tanggalA'] 		= date('d-m-Y') ;
				$data['tanggalB'] 		= date('d-m-Y');
				$data['row']			= $this->model->code("SELECT * FROM v_penjualan WHERE tgl_jual = '".date('Y-m-d')."' ORDER BY tgl_jual DESC");
			}else{
				$data['tanggalA'] 		= $this->input->post('tanggalanA');
				$data['tanggalB'] 		= $this->input->post('tanggalanB');
				$data['row']			= $this->model->code("SELECT * FROM v_penjualan WHERE tgl_jual BETWEEN  '".$this->Date2String($this->input->post('tanggalanA'))."' AND  '".$this->Date2String($this->input->post('tanggalanB'))."' ORDER BY tgl_jual DESC");
			}
			
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/penjualan/list-penjualan-marketing-selectday',$data);
			$this->load->view('back-end/container/footer');

		}

		public function list_penjualanpending_selectday($Aksi="",$Id=""){

			$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Penjualan Seller Msglow' ;
			$dataHeader['link']		= 'kemasan';
			$data['action'] 		= $Aksi ;
			if(empty($Aksi)){
				$data['tanggalA'] 		= date('d-m-Y') ;
				$data['tanggalB'] 		= date('d-m-Y');
				$data['row']			= $this->model->code("SELECT * FROM v_penjualan WHERE tgl_jual = '".date('Y-m-d')."' AND approved_by IS NULL ORDER BY tgl_jual DESC");
			}else{
				$data['tanggalA'] 		= $this->input->post('tanggalanA');
				$data['tanggalB'] 		= $this->input->post('tanggalanB');
				$data['row']			= $this->model->code("SELECT * FROM v_penjualan WHERE tgl_jual BETWEEN '".$this->Date2String($this->input->post('tanggalanA'))."' AND  '".$this->Date2String($this->input->post('tanggalanB'))."' AND approved_by IS NULL ORDER BY tgl_jual DESC");
			}
			
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/penjualan/list-penjualan-marketing-selectday',$data);
			$this->load->view('back-end/container/footer');

		}

		public function list_pembayaran_day($Aksi="",$Id=""){

			$dataHeader['title']	= "Pembayaran Penjualan Seller - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Penjualan Seller Msglow' ;
			$dataHeader['link']		= 'kemasan';
			$data['action'] 		= $Aksi ; 
			$data['row']			= $this->model->code("SELECT * FROM v_pembayaran_seller WHERE tanggal = '".date('Y-m-d')."' AND approved_by IS NOT NULL ");
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/penjualan/list-pembayaran-marketing',$data);
			$this->load->view('back-end/container/footer');

		}

		public function list_pembayaran_selectday($Aksi="",$Id=""){

			$dataHeader['title']	= "Pembayaran Penjualan Seller - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Penjualan Seller Msglow' ;
			$dataHeader['link']		= 'kemasan';
			$data['action'] 		= $Aksi ; 

			if(empty($Aksi)){
				$data['tanggalA'] 		= date('d-m-Y') ;
				$data['tanggalB'] 		= date('d-m-Y');
				$data['row']			= $this->model->code("SELECT * FROM v_pembayaran_seller WHERE tanggal = '".date('Y-m-d')."' AND approved_by IS NOT NULL ORDER BY tgl_jual DESC");
			}else{
				$data['tanggalA'] 		= $this->input->post('tanggalanA');
				$data['tanggalB'] 		= $this->input->post('tanggalanB');
				$data['row']			= $this->model->code("SELECT * FROM v_pembayaran_seller WHERE tanggal BETWEEN '".$this->Date2String($this->input->post('tanggalanA'))."' AND  '".$this->Date2String($this->input->post('tanggalanB'))."' AND approved_by IS NOT NULL ORDER BY tanggal DESC");
			}
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/penjualan/list-pembayaran-marketing-selectday',$data);
			$this->load->view('back-end/container/footer');

		}

		public function list_pembayaran_pending($Aksi="",$Id=""){

			$dataHeader['title']	= "Pembayaran Penjualan Seller - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Penjualan Seller Msglow' ;
			$dataHeader['link']		= 'kemasan';
			$data['action'] 		= $Aksi ; 
			$data['row']			= $this->model->code("SELECT * FROM v_pembayaran_seller WHERE approved_by IS NULL ");
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/penjualan/list-pembayaran-marketing',$data);
			$this->load->view('back-end/container/footer');

		}

		public function detail_penjualan($Id=""){

			$dataHeader['title']	= "Detail penjualan - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Penjualan Seller Msglow' ;
			$dataHeader['link']		= 'kemasan';
			$data['action'] 		= $Id ; 

			$data['po']				= $this->model->code("SELECT * FROM v_detail_produk_jual WHERE kode_jual = '".$Id."'");
			$data['detail']			= $this->model->code("SELECT * FROM v_pembayaran_seller WHERE kode_penjualan = '".$Id."'");

			$data['total_bayar']	= $this->model->code("SELECT sum(jumlah) as total , total_bayar FROM v_pembayaran_seller WHERE kode_penjualan = '".$Id."'");
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/penjualan/detail-penjualan',$data);
			$this->load->view('back-end/container/footer');

		}
		
		public function penjualan_seller_bayar($Aksi="",$Id=""){

			$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Penjualan Seller Msglow' ;
			$dataHeader['link']		= 'kemasan';
			$data['action'] 		= $Aksi ; 
			$data['row']			= $this->model->code("SELECT * FROM v_penjualan ORDER BY tgl_jual DESC");
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/penjualan/list-penjualan-marketing-bayar',$data);
			$this->load->view('back-end/container/footer');

		}

		public function terima_bayar_penjualan($Id=""){

			$dataHeader['title']	= "Input Pengemasan - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Penjualan Seller Msglow' ;
			$dataHeader['link']		= 'kemasan';
			$data['action'] 		= $Id ; 

			$data['po']				= $this->model->code("SELECT * FROM v_detail_produk_jual WHERE kode_jual = '".$Id."'");
			$data['detail']			= $this->model->code("SELECT * FROM penjualan_detail_bayar WHERE kode_jual = '".$Id."'");
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/penjualan/pembayaran-penjualan',$data);
			$this->load->view('back-end/container/footer');

		}
		
		public function laporan_marketing($Aksi=""){
			
			$dataHeader['title']		= "Laporan Purchase Order Kemasan MSGLOW";
			$dataHeader['menu']   		= 'Laporan Po Kemasan';
			$dataHeader['file']   		= 'laporan_po' ;
			$dataHeader['link']			= 'laporan_po';
			$data['action'] 			= $Aksi ;

			$data['po']					= $this->model->ViewWhere('v_po_produk','kode_po',$Aksi);
			$data['row']				= $this->model->ViewWhere('v_detail_po_produk','kode_pb',$Aksi);
			$data['bayar']				= $this->model->ViewWhere('v_terima_produk','kode_po',$Aksi);

	
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/penjualan/laporan-marketing',$data);
			$this->load->view('back-end/container/footer');
		}
}
