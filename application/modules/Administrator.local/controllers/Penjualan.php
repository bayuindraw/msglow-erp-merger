<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('model');
		$this->load->model('relasi');
		$this->load->library('session');
		$this->load->database();
		$this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}
	
	public  function Date2String($dTgl)
	{
		//return 2012-11-22
		list($cDate, $cMount, $cYear)	= explode("-", $dTgl);
		if (strlen($cDate) == 2) {
			$dTgl	= $cYear . "-" . $cMount . "-" . $cDate;
		}
		return $dTgl;
	}

	public  function String2Date($dTgl)
	{
		//return 22-11-2012  
		list($cYear, $cMount, $cDate)	= explode("-", $dTgl);
		if (strlen($cYear) == 4) {
			$dTgl	= $cDate . "-" . $cMount . "-" . $cYear;
		}
		return $dTgl;
	}

	public function TimeStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("H:i:s");
		return $Data;
	}

	public function DateStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("d-m-Y");
		return $Data;
	}

	public function DateTimeStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("Y-m-d h:i:s");
		return $Data;
	}

	public function signin($Action = "")
	{
		$data = "";
		if ($Action == "error") {
			$data['notif'] = "Username / Password Salah";
		}
		$msg 			 = 'My secret message';
		$this->load->view('back-end/login-secure', $data);
	}

	public function signinsecure($Action = "")
	{
		$data = "";
		if ($Action == "error") {
			$data['notif'] = "Username / Password Salah";
		}
		$msg 			 = 'My secret message';
		$this->load->view('back-end/login', $data);
	}

	public function kodePJL()
	{

		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOW-PJL-";

		$dbDate	=	$this->db->query("SELECT COUNT(id_penjualan) as JumlahTransaksi FROM penjualan_seller WHERE bulan = '" . date('m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 1;
			}
		} else {
			$nJumlahTransaksi = 1;
		}

		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}


		return $cKode;
	}

	public function kodeECER()
	{

		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOW-ECER-";

		$dbDate	=	$this->db->query("SELECT COUNT(id_penjualan) as JumlahTransaksi FROM penjualan_seller WHERE bulan = '" . date('m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 1;
			}
		} else {
			$nJumlahTransaksi = 1;
		}

		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}


		return $cKode;
	}

	public function kodeRETAIL()
	{

		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOW-RETAIL-";

		$dbDate	=	$this->db->query("SELECT COUNT(id_penjualan) as JumlahTransaksi FROM penjualan_seller WHERE bulan = '" . date('m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 1;
			}
		} else {
			$nJumlahTransaksi = 1;
		}

		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}


		return $cKode;
	}


	public function penjualan_po($Aksi = "", $Id = "")
	{
		$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;
		$data['kodepo']			= $this->kodeECER();
		$data['row']			= $this->model->ViewWhere('v_detail_produk_jual', 'kode_jual', $this->kodeECER());

		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/penjualan/agen-penjualan', $data);
		$this->load->view('back-end/container/footer');
	}

	public function penjualan_po_wajah($Aksi = "", $Id = "")
	{
		$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;
		$data['kodepo']			= $this->kodePJL();
		$data['row']			= $this->model->ViewWhere('v_detail_produk_jual', 'kode_jual', $this->kodePJL());

		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/penjualan/agen-penjualan-wajah', $data);
		$this->load->view('back-end/container/footer');
	}

	public function penjualan_ecer($Aksi = "", $Id = "")
	{
		$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;
		$data['kodepo']			= $this->kodeRETAIL();
		$data['row']			= $this->model->ViewWhere('v_detail_produk_jual', 'kode_jual', $this->kodeRETAIL());

		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/penjualan/ecer-penjualan', $data);
		$this->load->view('back-end/container/footer');
	}



	public function ambil_harga($Id = "")
	{

		$query = $this->model->ViewWhere('produk', 'id_produk', $Id);
		foreach ($query as $key => $vaData) {
			$harga = $vaData['harga'];
		}
		$data_pegawai = array('harga'   	=>  $harga);

		echo json_encode($data_pegawai);
	}

	public function ambil_harga_baru($Id = "")
	{

		$query = $this->model->ViewWhere('master_harga_display', 'id_ms_harga', $Id);
		foreach ($query as $key => $vaData) {
			$harga = $vaData['harga_total'];
		}
		$data_pegawai = array('harga'   	=>  $harga);

		echo json_encode($data_pegawai);
	}

	public function penjualan_seller($Aksi = "", $Id = "")
	{

		$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;
		$data['row']			= $this->model->code("SELECT * FROM v_penjualan ORDER BY tgl_jual DESC");

		$dataHeader['content'] = $this->load->view('back-end/penjualan/list-penjualan-marketing', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function list_penjualan_day($Aksi = "", $Id = "")
	{

		$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;
		$data['row']			= $this->model->code("SELECT * FROM v_penjualan WHERE tgl_jual = '" . date('Y-m-d') . "' ORDER BY tgl_jual DESC");

		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/penjualan/list-penjualan-marketing', $data);
		$this->load->view('back-end/container/footer');
	}

	public function list_penjualan_selectday($Aksi = "", $Id = "")
	{

		$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;
		if (empty($Aksi)) {
			$data['tanggalA'] 		= date('d-m-Y');
			$data['tanggalB'] 		= date('d-m-Y');
			$data['row']			= $this->model->code("SELECT * FROM v_penjualan WHERE tgl_jual = '" . date('Y-m-d') . "' ORDER BY tgl_jual DESC");
		} else {
			$data['tanggalA'] 		= $this->input->post('tanggalanA');
			$data['tanggalB'] 		= $this->input->post('tanggalanB');
			$data['row']			= $this->model->code("SELECT * FROM v_penjualan WHERE tgl_jual BETWEEN  '" . $this->Date2String($this->input->post('tanggalanA')) . "' AND  '" . $this->Date2String($this->input->post('tanggalanB')) . "' ORDER BY tgl_jual DESC");
		}


		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/penjualan/list-penjualan-marketing-selectday', $data);
		$this->load->view('back-end/container/footer');
	}

	public function list_penjualanpending_selectday($Aksi = "", $Id = "")
	{

		$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;
		if (empty($Aksi)) {
			$data['tanggalA'] 		= date('d-m-Y');
			$data['tanggalB'] 		= date('d-m-Y');
			$data['row']			= $this->model->code("SELECT * FROM v_penjualan WHERE tgl_jual = '" . date('Y-m-d') . "' AND approved_by IS NULL ORDER BY tgl_jual DESC");
		} else {
			$data['tanggalA'] 		= $this->input->post('tanggalanA');
			$data['tanggalB'] 		= $this->input->post('tanggalanB');
			$data['row']			= $this->model->code("SELECT * FROM v_penjualan WHERE tgl_jual BETWEEN '" . $this->Date2String($this->input->post('tanggalanA')) . "' AND  '" . $this->Date2String($this->input->post('tanggalanB')) . "' AND approved_by IS NULL ORDER BY tgl_jual DESC");
		}


		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/penjualan/list-penjualan-marketing-selectday', $data);
		$this->load->view('back-end/container/footer');
	}

	public function list_pembayaran_day($Aksi = "", $Id = "")
	{

		$dataHeader['title']	= "Pembayaran Penjualan Seller - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;
		$data['row']			= $this->model->code("SELECT * FROM v_pembayaran_seller WHERE tanggal = '" . date('Y-m-d') . "'");

		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/penjualan/list-pembayaran-marketing', $data);
		$this->load->view('back-end/container/footer');
	}

	public function list_pembayaran_selectday($Aksi = "", $Id = "")
	{

		$dataHeader['title']	= "Pembayaran Penjualan Seller - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;

		if (empty($Aksi)) {
			$data['tanggalA'] 		= date('d-m-Y');
			$data['tanggalB'] 		= date('d-m-Y');
			$data['row']			= $this->model->code("SELECT * FROM v_pembayaran_seller WHERE tanggal = '" . date('Y-m-d') . "' AND approved_by IS NULL ORDER BY tgl_jual DESC");
		} else {
			$data['tanggalA'] 		= $this->input->post('tanggalanA');
			$data['tanggalB'] 		= $this->input->post('tanggalanB');
			$data['row']			= $this->model->code("SELECT * FROM v_pembayaran_seller WHERE tanggal BETWEEN '" . $this->Date2String($this->input->post('tanggalanA')) . "' AND  '" . $this->Date2String($this->input->post('tanggalanB')) . "' AND approved_by IS NULL ORDER BY tanggal DESC");
		}

		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/penjualan/list-pembayaran-marketing-selectday', $data);
		$this->load->view('back-end/container/footer');
	}

	public function input_kemas($Aksi = "", $Id = "")
	{

		$dataHeader['title']	= "Input Pengemasan - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;
		$data['row']			= $this->model->code("SELECT * FROM v_penjualan ORDER BY tgl_jual DESC");

		$dataHeader['content'] = $this->load->view('back-end/penjualan/list-penjualan-kemas', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function kemas_produk($Id = "")
	{

		$dataHeader['title']	= "Input Pengemasan - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Id;
		$data['po']				= $this->model->code("SELECT * FROM v_detail_produk_jual WHERE kode_jual = '" . $Id . "'");
		$data['detail']			= $this->model->code("SELECT * FROM v_detail_kemas WHERE kode_jual = '" . $Id . "'");

		$dataHeader['content'] = $this->load->view('back-end/penjualan/kemas-produk', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function kemas_pending($Id = "")
	{

		$dataHeader['title']	= "Input Pengemasan - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Id;

		$data['row']			= $this->model->code("SELECT * FROM v_buat_sj ORDER BY tanggal DESC");


		$dataHeader['content'] = $this->load->view('back-end/penjualan/list-penjualan-kemas-pending', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function detail_penjualan($Id = "")
	{

		$dataHeader['title']	= "Detail penjualan - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Id;

		$data['po']				= $this->model->code("SELECT * FROM v_detail_produk_jual WHERE kode_jual = '" . $Id . "'");
		$data['detail']			= $this->model->code("SELECT * FROM v_pembayaran_seller WHERE kode_penjualan = '" . $Id . "'");

		$data['total_bayar']	= $this->model->code("SELECT sum(jumlah) as total , total_bayar FROM v_pembayaran_seller WHERE kode_penjualan = '" . $Id . "'");


		$dataHeader['content'] = $this->load->view('back-end/penjualan/detail-penjualan', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function penjualan_seller_bayar($Aksi = "", $Id = "")
	{

		$dataHeader['title']	= "Penjualan Agen - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Aksi;
		$data['row']			= $this->model->code("SELECT * FROM v_penjualan ORDER BY tgl_jual DESC");

		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/penjualan/list-penjualan-marketing-bayar', $data);
		$this->load->view('back-end/container/footer');
	}

	public function terima_bayar_penjualan($Id = "")
	{

		$dataHeader['title']	= "Input Pengemasan - ERP MSGLOW 2019";
		$dataHeader['menu']   	= 'Master';
		$dataHeader['file']   	= 'Penjualan Seller Msglow';
		$dataHeader['link']		= 'kemasan';
		$data['action'] 		= $Id;

		$data['po']				= $this->model->code("SELECT * FROM v_detail_produk_jual WHERE kode_jual = '" . $Id . "'");
		$data['detail']			= $this->model->code("SELECT * FROM penjualan_detail_bayar WHERE kode_jual = '" . $Id . "'");

		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/penjualan/pembayaran-penjualan', $data);
		$this->load->view('back-end/container/footer');
	}

	public function laporan_marketing($Aksi = "")
	{

		$dataHeader['title']		= "Laporan Purchase Order Kemasan MSGLOW";
		$dataHeader['menu']   		= 'Laporan Po Kemasan';
		$dataHeader['file']   		= 'Laporan Marketing';
		$dataHeader['link']			= 'laporan_po';
		$data['action'] 			= $Aksi;

		$data['po']					= $this->model->ViewWhere('v_po_produk', 'kode_po', $Aksi);
		$data['row']				= $this->model->ViewWhere('v_detail_po_produk', 'kode_pb', $Aksi);
		$data['bayar']				= $this->model->ViewWhere('v_terima_produk', 'kode_po', $Aksi);


		$dataHeader['content'] = $this->load->view('back-end/penjualan/laporan-marketing', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function laporan_penjualan_harian($bulan = "", $tahun = "", $idbarang = "")
	{
		$data['tanggal'] 	= $this->input->post('cTanggal');
		$data['row']		= $this->model->code("SELECT * FROM penjualan_seller WHERE tgl_jual = '" . $this->Date2String($this->input->post('cTanggal')) . "'");

		$this->load->view('back-end/penjualan/cetak/laporan-penjualan-harian', $data);
	}

	// public function laporan_penjualan_harian($bulan="",$tahun="",$idbarang=""){

	//     $data['tanggal']		= $this->input->post('cTanggal')	;
	//     $data['row']			= $this->model->code("SELECT * FROM penjualan_seller WHERE tgl_jual = '".$this->Date2String($this->input->post('cTanggal'))."'");
	//     $html				= $this->load->view('back-end/penjualan/cetak/laporan-penjualan-harian', $data, true);

	//     //this the the PDF filename that user will get to download
	//     $pdfFilePath 		= "laporan-penjualan-harian.pdf";

	//     //load mPDF library
	// 	$this->m_pdf->pdf->mPDF('','A4-L','10','ctimesbi','5','5','5','5','5','10','P');
	// 	$this->m_pdf->pdf->useOddEven = true ;

	//    //generate the PDF from the given html
	//     $this->m_pdf->pdf->WriteHTML($html);

	//     //download it.
	//     $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	// }

	public function laporan_penjualan_bulanan($bulan = "", $tahun = "", $idbarang = "")
	{
		$data['mbulan'] 	= $this->input->post('cBulan');
		$data['tahun']		= $this->input->post('cTahun');
		$data['row']		= $this->model->code("SELECT * FROM penjualan_seller WHERE MONTH(tgl_jual) = '" . $this->input->post('cBulan') . "' 
	     						  AND YEAR(tgl_jual) = '" . $this->input->post('cTahun') . "' ORDER BY tgl_jual ASC");

		$this->load->view('back-end/penjualan/cetak/laporan-penjualan-bulanan', $data);
	}

	// public function laporan_penjualan_bulanan($bulan="",$tahun="",$idbarang=""){

	//  $data['mbulan']	  		= $this->input->post('cBulan');
	// 	$data['tahun']	  		= $this->input->post('cTahun');

	// $data['row']			= $this->model->code("SELECT * FROM penjualan_seller WHERE MONTH(tgl_jual) = '".$this->input->post('cBulan')."' 
	// 						  AND YEAR(tgl_jual) = '".$this->input->post('cTahun')."' ORDER BY tgl_jual ASC");

	//     $html					= $this->load->view('back-end/penjualan/cetak/laporan-penjualan-bulanan', $data, true);

	//     //this the the PDF filename that user will get to download
	//     $pdfFilePath 		= "laporan-penjualan-harian.pdf";

	//     //load mPDF library
	// 	$this->m_pdf->pdf->mPDF('','A4-L','10','ctimesbi','5','5','5','5','5','10','P');
	// 	$this->m_pdf->pdf->useOddEven = true ;

	//    //generate the PDF from the given html
	//     $this->m_pdf->pdf->WriteHTML($html);


	//     //download it.
	//     $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	// }	

	public function laporan_penjualan_customer($bulan = "", $tahun = "", $idbarang = "")
	{
		$data['seller'] 	= $this->input->post('cSupplier');
		$data['mbulan']		= $this->input->post('cBulan');
		$data['tahun']		= $this->input->post('cTahun');
		$data['row']		= $this->model->code("SELECT * FROM penjualan_seller WHERE MONTH(tgl_jual) = '" . $this->input->post('cBulan') . "' 
	     					  AND YEAR(tgl_jual) = '" . $this->input->post('cTahun') . "' AND kode_seller = '" . $this->input->post('cSupplier') . "' ORDER BY tgl_jual ASC");

		$this->load->view('back-end/penjualan/cetak/laporan-penjualan-seller', $data);
	}

	// public function laporan_penjualan_customer($bulan = "", $tahun = "", $idbarang = "")
	// {

	// 	$data['seller']	  		= $this->input->post('cSupplier');
	// 	$data['mbulan']	  		= $this->input->post('cBulan');
	// 	$data['tahun']	  		= $this->input->post('cTahun');

	// $data['row']			= $this->model->code("SELECT * FROM penjualan_seller WHERE MONTH(tgl_jual) = '" . $this->input->post('cBulan') . "' 
	//  							  AND YEAR(tgl_jual) = '" . $this->input->post('cTahun') . "' AND kode_seller = '" . $this->input->post('cSupplier') . "' ORDER BY tgl_jual ASC");

	// 	$html					= $this->load->view('back-end/penjualan/cetak/laporan-penjualan-seller', $data, true);

	// 	//this the the PDF filename that user will get to download
	// 	$pdfFilePath 		= "laporan-penjualan-harian.pdf";

	// 	//load mPDF library
	// 	$this->m_pdf->pdf->mPDF('', 'A4-L', '10', 'ctimesbi', '5', '5', '5', '5', '5', '10', 'P');
	// 	$this->m_pdf->pdf->useOddEven = true;

	// 	//generate the PDF from the given html
	// 	$this->m_pdf->pdf->WriteHTML($html);


	// 	//download it.
	// 	$this->m_pdf->pdf->Output($pdfFilePath, 'I');
	// }

	public function laporan_penjualan_produk($bulan = "", $tahun = "", $idbarang = "")
	{
		$data['seller'] 	= $this->input->post('cSupplier');
		$data['mbulan']		= $this->input->post('cBulan');
		$data['tahun']		= $this->input->post('cTahun');
		if ($this->input->post('cSupplier') == 'ALL') {
			$data['row']			= $this->model->code("SELECT * FROM v_detail_jual WHERE MONTH(tgl_jual) = '" . $this->input->post('cBulan') . "' 
	     							  AND YEAR(tgl_jual) = '" . $this->input->post('cTahun') . "' ORDER BY id_barang ASC");
		} else {
			$data['row']			= $this->model->code("SELECT * FROM v_detail_jual WHERE MONTH(tgl_jual) = '" . $this->input->post('cBulan') . "' 
	     							  AND YEAR(tgl_jual) = '" . $this->input->post('cTahun') . "' AND id_barang = '" . $this->input->post('cSupplier') . "' ORDER BY tgl_jual ASC");
		}

		$this->load->view('back-end/penjualan/cetak/laporan-penjualan-produk', $data);
	}

	// public function laporan_penjualan_produk($bulan = "", $tahun = "", $idbarang = "")
	// {

	// 	$data['seller']	  		= $this->input->post('cSupplier');
	// 	$data['mbulan']	  		= $this->input->post('cBulan');
	// 	$data['tahun']	  		= $this->input->post('cTahun');
	// if ($this->input->post('cSupplier') == 'ALL') {
	// 	$data['row']			= $this->model->code("SELECT * FROM v_detail_jual WHERE MONTH(tgl_jual) = '" . $this->input->post('cBulan') . "' 
	//  							  AND YEAR(tgl_jual) = '" . $this->input->post('cTahun') . "' ORDER BY id_barang ASC");
	// } else {
	// 	$data['row']			= $this->model->code("SELECT * FROM v_detail_jual WHERE MONTH(tgl_jual) = '" . $this->input->post('cBulan') . "' 
	//  							  AND YEAR(tgl_jual) = '" . $this->input->post('cTahun') . "' AND id_barang = '" . $this->input->post('cSupplier') . "' ORDER BY tgl_jual ASC");
	// }

	// 	$html					= $this->load->view('back-end/penjualan/cetak/laporan-penjualan-produk', $data, true);

	// 	//this the the PDF filename that user will get to download
	// 	$pdfFilePath 		= "laporan-penjualan-harian.pdf";

	// 	//load mPDF library
	// 	$this->m_pdf->pdf->mPDF('', 'A4-L', '10', 'ctimesbi', '5', '5', '5', '5', '5', '10', 'P');
	// 	$this->m_pdf->pdf->useOddEven = true;

	// 	//generate the PDF from the given html
	// 	$this->m_pdf->pdf->WriteHTML($html);


	// 	//download it.
	// 	$this->m_pdf->pdf->Output($pdfFilePath, 'I');
	// }
}