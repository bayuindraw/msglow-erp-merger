<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_Act extends CI_Controller {

	public function __construct(){
		
        parent::__construct();
        ob_start();
		//error_reporting(0);     
	    //MenLoad model yang berada di Folder Model dan namany model
        $this->load->model('model');
        // Meload Library session 
        $this->load->library('session');
        //Meload database
        $this->load->database();
        //Meload url 
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download'); 
		 
    }
		public  function Date2String($dTgl){
			//return 2012-11-22
			list($cDate,$cMount,$cYear)	= explode("-",$dTgl) ;
			if(strlen($cDate) == 2){
				$dTgl	= $cYear . "-" . $cMount . "-" . $cDate ;
			}
			return $dTgl ; 
		}
		
		public  function String2Date($dTgl){
			//return 22-11-2012  
			list($cYear,$cMount,$cDate)	= explode("-",$dTgl) ;
			if(strlen($cYear) == 4){
				$dTgl	= $cDate . "-" . $cMount . "-" . $cYear ;
			} 
			return $dTgl ; 	
		}
		
		public function TimeStamp() {
			date_default_timezone_set("Asia/Jakarta");
			$Data = date("H:i:s");
			return $Data ;
			}
			
		public function DateStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("Y-m-d");
			return $Data ;
			}  
			
		public function DateTimeStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("Y-m-d h:i:s");
			return $Data ;
		}

		public function add_jual_produk($Type="",$id=""){

				$data = array (
				'tgl_jual' 			=> $this->Date2String($this->input->post('cTanggal')),
				'kode_jual' 		=> $this->input->post('cKodeJual'),
				'id_barang'			=> $this->input->post('NamaBarang'),
				'jumlah'			=> $this->input->post('JumlahPo'),
				'harga_satuan'		=> $this->input->post('cHarga'),
				'diskon'			=> $this->input->post('cDiskon'),
				'harga_jadi'		=> $this->input->post('cHargaJadi'),
				'total'				=> $this->input->post('cTotal'),
				
				);
					
				$this->session->set_userdata('seller',$this->input->post('Supplier'));
				$this->session->set_userdata('tanggal',$this->input->post('cTanggal'));
				$this->session->set_userdata('kodepo',$this->input->post('cKodeJual'));

				$this->model->Insert('pejualan_detail_seller',$data); 
				redirect(site_url('Administrator/Penjualan/penjualan_po'));
		}

		public function add_jual_produk_paket($Type="",$id=""){

				$this->session->set_userdata('pilihproduk',$this->input->post('NamaBarang'));	
				$this->session->set_userdata('seller',$this->input->post('Supplier'));
				$this->session->set_userdata('tanggal',$this->input->post('cTanggal'));
				$this->session->set_userdata('kodepo',$this->input->post('cKodeJual'));
				$this->session->set_userdata('jumlah',$this->input->post('JumlahPo'));
				$this->session->set_userdata('totalbayar',$this->input->post('cTotal'));

				
				redirect(site_url('Administrator/Penjualan/penjualan_po_wajah/detail'));
		}

		public function tambah_produk($id=""){

				$query = $this->model->code("SELECT * FROM v_detail_harga_produk WHERE id_ms_harga = '".$this->session->userdata('pilihproduk')."' 
						 AND id_produk = '".$this->input->post('cProduk')."'");
				foreach ($query as $key => $vaData) {
					$hargaSatuan = $vaData['harga'];
				}

				$data = array (
				'tgl_jual' 			=> $this->Date2String($this->session->userdata('tanggal')),
				'kode_jual' 		=> $this->session->userdata('kodepo'),
				'id_barang'			=> $this->input->post('cProduk'),
				'jumlah'			=> $this->input->post('cJumlahBrg'),
				'harga_satuan'		=> $hargaSatuan,
				'diskon'			=> '0',
				'harga_jadi'		=> $hargaSatuan,
				'total'				=> $hargaSatuan*$this->input->post('cJumlah'),
				
				);

				$this->model->Insert('pejualan_detail_seller',$data);
				redirect(site_url('Administrator/Penjualan/penjualan_po_wajah/detail'));
				
		}

		public function add_penjualan_agen($Type="",$id=""){

				$query = $this->model->code("SELECT sum(total) as jumlah FROM pejualan_detail_seller WHERE kode_jual = '".$this->session->userdata('kodepo')."'");
				foreach ($query as $key => $vaHarga) {
					$harga = $vaHarga['jumlah'];
				}

				$querySeller = $this->model->code("SELECT kode FROM member WHERE id_member = '".$this->session->userdata('seller')."'");
				foreach ($querySeller as $key => $vaSeller) {
					$kodeSeller = $vaSeller['kode'];
				}

				$data = array (
				'kode_penjualan' 	=> $this->session->userdata('kodepo'),
				'tgl_jual' 			=> $this->Date2String($this->session->userdata('tanggal')),
				'id_seller'			=> $this->session->userdata('seller'),
				'kode_seller'		=> $kodeSeller,
				'created_by'		=> $this->session->userdata('user'),
				'created_date'		=> $this->DateTimeStamp(),
				'total_bayar'		=> $harga,
				'bulan'				=> date('m'),
				);


				$this->model->Insert('penjualan_seller',$data); 
				/*unset($_SESSION['tanggal_po']);
				unset($_SESSION['supplier']);
				unset($_SESSION['kodepo']);*/

				redirect(site_url('Administrator/Penjualan/penjualan_po/I'));
			
		}

		public function penjualan_konfirmasi($id=""){

				$data = array (
				'approved_by' 			=> $this->session->userdata('user'),
				'approved_date' 		=> $this->DateTimeStamp(),
				);
			
				$this->model->Update('penjualan_seller','id_penjualan',$id,$data); 
			
		}

		public function kemasan_konfirmasi($id=""){

				$query = $this->model->code("SELECT * FROM penjualan_detail_kemas WHERE id_pengiriman = '".$id."'");
				foreach ($query as $key => $vaData) {
					$kodepenjualan = $vaData['kode_jual'];
				}

				$data = array (
				'approved_by' 			=> $this->session->userdata('user'),
				'approved_date' 		=> $this->DateTimeStamp(),
				);
			
				$this->model->Update('penjualan_detail_kemas','kode_jual',$kodepenjualan,$data); 
			
		}

		public function packing_produk($Type="",$id=""){

				$data = array (
				'tanggal' 			=> $this->Date2String($this->input->post('TanggalOrder')),
				'id_produk'			=> $this->input->post('NamaBarang'),
				'jumlah'			=> $this->input->post('JumlahBayar'),
				'created_by'		=> $this->session->userdata('user'),
				'created_date'		=> $this->DateTimeStamp(),
				'kode_jual'			=> $this->input->post('kodejual'),
				
				);
					
				$this->model->Insert('penjualan_detail_kemas',$data); 
				redirect(site_url('Administrator/Penjualan/kemas_produk/'.$this->input->post('kodejual').''));
		}

		public function terima_uang($Type="",$id=""){

				$data = array (
				'tanggal' 			=> $this->Date2String($this->input->post('TanggalOrder')),
				'id_produk'			=> $this->input->post('NamaBarang'),
				'jumlah'			=> $this->input->post('JumlahBayar'),
				'created_by'		=> $this->session->userdata('user'),
				'created_date'		=> $this->DateTimeStamp(),
				'kode_jual'			=> $this->input->post('kodejual'),
				
				);
					
				$this->model->Insert('penjualan_detail_bayar',$data); 
				redirect(site_url('Administrator/Penjualan/terima_bayar_penjualan/'.$this->input->post('kodejual').''));
		}


	}