<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Style\Border;

defined('BASEPATH') or exit('No direct script access allowed');

class Stock_Produk extends CI_Controller
{

	public function export_stock_monthly2()
	{
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"test.xls\"");
		//$data = array('depan', 'satu');
		//echo implode("\t", $data) . "\n";
		ob_end_clean();
	}

	public function export_stock_monthly($bulan, $tahun)
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);



		//$sheet->getStyle('C4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f8cbad');
		$arrdata = $this->model->Query("SELECT *, C.jumlah, C.rusak, A.nama_produk, A.klasifikasi FROM produk A LEFT JOIN tb_stock_produk_history C ON C.id_barang = A.id_produk AND C.tanggal = '" . $tahun . "-" . $bulan . "-01' LEFT JOIN m_product_type D ON D.product_type_name = A.klasifikasi ORDER BY D.product_type_order");
		//$arrcategoryproduct = $this->model->Query("SELECT * FROM m_category_product");
		$arrterima = $this->model->Query("SELECT SUM(jumlah) as jumlah, SUM(rusak) as rusak, tgl_terima, id_barang FROM terima_produk  GROUP BY id_barang, tgl_terima");
		foreach ($arrterima as $keyterima => $valueterima) {
			$dataterima[$valueterima['id_barang']][$valueterima['tgl_terima']] = $valueterima;
		}
		$arrkeluar = $this->model->Query("SELECT SUM(B.package_detail_quantity) as jumlah, A.package_date AS tanggal, B.product_id AS id_barang FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id GROUP BY B.product_id, A.package_date");
		foreach ($arrkeluar as $keykeluar => $valuekeluar) {
			$datakeluar[$valuekeluar['id_barang']][$valuekeluar['tanggal']] = $valuekeluar;
		}
		$abjad = 'B';
		$awal = 5;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');
		$sheet->setCellValue($abjad++ . $awal, 'KLASIFIKASI');
		$sheet->setCellValue($abjad++ . $awal, 'STOK AWAL');
		$sheet->setCellValue($abjad++ . $awal, 'STOK BARANG RUSAK AWAL');

		$nawal = $awal;

		$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		for ($i = 1; $i <= $nJumlahHari; $i++) {
			if ($i > 9) {
				$cNol = "";
			} else {
				$cNol = "0";
			}
			$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
			$no = 0;
			$titik_awal = $abjad;

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG DATANG');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG KELUAR');
			$sheet->getStyle($titik_awal . $awal . ':' . $abjad . $awal)->applyFromArray($styleArray);
			$sheet->mergeCells($titik_awal . $awal . ':' . $abjad . $awal);
			$sheet->setCellValue($titik_awal . $awal, substr($arrbln[$bulan], 0, 3) . '-' . (int)($cNol . $i));

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG RUSAK');
		}

		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG DATANG');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG KELUAR');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad . $awal, 'SISA STOCK KEMASAN BAGUS');

		$sheet->mergeCells($abjad_awal . ($awal - 3) . ':' . $abjad . ($awal - 3));
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$sheet->setCellValue($abjad_awal . ($awal - 3), 'LAPORAN BARANG JADI MS GLOW');
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setSize(20);
		$sheet->mergeCells($abjad_awal . ($awal - 2) . ':' . $abjad . ($awal - 2));
		$sheet->setCellValue($abjad_awal . ($awal - 2), 'PERIODE ' . strtoupper($arrbln[$bulan]));
		$sheet->setCellValue($abjad . ($awal - 1), date('d/m/Y H.i'));


		//$sheet->getStyle('A1:B2')->applyFromArray($styleArray);
		//$sheet ->getStyle($abjad_awal.$awal.':'.$abjad.($awal+1))->applyFromArray($styleArray);
		$sheet->freezePane('F7');
		$sheet->getSheetView()->setZoomScale(70);
		$sheet->getRowDimension($awal + 1)->setRowHeight(30);
		$irow = 1;
		$wrap_awal = "";
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal + 1))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = $wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}
		$sheet->getStyle($wrap_awal . '5:' . $wrap_akhir . '6')->getAlignment()->setWrapText(true);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$nawal++;
		$produk_global = "";
		$produk_merge_akhir = "";

		foreach ($arrdata as $key => $value) {
			$nabjad = 'B';
			$paling_awal = $nabjad;
			$nawal++;
			if ($produk_merge_awal == "") {
				$produk_merge_awal = $nawal;
			}
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk']);
			$xx = 1;
			if ($rgb == "") $rgb = 'e9f7df';
			if ($produk_global != $value['klasifikasi']) {
				$xx = 0;
				$xabjad = $nabjad;
				if ($produk_merge_akhir != "") {

					$sheet->mergeCells($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_akhir);
					$sheet->getStyle($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_awal)->applyFromArray($style);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
					//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
				}
				$produk_merge_awal = $nawal;
			}
			$produk_global = $value['klasifikasi'];
			$sheet->setCellValue($nabjad++ . $nawal, $value['klasifikasi']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['jumlah']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['rusak']);
			$jumlah = $value['jumlah'];
			$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$jmldatang = 0;
			$jmlkeluar = 0;
			for ($i = 1; $i <= $nJumlahHari; $i++) {
				if ($i > 9) {
					$cNol = "";
				} else {
					$cNol = "0";
				}
				$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
				$no = 0;

				$sheet->setCellValue($nabjad++ . $nawal, $dataterima[$value['id_produk']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, $datakeluar[$value['id_produk']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, $dataterima[$value['id_produk']][$date]['rusak']);
				$jmldatang = $jmldatang + (($dataterima[$value['id_produk']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_produk']][$date]['jumlah']);
				$jmlkeluar = $jmlkeluar + (($datakeluar[$value['id_produk']][$date]['jumlah'] == "") ? 0 : $datakeluar[$value['id_produk']][$date]['jumlah']);
				//$jumlah = $jumlah + (($dataterima[$value['id_produk']][$date]['jumlah'] == "")?0:$dataterima[$value['id_produk']][$date]['jumlah']) - (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']); 
			}
			$jumlah = $jumlah + $jmldatang - $jmlkeluar;
			$sheet->setCellValue($nabjad++ . $nawal, $jmldatang);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlkeluar);
			$sheet->setCellValue($nabjad . $nawal, $jumlah);
			$produk_merge_akhir = $nawal;
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
			$rgb = $value['category_product_rgb'];
			if ($rgb == "") $rgb = 'e9f7df';
			if ($jumlah < 20000) {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
			} else {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
			}
		}
		if ($xx == 1) {
			$sheet->mergeCells($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_akhir);
			$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray2);
			$sheet->getStyle($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_awal)->applyFromArray($style);
			$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
			//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
		}

		$nawal++;
		$nawal++;
		$sheet->setCellValue($abjad_awal . $nawal++, 'NB:');
		$abjad_awal2 = $abjad_awal;
		$sheet->getStyle($abjad_awal2 . $nawal . ':' . $abjad_awal2++ . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
		$sheet->setCellValue($abjad_awal2 . $nawal++, 'STOK DIBAWAH 20.000 PCS');
		/*foreach($arrcategoryproduct as $keyproduct => $valueproduct){
			$abjad_awal2 = $abjad_awal;
			$sheet->getStyle($abjad_awal2.$nawal.':'.$abjad_awal2++.$nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($valueproduct['category_product_rgb']);
			$sheet->setCellValue($abjad_awal2.$nawal++, $valueproduct['category_product_name']);
		}*/

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"STOCK BARANG JADI MSGLOW " . strtoupper($arrbln[$bulan]) . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function __construct()
	{

		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('model');
		$this->load->model('relasi');
		$this->load->library('session');
		$this->load->database();
		$this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}
	public  function Date2String($dTgl)
	{
		//return 2012-11-22
		list($cDate, $cMount, $cYear)	= explode("-", $dTgl);
		if (strlen($cDate) == 2) {
			$dTgl	= $cYear . "-" . $cMount . "-" . $cDate;
		}
		return $dTgl;
	}

	public  function String2Date($dTgl)
	{
		//return 22-11-2012  
		list($cYear, $cMount, $cDate)	= explode("-", $dTgl);
		if (strlen($cYear) == 4) {
			$dTgl	= $cDate . "-" . $cMount . "-" . $cYear;
		}
		return $dTgl;
	}

	public function TimeStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("H:i:s");
		return $Data;
	}

	public function DateStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("d-m-Y");
		return $Data;
	}

	public function DateTimeStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("Y-m-d h:i:s");
		return $Data;
	}

	public function signin($Action = "")
	{
		$data = "";
		if ($Action == "error") {
			$data['notif'] = "Username / Password Salah";
		}
		$msg 			 = 'My secret message';
		$this->load->view('back-end/login-secure', $data);
	}

	public function signinsecure($Action = "")
	{
		$data = "";
		if ($Action == "error") {
			$data['notif'] = "Username / Password Salah";
		}
		$msg 			 = 'My secret message';
		$this->load->view('back-end/login', $data);
	}

	public function stock_opname_produk($Aksi = "")
	{
		$dataHeader['title']		= "Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Stock Barang Jadi';
		$dataHeader['link']			= 'stock_opname_kemasan';
		$data['action'] 			= $Aksi;

		//$data['row']				= $this->db->query("SELECT * FROM  v_stock_produk where status = 1 ORDER BY id_barang = 99 DESC, id_barang = 100 DESC, id_barang = 202 DESC, id_barang ASC")->result_array();
		$data['row']				= $this->db->query("SELECT * FROM  v_stock_produk where status = 1 AND warehouse_id = '".$_SESSION['warehouse_id']."' ORDER BY id_barang = 99 DESC, id_barang = 100 DESC, id_barang = 202 DESC, id_barang ASC")->result_array();

		$dataHeader['content'] = $this->load->view('back-end/stock/produk-list-stock', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function stock_opname_produk_min_satu($Aksi = "")
	{
		$dataHeader['title']		= "Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Managerial Report';
		$dataHeader['link']			= 'stock_opname_kemasan';
		$data['action'] 			= $Aksi;

		$data['row']				= $this->db->query("SELECT * FROM  v_stock_produk where status = 1 AND warehouse_id = '".$_SESSION['warehouse_id']."' ORDER BY id_barang = 99 DESC, id_barang = 100 DESC, id_barang = 202 DESC, id_barang ASC")->result_array();

		$dataHeader['content'] = $this->load->view('back-end/stock/produk-list-stock-version', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function delivery_order_fullfill($Aksi = "")
	{
		$dataHeader['title']		= "Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'TOTAL DELIVERY PRODUCT';
		$dataHeader['link']			= 'stock_opname_kemasan';
		$data['action'] 			= $Aksi;

		$data['row']				= $this->db->query("SELECT *, sum(send_quantity) as total  FROM mv_product_daily_send GROUP by sku_name")->result_array();

		$dataHeader['content'] = $this->load->view('back-end/stock/delivery_order_full_fill', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function delivery_order_fullfill_tingkat($Aksi = "")
	{
		$dataHeader['title']		= "Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'TOTAL DELIVERY PRODUCT';
		$dataHeader['link']			= 'stock_opname_kemasan';
		$data['action'] 			= $Aksi;

		$data['row']				= $this->db->query("SELECT * FROM mv_delivery_detail WHERE product_id = '".$Aksi."'")->result_array();

		$dataHeader['content'] = $this->load->view('back-end/stock/delivery_order_full_fill_tingkat', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function delivery_order_fullfill_tingkat_akhir($Aksi = "")
	{
		$dataHeader['title']		= "Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'TOTAL DELIVERY PRODUCT';
		$dataHeader['link']			= 'stock_opname_kemasan';
		$data['action'] 			= $Aksi;

		$data['row']				= $this->db->query("SELECT A.*,B.nama AS seller_name , A.quantity as send_quantity FROM mv_delivery_bridge_detail A LEFT JOIN member B ON A.seller_code = B.kode WHERE sales_id = '".$Aksi."'")->result_array();

		$dataHeader['content'] = $this->load->view('back-end/stock/delivery_order_full_fill_tingkat_so', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function delivery_order_fullfill_tingkat_akhir_code($Aksi = "")
	{
		$dataHeader['title']		= "Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'TOTAL DELIVERY PRODUCT';
		$dataHeader['link']			= 'stock_opname_kemasan';
		$data['action'] 			= $Aksi;

		$data['row']				= $this->db->query("SELECT A.*,B.nama AS seller_name , A.quantity as send_quantity FROM mv_delivery_bridge_detail A LEFT JOIN member B ON A.seller_code = B.kode WHERE seller_code  = '".$Aksi."'")->result_array();

		$dataHeader['content'] = $this->load->view('back-end/stock/delivery_order_full_fill_tingkat_so', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function kodePOproduk()
	{
		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWPOFACTORY-";

		$dbDate	=	$this->db->query("SELECT COUNT(id_po_produk) as JumlahTransaksi FROM po_produk_draft WHERE bulan = '" . date('m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 1;
			}
		} else {
			$nJumlahTransaksi = 1;
		}

		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}


		return $cKode;
	}

	public function list_po_draft($id = "")
	{
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'po_produk';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi;
		$data['kodepo']				= $id;
		$data['row']				= $this->model->ViewWhere('po_produk_draft A LEFT JOIN v_detail_po_produk_draft B ON B.kode_pb = A.kode_po', 'A.id_po_produk', $data['kodepo']);
		$dataHeader['content'] = $this->load->view('back-end/stock/produk-detail-po-draft', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function po_produk($Aksi = "")
	{

		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'PO Barang Jadi';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi;
		$data['kodepo']				= $this->kodePOproduk();
		$data['row']				= $this->model->ViewWhere('v_detail_po_produk_draft', 'kode_pb', $this->kodePOproduk());


		$dataHeader['content'] = $this->load->view('back-end/stock/produk-input-po', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function po_produk_trial($Aksi = "")
	{

		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'PO Barang Jadi';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi;
		$data['kodepo']				= $this->kodePOproduk();
		$data['row']				= $this->model->ViewWhere('v_detail_po_produk_draft', 'kode_pb', $this->kodePOproduk());


		$dataHeader['content'] = $this->load->view('back-end/stock/produk-input-po-trial', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}


	public function po_produk_active($Aksi = "")
	{

		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Index';
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi;

		$data['row']				= $this->model->ViewAsc('v_po_produk', 'id_po_produk');


		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/stock/produk-list-active', $data);
		$this->load->view('back-end/container/footer');
	}

	public function po_produk_active_pic($Aksi = "")
	{

		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'PO Produk Active';
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi;

		$data['row']				= $this->model->ViewWhere('v_po_produk', 'active', 'Y');


		$dataHeader['content'] = $this->load->view('back-end/stock/produk-list-active-pic', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function kartu_stock_produk($Aksi = "")
	{

		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Kartu Stock Produk';
		$dataHeader['link']			= 'kartu_stock_kemasan';
		$data['action'] 			= $Aksi;

		if (empty($Aksi)) {
			$data['bulan']			= "";
			$data['tahun']			= "";
			$data['row']			= $this->model->code("SELECT * FROM v_stock_kemasan WHERE tanggal = '2017-01-01' ");
		} else {
			$data['bulan']			= $this->input->post('cBulan');
			$data['tahun']			= $this->input->post('cTahun');
			$data['row']			= $this->model->ViewAsc('v_stock_kemasan', 'id_stock');
		}


		$dataHeader['content'] = $this->load->view('back-end/stock/kartu-stock-produk', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function tampil_stock_produk()
	{
		$data['bulan']			= $this->input->post('bulan');
		$data['tahun']			= $this->input->post('tahun');
		$data['row']			= $this->model->code("SELECT * FROM v_stock_produk where status = 1 AND warehouse_id = '".$_SESSION['warehouse_id']."'");
		$this->load->view('back-end/stock/table/show_kartu_stock_produk', $data);
	}

	public function tampil_stock_produk_real($idBarang = "", $Bulan = "", $Tahun = "")
	{
		$data['idbarang']		= $idBarang;
		$data['bulan']			= $Bulan;
		$data['tahun']			= $Tahun;

		$this->load->view('back-end/stock/table/show_kartu_stock_detail-produk', $data);
	}

	public function cetaksuratjalankemasan($id = "")
	{

		$data['kode']				= 	$id;
		$data['row']				= $this->model->ViewWhere('v_datakluar_kemasan', 'id_kluar_kemasan', $id);
		//load the view and saved it into $html variable
		$html		=	$this->load->view('back-end/stock/cetak/sjkemasan', $data, true);

		//this the the PDF filename that user will get to download
		$pdfFilePath = "surat-jalan-kemasan.pdf";

		//load mPDF library
		$this->m_pdf->pdf->mPDF('', 'A5-L', '10', 'ctimesbi', '5', '5', '5', '5', '5', '10', 'L');
		$this->m_pdf->pdf->useOddEven = true;

		//generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);


		//download it.
		$this->m_pdf->pdf->Output($pdfFilePath, 'I');
	}

	public function laporan_stock_satuan($bulan = "", $tahun = "", $idbarang = "")
	{

		$data['bulan']		= 	$bulan;
		$data['tahun']		=	$tahun;
		$data['barang']		=   $idbarang;

		//$data['row']		= $this->model->ViewWhere('v_datakluar_kemasan','kode_kluar',$id);
		//load the view and saved it into $html variable

		$html		=	$this->load->view('back-end/stock/cetak/cetak-laporan-kartu-stock', $data, true);

		//this the the PDF filename that user will get to download
		$pdfFilePath = "surat-jalan-kemasan.pdf";

		//load mPDF library
		$this->m_pdf->pdf->mPDF('', 'A4', '10', 'ctimesbi', '5', '5', '5', '5', '5', '10', 'P');
		$this->m_pdf->pdf->useOddEven = true;

		//generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);


		//download it.
		$this->m_pdf->pdf->Output($pdfFilePath, 'I');
	}

	public function terima_produk($Aksi = "")
	{

		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Barang Datang';
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi;

		$data['po']					= $this->model->ViewWhere('v_po_produk', 'kode_po', $Aksi);
		$data['row']				= $this->model->ViewWhere('v_detail_po_produk', 'kode_pb', $Aksi);
		//$data['row']				= $this->db->query("SELECT nama_produk,	harga,	id_pokemasan,	kode_pb	user,	id_barang,	SUM(jumlah) as jumlah,	tanggal,	jam	harga_satuan,	total,	id_factory,	nama_factory,	alamat,	telepon FROM v_detail_po_produk WHERE kode_pb = '$Aksi' GROUP BY id_barang")->result_array();
		$data['bayar']				= $this->model->ViewWhere('v_terima_produk', 'kode_po', $Aksi);

		$arrquery2 = $this->model->code("SELECT SUM(jumlah) as total, id_barang FROM terima_produk WHERE kode_po = '" . $Aksi . "' AND LEFT(date_create, 10) = '" . date('Y-m-d') . "' GROUP BY id_barang");
		foreach ($arrquery2 as $index => $value) {
			$data['arr_insert_now'][$value['id_barang']] = $value['total'];
		}
		$query = $this->model->code("SELECT sum(jumlah) as totalproduk FROM  v_terima_produk WHERE kode_po = '" . $Aksi . "'");
		foreach ($query as $key => $vaTotal) {
			$totalp =  $vaTotal['totalproduk'];
		}

		$queryDua = $this->model->code("SELECT sum(jumlah) as totalproduk FROM  tb_detail_po_produk WHERE kode_pb = '" . $Aksi . "'");
		foreach ($queryDua as $key => $vaTotal) {
			$totalAll =  $vaTotal['totalproduk'];
		}

		if ($totalp >= $totalAll) {
			$datax = array(
				'active'    => 'T',
				'active_date_update' => date('Y-m-d')
			);
			$this->model->Update('po_produk', 'kode_po', $Aksi, $datax);
		}
		$queryTiga = $this->model->code("SELECT id_barang, tgl_terima, SUM(jumlah) as total, nama_produk FROM terima_produk A LEFT JOIN produk B ON B.id_produk  = A.id_barang WHERE kode_po = '" . $Aksi . "' GROUP BY tgl_terima, id_barang ORDER BY tgl_terima");
		foreach ($queryTiga as $key => $vaTiga) {
			$data['detail_terima'][$vaTiga['tgl_terima']][] = $vaTiga;
		}
		$dataHeader['content'] = $this->load->view('back-end/stock/produk-terima-produk', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function terima_produk2($Aksi = "")
	{
		//SELECT SUM(jumlah) FROM  tb_detail_po_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_pb WHERE B.active = 'T' AND B.active_date_update < '2020-09-26'
		//SELECT SUM(jumlah) FROM  v_terima_produk WHERE active = 'T' AND tgl_terima < '2020-09-26' AND active_date_update < '2020-09-26'
		//lama

		//SELECT SUM(jumlah) FROM tb_detail_po_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_pb WHERE A.tanggal BETWEEN '2020-09-26' AND '2020-09-29'
		//po bulan ini

		//SELECT SUM(jumlah) FROM v_terima_produk WHERE tgl_terima BETWEEN '2020-09-26' AND '2020-09-29'
		//realisasi bulan ini

		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Index';
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi;

		$data['po']					= $this->model->ViewWhere('v_po_produk', 'kode_po', $Aksi);
		$data['row']				= $this->model->ViewWhere('v_detail_po_produk', 'kode_pb', $Aksi);
		$data['bayar']				= $this->model->ViewWhere('v_terima_produk', 'kode_po', $Aksi);

		$arrquery2 = $this->model->code("SELECT SUM(jumlah) as total, id_barang FROM terima_produk WHERE kode_po = '" . $Aksi . "' AND LEFT(date_create, 10) = '" . date('Y-m-d') . "' GROUP BY id_barang");
		foreach ($arrquery2 as $index => $value) {
			$data['arr_insert_now'][$value['id_barang']] = $value['total'];
		}
		$query = $this->model->code("SELECT sum(jumlah) as totalproduk FROM  v_terima_produk WHERE kode_po = '" . $Aksi . "'");
		foreach ($query as $key => $vaTotal) {
			$totalp =  $vaTotal['totalproduk'];
		}

		$queryDua = $this->model->code("SELECT sum(jumlah) as totalproduk FROM  tb_detail_po_produk WHERE kode_pb = '" . $Aksi . "'");
		foreach ($queryDua as $key => $vaTotal) {
			$totalAll =  $vaTotal['totalproduk'];
		}

		if ($totalp == $totalAll) {
			$datax = array(
				'active'    => 'T',
				'active_date_update' => date('Y-m-d')
			);
			$this->model->Update('po_produk', 'kode_po', $Aksi, $datax);
		}
		$queryTiga = $this->model->code("SELECT id_barang, tgl_terima, SUM(jumlah) as total, nama_produk FROM terima_produk A LEFT JOIN produk B ON B.id_produk  = A.id_barang WHERE kode_po = '" . $Aksi . "' GROUP BY tgl_terima, id_barang ORDER BY tgl_terima");
		foreach ($queryTiga as $key => $vaTiga) {
			$data['detail_terima'][$vaTiga['tgl_terima']][] = $vaTiga;
		}
		$dataHeader['content'] = $this->load->view('back-end/stock/produk-terima-produk2', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function laporan_stock_satuan_produk($bulan = "", $tahun = "", $idbarang = "")
	{

		$data['bulan']		= 	$bulan;
		$data['tahun']		=	$tahun;
		$data['barang']		=   $idbarang;

		//$data['row']		= $this->model->ViewWhere('v_datakluar_kemasan','kode_kluar',$id);
		//load the view and saved it into $html variable

		$html		=	$this->load->view('back-end/stock/cetak/cetak-laporan-kartu-stock', $data, true);

		//this the the PDF filename that user will get to download
		$pdfFilePath = "surat-jalan-kemasan.pdf";

		//load mPDF library
		$this->m_pdf->pdf->mPDF('', 'A4', '10', 'ctimesbi', '5', '5', '5', '5', '5', '10', 'P');
		$this->m_pdf->pdf->useOddEven = true;

		//generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);


		//download it.
		$this->m_pdf->pdf->Output($pdfFilePath, 'I');
	}

	public function tampil_edit_stock($idterima = "")
	{
		$data['terima']		= $idterima;
		$data['row']		= $this->model->ViewWhere('v_detail_kemasan_po', 'id_terima_kemasan', $idterima);
		$this->load->view('back-end/stock/table/tampil-edit-kemasan', $data);
	}

	public function cetak_stock_kemasan_real()
	{


		$data['row']	= $this->model->ViewAsc('v_stock_kemasan_new', 'id_kemasan');

		$html			= $this->load->view('back-end/stock/cetak/kemasan-cetak-stock-actual', $data, true);

		$pdfFilePath 	= "cetak stock kemasan actual.pdf";

		//load mPDF library
		$this->m_pdf->pdf->mPDF('', 'A4', '10', 'ctimesbi', '5', '5', '5', '5', '5', '10', 'P');
		$this->m_pdf->pdf->useOddEven = true;

		//generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);


		//download it.
		$this->m_pdf->pdf->Output($pdfFilePath, 'I');
	}

	public function laporan_po_kemasan($Aksi = "")
	{

		$dataHeader['title']		= "Laporan Purchase Order Kemasan MSGLOW";
		$dataHeader['menu']   		= 'Laporan Po Kemasan';
		$dataHeader['file']   		= 'laporan_po';
		$dataHeader['link']			= 'laporan_po';
		$data['action'] 			= $Aksi;

		$data['po']					= $this->model->ViewWhere('v_po_produk', 'kode_po', $Aksi);
		$data['row']				= $this->model->ViewWhere('v_detail_po_produk', 'kode_pb', $Aksi);
		$data['bayar']				= $this->model->ViewWhere('v_terima_produk', 'kode_po', $Aksi);


		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/stock/kemasan/laporan-po-kemasan', $data);
		$this->load->view('back-end/container/footer');
	}

	function get_kemasan()
	{
		$category_id = $this->input->post('id', TRUE);
		$data = $this->model->get_sub_category($category_id)->result();
		echo json_encode($data);
	}

	function bersih_data()
	{
		$this->session->unset_userdata('tanggal_po');
		$this->session->unset_userdata('supplier');
		redirect(site_url('Administrator/Stock/po_kemasan/'));
	}

	public function tampil_edit_stock_jadi($idterima = "")
	{
		$data['terima']		= $idterima;
		$data['row']		= $this->model->ViewWhere('v_detail_terima_produk', 'id_terima_kemasan', $idterima);
		$this->load->view('back-end/stock/table/tampil-edit-produk', $data);
	}

	public function cetak_stock_barang_real()
	{
		$data['row']	= $this->model->ViewAsc('v_stock_produk', 'id_barang');

		$this->load->view('back-end/stock/cetak/barang-cetak-stock-actual', $data);
	}

	// public function cetak_stock_barang_real()
	// {


	// 	$data['row']	= $this->model->ViewAsc('v_stock_produk', 'id_barang');

	// 	$html			= $this->load->view('back-end/stock/cetak/barang-cetak-stock-actual', $data, true);

	// 	$pdfFilePath 	= "cetak stock kemasan actual.pdf";

	// 	//load mPDF library
	// 	$this->m_pdf->pdf->mPDF('', 'A4', '10', 'ctimesbi', '5', '5', '5', '5', '5', '10', 'P');
	// 	$this->m_pdf->pdf->useOddEven = true;

	// 	//generate the PDF from the given html
	// 	$this->m_pdf->pdf->WriteHTML($html);


	// 	//download it.
	// 	$this->m_pdf->pdf->Output($pdfFilePath, 'I');
	// }

	public function cetak_laporan_po_kemasan_detail()
	{

		$data['mbulan']	= $this->input->post('cBulan');

		$data['tahun']	= $this->input->post('cBulan');

		$data['row']	= $this->model->code("SELECT * FROM v_detail_po_kemas WHERE MONTH(tanggal) = '" . $this->input->post('cBulan') . "' AND YEAR(tanggal) = '" . $this->input->post('cTahun') . "' ");

		$html			= $this->load->view('back-end/stock/cetak/kemasan-laporan-po-detail', $data, true);

		$pdfFilePath 	= "cetak detail po kemasan periode.pdf";

		//load mPDF library
		$this->m_pdf->pdf->mPDF('', 'A4', '10', 'ctimesbi', '5', '5', '5', '5', '5', '10', 'P');
		$this->m_pdf->pdf->useOddEven = true;

		//generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);


		//download it.
		$this->m_pdf->pdf->Output($pdfFilePath, 'I');
	}

	public function cetak_laporan_kluar_kemasan()
	{

		$data['mbulan']	= $this->input->post('cBulan');

		$data['tahun']	= $this->input->post('cBulan');

		$data['row']	= $this->model->code("SELECT * FROM v_kluar_kemasan WHERE MONTH(tanggal) = '" . $this->input->post('cBulan') . "' AND YEAR(tanggal) = '" . $this->input->post('cTahun') . "' ");

		$html			= $this->load->view('back-end/stock/cetak/kemasan-laporan-kemasan-kluar-detail', $data, true);

		$pdfFilePath 	= "cetak detail po kemasan periode.pdf";

		//load mPDF library
		$this->m_pdf->pdf->mPDF('', 'A4', '10', 'ctimesbi', '5', '5', '5', '5', '5', '10', 'P');
		$this->m_pdf->pdf->useOddEven = true;

		//generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);


		//download it.
		$this->m_pdf->pdf->Output($pdfFilePath, 'I');
	}

	public function detail_po_active_produk($Aksi = "")
	{

		$dataHeader['title']		= "Purchase Order Active Barang Jadi - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Index';
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi;

		$data['row']				= $this->model->ViewWhere('v_detail_po_produk', 'kode_pb', $Aksi);
		$data['bayar']				= $this->model->ViewWhere('bayar_produk', 'kode_po', $Aksi);
		$data['terima']				= $this->model->ViewWhere('v_terima_produk', 'kode_po', $Aksi);


		$this->load->view('back-end/container/header', $dataHeader);
		$this->load->view('back-end/stock/produk-detail-po-active', $data);
		$this->load->view('back-end/container/footer');
	}
}
