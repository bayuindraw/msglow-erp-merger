<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	// Syarat  :  
	
	// 1 . Select  = View 
	// 2 . Insert  = Ins
	// 3 . Update  = Updt
	// 4 . Delete  = Del

class Model extends CI_Model {

	
	
	////// MASTER //////
	
	public function Login($user,$pass) {
		$Query = $this->db->query("SELECT * FROM username WHERE username = '$user' AND password = '$pass' ");
		return $Query;	
	}
	public function LoginScr($user,$pass) {
		$Query = $this->db->query("SELECT * FROM v_login WHERE username = '$user' AND password = '$pass' ");
		return $Query;	
	}
	public function Code($Query) {
		$Query = $this->db->query("  ".$Query."  ");
		return $Query->result_array();	
	}
	public function update_send($jml, $tanggal, $id_barang) {
		/*add_edit_pengeluaran_detail_kemasan 
		$query =  $this->model->ViewWhere('tb_stock_kemasan','id_barang',$this->input->post('NamaBarang'));
						foreach ($query as $key => $vaData) {
							$tot = $vaData['jumlah'];
						}

						$dataUpdate = array (
							'jumlah'		=> $tot - $this->input->post('JumlahPo')
							);

						$this->model->update('tb_stock_kemasan','id_barang',$this->input->post('NamaBarang'),$dataUpdate);*/
		
		
		//$this->model->update_send($this->input->post('JumlahPo'), $this->Date2String($this->input->post('TanggalOrder')), $this->input->post('NamaBarang')); ediit
		
		//add_kluar_kemas
		//$this->model->update_send($vaCek['jumlah'], $this->Date2String($this->session->userdata('tanggal_po')),$vaCek['id_barang']);
		$Query = $this->db->query("UPDATE tb_stock_kemasan_history SET jumlah = (jumlah - ".$jml.") WHERE id_barang = '".$id_barang."' AND tanggal > '".$tanggal."'");
		return $Query;	
	}
	public function update_keluar($jml, $tanggal, $id_barang) {
		$Query = $this->db->query("UPDATE tb_stock_kemasan SET jumlah = (jumlah - ".$jml.") WHERE id_barang = '".$id_barang."'");
		$Query = $this->db->query("UPDATE tb_stock_kemasan_history SET jumlah = (jumlah - ".$jml.") WHERE id_barang = '".$id_barang."' AND tanggal > '".$tanggal."'");
		return $Query;	
	}
	public function update_stock($jml, $tanggal, $id_barang) {
		$Query = $this->db->query("UPDATE tb_stock_kemasan_history SET jumlah = (jumlah + ".$jml.") WHERE id_barang = '".$id_barang."' AND tanggal > '".$tanggal."'");
		return $Query;	
	}
	public function update_rusak($jml, $tanggal, $id_barang, $rusakawal = 0) {
		$Query = $this->db->query("UPDATE tb_stock_kemasan_history SET rusak = (rusak + ".($jml - $rusakawal).") WHERE id_barang = '".$id_barang."' AND tanggal > '".$tanggal."'");
		return $Query;	
	}
	public function Code_row($Query) {
		$Query = $this->db->query("  ".$Query."  ");
		return $Query->row_array();	
	}
	public function Codes($Query) {
		$Query = $this->db->query("  ".$Query."  ");
		return $Query;	
	}
	public function LastId($kolom,$table) {
		$Query = $this->db->query("SELECT MAX($kolom) AS LastIdFix FROM  $table");
		return $Query->result_array();	
	}
	public function Query($Query) { 
		$Query = $this->db->query($Query);
		return $Query->result_array();	
	}
	public function View($Table,$Order) {
		$Query = $this->db->query("SELECT * FROM ".$Table." ORDER BY ".$Order." DESC");
		return $Query->result_array();	
	}
	public function ViewGroupBy($Table,$Order) {
		$Query = $this->db->query("SELECT * FROM ".$Table." GROUP BY ".$Order." DESC");
		return $Query->result_array();	
	}
	public function ViewASC($Table,$Order) {
		$Query = $this->db->query("SELECT * FROM ".$Table." ORDER BY ".$Order." ASC");
		return $Query->result_array();	
	}

	//Bayu: nambahin where
	public function ViewWhereASC($Table,$Where,$Order) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$Where." ORDER BY ".$Order." ASC");
		return $Query->result_array();	
	}

	public function ViewLimit($Table,$Order,$Limit) {
		$Query = $this->db->query("SELECT * FROM ".$Table." ORDER BY ".$Order." DESC LIMIT 0,$Limit");
		return $Query->result_array();	
	}
	public function ViewLimitGroup($Table,$Group,$Limit) {
		$Query = $this->db->query("SELECT * FROM ".$Table." GROUP BY ".$Group." DESC LIMIT 0,$Limit");
		return $Query->result_array();	
	}
	public function ViewLimitWhere($Table,$Order,$Limit,$WhereField,$WhereValue) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' ORDER BY ".$Order." DESC LIMIT 0,$Limit");
		return $Query->result_array();	
	}
	public function ViewWhere($Table,$WhereField,$WhereValue) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' ");
		return $Query->result_array();	
	}
	public function ViewWherePnp($Table,$WhereField,$WhereValue) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' AND pindah = '0' AND id_kota = '".$this->session->userdata('kota')."' ORDER BY jam ASC");
		return $Query->result_array();	
	}
	public function ViewWhereAnd($Table,$WhereField,$WhereValue,$WhereField2,$WhereValue2) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' AND ".$WhereField2." = '".$WhereValue2."'");
		return $Query->result_array();	
	}
	public function ViewWhereAndPNP($Table,$WhereField,$WhereValue,$WhereField2,$WhereValue2) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' AND ".$WhereField2." = '".$WhereValue2."' ORDER BY tanggal DESC");
		return $Query->result_array();	
	}
	public function ViewWhereAndtri($Table,$WhereField,$WhereValue,$WhereField2,$WhereValue2,$WhereField3,$WhereValue3) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' AND ".$WhereField2." = '".$WhereValue2."' AND ".$WhereField3." = '".$WhereValue3."'");
		return $Query->result_array();	
	}

	public function ViewWhereAndtriPNP($Table,$WhereField,$WhereValue,$WhereField2,$WhereValue2,$WhereField3,$WhereValue3) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' AND ".$WhereField2." = '".$WhereValue2."' AND ".$WhereField3." = '".$WhereValue3."' AND id_kota = '".$this->session->userdata('kota')."' ");
		return $Query->result_array();	
	}

	public function ViewWhereAndtriPKT($Table,$WhereField,$WhereValue,$WhereField2,$WhereValue2,$WhereField3,$WhereValue3) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' AND ".$WhereField2." = '".$WhereValue2."' AND ".$WhereField3." = '".$WhereValue3."' AND id_kota = '".$this->session->userdata('kota')."' ORDER BY id_paket DESC");
		return $Query->result_array();	
	}

	public function KasPaketLunas() {
		$Query = $this->db->query("SELECT sum(biaya-diskon) as paketlunas FROM paket WHERE batal_paket = '0' AND created_date = '".date("d-m-Y")."' AND bayar = '1'  AND (created_by = '".$this->session->userdata('nama')."' OR is_edit = '".$this->session->userdata('nama')."') ");
		return $Query->result_array();	
	}

	public function KasPaketTerima() {
		$Query = $this->db->query("SELECT sum(biaya-diskon) as paketlunas FROM paket WHERE batal_paket = '0' AND tanggal_kirim != '".date("d-m-Y")."' AND created_date = '".date("d-m-Y")."' AND bayar = '1'  AND created_by = '".$this->session->userdata('nama')."' ");
		return $Query->result_array();	
	}

	public function KasPenumpangLunas() {
		$Query = $this->db->query("SELECT sum(tarif-komisi_agen) as paketlunas FROM penumpang WHERE batal = '0' AND pindah = '0' AND created_date = '".date("d-m-Y")."' AND bayar = '2'  AND id_karyawan = '".$this->session->userdata('nama')."' ");
		return $Query->result_array();	
	}

	public function KasPenumpangTerima() {
		$Query = $this->db->query("SELECT sum(tarif-komisi_agen) as paketlunas FROM penumpang WHERE batal = '0' AND pindah = '0' AND tanggal != '".date("d-m-Y")."' AND bayar = '2'  AND id_karyawan = '".$this->session->userdata('nama')."' ");
		return $Query->result_array();	
	}

	public function ViewWhereGroup($Table,$WhereField,$WhereValue,$cGroup) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' GROUP BY ".$cGroup." ");
		return $Query->result_array();	
	}
	public function ViewWhereLike($Table,$WhereField,$WhereValue) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." LIKE '%".$WhereValue."%'");
		return $Query->result_array();	
	}
	public function ViewWhereAktor($Table,$WhereField,$WhereValue) {
		$Query = $this->db->query("SELECT * FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' ORDER BY id DESC");
		return $Query->result_array();	
	}

	public function ViewWhereSum($Table,$WhereField,$WhereValue,$nTahun) {
		$Query = $this->db->query("SELECT SUM(jumlah) as Jumlah FROM ".$Table." WHERE ".$WhereField." = '".$WhereValue."' AND tahun = '".$nTahun."' ");
		return $Query->result_array();	
	}

	public function SelectPaket() {
		$Query = $this->db->query("SELECT MAX(right(no_resi,'4')) as angka FROM paket");
		return $Query->result_array();	
	}

	public function SelectPenumpang() {
		$Query = $this->db->query("SELECT MAX(right(no_resi,'4')) as angka FROM penumpang ORDER BY id_penumpang DESC");
		return $Query->result_array();	
	}

	public function SelectSpPenumpang() {
		$Query = $this->db->query("SELECT MAX(right(sp_penumpang,'3')) as angka FROM antar_paket");
		return $Query->result_array();	
	}

	public function SelectSpPaket() {
		$Query = $this->db->query("SELECT MAX(right(sp_paket,'3')) as angka FROM antar_paket");
		return $Query->result_array();	
	}

	public function SelectBSD() {
		$Query = $this->db->query("SELECT MAX(right(noBSD,'2')) as angka FROM tr_bsd_paket");
		return $Query->result_array();	
	}
	public function SelectBSDP() {
		$Query = $this->db->query("SELECT MAX(right(noBSD,'2')) as angka FROM tr_bsd_penumpang");
		return $Query->result_array();	
	}



	public function Insert($Table,$Value){
		$Query = $this->db->insert($Table,$Value);
		return $Query ;
	}
	public function Update($Table,$Where,$WhereValue,$Value){
		$this->db->where($Where,$WhereValue);
		$this->db->update($Table,$Value);
	}
	public function UpdateDua($Table,$Where,$WhereValue,$WhereDua,$WhereValueDua,$Value){
		$this->db->where($Where,$WhereValue);
		$this->db->where($WhereDua,$WhereValueDua);
		$this->db->update($Table,$Value);
	}
	public function UpdatePOkemasan($Table,$Where,$WhereValue,$WhereDua,$WhereValueDua,$Value){
		$this->db->where($Where,$WhereValue);
		$this->db->where($WhereDua,$WhereValueDua);
		$this->db->update($Table,$Value);
	}
	public function Delete($Table,$Where,$WhereValue){
		$this->db->where($Where,$WhereValue);
		$this->db->delete($Table);
	}

	public function GetId($Id,$Table) {
		$Query = $this->db->query("SELECT max($Id) FROM ".$Table." ");
		return $Query->result_array();	
	}

	function get_sub_category($category_id){
        $query = $this->db->get_where('v_stock_kemasan_new', array('id_kemasan' => $id_kemasan,'nama_kemasan' => $nama_kemasan));
        return $query;
    }	
}