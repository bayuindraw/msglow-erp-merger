            <li class="">
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Master/index_barang"><i class="icon-speedometer"></i><span> Dashboard</span></a>
            </li>
            <li class="nav-level">Barang jadi</li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock_Produk/stock_opname_produk"><i class="icon-list"></i></i><span> Stock Barang Jadi</span></a>
            </li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock_Produk/kartu_stock_produk"><i class="icon-screen-desktop"></i></i><span> Kartu Stock</span></a>
            </li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock_Produk/po_produk"><i class="icon-tag"></i><span> Input Purchase Order</span></a>
            </li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span> List PO</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <!-- <li><a class="waves-effect waves-dark" href="#"><i class="icon-arrow-right"></i><span>Pembayaran PO</span></a></li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="icon-arrow-right"></i><span>Penerimaan PO</span></a></li> -->
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock_Produk/po_produk_active_pic">
                      <i class="icon-arrow-right"></i><span>Po Active</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="icon-arrow-right"></i><span>Po Selesai</span></a></li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="icon-arrow-right"></i><span>Retur Barang Jadi</span></a></li>
                </ul>
            </li>
            <li class="nav-level">Pengemasan</li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/input_kemas"><i class="icon-basket"></i><span>Data Penjualan</span></a>
            </li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span> Data Kemas</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/kemas_pending">
                        <i class="icon-arrow-right"></i><span>List Kemas Pending</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/Penjualan_Seller">
                        <i class="icon-arrow-right"></i><span>List Kemas Approved</span></a>
                    </li>
                    
                </ul>
            </li>
            <li class="nav-level">Laporan</li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/laporan_marketing"><i class="icon-basket"></i><span>LAPORAN</span></a>
            </li>