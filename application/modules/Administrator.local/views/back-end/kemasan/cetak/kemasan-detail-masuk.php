<link rel="stylesheet" type="text/css" href="http://165.22.105.2/assets/css/main.css">
<style type="text/css">
	.table1 {
    font-family: sans-serif;
    color: black;
    border-collapse: collapse;
	}
	 
	.table1, th, td {
	    border: 1px solid black;
	    padding: 3px 10px;
	}
</style>
<?php
	function String2Date($dTgl){
			//return 22-11-2012  
			list($cYear,$cMount,$cDate)	= explode("-",$dTgl) ;
			if(strlen($cYear) == 4){
				$dTgl	= $cDate . "-" . $cMount . "-" . $cYear ;
			} 
			return $dTgl ; 	
		}		
	$bulan = array(
                '01' => 'JANUARI',
                '02' => 'FEBRUARI',
                '03' => 'MARET',
                '04' => 'APRIL',
                '05' => 'MEI',
                '06' => 'JUNI',
                '07' => 'JULI',
                '08' => 'AGUSTUS',
                '09' => 'SEPTEMBER',
                '10' => 'OKTOBER',
                '11' => 'NOVEMBER',
                '12' => 'DESEMBER',
        );

	 function DateTimeStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("d-m-Y h:i:s");
			return $Data ;
		} 
?>
<h2 align="center" style="color:black">LAPORAN PENERIMAAN KEMASAN MS GLOW</h2>
<h3 align="center" style="color:black"><i>PERIODE <?=$bulan[$mbulan]?></i></h3>

<span align="right">Tanggal Cetak : <?=DateTimeStamp()?></span>

<table border="1" style="width:100%" class="table1">
	<tr style="background-color: #95fffd">
		<td align="center" style="width:2%"><b>No</b></td>
		<td align="center" style="width:7%"><b>Nama Kemasan</b></td>
		<?php 
			foreach ($tanggal as $key => $vaTanggal) {
				$tanggal = $vaTanggal['tgl_terima'];
		?>
		<td align="center" style="width:7%"><b><?=String2Date($vaTanggal['tgl_terima'])?><b></td>
		<?php 
			}
		?>
		<td align="center" style="width:15%"><b>Total Terima</b></td>
	</tr>
	<?php 
		$no = 0;
		foreach ($kemasan as $key => $vaData) {
		
	?>
	<tr>
		<td><?= ++$no ?></td>
		<td><?=$vaData['nama_kemasan']?></td>
		<td>
			<?php 
				if($supplier == 'ALL'){
					$query = $this->model->code("SELECT sum(jumlah) as total FROM v_terima_kemasan WHERE tgl_terima = '".$tanggal."' AND id_barang = '".$vaData['id_barang']."'");
				}else{
					$query = $this->model->code("SELECT sum(jumlah) as total FROM v_terima_kemasan WHERE tgl_terima = '".$tanggal."' AND supplier = '".$supplier."' AND id_barang = '".$vaData['id_barang']."'");
				}
				foreach ($query as $key => $vaRow) {
					echo $semua = $vaRow['total'];
				}

			?>
		</td>
		<td align="center"></td>
	</tr>
   <?php }?>
</table>

<script>
	window.print();
</script>