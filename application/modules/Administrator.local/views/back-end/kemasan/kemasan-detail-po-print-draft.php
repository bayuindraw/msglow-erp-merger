
<div class="row">
							<div class="col-lg-12 col-xl-12">							
									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Form Detail Purchase Order Kemasan
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">
									
									<form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock_Act/approve_po_print" method="Post">
										<div dir  id="dir" content="table_stock">
                        <table class="table table-striped table-bordered nowrap" style="font-size: 13px">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Kemasan</th>
                            <th>Jumlah Order</th>
                            <!--<th>Action</th>-->
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['nama_kemasan']?></td>
                            <td><div class="form-group form-inline"><input type="text" name="qty[<?= $vaData['id_pokemasan'] ?>]" value="<?=$vaData['jumlah']?>" style="width:20%" class="form-control"> &nbsp; Pcs</div></td>
                            <!--<td>
                                <a href="<?=base_url()?>Administrator/Stock_Act/hapus_detail_po/<?=$vaData['id_pokemasan']?><?=$vaData['kode_pb']?>" type="button" class="btn btn-danger waves-effect waves-light"><i class="icofont icofont-ui-delete"></i></a>
                            </td>-->
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                      </div>
					  <div id="btn-pb">
                          
                          <input type="hidden" name="id" value="<?= $kodepo ?>">
                          <input type="hidden" name="kode" value="<?=$vaData['kode_po']?>">
                            <button type="submit" class="btn btn-success waves-effect waves-light">
                             APPROVE PO
                            </button>
                            <a href="<?=base_url()?>Administrator/Stock/po_kemasan_pic" class="btn btn-warning waves-effect waves-light">
                             BATALKAN
                            </a>
                          
                      </div>
					  </form>
											</div>
</div>										
</div>										
								
</div>