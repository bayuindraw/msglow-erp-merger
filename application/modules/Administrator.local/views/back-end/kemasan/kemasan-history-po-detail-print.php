<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            History Pengajuan PO Print
          </h3>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            PO yang diajukan
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">


        <div dir id="dir" content="table_stock">
          <table class="table table-striped table-bordered nowrap" style="font-size: 13px">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kemasan</th>
                <th>Jumlah Order</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 0;
              foreach ($row1 as $key => $vaData) {
              ?>
                <tr>
                  <td><?= ++$no ?></td>
                  <td><?= $vaData['nama_kemasan'] ?></td>
                  <td><?= number_format($vaData['jumlah']) ?> Pcs</td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          <a href="<?= base_url() ?>Administrator/Stock/export_pdf_print/<?= $id; ?>" class="btn btn-danger btn-elevate btn-icon-sm" style="margin-top: 15px;" target="_blank">
            <i class="la la-print"></i>
            CETAK
          </a>
        </div>
      </div>

    </div>
  </div>

  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            PO yang disetujui
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">


        <div dir id="dir" content="table_stock">
          <table class="table table-striped table-bordered nowrap" style="font-size: 13px">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kemasan</th>
                <th>Jumlah Order</th>

              </tr>
            </thead>
            <tbody>
              <?php
              $no = 0;
              foreach ($row2 as $key => $vaData) {
              ?>
                <tr>
                  <td><?= ++$no ?></td>
                  <td><?= $vaData['nama_kemasan'] ?></td>
                  <td><?= number_format($vaData['jumlah']) ?> Pcs</td>

                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>



    </div>
  </div>
</div>