<!--<div class="main-header">
              <h4><?=$menu?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="<?=base_url()?>"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Stock</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Stock Opname Kemasan</a>
                    </li>
                </ol>
            </div>-->
<div class="kt-portlet kt-portlet--mobile">
  
          
<!--<div class="card-header"><h5 class="card-header-text">STOCK REAL KEMASAN</h5></div>-->
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											STOCK REAL KEMASAN
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
											</div>
											<div class="kt-portlet__head-actions">
												<a href="<?=base_url()?>Administrator/Stock/cetak_stock_kemasan_real" class="btn btn-primary waves-effect waves-light" target="_blank"> <i class="la la-print"></i> <span class="m-l-10">Cetak Stock</span></a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<div dir  id="dir" content="table">
                     <table class="table table-striped table-bordered nowrap" id="multi-colum-dt">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Kemasan</th>
                            <th>Kategori</th>
                            <th>Stock</th>
                            <th>Rusak</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['nama_kemasan']?> <label style="font-weight: 800;color: red;">(<?=$vaData['kode_sup']?>)</label></td>
                            <td><?=$vaData['kategori']?></td>
                            <td><?=number_format($vaData['jumlah']-$vaData['rusak'])?></td>
                            <td><?=number_format($vaData['rusak'])?></td>
                            <td>
                              <?php

                                  if($vaData['jumlah'] <= '0'){
                                    $kemasan  = 'STOCK HABIS';
                                    $label    = 'btn-danger';
                                  }elseif($vaData['jumlah'] > 0 || $vaData['jumlah'] < $vaData['min_jumlah']){
                                    $kemasan  = 'AKAN HABIS';
                                    $label    = 'btn-warning';
                                  }elseif($vaData['jumlah'] > $vaData['min_jumlah']){
                                    $kemasan  = 'STOCK TERSEDIA';
                                    $label    = 'btn-primary';
                                  }

                              ?>
                              <strong class="btn <?=$label?>"><?=$kemasan?></strong>
                            </td>
                            
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                   </div> 

									<!--end: Datatable -->
								</div>
							</div>