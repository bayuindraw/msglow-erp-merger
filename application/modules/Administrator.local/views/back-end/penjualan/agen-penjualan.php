<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$file?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Penjualan</a>
                    </li>
                    
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-lg-12">

    <!-- Row start -->
    <div class="row">

      <div class="col-lg-12">

        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">Form Input Penjualan Seller Msglow</h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->

                <!-- Nav tabs -->
                <ul class="nav nav-tabs md-tabs " role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#data" role="tab">
                      <i class="icon-grid"></i> &nbsp;&nbsp; Form Input Penjualan Seller Msglow</a>
                      <div class="slide">
                        
                      </div>
                  </li>
                  
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                  <br/> <br/>
          <div class="tab-pane active" id="data" role="tabpanel">
              <div class="col-sm-6 col-xs-12">
                  <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Penjualan_Act/add_jual_produk" method="Post">
                   
                    <div class="md-group-add-on p-relative">
                     <span class="md-add-on">
                      <i class="icofont icofont-barcode"></i>
                    </span>
                    
                    <div class="md-input-wrapper">
                      <input type="text" id="cKodeJual" name="cKodeJual" 
                      class="md-form-control md-static" value="<?=$kodepo?>" >
                      <label for="KodePurchaseOrder">Kode Penjualan</label>
                      <span class="messages"></span>
                    </div>
                  </div>
                    <div class="md-group-add-on p-relative">
                     <span class="md-add-on">
                      <i class="icofont icofont-ui-calendar"></i>
                    </span>
                    <div class="md-input-wrapper">
                      <input type="text" id="cTanggal" name="cTanggal" 
                      class="md-form-control md-static floating-label 4IDE-date" value="<?=$this->session->userdata('tanggal')?>">
                      <label for="KodeBarang">Tanggal Penjualan</label>
                      <span class="messages"></span>
                      <input type="hidden" id="cIdOutlet" name="NamaOutlet" 
                      class="md-form-control md-static floating-label" value="1">
                      </div>
                    </div>
                      <div class="md-group-add-on p-relative">
                         <span class="md-add-on">
                          <i class="icon-handbag"></i>
                        </span>
                        <div class="md-input-wrapper">
                         <select name="Supplier" id="pilihSeller" class="md-form-control md-static">
                          <option></option>
                            <?php
                              $query = $this->model->code("SELECT * FROM member ORDER BY id_member LIMIT 100");
                              foreach ($query as $key => $vaSupplier) {
                            ?>
                              <option value="<?=$vaSupplier['id_member']?>" <?php if($this->session->userdata('seller') == $vaSupplier['id_member']){?> selected <?php } ?>>
                                 <?=$vaSupplier['kode']?> - <?=$vaSupplier['nama']?>
                              </option>
                           <?php } ?>
                         </select>
                          <label for="Pilih Supplier"></label>
                          <span class="messages"></span>
                        </div>
                      </div>
                      <div class="md-group-add-on p-relative">
                         <span class="md-add-on">
                          <i class="icofont icofont-ui-tag"></i>
                        </span>
                        <div class="md-input-wrapper">
                         <select name="NamaBarang" id="cIdStock" class="md-form-control md-static" onchange="ambilharga()">

                          <option></option>
                          <?php
                            $query = $this->model->ViewAsc('v_stock_produk','id_barang');
                            foreach ($query as $key => $vaKemasan) {
                          ?>
                            <option value="<?=$vaKemasan['id_barang']?>"><?=$vaKemasan['nama_produk']?></option>
                          <?php } ?>
                         </select>
                          <label for="NamaBarang"></label>
                          <span class="messages"></span>
                        </div>
                      </div>
                      <div class="md-group-add-on p-relative">
                       <span class="md-add-on">
                        <i class="icofont icofont-plus-circle"></i>
                      </span>
                       <div class="md-input-wrapper">

                       <input type="text" id="cHarga" name="cHarga" value="0" 
                       class="md-form-control md-static" readonly>
                       <label for="JumlahPo">Harga</label>
                       <span class="messages"></span>
                       </div>
                     </div>
                      <div class="md-group-add-on p-relative">
                       <span class="md-add-on">
                        <i class="icofont icofont-plus-circle"></i>
                      </span>
                       <div class="md-input-wrapper">

                       <input type="text" id="cJumlah" name="JumlahPo" value="0" onkeyup="return sum();" 
                       class="md-form-control md-static">
                       <label for="JumlahPo">Jumlah (PCS)</label>
                       <span class="messages"></span>
                       </div>
                     </div>
                     
                     <div class="md-group-add-on p-relative">
                             <span class="md-add-on">
                              <i class="icofont icofont-plus-circle"></i>
                            </span>
                             <div class="md-input-wrapper">

                             <input type="text" id="cDiskon" name="cDiskon" value="0" onkeyup="return sum();" 
                             class="md-form-control md-static">
                             <label for="JumlahPo">Diskon</label>
                             <span class="messages"></span>
                             </div>
                     </div>
                     <div class="md-group-add-on p-relative">
                       <span class="md-add-on">
                        <i class="icofont icofont-plus-circle"></i>
                      </span>
                       <div class="md-input-wrapper">

                       <input type="text" id="cHargaJadi" name="cHargaJadi" value="0" readonly 
                       class="md-form-control md-static">
                       <label for="JumlahPo">Harga Jadi</label>
                       <span class="messages"></span>
                       </div>
                     </div>
                     <div class="md-group-add-on p-relative">
                       <span class="md-add-on">
                        <i class="icofont icofont-plus-circle"></i>
                      </span>
                       <div class="md-input-wrapper">

                       <input type="text" id="cTotal" name="cTotal" value="0" readonly 
                       class="md-form-control md-static">
                       <label for="JumlahPo">Total Harga</label>
                       <span class="messages"></span>
                       </div>
                     </div>
                        <div class="md-input-wrapper">     
                         <button type="submit" class="btn btn-primary waves-effect waves-light " title="Tambahkan Kemasan">
                         <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Tambahkan Barang</span>
                         </button>
                     </div>
                      </form>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                      <div dir  id="dir" content="table_stock">
                        <table class="table table-striped table-bordered nowrap" style="font-size: 13px">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Produk</th>
                            <th>Jumlah Order</th>
                            <th>Diskon</th>
                            <th>Harga Jadi</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['nama_produk']?></td>
                            <td><?=number_format($vaData['jumlah'])?> Pcs</td>
                            <td>Rp. <?=number_format($vaData['diskon'])?></td>
                            <td>Rp. <?=number_format($vaData['harga_jadi'])?></td>
                            <td>
                                <button type="button" class="btn btn-danger waves-effect waves-light" title="Hapus 1/4MN POLOS 310GR SC "><i class="icofont icofont-ui-delete"></i></button>
                            </td>
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                      </div>
                      <div id="btn-pb">
                          <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Penjualan_Act/add_penjualan_agen" method="Post">
                          <input type="hidden" name="tanggalpo" value="<?=$this->session->userdata('tanggal')?>">
                          <input type="hidden" name="supplier" value="<?=$this->session->userdata('supplier')?>">
                          <button type="submit" class="btn btn-inverse-success waves-effect waves-light">
                             BUAT PENJUALAN
                          </button>
                          </form>
                      </div>
                    </div>
                      <input type="hidden" name="aksi" value="simpan" id="aksi" name="aksi">
                      <input type="hidden" name="hapus" value="" id="hapus" name="hapus">
                      </div>
                    </div>


                  </div>
                 
              </div>

            </div>
            <!-- Row end -->
          </div>
        </div>
      </div>
    </div>
    <!-- Row end -->
  </div>
</div>
<!-- loader ends -->
</div>
<script type="text/javascript">
              function ambilharga(){
                  var nip = $("#cIdStock").val();
                  $.ajax({
                       type: "GET",
                       url: "<?=site_url('Administrator/Penjualan/ambil_harga')?>/"+nip,
                       /*data:"kode="+nip,*/
                        success:function(data){
                          var json = data,
                          obj = JSON.parse(json);
                          $('#cHarga').val(obj.harga);
                        }
                      });
                  }

            function sum() {
                  var txtFirstNumberValue = document.getElementById('cJumlah').value;
                  var txtSecondNumberValue = document.getElementById('cDiskon').value;
                  var txtThirdNumberValue = document.getElementById('cHarga').value;
                  var resultDua = parseInt(txtThirdNumberValue) - parseInt(txtSecondNumberValue);
                  var result = parseInt(resultDua) * parseInt(txtFirstNumberValue);
                  if (!isNaN(resultDua)) {
                     document.getElementById('cHargaJadi').value = resultDua;
                  }
                  if (!isNaN(result)) {
                     document.getElementById('cTotal').value = result;
                  }
            }
</script>