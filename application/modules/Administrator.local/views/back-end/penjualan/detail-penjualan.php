<?php

foreach ($po as $key => $vaPO) {
  $totaluang = $vaPO['total_biaya'];
  $supplier = $vaPO['nama_supplier'];
}

$query = $this->model->code("SELECT sum(jumlah) as totalbayar FROM bayar_kemasan WHERE kode_po = '" . $action . "'");
foreach ($query as $key => $vaBayar) {
  $totalbayar = $vaBayar['totalbayar'];
}

?>

<div class="row">
  <div class="col-lg-12 col-xl-12">
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            KODE PENJUALAN : <?= $action ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
        <h6>Detail ORDER : </h6>
        <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Produk</th>
              <th>Order</th>
              <?php

              $query = $this->model->code("SELECT * FROM penjualan_detail_kemas WHERE kode_jual = '" . $action . "' GROUP BY tanggal ");
              foreach ($query as $key => $vaTanggal) {

              ?>
                <th><?= ($vaTanggal['tanggal']) ?></th>
              <?php } ?>
              <th>Total Kirim</th>
              <th>Kurang Kirim</th>
              <th>Harga</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 0;
            foreach ($po as $key => $vaData) {
            ?>
              <tr>
                <td><?= ++$no ?></td>
                <td><?= $vaData['nama_produk'] ?></td>
                <td><?= number_format($vaData['jumlah']) ?> Pcs</td>
                <?php

                $query = $this->model->code("SELECT * FROM penjualan_detail_kemas WHERE kode_jual = '" . $action . "' GROUP BY tanggal ");
                foreach ($query as $key => $vaTanggal) {
                  $queryProduk = $this->model->code("SELECT SUM(jumlah) as total FROM penjualan_detail_kemas WHERE kode_jual = '" . $action . "' 
                                                 AND id_produk = '" . $vaData['id_barang'] . "' AND tanggal = '" . $vaTanggal['tanggal'] . "'");
                  foreach ($queryProduk as $key => $vaNilai) {
                    $total = $vaNilai['total'];
                  }

                ?>
                  <td><?= $total ?></td>
                <?php
                  $queryProdukDua = $this->model->code("SELECT SUM(jumlah) as total FROM penjualan_detail_kemas WHERE kode_jual = '" . $action . "' 
                                                 AND id_produk = '" . $vaData['id_barang'] . "' ");
                  foreach ($queryProdukDua as $key => $vaNilai) {
                    $totalKirim = $vaNilai['total'];
                  }
                }

                ?>
                <td><b><?= number_format($totalKirim) ?></b></td>
                <td><b><?= number_format($vaData['jumlah'] - $totalKirim) ?></b></td>
                <td>Rp. <?= number_format($vaData['harga_satuan']) ?></td>
                <td>Rp. <?= number_format($vaData['harga_satuan'] * $totalKirim) ?></td>
              </tr>
            <?php } ?>

          </tbody>
        </table>
        <hr>

        <h6>Penerima Pembayaran Penjualan : <?= $action ?></h6>
        <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal Terima</th>
              <th>Bank</th>
              <th>Nominal</th>
              <th>Keterangan</th>
              <th>User</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 0;
            foreach ($detail as $key => $vaData) {
            ?>
              <tr>
                <td><?= ++$no ?></td>
                <td><?= $vaData['tanggal'] ?></td>
                <td><?= $vaData['id_produk'] ?></td>
                <td align="right">Rp. <?= number_format($vaData['jumlah']) ?></td>
                <td><b><?= $vaData['keterangan'] ?></b></td>
                <td><?= $vaData['created_by'] ?></td>
              </tr>
            <?php } ?>
            <?php

            foreach ($total_bayar as $key => $vaBayar) {
              $totalB     = $vaBayar['total'];
              $totalJual  = $vaBayar['total_bayar'];
            }

            ?>
            <tr>
              <td colspan="3">Total Penjualan</td>
              <td align="right">Rp. <?= number_format($totalJual) ?></td>
            </tr>
            <tr>
              <td colspan="3">Saldo</td>
              <td align="right">Rp. <?= number_format(0) ?></td>
            </tr>
            <tr>
              <td colspan="3">Total Bayar</td>
              <td align="right">Rp. <?= number_format($totalB) ?></td>
            </tr>
            <tr>
              <td colspan="3">Kurang Bayar</td>
              <td align="right">Rp. <?= number_format($totalJual - $totalB) ?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-paket" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Edit Penerimaan Stock Barang Jadi</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>