<link rel="stylesheet" type="text/css" href="http://165.22.105.2/assets/css/main.css">
<style type="text/css">
	.table1 {
    font-family: sans-serif;
    color: black;
    border-collapse: collapse;
	}
	 
	.table1, th, td {
	    border: 1px solid black;
	    padding: 3px 10px;
	}
</style>
<?php
	function String2Date($dTgl){
			//return 22-11-2012  
			list($cYear,$cMount,$cDate)	= explode("-",$dTgl) ;
			if(strlen($cYear) == 4){
				$dTgl	= $cDate . "-" . $cMount . "-" . $cYear ;
			} 
			return $dTgl ; 	
		}	

	$bulan = array(
                '01' => 'JANUARI',
                '02' => 'FEBRUARI',
                '03' => 'MARET',
                '04' => 'APRIL',
                '05' => 'MEI',
                '06' => 'JUNI',
                '07' => 'JULI',
                '08' => 'AGUSTUS',
                '09' => 'SEPTEMBER',
                '10' => 'OKTOBER',
                '11' => 'NOVEMBER',
                '12' => 'DESEMBER',
        );

	 function DateTimeStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("d-m-Y h:i:s");
			return $Data ;
		} 
?>
<h2 align="center" style="color:black">LAPORAN DETAIL KEMASAN KLUAR MS GLOW</h2>
<h3 align="center" style="color:black"><i>PERIODE <?=$bulan[$mbulan]?></i></h3>

<span align="right">Tanggal Cetak : <?=DateTimeStamp()?></span>
<?php 
	
	foreach ($row as $key => $vaData) {

?>
<h3>NAMA PABRIK : <?=$vaData['nama_factory']?></h3>
<table border="1" style="width:100%" class="table1">
	<tr style="background-color: #95fffd">
		<td align="center" style="width:2%"><b>No</b></td>
		<td align="center" style="width:7%"><b>Nama Kemasan</b></td>
		<?php 

		  $nJumlahHari = cal_days_in_month(CAL_GREGORIAN,$mbulan,$tahun);
          for($i=1;$i<=$nJumlahHari;$i++){
            if($i > 9){
              $cNol = "";
            }else{
              $cNol = "0";
            }
          $date = $tahun."-".$mbulan."-".$cNol.$i ;
		?>
		<td><?=String2Date($date)?></td>
	<?php } ?>
		<td align="center" style="width:15%"><b>Total</b></td>
	</tr>
	<?php 
		$no = 0;
		$query = $this->model->code("SELECT * FROM v_kluar_kemasan WHERE MONTH(tanggal) = '".$this->input->post('cBulan')."' 
				 AND YEAR(tanggal) = '".$this->input->post('cTahun')."' AND id_pabrik = '".$vaData['id_pabrik']."' GROUP BY nama_kemasan");
		foreach ($query as $key => $vaRow) {
		
	?>
	<tr>
		<td><?= ++$no ?></td>
		<td width="15%"><?=$vaRow['nama_kemasan']?></td>
		
		<td align="center"><?=number_format($vaRow['jumlah'])?></td>
	</tr>
   <?php }?>
</table>
<?php } ?>

<script>
	window.print();
</script>