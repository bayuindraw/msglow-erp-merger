<link rel="stylesheet" type="text/css" href="http://165.22.105.2/assets/css/main.css">
<style type="text/css">
	.table1 {
    font-family: sans-serif;
    color: black;
    border-collapse: collapse;
	}
	 
	.table1, th, td {
	    border: 1px solid black;
	    padding: 3px 10px;
	}
</style>
<?php
		
	$bulan = array(
                '01' => 'JANUARI',
                '02' => 'FEBRUARI',
                '03' => 'MARET',
                '04' => 'APRIL',
                '05' => 'MEI',
                '06' => 'JUNI',
                '07' => 'JULI',
                '08' => 'AGUSTUS',
                '09' => 'SEPTEMBER',
                '10' => 'OKTOBER',
                '11' => 'NOVEMBER',
                '12' => 'DESEMBER',
        );

	 function DateTimeStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("d-m-Y h:i:s");
			return $Data ;
		} 
?>

<h2 align="center" style="color:black">LAPORAN DETAIL PO KEMASAN MS GLOW</h2>
<h3 align="center" style="color:black"><i>PERIODE <?=$bulan[$mbulan]?></i></h3>
<div align="right" style="margin-bottom: 10px">
<span style="font-size: 12px">Tanggal Cetak : <?=DateTimeStamp()?></span>
</div>
<?php 
	

	foreach ($row as $key => $vaData) {

?>
<br>
<h3>KODE PO : <?=$vaData['kode_pb']?> | NAMA SUPPLIER : <?=$vaData['nama_supplier']?></h3>
<table border="1" style="width:100%;font-size: 12px" class="table1">
	<tr style="background-color: #95fffd">
		<td align="center" style="width:20%"><b>NAMA KEMASAN</b></td>
		<td align="center" style="width:15%"><b>QTY ORDER</b></td>
		<td align="center" style="width:15%"><b>QTY TERIMA</b></td>
		<td align="center" style="width:15%"><b>QTY KURANG</b></td>
	</tr>
	<?php 
		$query = $this->model->code("SELECT * FROM v_detail_po_kemas WHERE MONTH(tanggal) = '".$this->input->post('cBulan')."' AND YEAR(tanggal) = '".$this->input->post('cTahun')."' AND nama_supplier = '".$vaData['nama_supplier']."' ");
		foreach ($query as $key => $vaRow) {
		
		$queryJum = $this->model->code("SELECT sum(jumlah) as totalbayar FROM terima_kemasan WHERE kode_po = '".$vaRow['kode_pb']."' AND id_barang = '".$vaRow['id_barang']."'");
	      foreach ($queryJum as $key => $vaBayar) {
	        $totalterima = $vaBayar['totalbayar'];
	      }
	?>
	<tr>
		<td><?=$vaRow['nama_kemasan']?></td>
		<td align="center"><?=number_format($vaRow['jumlah'])?></td>
		<td align="center"><?=number_format($totalterima)?></td>
		<td align="center"><?=number_format($vaRow['jumlah']-$totalterima)?></td>
	</tr>
   <?php }?>
</table>

<?php } ?>

<script>
	window.print();
</script>