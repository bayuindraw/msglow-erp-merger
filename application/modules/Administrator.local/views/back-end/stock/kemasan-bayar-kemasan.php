<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$file?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">UI Elements</a>
                    </li>
                    <li class="breadcrumb-item"><a href="panels-wells.html">Panels Wells</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-lg-12">

    
    <?php 

      foreach ($po as $key => $vaPO) {
        $totaluang = $vaPO['total_biaya'];
        $supplier = $vaPO['nama_supplier'];
      }

      $query = $this->model->code("SELECT sum(jumlah) as totalbayar FROM bayar_kemasan WHERE kode_po = '".$action."'");
      foreach ($query as $key => $vaBayar) {
        $totalbayar = $vaBayar['totalbayar'];
      }

    ?>
    <!-- Row start -->
    <div class="row">

      <div class="col-lg-12">

        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">PEMBAYARAN PO KEMASAN KODE PO : <?=$action?></h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->
                <div class="tab-content">
                  <div class="tab-pane active" id="data" role="tabpanel">
                          <h6>Detail Purchase Order : </h6>
                          <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Supplier</th>
                                <th>Nama Kemasan</th>
                                <th>Jumlah Pembelian</th>
                                <th>Harga Satuan</th>
                                <th>Total Harga</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $no=0;
                                foreach ($row as $key => $vaData) {
                              ?>
                              <tr>
                                <td><?=++$no?></td>
                                <td><?=$vaData['nama_supplier']?></td>
                                <td><?=$vaData['nama_kemasan']?></td>
                                <td><?=number_format($vaData['jumlah'])?> Pcs</td>
                                <td>Rp. <?=number_format($vaData['harga_satuan'])?></td>
                                <td>Rp. <?=number_format($vaData['total'])?></td>
                              </tr>
                             <?php } ?>
                             <tr>
                               <td colspan="5" align="right"> <b>TOTAL</b> </td>
                               <td><b>Rp. <?=number_format($totaluang)?></b></td>
                             </tr>
                            </tbody>
                         </table>
                   <hr>
                   <h6>Detail Termin Pembayaran : </h6>
                          <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Tanggal Bayar</th>
                                <th>Bank</th>
                                <th>User</th>
                                <th>Jumlah Pembayaran</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $no=0;
                                foreach ($bayar as $key => $vaData) {
                              ?>
                              <tr>
                                <td><?=++$no?></td>
                                <td><?=$vaData['tgl_bayar']?></td>
                                <td><?=$vaData['bank']?></td>
                                <td><?=$vaData['user']?></td>
                                <td>Rp. <?=number_format($vaData['jumlah'])?></td>
                              </tr>
                             <?php } ?>
                             <tr>
                               <td colspan="4" align="right"> <b>TOTAL PEMBAYARAN</b> </td>
                               <td><b>Rp. <?=number_format($totalbayar)?></b></td>
                             </tr>
                             <tr>
                               <td colspan="4" align="right"> <b>KURANG PEMBAYARAN</b> </td>
                               <td><b>Rp. <?=number_format($totaluang-$totalbayar)?></b></td>
                             </tr>
                            </tbody>
                         </table>
                  <hr>
                  <h5>Form Pembayaran Po : <?=$action?> | Supplier : <?=$supplier?></h5>
                  <hr>
                   <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock_Act/bayar_kemasan" method="post" novalidate>
                    <div class="md-group-add-on p-relative">
                     <span class="md-add-on">
                      <i class="icofont icofont-barcode"></i>
                    </span>
                    
                    <div class="md-input-wrapper">
                      <input type="text" id="cKodePo" name="KodePurchaseOrder" 
                      class="md-form-control md-static" value="<?=$action?>" readonly>
                      <label for="KodePurchaseOrder">Kode Purchase Order</label>
                      <span class="messages"></span>
                    </div>
                  </div>
                    <div class="md-group-add-on p-relative">
                     <span class="md-add-on">
                      <i class="icofont icofont-ui-calendar"></i>
                    </span>
                    <div class="md-input-wrapper">
                      <input type="text" id="dTglPo" name="TanggalOrder" 
                      class="md-form-control md-static floating-label 4IDE-date" value="<?=date('d-m-Y')?>">
                      <label for="KodeBarang">Tanggal Pembayaran</label>
                      <span class="messages"></span>
                      
                      </div>
                    </div>
                  <div class="md-group-add-on p-relative">
                       <span class="md-add-on">
                        <i class="icofont icofont-ui-tag"></i>
                      </span>
                     <div class="md-input-wrapper">
                        <input type="text" class="md-form-control md-static"  name="JumlahBayar" id="JumlahBayar">
                        <label for="NamaBarang">Jumlah Pembayaran</label>
                        <span class="messages"></span>
                      </div>
                    </div>
                    <div class="md-group-add-on p-relative">
                       <span class="md-add-on">
                        <i class="icofont icofont-cur-dollar"></i>
                      </span>
                     <div class="md-input-wrapper">
                        <input type="text"   class="md-form-control md-static"  id="Bank" name="Bank">
                        <label for="HargaBeli">Bank</label>
                        <span class="messages"></span>
                      </div>
                    </div>
                   

                  <div class="md-input-wrapper">     
                   <button type="submit" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" 
                   data-placement="top" title="{{cValueButton}}">
                   <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
                 </button>
               </div>
                </form>
                
                </div>
              </div>

            </div>
            <!-- Row end -->
          </div>
        </div>
      </div>
    </div>
    <!-- Row end -->
  </div>
</div>
<!-- loader ends -->
</div>
