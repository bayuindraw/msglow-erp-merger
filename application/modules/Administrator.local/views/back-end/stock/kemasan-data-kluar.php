<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$menu?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">UI Elements</a>
                    </li>
                    <li class="breadcrumb-item"><a href="panels-wells.html">Panels Wells</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-lg-12">

    <!-- Row start -->
    <div class="row">

      <div class="col-lg-12">

        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">Pengiriman Kemasan</h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <div class="tab-content">
                  
                  <div class="tab-pane active" id="data" role="tabpanel">
                  
                   <div dir  id="dir" content="table">
                     <table id="multi-colum-dt" class="table table-striped table-bordered nowrap">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Kode Pengiriman</th>
                            <th>Nama Pabrik</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Tanggal Pengiriman</th>
                            <th>User</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['kode_kluar']?></td>
                            <td><?=$vaData['kode_factory']?></td>
                            <td><?=substr($vaData['alamat'],0,20)?></td>
                            <td><?=$vaData['telepon']?></td>
                            <td><?=$vaData['tgl_kirim']?></td>
                            <td><?=$vaData['user']?></td>
                            <td>
                                <a href="<?=base_url()?>Administrator/Stock/edit_pengeluaran_kemasan/<?=$vaData['id_kluar_kemasan']?>" class="btn btn-primary waves-effect waves-light" ><i class="icofont icofont-ui-edit"></i></a>
                                <a href="<?=base_url()?>Administrator/Stock/cetaksuratjalankemasan/<?=$vaData['id_kluar_kemasan']?>" target="_blank" class="btn btn-danger waves-effect waves-light" ><i class="icon-printer""></i></a>
                            </td>
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                   </div> 
                   <span ng-bind="msg"></span>
                  </div>

              </div>

            </div>
            <!-- Row end -->
          </div>
        </div>
      </div>
    </div>
    <!-- Row end -->
  </div>
</div>
<!-- loader ends -->
</div>
