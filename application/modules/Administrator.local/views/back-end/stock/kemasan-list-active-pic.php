<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$file?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">UI Elements</a>
                    </li>
                    <li class="breadcrumb-item"><a href="panels-wells.html">Panels Wells</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-lg-12">

    <!-- Row start -->
    <div class="row">

      <div class="col-lg-12">

        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">DAFTAR LIST PO ACTIVE</h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->

                <!-- Tab panes -->
                <div class="tab-content">
                  <br/> <br/>
                    <div class="tab-pane active" id="data" role="tabpanel">
                        <div class="row">
                          <?php 
                            foreach ($row as $key => $vaData) {

                            $query = $this->model->code("SELECT sum(jumlah) as totalproduk FROM  v_terima_kemasan WHERE kode_po = '".$vaData['kode_po']."'");
                            foreach ($query as $key => $vaTotal) {
                              $totalp =  $vaTotal['totalproduk'];
                            }

                            $queryDua = $this->model->code("SELECT sum(jumlah) as totalproduk FROM  tb_detail_po_kemasan WHERE kode_pb = '".$vaData['kode_po']."'");
                            foreach ($queryDua as $key => $vaTotal) {
                              $totalAll =  $vaTotal['totalproduk'];
                            }

                            if($totalp >= $totalAll){
                              $color = 'PO SELESAI';
                              $warna = 'primary';
                                $data = array (
                                'active'    => 'T',
                                );
                              $this->model->Update('po_kemasan','id_po_kemasan',$vaData['id_po_kemasan'],$data);
                            }else{
                              $color = 'PO BERJALAN';
                              $warna = 'warning';
                            }

                          ?>
                           <div class="col-md-4">
                              <div class="col-sm-12 card dashboard-product">
                                <span><a href="#"><?=$vaData['kode_po']?> (<?=$color?>)</a></span> <br>
                                  <table class="table" width="100%">
                                    <tr>
                                      <td>Supplier</td><td>:</td><td><?=substr($vaData['nama_supplier'],0,15)?></td>
                                    </tr>
                                    <tr>
                                      <td>Total Kemasan</td><td>:</td><td><?=$totalAll?> Item</td>
                                    </tr>
                                    <tr>
                                      <td>Pembayaran</td><td>:</td><td>Rp. 0.00</td>
                                    </tr>
                                    <tr>
                                      <td>Terima Kemasan</td><td>:</td><td><?=$totalp?> Item</td>
                                    </tr>
                                  </table>
                                 
                                  <!-- <span class="label label-warning">Active</span> -->
                                  <span style="font-size: 11px" class="label label-primary"><?=$vaData['tanggal']?></span>  
                                  <span class="label label-success" ><a href="<?=base_url()?>Administrator/Stock/terima_kemasan/<?=$vaData['kode_po']?>" style="font-size: 12px;color:white">Penerimaan PO</a></span>
                                  <span class="label label-<?=$warna?>" ><?=$color?></span>
                                  <div class="side-box bg-<?=$warna?>">
                                  <i class="icon-wallet"></i>
                                  </div>
                              </div>
                            </div>
                          <?php } ?>
                          </div>
                    </div>
                  </div>
                 
              </div>

            </div>
            <!-- Row end -->
          </div>
        </div>
      </div>
    </div>
    <!-- Row end -->
  </div>
</div>
<!-- loader ends -->
</div>
<script type="text/javascript">
  

</script>