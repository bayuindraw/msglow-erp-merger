<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$file?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Stock</a>
                    </li>
                    <li class="breadcrumb-item"><a href="panels-wells.html">Barang Jadi</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-lg-12">

    
    <?php 

      foreach ($po as $key => $vaPO) {
        $totaluang = $vaPO['total_biaya'];
        $supplier = $vaPO['nama_supplier'];
      }

      $query = $this->model->code("SELECT sum(jumlah) as totalbayar FROM bayar_kemasan WHERE kode_po = '".$action."'");
      foreach ($query as $key => $vaBayar) {
        $totalbayar = $vaBayar['totalbayar'];
      }

      $queryJum = $this->model->code("SELECT sum(jumlah) as totalbayar FROM terima_kemasan WHERE kode_po = '".$action."'");
      foreach ($queryJum as $key => $vaBayar) {
        $totalterima = $vaBayar['totalbayar'];
      }

    ?>
    <!-- Row start -->
    <div class="row">

      <div class="col-lg-12">

        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">PENERIMAAN KEMASAN DARI KODE PO : <?=$action?></h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->
                <div class="tab-content">
                  <div class="tab-pane active" id="data" role="tabpanel">
                          <h6>Detail Purchase Order : </h6>
                          <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Supplier</th>
                                <th>Nama Kemasan</th>
                                <th>Jumlah Pembelian</th>
                                <th>Total Terima</th>
                                <th>Kurang Terima</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $no=0;
                                foreach ($row as $key => $vaData) {
                                $queryJum = $this->model->code("SELECT sum(jumlah) as totalbayar FROM terima_kemasan WHERE kode_po = '".$action."' AND id_barang =  '".$vaData['id_barang']."'");
                                foreach ($queryJum as $key => $vaBayar) {
                                  $totalterima = $vaBayar['totalbayar'];
                                }
                              ?>
                              <tr>
                                <td><?=++$no?></td>
                                <td><?=$vaData['nama_supplier']?></td>
                                <td><?=$vaData['nama_kemasan']?></td>
                                <td><?=number_format($vaData['jumlah'])?> Pcs</td>
                                <td><?=number_format($totalterima)?> Pcs</td>
                                <td><?=number_format($vaData['jumlah']-$totalterima)?> Pcs</td>
                              </tr>
                             <?php } ?>
                             
                            </tbody>
                         </table>
                   <hr>
                   <h6>Detail Penerimaan Purchase Order : </h6>
                          <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Tanggal Terima</th>
                                <th>Nama Kemasan</th>
                                <th>Jumlah</th>
                                <th>Rusak</th>
                                <th>Baik</th>
                                <th>User</th>
                                <th>#</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $no=0;
                                foreach ($bayar as $key => $vaData) {
                              ?>
                              <tr>
                                <td><?=++$no?></td>
                                <td><?=$vaData['tgl_terima']?></td>
                                <td><?=$vaData['nama_kemasan']?></td>
                                <td><?=number_format($vaData['jumlah'])?> Pcs</td>
                                <td><?=number_format($vaData['rusak'])?> Pcs</td>
                                <td><?=number_format($vaData['baik'])?> Pcs</td>
                                <td><?=$vaData['user']?></td>
                                <td>
                                  <button class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" 
                                   data-placement="top" onclick="showEditKemasan('<?=$vaData['id_terima_kemasan']?>')">
                                   <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Edit</span>
                                    </button>
                                </td>
                              </tr>
                             <?php } ?>
                             
                            </tbody>
                         </table>
                  <hr>
                  <h5>Form Penerimaan Barang Dari Kode PO : <?=$action?> | Supplier : <?=$supplier?></h5>
                  <hr>
                   <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock_Act/terima_kemasan" method="post" novalidate>
                    <div class="md-group-add-on p-relative">
                     <span class="md-add-on">
                      <i class="icofont icofont-barcode"></i>
                    </span>
                    
                    <div class="md-input-wrapper">
                      <input type="text" id="cKodePo" name="KodePurchaseOrder" 
                      class="md-form-control md-static" value="<?=$action?>" readonly>
                      <label for="KodePurchaseOrder">Kode Purchase Order</label>
                      <span class="messages"></span>
                    </div>
                  </div>
                    <div class="md-group-add-on p-relative">
                     <span class="md-add-on">
                      <i class="icofont icofont-ui-calendar"></i>
                    </span>
                    <div class="md-input-wrapper">
                      <input type="text" id="dTglPo" name="TanggalOrder" 
                      class="md-form-control md-static floating-label 4IDE-date" value="<?=date('d-m-Y')?>" required>
                      <label for="KodeBarang">Tanggal Terima</label>
                      <span class="messages"></span>
                      
                      </div>
                    </div>
                  <div class="md-group-add-on p-relative">
                         <span class="md-add-on">
                          <i class="icofont icofont-ui-tag"></i>
                        </span>
                        <div class="md-input-wrapper">
                         <select name="NamaBarang" id="cIdStock" class="md-form-control md-static" required>

                          <option></option>
                          <?php
                            $query = $this->model->ViewWhere('v_detail_po_kemas','kode_pb',$action);
                            foreach ($query as $key => $vaKemasan) {
                          ?>
                          <option value="<?=$vaKemasan['id_barang']?>"><?=$vaKemasan['nama_kemasan']?></option>
                         <?php } ?>
                         </select>
                          <label for="NamaBarang"></label>
                          <span class="messages"></span>
                        </div>
                      </div>
                  <div class="md-group-add-on p-relative">
                       <span class="md-add-on">
                        <i class="icofont icofont-ui-tag"></i>
                      </span>
                     <div class="md-input-wrapper">
                        <input type="text" class="md-form-control md-static"  name="JumlahBayar" id="JumlahBayar" required>
                        <label for="NamaBarang">Jumlah Terima</label>
                        <span class="messages"></span>
                      </div>
                    </div>
                    
                   

                  <div class="md-input-wrapper">     
                   <button type="submit" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" 
                   data-placement="top" title="{{cValueButton}}">
                   <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
                    </button>
               </div>
                </form>
                
                </div>
              </div>

            </div>
            <!-- Row end -->
          </div>
        </div>
      </div>
    </div>
    <!-- Row end -->
  </div>
</div>
<!-- loader ends -->
</div>
                            <div class="modal fade" id="modal-paket" tabindex="-1" role="dialog">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title">Edit Penerimaan Stock Kemasan</h4>
                                        </div>
                                        <div class="modal-body">
                                            
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">
                              function showEditKemasan($idTerima){
                                $("#modal-paket").modal('show');
                                  $.ajax({
                                   type: "POST",

                                   url: "<?php echo base_url()?>Administrator/Stock/tampil_edit_stock/"+$idTerima,
                                   cache: false,
                                    success:function(msg){
                                      $(".modal-body").html(msg);

                                    }
                                 });
                                }  
                            </script>