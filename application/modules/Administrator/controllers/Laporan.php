<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct(){
		
        parent::__construct();
        ob_start();
		/*error_reporting(0);         */
        $this->load->model('model');
        $this->load->model('relasi');
        $this->load->library('session');
        $this->load->database();
        $this->load->library('m_pdf');
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download'); 
		 
    }
		public  function Date2String($dTgl){
			//return 2012-11-22
			list($cDate,$cMount,$cYear)	= explode("-",$dTgl) ;
			if(strlen($cDate) == 2){
				$dTgl	= $cYear . "-" . $cMount . "-" . $cDate ;
			}
			return $dTgl ; 
		}
		
		public  function String2Date($dTgl){
			//return 22-11-2012  
			list($cYear,$cMount,$cDate)	= explode("-",$dTgl) ;
			if(strlen($cYear) == 4){
				$dTgl	= $cDate . "-" . $cMount . "-" . $cYear ;
			} 
			return $dTgl ; 	
		}
		
		public function TimeStamp() {
			date_default_timezone_set("Asia/Jakarta");
			$Data = date("H:i:s");
			return $Data ;
		}
			
		public function DateStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("d-m-Y");
			return $Data ;
		}  
			
		public function DateTimeStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("Y-m-d h:i:s");
			return $Data ;
		} 

		public function signin($Action=""){
		$data = "" ;
		if($Action == "error"){
			$data['notif'] = "Username / Password Salah" ;
			}
			$msg 			 = 'My secret message';
			$this->load->view('back-end/login-secure',$data);
		
		}

		

		public function cetaksuratjalankemasan($id=""){
	       
	        $data['kode']				= 	$id	;
	        $data['row']				= $this->model->ViewWhere('v_datakluar_kemasan','id_kluar_kemasan',$id);
	        //load the view and saved it into $html variable
	        $html		=	$this->load->view('back-end/stock/cetak/sjkemasan', $data, true);
				 
	        //this the the PDF filename that user will get to download
	        $pdfFilePath = "surat-jalan-kemasan.pdf";
	 
	        //load mPDF library
	 		$this->m_pdf->pdf->mPDF('','A5-L','10','ctimesbi','5','5','5','5','5','10','L');
	 		$this->m_pdf->pdf->useOddEven = true ;

	       //generate the PDF from the given html
	        $this->m_pdf->pdf->WriteHTML($html);
	     
	 
	        //download it.
	        $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	    }		

	    public function laporan_stock_satuan($bulan="",$tahun="",$idbarang=""){
	       
	        $data['bulan']		= 	$bulan	;
	        $data['tahun']		=	$tahun  ;
	        $data['barang']		=   $idbarang ;

	        //$data['row']		= $this->model->ViewWhere('v_datakluar_kemasan','kode_kluar',$id);
	        //load the view and saved it into $html variable

	        $html		=	$this->load->view('back-end/stock/cetak/cetak-laporan-kartu-stock', $data, true);
				 
	        //this the the PDF filename that user will get to download
	        $pdfFilePath = "surat-jalan-kemasan.pdf";
	 
	        //load mPDF library
	 		$this->m_pdf->pdf->mPDF('','A4','10','ctimesbi','5','5','5','5','5','10','P');
	 		$this->m_pdf->pdf->useOddEven = true ;

	       //generate the PDF from the given html
	        $this->m_pdf->pdf->WriteHTML($html);
	     
	 
	        //download it.
	        $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	    }	

	    

		 public function laporan_stock_satuan_produk($bulan="",$tahun="",$idbarang=""){
	       
	        $data['bulan']		= 	$bulan	;
	        $data['tahun']		=	$tahun  ;
	        $data['barang']		=   $idbarang ;

	        //$data['row']		= $this->model->ViewWhere('v_datakluar_kemasan','kode_kluar',$id);
	        //load the view and saved it into $html variable

	        $html		=	$this->load->view('back-end/stock/cetak/cetak-laporan-kartu-stock', $data, true);
				 
	        //this the the PDF filename that user will get to download
	        $pdfFilePath = "surat-jalan-kemasan.pdf";
	 
	        //load mPDF library
	 		$this->m_pdf->pdf->mPDF('','A4','10','ctimesbi','5','5','5','5','5','10','P');
	 		$this->m_pdf->pdf->useOddEven = true ;

	       //generate the PDF from the given html
	        $this->m_pdf->pdf->WriteHTML($html);
	     
	 
	        //download it.
	        $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	    }		

	   

		public function cetak_stock_kemasan_real(){
	       
	        
	        $data['row']	= $this->model->ViewAsc('v_stock_kemasan_new','id_kemasan');
	        
	        $html			= $this->load->view('back-end/stock/cetak/kemasan-cetak-stock-actual', $data, true);
				 
	        $pdfFilePath 	= "cetak stock kemasan actual.pdf";
	 
	        //load mPDF library
	 		$this->m_pdf->pdf->mPDF('','A4','10','ctimesbi','5','5','5','5','5','10','P');
	 		$this->m_pdf->pdf->useOddEven = true ;

	       //generate the PDF from the given html
	        $this->m_pdf->pdf->WriteHTML($html);
	     
	 
	        //download it.
	        $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	    }	

		public function cetak_stock_barang_real(){
	       
	        
	        $data['row']	= $this->model->ViewAsc('v_stock_produk','id_barang');
	        
	        $html			= $this->load->view('back-end/stock/cetak/barang-cetak-stock-actual', $data, true);
				 
	        $pdfFilePath 	= "cetak stock kemasan actual.pdf";
	 
	        //load mPDF library
	 		$this->m_pdf->pdf->mPDF('','A4','10','ctimesbi','5','5','5','5','5','10','P');
	 		$this->m_pdf->pdf->useOddEven = true ;

	       //generate the PDF from the given html
	        $this->m_pdf->pdf->WriteHTML($html);
	     
	 
	        //download it.
	        $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	    }

	    public function cetak_laporan_po_kemasan_detail(){
	       	
	       	$data['mbulan']	= $this->input->post('cBulan');

	       	$data['tahun']	= $this->input->post('cBulan');
	        
	        $data['row']	= $this->model->code("SELECT * FROM v_detail_po_kemas WHERE MONTH(tanggal) = '".$this->input->post('cBulan')."' AND YEAR(tanggal) = '".$this->input->post('cTahun')."' ");
	        
	        $html			= $this->load->view('back-end/stock/cetak/kemasan-laporan-po-detail', $data, true);
				 
	        $pdfFilePath 	= "cetak detail po kemasan periode.pdf";
	 
	        //load mPDF library
	 		$this->m_pdf->pdf->mPDF('','A4','10','ctimesbi','5','5','5','5','5','10','P');
	 		$this->m_pdf->pdf->useOddEven = true ;

	       //generate the PDF from the given html
	        $this->m_pdf->pdf->WriteHTML($html);
	     
	 
	        //download it.
	        $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	    }

	    public function cetak_laporan_kluar_kemasan(){
	       	
	       	$data['mbulan']	= $this->input->post('cBulan');

	       	$data['tahun']	= $this->input->post('cBulan');
	        
	        $data['row']	= $this->model->code("SELECT * FROM v_kluar_kemasan WHERE MONTH(tanggal) = '".$this->input->post('cBulan')."' AND YEAR(tanggal) = '".$this->input->post('cTahun')."' ");
	        
	        $html			= $this->load->view('back-end/stock/cetak/kemasan-laporan-kemasan-kluar-detail', $data, true);
				 
	        $pdfFilePath 	= "cetak detail po kemasan periode.pdf";
	 
	        //load mPDF library
	 		$this->m_pdf->pdf->mPDF('','A4','10','ctimesbi','5','5','5','5','5','10','P');
	 		$this->m_pdf->pdf->useOddEven = true ;

	       //generate the PDF from the given html
	        $this->m_pdf->pdf->WriteHTML($html);
	     
	 
	        //download it.
	        $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	    }
}		
