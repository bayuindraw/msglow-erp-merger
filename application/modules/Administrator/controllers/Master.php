<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	public function __construct(){
		
        parent::__construct();
        ob_start();
		/*error_reporting(0);         */
        $this->load->model('model');
        $this->load->model('relasi');
        $this->load->library('session');
        $this->load->database();
        $this->load->library('m_pdf');
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download'); 
		 
    }
		public  function Date2String($dTgl){
			//return 2012-11-22
			list($cDate,$cMount,$cYear)	= explode("-",$dTgl) ;
			if(strlen($cDate) == 2){
				$dTgl	= $cYear . "-" . $cMount . "-" . $cDate ;
			}
			return $dTgl ; 
		}
		
		public  function String2Date($dTgl){
			//return 22-11-2012  
			list($cYear,$cMount,$cDate)	= explode("-",$dTgl) ;
			if(strlen($cYear) == 4){
				$dTgl	= $cDate . "-" . $cMount . "-" . $cYear ;
			} 
			return $dTgl ; 	
		}
		
		public function TimeStamp() {
			date_default_timezone_set("Asia/Jakarta");
			$Data = date("H:i:s");
			return $Data ;
		}
			
		public function DateStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("d-m-Y");
			return $Data ;
		}  
			
		public function DateTimeStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("Y-m-d h:i:s");
			return $Data ;
		} 

		public function signin($Action=""){
		$data = "" ;
		if($Action == "error"){
			$data['notif'] = "Username / Password Salah" ;
			}
			$msg 			 = 'My secret message';
			$this->load->view('back-end/login',$data);
		
		}

		public function index($Aksi=""){
			
			$dataHeader['title']		= "Beranda - ERP MSGLOW 2019";
			$dataHeader['menu']   		= 'Master';
			$dataHeader['file']   		= 'Index' ;
			$dataHeader['link']			= 'index';
			$data['action'] 			= $Aksi ;

			$data['kemasan']			= $this->model->ViewAsc('v_stock_kemasan_new','id_kemasan');
			$data['produk']				= $this->model->ViewAsc('v_stock_produk','id_barang');

			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/master/home',$data);
			$this->load->view('back-end/container/footer');
		}

		public function index_pic($Aksi=""){
			
			$dataHeader['title']		= "Beranda - ERP MSGLOW 2019";
			$dataHeader['menu']   		= 'Master';
			$dataHeader['file']   		= 'Dashboard Stock Kemasan' ;
			$dataHeader['link']			= 'index';
			$data['action'] 			= $Aksi ;

			$data['kemasan']			= $this->model->ViewAsc('v_stock_kemasan_new','id_kemasan');
			$data['produk']				= $this->model->code("SELECT * FROM v_kluar_kemasan LIMIT 50");
			
			
			$dataHeader['content'] = $this->load->view('back-end/master/home-pic', $data, TRUE);
			$this->load->view('Layout/home',$dataHeader);
		}

		public function index_barang($Aksi=""){
			
			$dataHeader['title']		= "Beranda - ERP MSGLOW 2019";
			$dataHeader['menu']   		= 'Master';
			$dataHeader['file']   		= 'Dashboard Stock Barang Jadi Gudang';
			$dataHeader['link']			= 'index';
			$data['action'] 			= $Aksi ;

			$data['kemasan']			= $this->model->ViewAsc('v_stock_produk','id_barang');

			$dataHeader['content'] = $this->load->view('back-end/master/home-barang', $data, TRUE);
			$this->load->view('Layout/home',$dataHeader);
		}

		public function index_marketing($Aksi=""){
			
			$dataHeader['title']		= "Beranda - ERP MSGLOW 2019";
			$dataHeader['menu']   		= 'Master';
			$dataHeader['file']   		= 'Index' ;
			$dataHeader['link']			= 'index';
			$data['action'] 			= $Aksi ;

			$data['kemasan']			= $this->model->ViewAsc('v_stock_produk','id_barang');
			$data['penjualan']			= $this->model->ViewWhere('penjualan_seller','tgl_jual',date('Y-m-d'));
			

			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/master/home-marketing',$data);
			$this->load->view('back-end/container/footer');
		}

		public function index_accounting($Aksi=""){
			
			$dataHeader['title']		= "Beranda - ERP MSGLOW 2019";
			$dataHeader['menu']   		= 'Master';
			$dataHeader['file']   		= 'Index' ;
			$dataHeader['link']			= 'index';
			$data['action'] 			= $Aksi ;

			$data['kemasan']			= $this->model->ViewAsc('v_stock_produk','id_barang');
			$data['penjualan']			= $this->model->ViewWhere('penjualan_seller','tgl_jual',date('Y-m-d'));
			

			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/master/home-accounting',$data);
			$this->load->view('back-end/container/footer');
		}


		public function Kemasan($Aksi="",$Id=""){
			$dataHeader['title']	= "Data Kemasan - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Kemasan Msglow' ;
			$dataHeader['link']		= 'kemasan';
			$data['action'] 		= $Aksi ; 
			
			$data['row']			= $this->model->ViewAsc('v_kemasan_new','id_kemasan');
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/master/master-kemasan',$data);
			$this->load->view('back-end/container/footer');
		}

		
		public function Produk($Aksi="",$Id=""){
			$dataHeader['title']	= "Data Produk - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Produk Msglow' ;
			$dataHeader['link']		= 'produk';
			$data['action'] 		= $Aksi ; 
			
			$data['row']			= $this->model->ViewWhere('produk','is_delete','0');
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/master/master-barang-jadi',$data);
			$this->load->view('back-end/container/footer');
		}

		
		public function add_produk(){
			
			$this->load->view('back-end/master/add/show_add_produk',$data);
		}

		
		public function edit_produk($Aksi="",$Id=""){

			$data['field'] 			= $this->model->ViewWhere('produk','id_produk',$Id);
			$data['action'] 		= 'edit';
			$this->load->view('back-end/master/add/show_add_produk',$data);
		}

		public function Supplier($Aksi="",$Id=""){
			$dataHeader['title']	= "Data Supplier - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Produk Msglow' ;
			$dataHeader['link']		= 'supplier';
			$data['action'] 		= $Aksi ; 
			
			$data['row']			= $this->model->ViewAsc('supplier','nama_supplier');
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/master/master-supplier',$data);
			$this->load->view('back-end/container/footer');
		}

		public function Pabrik($Aksi="",$Id=""){
			$dataHeader['title']	= "Data Pabrik - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Produk Msglow' ;
			$dataHeader['link']		= 'pabrik';
			$data['action'] 		= $Aksi ; 
			
			$data['row']			= $this->model->ViewAsc('factory','nama_factory');
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/master/master-pabrik',$data);
			$this->load->view('back-end/container/footer');
		}

		public function seller($Aksi="",$Id=""){
			$dataHeader['title']	= "Data Seller - ERP MSGLOW 2019";
			$dataHeader['menu']   	= 'Master';
			$dataHeader['file']   	= 'Produk Msglow' ;
			$dataHeader['link']		= 'pabrik';
			$data['action'] 		= $Aksi ; 
			
			$data['row']			= $this->model->ViewAsc('member','id_member');
			
			$this->load->view('back-end/container/header',$dataHeader);
			$this->load->view('back-end/master/master-seller',$data);
			$this->load->view('back-end/container/footer');
		}

}
