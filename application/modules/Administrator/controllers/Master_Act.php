<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_Act extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		ob_start();
		//error_reporting(0);     
		//MenLoad model yang berada di Folder Model dan namany model
		$this->load->model('model');
		// Meload Library session 
		$this->load->library('session');
		//Meload database
		$this->load->database();
		//Meload url 
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}
	public  function Date2String($dTgl)
	{
		//return 2012-11-22
		list($cDate, $cMount, $cYear)	= explode("-", $dTgl);
		if (strlen($cDate) == 2) {
			$dTgl	= $cYear . "-" . $cMount . "-" . $cDate;
		}
		return $dTgl;
	}

	public  function String2Date($dTgl)
	{
		//return 22-11-2012  
		list($cYear, $cMount, $cDate)	= explode("-", $dTgl);
		if (strlen($cYear) == 4) {
			$dTgl	= $cDate . "-" . $cMount . "-" . $cYear;
		}
		return $dTgl;
	}

	public function TimeStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("H:i:s");
		return $Data;
	}

	public function DateStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("d-m-Y");
		return $Data;
	}

	public function DateTimeStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("Y-m-d h:i:s");
		return $Data;
	}

	function JudulSeo($s)
	{
		$s = trim($s);
		$c = array(' ');
		$d = array(
			'-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '"',
			'{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&',
			'*', '=', '?', '+'
		);

		$s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d

		$s = strtolower(str_replace($c, '-', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
		return $s;
	}

	public function signin()
	{

		$cUsername 	= $this->input->post('username');
		$cPassword	= $this->input->post('pass');

		$cModel		= $this->model->Login($cUsername, $cPassword);
		if ($cModel->num_rows() > 0) {
			foreach ($cModel->result_array() as $key => $vaUser) {
				$cIdUser 	=	$vaUser['id_user'];
				$cNama 		=	$vaUser['nama'];
				$cUser 		=	$vaUser['username'];
				$cLevel 	=	$vaUser['level'];
				$cIdPegawai = 	$vaUser['id_pegawai'];
			}
			echo "sukses-" . $cLevel . "";
			$this->session->set_userdata('isLogin', 'True');
			$this->session->set_userdata('id_user', $cIdUser);
			$this->session->set_userdata('nama', $cNama);
			$this->session->set_userdata('level', $cLevel);
			$this->session->set_userdata('user', $cUser);
			$this->session->set_userdata('pegawai', $cIdPegawai);
			if ($cLevel == '1') {
				redirect(site_url('Administrator/Master/index'));
			} elseif ($cLevel == '4') {
				redirect(site_url('Administrator/Master/index_pic'));
			} elseif ($cLevel == '5') {
				redirect(site_url('Administrator/Master/index_barang'));
			} elseif ($cLevel == '6') {
				redirect(site_url('Administrator/Master/index_marketing'));
			} elseif ($cLevel == '7') {
				redirect(site_url('Administrator/Master/index_accounting'));
			}
		} else {
			redirect(site_url('Administrator/Master/signin'));
		}
	}


	public function logout()
	{
		$this->session->sess_destroy();
		redirect(site_url('login.html'));
	}

	public function secure()
	{
		$data = array(
			'key_open' 		=> '0',
		);
		$this->session->sess_destroy();
		//$this->model->Update('scr_loginkey','username',$this->session->userdata('nama'),$data);
		$this->model->Update('scr_webapp', 'username', $this->session->userdata('nama'), $data);
		redirect(site_url('Administrator/master/signinsecure'));
	}

	public function Kemasan($Type = "", $id = "")
	{


		if ($Type == "simpan") {

			$data = array(
				'kode_kemasan' 		=> $this->input->post('cKodeKemasan'),
				'nama_kemasan' 		=> $this->input->post('cNama'),
				'is_created'		=> $this->session->userdata('nama'),
				'created_date'		=> $this->DateTimeStamp(),
				'is_delete'			=> '0'
			);

			$this->model->Insert('kemasan', $data);
			redirect(site_url('Administrator/Master/Kemasan/I'));
		} elseif ($Type == "ubah") {

			$data = array(
				'kode_kemasan' 		=> $this->input->post('cKodeKemasan'),
				'nama_kemasan' 		=> $this->input->post('cNama'),
				'is_edit'			=> $this->session->userdata('nama'),
				'edit_date'			=> $this->DateTimeStamp(),
			);

			$this->model->Update('kemasan', 'id_kemasan', $id, $data);
			redirect(site_url('Administrator/Master/Kemasan/U'));
		} elseif ($Type == "hapus") {

			$data = array(
				'is_delete'			=> $this->session->userdata('nama'),
				'delete_date'		=> $this->DateTimeStamp(),
			);

			$this->model->Update('kemasan', 'id_kemasan', $id, $data);
			redirect(site_url('Administrator/Master/Kemasan/D'));
		}
	}

	public function Produk($Type = "", $id = "")
	{


		if ($Type == "simpan") {

			$data = array(
				'kode_produk' 		=> $this->input->post('cKodeProduk'),
				'nama_produk' 		=> $this->input->post('cNamaProduk'),
				'harga' 			=> $this->input->post('cHarga'),
				'is_created'		=> $this->session->userdata('nama'),
				'created_date'		=> $this->DateTimeStamp(),
				'is_delete'			=> '0'
			);

			$this->model->Insert('produk', $data);
			redirect(site_url('Administrator/Master/Produk/I'));
		} elseif ($Type == "ubah") {

			$data = array(
				'kode_produk' 		=> $this->input->post('cKodeProduk'),
				'nama_produk' 		=> $this->input->post('cNamaProduk'),
				'harga' 			=> $this->input->post('cHarga'),
				'is_edit'			=> $this->session->userdata('nama'),
				'edit_date'			=> $this->DateTimeStamp(),
			);

			$this->model->Update('produk', 'id_produk', $id, $data);
			redirect(site_url('Administrator/Master/Produk/U'));
		} elseif ($Type == "hapus") {

			$data = array(
				'is_delete'			=> $this->session->userdata('nama'),
				'delete_date'		=> $this->DateTimeStamp(),
			);

			$this->model->Update('produk', 'id_produk', $id, $data);
			redirect(site_url('Administrator/Master/Produk/D'));
		}
	}
}
