<?php

require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
	//use PhpOffice\PhpSpreadsheet\Style\Border;

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {
	public function __construct(){
		
		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('model');
		$this->load->model('relasi');
		$this->load->library('session');
		$this->load->database();
		$this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download'); 
		
	}
	
	public function export_stock_monthly2(){
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"test.xls\"");
		//$data = array('depan', 'satu');
		//echo implode("\t", $data) . "\n";
		ob_end_clean();
	}
	
	public function export_stock_monthly($bulan, $tahun){ 
		ob_end_clean();
		$arrbln = array('01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember');
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();
		
		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		
		
		
				//$sheet->getStyle('C4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f8cbad');
		$arrdata = $this->model->Query("SELECT *, C.jumlah, C.rusak FROM kemasan A LEFT JOIN produk B ON B.id_produk = A.produk LEFT JOIN tb_stock_kemasan_history C ON C.id_barang = A.id_kemasan AND C.tanggal = '".$tahun."-".$bulan."-01' LEFT JOIN m_category_product D ON D.category_product_id = B.produk_kategori ORDER BY B.produk_kategori, A.produk");
		$arrcategoryproduct = $this->model->Query("SELECT * FROM m_category_product");
		$arrterima = $this->model->Query("SELECT SUM(jumlah) as jumlah, SUM(rusak) as rusak, tgl_terima, id_barang FROM terima_kemasan  GROUP BY id_barang, tgl_terima");
		foreach($arrterima as $keyterima => $valueterima){
			$dataterima[$valueterima['id_barang']][$valueterima['tgl_terima']] = $valueterima;
		}
		$arrterima_print = $this->model->Query("SELECT SUM(jumlah) as jumlah, SUM(rusak) as rusak, tgl_terima, id_barang FROM terima_print  GROUP BY id_barang, tgl_terima");
		foreach($arrterima_print as $keyterima_print => $valueterima_print){
			$dataterima_print[$valueterima_print['id_barang']][$valueterima_print['tgl_terima']] = $valueterima_print;
		}
		$arrkeluar = $this->model->Query("SELECT SUM(jumlah) as jumlah, tanggal, id_barang FROM tb_detail_kluar_kemasan GROUP BY id_barang, tanggal");
		foreach($arrkeluar as $keykeluar => $valuekeluar){
			$datakeluar[$valuekeluar['id_barang']][$valuekeluar['tanggal']] = $valuekeluar;
		}
		
		$abjad = 'B';
		$awal = 5;
		$sheet->mergeCells($abjad.$awal.':'.$abjad++.($awal+1));
		$sheet->mergeCells($abjad.$awal.':'.$abjad++.($awal+1));
		$sheet->mergeCells($abjad.$awal.':'.$abjad++.($awal+1));
		$sheet->mergeCells($abjad.$awal.':'.$abjad++.($awal+1));
		
		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++.$awal, 'NAMA BARANG');
		$sheet->setCellValue($abjad++.$awal, 'KLASIFIKASI');
		$sheet->setCellValue($abjad++.$awal, 'STOK AWAL');
		$sheet->setCellValue($abjad++.$awal, 'STOK BARANG RUSAK AWAL');
		
		$nawal = $awal;
		
		$nJumlahHari = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
		for($i=1;$i<=$nJumlahHari;$i++){
			if($i > 9){
				$cNol = "";
			}else{
				$cNol = "0";
			}
			$date = $tahun."-".$bulan."-".$cNol.$i;
			$no = 0;
			$titik_awal = $abjad;
			
			$sheet->setCellValue($abjad++.($awal+1), 'BARANG DATANG');
			$sheet->setCellValue($abjad++.($awal+1), 'BARANG KELUAR');
			$sheet->getStyle($titik_awal.$awal.':'.$abjad.$awal)->applyFromArray($styleArray);
			$sheet->mergeCells($titik_awal.$awal.':'.$abjad.$awal);
			$sheet->setCellValue($titik_awal.$awal, substr($arrbln[$bulan], 0, 3).'-'.(int)($cNol.$i));
			
			$sheet->setCellValue($abjad++.($awal+1), 'BARANG RUSAK');
			
		}
		
		$sheet->mergeCells($abjad.$awal.':'.$abjad.($awal+1));
		$sheet->setCellValue($abjad++.$awal, 'TOTAL BARANG DATANG');
		$sheet->mergeCells($abjad.$awal.':'.$abjad.($awal+1));
		$sheet->setCellValue($abjad++.$awal, 'TOTAL BARANG KELUAR');
		$sheet->mergeCells($abjad.$awal.':'.$abjad.($awal+1));
		$sheet->setCellValue($abjad.$awal, 'SISA STOCK KEMASAN BAGUS');
		
		$sheet->mergeCells($abjad_awal.($awal - 3).':'.$abjad.($awal - 3));
		$sheet->getStyle($abjad_awal.($awal - 3).':'.$abjad_awal.($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal.($awal - 3).':'.$abjad_awal.($awal - 2))->getFont()->setBold( true );
		$sheet->getStyle($abjad_awal.($awal - 3).':'.$abjad_awal.($awal - 3))->getFont()->setSize(28);
		$sheet->setCellValue($abjad_awal.($awal - 3), 'LAPORAN STOK BAHAN KEMAS MS GLOW');
		$sheet->getStyle($abjad_awal.($awal - 2).':'.$abjad_awal.($awal - 2))->getFont()->setSize(20);
		$sheet->mergeCells($abjad_awal.($awal - 2).':'.$abjad.($awal - 2));
		$sheet->setCellValue($abjad_awal.($awal - 2), 'PERIODE '.strtoupper($arrbln[$bulan]));
		$sheet->setCellValue($abjad.($awal - 1), date('d/m/Y H.i'));
		
		
		//$sheet->getStyle('A1:B2')->applyFromArray($styleArray);
		//$sheet ->getStyle($abjad_awal.$awal.':'.$abjad.($awal+1))->applyFromArray($styleArray);
		$sheet->freezePane('F7');
		$sheet->getSheetView()->setZoomScale(70);
		$sheet->getRowDimension($awal+1)->setRowHeight(30);
		$irow = 1;
		$wrap_awal = "";
		for( $x = $abjad_awal; ; $x++) {
			
			$sheet->getStyle($x.$awal.':'.$x.($awal+1))->applyFromArray($styleArray);
			if($irow <= 2){
				$sheet->getColumnDimension($x)->setAutoSize(true);
			}else{
				if($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = $wrap_akhir;
				$wrap_akhir = $x;
			}
			if( $x == $abjad) break;
			$irow++; 
		}
		$sheet->getStyle($wrap_awal.'5:'.$wrap_akhir.'6')->getAlignment()->setWrapText(true);
		$sheet->getStyle($abjad_awal.$awal.':'.$abjad.($awal+1))->applyFromArray($style);
		$sheet->getStyle($abjad_awal.$awal.':'.$abjad.($awal+1))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal.$awal.':'.$abjad.($awal+1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$nawal++;
		$produk_global = "";
		$produk_merge_akhir = "";
		
		foreach($arrdata as $key => $value){
			$nabjad = 'B';  
			$paling_awal = $nabjad;
			$nawal++;
			if($produk_merge_awal == ""){
				$produk_merge_awal = $nawal;
			}
			
			$sheet->setCellValue($nabjad++.$nawal, $value['nama_kemasan']);
			//$xx = 1;
			if($rgb == "") $rgb = 'e9f7df';
			if($produk_global != $value['nama_produk']){
				//$xx = 0;
				$xabjad = $nabjad;
				if($produk_merge_akhir != ""){
					
					$sheet->mergeCells($nabjad.$produk_merge_awal.':'.$nabjad.$produk_merge_akhir);
					$sheet->getStyle($nabjad.$produk_merge_awal.':'.$nabjad.$produk_merge_awal)->applyFromArray($style);
					$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$wrap_akhir.$produk_merge_akhir)->applyFromArray($styleArray);
					$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$wrap_akhir_before.$produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
					//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
				}
				$produk_merge_awal = $nawal;
			}
			$produk_global = $value['nama_produk'];
			$sheet->setCellValue($nabjad++.$nawal, $value['nama_produk']);
			$sheet->setCellValue($nabjad++.$nawal, $value['jumlah']);
			$sheet->setCellValue($nabjad++.$nawal, $value['rusak']);
			$jumlah = $value['jumlah'];
			$nJumlahHari = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
			$jmldatang = 0;
			$jmlkeluar = 0;
			for($i=1;$i<=$nJumlahHari;$i++){
				if($i > 9){
					$cNol = "";
				}else{
					$cNol = "0";
				}
				$date = $tahun."-".$bulan."-".$cNol.$i;
				$no = 0;
				
				$sheet->setCellValue($nabjad++.$nawal, ($dataterima[$value['id_kemasan']][$date]['jumlah'] != "" || $dataterima_print[$value['id_kemasan']][$date]['jumlah'] != "")?((($dataterima[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima[$value['id_kemasan']][$date]['jumlah']) + (($dataterima_print[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima_print[$value['id_kemasan']][$date]['jumlah'])):"");
				$sheet->setCellValue($nabjad++.$nawal, $datakeluar[$value['id_kemasan']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++.$nawal, $dataterima[$value['id_kemasan']][$date]['rusak']);
				$jmldatang = $jmldatang + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima[$value['id_kemasan']][$date]['jumlah']) + (($dataterima_print[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima_print[$value['id_kemasan']][$date]['jumlah']);
				$jmlkeluar = $jmlkeluar + (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']);
				//$jumlah = $jumlah + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima[$value['id_kemasan']][$date]['jumlah']) - (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']); 
			}
			$jumlah = $jumlah + $jmldatang - $jmlkeluar;
			$sheet->setCellValue($nabjad++.$nawal, $jmldatang);
			$sheet->setCellValue($nabjad++.$nawal, $jmlkeluar);
			$sheet->setCellValue($nabjad.$nawal, $jumlah);
			$produk_merge_akhir = $nawal;
			for( $x = $abjad_awal; ; $x++) {
				$sheet->getStyle($x.$nawal.':'.$x.$nawal)->applyFromArray($styleArray_thin);
				if( $x == $nabjad) break;
			}
			$rgb = $value['category_product_rgb'];
			if($rgb == "") $rgb = 'e9f7df';
			if($jumlah < 20000){
				$sheet->getStyle($nabjad.$nawal.':'.$nabjad.$nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
			}else{
				$sheet->getStyle($nabjad.$nawal.':'.$nabjad.$nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
			}
		}
		//if($xx == 1){
		$sheet->mergeCells($xabjad.$produk_merge_awal.':'.$xabjad.$produk_merge_akhir);
		$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$wrap_akhir.$produk_merge_akhir)->applyFromArray($styleArray2);
		$sheet->getStyle($xabjad.$produk_merge_awal.':'.$xabjad.$produk_merge_awal)->applyFromArray($style);
		$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$wrap_akhir_before.$produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
			//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
		//}
		
		$nawal++; 
		$nawal++;
		$sheet->setCellValue($abjad_awal.$nawal++, 'NB:');
		$abjad_awal2 = $abjad_awal;
		$sheet->getStyle($abjad_awal2.$nawal.':'.$abjad_awal2++.$nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
		$sheet->setCellValue($abjad_awal2.$nawal++, 'STOK DIBAWAH 20.000 PCS');
		foreach($arrcategoryproduct as $keyproduct => $valueproduct){
			$abjad_awal2 = $abjad_awal;
			$sheet->getStyle($abjad_awal2.$nawal.':'.$abjad_awal2++.$nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($valueproduct['category_product_rgb']);
			$sheet->setCellValue($abjad_awal2.$nawal++, $valueproduct['category_product_name']);
		}
		
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"STOCK KEMASAN MSGLOW ".strtoupper($arrbln[$bulan]).".xls\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output'); 
	}
	
	
	public  function Date2String($dTgl){
			//return 2012-11-22
		list($cDate,$cMount,$cYear)	= explode("-",$dTgl) ;
		if(strlen($cDate) == 2){
			$dTgl	= $cYear . "-" . $cMount . "-" . $cDate ;
		}
		return $dTgl ; 
	}
	
	public  function String2Date($dTgl){
			//return 22-11-2012  
		list($cYear,$cMount,$cDate)	= explode("-",$dTgl) ;
		if(strlen($cYear) == 4){
			$dTgl	= $cDate . "-" . $cMount . "-" . $cYear ;
		} 
		return $dTgl ; 	
	}
	
	public function TimeStamp() {
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("H:i:s");
		return $Data ;
	}
	
	public function DateStamp() {
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("d-m-Y");
		return $Data ;
	}  
	
	public function DateTimeStamp() {
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("Y-m-d h:i:s");
		return $Data ;
	} 

	public function signin($Action=""){
		$data = "" ;
		if($Action == "error"){
			$data['notif'] = "Username / Password Salah";
		}
		$msg 			 = 'My secret message';
		$this->load->view('back-end/login-secure',$data);
		
	}

	public function signinsecure($Action=""){
		$data = "" ;
		if($Action == "error"){
			$data['notif'] = "Username / Password Salah";
		}
		$msg 			 = 'My secret message';
		$this->load->view('back-end/login',$data);
		
	}
	
	public function po_kemasan_history($Aksi=""){
		$dataHeader['title']		= "Laporan Pengiriman Kemasan  - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'History PO';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['row']				= $this->model->View('po_kemasan_draft A LEFT JOIN supplier B ON B.id_supplier = A.supplier','A.tanggal');
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-history-po', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function po_kemasan_history_print($Aksi=""){
		$dataHeader['title']		= "Laporan Pengiriman Kemasan  - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'History PO Print';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['row']				= $this->model->View('po_print_draft A LEFT JOIN supplier B ON B.id_supplier = A.supplier','A.tanggal');
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-history-po-print', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function po_print_history($Aksi=""){
		$dataHeader['title']		= "Laporan Pengiriman Kemasan  - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'List PO' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['row']				= $this->model->View('po_print_draft A LEFT JOIN supplier B ON B.id_supplier = A.supplier','A.tanggal');
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-history-print-po', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function po_kemasan_history_detail($id = "")
	{
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Detail History PO';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi;
		$data['kodepo']				= $this->kodePO();
		$data['row1']				= $this->model->ViewWhere('v_detail_po_kemas_draft', 'kode_pb', $id);
		$data['row2']				= $this->model->ViewWhere('v_detail_po_kemas', 'kode_pb', $id);
		$data['id'] = $id;
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-history-po-detail', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function po_kemasan_history_detail_print($id = "")
	{
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'List PO';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi;
		$data['kodepo']				= $this->kodePO();
		$data['row1']				= $this->model->ViewWhere('v_detail_po_print_draft', 'kode_pb', $id);
		$data['row2']				= $this->model->ViewWhere('v_detail_po_print', 'kode_pb', $id);
		$data['id'] = $id;
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-history-po-detail-print', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function export_pdf_po_kemasan($id = "")
	{
		$data['row1'] = $this->model->ViewWhere('v_detail_po_kemas_draft', 'kode_pb', $id);
		$this->load->view('back-end/kemasan/export-pdf-po', $data);
	}
	
	public function export_pdf_po_kemasan_real($id = "")
	{
		$data['row1'] = $this->model->ViewWhere('v_detail_po_kemas', 'kode_pb', $id);
		$this->load->view('back-end/kemasan/export-pdf-po', $data);
	}

	public function export_pdf_print($id = "")
	{
		$data['row1'] = $this->model->ViewWhere('v_detail_po_print_draft', 'kode_pb', $id);
		$this->load->view('back-end/kemasan/export-pdf-print', $data);
	}
	
	public function po_print_history_detail($id=""){
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'List PO' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['kodepo']				= $this->kodePO();
		$data['row1']				= $this->model->ViewWhere('v_detail_po_print_draft','kode_pb',$id);
		$data['row2']				= $this->model->ViewWhere('v_detail_po_print','kode_pb',$id);

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-history-po-detail', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function stock_opname_kemasan($Aksi=""){
		
		$dataHeader['title']		= "Stock Opname Kemasan ".date('d-m-Y h:i:s')."";
		$dataHeader['menu']   		= 'Stock Opname Kemasan';
		$dataHeader['file']   		= 'Stock Kemasan' ;
		$dataHeader['link']			= 'stock_opname_kemasan';
		$data['action'] 			= $Aksi ;

		$data['row']				= $this->model->ViewAsc('v_stock_kemasan_new','id_kemasan');
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-list-stock', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function kartu_stock_kemasan($Aksi=""){
		
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Kartu Stock Kemasan' ;
		$dataHeader['link']			= 'kartu_stock_kemasan';
		$data['action'] 			= $Aksi ;

		if(empty($Aksi)){
			$data['bulan']			= "";
			$data['tahun']			= "";
			$data['row']			= $this->model->code("SELECT * FROM v_stock_kemasan WHERE tanggal = '2017-01-01' ");
		}else{
			$data['bulan']			= $this->input->post('cBulan');
			$data['tahun']			= $this->input->post('cTahun');
			$data['row']			= $this->model->ViewAsc('v_stock_kemasan','id_kemasan');
		}

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kartu-stock-kemasan', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function get_kemasan_supplier(){
		$id			= $this->input->post('id');
		$arrdata = $this->model->code("SELECT * FROM supplier A LEFT JOIN kemasan B ON B.supplier = A.kode_sup WHERE A.id_supplier = '$id'");
		$html = "<option></option>";
		foreach($arrdata as $index => $value){
			$html = $html.'<option value="'.$value['id_kemasan'].'">'.$value['nama_kemasan'].' ('.$value['supplier'].')</option>';
		}
		echo $html;
	}
	
	public function tampil_stock(){
		$data['bulan']			= $this->input->post('bulan');
		$data['tahun']			= $this->input->post('tahun');
		$data['row']			= $this->model->code("SELECT * FROM v_stock_kemasan_new");			
		$this->load->view('back-end/stock/table/show_kartu_stock',$data);
		
	}

	public function tampil_stock_kemasan_real($idBarang="",$Bulan="",$Tahun=""){
		$data['idbarang']		= $idBarang;
		$data['bulan']			= $Bulan;
		$data['tahun']			= $Tahun;		

		$this->load->view('back-end/kemasan/table/show_kartu_stock_detail',$data);
		
	}


	public function show_kartu_stock_kemasan($bulan="",$tahun="",$id=""){

		$data['bulan']				= $bulan;
		$data['tahun']				= $tahun;
		$data['pro']				= $id;
		$data['produk']				= $this->model->ViewWhere('kemasan','id_kemasan',$id);

		$this->load->view('back-end/kemasan/add/show_kartu_stock_kemasan',$data);

	}

	public function kodePO(){

		$cFormatTahun  = substr(date('Y'),2,2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWPOKM-";

		$dbDate	=	$this->db->query("SELECT COUNT(id_po_kemasan) as JumlahTransaksi FROM po_kemasan_draft WHERE bulan = '".date('m')."' AND tahun = '". $cFormatTahun."'");
		if($dbDate->num_rows() > 0){
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi']+1;
			}
		}else{
			$nJumlahTransaksi = 1;
		}
		
		$panjang = strlen($nJumlahTransaksi);
		if($panjang == 1){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'00'.$nJumlahTransaksi;
		}elseif($panjang == 2){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'0'.$nJumlahTransaksi;
		}elseif($panjang == 3){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.$nJumlahTransaksi;
		}
		

		return $cKode;
		
	}
	
	public function kodePOprint(){

		$cFormatTahun  = substr(date('Y'),2,2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWPRINT-";

		$dbDate	=	$this->db->query("SELECT COUNT(id_po_kemasan) as JumlahTransaksi FROM po_print_draft WHERE bulan = '".date('m')."' AND tahun = '". $cFormatTahun."'");
		if($dbDate->num_rows() > 0){
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi']+1;
			}
		}else{
			$nJumlahTransaksi = 1;
		}
		
		$panjang = strlen($nJumlahTransaksi);
		if($panjang == 1){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'00'.$nJumlahTransaksi;
		}elseif($panjang == 2){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'0'.$nJumlahTransaksi;
		}elseif($panjang == 3){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.$nJumlahTransaksi;
		}
		

		return $cKode;
		
	}

	public function po_kemasan($Aksi=""){
		
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'PO Kemasan' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['kodepo']				= $this->kodePO();
		$data['row']				= $this->model->ViewWhere('v_detail_po_kemas_draft','kode_pb',$this->kodePO());

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-input-po', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function edit_po_kemasan($Aksi = "")
	{
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Edit Po Kemasan';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi;

		$query						= $this->model->ViewWhere('po_kemasan_draft', 'id_po_kemasan', $Aksi);

		foreach ($query as $key => $vaData) {
			$kode 		= $vaData['kode_po'];
			$supplier 	= $vaData['supplier'];
			$tanggal 	= $vaData['tanggal'];
		}
		$data['id_pokemasan']		= $Aksi;
		$data['tanggal']			= $tanggal;
		$data['supplier']			= $supplier;
		$data['kode_pb']			= $kode;
		$data['row']				= $this->model->ViewWhere('v_detail_po_kemas_draft', 'kode_pb', $kode);
		$data['num_rows']			= $this->model->Codes("SELECT * FROM v_detail_po_kemas_draft WHERE kode_pb = '".$kode."' ")->num_rows();
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-edit-po', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function edit_po_kemasan_print($Aksi = "")
	{

		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Edit Po Kemasan Print';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi;

		$query						= $this->model->ViewWhere('po_print_draft', 'id_po_kemasan', $Aksi);

		foreach ($query as $key => $vaData) {
			$kode 		= $vaData['kode_po'];
			$supplier 	= $vaData['supplier'];
			$tanggal 	= $vaData['tanggal'];
		}
		$data['id_pokemasan']		= $Aksi;
		$data['tanggal']			= $tanggal;
		$data['supplier']			= $supplier;
		$data['kode_pb']			= $kode;
		$data['row']				= $this->model->ViewWhere('v_detail_po_print_draft', 'kode_pb', $kode);
		$data['num_rows']			= $this->model->Codes("SELECT * FROM v_detail_po_print_draft WHERE kode_pb = '".$kode."' ")->num_rows();
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-edit-po-print', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function tampil_history_approve($Aksi = ""){
		
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Edit Po Kemasan';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi;

		$query						= $this->model->ViewWhere('po_kemasan_draft', 'id_po_kemasan', $Aksi);

		foreach ($query as $key => $vaData) {
			$kode 		= $vaData['kode_po'];
			$supplier 	= $vaData['supplier'];
			$tanggal 	= $vaData['tanggal'];
		}
		$data['id_pokemasan']		= $Aksi;
		$data['tanggal']			= $tanggal;
		$data['supplier']			= $supplier;
		$data['kode_pb']			= $kode;
		$data['row']				= $this->model->ViewWhere('v_detail_po_kemas_draft', 'kode_pb', $kode);
		$data['num_rows']			= $this->model->Codes("SELECT * FROM v_detail_po_kemas_draft WHERE kode_pb = '".$kode."' ")->num_rows();
		
		$this->load->view('back-end/stock/table/tampil-history-approve',$data);
	}
	
	public function tampil_history_approve_print($Aksi = "")
	{

		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Edit Po Kemasan Print';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi;

		$query						= $this->model->ViewWhere('po_print_draft', 'id_po_kemasan', $Aksi);

		foreach ($query as $key => $vaData) {
			$kode 		= $vaData['kode_po'];
			$supplier 	= $vaData['supplier'];
			$tanggal 	= $vaData['tanggal'];
		}
		$data['id_pokemasan']		= $Aksi;
		$data['tanggal']			= $tanggal;
		$data['supplier']			= $supplier;
		$data['kode_pb']			= $kode;
		$data['row']				= $this->model->ViewWhere('v_detail_po_print_draft', 'kode_pb', $kode);
		$data['num_rows']			= $this->model->Codes("SELECT * FROM v_detail_po_print_draft WHERE kode_pb = '".$kode."' ")->num_rows();
		
		$this->load->view('back-end/stock/table/tampil-history-approve-print',$data);
	}
	
	public function po_kemasan_print($Aksi=""){
		
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'PO Print' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['kodepo']				= $this->kodePOprint();
		$data['row']				= $this->model->ViewWhere('v_detail_po_print_draft','kode_pb',$this->kodePOprint());

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-input-po-print', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function po_kemasan_pic($Aksi=""){
		$dataHeader['title']		= "Laporan Pengiriman Kemasan  - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'listpokemasan' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['row']				= $this->model->View('po_kemasan_draft A LEFT JOIN supplier B ON B.id_supplier = A.supplier WHERE status_approve IS NULL','A.tanggal');
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-list-po-draft', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function po_print_pic($Aksi=""){
		$dataHeader['title']		= "Laporan Pengiriman Kemasan  - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'listpokemasan';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['row']				= $this->model->View('po_print_draft A LEFT JOIN supplier B ON B.id_supplier = A.supplier WHERE status_approve IS NULL','A.tanggal');
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-list-po-print-draft', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function po_produk_pic($Aksi=""){
		$dataHeader['title']		= "Laporan Pengiriman Kemasan  - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'listproduk' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['row']				= $this->model->View('po_produk_draft A LEFT JOIN factory B ON B.id_factory = A.factory WHERE A.status_approve IS NULL OR A.status_approve = ""','tanggal');
		
		$dataHeader['content'] = $this->load->view('back-end/stock/produk-list-po-draft', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	
	public function list_po_print_draft($id=""){
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'po_kemasan' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi; 
		$data['kodepo']				= $id;
		$data['row']				= $this->model->ViewWhere('po_print_draft A LEFT JOIN v_detail_po_print_draft B ON B.kode_pb = A.kode_po','A.id_po_kemasan',$data['kodepo']); 
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-detail-po-print-draft', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function list_po_draft($id=""){
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'po_kemasan' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi; 
		$data['kodepo']				= $id;
		$data['row']				= $this->model->ViewWhere('po_kemasan_draft A LEFT JOIN v_detail_po_kemas_draft B ON B.kode_pb = A.kode_po','A.id_po_kemasan',$data['kodepo']); 
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-detail-po-draft', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function show_kemasan(){

		$data['produk']				= $this->model->ViewAsc('produk','id_produk');

		$this->load->view('back-end/kemasan/add/show_produk',$data);

	}

	public function po_kemasan_active($Aksi=""){
		
		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Index' ;
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi ;

		$data['row']				= $this->model->ViewAsc('v_po_kemasan','id_po_kemasan');

		
		$this->load->view('back-end/container/header',$dataHeader);
		$this->load->view('back-end/kemasan/kemasan-list-active',$data);
		$this->load->view('back-end/container/footer');
	}

	public function po_kemasan_active_pic($Aksi=""){
		
		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'PO Kemasan Active' ;
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi ;

		$data['row']				= $this->model->ViewWhere('v_po_kemasan','active','Y');

		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-list-active-pic', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function po_print_active_pic($Aksi=""){
		
		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'PO Print Active' ;
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi ;

		$data['row']				= $this->model->ViewWhere('v_po_print','active','Y');

		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-list-print-active-pic', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function po_kemasan_active_pic_tidak($Aksi=""){
		
		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'PO Kemasan Selesai' ;
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi ;

		$data['row']				= $this->model->ViewWhere('v_po_kemasan','active','T');

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-list-active-pic', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function detail_po_active($Aksi=""){
		
		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Index' ;
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi ;

		$data['row']				= $this->model->ViewWhere('v_detail_po_kemas','kode_pb',$Aksi);
		$data['bayar']				= $this->model->ViewWhere('bayar_kemasan','kode_po',$Aksi);
		$data['terima']				= $this->model->ViewWhere('v_terima_kemasan','kode_po',$Aksi);

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-detail-po-active', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function bayar_kemasan($Aksi=""){
		
		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Index' ;
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi ;

		$data['po']					= $this->model->ViewWhere('v_po_kemasan','kode_po',$Aksi);
		$data['row']				= $this->model->ViewWhere('v_detail_po_kemas','kode_pb',$Aksi);
		$data['bayar']				= $this->model->ViewWhere('bayar_kemasan','kode_po',$Aksi);

		
		$this->load->view('back-end/container/header',$dataHeader);
		$this->load->view('back-end/kemasan/kemasan-bayar-kemasan',$data);
		$this->load->view('back-end/container/footer');
	}

	public function terima_kemasan($Aksi=""){
		
		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Penerimaan PO' ;
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi ;

		$data['po']					= $this->model->ViewWhere('v_po_kemasan','kode_po',$Aksi);
		$data['row']				= $this->model->ViewWhere('v_detail_po_kemas','kode_pb',$Aksi);
		$data['bayar']				= $this->model->ViewWhere('v_terima_kemasan','kode_po',$Aksi);
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-terima-kemasan', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function terima_print($Aksi=""){
		
		$dataHeader['title']		= "Purchase Order Active - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Kirim & Terima Print' ;
		$dataHeader['link']			= 'po_produk_active';
		$data['action'] 			= $Aksi ;

		$data['po']					= $this->model->ViewWhere('v_po_print','kode_po',$Aksi);
		$data['row']				= $this->model->ViewWhere('v_detail_po_print','kode_pb',$Aksi);
		$data['bayar']				= $this->model->ViewWhere('v_terima_print','kode_po',$Aksi);

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-terima-print', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function kodePengeluaran(){

		$cFormatTahun  = substr(date('Y'),2,2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSG/PIC/OUT/";

		$dbDate	=	$this->db->query("SELECT COUNT(id_kluar_kemasan) as JumlahTransaksi FROM kluar_kemasan WHERE bulan = '".date('m')."'");
		if($dbDate->num_rows() > 0){
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi']+1;
			}
		}else{
			$nJumlahTransaksi = 1;
		}
		
		$panjang = strlen($nJumlahTransaksi);
		if($panjang == 1){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'00'.$nJumlahTransaksi;
		}elseif($panjang == 2){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'0'.$nJumlahTransaksi;
		}elseif($panjang == 3){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.$nJumlahTransaksi;
		}
		

		return $cKode;
		
	}

	public function edit_pengeluaran_kemasan($Aksi=""){
		
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Keluar Kemasan' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		
		$data['kluar']				= $this->model->ViewWhere('kluar_kemasan','id_kluar_kemasan',$Aksi);
		$query						= $data['kluar'];
		foreach ($query as $key => $vaData) {
			$kode 		= $vaData['kode_kluar'];
			$factory 	= $vaData['pabrik'];
			$tanggal 	= $vaData['tgl_kirim'];
			$idKluar 	= $vaData['id_kluar_kemasan'];
		}
		$data['idkluar']			= $Aksi;
		$data['tanggal']			= $tanggal;
		$data['factory']			= $factory;
		$data['kodepo']				= $kode;
		$data['row']				= $this->model->ViewWhere('v_kluar_kemasan','kode_kluar',$kode);

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-edit-pengeluaran', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function edit_pengeluaran_print($id="", $Aksi=""){
		
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Keluar Kemasan' ;
		$dataHeader['link']			= 'po_produk';
		$Aksi = str_replace('%7C', '/', str_replace('|', '/', $Aksi));
		$kode = $Aksi;
		if($Aksi == ""){ 
			$kode = $this->kodePengeluaran();
			$Aksi = $kode;
		}
		$data['action'] 			= $Aksi ;
		
		$data['kluar']				= $this->model->ViewWhere('kluar_kemasan','kode_kluar',$Aksi,'kode_po',$id);
		$query						= $data['kluar'];
		foreach ($query as $key => $vaData) {
			$kode 		= $vaData['kode_kluar'];
			$factory 	= $vaData['pabrik'];
			$tanggal 	= $vaData['tgl_kirim'];
			$idKluar 	= $vaData['id_kluar_kemasan'];
		}
		$data['idkluar']			= $Aksi;
		$data['tanggal']			= $tanggal;
		$data['factory']			= $factory;
		$data['kodepo']				= $kode;
		$data['id']				= $id;
		$data['row']				= $this->model->ViewWhere('v_kluar_kemasan','kode_kluar',$kode);

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-edit-pengeluaran-print', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function pengeluaran_kemasan($Aksi=""){
		
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Input Pengeluaran Kemasan';
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['kodepo']				= $this->kodePengeluaran();
		$data['row']				= $this->model->ViewWhere('v_kluar_kemasan','kode_kluar',$this->kodePengeluaran());
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-input-pengeluaran', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function pengeluaran_kemasan_new($Aksi=""){
		
		$dataHeader['title']		= "Kartu Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Pengeluaran Kemasan' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['kodepo']				= $this->kodePengeluaran();
		$data['row']				= $this->model->ViewWhere('v_kluar_kemasan','kode_kluar',$this->kodePengeluaran());
		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-input-pengeluaran-new', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	public function data_pengeluaran_kemasan($Aksi=""){
		
		$dataHeader['title']		= "Laporan Pengiriman Kemasan  - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Data Pengeluaran Kemasan' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['row']				= $this->model->ViewAsc('v_datakluar_kemasan','tgl_kirim');

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-data-kluar', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	
	public function data_pengeluaran_print($kode=""){
		
		$dataHeader['title']		= "Laporan Pengiriman Kemasan  - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Pengeluaran Kemasan' ;
		$dataHeader['link']			= 'po_produk';
		$data['action'] 			= $Aksi ;
		$data['kode'] 			= $kode ;
		$data['row']				= $this->model->ViewWhere('v_datakluar_kemasan', 'kode_po', $kode);

		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan-data-kluar-print', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}
	

	public function stock_opname_produk($Aksi=""){
		
		$dataHeader['title']		= "Stock Opname - ERP MSGLOW 2019";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'opnameproduk' ;
		$dataHeader['link']			= 'stock_opname_kemasan';
		$data['action'] 			= $Aksi ;

		$data['row']				= $this->model->ViewAsc('v_stock_produk','id_barang');

		$this->load->view('back-end/container/header',$dataHeader);
		$this->load->view('back-end/stock/produk-list-stock',$data);
		$this->load->view('back-end/container/footer');
	}

	public function kodePOproduk(){

		$cFormatTahun  = substr(date('Y'),2,2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWPOFACTORY-";

		$dbDate	=	$this->db->query("SELECT COUNT(id_po_produk) as JumlahTransaksi FROM po_produk WHERE bulan = '".date('m')."'");
		if($dbDate->num_rows() > 0){
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi']+1;
			}
		}else{
			$nJumlahTransaksi = 1;
		}
		
		$panjang = strlen($nJumlahTransaksi);
		if($panjang == 1){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'00'.$nJumlahTransaksi;
		}elseif($panjang == 2){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'0'.$nJumlahTransaksi;
		}elseif($panjang == 3){
			$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.$nJumlahTransaksi;
		}
		

		return $cKode;
		
	}

	
	public function cetaksuratjalankemasan($id = "")
	{
		
		$data['kode']	= $id;
		$data['row'] = $this->model->ViewWhere('v_datakluar_kemasan', 'id_kluar_kemasan', $id);
		
		$this->load->view('back-end/stock/cetak/sjkemasan', $data);
	}

		// public function cetaksuratjalankemasan($id=""){
	
	    //     //$data['kode']				= $id	;
	    //     //$data['row']				= $this->model->ViewWhere('v_datakluar_kemasan','id_kluar_kemasan',$id);
	    //     ////load the view and saved it into $html variable
	    //     //$html		=	$this->load->view('back-end/stock/cetak/sjkemasan', $data, true);
	
	    //     //this the the PDF filename that user will get to download
	    //     $pdfFilePath = "surat-jalan-kemasan.pdf";
	
	    //     //load mPDF library
	 	// 	$this->m_pdf->pdf->mPDF('','A5-L','10','ctimesbi','5','5','5','5','5','10','L');
	 	// 	$this->m_pdf->pdf->useOddEven = true ;

	    //    //generate the PDF from the given html
	    //     $this->m_pdf->pdf->WriteHTML('xx');
	
		// 	$this->m_pdf->pdf->debug = true;
		// 	$this->m_pdf->pdf->Output();
	    //     //download it.
	    //     //$this->m_pdf->pdf->Output($pdfFilePath,'I');        
		// }		
	

	public function laporan_stock_satuan($bulan="",$tahun="",$idbarang="")
	{
		$data['bulan']		= $bulan;
		$data['tahun']		= $tahun;
		$data['barang']		= $idbarang;
		$data['row'] 		= $this->model->ViewWhere('v_datakluar_kemasan','kode_kluar',$id);
		
		$this->load->view('back-end/stock/cetak/cetak-laporan-kartu-stock', $data);
	}

	    // public function laporan_stock_satuan($bulan="",$tahun="",$idbarang=""){
	
	    //     $data['bulan']		= 	$bulan	;
	    //     $data['tahun']		=	$tahun  ;
	    //     $data['barang']		=   $idbarang ;

	    //     //$data['row']		= $this->model->ViewWhere('v_datakluar_kemasan','kode_kluar',$id);
	    //     //load the view and saved it into $html variable

	    //     $html		=	$this->load->view('back-end/stock/cetak/cetak-laporan-kartu-stock', $data, true);
	
	    //     //this the the PDF filename that user will get to download
	    //     $pdfFilePath = "surat-jalan-kemasan.pdf";
	
	    //     //load mPDF library
	 	// 	$this->m_pdf->pdf->mPDF('','A4','10','ctimesbi','5','5','5','5','5','10','P');
	 	// 	$this->m_pdf->pdf->useOddEven = true ;

	    //    //generate the PDF from the given html
	    //     $this->m_pdf->pdf->WriteHTML($html);
	
	
	    //     //download it.
	    //     $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	    // }

	
	public function laporan_stock_satuan_produk($bulan="",$tahun="",$idbarang=""){
		
		$data['bulan']		= 	$bulan	;
		$data['tahun']		=	$tahun  ;
		$data['barang']		=   $idbarang ;

	        //$data['row']		= $this->model->ViewWhere('v_datakluar_kemasan','kode_kluar',$id);
	        //load the view and saved it into $html variable

		$html		=	$this->load->view('back-end/stock/cetak/cetak-laporan-kartu-stock', $data, true);
		
	        //this the the PDF filename that user will get to download
		$pdfFilePath = "Laporan Kartu Stock";
		
	        //load mPDF library
		$this->m_pdf->pdf->mPDF('','A4','10','ctimesbi','5','5','5','5','5','10','P');
		$this->m_pdf->pdf->useOddEven = true ;

	       //generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);
		
		
	        //download it.
		$this->m_pdf->pdf->Output($pdfFilePath,'I');        
	}		

	public function tampil_edit_stock($idterima=""){
		$data['terima']		= $idterima;
		$data['row']		= $this->model->ViewWhere('v_detail_kemasan_po','id_terima_kemasan',$idterima);		
		$this->load->view('back-end/stock/table/tampil-edit-kemasan',$data);
		
	}
	
	public function tampil_edit_stock_print($idterima=""){
		$data['terima']		= $idterima;
		$data['row']		= $this->model->ViewWhere('v_detail_print_po','id_terima_kemasan',$idterima);		
		$this->load->view('back-end/stock/table/tampil-edit-print',$data);
		
	}

	public function cetak_stock_kemasan_real()
	{
		$data['row']	= $this->model->ViewAsc('v_stock_kemasan_new','id_kemasan');
		
		$this->load->view('back-end/kemasan/cetak/kemasan-cetak-stock-actual', $data);
	}

		// public function cetak_stock_kemasan_real(){
	
	
	    //     $data['row']	= $this->model->ViewAsc('v_stock_kemasan_new','id_kemasan');
	
	    //     $html			= $this->load->view('back-end/kemasan/cetak/kemasan-cetak-stock-actual', $data, true);
	
	    //     $pdfFilePath 	= "cetak stock kemasan actual.pdf";
	
	    //     //load mPDF library
	 	// 	$this->m_pdf->pdf->mPDF('','A4','10','ctimesbi','5','5','5','5','5','10','P');
	 	// 	$this->m_pdf->pdf->useOddEven = true ;

	    //    //generate the PDF from the given html
	    //     $this->m_pdf->pdf->WriteHTML($html);
	
	
	    //     //download it.
	    //     $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	    // }	

	public function laporan_po_kemasan($Aksi=""){
		
		$dataHeader['title']		= "Laporan Purchase Order Kemasan MSGLOW";
		$dataHeader['menu']   		= 'Laporan Po Kemasan';
		$dataHeader['file']   		= 'Laporan' ;
		$dataHeader['link']			= 'laporan_po';
		$data['action'] 			= $Aksi ;

		$data['po']					= $this->model->ViewWhere('v_po_produk','kode_po',$Aksi);
		$data['row']				= $this->model->ViewWhere('v_detail_po_produk','kode_pb',$Aksi);
		$data['bayar']				= $this->model->ViewWhere('v_terima_produk','kode_po',$Aksi);

		
		$dataHeader['content'] = $this->load->view('back-end/kemasan/kemasan/laporan-po-kemasan', $data, TRUE);
		$this->load->view('Layout/home',$dataHeader);
	}

	function get_kemasan(){
		$category_id = $this->input->post('id',TRUE);
		$data = $this->model->get_sub_category($category_id)->result();
		echo json_encode($data);
	}

	function bersih_data(){
		$this->session->unset_userdata('tanggal_po');
		$this->session->unset_userdata('supplier');
		redirect(site_url('Administrator/Stock/po_kemasan/'));
	}
	
	function bersih_data_print(){
		$this->session->unset_userdata('tanggal_po');
		$this->session->unset_userdata('supplier');
		redirect(site_url('Administrator/Stock/po_kemasan_print/'));
	}

	public function tampil_edit_stock_jadi($idterima=""){
		$data['terima']		= $idterima;
		$data['row']		= $this->model->ViewWhere('v_detail_terima_produk','id_terima_kemasan',$idterima);		
		$this->load->view('back-end/stock/table/tampil-edit-produk',$data);
		
	}

	public function cetak_stock_barang_real(){
		
		
		$data['row']	= $this->model->ViewAsc('v_stock_produk','id_barang');
		
		$html			= $this->load->view('back-end/stock/cetak/barang-cetak-stock-actual', $data, true);
		
		$pdfFilePath 	= "cetak stock kemasan actual.pdf";
		
	        //load mPDF library
		$this->m_pdf->pdf->mPDF('','A4','10','ctimesbi','5','5','5','5','5','10','P');
		$this->m_pdf->pdf->useOddEven = true ;

	       //generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);
		
		
	        //download it.
		$this->m_pdf->pdf->Output($pdfFilePath,'I');        
	}

	public function cetak_laporan_po_kemasan_detail()
	{
		
		$data['mbulan']	= $this->input->post('cBulan');
		$data['tahun']	= $this->input->post('cTahun');
		$data['row']	= $this->model->code("SELECT * FROM v_detail_po_kemas WHERE MONTH(tanggal) = '".$this->input->post('cBulan')."' AND YEAR(tanggal) = '".$this->input->post('cTahun')."' GROUP BY nama_supplier");
		
		$this->load->view('back-end/stock/cetak/kemasan-laporan-po-detail', $data);
	}

	    // public function cetak_laporan_po_kemasan_detail(){
	
	    //    	$data['mbulan']	= $this->input->post('cBulan');

	    //    	$data['tahun']	= $this->input->post('cBulan');
	
	    //     $data['row']	= $this->model->code("SELECT * FROM v_detail_po_kemas WHERE MONTH(tanggal) = '".$this->input->post('cBulan')."' AND YEAR(tanggal) = '".$this->input->post('cTahun')."' GROUP BY nama_supplier");
	
	    //     $html			= $this->load->view('back-end/stock/cetak/kemasan-laporan-po-detail', $data, true);
	
	    //     $pdfFilePath 	= "cetak detail po kemasan periode.pdf";
	
	    //     //load mPDF library
	 	// 	$this->m_pdf->pdf->mPDF('','A4-L','10','ctimesbi','5','5','5','5','5','10','L');
	 	// 	$this->m_pdf->pdf->useOddEven = true ;

	    //    //generate the PDF from the given html
	    //     $this->m_pdf->pdf->WriteHTML($html);
	
	
	    //     //download it.
	    //     $this->m_pdf->pdf->Output($pdfFilePath,'I');        
	    // }

	public function cetak_laporan_kluar_kemasan()
	{
		$data['mbulan']	= $this->input->post('cBulan');
		$data['tahun']	= $this->input->post('cTahun');
		$data['row']	= $this->model->code("SELECT * FROM v_kluar_kemasan WHERE MONTH(tanggal) = '".$this->input->post('cBulan')."'
			AND YEAR(tanggal) = '".$this->input->post('cTahun')."' GROUP BY id_pabrik");
		
		$this->load->view('back-end/stock/cetak/kemasan-laporan-kemasan-kluar-detail', $data);
	}

	    // public function cetak_laporan_kluar_kemasan(){
	
	    //    	$data['mbulan']	= $this->input->post('cBulan');

	    //    	$data['tahun']	= $this->input->post('cTahun');
	
	    //     $data['row']	= $this->model->code("SELECT * FROM v_kluar_kemasan WHERE MONTH(tanggal) = '".$this->input->post('cBulan')."' 
	    //     				  AND YEAR(tanggal) = '".$this->input->post('cTahun')."' GROUP BY id_pabrik");
	
	    //     $html			= $this->load->view('back-end/stock/cetak/kemasan-laporan-kemasan-kluar-detail', $data, true);
	
	    //     $pdfFilePath 	= "cetak detail po kemasan periode.pdf";
	
	    //     //load mPDF library
	 	// 	$this->m_pdf->pdf->mPDF('','Legal-L','10','ctimesbi','5','5','5','5','5','10','L');
	 	// 	$this->m_pdf->pdf->useOddEven = true ;

	    //    //generate the PDF from the given html
	    //     $this->m_pdf->pdf->WriteHTML($html);
	
	
	    //     //download it.
	    //     $this->m_pdf->pdf->Output($pdfFilePath,'I');        
		// }
	
	public function cetak_laporan_penerimaan()
	{
		$data['supplier'] = $this->input->post('cSupplier');
		$data['mbulan']	  = $this->input->post('cBulan');
		$data['tahun']	  = $this->input->post('cBulan');
		
		if($this->input->post('cSupplier') == 'ALL'){
			$data['row']		= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
				AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' ");
			$data['kemasan']	= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
				AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' GROUP BY nama_kemasan");
			$data['tanggal']	= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
				AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' GROUP BY tgl_terima");
		}else{
			$data['row']		= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
				AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' AND supplier = '".$this->input->post('cSupplier')."' ");
			$data['kemasan']	= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
				AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' AND supplier = '".$this->input->post('cSupplier')."' GROUP BY nama_kemasan");
			$data['tanggal']	= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
				AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' AND supplier = '".$this->input->post('cSupplier')."' GROUP BY tgl_terima");
		}
		
		$this->load->view('back-end/kemasan/cetak/kemasan-detail-masuk', $data);
	}

// 	     public function cetak_laporan_penerimaan(){
	
// 	       	$data['supplier'] = $this->input->post('cSupplier');

// 	       	$data['mbulan']	  = $this->input->post('cBulan');

// 	       	$data['tahun']	  = $this->input->post('cBulan');
	

// 	       	if($this->input->post('cSupplier') == 'ALL'){
// 	       		$data['row']		= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
// 	        	              	  AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' ");
// 	       		$data['kemasan']	= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
// 	        	              	  AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' GROUP BY nama_kemasan");
// 	       		$data['tanggal']	= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
// 	        	                  	AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' GROUP BY tgl_terima");
// 	       	}else{
// 	       		$data['row']		= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
// 	        	                  	AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' AND supplier = '".$this->input->post('cSupplier')."' ");
// 	       		$data['kemasan']	= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
// 	        	              	  AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' AND supplier = '".$this->input->post('cSupplier')."' GROUP BY nama_kemasan");
// 	       		$data['tanggal']	= $this->model->code("SELECT * FROM v_terima_kemasan WHERE MONTH(tgl_terima) = '".$this->input->post('cBulan')."' 
// 	        	                  	AND YEAR(tgl_terima) = '".$this->input->post('cTahun')."' AND supplier = '".$this->input->post('cSupplier')."' GROUP BY tgl_terima");
// 	       	}
	
	
// 	        $html			= $this->load->view('back-end/kemasan/cetak/kemasan-detail-masuk', $data, true);
	
// 	        $pdfFilePath 	= "cetak detail po kemasan periode.pdf";
	
// 	        //load mPDF library
// 	 		$this->m_pdf->pdf->mPDF('','A4-L','10','ctimesbi','5','5','5','5','5','10','L');
// 	 		$this->m_pdf->pdf->useOddEven = true ;

// 	       //generate the PDF from the given html
// 	        $this->m_pdf->pdf->WriteHTML($html);
	
	
// 	        //download it.
// 	        $this->m_pdf->pdf->Output($pdfFilePath,'I');        
// 	    }

}