<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stock_Act extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		ob_start();
		error_reporting(0);
		//MenLoad model yang berada di Folder Model dan namany model
		$this->load->model('model');
		// Meload Library session 
		$this->load->library('session');
		//Meload database
		$this->load->database();
		//Meload url 
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}
	public  function Date2String($dTgl)
	{
		//return 2012-11-22
		list($cDate, $cMount, $cYear)	= explode("-", $dTgl);
		if (strlen($cDate) == 2) {
			$dTgl	= $cYear . "-" . $cMount . "-" . $cDate;
		}
		return $dTgl;
	}

	public  function String2Date($dTgl)
	{
		//return 22-11-2012  
		list($cYear, $cMount, $cDate)	= explode("-", $dTgl);
		if (strlen($cYear) == 4) {
			$dTgl	= $cDate . "-" . $cMount . "-" . $cYear;
		}
		return $dTgl;
	}

	public function TimeStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("H:i:s");
		return $Data;
	}

	public function DateStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("d-m-Y");
		return $Data;
	}

	public function DateTimeStamp()
	{
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("Y-m-d h:i:s");
		return $Data;
	}

	public function Produk($Type = "", $id = "")
	{

		$data = array(
			'tanggal' 		=> $this->Date2String($this->input->post('cTanggal')),
			'id_barang' 	=> $this->input->post('cKodeProduk'),
			'jumlah'		=> $this->input->post('cJumlah'),
			'min_jumlah'	=> $this->input->post('cMin'),

		);

		if ($Type == "simpan") {
			$this->model->Insert('tb_stock_produk', $data);
			redirect(site_url('Administrator/Stock/stock_opname/I'));
		} elseif ($Type == "ubah") {
			$this->model->Update('tb_stock_produk', 'id_stock', $id, $data);
			redirect(site_url('Administrator/Stock/stock_opname/U'));
		} elseif ($Type == "hapus") {

			$data = array(
				'is_delete'			=> $this->session->userdata('nama'),
				'delete_date'		=> $this->DateTimeStamp(),
			);

			$this->model->Update('tb_stock_produk', 'id_stock', $id, $data);
			redirect(site_url('Administrator/Stock/stock_opname/D'));
		}
	}

	public function add_detail_kemasan($Type = "", $id = "")
	{

		$query = $this->model->ViewWhere('kemasan', 'id_kemasan', $this->input->post('NamaBarang'));
		foreach ($query as $key => $vaHarga) {
			$harga = $vaHarga['harga'];
		}

		$data = array(
			'kode_pb' 		=> $this->input->post('KodePurchaseOrder'),
			'id_barang' 	=> $this->input->post('NamaBarang'),
			'jumlah'		=> $this->input->post('JumlahPo'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
			'jam'			=> '00:00',
			'user'			=> $this->session->userdata('user_fullname'),
			'harga_satuan'	=> $harga,
			'total'			=> $harga * $this->input->post('JumlahPo'),
			'id_supplier'	=> $this->input->post('Supplier')
		);

		$this->session->set_userdata('supplier', $this->input->post('Supplier'));
		$this->session->set_userdata('tanggal_po', $this->input->post('TanggalOrder'));
		$this->session->set_userdata('kodepo', $this->input->post('KodePurchaseOrder'));

		$this->model->Insert('tb_detail_po_kemasan_draft', $data);
		redirect(site_url('Administrator/Stock_Kemasan/po_kemasan'));
	}

	public function add_detail_print($Type = "", $id = "")
	{

		$query = $this->model->ViewWhere('kemasan', 'id_kemasan', $this->input->post('NamaBarang'));
		foreach ($query as $key => $vaHarga) {
			$harga = $vaHarga['harga'];
		}

		$data = array(
			'kode_pb' 		=> $this->input->post('KodePurchaseOrder'),
			'id_barang' 	=> $this->input->post('NamaBarang'),
			'jumlah'		=> $this->input->post('JumlahPo'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
			'jam'			=> '00:00',
			'user'			=> $this->session->userdata('user_fullname'),
			'harga_satuan'	=> $harga,
			'total'			=> $harga * $this->input->post('JumlahPo'),
			'id_supplier'	=> $this->input->post('Supplier')
		);

		$this->session->set_userdata('supplier', $this->input->post('Supplier'));
		$this->session->set_userdata('tanggal_po', $this->input->post('TanggalOrder'));
		$this->session->set_userdata('kodepo', $this->input->post('KodePurchaseOrder'));

		$this->model->Insert('tb_detail_po_print_draft', $data);
		redirect(site_url('Administrator/Stock/po_kemasan_print'));
	}

	public function approve_po_produk($Type = "", $id = "")
	{
		$this->db->trans_begin();
		if ($_POST['simpan'] == "simpan") {

			$dataUpdate['status_approve'] = "1";
			$this->model->update('po_produk_draft', 'id_po_produk', $this->input->post('id'), $dataUpdate);
			$arrtb_po_kemasan_draft = $this->model->ViewWhere('po_produk_draft', 'id_po_produk', $this->input->post('id'));
			$id_po = "0";
			foreach ($arrtb_po_kemasan_draft as $key2 => $value2) {
				$data2 = $value2;
				$this->model->Insert('po_produk', $data2);
				$id_po = $this->db->insert_id();
				$lq = $this->db->last_query();

				//UPDATE LOG BAYU
				$datalog = array(
					'po_goods_id' => $id_po,
					'user_id' => $_SESSION['user_id'],
					'po_goods_log_status' => 1,
					'po_goods_log_detail' => "PO",
					'po_goods_log_query' => $lq,
					'po_goods_log_date_create' => date('Y-m-d H:i:s'),
					'po_goods_log_date' => $value2['tanggal'],
					'po_goods_log_type' => 1,
					'po_goods_log_url' => base_url().'/Stock_Act/approve_po_produk',
					'po_goods_log_form_url' => base_url().'/Stock_Act/add_po_produk'
				);
				$this->model->Insert('l_po_goods_log', $datalog);
			}

			$arrtb_detail_po_kemasan_draft = $this->model->ViewWhere('tb_detail_po_produk_draft', 'kode_pb', $this->input->post('kode'));
			foreach ($arrtb_detail_po_kemasan_draft as $key => $value) {
				$data = $value;
				$data['jumlah'] = $_POST['qty'][$value['id_pokemasan']];
				$data['total'] = $_POST['qty'][$value['id_pokemasan']] * $data['harga_satuan'];
				$this->model->Insert('tb_detail_po_produk', $data);
				$id_po_d = $this->db->insert_id();
				$lq = $this->db->last_query();

				$dt_p_global = $this->db->query("SELECT a.id, a.kode, a.nama_produk FROM produk_global a JOIN produk b ON a.kode = b.kd_pd WHERE b.id_produk = '".$value['id_barang']."'")->result_array();
				$product_global_id = 0;
				$product_global_code = "";
				$product_global_name = "";
				if (count($dt_p_global)>0) {
					$product_global_id = $dt_p_global[0]['id'];
					$product_global_code = $dt_p_global[0]['kode'];
					$product_global_name = $dt_p_global[0]['nama_produk'];
				}

				//UPDATE LOG BAYU
				$datalog = array(
					'po_goods_id' => $id_po,
					'user_id' => $_SESSION['user_id'],
					'po_goods_detail_log_status' => 1,
					'po_goods_detail_log_detail' => "PO Detail",
					'po_goods_detail_log_query' => $lq,
					'po_goods_detail_log_date_create' => date('Y-m-d H:i:s'),
					'po_goods_detail_id' => $id_po_d,
					'po_goods_detail_log_price' => $data['total'],
					'po_goods_detail_log_quantity' => $value['jumlah'],
					'po_goods_detail_log_quantity_before' => $value['jumlah'],
					'po_goods_detail_log_quantity_current' => $value['jumlah'],
					'product_id' => $value['id_barang'],
					'product_global_id' => $product_global_id,
					'product_global_code' => $product_global_code,
					'product_global_name' => $product_global_name,
					'po_goods_detail_log_type' => 1,
					'po_goods_detail_log_url' => base_url().'/Stock_Act/approve_po_produk',
					'po_goods_detail_log_form_url' => base_url().'/Stock_Act/add_po_produk'
				);
				$this->model->Insert('l_po_goods_detail_log', $datalog);
			}

		} else if ($_POST['tolak'] == "tolak") {
			$dataUpdate['status_approve'] = "2";
			$this->model->update('po_produk_draft', 'id_po_produk', $this->input->post('id'), $dataUpdate);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('Administrator/Stock/po_produk_pic'));
	}

	public function approve_po_kemas($Type = "", $id = "")
	{
		$arrtb_detail_po_kemasan_draft = $this->model->ViewWhere('tb_detail_po_kemasan_draft', 'kode_pb', $this->input->post('kode'));
		foreach ($arrtb_detail_po_kemasan_draft as $key => $value) {
			$data = $value;
			$data['jumlah'] = $_POST['qty'][$value['id_pokemasan']];
			$data['total'] = $_POST['qty'][$value['id_pokemasan']] * $data['harga_satuan'];
			$this->model->Insert('tb_detail_po_kemasan', $data);
		}
		$dataUpdate['status_approve'] = "1";
		$this->model->update('po_kemasan_draft', 'id_po_kemasan', $this->input->post('id'), $dataUpdate);
		$arrtb_po_kemasan_draft = $this->model->ViewWhere('po_kemasan_draft', 'id_po_kemasan', $this->input->post('id'));
		foreach ($arrtb_po_kemasan_draft as $key2 => $value2) {
			$data2 = $value2;
			$this->model->Insert('po_kemasan', $data2);
		}
		redirect(site_url('Administrator/Stock/po_kemasan_pic'));
	}

	public function approve_po_print($Type = "", $id = "")
	{
		$arrtb_detail_po_kemasan_draft = $this->model->ViewWhere('tb_detail_po_print_draft', 'kode_pb', $this->input->post('kode'));
		foreach ($arrtb_detail_po_kemasan_draft as $key => $value) {
			$data = $value;
			$data['jumlah'] = $_POST['qty'][$value['id_pokemasan']];
			$data['total'] = $_POST['qty'][$value['id_pokemasan']] * $data['harga_satuan'];
			$this->model->Insert('tb_detail_po_print', $data);
		}
		$dataUpdate['status_approve'] = "1";
		$this->model->update('po_print_draft', 'id_po_kemasan', $this->input->post('id'), $dataUpdate);
		$arrtb_po_kemasan_draft = $this->model->ViewWhere('po_print_draft', 'id_po_kemasan', $this->input->post('id'));
		foreach ($arrtb_po_kemasan_draft as $key2 => $value2) {
			$data2 = $value2;
			$this->model->Insert('po_print', $data2);
		}
		redirect(site_url('Administrator/Stock/po_print_pic'));
	}

	public function add_po_kemas($Type = "", $id = "")
	{

		$query = $this->model->code("SELECT sum(total) as jumlah FROM tb_detail_po_kemasan_draft WHERE kode_pb = '" . $this->session->userdata('kodepo') . "'");
		foreach ($query as $key => $vaHarga) {
			$harga = $vaHarga['jumlah'];
		}

		$data = array(
			'kode_po' 		=> $this->session->userdata('kodepo'),
			'tanggal' 		=> $this->Date2String($this->session->userdata('tanggal_po')),
			'supplier'		=> $this->session->userdata('supplier'),
			'user'			=> $this->session->userdata('user_fullname'),
			'total_biaya'	=> $harga,
			'bulan'			=> date('m'),
			'tahun'			=> date('y'),
			'active'		=> 'Y',
		);


		$this->model->Insert('po_kemasan_draft', $data);
		unset($_SESSION['tanggal_po']);
		unset($_SESSION['supplier']);
		unset($_SESSION['kodepo']);

		redirect(site_url('Administrator/Stock/po_kemasan/I'));
	}

	public function add_po_kemas2($Type = "")
	{

		$data = array(
			'kode_po' 		=> $this->input->post('kodepo'),
			'tanggal' 		=> $this->Date2String($this->input->post('tanggal_po')),
			'supplier'		=> $this->session->userdata('supplier'),
			'user'			=> $this->session->userdata('user_fullname'),
			'bulan'			=> date('m'),
			'tahun'			=> date('y'),
			'active'		=> 'Y',
		);


		$this->model->Insert('po_kemasan_draft', $data);

		$query = $this->db->query("SELECT MAX(id_po_kemasan) as id_po_kemasan FROM po_kemasan_draft")->row_array();

		// redirect(site_url('Administrator/Stock/po_kemasan'));
		redirect(site_url('Administrator/Stock/edit_po_kemasan/' . $query['id_po_kemasan']));
	}
	public function add_print($Type = "")
	{

		$data = array(
			'kode_po' 		=> $this->input->post('kodepo'),
			'tanggal'		=> $this->Date2String($this->input->post('tanggal_po')),
			'supplier'		=> $this->session->userdata('supplier'),
			'user'			=> $this->session->userdata('user_fullname'),
			'bulan'		    => date('m'),
			'tahun'			=> date('y'),
			'active'		=> 'Y',
		);

		$this->model->Insert('po_print_draft', $data);


		$query = $this->db->query("SELECT MAX(id_po_kemasan) as id_po_kemasan FROM po_print_draft")->row_array();


		// redirect(site_url('Administrator/Stock/po_kemasan_print'));
		redirect(site_url('Administrator/Stock/edit_po_kemasan_print/' . $query['id_po_kemasan']));
	}

	public function add_po_print($Type = "", $id = "")
	{

		$query = $this->model->code("SELECT sum(total) as jumlah FROM tb_detail_po_print_draft WHERE kode_pb = '" . $this->session->userdata('kodepo') . "'");
		foreach ($query as $key => $vaHarga) {
			$harga = $vaHarga['jumlah'];
		}

		$data = array(
			'kode_po' 		=> $this->session->userdata('kodepo'),
			'tanggal' 		=> $this->Date2String($this->session->userdata('tanggal_po')),
			'supplier'		=> $this->session->userdata('supplier'),
			'user'			=> $this->session->userdata('user_fullname'),
			'total_biaya'	=> $harga,
			'bulan'			=> date('m'),
			'tahun'			=> date('y'),
			'active'		=> 'Y',
		);


		$this->model->Insert('po_print_draft', $data);
		unset($_SESSION['tanggal_po']);
		unset($_SESSION['supplier']);
		unset($_SESSION['kodepo']);

		redirect(site_url('Administrator/Stock/po_kemasan_print/I'));
	}

	public function bayar_kemasan($Type = "", $id = "")
	{

		$data = array(
			'kode_po' 		=> $this->input->post('KodePurchaseOrder'),
			'tgl_bayar' 	=> $this->Date2String($this->input->post('TanggalOrder')),
			'jumlah'		=> $this->input->post('JumlahBayar'),
			'user'			=> $this->session->userdata('nama'),
			'bank'			=> $this->input->post('Bank'),
		);

		$this->model->Insert('bayar_kemasan', $data);
		redirect(site_url('Administrator/Stock/bayar_kemasan/' . $this->input->post('KodePurchaseOrder') . ''));
	}

	public function terima_kemasan($Type = "", $id = "")
	{
		$this->db->trans_begin();
		$data = array(
			'kode_po' 		=> $this->input->post('KodePurchaseOrder'),
			'tgl_terima' 	=> $this->Date2String($this->input->post('TanggalOrder')),
			'jumlah'		=> $this->input->post('JumlahBayar'),
			'user'			=> $this->session->userdata('user_fullname'),
			'id_barang'		=> $this->input->post('NamaBarang'),
		);

		$queryStock = $this->model->code("SELECT * FROM tb_stock_kemasan WHERE id_barang = '" . $this->input->post('NamaBarang') . "'");
		foreach ($queryStock as $key => $vaRow) {
			$stockAwal = $vaRow['jumlah'];
		}

		$stockall = $stockAwal + $this->input->post('JumlahBayar');

		$dataUpdate = array(
			'jumlah'		=> $stockall
		);

		$queryInvoice = $this->model->code("SELECT * FROM kemasan WHERE id_kemasan = '" . $this->input->post('NamaBarang') . "'");
		foreach ($queryInvoice as $key => $vaRow) {
			$hargaKemasan 	= $vaRow['harga'];
			$idSupplier 	= $vaRow['supplier'];
		}

		$hargaTotal =  $hargaKemasan * $this->input->post('JumlahBayar');

		$dataDetailInvo = array(
			'tanggal'			=> $this->Date2String($this->input->post('TanggalOrder')),
			'id_supplier'		=> $idSupplier,
			'id_kemasan' 		=> $this->input->post('KodePurchaseOrder'),
			'kode_po' 			=> $this->input->post('NamaBarang'),
			'harga'				=> $hargaKemasan,
			'jumlah'			=> $this->input->post('JumlahBayar'),
			'total'				=> $hargaTotal,

		);

		$this->model->update_stock($this->input->post('JumlahBayar'), $this->Date2String($this->input->post('TanggalOrder')), $this->input->post('NamaBarang'));
		$this->model->Insert('terima_kemasan', $data);

		$this->model->Insert('detail_invoice_kemasan', $dataDetailInvo);

		$this->model->update('tb_stock_kemasan', 'id_barang', $this->input->post('NamaBarang'), $dataUpdate);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('Administrator/Stock/terima_kemasan/' . $this->input->post('KodePurchaseOrder') . ''));
	}

	public function chk_confirmation_password($id = "")
	{
		$arrdata = $this->model->Code_row("SELECT user_password FROM m_user WHERE user_id = '" . $_SESSION['user_id'] . "'");
		if (md5($_POST['Password']) == $arrdata['user_password']) {
			redirect(site_url('Administrator/Stock/edit_po_kemasan/' . $id . ''));
		} else {
			redirect(site_url('Administrator/Stock/po_kemasan_history'));
		}
	}

	public function chk_confirmation_password_print($id = "")
	{
		$arrdata = $this->model->Code_row("SELECT user_password FROM m_user WHERE user_id = '" . $_SESSION['user_id'] . "'");
		if (md5($_POST['Password']) == $arrdata['user_password']) {
			redirect(site_url('Administrator/Stock/edit_po_kemasan_print/' . $id . ''));
		} else {
			redirect(site_url('Administrator/Stock/po_kemasan_history_print'));
		}
	}

	public function selesai_kemasan($id = "")
	{

		$data = array(
			'active'    => 'T',
		);
		$this->model->Update('po_kemasan', 'kode_po', $id, $data);
		redirect(site_url('Administrator/Stock/terima_kemasan/' . $id . ''));
	}

	public function selesai_print($id = "")
	{

		$data = array(
			'active'    => 'T',
		);
		$this->model->Update('po_print', 'kode_po', $id, $data);
		redirect(site_url('Administrator/Stock/terima_print/' . $id . ''));
	}

	public function terima_print($Type = "", $id = "")
	{

		$data = array(
			'kode_po' 		=> $this->input->post('KodePurchaseOrder'),
			'tgl_terima' 	=> $this->Date2String($this->input->post('TanggalOrder')),
			'jumlah'		=> $this->input->post('JumlahBayar'),
			'user'			=> $this->session->userdata('user_fullname'),
			'id_barang'		=> $this->input->post('NamaBarang'),
		);

		$queryStock = $this->model->code("SELECT * FROM tb_stock_kemasan WHERE id_barang = '" . $this->input->post('NamaBarang') . "'");
		foreach ($queryStock as $key => $vaRow) {
			$stockAwal = $vaRow['jumlah'];
		}

		$stockall = $stockAwal + $this->input->post('JumlahBayar');

		$dataUpdate = array(
			'jumlah'		=> $stockall
		);

		$queryInvoice = $this->model->code("SELECT * FROM kemasan WHERE id_kemasan = '" . $this->input->post('NamaBarang') . "'");
		foreach ($queryInvoice as $key => $vaRow) {
			$hargaKemasan 	= $vaRow['harga'];
			$idSupplier 	= $vaRow['supplier'];
		}

		$hargaTotal =  $hargaKemasan * $this->input->post('JumlahBayar');

		$dataDetailInvo = array(
			'tanggal'			=> $this->Date2String($this->input->post('TanggalOrder')),
			'id_supplier'		=> $idSupplier,
			'id_kemasan' 		=> $this->input->post('KodePurchaseOrder'),
			'kode_po' 			=> $this->input->post('NamaBarang'),
			'harga'				=> $hargaKemasan,
			'jumlah'			=> $this->input->post('JumlahBayar'),
			'total'				=> $hargaTotal,

		);


		$this->model->Insert('terima_print', $data);

		$this->model->Insert('detail_invoice_print', $dataDetailInvo);

		$this->model->update('tb_stock_kemasan', 'id_barang', $this->input->post('NamaBarang'), $dataUpdate);
		redirect(site_url('Administrator/Stock/terima_print/' . $this->input->post('KodePurchaseOrder') . ''));
	}


	public function add_pengeluaran_detail_kemasan($Type = "", $id = "")
	{

		$data = array(
			'kode_kluar' 	=> $this->input->post('KodePurchaseOrder'),
			'id_barang' 	=> $this->input->post('NamaBarang'),
			'jumlah'		=> $this->input->post('JumlahPo'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
			'jam'			=> '00:00',
			'user'			=> $this->session->userdata('user_fullname'),
			'id_pabrik'		=> $this->input->post('Supplier')
		);

		$this->session->set_userdata('factory', $this->input->post('Supplier'));
		$this->session->set_userdata('tanggal_po', $this->input->post('TanggalOrder'));
		$this->session->set_userdata('kodepo', $this->input->post('KodePurchaseOrder'));

		$this->model->Insert('tb_detail_kluar_kemasan', $data);
		redirect(site_url('Administrator/Stock/pengeluaran_kemasan'));
	}

	public function add_pengeluaran_detail_kemasan_new($Type = "", $id = "")
	{

		$data = array(
			'kode_kluar' 	=> $this->input->post('KodePurchaseOrder'),
			'id_barang' 	=> $this->input->post('NamaBarang'),
			'jumlah'		=> $this->input->post('JumlahPo'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
			'jam'			=> '00:00',
			'user'			=> $this->session->userdata('nama'),
			'id_pabrik'		=> $this->input->post('Supplier')
		);

		$this->session->set_userdata('factory', $this->input->post('Supplier'));
		$this->session->set_userdata('tanggal_po', $this->input->post('TanggalOrder'));
		$this->session->set_userdata('kodepo', $this->input->post('KodePurchaseOrder'));

		$this->model->Insert('tb_detail_kluar_kemasan', $data);
		redirect(site_url('Administrator/Stock/pengeluaran_kemasan_new/' . $Type));
	}

	public function add_edit_kemasan_detail_po($Type = "", $id = "")
	{
		$this->db->trans_begin();
		$query = $this->model->ViewWhere('kemasan', 'id_kemasan', $this->input->post('NamaBarang'));
		foreach ($query as $key => $vaHarga) {
			$harga = $vaHarga['harga'];
		}

		$data = array(
			'kode_pb' 		=> $this->input->post('KodePurchaseOrder'),
			'id_barang' 	=> $this->input->post('NamaBarang'),
			'jumlah'		=> $this->input->post('JumlahPo'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
			'jam'			=> '00:00',
			'user'			=> $this->session->userdata('user_fullname'),
			'harga_satuan'	=> $harga,
			'total'			=> $harga * $this->input->post('JumlahPo'),
			'id_supplier'	=> $this->input->post('Supplier')
		);

		$po_kemasan = array(
			'supplier'		=> $this->input->post('Supplier'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
		);


		$this->session->set_userdata('factory', $this->input->post('Supplier'));
		$this->session->set_userdata('tanggal_po', $this->input->post('TanggalOrder'));
		$this->session->set_userdata('kodepo', $this->input->post('KodePurchaseOrder'));

		//Insert
		$this->model->Insert('tb_detail_po_kemasan_draft', $data);

		//Update
		$this->model->update('po_kemasan_draft', 'id_po_kemasan', $Type, $po_kemasan);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('Administrator/Stock/edit_po_kemasan/' . $Type));
	}

	public function add_edit_kemasan_detail_po_print($Type = "", $id = "")
	{
		$this->db->trans_begin();
		$query = $this->model->ViewWhere('kemasan', 'id_kemasan', $this->input->post('NamaBarang'));
		foreach ($query as $key => $vaHarga) {
			$harga = $vaHarga['harga'];
		}

		$data = array(
			'kode_pb' 		=> $this->input->post('KodePurchaseOrder'),
			'id_barang' 	=> $this->input->post('NamaBarang'),
			'jumlah'		=> $this->input->post('JumlahPo'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
			'jam'			=> '00:00',
			'user'			=> $this->session->userdata('user_fullname'),
			'harga_satuan'	=> $harga,
			'total'			=> $harga * $this->input->post('JumlahPo'),
			'id_supplier'	=> $this->input->post('Supplier')
		);

		$po_kemasan = array(
			'supplier'		=> $this->input->post('Supplier'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
		);


		$this->session->set_userdata('factory', $this->input->post('Supplier'));
		$this->session->set_userdata('tanggal_po', $this->input->post('TanggalOrder'));
		$this->session->set_userdata('kodepo', $this->input->post('KodePurchaseOrder'));

		//Insert
		$this->model->Insert('tb_detail_po_print_draft', $data);

		//Update
		$this->model->update('po_print_draft', 'id_po_kemasan', $Type, $po_kemasan);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('Administrator/Stock/edit_po_kemasan_print/' . $Type));
	}

	public function history_po()
	{

		redirect(site_url('Administrator/Stock/po_kemasan_history/'));
	}

	public function history_print()
	{

		redirect(site_url('Administrator/Stock/po_kemasan_history_print/'));
	}

	public function add_edit_pengeluaran_detail_kemasan($Type = "", $id = "")
	{

		$data = array(
			'kode_kluar' 	=> $this->input->post('KodePurchaseOrder'),
			'id_barang' 	=> $this->input->post('NamaBarang'),
			'jumlah'		=> $this->input->post('JumlahPo'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
			'jam'			=> '00:00',
			'user'			=> $this->session->userdata('user_fullname'),
			'id_pabrik'		=> $this->input->post('Supplier')
		);

		$this->session->set_userdata('factory', $this->input->post('Supplier'));
		$this->session->set_userdata('tanggal_po', $this->input->post('TanggalOrder'));
		$this->session->set_userdata('kodepo', $this->input->post('KodePurchaseOrder'));

		$this->model->Insert('tb_detail_kluar_kemasan', $data);
		redirect(site_url('Administrator/Stock/edit_pengeluaran_kemasan/' . $Type . ''));
	}

	public function add_edit_pengeluaran_detail_print($id = "", $Type = "")
	{
		$Type = str_replace('%7C', '|', $Type);
		$data = array(
			'kode_kluar' 	=> $this->input->post('KodePurchaseOrder'),
			'id_barang' 	=> $this->input->post('NamaBarang'),
			'jumlah'		=> $this->input->post('JumlahPo'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
			'jam'			=> '00:00',
			'user'			=> $this->session->userdata('user_fullname'),
			'id_pabrik'		=> $this->input->post('Supplier')
		);

		$this->session->set_userdata('factory', $this->input->post('Supplier'));
		$this->session->set_userdata('tanggal_po', $this->input->post('TanggalOrder'));
		$this->session->set_userdata('kodepo', $this->input->post('KodePurchaseOrder'));

		$this->model->Insert('tb_detail_kluar_kemasan', $data);
		redirect(site_url('Administrator/Stock/edit_pengeluaran_print/' . $id . '/' . $Type . ''));
	}

	public function add_kluar_kemas($Type = "", $id = "")
	{
		$this->db->trans_begin();
		$data = array(
			'kode_kluar' 	=> $this->input->post('kodepo'),
			'tgl_kirim' 	=> $this->Date2String($this->input->post('tanggalpo')),
			'pabrik'		=> $this->input->post('pabrik'),
			'user'			=> $this->session->userdata('user_fullname'),
			'bulan'			=> date('m'),
			'tahun'			=> date('y'),
		);

		if ($Type != "") {
			$data['kode_po'] = $Type;
			$data['tipe'] = 'print';
		}
		$this->model->Insert('kluar_kemasan', $data);

		$cekKL =  $this->model->ViewWhere('v_kluar_kemasan', 'kode_kluar', $this->input->post('kodepo'));
		foreach ($cekKL as $key => $vaCek) {
			$this->model->Codes("INSERT INTO t_factory_stock (factory_id, ingredient_id, factory_stock_amount) VALUES(" . $vaCek['id_pabrik'] . ", " . $vaCek['id_barang'] . ", " . $vaCek['jumlah'] . ") ON DUPLICATE KEY UPDATE factory_stock_amount=factory_stock_amount+" . $vaCek['jumlah'] . "");
			$query =  $this->model->ViewWhere('tb_stock_kemasan', 'id_barang', $vaCek['id_barang']);
			foreach ($query as $key => $vaData) {
				$tot = $vaData['jumlah'];
			}

			$dataUpdate = array(
				'jumlah'		=> $tot - $vaCek['jumlah']
			);

			$this->model->update('tb_stock_kemasan', 'id_barang', $vaCek['id_barang'], $dataUpdate);
		}

		unset($_SESSION['tanggal_po']);
		unset($_SESSION['factory']);
		unset($_SESSION['kodepo']);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		if ($Type != "") {
			redirect(site_url('Administrator/Stock/data_pengeluaran_print/' . $Type));
		} else {
			redirect(site_url('Administrator/Stock/data_pengeluaran_kemasan'));
		}
	}

	public function add_detail_produk($Type = "", $id = "")
	{

		/*$query = $this->model->ViewWhere('produk','id_produk',$this->input->post('NamaBarang'));
		foreach ($query as $key => $vaHarga) {
			$harga = $vaHarga['harga'];
		}*/
		$harga = $this->input->post('Harga');

		$data = array(
			'kode_pb' 		=> $this->input->post('KodePurchaseOrder'),
			'id_barang' 	=> $this->input->post('NamaBarang'),
			'jumlah'		=> $this->input->post('JumlahPo'),
			'tanggal'		=> $this->Date2String($this->input->post('TanggalOrder')),
			'jam'			=> '00:00',
			'user'			=> $this->session->userdata('user_fullname'),
			//'harga_satuan'	=> $harga,
			'total'			=> $harga * $this->input->post('JumlahPo'),
			'id_factory'	=> $this->input->post('Supplier')
		);

		$this->session->set_userdata('factory', $this->input->post('Supplier'));
		$this->session->set_userdata('tanggal_po', $this->input->post('TanggalOrder'));
		$this->session->set_userdata('kodepo', $this->input->post('KodePurchaseOrder'));

		$this->model->Insert('tb_detail_po_produk_draft', $data);
		redirect(site_url('Administrator/Stock_Produk/po_produk'));
	}

	public function add_po_produk($Type = "", $id = "")
	{

		$query = $this->model->code("SELECT sum(total) as jumlah FROM tb_detail_po_produk_draft WHERE kode_pb = '" . $this->session->userdata('kodepo') . "'");
		foreach ($query as $key => $vaHarga) {
			$harga = $vaHarga['jumlah'];
		}

		$data = array(
			'kode_po' 		=> $this->session->userdata('kodepo'),
			'tanggal' 		=> $this->Date2String($this->session->userdata('tanggal_po')),
			'factory'		=> $this->session->userdata('factory'),
			'user'			=> $this->session->userdata('user_fullname'),
			'total_biaya'	=> $harga,
			'bulan'			=> date('m'),
			'tahun'			=> date('y'),
			'active'		=> 'Y',
		);


		$this->model->Insert('po_produk_draft', $data);
		unset($_SESSION['tanggal_po']);
		unset($_SESSION['supplier']);
		unset($_SESSION['kodepo']);

		redirect(site_url('Administrator/Stock_Produk/po_produk/I'));
	}

	public function add_po_kemasan($Type = "", $id = "")
	{

		$query = $this->model->code("SELECT sum(total) as jumlah FROM tb_detail_po_kemasan_draft WHERE kode_pb = '" . $this->session->userdata('kodepo') . "'");
		foreach ($query as $key => $vaHarga) {
			$harga = $vaHarga['jumlah'];
		}

		$data = array(
			'kode_po' 		=> $this->session->userdata('kodepo'),
			'tanggal' 		=> $this->Date2String($this->session->userdata('tanggal_po')),
			'supplier'		=> $this->session->userdata('supplier'),
			'user'			=> $this->session->userdata('user_fullname'),
			'total_biaya'	=> $harga,
			'bulan'			=> date('m'),
			'tahun'			=> date('y'),
			'active'		=> 'Y',
		);

		$this->model->Insert('po_kemasan_draft', $data);
		unset($_SESSION['tanggal_po']);
		unset($_SESSION['supplier']);
		unset($_SESSION['kodepo']);

		redirect(site_url('Administrator/Stock_Kemasan/po_kemasan'));
	}

	public function terima_produk($Type = "", $id = "")
	{
		$this->db->trans_begin();

		$data = array(
			'kode_po' 		=> $this->input->post('KodePurchaseOrder'),
			'tgl_terima' 	=> $this->Date2String($this->input->post('TanggalOrder')),
			'jumlah'		=> $this->input->post('JumlahBayar'),
			'user'			=> $this->session->userdata('user_fullname'),
			'id_barang'		=> $this->input->post('NamaBarang'),
		);

		$queryStock = $this->model->code("SELECT * FROM tb_stock_produk WHERE id_barang = '" . $this->input->post('NamaBarang') . "'");
		foreach ($queryStock as $key => $vaRow) {
			$stockAwal = $vaRow['jumlah'];
		}

		$stockall = $stockAwal + $this->input->post('JumlahBayar');

		$dataUpdate = array(
			'jumlah'		=> $stockall
		);

		$this->model->Insert('terima_produk', $data);
		$this->model->update('tb_stock_produk', 'id_barang', $this->input->post('NamaBarang'), $dataUpdate);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('Administrator/Stock_Produk/terima_produk/' . $this->input->post('KodePurchaseOrder') . ''));
	}

	public function terima_produk2($Type = "", $id = "")
	{
		$this->db->trans_begin();

		$kodepo 			= $_POST['KodePurchaseOrder'];
		$tanggal 			= $_POST['TanggalOrder'];
		$nomor_surat_jalan 	= $_POST['nomor_surat_jalan'];
		$data_produkx 		= str_replace(",", "", $_POST['id_produk']);
				//$_POST['id_produk'] |code sebelumnya|
		foreach ($data_produkx as $aindex => $data_produk) {
			foreach ($data_produk as $index => $value) {
				$value = str_replace(",", "", $value);
				if ($value > 0) {
					$data = array(
						'kode_po' 		=> $this->input->post('KodePurchaseOrder'),
						'tgl_terima' 	=> $this->Date2String($this->input->post('TanggalOrder')),
						'jumlah'		=> $value,
						'jumlah_sj'		=> $value,
						'user'			=> $this->session->userdata('user_fullname'),
						'id_barang'		=> $index,
						'warehouse_id'		=> $_SESSION['warehouse_id'], 
						'nomor_surat_jalan'		=> $nomor_surat_jalan,
					);
					$queryStock = $this->model->code("SELECT * FROM tb_stock_produk WHERE id_barang = '" . $index . "'");
					foreach ($queryStock as $key => $vaRow) {
						$stockAwal = $vaRow['jumlah'];
					}

					$stockall = $stockAwal + $value;

					$dataUpdate = array(
						'jumlah'		=> $stockall  
					);
					$this->model->Insert('terima_produk', $data);
					$id_terima = $this->db->insert_id();
					$lq = $this->db->last_query();

					$table = "t_incoming_goods";
					$kolom = "incoming_goods_code";
					$tgl = date('ymd');
					$sql = $this->db->query("SELECT * FROM $table WHERE SUBSTRING($kolom, 9, 6) = '$tgl' order by $kolom DESC limit 1")->result_array();
					if(count($sql)<1){
						$id_ig = 'MSGLOWIN'.date('ymd').'0001';
					} else {
						$temp = (int)substr($sql[0][$kolom], 14);
						$no = $temp+1;
						if($no<=9 && $no>1){
							$id_ig = 'MSGLOWIN'.date('ymd').'000'.$no;
						} elseif($no<=99 && $no>9){
							$id_ig = 'MSGLOWIN'.date('ymd').'00'.$no;
						} elseif($no<=999 && $no>99){
							$id_ig = 'MSGLOWIN'.date('ymd').'0'.$no;
						} else {
							$id_ig = 'MSGLOWIN'.date('ymd').''.$no;
						}
					}

					$in = array(
						'incoming_goods_code' => $id_ig,
						'warehouse_id' => $_SESSION['warehouse_id'],
						'warehouse_origin_id' => $_SESSION['warehouse_id'],
						'incoming_goods_date' => $this->Date2String($this->input->post('TanggalOrder')),
						'incoming_goods_create_user_id' => $_SESSION['user_id'],
						'incoming_goods_create_date' => date('Y-m-d H:i:s')
					);
					$exec = $this->model->insert("t_incoming_goods", $in);
					$id_inc = $this->db->insert_id();

					$datalog = array(
						'incoming_goods_id' => $id_inc,
						'user_id' => $_SESSION['user_id'],
						'inbound_log_status' => 1,
						'inbound_log_detail' => 'Inbound',
						'inbound_log_query' => $lq,
						'inbound_date_create' => date('Y-m-d H:i:s'),
						'terima_produk_id' => $id_terima,
						'adjustment_target' => $id_terima,
						'inbound_quantity' => $value,
						'inbound_date' => $this->Date2String($this->input->post('TanggalOrder')),
						'inbound_type' => 1,
						'inbound_url' => base_url().'Administrator/Stock_Act/terima_produk2',
						'inbound_form_url' => base_url().'/Administrator/Stock_Produk/terima_produk/'.$this->input->post('KodePurchaseOrder')
					);
					$this->model->Insert('l_inbound_log', $datalog);

							//$this->model->update('tb_stock_produk','id_barang',$index,$dataUpdate);
							//$this->model->UpdateDua('tb_stock_produk_history','id_barang',$index,'tanggal >',$this->Date2String($this->input->post('TanggalOrder')),$dataUpdate);
							//$this->db->query(); 
							//$this->db->query(); 
					$this->model->Codes("UPDATE tb_stock_produk SET jumlah = (jumlah + $value) WHERE id_barang = '" . $index . "' AND warehouse_id = '".$_SESSION['warehouse_id']."'");
					$this->model->Codes("UPDATE tb_stock_produk_history SET jumlah = (jumlah + $value) WHERE id_barang = '$index' AND REPLACE(tanggal, '-', '') > REPLACE('" . $this->Date2String($this->input->post('TanggalOrder')) . "', '-', '') AND warehouse_id = '".$_SESSION['warehouse_id']."'");
				}
			}
		}
		$query = $this->model->code("SELECT sum(jumlah) as totalproduk FROM  v_terima_produk WHERE kode_po = '" . $_POST['KodePurchaseOrder'] . "'");
		foreach ($query as $key => $vaTotal) {
			$totalp =  $vaTotal['totalproduk'];
		}

		$queryDua = $this->model->code("SELECT sum(jumlah) as totalproduk FROM  tb_detail_po_produk WHERE kode_pb = '" . $_POST['KodePurchaseOrder'] . "'");
		foreach ($queryDua as $key => $vaTotal) {
			$totalAll =  $vaTotal['totalproduk'];
		}

		if ($totalp >= $totalAll) {
			$datax = array(
				'active'    => 'T',
				'active_date_update' => date('Y-m-d')
			);
			$this->model->Update('po_produk', 'kode_po', $_POST['KodePurchaseOrder'], $datax);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('Administrator/Stock_Produk/terima_produk/'.$this->input->post('KodePurchaseOrder').''));
				// redirect(site_url('Po_produk'));
	}

	public function hapus_detail_po($id = "", $id_pokemasan = "")
	{

		$this->model->Delete('tb_detail_po_kemasan_draft', 'id_pokemasan', $id);
		redirect(site_url('Administrator/Stock/edit_po_kemasan/' . $id_pokemasan));
	}

	public function hapus_po_kemasan($id = "", $id_po_kemasan = "")
	{

		$this->model->Delete('po_kemasan_draft', 'id_po_kemasan', $id);
		redirect(site_url('Administrator/Stock/po_kemasan_pic/' . $id_po_kemasan));
	}

	public function hapus_po_produk($id = "", $id_po_produk = "")
	{

		$this->model->Delete('po_produk_draft', 'id_po_produk', $id);
		redirect(site_url('Administrator/Stock/po_produk_pic/' . $id_po_produk));
	}

	public function hapus_detail_po_print($id = "", $id_pokemasan = "")
	{

		$this->model->Delete('tb_detail_po_print_draft', 'id_pokemasan', $id);
		redirect(site_url('Administrator/Stock/edit_po_kemasan_print/' . $id_pokemasan));
	}

	public function hapus_detail_order($id = "", $kodepurchase = "")
	{

		$this->model->Delete('tb_detail_po_produk_draft', 'id_pokemasan', $id);
		redirect(site_url('Administrator/Stock_Produk/po_produk'));
	}

	public function hapus_detail_order_kemasan($id = "", $kodepurchase = "")
	{

		$this->model->Delete('tb_detail_po_kemasan_draft', 'id_pokemasan', $id);
		redirect(site_url('Administrator/Stock_Kemasan/po_kemasan'));
	}

	public function hapus_detail_kluar2($id = "", $kodepurchase = "")
	{
		$arrdata = $this->model->ViewWhere('tb_detail_kluar_kemasan', 'id_pokemasan', $id);
		$this->model->update_keluar($arrdata[0]['jumlah'], $arrdata[0]['tanggal'], $arrdata[0]['id_barang']);
		$this->model->Delete('tb_detail_kluar_kemasan', 'id_pokemasan', $id);
		redirect(site_url('Administrator/Stock/edit_pengeluaran_kemasan/' . $kodepurchase . ''));
	}

	public function hapus_detail_kluar_print($id = "", $kode = "", $kodepurchase = "")
	{
		$arrdata = $this->model->ViewWhere('tb_detail_kluar_kemasan', 'id_pokemasan', $id);
		$this->model->update_keluar($arrdata[0]['jumlah'], $arrdata[0]['tanggal'], $arrdata[0]['id_barang']);
		$this->model->Delete('tb_detail_kluar_kemasan', 'id_pokemasan', $id);
		redirect(site_url('Administrator/Stock/edit_pengeluaran_print/' . $kode . '/' . $kodepurchase . ''));
	}

	public function hapus_detail_kluar($id = "", $kodepurchase = "")
	{

		$this->model->Delete('tb_detail_kluar_kemasan', 'id_pokemasan', $id);
		redirect(site_url('Administrator/Stock/pengeluaran_kemasan/' . $kodepurchase . ''));
	}

	public function terima_kemasan_edit($Type = "", $id = "")
	{
		$this->db->trans_begin();
		$data = array(
			'baik'		=> $this->input->post('terimabagus'),
			'rusak'		=> $this->input->post('terimrusak'),
		);
		$tanggal = $this->input->post('TanggalOrder');
		$query = $this->model->code("SELECT * FROM terima_kemasan WHERE id_terima_kemasan = '" . $this->input->post('kodepo') . "' ");
		foreach ($query as $key => $vaData) {
			$idbarang = $vaData['id_barang'];
			$rusaksebelumnya = ($vaData['rusak'] == "" ? 0 : $vaData['rusak']);
		}

		$queryStock = $this->model->code("SELECT * FROM tb_stock_kemasan WHERE id_barang = '" . $idbarang . "'");
		foreach ($queryStock as $key => $vaRow) {
			$rusakawal = $vaRow['rusak'];
		}
		$rusakall = $rusakawal - $rusaksebelumnya + $this->input->post('terimrusak');
		$dataStock = array(
			'rusak'		=> $rusakall,
		);

		$this->model->update_rusak($this->input->post('terimrusak'), $tanggal, $idbarang, $rusaksebelumnya);
		$this->model->Update('terima_kemasan', 'id_terima_kemasan', $this->input->post('kodepo'), $data);

		$this->model->Update('tb_stock_kemasan', 'id_barang', $idbarang, $dataStock);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('Administrator/Stock/terima_kemasan/' . $this->input->post('kode') . ''));
	}

	public function terima_print_edit($Type = "", $id = "")
	{
		$this->db->trans_begin();
		$data = array(
			'baik'		=> $this->input->post('terimabagus'),
			'rusak'		=> $this->input->post('terimrusak'),
		);
		$query = $this->model->code("SELECT * FROM terima_print WHERE id_terima_kemasan = '" . $this->input->post('kodepo') . "' ");
		foreach ($query as $key => $vaData) {
			$idbarang = $vaData['id_barang'];
			$rusaksebelumnya = ($vaData['rusak'] == "" ? 0 : $vaData['rusak']);
		}

		$queryStock = $this->model->code("SELECT * FROM tb_stock_kemasan WHERE id_barang = '" . $idbarang . "'");
		foreach ($queryStock as $key => $vaRow) {
			$rusakawal = $vaRow['rusak'];
		}
		$rusakall = $rusakawal - $rusaksebelumnya + $this->input->post('terimrusak');
		$dataStock = array(
			'rusak'		=> $rusakall,
		);


		$this->model->Update('terima_print', 'id_terima_kemasan', $this->input->post('kodepo'), $data);

		$this->model->Update('tb_stock_kemasan', 'id_barang', $idbarang, $dataStock);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('Administrator/Stock/terima_print/' . $this->input->post('kode') . ''));
	}

	public function terima_produk_edit($Type = "", $id = "")
	{
		$this->db->trans_begin();
		$data = array(
			'baik'		=> $this->input->post('terimabagus'),
			'rusak'		=> $this->input->post('terimrusak'),
		);

		$query = $this->model->code("SELECT * FROM terima_produk WHERE id_terima_kemasan = '" . $this->input->post('kodepo') . "' ");
		foreach ($query as $key => $vaData) {
			$idbarang = $vaData['id_barang'];
			$rusaksebelumnya = ($vaData['rusak'] == "" ? 0 : $vaData['rusak']);
			$baiksebelumnya = ($vaData['baik'] == "" ? $vaData['jumlah'] : $vaData['baik']);
		}

		$query = $this->model->ViewWhere('tb_stock_produk', 'id_barang', $this->input->post('idProduk'));
		foreach ($query as $key => $vaData) {
			$jumlahBaik = $vaData['jumlah'];
			$jumlahBuruk = $vaData['rusak'];
		}

		$jBaik 	= $this->input->post('terimabagus') + ($jumlahBaik - $baiksebelumnya);
		$jRusak = $this->input->post('terimrusak') + ($jumlahBuruk - $rusaksebelumnya);

		$dataStock = array(
			'jumlah'	=> $jBaik,
			'rusak'		=> $jRusak,
		);

		$this->model->Update('terima_produk', 'id_terima_kemasan', $this->input->post('kodepo'), $data);
		$this->model->Update('tb_stock_produk', 'id_barang', $this->input->post('idProduk'), $dataStock);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('Administrator/Stock_Produk/terima_produk/' . $this->input->post('kode') . ''));
	}

	public function cetak_stock_barang_real()
	{


		$data['row']	= $this->model->ViewAsc('v_stock_produk', 'id_barang');

		$html			= $this->load->view('back-end/stock/cetak/barang-cetak-stock-actual', $data, true);

		$pdfFilePath 	= "cetak stock kemasan actual.pdf";

		//load mPDF library
		$this->m_pdf->pdf->mPDF('', 'A4', '10', 'ctimesbi', '5', '5', '5', '5', '5', '10', 'P');
		$this->m_pdf->pdf->useOddEven = true;

		//generate the PDF from the given html
		$this->m_pdf->pdf->WriteHTML($html);


		//download it.
		$this->m_pdf->pdf->Output($pdfFilePath, 'I');
	}
}
