

<script src="<?=base_url()?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery-ui.min.js"></script>
<script src="<?=base_url()?>assets/js/tether.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/plugins/waves/js/waves.min.js"></script>
<script src="<?=base_url()?>assets/plugins/slimscroll/js/jquery.slimscroll.js"></script>
<script src="<?=base_url()?>assets/plugins/slimscroll/js/jquery.nicescroll.min.js"></script>
<script src="<?=base_url()?>assets/plugins/notification/js/bootstrap-growl.min.js"></script>
<script src="<?=base_url()?>assets/js/menu.js"></script>
<script src="<?=base_url()?>assets/js/main.js" type="text/javascript" ></script> 
<script type="text/javascript" src="<?=base_url()?>assets/pages/elements.js"></script>
<script src="<?=base_url()?>assets/plugins/data-table/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/plugins/data-table/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>assets/plugins/data-table/js/jszip.min.js"></script>
<script src="<?=base_url()?>assets/plugins/data-table/js/pdfmake.min.js"></script>
<script src="<?=base_url()?>assets/plugins/data-table/js/vfs_fonts.js"></script>
<script src="<?=base_url()?>assets/plugins/data-table/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>assets/plugins/data-table/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>assets/plugins/data-table/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/plugins/data-table/js/dataTables.responsive.min.js"></script>
<script src="<?=base_url()?>assets/plugins/data-table/js/responsive.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/plugins/datepicker/js/moment-with-locales.min.js"></script>
<script src="<?=base_url()?>assets/plugins/datepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="<?=base_url()?>assets/plugins/sweetalert/js/sweetalert.js"></script>
<script src="<?=base_url()?>assets/plugins/toastr/toastr.min.js"></script>
<script src="<?=base_url()?>assets/plugins/select2/js/select2.full.min.js"></script>


<script type="text/javascript">
	$('#multi-colum-dt').DataTable({
        pageLength: 100,
        columnDefs: [{
            targets: [0],
            orderData: [0, 1]
        }, {
            targets: [1],
            orderData: [1, 0]
        },]
    });
   
   	$('#cIdStock').select2({  
	   allowClear: true,
	   placeholder: 'Pilih Nama Stock Barang',
	  });
   	$('#pilihSupplier').select2({  
	   allowClear: true,
	   placeholder: 'Pilih Supplier',
	  });
    $('#pilihPabrik').select2({  
       allowClear: true,
       placeholder: 'Pilih Pabrik',
      });
    $('#pilihBulan').select2({  
       allowClear: true,
       placeholder: 'Pilih Bulan',
      });
    $('#pilihTahun').select2({  
       allowClear: true,
       placeholder: 'Pilih Tahun',
      });
    $('#pilihBulanDua').select2({  
       allowClear: true,
       placeholder: 'Pilih Bulan',
      });
    $('#pilihTahunDua').select2({  
       allowClear: true,
       placeholder: 'Pilih Tahun',
      });
    $('.pilihbulan').select2({  
       allowClear: true,
       placeholder: 'Pilih Bulan',
      });
    $('.pilihtahun').select2({  
       allowClear: true,
       placeholder: 'Pilih Tahun',
      });
    $('#pilihSeller').select2({  
       allowClear: true,
       placeholder: 'Pilih Seller Msglow',
      });
    $('#pilihrekening').select2({  
     allowClear: true,
     placeholder: 'Pilih Rekening Penerimaan',
    });
    $('#pilihproduk').select2({  
     allowClear: true,
     placeholder: 'Pilih Produk',
    });
    $('#pilihprodukJL').select2({  
     allowClear: true,
     placeholder: 'Pilih Produk',
    });
    
</script>

</body>
</html>
