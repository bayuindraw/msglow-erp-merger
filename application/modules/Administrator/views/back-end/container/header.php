<?php 
  if ($this->session->userdata('isLogin') == "" ) {
    redirect(site_url('Administrator/Master/signin'));
  }
 function String2Date($dTgl){
      //return 22-11-2012  
      list($cYear,$cMount,$cDate) = explode("-",$dTgl) ;
      if(strlen($cYear) == 4){
        $dTgl = $cDate . "-" . $cMount . "-" . $cYear ;
      } 
      return $dTgl ;  
    }
?>
<html>
<head>
  <title><?=$title?></title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="description" content="Phoenixcoded">
  <meta name="keywords"
  content=", Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
  <meta name="author" content="Phoenixcoded">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="<?=base_url()?>assets/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?=base_url()?>assets/images/favicon.ico" type="image/x-icon">

    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <!-- iconfont -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/icon/icofont/css/icofont.css">

    <!-- simple line icon -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/icon/simple-line-icons/css/simple-line-icons.css">

    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap.min.css">

    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/main.css">

    <!-- Responsive.css-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/responsive.css">

    <!--color css-->
  	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/color/color-1.css" id="color"/>

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/data-table/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/data-table/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/sweetalert/css/sweetalert.css">

      <!-- Sweetalert CSS -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/sweetalert/css/sweetalert.css">

    <!-- Modal animation CSS -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/modal/css/component.css">


    <!-- Notification -->
    <link href="<?=base_url()?>assets/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Date Picker css -->
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/datepicker/css/bootstrap-material-datetimepicker.css" />

    <script src="https://ecomfe.github.io/echarts-examples/public/vendors/echarts/echarts-all-3.js?_v_=1510583857922"></script>


    <!-- Select 2 css -->
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/select2/css/s2-docs.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/widget.css">
    <script src="<?=base_url()?>assets/pages/modal.js"></script>

    <!-- notification -->
    <script src="<?=base_url()?>assets/plugins/notification/js/bootstrap-growl.min.js"></script>

    <!-- Sweetalert js -->
    <script src="<?=base_url()?>assets/plugins/sweetalert/js/sweetalert.js"></script>

    <!-- Model animation js -->
    <script src="<?=base_url()?>assets/plugins/modal/js/classie.js"></script>
    <script src="<?=base_url()?>assets/plugins/modal/js/modalEffects.js"></script>




</head>
<body class="sidebar-mini fixed">
 <div class="wrapper">
     <header class="main-header-top hidden-print">
        <a href="<?=base_url()?>" class="logo"><img class="img-fluid able-logo" src="<?=base_url()?>assets/images/logo-office-putih.png" alt="Theme-logo"></a>
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button--><a href="#!" data-toggle="offcanvas" class="sidebar-toggle"></a>
          <!-- Navbar Right Menu-->
          <div class="navbar-custom-menu">
            <ul class="top-nav">
              <!--Notification Menu-->

              <li class="dropdown pc-rheader-submenu message-notification search-toggle">
                <a href="#!" id="morphsearch-search" class="drop icon-circle txt-white">
                    <i class="icofont icofont-search-alt-1"></i>
                </a>
              </li>
              <!-- chat dropdown -->
              <li class="pc-rheader-submenu ">
                        <a href="#!" class="drop icon-circle displayChatbox">
                            <i class="icon-bell"></i>
                            <span class="badge badge-danger header-badge blink">5</span>
                        </a>

                    </li>
              
              <!-- User Menu-->
              <li class="dropdown">
                <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle drop-image">
                              <span><img class="img-circle " src="<?=base_url()?>assets/images/avatar-1.png" style="width:40px;" alt="User Image"></span>
                              <span><?=$this->session->userdata('nama')?><i class=" icofont icofont-simple-down"></i></span>

              </a>
                <ul class="dropdown-menu settings-menu">
                  <li><a href="profile.html"><i class="icon-user"></i> Akun</a></li>
                  <li><a href="message.html"><i class="icon-envelope-open"></i> Aktivitas</a></li>
                  <li class="p-0"><div class="dropdown-divider m-0"></div></li>         
                  <li><a href="<?=base_url()?>Administrator/Master_Act/logout"><i class="icon-logout"></i> Logout</a></li>

                </ul>
              </li>
            </ul>

              
          </div>
        </nav>
      </header>
       <!-- Side-Nav-->
      <aside class="main-sidebar hidden-print " >
        <section class="sidebar" id="sidebar-scroll">
          <div class="user-panel">
            <div class="f-left image"><img src="<?=base_url()?>/assets/images/avatar-1.png" alt="User Image" class="img-circle"></div>
            <div class="f-left info">
              <p><?=$this->session->userdata('nama')?></p>
              <p class="designation"><?=$this->session->userdata('id_user')?><i class="icofont icofont-caret-down m-l-5"></i></p>
            </div>
          </div>
          <!-- sidebar profile Menu-->
          <ul class="sidebar-menu">
            
            <?php 
              if($this->session->userdata('level') == '1') {
            ?>  
                  <?php
                    include('header-admin.php');
                  ?>
            <?php 
                }elseif($this->session->userdata('level') == '4'){ 
            ?>
              
                  <?php
                    include('header-kemasan.php');
                  ?>

            <?php 
                }elseif($this->session->userdata('level') == '5'){
            ?>
                  <?php
                    include('header-produk.php');
                  ?>
            <?php 
                }elseif($this->session->userdata('level') == '6'){
            ?>
                  <?php
                    include('header-marketing.php');
                  ?>
            <?php 
                }elseif($this->session->userdata('level') == '7'){
            ?>
                  <?php
                    include('header-accounting.php');
                  ?>
            <?php } ?>
            
            <br><br><br>
          </ul>
        </section>
      </aside>
      <!-- Sidebar chat start -->
       <div id="sidebar" class="p-fixed header-users showChat">
            <div class="had-container">
                <div class="card card_main header-users-main">
                    <div class="card-content user-box">
                        <br>
                        <div class="media friendlist-main">

                            <h6>Aktivitas User</h6>

                        </div>
                        <div class="main-friend-list">
                          <div class="media friendlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">

                              <a class="media-left" href="#!">
                                  <img class="media-object img-circle" src="<?=base_url()?>assets/images/avatar-1.png" alt="Generic placeholder image">
                                  <div class="live-status bg-success"></div>
                              </a>
                              <div class="media-body">
                                  <div class="friend-header">Josephin Doe</div>
                                  <span>20min ago</span>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
