
<link rel="stylesheet" type="text/css" href="http://165.22.105.2/assets/css/main.css">
<style type="text/css">
	.table1 {
    font-family: sans-serif;
    color: black;
    border-collapse: collapse;
	}
	 
	.table1, th, td {
	    border: 1px solid black;
	    padding: 3px 10px;
	}
</style>
<?php
		
	$bulan = array(
                '01' => 'JANUARI',
                '02' => 'FEBRUARI',
                '03' => 'MARET',
                '04' => 'APRIL',
                '05' => 'MEI',
                '06' => 'JUNI',
                '07' => 'JULI',
                '08' => 'AGUSTUS',
                '09' => 'SEPTEMBER',
                '10' => 'OKTOBER',
                '11' => 'NOVEMBER',
                '12' => 'DESEMBER',
        );

	 function DateTimeStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("d-m-Y h:i:s");
			return $Data ;
		} 
?>
<h2 align="center" style="color:black">LAPORAN DETAIL PO KEMASAN MS GLOW</h2>
<h3 align="center" style="color:black"><i>PERIODE <?=$bulan[$mbulan]?></i></h3>
<div align="right" style="margin-bottom: 10px">
<span style="font-size: 10px">Tanggal Cetak : <?=DateTimeStamp()?></span>
</div>

<table border="1" style="width:100%;font-size: 12px" class="table1">
	<tr style="background-color: #95fffd">
		<td align="center" style="width:20%"><b>NAMA KEMASAN</b></td>
		<td align="center" style="width:15%"><b>NAMA SUPPLIER</b></td>
		<td align="center" style="width:15%"><b>QTY ORDER</b></td>
		<td align="center" style="width:15%"><b>QTY TERIMA</b></td>
		<td align="center" style="width:15%"><b>QTY KURANG</b></td>
	</tr>
	<?php 

		foreach ($row as $key => $vaData) {
		
		$queryJum = $this->model->code("SELECT sum(jumlah) as totalbayar FROM terima_kemasan WHERE kode_po = '".$vaData['kode_pb']."' AND id_barang = '".$vaData['id_barang']."'");
	      foreach ($queryJum as $key => $vaBayar) {
	        $totalterima = $vaBayar['totalbayar'];
	      }
	?>
	<tr>
		<td><?=$vaData['nama_kemasan']?></td>
		<td><?=$vaData['nama_supplier']?></td>
		<td align="center"><?=number_format($vaData['jumlah'])?></td>
		<td align="center"><?=number_format($totalterima)?></td>
		<td align="center"><?=number_format($vaData['jumlah']-$totalterima)?></td>
	</tr>
   <?php }?>
</table>