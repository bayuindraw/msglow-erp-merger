							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											Pengiriman Kemasan
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<a href="<?= site_url() ?>Administrator/Stock/pengeluaran_kemasan_new/<?=$kode?>" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Tambah
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<div dir  id="dir" content="table">
                     <table id="multi-colum-dt" class="table table-striped table-bordered nowrap">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Kode Pengiriman</th>
                            <th>Nama Pabrik</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Tanggal Pengiriman</th>
                            <th>User</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['kode_kluar']?></td>
                            <td><?=$vaData['kode_factory']?></td>
                            <td><?=substr($vaData['alamat'],0,20)?></td>
                            <td><?=$vaData['telepon']?></td>
                            <td><?=$vaData['tgl_kirim']?></td>
                            <td><?=$vaData['user']?></td>
                            <td>
                                <a href="<?=base_url()?>Administrator/Stock/edit_pengeluaran_print/<?=$kode?>/<?=str_replace('/', '|', $vaData['kode_kluar'])?>" class="btn btn-primary waves-effect waves-light" ><i class="icofont icofont-ui-edit flaticon2-edit"></i></a>
                                <a href="<?=base_url()?>Administrator/Stock/cetaksuratjalankemasan/<?=$vaData['id_kluar_kemasan']?>" target="_blank" class="btn btn-danger waves-effect waves-light" ><i class="icon-printer flaticon2-print"></i></a>
                            </td>
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                   </div>

									<!--end: Datatable -->
								</div>
							</div> 
							
					