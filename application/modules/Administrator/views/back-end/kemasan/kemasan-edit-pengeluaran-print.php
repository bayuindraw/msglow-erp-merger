<div class="row">
								<div class="col-lg-12 col-xl-12">
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Form Input Pengeluaran Kemasan (<?= $row[0]['nama_factory'] ?>)
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">
										<form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock_Act/add_edit_pengeluaran_detail_print/<?= $id ?>/<?=str_replace('/', '|', $idkluar)?>" method="Post">
												<input type="hidden" name="id" value="<?=$id?>">
												<div class="form-group">
													<label>Kode Pengeluaran Kemasan</label>
													<input type="text" id="cKodePo" name="KodePurchaseOrder" 
                      class="form-control static" value="<?=$kodepo?>" >
												</div>
												<div class="form-group">
													<label>Tanggal Pengiriman</label>
													<input type="text" id="dTglPo" name="TanggalOrder" 
                      class="form-control static floating-label 4IDE-date date_picker" value="<?=$tanggal?>">
												</div>
												<?php if($row[0]['id_pabrik'] != ""){ ?>
													<input type="hidden" name="Supplier" value="<?= $row[0]['id_pabrik']; ?>" >
 												<?php }else{ ?>
												<div class="form-group">
													<label>Pilih Pabrik</label>
													<select name="Supplier" id="pilihPabrik" class="form-control static">

                          <option></option>
                          <?php
                            $query = $this->model->ViewAsc('factory','id_factory');
                            foreach ($query as $key => $vaSupplier) {
                          ?>
                          <option value="<?=$vaSupplier['id_factory']?>" <?php if($factory == $vaSupplier['id_factory']){?> selected <?php } ?>><?=$vaSupplier['nama_factory']?></option>
                         <?php } ?>
                         </select>
												</div>
																				<?php } ?>
												<div class="form-group">
													<label>Nama Barang</label>
													<select name="NamaBarang" id="cIdStock" class="form-control static">

                          <option></option>
                          <?php
                            $query = $this->model->ViewAsc('v_stock_kemasan_new','id_barang');
                            foreach ($query as $key => $vaKemasan) {
                          ?>
                          <option value="<?=$vaKemasan['id_barang']?>"><?=$vaKemasan['nama_kemasan']?> (<?=$vaKemasan['kode_sup']?>)</option>
                         <?php } ?>
                         </select>
												</div>
												<div class="form-group">
													<label>Jumlah Pengiriman (PCS)</label>
													<input type="text" id="cJumlah" name="JumlahPo" value="0" onkeyup="return getTotal();" onchange="getTotal();" 
                       class="form-control static">
												</div>
                        <div class="md-input-wrapper">     
                         <button type="submit" class="btn btn-primary waves-effect waves-light " title="Tambahkan Kemasan">
                         <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Tambahkan Pengeluaran Kemasan</span>
                         </button>
                     </div>
                      </form>
					  
												<br>
												
                          <div dir  id="dir" content="table_stock">
                        <table class="table table-striped table-bordered nowrap" style="font-size: 13px">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Kemasan</th>
                            <th>Jumlah Order</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['nama_kemasan']?></td>
                            <td><?=number_format($vaData['jumlah'])?> Pcs</td>
                            <td>
                                <a type="button" class="btn btn-danger waves-effect waves-light" href="<?=base_url()?>Administrator/Stock_Act/hapus_detail_kluar_print/<?=$vaData['id_pokemasan']?>/<?= $id ?>/<?=str_replace('/','|',$idkluar)?>"><i class="icofont icofont-ui-delete flaticon2-trash"></i></a>
                            </td>
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                      </div>
					  <div id="btn-pb">
                          <!--<form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock_Act/add_kluar_kemas/<?= $id ?>" method="Post">-->
                          <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock/data_pengeluaran_print/<?= $id ?>" method="Post">
                          <input type="hidden" name="tanggalpo" value="<?=$this->session->userdata('tanggal_po')?>">
                          <input type="hidden" name="pabrik" value="<?=$this->session->userdata('factory')?>">
                            <button type="submit" class="btn btn-success waves-effect waves-light">
                             SIMPAN PENGELUARAN KEMASAN
                          </button>
                          </form>
                      </div>
					  
					  
                      </div>
                      </div>
                      </div>
                      </div>
						 
