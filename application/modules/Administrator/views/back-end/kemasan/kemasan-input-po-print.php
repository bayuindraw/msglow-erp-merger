<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Form Input Purchase Order Print
          </h3>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">



      <div class="kt-portlet__body">


        <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Stock_Act/add_print" method="Post">

          <div class="form-group">
            <i class="fas fa-barcode"></i>
            <label>Kode Purchase Order</label>
            <input type="text" id="cKodePo" name="kodepo" class="form-control md-form-control md-static" value="<?= $kodepo ?>">
          </div>
          <div class="form-group">
            <i class="far fa-calendar-alt"></i>
            <label>Tanggal Purchase Order</label>
            <input type="text" id="dTglPo" name="tanggal_po" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= $this->session->userdata('tanggal_po') ?>" required>
            <input type="hidden" id="cIdOutlet" name="NamaOutlet" class="md-form-control md-static floating-label" value="1">
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <input type="hidden" name="supplier" value="<?= $this->session->userdata('supplier') ?>">
              <button type="submit" class="btn btn-success waves-effect waves-light">
                SIMPAN PO
              </button>
              <a href="<?= base_url() ?>Administrator/Stock/bersih_data_print" class="btn btn-warning waves-effect waves-light">
                BERSIHKAN DATA
              </a>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>



</div>
</div>
</div>