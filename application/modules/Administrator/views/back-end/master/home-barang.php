
  <div class="col-xl-12 p-0">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h4>ERP MSGLOW</h4>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-header">
          <h5>STOCK BARANG JADI <?= date('d-F-Y') ?></h5>
        </div>
        <div class="card-block">
          <table class="table" style="font-size: 13px">
            <thead>
              <th>Nama Produk</th>
              <th>Jumlah Baik</th>
              <th>Jumlah Rusak</th>
            </thead>
            <tbody>
              <?php
              foreach ($kemasan as $key => $vaData) {
				if($vaData['tipe'] == "1"){
			  ?>
                <tr>
                  <td><?= $vaData['nama_produk'] ?></td>
                  <td><?= number_format($vaData['jumlah']) ?></td>
                  <td><?= number_format($vaData['rusak']) ?></td>
                </tr>
              <?php
				}
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="card">
        <div class="card-header">
          <h5>STOCK GUDANG <?= date('d-F-Y') ?></h5>
        </div>
        <div class="card-block">
          <table class="table" style="font-size: 13px">
            <thead>
              <th>Nama Produk</th>
              <th>Jumlah Baik</th>
              <th>Jumlah Rusak</th>
            </thead>
            <tbody>
              <?php
              foreach ($kemasan as $key => $vaData) {
				if($vaData['tipe'] == "2"){
			  ?>
                <tr>
                  <td><?= $vaData['nama_produk'] ?></td>
                  <td><?= number_format($vaData['jumlah']) ?></td>
                  <td><?= number_format($vaData['rusak']) ?></td>
                </tr>
              <?php
				}
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div> 