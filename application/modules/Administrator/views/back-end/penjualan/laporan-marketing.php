<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Cetak Laporan Marketing
          </h3>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN PENJUALAN HARIAN
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Penjualan/laporan_penjualan_harian" method=" Post" target="_blank">
          <div class="form-group">
            <label>Pilih Tanggal</label>
            <input type="text" id="cTanggal" name="cTanggal" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= $this->session->userdata('tanggal') ?>" placeholder="Pilih Tanggal Penjualan">
            <input type="hidden" id="cIdOutlet" name="NamaOutlet" class="md-form-control md-static floating-label" value="1">
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>

  <div class="col-lg-6 col-xl-6">
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN PENJUALAN PERIODE BULAN
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">


        <label>Pilih Bulan</label>
        <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Penjualan/laporan_penjualan_bulanan" method="Post" target="_blank">
          <div class="form-group">
            <select name="cBulan" id="pilihBulanDua" class="form-control">
              <option></option>
              <option value="01">Januari</option>
              <option value="02">Februari</option>
              <option value="03">Maret</option>
              <option value="04">April</option>
              <option value="05">Mei</option>
              <option value="06">Juni</option>
              <option value="07">Juli</option>
              <option value="08">Agustus</option>
              <option value="09">September</option>
              <option value="10">Oktober</option>
              <option value="11">November</option>
              <option value="12">Desember</option>
            </select>
          </div>
          <label>Pilih Tahun</label>
          <div class="form-group">
            <select name="cTahun" id="pilihTahunDua" class="form-control">
              <option></option>
              <option value="2019">2019</option>
              <option value="2020">2020</option>
              <option value="2021">2021</option>
            </select>
          </div>

          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-success waves-effect waves-light " title="Tampilkan Stock">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span>
            </div>
          </div>
        </form>
      </div>



    </div>
  </div>

  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN PENJUALAN PER SELLER
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <label>Pilih Seller</label>
        <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Penjualan/laporan_penjualan_customer" method="Post" target="_blank">

          <div class="form-group">
            <select name="cSupplier" id="pilihSeller" class="form-control">
              <option value=""></option>
              <option value="ALL">SEMUA SELLER</option>
              <?php
              $query = $this->model->ViewAsc('member', 'kode');
              foreach ($query as $key => $vaSupplier) {
              ?>
                <option value="<?= $vaSupplier['kode'] ?>"><?= $vaSupplier['kode'] ?> / <?= $vaSupplier['nama'] ?></option>

              <?php } ?>
            </select>
          </div>

          <label>Pilih Bulan</label>
          <div class="form-group">
            <select name="cBulan" id="pilihBulanDua" class="form-control pilihbulan">
              <option></option>
              <option value="01">Januari</option>
              <option value="02">Februari</option>
              <option value="03">Maret</option>
              <option value="04">April</option>
              <option value="05">Mei</option>
              <option value="06">Juni</option>
              <option value="07">Juli</option>
              <option value="08">Agustus</option>
              <option value="09">September</option>
              <option value="10">Oktober</option>
              <option value="11">November</option>
              <option value="12">Desember</option>
            </select>
          </div>

          <label>Pilih Tahun</label>
          <div class="form-group">
            <select name="cTahun" id="pilihTahunDua" class="form-control pilihtahun">
              <option></option>
              <option value="2019">2019</option>
              <option value="2020">2020</option>
              <option value="2021">2021</option>
            </select>
          </div>

          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-warning waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Tampilkan Stock" onclick="showKartuStok()">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span></button>
            </div>
          </div>
        </form>
      </div>



    </div>
  </div>
  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN PER PRODUK
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <label>Pilih Produk</label>
        <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Penjualan/laporan_penjualan_produk" method="Post" target="_blank">

          <div class="form-group">
            <select name="cSupplier" id="pilihproduk" class="form-control">
              <option value=""></option>
              <option value="ALL">SEMUA PRODUK</option>
              <?php
              $query = $this->model->ViewAsc('produk', 'id_produk');
              foreach ($query as $key => $vaSupplier) {
              ?>
                <option value="<?= $vaSupplier['id_produk'] ?>"><?= $vaSupplier['nama_produk'] ?></option>

              <?php } ?>
            </select>
          </div>

          <label>Pilih Bulan</label>
          <div class="form-group">
            <select name="cBulan" id="pilihBulanDua" class="form-control pilihbulan">
              <option></option>
              <option value="01">Januari</option>
              <option value="02">Februari</option>
              <option value="03">Maret</option>
              <option value="04">April</option>
              <option value="05">Mei</option>
              <option value="06">Juni</option>
              <option value="07">Juli</option>
              <option value="08">Agustus</option>
              <option value="09">September</option>
              <option value="10">Oktober</option>
              <option value="11">November</option>
              <option value="12">Desember</option>
            </select>
          </div>

          <label>Pilih Tahun</label>
          <div class="form-group">
            <select name="cTahun" id="pilihTahunDua" class="form-control pilihtahun">
              <option></option>
              <option value="2019">2019</option>
              <option value="2020">2020</option>
              <option value="2021">2021</option>
            </select>
          </div>

          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-danger waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Tampilkan Stock" onclick="showKartuStok()">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span></button>
            </div>
          </div>
        </form>
      </div>



    </div>
  </div>
</div>