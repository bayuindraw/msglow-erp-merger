<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$file?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">UI Elements</a>
                    </li>
                    <li class="breadcrumb-item"><a href="panels-wells.html">Panels Wells</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-lg-12">

    <!-- Row start -->
    <div class="row">

      <div class="col-lg-12">

        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">DETAIL PO : <?=$action?></h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php 

                      $query = $this->model->ViewWhere('v_po_kemasan','kode_po',$action);
                      foreach ($query as $key => $vaDetail) {
                        $kodePo = $vaDetail['kode_po'];
                        $supplier = $vaDetail['nama_supplier'];
                        $tanggal = $vaDetail['tanggal'];

                      }

                    ?>
                    <div class="tab-pane active" id="data" role="tabpanel">
                        <div class="row">
                          <div class="col-sm-6">
                            <h5>Detail Purchase Order: </h5>
                            <table class="table table-striped table-bordered" width="50%">
                              <tr>
                                <td>Kode Po</td> <td>:</td> <td><?=$kodePo?></td>
                              </tr>
                              <tr>
                                <td>Supplier</td> <td>:</td> <td><?=$supplier?></td>
                              </tr>
                              <tr>
                                <td>Tanggal PO</td> <td>:</td> <td><?=$tanggal?></td>
                              </tr>
                           </table>
                          </div>
                          <div class="col-sm-6">
                            <h4 style="color:white">Detail Purchase Order: </h4>
                            <table class="table table-striped table-bordered" width="50%">
                              <tr>
                                <td>User Po</td> <td>:</td> <td></td>
                              </tr>
                              <tr>
                                <td>Bulan Po</td> <td>:</td> <td><?=date('m')?></td>
                              </tr>
                              <tr>
                                <td>Tahun PO</td> <td>:</td> <td><?=date('Y')?></td>
                              </tr>
                           </table>
                          </div>
                          <div class="col-sm-12">
                          <hr>
                          <h4>Detail Kemasan : </h4>
                          <table class="table table-striped table-bordered nowrap" style="font-size: 14px">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Nama Kemasan</th>
                                <th>Jumlah Pembelian</th>
                                <th>Total Terima</th>
                                <th>Retur</th>
                                <th>Kurang Terima</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $no=0;
                                foreach ($row as $key => $vaData) {

                                $query = $this->model->code("SELECT sum(jumlah) as terimabarang FROM terima_kemasan WHERE kode_po = '".$action."' AND id_barang = '".$vaData['id_barang']."'");
                                foreach ($query as $key => $vaTerima) {
                                  $terimaPO = $vaTerima['terimabarang'];
                                }

                              ?>
                              <tr>
                                <td><?=++$no?></td>
                                <td><?=$vaData['nama_kemasan']?></td>
                                <td><?=number_format($vaData['jumlah'])?> Pcs</td>
                                <td><?=number_format($terimaPO)?> Pcs</td>
                                <td>0 Pcs</td>
                                <td><?=number_format($vaData['jumlah']-$terimaPO)?> Pcs</td>
                              </tr>
                             <?php } ?>
                            </tbody>
                         </table>
                        </div>

                        </div>
                    </div>
                  </div>
                 
              </div>

            </div>
            <!-- Row end -->
          </div>


        </div>
        <div class="col-sm-6">
        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">PENERIMAAN KEMASAN KODE PO : <?=$action?></h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->

                <!-- Tab panes -->
                <div class="tab-content">
                  
                    <div class="tab-pane active" id="data" role="tabpanel">
                        <div class="row">
                          <div class="col-sm-12">
                            <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Tanggal Terima</th>
                                <th>Nama Kemasan</th>
                                <th>Jumlah</th>
                                <th>User</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $no=0;
                                foreach ($terima as $key => $vaData) {
                              ?>
                              <tr>
                                <td><?=++$no?></td>
                                <td><?=$vaData['tgl_terima']?></td>
                                <td><?=$vaData['nama_kemasan']?></td>
                                <td><?=number_format($vaData['jumlah'])?> Pcs</td>
                                <td><?=$vaData['user']?></td>
                              </tr>
                             <?php } ?>
                             
                            </tbody>
                         </table>
                        </div>

                        </div>
                    </div>
                  </div>
                 
              </div>

            </div>
            <!-- Row end -->
          </div>

          
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">PEMBAYARAN KEMASAN KODE PO : <?=$action?></h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->

                <!-- Tab panes -->
                <div class="tab-content">
                  
                    <div class="tab-pane active" id="data" role="tabpanel">
                        <div class="row">
                          <div class="col-sm-12">
                            <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Tanggal Bayar</th>
                                <th>Bank</th>
                                <th>User</th>
                                <th>Jumlah Pembayaran</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $no=0;
                                foreach ($bayar as $key => $vaData) {
                              ?>
                              <tr>
                                <td><?=++$no?></td>
                                <td><?=$vaData['tgl_bayar']?></td>
                                <td><?=$vaData['bank']?></td>
                                <td><?=$vaData['user']?></td>
                                <td>Rp. <?=number_format($vaData['jumlah'])?></td>
                              </tr>
                             <?php } ?>
                             
                            </tbody>
                         </table>
                           </div>

                        </div>
                    </div>
                  </div>
                 
              </div>

            </div>
            <!-- Row end -->
          </div>

          
        </div>
      </div>
      </div>
    </div>
    <!-- Row end -->
  </div>
</div>
<!-- loader ends -->
</div>
<script type="text/javascript">
  

</script>