<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$menu?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Stock Kemasan</a>
                    </li>
                    <li class="breadcrumb-item"><a href="panels-wells.html">Pengeluaran Kemasan</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-lg-12">
    <!-- Row start -->
    <div class="row">

      <div class="col-lg-12">

        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">Form Input Pengeluaran Kemasan</h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->

                <!-- Nav tabs -->
                <ul class="nav nav-tabs md-tabs " role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#data" role="tab">
                      <i class="icon-grid"></i> &nbsp;&nbsp; Form Input Pengeluaran Kemasan</a>
                      <div class="slide">
                        
                      </div>
                  </li>
                  
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                  <br/> <br/>
          <div class="tab-pane active" id="data" role="tabpanel">
              <div class="col-sm-6 col-xs-12">
                  <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock_Act/add_edit_pengeluaran_detail_kemasan/<?=$idkluar?>" method="Post">
                   
                    <div class="md-group-add-on p-relative">
                     <span class="md-add-on">
                      <i class="icofont icofont-barcode"></i>
                    </span>
                    
                    <div class="md-input-wrapper">
                      <input type="text" id="cKodePo" name="KodePurchaseOrder" 
                      class="md-form-control md-static" value="<?=$kodepo?>" >
                      <label for="KodePurchaseOrder">Kode Pengeluaran Kemasan</label>
                      <span class="messages"></span>
                    </div>
                  </div>
                    <div class="md-group-add-on p-relative">
                     <span class="md-add-on">
                      <i class="icofont icofont-ui-calendar"></i>
                    </span>
                    <div class="md-input-wrapper">
                      <input type="text" id="dTglPo" name="TanggalOrder" 
                      class="md-form-control md-static floating-label 4IDE-date" value="<?=$tanggal?>">
                      <label for="KodeBarang">Tanggal Pengiriman</label>
                      <span class="messages"></span>
                      
                      </div>
                    </div>
                      <div class="md-group-add-on p-relative">
                         <span class="md-add-on">
                          <i class="icon-handbag"></i>
                        </span>
                        <div class="md-input-wrapper">
                         <select name="Supplier" id="pilihPabrik" class="md-form-control md-static">

                          <option></option>
                          <?php
                            $query = $this->model->ViewAsc('factory','id_factory');
                            foreach ($query as $key => $vaSupplier) {
                          ?>
                          <option value="<?=$vaSupplier['id_factory']?>" <?php if($factory == $vaSupplier['id_factory']){?> selected <?php } ?>><?=$vaSupplier['nama_factory']?></option>
                         <?php } ?>
                         </select>
                          <label for="Pilih Pabrik"></label>
                          <span class="messages"></span>
                        </div>
                      </div>
                      <div class="md-group-add-on p-relative">
                         <span class="md-add-on">
                          <i class="icofont icofont-ui-tag"></i>
                        </span>
                        <div class="md-input-wrapper">
                         <select name="NamaBarang" id="cIdStock" class="md-form-control md-static">

                          <option></option>
                          <?php
                            $query = $this->model->ViewAsc('v_stock_kemasan_new','id_barang');
                            foreach ($query as $key => $vaKemasan) {
                          ?>
                          <option value="<?=$vaKemasan['id_barang']?>"><?=$vaKemasan['nama_kemasan']?> (<?=$vaKemasan['kode_sup']?>)</option>
                         <?php } ?>
                         </select>
                          <label for="NamaBarang"></label>
                          <span class="messages"></span>
                        </div>
                      </div>

                      <div class="md-group-add-on p-relative">
                       <span class="md-add-on">
                        <i class="icofont icofont-plus-circle"></i>
                      </span>
                       <div class="md-input-wrapper">

                       <input type="text" id="cJumlah" name="JumlahPo" value="0" onkeyup="return getTotal();" onchange="getTotal();" 
                       class="md-form-control md-static">
                       <label for="JumlahPo">Jumlah Pengiriman (PCS)</label>
                       <span class="messages"></span>
                       </div>
                     </div>
                        <div class="md-input-wrapper">     
                         <button type="submit" class="btn btn-primary waves-effect waves-light " title="Tambahkan Kemasan">
                         <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Tambahkan Pengeluaran Kemasan</span>
                         </button>
                     </div>
                      </form>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                      <div dir  id="dir" content="table_stock">
                        <table class="table table-striped table-bordered nowrap" style="font-size: 13px">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Kemasan</th>
                            <th>Jumlah Order</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['nama_kemasan']?></td>
                            <td><?=number_format($vaData['jumlah'])?> Pcs</td>
                            <td>
                                <a type="button" class="btn btn-danger waves-effect waves-light" href="<?=base_url()?>Administrator/Stock_Act/hapus_detail_kluar/<?=$vaData['id_pokemasan']?>/<?=$idkluar?>"><i class="icofont icofont-ui-delete"></i></a>
                            </td>
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                      </div>
                      <div id="btn-pb">
                          <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock_Act/add_kluar_kemas" method="Post">
                          <input type="hidden" name="tanggalpo" value="<?=$this->session->userdata('tanggal_po')?>">
                          <input type="hidden" name="pabrik" value="<?=$this->session->userdata('factory')?>">
                            <button type="submit" class="btn btn-inverse-success waves-effect waves-light">
                             SIMPAN PENGELUARAN KEMASAN
                          </button>
                          </form>
                      </div>
                    </div>
                      <input type="hidden" name="aksi" value="simpan" id="aksi" name="aksi">
                      <input type="hidden" name="hapus" value="" id="hapus" name="hapus">
                      </div>
                    </div>


                  </div>
                 
              </div>

            </div>
            <!-- Row end -->
          </div>
        </div>
      </div>
    </div>
    <!-- Row end -->
  </div>
</div>
<!-- loader ends -->
</div>
<script type="text/javascript">
  

</script>