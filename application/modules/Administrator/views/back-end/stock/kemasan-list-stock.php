<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$menu?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="<?=base_url()?>"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Stock</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Stock Opname Kemasan</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-lg-12">

    <!-- Row start -->
    <div class="row">

      <div class="col-lg-12">

        <div class="card">

          <!-- Radio-Button start -->
          <div class="card-header"><h5 class="card-header-text">STOCK REAL KEMASAN</h5></div>
          <div class="card-block ">
            <!-- Row start -->
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-xs-12">
                <!-- <h6 class="sub-title">Tab With Icon</h6> -->
                <div class="tab-content">
                  
                  <div class="tab-pane active" id="data" role="tabpanel">
                  <a href="<?=base_url()?>Administrator/Stock/cetak_stock_kemasan_real" class="btn btn-primary waves-effect waves-light "> <i class="icon-printer"></i> <span class="m-l-10">Cetak Stock</span></a>
                  <hr>
                   <div dir  id="dir" content="table">
                     <table class="table table-striped table-bordered nowrap" id="multi-colum-dt">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Kemasan</th>
                            <th>Kategori</th>
                            <th>Stock</th>
                            <th>Rusak</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no=0;
                            foreach ($row as $key => $vaData) {
                          ?>
                          <tr>
                            <td><?=++$no?></td>
                            <td><?=$vaData['nama_kemasan']?> <label style="font-weight: 800;color: red;">(<?=$vaData['kode_sup']?>)</label></td>
                            <td><?=$vaData['kategori']?></td>
                            <td><?=number_format($vaData['jumlah'])?></td>
                            <td><?=number_format($vaData['rusak'])?></td>
                            <td>
                              <?php 

                                  if($vaData['jumlah'] <= '0'){
                                    $kemasan  = 'STOCK HABIS';
                                    $label    = 'label-danger';
                                  }elseif($vaData['jumlah'] > 0 || $vaData['jumlah'] < $vaData['min_jumlah']){
                                    $kemasan  = 'AKAN HABIS';
                                    $label    = 'label-warning';
                                  }elseif($vaData['jumlah'] > $vaData['min_jumlah']){
                                    $kemasan  = 'STOCK TERSEDIA';
                                    $label    = 'label-primary';
                                  }

                              ?>
                              <strong class="label <?=$label?>"><?=$kemasan?></strong>
                            </td>
                            
                          </tr>
                         <?php } ?>
                        </tbody>
                     </table>
                   </div> 
                   <span ng-bind="msg"></span>
                  </div>

              </div>

            </div>
            <!-- Row end -->
          </div>
        </div>
      </div>
    </div>
    <!-- Row end -->
  </div>
</div>
<!-- loader ends -->
</div>
