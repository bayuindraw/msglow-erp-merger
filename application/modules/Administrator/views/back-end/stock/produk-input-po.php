<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Form Input Purchase Order Barang Jadi
          </h3>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">

      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Stock_Act/add_detail_produk" method="Post">

          <div class="form-group">
            <i class="fas fa-barcode"></i>
            <label>Kode Purchase Order</label>
            <input type="text" id="cKodePo" name="KodePurchaseOrder" class="form-control md-form-control md-static" value="<?= $kodepo ?>">
          </div>
          <div class="form-group">
            <i class="far fa-calendar-alt"></i>
            <label>Tanggal Purchase Order</label>
            <input type="text" id="dTglPo" name="TanggalOrder" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= /*$this->session->userdata('tanggal_po')*/ @$row[0]['tanggal'] ?>" required>
            <input type="hidden" id="cIdOutlet" name="NamaOutlet" class="md-form-control md-static floating-label" value="1">
          </div>
          <div class="form-group">
            <i class="fas fa-shopping-bag"></i>
            <label>Pilih Pabrik</label>
            <?php if (@$row[0]['id_factory'] != "") {
              $arrfactoryx = $this->model->ViewWhere('factory', 'id_factory', @$row[0]['id_factory']);
            ?>
              <input type="hidden" name="Supplier" class="md-form-control md-static floating-label" value="<?= @$row[0]['id_factory'] ?>">
              <?= "<br>" . @$row[0]['nama_factory']; ?>
            <?php  } else { ?>
              <select name="Supplier" id="pilihPabrik" class="form-control md-form-control md-static" required onchange="return changeValue(this.value)">

                <option></option>
                <?php

                $query = $this->model->ViewAsc('factory', 'id_factory');
                foreach ($query as $key => $vaSupplier) {
					
                ?>
                  <option value="<?= $vaSupplier['id_factory'] ?>" <?php if ($this->session->userdata('factory') == $vaSupplier['id_factory']) { ?> selected <?php } ?>><?= $vaSupplier['nama_factory'] ?></option>

                <?php }
							?>
              </select>
            <?php  } ?>
          </div>
          <div class="form-group">
            <i class="fas fa-tag"></i>
            <label>Nama Barang</label>
            <?php

            if (empty($this->session->userdata('supplier'))) {
				
            ?>
              <select name="NamaBarang" id="cIdStock" class="md-form-control md-static subkategori" required>

                <option></option>
                <?php
                $query = $this->db->query("SELECT * FROM v_stock_produk WHERE status = 1 AND warehouse_id = '".$_SESSION['warehouse_id']."'")->result_array();;
                foreach ($query as $key => $vaKemasan) {
					if($vaKemasan['status'] == "1"){
				?>
                  <option value="<?= $vaKemasan['id_barang'] ?>"><?= $vaKemasan['nama_produk'] ?> (<?= (($vaKemasan['tipe'] == "1") ? "Barang Jadi" : "Gudang") ?>)</option>
                <?php }
				}				?>
              </select>
            <?php } ?>
          </div>
          <div class="form-group">
            <i class="fas fa-plus-circle"></i>
            <label>Jumlah Purchase Order (PCS)</label>
            <input type="text" id="cJumlah" name="JumlahPo" class="form-control" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="md-form-control md-static" required>
          </div>
          <!--<div class="form-group">
            <i class="fas fa-comments-dollar"></i>
            <label>Harga per-Pcs</label>
            <input type="text" id="cJumlah" name="Harga" class="form-control" value="0" class="md-form-control md-static" required>
          </div>-->
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " title="Tambahkan Barang Jadi">
                <i class="fas fa-pencil-alt"></i><span class="m-l-10"> Tambahkan Barang Jadi</span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">

      <div class="kt-portlet__body">


        <div dir id="dir" content="table_stock">
          <table class="table table-striped table-bordered nowrap" style="font-size: 13px">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Produk</th>
                <th>Jumlah Order</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 0;
              foreach ($row as $key => $vaData) {
              ?>
                <tr>
                  <td><?= ++$no ?></td>
                  <td><?= $vaData['nama_produk'] ?></td>
                  <td><?= number_format($vaData['jumlah']) ?> Pcs</td>
                  <td>
                    <a href="<?= base_url() ?>Administrator/Stock_Act/hapus_detail_order/<?= $vaData['id_pokemasan'] ?>/<?= $vaData['kode_pb'] ?>" type="button" class="btn btn-danger waves-effect waves-light"><i class="icofont icofont-ui-delete flaticon2-trash"></i><br />Hapus</a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div id="btn-pb">
          <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Stock_Act/add_po_produk" method="Post">
            <input type="hidden" name="tanggalpo" value="<?= $this->session->userdata('tanggal_po') ?>">
            <input type="hidden" name="supplier" value="<?= $this->session->userdata('supplier') ?>">
            <?php if (count($row) > 0) { ?>
              <button type="submit" class="btn btn-success waves-effect waves-light">
                SIMPAN PO
              </button>
            <?php } ?>
          </form>
        </div>
      </div>
      <input type="hidden" name="aksi" value="simpan" id="aksi" name="aksi">
      <input type="hidden" name="hapus" value="" id="hapus" name="hapus">
    </div>
  </div>
</div>