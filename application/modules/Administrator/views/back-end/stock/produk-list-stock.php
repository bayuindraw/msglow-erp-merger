<!--<div class="main-header">
              <h4><?= $menu ?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Stock</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Stock Opname Kemasan</a>
                    </li>
                </ol>
            </div>-->
<div class="kt-portlet kt-portlet--mobile">


  <!--<div class="card-header"><h5 class="card-header-text">STOCK REAL KEMASAN</h5></div>-->
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <!--<span class="kt-portlet__head-icon">
                      <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>-->
      <h3 class="kt-portlet__head-title">
        STOCK HARI INI BARANG JADI
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
          <a href="<?= base_url() ?>Laporan2/daily_stock" class="btn btn-primary waves-effect waves-light" target="_blank"> <i class="la la-print"></i> <span class="m-l-10">Cetak Stock</span></a>
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">

    <!--begin: Datatable -->
    <div dir id="dir" content="table">
      <table class="table table-striped table-bordered nowrap">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Produk</th>
            <th>Nama Produk</th>
            <th>Stock Baik</th>
            <th>Rusak</th>
            <th>Sample</th>
            <th>Pembagian</th>
            <th>Total Stock</th>
            <th>Sisa Stock</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 0;
          
          foreach ($row as $key => $vaData) {
            $sample = 0; 
            $query = $this->model->code("SELECT sum(B.package_detail_quantity) as total FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE B.product_id = '".$vaData['id_barang']."' AND A.member_code = 'MSGLOWXX101' AND REPLACE(LEFT(A.package_date, 7), '-', '') >= 202111 AND A.warehouse_id = '".$_SESSION['warehouse_id']."'");
			$query2 = $this->model->code("SELECT SUM(jumlah) as total FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po WHERE factory = '19' AND A.id_barang = '".$vaData['id_barang']."' AND REPLACE(LEFT(A.tgl_terima, 7), '-', '') >= 202111 AND A.warehouse_id = '".$_SESSION['warehouse_id']."'");
			foreach ($query as $key => $vaRusak) {
              $rusak = $vaRusak['total'];
            }
			$rusak -= ((@$query2[0]['total'] == "")?0:$query2[0]['total']);
			$query = $this->model->code("SELECT sum(package_detail_quantity) as total FROM v_sample_produk WHERE id_produk = '".$vaData['id_barang']."' AND REPLACE(LEFT(package_date, 7), '-', '') >= 202111 AND warehouse_id = '".$_SESSION['warehouse_id']."'");
            foreach ($query as $key => $vaSample) {
              $sample = $vaSample['total'];
            }
            $pembagian = 0;
            $queryPem = $this->model->code("SELECT SUM(package_detail_quantity) as total FROM t_package_trial_detail_draft WHERE product_id = '".$vaData['id_barang']."' AND package_id IS NULL GROUP BY product_id");
            foreach ($queryPem as $key => $vaPembagian) {
              $pembagian = $vaPembagian['total'];
            }
          ?>
            <tr>
              <td><?= ++$no ?></td>
              <td><?= $vaData['kode_produk'] ?></td>
              <td><?= $vaData['nama_produk'] ?></td>
              <td><?= number_format($vaData['jumlah']) ?></td>
              <td><?php if(!empty($rusak)){ echo $rusak; }else{ echo "0"; } ?></td>
              <td><?php if(!empty($sample)){ echo $sample; }else{ echo "0"; } ?></td>
              <td><?php if(!empty($pembagian)){ echo number_format($pembagian); }else{ echo "0"; }?></td>
              <td><b><?= number_format($vaData['jumlah'] + $rusak + $sample ) ?></b></td>
              <td><?= number_format($vaData['jumlah'] - $pembagian ) ?></td>

            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <!--end: Datatable -->
  </div>
</div>