<?php 
function String2Date($dTgl){
      //return 22-11-2012  
      list($cYear,$cMount,$cDate) = explode("-",$dTgl) ;
      if(strlen($cYear) == 4){
        $dTgl = $cDate . "-" . $cMount . "-" . $cYear ;
      } 
      return $dTgl ;  
    }
$stockawal = $this->model->code("SELECT * FROM tb_stock_kemasan_history WHERE LEFT(tanggal, 7) = '".$tahun.'-'.$bulan."' AND id_barang = '".$idbarang."' ORDER BY tanggal DESC LIMIT 1");
$stockawal = $stockawal[0];
?>
<table cellspacing="0" id="advanced-table" class="table dt-responsive nowrap table-small-font table-bordered table-striped">
  <thead>
      <tr>
        <th>Tanggal</th>
        <th>Jumlah Awal</th>
        <th>Pemasukan</th>
        <th>Pengeluaran</th>
        <th>Jumlah Akhir</th>
      </tr>
    </thead>
    <tbody>
      <?php 
          $nJumlahHari = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
          for($i=1;$i<=$nJumlahHari;$i++){
            if($i > 9){
              $cNol = "";
            }else{
              $cNol = "0";
            }
          $date = $tahun."-".$bulan."-".$cNol.$i ;
          $no=0;

      ?>
       <tr>
         <td><?=String2Date($date)?></td>
         <td>
           <?php 
               /*$queryStock = $this->model->code("SELECT sum(jumlah) as total FROM v_terima_produk WHERE tgl_terima < '".String2Date($date)."' AND id_barang = '".$idbarang."'");
                foreach ($queryStock as $key => $vaAwal) {
                  $pembelian = $vaAwal['total'];
                }

              $queryKluar = $this->model->code("SELECT sum(jumlah) as total FROM detail_pengeluaran_kemasan WHERE tanggal < '".String2Date($date)."' AND id_barang = '".$idbarang."'");
              foreach ($queryKluar as $key => $vaAkhir) {
                  $pengeluaran = $vaAkhir['total'];
              }

              
              $total =  $pembelian-$pengeluaran;*/
				echo ($stockawal['jumlah'] == 0)?"":$stockawal['jumlah'];			  
		   ?>
           <?php 
            $besok = mktime(0, 0, 0, date("m"), date("d")+1, date("Y"));
            if($date <  date('Y-m-d', $besok)){?>
              <b><?= $total ?></b>
           <?php }?>
         </td>
         <td>
           <?php 
             $queryAwal = $this->model->code("SELECT * FROM v_terima_produk WHERE tgl_terima = '".$date."' AND id_barang = '".$idbarang."'");
              foreach ($queryAwal as $key => $vaAwal) {
				 $stockawal['jumlah'] += $vaAwal['jumlah'];
                echo "".$vaAwal['kode_factory']." : ".$vaAwal['jumlah']."<br>";
              }

              $queryTambah = $this->model->code("SELECT  sum(jumlah) as total FROM v_terima_produk WHERE tgl_terima = '".String2Date($date)."' AND id_barang = '".$idbarang."'");
              foreach ($queryTambah as $key => $vaAwal) {
                $pemasukanHari = $vaAwal['total'];
              }

           ?>
         </td>
         <td><?php 

             $queryAkhir = $this->model->code("SELECT * FROM detail_pengeluaran_kemasan WHERE tanggal = '".String2Date($date)."' AND id_barang = '".$idbarang."'");
              foreach ($queryAkhir as $key => $vaAkhir) {
				  $stockawal['jumlah'] -= $vaAkhir['jumlah'];
                echo "".$vaAkhir['kode_factory']." : ".$vaAkhir['jumlah']."<br>";
              }

             $queryStockAkhir = $this->model->code("SELECT sum(jumlah) as total FROM detail_pengeluaran_kemasan WHERE tanggal = '".String2Date($date)."' AND id_barang = '".$idbarang."'");
              foreach ($queryStockAkhir as $key => $vaAkhir) {
                 $akhirStock = $vaAkhir['total'];
              }

           ?></td>
         <td> <b><?= /*($total + $pemasukanHari) - $akhirStock*/ ($stockawal['jumlah'] == 0)?"":$stockawal['jumlah']; ?></b></td>
       </tr>                  
      <?php } ?>
    </tbody>
  </table>
   <a href="<?=base_url()?>Administrator/Stock/laporan_stock_satuan_produk/<?=$bulan?>/<?=$tahun?>/<?=$idbarang?>" target="_blank" class="btn btn-warning waves-effect waves-light"> <i class="fa fa-print"></i> Print Laporan</a>
                            