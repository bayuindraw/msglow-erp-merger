            <?php 

              foreach ($row as $key => $vaData) {
                $tanggal = $vaData['tgl_terima'];
                $namabarang = $vaData['nama_kemasan'];
                $JumlahSJ   = $vaData['jumlah'];
                $kodePo     = $vaData['id_terima_kemasan'];
                $kode       = $vaData['kode_po'];
                $baik       = $vaData['baik'];
                $rusak      = $vaData['rusak'];
              }

            ?>
            <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock_Act/terima_print_edit" method="post" novalidate>
                    
                    <div class="md-group-add-on p-relative">
                     <span class="md-add-on">
                      <i class="icofont icofont-ui-calendar"></i>
                    </span>
                    <div class="md-input-wrapper">
                      <input type="text" id="dTglPo" name="TanggalOrder" 
                      class="form-control md-static floating-label 4IDE-date" value="<?=$tanggal?>" required>
                      <label for="KodeBarang">Tanggal Terima</label>
                      <span class="messages"></span>
                      
                      </div>
                    </div>
                    <div class="md-group-add-on p-relative">
                         <span class="md-add-on">
                          <i class="icofont icofont-ui-tag"></i>
                        </span>
                       <div class="md-input-wrapper">
                          <input type="text" class="form-control md-static"  value="<?=$namabarang?>" required>
                          <label for="NamaBarang">Nama Produk</label>
                          <span class="messages"></span>
                        </div>
                      </div>
                    <div class="md-group-add-on p-relative">
                         <span class="md-add-on">
                          <i class="icofont icofont-ui-tag"></i>
                        </span>
                       <div class="md-input-wrapper">
                          <input type="text" class="form-control md-static"  value="<?=$JumlahSJ?>" required>
                          <input type="hidden" class="form-control md-static"  value="<?=$kodePo?>" name="kodepo" required>
                          <input type="hidden" class="form-control md-static"  value="<?=$kode?>" name="kode" required>
                          <label for="NamaBarang">Jumlah Terima Sesuai SJ</label>
                          <span class="messages"></span>
                        </div>
                      </div>
                    <div class="md-group-add-on p-relative">
                         <span class="md-add-on">
                          <i class="icofont icofont-ui-tag"></i>
                        </span>
                       <div class="md-input-wrapper">
                          <input type="text" class="form-control md-static"  name="terimabagus" id="JumlahBayar" value="<?=$baik?>" required>
                          <label for="NamaBarang">Jumlah Terima Bagus</label>
                          <span class="messages"></span>
                        </div>
                      </div>
                    <div class="md-group-add-on p-relative">
                         <span class="md-add-on">
                          <i class="icofont icofont-ui-tag"></i>
                        </span>
                       <div class="md-input-wrapper">
                          <input type="text" class="form-control md-static"  name="terimrusak" id="JumlahBayar" value="<?=$rusak?>" required>
                          <label for="NamaBarang">Jumlah Terima Rusak</label>
                          <span class="messages"></span>
                        </div>
                      </div>
                    
                   

                  <div class="md-input-wrapper">     
                   <button type="submit" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" 
                   data-placement="top" title="{{cValueButton}}">
                   <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
                    </button>
               </div>
                </form>