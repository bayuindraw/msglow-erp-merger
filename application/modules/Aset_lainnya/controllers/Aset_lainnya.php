<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Aset_lainnya extends CI_Controller
{

	var $url_ = "Aset_lainnya";
	var $id_ = "asset_id";
	var $eng_ = "Asset";
	var $ind_ = "Aset";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function form()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['arrakun'] = "<option></option>";
		$datacontent['arrakuncoa4'] = "";
		$arrakuns = $this->db->query("SELECT * FROM coa_4 WHERE coa3_id = 180")->result_array();
		foreach ($arrakuns as $arrakun) {
			$datacontent['arrakun'] .= "<option value=" . $arrakun['id'] . ">" . $arrakun['nama'] . "</option>";
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_coa4($id)
	{
		$data = '';
		if ($id == 1) {
			foreach ($this->db->get_where('coa_4', ['coa3_id' => 10])->result_array() as $value) {
				$data .= "<option value=" . $value['id'] . ">" . $value['nama'] . "</option>";
			}
		} else if ($id == 2) {
			foreach ($this->db->get_where('coa_4', ['coa3_id' => 11])->result_array() as $value) {
				$data .= "<option value=" . $value['id'] . ">" . $value['nama'] . "</option>";
			}
		} else if ($id == 4) {
			foreach ($this->db->get_where('coa_4', ['coa3_id' => 180])->result_array() as $value) {
				$data .= "<option value=" . $value['id'] . ">" . $value['nama'] . "</option>";
			}
		}
		echo $data;
	}

	public function simpan()
	{
		$this->db->trans_begin();
		foreach ($_POST['input2'] as $input) {
			$coaAset = $this->db->get_where('coa_4', ['id' => $input['coa_asset_id']])->row_array();
			$coaBank = $this->db->get_where('coa_4', ['id' => $input['coa4_pakai']])->row_array();

			$t_material = ([
				'material_name' => $input['material_name'],
				'material_date' => $input['material_date'],
				'material_value' => str_replace(",", "", $input['material_value']),
				'material_paid' => str_replace(",", "", $input['material_paid']),
				'material_create_date' => date('Y-m-d H:i:s'),
				'material_create_user_id' => $_SESSION['user_id'],
				'coa_id' => $coaAset['id'],
			]);
			if ($t_material['material_value'] > $t_material['material_paid']) {
				$coaUtang = $this->Model->insert_coa_utang($t_material['material_name']);
				$t_material['coa_utang_id'] = $coaUtang['id'];
			}
			$this->db->insert('t_material', $t_material);
			$t_material['material_id'] = $this->db->insert_id();

			$header = ([
				'coa_transaction_date' => $input['material_date'],
				'date_create' => date('Y-m-d'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_debit' => $t_material['material_value'],
				'coa_transaction_credit' => $t_material['material_value'],
				'coa_transaction_payment' => $t_material['material_value'],
				'coa_transaction_realization' => 0,
				'coa_transaction_realization_date' => date('Y-m-d'),
			]);
			$this->db->insert('t_coa_transaction_header', $header);
			$idHeader = $this->db->insert_id();

			$akunAset = ([
				'coa_name' => $coaAset['nama'],
				'coa_code' => $coaAset['kode'],
				'coa_date' => $input['material_date'],
				'coa_level' => 4,
				'coa_debit' => $t_material['material_value'],
				'coa_transaction_note' => 'Pembelian Aset Lainnya By ' . $_SESSION['user_fullname'],
				'date_create' => date('Y-m-d'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 15,
				'coa_transaction_source_id' => $t_material['material_id'],
				'coa_group_id' => $idHeader,
				'coa_id' => $coaAset['id'],
				'coa_transaction_realization' => 0,
			]);
			$this->db->insert('t_coa_transaction', $akunAset);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $akunAset[coa_debit] WHERE coa_id = '$akunAset[coa_id]' AND coa_level = '$akunAset[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $akunAset[coa_debit] WHERE coa_id = '$akunAset[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunAset['coa_date'] . "', '-', '') AND coa_level = '$akunAset[coa_level]'");

			if ($t_material['material_paid'] > 0) {
				$akunBank = ([
					'coa_name' => $coaBank['nama'],
					'coa_code' => $coaBank['kode'],
					'coa_date' => $input['material_date'],
					'coa_level' => 4,
					'coa_credit' => $t_material['material_paid'],
					'coa_transaction_note' => 'Pembelian Aset Lainnya By ' . $_SESSION['user_fullname'],
					'date_create' => date('Y-m-d'),
					'user_create' => $_SESSION['user_id'],
					'coa_transaction_source' => 15,
					'coa_transaction_source_id' => $t_material['material_id'],
					'coa_group_id' => $idHeader,
					'coa_id' => $coaBank['id'],
					'coa_transaction_realization' => 0,
				]);
				$this->db->insert('t_coa_transaction', $akunBank);
				$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $akunBank[coa_credit] WHERE coa_id = '$akunBank[coa_id]' AND coa_level = '$akunBank[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $akunBank[coa_credit] WHERE coa_id = '$akunBank[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunBank['coa_date'] . "', '-', '') AND coa_level = '$akunBank[coa_level]'");
			}

			if ($t_material['material_value'] > $t_material['material_paid']) {
				$akunUtang = ([
					'coa_name' => $coaUtang['nama'],
					'coa_code' => $coaUtang['kode'],
					'coa_date' => $input['material_date'],
					'coa_level' => 4,
					'coa_credit' => $t_material['material_value'] - $t_material['material_paid'],
					'coa_transaction_note' => 'Pembelian Aset Lainnya By ' . $_SESSION['user_fullname'],
					'date_create' => date('Y-m-d'),
					'user_create' => $_SESSION['user_id'],
					'coa_transaction_source' => 15,
					'coa_transaction_source_id' => $t_material['material_id'],
					'coa_group_id' => $idHeader,
					'coa_id' => $coaUtang['id'],
					'coa_transaction_realization' => 0,
				]);
				$this->db->insert('t_coa_transaction', $akunUtang);
				$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $akunUtang[coa_credit] WHERE coa_id = '$akunUtang[coa_id]' AND coa_level = '$akunUtang[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $akunUtang[coa_credit] WHERE coa_id = '$akunUtang[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunUtang['coa_date'] . "', '-', '') AND coa_level = '$akunUtang[coa_level]'");
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(base_url() . 'Aset_lainnya/form');
	}

	public function input_old_coa($date, $t_material, $coaAkum, $coaBeban, $idHeader)
	{
		$tgl1 = $date;
		$tgl2 = date('Y-m-d');
		$awal1 = substr(str_replace('-', '', $tgl1), 0, 6);
		$akhir1 = substr(str_replace('-', '', $tgl1), 6, 8);
		$awal2 = substr(str_replace('-', '', $tgl2), 0, 6);
		$akhir2 = substr(str_replace('-', '', $tgl2), 6, 8);
		$tgl_akhir1 = date("t", strtotime($tgl1));
		for ($i = ($awal1 + 1); $i <= $awal2; $i++) {
			if (substr($i, 4, 6) == 13) {
				$i = (substr($i, 0, 4) + 1) . '01';
			}
			$tgl_akhir = date("Y-m-t", strtotime(substr($i, 0, 4) . '-' . substr($i, 4, 6) . '-01'));
			if ($awal2 == $i) {
				if ($akhir1 > substr($tgl_akhir, 8, 10)) {
					if ($akhir1 <= $akhir2 || ($akhir1 == $tgl_akhir1 && $akhir2 == substr($tgl_akhir, 8, 10))) {
						$this->Model->input_old_coa($tgl_akhir, $t_material, $coaAkum, $coaBeban, $idHeader);
					}
				} else {
					if ($akhir1 <= $akhir2) {
						$this->Model->input_old_coa(substr($tgl_akhir, 0, 8) . $akhir1, $t_material, $coaAkum, $coaBeban, $idHeader);
					}
				}
			} else {
				if ($akhir1 > substr($tgl_akhir, 8, 10)) {
					$this->Model->input_old_coa(substr($tgl_akhir, 0, 8) . substr($tgl_akhir, 8, 10), $t_material, $coaAkum, $coaBeban, $idHeader);
				} else {
					$this->Model->input_old_coa(substr($tgl_akhir, 0, 8) . $akhir1, $t_material, $coaAkum, $coaBeban, $idHeader);
				}
			}
			echo "<br>";
			if (substr($i, 4, 6) == 12) $i += 88;
		}
		return '';
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function get_data_detail($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_detail($id);
	}

	public function detail($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['id'] = $id;
		$datacontent['title'] = 'Data Penyusutan';
		$datacontent['card'] = $this->db->query("SELECT A.asset_value, A.asset_date, sum(B.coa_credit) as coa_credit FROM t_asset A left JOIN t_coa_transaction B ON B.coa_id = A.coa_shrink_id WHERE A.asset_id = '$id' group by asset_id")->row_array();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
}
