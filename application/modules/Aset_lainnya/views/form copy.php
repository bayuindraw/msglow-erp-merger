<div class="row">
	<div class="col-lg-12 col-xl-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" onsubmit="return confirm('Are you sure?')" enctype="multipart/form-data">
				<div class="kt-portlet__body">
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Nama Aset</th>
								<th width="10%">Akun</th>
								<th>Tanggal Beli</th>
								<th width="8%">Harga</th>
								<th width="10%">Dibayar</th>
								<th>Akun Pembayaran</th>
								<th>Pilihan Akun</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="x">
							<?php
							$totalx = 0;
							$i = 0;
							?>
							<tr id="tr0">
								<td><input type="text" class="form-control" placeholder="Nama Aset" name="input2[0][material_name]" required></td>
								<td><select name="input2[0][coa_asset_id]" class="pilihAkun form-control md-static" required><?= $arrakun ?></select></td>
								<td><input type="text" class="form-control date_picker" autocomplete="off" placeholder="Tanggal" name="input2[0][material_date]" required></td>
								<td><input type="text" class="form-control numeric" placeholder="Harga" name="input2[0][material_value]"></td>
								<td><input type="text" class="form-control numeric" placeholder="Dibayar" name="input2[0][material_paid]"></td>
								<td>
									<select id="material_type0" name="input2[0][material_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(0);" required>
										<option value=""></option>
										<option value="1">KAS</option>
										<option value="2">BANK</option>
									</select>
								</td>
								<td>
									<select id="arrakuncoa40" name="input2[0][coa4_pakai]" class="pilihAkun form-control md-static" required></select>
								</td>
								<td><button onclick="myDeleteFunction(0);chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
							</tr>
							<?php $i++; ?>
						</tbody>
						<tbody>
							<tr>
								<td colspan="3">Total Nominal : <span id="total"><?= $totalx ?></span></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<button type="submit" name="simpan" value="simpan" id="btn_submit" class="btn btn-primary">Simpan</button>
							<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
						</div>
					</div>
			</form>
		</div>
	</div>
</div>

<script>
	var count = <?= $i; ?>;

	function myCreateFunction() {
		count = count + 1;
		var html = '<tr id="tr' + count + '"><td><input type="text" class="form-control" placeholder="Nama Aset" name="input2[' + count + '][material_name]" required></td><td><select name="input2[' + count + '][coa_asset_id]" class="pilihAkun' + count + ' form-control md-static" required><?= $arrakun ?></select></td><td><input type="text" class="form-control date_picker" placeholder="Tanggal" name="input2[' + count + '][material_date]" required></td><td><input type="text" class="form-control numeric" placeholder="Harga" name="input2[' + count + '][material_value]"></td><td><input type="text" class="form-control numeric" placeholder="Dibayar" name="input2[' + count + '][material_paid]"></td><td><select id="material_type' + count + '" name="input2[' + count + '][material_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(' + count + ');" required><option value=""></option><option value="1">KAS</option><option value="2">BANK</option></select></td><td><select id="arrakuncoa4' + count + '" name="input2[' + count + '][coa4_pakai]" class="pilihAkun form-control md-static" required></select></td><td><button onclick="myDeleteFunction(' + count + ');chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$('.pilihAkun' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('#material_type' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('#arrakuncoa4' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('.date_picker, #kt_datepicker_1_validate').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			format: "yyyy-mm-dd"
		});
		$(".numeric").mask("#,##0", {
			reverse: true
		});
	}

	function myDeleteFunction(id) {
		$('#tr' + id).remove();
	}

	function cekCoa4(id) {
		$.ajax({
			url: '<?= base_url() ?>Aset_tetap/get_coa4/' + $('#material_type' + id + ' option:selected').val(),
			success: function(result) {
				$("#arrakuncoa4" + id + "").html(result);
			}
		});
	}
</script>