<?php
defined('BASEPATH') or exit('No direct script access allowed');

class COA extends CI_Controller
{
    var $url_ = "COA";
    var $id_ = "id";
    var $eng_ = "Coa";
    var $ind_ = "Coa";

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Coa_model');
        $this->load->model('relasi');
        $this->load->library('session');
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('download');
    }
    public  function Date2String($dTgl)
    {
        //return 2012-11-22
        list($cDate, $cMount, $cYear)    = explode("-", $dTgl);
        if (strlen($cDate) == 2) {
            $dTgl    = $cYear . "-" . $cMount . "-" . $cDate;
        }
        return $dTgl;
    }

    public  function String2Date($dTgl)
    {
        //return 22-11-2012  
        list($cYear, $cMount, $cDate)    = explode("-", $dTgl);
        if (strlen($cYear) == 4) {
            $dTgl    = $cDate . "-" . $cMount . "-" . $cYear;
        }
        return $dTgl;
    }

    public function TimeStamp()
    {
        date_default_timezone_set("Asia/Jakarta");
        $Data = date("H:i:s");
        return $Data;
    }

    public function DateStamp()
    {
        date_default_timezone_set("Asia/Jakarta");
        $Data = date("d-m-Y");
        return $Data;
    }

    public function DateTimeStamp()
    {
        date_default_timezone_set("Asia/Jakarta");
        $Data = date("Y-m-d h:i:s");
        return $Data;
    }

    public function index($action = "", $id = "")
    {

        $dataHeader['title']        = "SAPA MSGLOW | CUSTOMER SERVICE MSGLOW";
        $dataHeader['menu']           = 'Master';
        $dataHeader['file']           = 'Dashboard';
        $dataHeader['link']           = 'index';

        if ($action == "edit") {
            $data['coa'] = $this->Coa_model->get_coa1_where($id);
        }

        $data['coas'] = $this->Coa_model->get_coa1();
        $data['file'] = $this->ind_;
        $data['content'] = $this->load->view('List_coa', $data, TRUE);
        $this->load->view('Layout/home', $data);
    }

    public function coa2($action = "", $id = "")
    {
        $dataHeader['title']        = "SAPA MSGLOW | CUSTOMER SERVICE MSGLOW";
        $dataHeader['menu']           = 'Master';
        $dataHeader['file']           = 'Dashboard';
        $dataHeader['link']           = 'index';

        if ($action == "edit") {
            $data['coa'] = $this->Coa_model->get_coa2_where($id);
        }

        $data['coas'] = $this->Coa_model->get_coa2();
        $data['coas1'] = $this->Coa_model->get_coa1();
        $data['file'] = $this->ind_;
        $data['content'] = $this->load->view('List_coa2', $data, TRUE);
        $this->load->view('Layout/home', $data);
    }

    public function coa3($action = "", $id = "")
    {
        $dataHeader['title']        = "SAPA MSGLOW | CUSTOMER SERVICE MSGLOW";
        $dataHeader['menu']           = 'Master';
        $dataHeader['file']           = 'Dashboard';
        $dataHeader['link']           = 'index';

        if ($action == "edit") {
            $data['coa'] = $this->Coa_model->get_coa3_where($id);
        }

        $data['coas'] = $this->Coa_model->get_coa3();
        $data['coas2'] = $this->Coa_model->get_coa2();
        $data['file'] = $this->ind_;
        $data['content'] = $this->load->view('List_coa3', $data, TRUE);
        $this->load->view('Layout/home', $data);
    }
    public function coa4($action = "", $id = "")
    {
        $dataHeader['title']        = "SAPA MSGLOW | CUSTOMER SERVICE MSGLOW";
        $dataHeader['menu']           = 'Master';
        $dataHeader['file']           = 'Dashboard';
        $dataHeader['link']           = 'index';

        if ($action == "edit") {
            $data['coa'] = $this->Coa_model->get_coa4_where($id);
        }

        $data['coas'] = $this->Coa_model->get_coa4();
        $data['coas3'] = $this->Coa_model->get_coa3();
        $data['file'] = $this->ind_;
        $data['url'] = 'COA';
        $data['content'] = $this->load->view('List_coa4', $data, TRUE);
        $this->load->view('Layout/home', $data);
    }
    public function edit_saldo($id = "")
    {
        $dataHeader['title']        = "SAPA MSGLOW | CUSTOMER SERVICE MSGLOW";
        $dataHeader['menu']           = 'Master';
        $dataHeader['file']           = 'Dashboard';
        $dataHeader['link']           = 'index';

        $data['coas'] = $this->Coa_model->get_coa($id);
        $data['file'] = $this->ind_;
        $data['url'] = 'COA';
        $data['coa_level'] = $id;
        $data['content'] = $this->load->view('edit_saldo', $data, TRUE);
        $this->load->view('Layout/home', $data);
    }

    public function get_data_coa4()
    {
        $data['datatable'] = $this->Coa_model->get_data_coa4();
    }

    // public function edit_coa($id)
    // {
    //     $dataHeader['title']        = "SAPA MSGLOW | CUSTOMER SERVICE MSGLOW";
    //     $dataHeader['menu']           = 'Master';
    //     $dataHeader['file']           = 'Dashboard';
    //     $dataHeader['link']           = 'index';

    //     $data['coas'] = $this->Coa_model->get_coa1();
    //     $data['coa'] = $this->Coa_model->get_coa1_detail($id);
    //     $this->load->view('Marketing/back-end/container/header', $dataHeader);
    //     $this->load->view('List_coa', $data);
    //     $this->load->view('Marketing/back-end/container/footer');
    // }

    // public function edit_coa2($id)
    // {
    //     $dataHeader['title']        = "SAPA MSGLOW | CUSTOMER SERVICE MSGLOW";
    //     $dataHeader['menu']           = 'Master';
    //     $dataHeader['file']           = 'Dashboard';
    //     $dataHeader['link']           = 'index';

    //     $data['coas'] = $this->Coa_model->get_coa2();
    //     $data['coa'] = $this->Coa_model->get_coa2_detail($id);
    //     $data['coas1'] = $this->Coa_model->get_coa1();
    //     $this->load->view('Marketing/back-end/container/header', $dataHeader);
    //     $this->load->view('List_coa2', $data);
    //     $this->load->view('Marketing/back-end/container/footer');
    // }

    // public function edit_coa3($id)
    // {
    //     $dataHeader['title']        = "SAPA MSGLOW | CUSTOMER SERVICE MSGLOW";
    //     $dataHeader['menu']           = 'Master';
    //     $dataHeader['file']           = 'Dashboard';
    //     $dataHeader['link']           = 'index';

    //     $data['coas'] = $this->Coa_model->get_coa3();
    //     $data['coa'] = $this->Coa_model->get_coa3_detail($id);
    //     $data['coas2'] = $this->Coa_model->get_coa2();
    //     $this->load->view('Marketing/back-end/container/header', $dataHeader);
    //     $this->load->view('List_coa3', $data);
    //     $this->load->view('Marketing/back-end/container/footer');
    // }

    public function store_coa()
    {
        $data = $_POST['input'];
        $count = $this->Coa_model->get_coa1_count();
        $data['kode'] = (count($count) + 1) . '00';
        $this->db->insert('coa_1', $data);
        $data['id'] = $this->db->insert_id();
        $total = ([
            'coa_id' => $data['id'],
            'coa_code' => $data['kode'],
            'coa_level' => 1,
            'date_create' => date('Y-m-d H:i:s'),
            'coa_name' => $data['nama'],
        ]);
        $this->db->insert('t_coa_total', $total);
        $total['coa_total_date'] = date('Y-m') . '-01';
        $this->db->insert('t_coa_total_history', $total);
        redirect('COA');
    }

    public function store_coa2()
    {
        $data = $_POST['input'];
        $count = $this->Coa_model->get_coa2_count($data['coa1_id']);
        $kode = $this->Coa_model->get_coa1_detail($data['coa1_id']);
        $data['kode'] = $kode['kode'] . '.' . (count($count) + 1);
        $this->db->insert('coa_2', $data);
        $data['id'] = $this->db->insert_id();
        $total = ([
            'coa_id' => $data['id'],
            'coa_code' => $data['kode'],
            'coa_level' => 2,
            'date_create' => date('Y-m-d H:i:s'),
            'coa_name' => $data['nama'],
        ]);
        $this->db->insert('t_coa_total', $total);
        $total['coa_total_date'] = date('Y-m') . '-01';
        $this->db->insert('t_coa_total_history', $total);
        redirect('COA/coa2');
    }

    public function store_coa3()
    {
        $data = $_POST['input'];
        $count = $this->Coa_model->get_coa3_count($data['coa2_id']);
        $kode = $this->Coa_model->get_coa2_detail($data['coa2_id']);
        $data['kode'] = $kode['kode'] . '.' . (count($count) + 1);
        $this->db->insert('coa_3', $data);
        $data['id'] = $this->db->insert_id();
        $total = ([
            'coa_id' => $data['id'],
            'coa_code' => $data['kode'],
            'coa_level' => 3,
            'date_create' => date('Y-m-d H:i:s'),
            'coa_name' => $data['nama'],
        ]);
        $this->db->insert('t_coa_total', $total);
        $total['coa_total_date'] = date('Y-m') . '-01';
        $this->db->insert('t_coa_total_history', $total);
        redirect('COA/coa3');
    }

    public function store_coa4()
    {
        $data = $_POST['input'];
        $count = $this->Coa_model->get_coa4_count($data['coa3_id']);
        $kode = $this->Coa_model->get_coa3_detail($data['coa3_id']);
        $data['kode'] = $kode['kode'] . '.' . (count($count) + 1);
        $this->db->insert('coa_4', $data);

        $data['id'] = $this->db->insert_id();
        $total = ([
            'coa_id' => $data['id'],
            'coa_code' => $data['kode'],
            'coa_level' => 4,
            'date_create' => date('Y-m-d H:i:s'),
            'coa_name' => $data['nama'],
        ]);
        $this->db->insert('t_coa_total', $total);
        $total['coa_total_date'] = date('Y-m') . '-01';
        $this->db->insert('t_coa_total_history', $total);
        redirect('COA/coa4');
    }

    public function simpan_saldo()
    {
        $date = date('Y-m-d');
        foreach ($_POST['saldo_awal'] as $index => $saldo) {
            $saldo = str_replace(",", "", $saldo);
            if ($saldo != $_POST['saldo_sebelum'][$index]) {
                $coa = $this->db->get_where('coa_' . $_POST['coa_level'] . '', ['id' => $index])->row_array();
                if ($_POST['type_saldo'][$index] == 'debit') {
                    $this->db->query("INSERT INTO t_coa_total_history (coa_id, coa_code, coa_total_debit, coa_level, coa_name, coa_total_date) VALUES('$coa[id]', '$coa[kode]', '$saldo', '$_POST[coa_level]', '$coa[nama]', '$date') ON DUPLICATE KEY UPDATE coa_total_debit = '$saldo'");
                } else {
                    $this->db->query("INSERT INTO t_coa_total_history (coa_id, coa_code, coa_total_credit, coa_level, coa_name, coa_total_date) VALUES('$coa[id]', '$coa[kode]', '$saldo', '$_POST[coa_level]', '$coa[nama]', '$date') ON DUPLICATE KEY UPDATE coa_total_credit = '$saldo'");
                }
            }
        }
        redirect('COA/coa' . $_POST['coa_level'] . '');
    }

    public function update_coa()
    {
        $data = $_POST['input'];
        $this->db->update('coa_1', $data, ['id' => $data['id']]);
        redirect('COA');
    }

    public function update_coa2()
    {
        $data = $_POST['input'];
        $coa = $this->db->get_where('coa_2', ['id' => $data['id']])->row_array();
        if ($coa['coa2_id'] == $data['coa2_id']) {
            $this->db->update('coa_2', $data, ['id' => $data['id']]);
        } else {
            $coa3 = $this->Coa_model->get_coa3_count($data['coa2_id']);
            $coa2 = $this->Coa_model->get_coa2_detail($data['coa2_id']);
            $data['kode'] = $coa2['kode'] . '.' . (count($coa3) + 1);
            $this->db->update('coa_2', $data, ['id' => $data['id']]);
        }
        redirect('COA/coa2');
    }
    public function update_coa3()
    {
        $data = $_POST['input'];
        $coa = $this->db->get_where('coa_3', ['id' => $data['id']])->row_array();
        if ($coa['coa2_id'] == $data['coa2_id']) {
            $this->db->update('coa_3', $data, ['id' => $data['id']]);
        } else {
            $coa3 = $this->Coa_model->get_coa3_count($data['coa2_id']);
            $coa2 = $this->Coa_model->get_coa2_detail($data['coa2_id']);
            $data['kode'] = $coa2['kode'] . '.' . (count($coa3) + 1);
            $this->db->update('coa_3', $data, ['id' => $data['id']]);
        }
        redirect('COA/coa3');
    }

    public function update_coa4()
    {
        $data = $_POST['input'];
        $coa = $this->db->get_where('coa_4', ['id' => $data['id']])->row_array();
        if ($coa['coa3_id'] == $data['coa3_id']) {
            $this->db->update('coa_4', $data, ['id' => $data['id']]);
        } else {
            $coa4 = $this->Coa_model->get_coa4_count($data['coa3_id']);
            $coa3 = $this->Coa_model->get_coa3_detail($data['coa3_id']);
            $data['kode'] = $coa3['kode'] . '.' . (count($coa4) + 1);
            $this->db->update('coa_4', $data, ['id' => $data['id']]);
        }
        redirect('COA/coa4');
    }
}
