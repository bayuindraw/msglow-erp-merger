    <?php if ($this->session->flashdata('flash')) : ?>
        <div class="alert alert-success">
            <?= $this->session->flashdata('flash'); ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <form id="add-category-ticket" method="POST" action="<?= isset($coa) ? base_url() . 'COA/update_coa3' : base_url() . 'COA/store_coa3' ?>">
                        <?php if (isset($coa)) : ?>
                            <div class="form-group">
                                <label for="name"> Kode</label>
                                <input type="text" name="input[kode]" id="kode" class="form-control" placeholder="Kode COA" aria-describedby="kode" value="<?= isset($coa) ? $coa[0]['kode'] : '' ?>">
                            </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <label for="name"> Tipe coa2</label>
                            <select id="selectCOA" class="form-control" name="input[coa2_id]">
                                <?php
                                foreach ($coas2 as $coa2) {
                                ?>
                                    <option value="<?= $coa2['id'] ?>" <?= isset($coa) ? $coa[0]['coa2_id'] == $coa2['id'] ? 'selected' : '' : '' ?>><?= $coa2['kode'] ?> - <?= $coa2['nama'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name"> COA</label>
                            <input type="text" name="input[nama]" id="nama" class="form-control" placeholder="Nama COA" aria-describedby="nama" value="<?= isset($coa) ? $coa[0]['nama'] : '' ?>">
                            <input type="hidden" name="input[id]" id="nama" class="form-control" placeholder="Nama COA" aria-describedby="nama" value="<?= isset($coa) ? $coa[0]['id'] : '' ?>">
                        </div>
                        <div class="text-right">
                            <?php if (isset($coa)) : ?>
                                <a href="<?= base_url() ?>COA/coa3" class="btn btn-danger btn-sm waves-effect waves-light"><span class="btn-label"><i class="fas fa-ban"></i></span> Batal</a>
                            <?php endif; ?>
                            <button class="btn btn-success btn-sm waves-effect waves-light" type="submit"><span class="btn-label"><i class="fas fa-save"></i></span> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card">
                <a class="btn btn-success mt-2" href="<?= base_url() ?>COA/edit_saldo/3" style="position: absolute; right: 0; margin-right: 20px">Edit Saldo</a>
                <br>
                <br>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="listCoa" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th width="10%">Kode</th>
                                    <th width="">Nama</th>
                                    <th width="5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($coas as $coa) : ?>
                                    <tr>
                                        <td><?= $coa['kode'] ?></td>
                                        <td><?= $coa['nama'] ?></td>
                                        <td>
                                            <a href="<?= base_url() ?>COA/coa3/edit/<?= $coa['id'] ?>" class="btn btn-info btn-sm waves-effect waves-light del-product" type="submit"><span class="btn-label"><i class="fas fa-edit"></i></span></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#listCoa').DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });
    </script>