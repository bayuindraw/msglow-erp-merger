<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="kt-portlet">
            <form method="post" action="<?= site_url($url . '/simpan_saldo'); ?>" enctype="multipart/form-data">
                <input type="hidden" name="coa_level" value="<?= $coa_level ?>" />
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Detail
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Saldo Awal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0;
                            foreach ($coas as $key => $vaData) {
                            ?>
                                <tr>
                                    <td><?= ++$no ?></td>
                                    <td><?= $vaData['kode'] ?></td>
                                    <td><?= $vaData['nama'] ?></td>
                                    <?php if (substr($vaData['kode'], 0, 1) == 1 || substr($vaData['kode'], 0, 1) >= 5 || substr($vaData['kode'], 0, 1) <= 7) { ?>
                                        <td><input type="text" name="saldo_awal[<?= $vaData['id'] ?>]" class="form-control numeric" value="<?= $vaData['coa_total_debit'] == null || $vaData['coa_total_debit'] == 0.00 ? 0 :  str_replace(".00", "", $vaData['coa_total_debit']) ?>" /></td>
                                        <input type="hidden" name="type_saldo[<?= $vaData['id'] ?>]" value="debit" />
                                        <input type="hidden" name="saldo_sebelum[<?= $vaData['id'] ?>]" value="<?= $vaData['coa_total_debit'] == null || $vaData['coa_total_debit'] == 0.00 ? 0 :  str_replace(".00", "", $vaData['coa_total_debit']) ?>" />
                                    <?php } else { ?>
                                        <td><input type="text" name="saldo_awal[<?= $vaData['id'] ?>]" class="form-control numeric" value="<?= $vaData['coa_total_credit'] == null || $vaData['coa_total_credit'] == 0.00 ? 0 :  str_replace(".00", "", $vaData['coa_total_credit']) ?>" /></td>
                                        <input type="hidden" name="type_saldo[<?= $vaData['id'] ?>]" value="credit" />
                                        <input type="hidden" name="saldo_sebelum[<?= $vaData['id'] ?>]" value="<?= $vaData['coa_total_credit'] == null || $vaData['coa_total_credit'] == 0.00 ? 0 :  str_replace(".00", "", $vaData['coa_total_credit']) ?>" />
                                    <?php } ?>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                </div>

                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                        <a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
                    </div>
                </div>
            </form>


        </div>
    </div>
</div>