<div class="row">
	<div class="col-lg-12 col-xl-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>

			<!--begin::Form-->
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" onsubmit="return confirm('Are you sure?')" enctype="multipart/form-data">
				<div class="kt-portlet__body" id="x">
					<?php
					$totalx = 0;
					$i = 0;
					$y = 0;
					?>
					<div id="body0">
						<div class="form-group row">
							<div class="col-lg-3">
								<label>Keterangan:</label>
								<input type="text" class="form-control" placeholder="Keterangan" name="input2[0][activities_name]" required>
								<input type="hidden" id="coa3id0" value="1513" class="form-control" name="input2[0][coa3_id]" required>
							</div>
							<div class="col-lg-3">
								<label class="">Akun:</label>
								<select id="akuncoa0" name="input2[0][type_biaya]" class="pilihAkun form-control md-static" onchange="cekProduk(0);" required>
									<option value="1">Biaya Operasional
										<option>
											<option Value="2">Biaya Marketing
												<option>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-3">
												<label>Tanggal Mulai Perjanjian</label>
												<input type="text" class="form-control date_picker" autocomplete="off" placeholder="Tanggal" name="input2[0][activities_date]" required>
											</div>
											<div class="col-lg-3">
												<label>Masa</label>
												<input type="number" class="form-control" placeholder="Masa" name="input2[0][activities_span]" required>
											</div>
											<div class="col-lg-3">
												<label class="">Harga:</label>
												<input type="text" class="form-control numeric" id="harga0" onkeyup="cekMax(0)" onblur="cekNol(0)" value="0" placeholder="Harga" autocomplete="off" name="input2[0][activities_value]" required>
											</div>
											<div class="col-lg-3">
												<label class="">Pembayaran:</label>
												<input type="text" class="form-control numeric" id="bayar0" onkeyup="cekMax(0)" onblur="cekNol(0)" value="0" placeholder="Pembayaran" autocomplete="off" name="input2[0][activities_paid]" required>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-lg-4">
												<label>Via Pembayaran:</label>
												<select id="activities_type0" name="input2[0][activities_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(0);" required>
													<option value=""></option>
													<option value="1">KAS</option>
													<option value="2">BANK</option>
												</select>
											</div>
											<div class="col-lg-4">
												<label class="">Pilihan Akun:</label>
												<select id="arrakuncoa40" name="input2[0][coa4_pakai]" class="pilihAkun form-control md-static" required></select>
											</div>
											<div class="col-lg-4">
												<label class="">Upload Kontrak:</label>
												<input type="file" class="form-control" name="input2[0][activities_file]">
											</div>
										</div>
						<!-- <div class="form-group row">
							<div class="col-lg-6 kt-align-right">
								<button onclick="myDeleteFunction(0);chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button>
							</div>
						</div> -->
					</div>
					<?php $i++; ?>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="simpan" value="simpan" id="btn_submit" class="btn btn-primary">Simpan</button>
								<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
							</div>
							<!-- <div class="col-lg-6 kt-align-right">
								<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
							</div> -->
						</div>
					</div>
				</div>
			</form>

			<!--end::Form-->
		</div>

		<!--end::Portlet-->
	</div>
</div>

<script>
	var count = <?= $i; ?>;
	var ysix = <?= $y; ?>;

	$('.pilihProduk').select2({
		allowClear: true,
		placeholder: 'Pilih Produk',
	});

	function myCreateFunction() {
		count = count + 1;
		var html = '<div id="body' + count + '"><div class="form-group row">	<div class="col-lg-4">		<label>Keterangan:</label>		<input type="text" class="form-control" placeholder="Keterangan" name="input2[' + count + '][activities_name]" required>	</div>	<div class="col-lg-4">		<label class="">Akun:</label>		<select id="akuncoa' + count + '" name="input2[' + count + '][coa3_id]" class="pilihAkun form-control md-static" onchange="cekProduk(' + count + ');" required><?= $arrakun ?></select>	</div>	<div class="col-lg-4">		<label class="">Produk</label>		<select id="arrproduk' + count + '" name="input2[' + count + '][id_product]" class="pilihAkun form-control md-static" required></select>	</div></div><div class="form-group row">	<div class="col-lg-3">		<label>Tanggal Mulai Perjanjian</label>		<input type="text" class="form-control date_picker" autocomplete="off" placeholder="Tanggal" name="input2[' + count + '][activities_date]" required>	</div>	<div class="col-lg-3">		<label>Masa</label>		<input type="number" class="form-control" placeholder="Masa" name="input2[' + count + '][activities_span]">	</div>	<div class="col-lg-3">		<label class="">Harga:</label>		<input type="text" class="form-control numeric" placeholder="Harga" name="input2[' + count + '][activities_value]">	</div>	<div class="col-lg-3">		<label class="">Pembayaran:</label>		<input type="text" class="form-control numeric" placeholder="Pembayaran" name="input2[' + count + '][activities_paid]">	</div></div><div class="form-group row">	<div class="col-lg-4">		<label>Via Pembayaran:</label>		<select id="activities_type' + count + '" name="input2[' + count + '][activities_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(' + count + ');" required>			<option value=""></option>			<option value="1">KAS</option>			<option value="2">BANK</option>		</select>	</div>	<div class="col-lg-4">		<label class="">Pilihan Akun:</label>		<select id="arrakuncoa4' + count + '" name="input2[' + count + '][coa4_pakai]" class="pilihAkun form-control md-static" required></select>	</div>	<div class="col-lg-4">		<label class="">Upload Kontrak:</label>		<input type="file" class="form-control" name="input2[' + count + '][activities_file]">	</div></div><div class="form-group row">	<div class="col-lg-6 kt-align-right">		<button onclick="myDeleteFunction(' + count + ');chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button>	</div></div></div>';
		$('#x').append(html);
		$('.pilihAkun' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('#activities_type' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('#arrakuncoa4' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('.date_picker, #kt_datepicker_1_validate').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			format: "yyyy-mm-dd"
		});
		$(".numeric").mask("#,##0", {
			reverse: true
		});
	}

	function plusProduk(count) {
		ysix = ysix + 1;
		var html = '<div class="input-group" id="prodgrup' + ysix + '"><select id="arrproduk' + ysix + '" name="input2[' + count + '][id_brand][' + ysix + ']" class="form-control md-static" required onchange="getProduk(' + ysix + ')"><?= $arrbrand ?></select><div class="input-group-append">	<button class="input-group-text" onclick="plusProduk(' + count + ');"><i class="la la-plus"></i></button>	<button class="input-group-text" onclick="minusProduk(' + ysix + ');"><i class="la la-minus"></i></button></div></div>';
		var test = '<select id="listproduk' + ysix + '" name="input2[' + count + '][id_product][' + ysix + ']" class="form-control"></select>';
		var pres = '<input type="number" id="presentase' + ysix + '" name="input2[' + count + '][presentase][' + ysix + ']" class="form-control sum_persen" onkeyup="chekPersen()" />';
		$('#produk' + count).append(html);
		$('#cardproduk' + count).append(test);
		$('#cardpresentase' + count).append(pres);
		$('.pilihProduk').select2({
			allowClear: true,
			placeholder: 'Pilih Produk',
		});
	}

	function myDeleteFunction(id) {
		$('#body' + id).remove();
	}

	function minusProduk(id) {
		$('#prodgrup' + id).remove();
		$('#listproduk' + id).remove();
		$('#presentase' + id).remove();
		chekPersen();
	}

	function cekCoa4(id) {
		$.ajax({
			url: '<?= base_url() ?>Dibayar_dimuka/get_coa4/' + $('#activities_type' + id + ' option:selected').val(),
			success: function(result) {
				$("#arrakuncoa4" + id + "").html(result);
			}
		});
	}

	function getProduk(id) {
		$.ajax({
			url: '<?= base_url() ?>Dibayar_dimuka/get_produk/' + $('#arrproduk' + id + ' option:selected').val(),
			success: function(result) {
				$("#listproduk" + id + "").html(result);
			}
		});
		if ($('#arrproduk' + id + ' option:selected').val() == 1) {
			$('#coa3id0').val('1513')
		} else {
			$('#coa3id0').val($('#arrproduk' + id + ' option:selected').attr('coa_id'))
		}
	}

	function cekProduk(id) {
		$.ajax({
			url: '<?= base_url() ?>Dibayar_dimuka/get_form/' + $('#akuncoa' + id + ' option:selected').val(),
			success: function(result) {
				if (result == 1) {
					$('#body0').append('<div id="hideProduk0" class="col-lg-12"><div class="form-group row">	<div class="col-lg-4" id="produk0">		<label class="">Brand</label>		<div class="input-group" id="prodgrup0">			<select id="arrproduk0" name="input2[0][id_brand][0]" class="form-control md-static" required onchange="getProduk(0)"><?= $arrbrand ?></select>			<div class="input-group-append">				<button class="input-group-text" onclick="plusProduk(0);"><i class="la la-plus"></i></button>			</div>		</div>	</div>	<div class="col-lg-4" id="cardproduk0">		<label class="">Produk</label>		<select id="listproduk0" name="input2[0][id_product][0]" class="form-control pilihProduk"></select>	</div>	<div class="col-lg-4" id="cardpresentase0">		<label class="">Presentase (%)</label>		<input type="number" id="presentase0" name="input2[0][presentase][0]" value="100" class="form-control sum_persen" onkeyup="chekPersen();" />	</div></div></div>');
					// document.getElementById("hideProduk" + id).style.display = "block";
				} else {
					$('#hideProduk0').remove();
				}
			}
		});
		if ($('#akuncoa' + id + ' option:selected').val() == 1) {
			$('#coa3id0').val('1513')
		} else {
			$('#coa3id0').val('')
		}
	}

	function chekPersen() {
		var sum = 0;
		$('.sum_persen').each(function() {
			nilai_awal = $(this).val();
			if (nilai_awal == "") nilai_awal = 0;
			sum += parseFloat(nilai_awal);
		});
		if (sum != 100) {
			document.getElementById("btn_submit").style.display = "none";
		} else {
			document.getElementById("btn_submit").style.display = "block";

		}
	}

	function cekMax(i) {
		// if ($('#bayar' + i).val() == '') $('#bayar' + i).val(0);
		// if ($('#harga' + i).val() == '') $('#harga' + i).val(0);
		
		//UPDATE BAYU
		if ($('#harga' + i).val()!="") {
			if ($('#bayar' + i).val().replaceAll(",", "") > $('#harga' + i).val().replaceAll(",", "")) {
				$('#bayar' + i).val(commafy($('#harga' + i).val()));
			}
		} else {
			$('#bayar' + i).val(0);
		}
	}

	function cekNol(i) {
		if ($('#bayar' + i).val() == '') $('#bayar' + i).val(0);
		if ($('#harga' + i).val() == '') $('#harga' + i).val(0);
	}
</script>