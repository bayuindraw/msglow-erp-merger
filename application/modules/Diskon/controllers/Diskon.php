<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Diskon extends CI_Controller
{
    //TES
    var $url_ = "Diskon";
    var $id_  = "id_factory";
    var $eng_ = "Diskon";
    var $ind_ = "Diskon";

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            $this->url_ . 'Model'  =>  'Model',
        ));
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        // update discount to expired
        $this->db->query("UPDATE m_discount SET discount_active = 2 WHERE discount_end_date = DATE_FORMAT(NOW(), '%Y-%m-%d')");

        $datacontent['url'] = $this->url_;
        $datacontent['title'] = 'Data ' . $this->ind_; 
        $datacontent['diskons'] = $this->db->get('m_discount')->result_array();
        $data['file'] = $this->ind_;
        $data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
        $data['title'] = $datacontent['title'];
        $this->load->view('Layout/home', $data);
    }

    public function get_data_active()
    {
        $datacontent['datatable'] = $this->Model->get_data_active();
    }

    public function get_data_inactive()
    {
        $datacontent['datatable'] = $this->Model->get_data_inactive();
    }

    public function get_data_expired()
    {
        $datacontent['datatable'] = $this->Model->get_data_expired();
    }

    public function form()
    {
        $datacontent['url'] = $this->url_;
        $datacontent['title'] = 'Form Data ' . $this->ind_;
        $datacontent['m_member_status'] = $this->db->get('m_member_status')->result_array();
        $products = $this->db->get('produk_global')->result_array();
        $datacontent['arrproduct'] = '';
        foreach ($products as $product) {
            $datacontent['arrproduct'] .= '<option value="' . $product['kode'] . '">' . $product['nama_produk'] . '</option>';
        }
        $data['file'] = $this->ind_;
        $data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
        $data['title'] = $datacontent['title'];
        $this->load->view('Layout/home', $data);
    }

    public function edit($id)
    {
        $datacontent['url'] = $this->url_;
        $datacontent['id'] = $id;
        $datacontent['title'] = 'Data ' . $this->ind_;
        $products = $this->db->get('produk_global')->result_array();
        $datacontent['arrproduct'] = '';
        $datacontent['arrproduct_harga'] = '';
        $datacontent['arrproduct_quantity'] = '';
        $datacontent['discount'] = $this->db->get_where('m_discount', ['discount_id' => $id])->row_array();
        $discount_terms = $this->db->query("SELECT B.* FROM m_discount_terms A JOIN m_discount_terms_product B ON A.discount_terms_id = B.discount_terms_id WHERE A.discount_id = $id")->result_array();
        $datacontent['discount_terms'] = array();
        $discount_terms_product = [];
        $discount_terms_price_query = $this->db->query("SELECT B.* FROM m_discount_terms A JOIN m_discount_terms_product B ON A.discount_terms_id = B.discount_terms_id WHERE A.discount_id = $id GROUP BY B.product_global_code, B.discount_terms_product_price ORDER BY B.discount_terms_id ASC")->result_array();
        $discount_terms_price = [];
        $discount_terms_price_new = [];
        // $discount_terms_qty_query = $this->db->query("SELECT DISTINCT B.discount_terms_id, B.discount_terms_product_quantity, B.discount_terms_product_price, B.discount_terms_product_quantity_limit FROM m_discount_terms A JOIN m_discount_terms_product B ON A.discount_terms_id = B.discount_terms_id WHERE A.discount_id = $id GROUP BY B.discount_terms_product_quantity, B.discount_terms_id")->result_array();
        $discount_terms_qty_query = $this->db->query("SELECT DISTINCT B.product_global_code, B.discount_terms_id, B.discount_terms_product_quantity, B.discount_terms_product_price, B.discount_terms_product_quantity_limit FROM m_discount_terms A JOIN m_discount_terms_product B ON A.discount_terms_id = B.discount_terms_id WHERE A.discount_id = $id GROUP BY B.discount_terms_product_quantity, B.discount_terms_id, B.product_global_code ORDER BY B.discount_terms_id ASC")->result_array();
        $discount_terms_qty = [];
        // qty tiering
        if (count($discount_terms_qty_query) > 0) {
            foreach ($discount_terms_qty_query as $terms) {
                $discount_terms_qty[$terms['product_global_code']][] = (object) [
                    "qty" => $terms['discount_terms_product_quantity'],
                    "price" => $terms['discount_terms_product_price'],
                    "qty_limit" => $terms['discount_terms_product_quantity_limit'],
                ];
            }
        }

        foreach ($discount_terms as  $discount_term) {
            $product = $this->db->get_where('produk_global', ['kode' => $discount_term['product_global_code']])->row_array();
            if ($discount_term['discount_terms_product_type'] == 2) {
                $discount_term['nama_produk'] = $product['nama_produk'];
                $discount_terms_product[$discount_term['discount_terms_id']][$product['nama_produk']][] = $discount_term;
                $datacontent['discount_terms']['produk'][$discount_term['discount_terms_product_quantity']][$discount_term['discount_terms_id']][] = $discount_term;
            } else {
                $discount_term['nama_produk'] = $product['nama_produk'];
                $datacontent['discount_terms']['quantity'][$discount_term['discount_terms_id']][] = $discount_term;
            }
        }

        // echo "<pre>";
        // print_r($discount_terms_qty);
        // echo '</pre>';
        // die();

        // price
        $prcs = [];
        $qty_limit = [];

        if (count($discount_terms_price_query) > 0) {
            foreach ($discount_terms_price_query as $terms) {
                $product = $this->db->get_where('produk_global', ['kode' => $terms['product_global_code']])->row_array();
                $terms['nama_produk'] = $product['nama_produk'];
                $prcs[$terms['discount_terms_id']][] = $terms['discount_terms_product_price'];
                $discount_terms_price[$terms['discount_terms_id']]['limit'] = $terms['discount_terms_product_quantity_limit'];
                $discount_terms_price[$terms['discount_terms_id']]['tiering'] = $discount_terms_qty[$terms['product_global_code']]; //$discount_terms_qty[$terms['discount_terms_id']];
                $discount_terms_price[$terms['discount_terms_id']]['price'][] = $terms['discount_terms_product_price'];

                $discount_terms_price_new[$terms['discount_terms_product_quantity']][$terms['discount_terms_id']][] = $terms;
                $qty_limit[] = $terms['discount_terms_product_quantity_limit'];
            }

            foreach ($discount_terms_price as $i => $p) {
                $discount_terms_price[$i]['price'] = $this->my_array_unique($prcs[$i]);
                foreach($p['tiering'] as $tier) {
                    $discount_terms_price[$i]['data'][] = (object) [
                        "tier" => $tier,
                    ];
                }
            }
        }
        
        $datacontent['discount_terms_price'] = $discount_terms_price_new; //$discount_terms_price;
        $datacontent['discount_terms_product'] = $discount_terms_product;
        $datacontent['qty_limit'] = max($qty_limit);

        // echo "<pre>";
        // print_r($discount_terms_price_new);
        // echo '</pre>';
        // die();

        $members = $this->db->get_where('m_discount_member', ['discount_id' => $id])->result_array();
        foreach ($members as $member) {
            $datacontent['member'][$member['member_status']] = $member;
        }
        foreach ($products as $product) {
            $datacontent['arrproduct'] .= '<option value="' . $product['kode'] . '">' . $product['nama_produk'] . '</option>';
        }
        $rewards = $this->db->query("SELECT B.* FROM m_discount_reward A JOIN m_discount_reward_product B ON A.discount_reward_id = B. discount_reward_id WHERE A.discount_id = $id")->result_array();

        foreach ($rewards as $reward) {
            if ($reward['discount_reward_product_type'] == 1) {
                $datacontent['reward']['harga'] = $reward['discount_reward_product_deduction_cost'];
            }
            if ($reward['discount_reward_product_type'] == 2) {
                $datacontent['reward']['persen'] = $reward['discount_reward_product_percentage'];
            }
            if ($reward['discount_reward_product_type'] == 3) {
                $product = $this->db->get_where('produk', ['kd_pd' => $reward['product_global_code']])->row_array();
                $reward['nama_produk'] = $product['nama_produk'];
                $datacontent['reward']['barang'][$reward['discount_reward_id']][] = $reward;
            }
        }
        $data['file'] = $this->ind_;
        $data['content'] = $this->load->view($this->url_ . '/form_edit', $datacontent, TRUE);
        $data['title'] = $datacontent['title'];
        $this->load->view('Layout/home', $data);
    }

    function my_array_unique($array, $keep_key_assoc = false){
        $duplicate_keys = array();
        $tmp = array();       
    
        foreach ($array as $key => $val){
            // convert objects to arrays, in_array() does not support objects
            if (is_object($val))
                $val = (array)$val;
    
            if (!in_array($val, $tmp))
                $tmp[] = $val;
            else
                $duplicate_keys[] = $key;
        }
    
        foreach ($duplicate_keys as $key)
            unset($array[$key]);
        return $keep_key_assoc ? $array : array_values($array);
    }

    public function simpan()
    {
        $this->db->trans_begin();
        $post = $this->input->post();

        // echo "<pre>";
        // print_r($post);
        // echo '</pre>';
        // die();

        $products_tier_price = [];
        if(isset($post['products_tier'])) {
            foreach ($post['products_tier'] as $prods) {
                foreach ($prods['price'] as $price) {
                    $products_tier_price[] = $price;
                }
            }
        }
        
        // $data_insert = [];
        // foreach ($post['products_tier'] as $prods) {            
        //     if (isset($prods['product'])) {
        //         foreach ($prods['product'] as $product) {
        //             $products_qty = $post['tiering']['quantity'];
        //             $max_qty = max(array_keys($products_qty));
        //             if (isset($products_qty)) {
        //                 foreach ($products_qty as $idx => $qty) {
        //                     $dt_prod_global = $this->Model->get_table_where("id","produk_global","kode = '".$product."'")->result_array();
        //                     $qty_limit = $idx == $max_qty ? str_replace(",", "", $post['qty_limit']) : str_replace(",", "", (double)$products_qty[$idx+1]-1);
        //                     $data_insert[] = [
        //                         'product_global_id' => $dt_prod_global[0]['id'],
        //                         'product_global_code' => $product,
        //                         'discount_terms_product_quantity' => str_replace(",", "", (double)$qty),
        //                         'discount_terms_product_price' => str_replace(",", "", $prods['price'][$idx]),
        //                         'discount_terms_product_type' => 2,
        //                         'discount_terms_product_quantity_limit' => $qty_limit,
        //                     ];
        //                 }
        //             }
        //         }
        //     }
        // }

        // echo "<pre>";
        // print_r($data_insert);
        // echo '</pre>';
        // die();
        
        // discount tiering
        if($post['discount_type'] == 2) {
            // insert into m_discount
            $discount = [
                'discount_code' => $post['discount_code'],
                'discount_name' => $post['discount_name'],
                'discount_start_date' => $post['tgl_mulai'],
                'discount_end_date' => $post['tgl_selesai'],
                'discount_create_user_id' => $_SESSION['user_id'],
                'discount_active' => ((date('Ymd') >= str_replace('-', '', $post['tgl_mulai']) && date('Ymd') <= str_replace('-', '', $post['tgl_selesai'])) ? 1 : 0),
                'discount_type' => $post['discount_type'],
                'warehouse_id' => $_SESSION['warehouse_id'],
            ];
            $this->db->insert('m_discount', $discount);
            $discount_id = $this->db->insert_id();

            // insert into m_discount_member
            if (isset($post['members'])) {
                foreach ($post['members'] as $member) {
                    $diskon_member = ([
                        'discount_id' =>  $discount_id,
                        'member_status' => $member,
                    ]);
                    $this->db->insert('m_discount_member', $diskon_member);
                }
            }
            
            // insert into m_terms & m_terms_product
            if (isset($post['tiering'])) {
                foreach ($post['tiering']['quantity'] as $idx => $qty) {
                    $products_qty = $post['tiering']['quantity'];

                    // insert into m_discount_header
                    $discount_header = [
                        'discount_id' =>  $discount_id,
                    ];
                    $this->db->insert('m_discount_header', $discount_header);
                    $discount_header_id = $this->db->insert_id();

                    // insert into m_discount_reward
                    $discount_reward = [
                        'discount_id' => $discount_id,
                        'discount_header_id' => $discount_header_id,
                        // 'discount_terms_id' => $discount_terms_id,
                    ];
                    $this->db->insert('m_discount_reward', $discount_reward);
                    $discount_reward_id = $this->db->insert_id();

                    if (count($products_tier_price) > 0) {
                        foreach ($products_tier_price as $tier_price) {
                            // insert into m_discount_reward_product
                            $diskon_reward_product = [
                                'discount_reward_id' => $discount_reward_id,
                                'discount_reward_product_type' => 4,
                                // 'discount_reward_product_price' => str_replace(",", "", $tier_price),
                                // 'discount_terms_id' => $discount_terms_id,
                            ];
                            $this->db->insert('m_discount_reward_product', $diskon_reward_product);
                        }
                    }

                    foreach ($post['products_tier'] as $prods) {
                        // insert into m_discount_terms
                        $this->db->insert('m_discount_terms', ['discount_id' => $discount_id, 'discount_header_id' => $discount_header_id]);
                        $discount_terms_id = $this->db->insert_id();
                        
                        if (isset($prods['product'])) {
                            foreach ($prods['product'] as $product) {
                                $max_qty = max(array_keys($products_qty));
                                if (isset($products_qty)) {
                                    $dt_prod_global = $this->Model->get_table_where("id","produk_global","kode = '".$product."'")->result_array();
                                    $qty_limit = $idx == $max_qty ? str_replace(",", "", $post['qty_limit']) : str_replace(",", "", (double)$products_qty[$idx+1]-1);
                                    $diskon_term_prod = [
                                        'discount_terms_id' => $discount_terms_id,
                                        'discount_id' => $discount_id,
                                        'product_global_id' => $dt_prod_global[0]['id'],
                                        'product_global_code' => $product,
                                        'discount_terms_product_quantity' => str_replace(",", "", (double)$qty),
                                        'discount_terms_product_price' => str_replace(",", "", $prods['price'][$idx]),
                                        'discount_terms_product_type' => 3,
                                        'discount_terms_product_quantity_limit' => $qty_limit,
                                    ];
                                    $this->db->insert('m_discount_terms_product', $diskon_term_prod);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            //DISKON KELIPATAN DAN MINIMAL PEMBELIAN PER PCS BAYU
            if ($this->input->post('tgl_selesai') >= date('Y-m-d') and $this->input->post('tgl_mulai') <= date('Y-m-d')) {
                $disc_act = 1;
            } else {
                $disc_act = 0;
            }
            $diskon = ([
                'discount_code' => $this->input->post('discount_code'),
                'discount_name' => $this->input->post('discount_name'),
                'discount_start_date' => $this->input->post('tgl_mulai'),
                'discount_end_date' => $this->input->post('tgl_selesai'),
                'discount_create_user_id' => $this->session->user_id,
                'discount_create_date' => date('Y-m-d H:i:s'),
                'discount_active' => $disc_act,
                'discount_type' => 1,
                'discount_category' => $this->input->post('discount_category'),
                'warehouse_id' => $this->session->warehouse_id
            ]);

            if (isset($_POST['akumulasi'])) {
                $diskon['discount_accumulation'] = 1;
            }
    
            $this->db->insert('m_discount', $diskon);
            $diskon['discount_id'] = $this->db->insert_id();
    
            if (isset($_POST['members'])) {
                foreach ($_POST['members'] as $index => $for) {
                    $diskon_member = ([
                        'discount_id' =>  $diskon['discount_id'],
                        'member_status' => $index,
                    ]);
                    $this->db->insert('m_discount_member', $diskon_member);
                }
            }
    
            //DISKON HEADER BAYU
            $diskon_header = ([
                'discount_id' =>  $diskon['discount_id']
            ]);
            $this->db->insert('m_discount_header', $diskon_header);
            $disc_header_id = $this->db->insert_id();
    
            //SYARAT QUANTITY BAYU
            if (isset($this->input->post('input2')[0]['product'])) {
                foreach ($this->input->post('input2') as $input2) {
                    $data_in = array(
                        'discount_header_id' => $disc_header_id,
                        'discount_id' => $diskon['discount_id']
                    );
                    $this->db->insert('m_discount_terms', $data_in);
                    $discount_terms_id = $this->db->insert_id();
    
                    foreach ($input2['product'] as $product) {
                        $dt_prod_global = $this->Model->get_table_where("id","produk_global","kode = '".$product."'")->result_array();
    
                        $diskon_term_prod = array();
                        $diskon_term_prod = ([
                            'discount_terms_id' => $discount_terms_id,
                            'discount_id' => $diskon['discount_id'],
                            'product_global_id' => $dt_prod_global[0]['id'],
                            'product_global_code' => $product,
                            'discount_terms_product_quantity' => str_replace(",", "", $input2['quantity']),
                            'discount_terms_product_type' => 1,
                        ]);
                        $this->db->insert('m_discount_terms_product', $diskon_term_prod);
                    }
                }
            }
    
            //INSERT m_discount_reward BAYU
            $data_in = array(
                'discount_id' => $diskon['discount_id'],
                'discount_header_id' => $disc_header_id
            );
            $this->db->insert('m_discount_reward', $data_in);
            $discount_reward_id = $this->db->insert_id();
            
            //INSERT m_discount_reward_product DISKON MINIMAL PEMBELIAN PER PCS BAYU
            if ($this->input->post('diskon') !== '') {
                $diskon_reward_product = ([
                    'discount_reward_id' => $discount_reward_id,
                    'discount_reward_product_type' => 2,
                    'discount_reward_product_percentage' => str_replace(",", "", $this->input->post('diskon'))
                ]);
                $this->db->insert('m_discount_reward_product', $diskon_reward_product);
            }
    
            //INSERT m_discount_reward_product BAYU
            foreach ($this->input->post('input3') as $input3) {
                if (isset($input3['product'])) {
                    foreach ($input3['product'] as $product) {
                        $dt_prod_global = $this->Model->get_table_where("id","produk_global","kode = '".$product."'")->result_array();
    
                        $diskon_reward_product = array();
                        $diskon_reward_product = ([
                            'discount_reward_id' => $discount_reward_id,
                            'discount_reward_product_type' => 3,
                            'product_global_id' => $dt_prod_global[0]['id'],
                            'product_global_code' => $product,
                            'discount_reward_product_quantity' => $input3['quantity'],
                        ]);
                        $this->db->insert('m_discount_reward_product', $diskon_reward_product);
                    }
                }
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        redirect(base_url() . 'Diskon');
    }

    public function simpan_edit($id)
    {
        $this->db->trans_begin();
        $this->db->query("DELETE FROM m_discount_terms A JOIN m_discount_terms_product B ON A.discount_terms_id = B.discount_terms_id WHERE A.discount_id = $id");
        $this->db->query("DELETE FROM m_discount_reward A JOIN m_discount_reward_product B ON A.discount_reward_id = B.discount_reward_id WHERE A.discount_id = $id");
        $this->db->query("DELETE FROM m_discount_member WHERE discount_id = $id");
        $diskon = $this->db->get_where('m_discount', ['discount_id' => $id])->row_array();
        if (isset($_POST['members'])) {
            foreach ($_POST['members'] as $index => $for) {
                $diskon_member = ([
                    'discount_id' =>  $diskon['discount_id'],
                    'member_status' => $index,
                ]);
                $this->db->insert('m_discount_member', $diskon_member);
            }
        }
        if (isset($_POST['products_tier'][0]['product'])) {
            foreach ($_POST['products_tier'] as $input1) {
                $this->db->insert('m_discount_terms', ['discount_id' => $diskon['discount_id']]);
                $diskon_term['discount_terms_id'] = $this->db->insert_id();

                foreach ($input1['product'] as $product) {
                    $diskon_term_prod = array();
                    $diskon_term_prod = ([
                        'discount_terms_id' => $diskon_term['discount_terms_id'],
                        'discount_id' => $diskon['discount_id'],
                        'product_kd' => $product,
                        'discount_terms_product_price' => str_replace(",", "", $input1['price']),
                        'discount_terms_product_type' => 2,
                    ]);
                    $this->db->insert('m_discount_terms_product', $diskon_term_prod);
                }
            }
        }
        if (isset($_POST['input2'][0]['product'])) {
            foreach ($_POST['input2'] as $input2) {
                $this->db->insert('m_discount_terms', ['discount_id' => $diskon['discount_id']]);
                $diskon_term['discount_terms_id'] = $this->db->insert_id();

                foreach ($input2['product'] as $product) {
                    $diskon_term_prod = array();
                    $diskon_term_prod = ([
                        'discount_terms_id' => $diskon_term['discount_terms_id'],
                        'discount_id' => $diskon['discount_id'],
                        'product_kd' => $product,
                        'discount_terms_product_quantity' => str_replace(",", "", $input2['quantity']),
                        'discount_terms_product_type' => 1,
                    ]);
                    $this->db->insert('m_discount_terms_product', $diskon_term_prod);
                }
            }
        }
        $this->db->insert('m_discount_reward', ['discount_id' => $diskon['discount_id']]);
        $diskon_reward['discount_reward_id'] = $this->db->insert_id();

        if ($_POST['promo']['pot_harga'] !== '' || $_POST['promo']['diskon'] !== '') {
            $pot_harga = str_replace(",", "", $_POST['promo']['pot_harga']);
            if ($pot_harga > 0 || $pot_harga != '') {
                $diskon_reward_product = ([
                    'discount_reward_product_type' => 1,
                    'discount_reward_product_deduction_cost' => $pot_harga,
                ]);
            } else {
                $diskon_reward_product = ([
                    'discount_reward_product_type' => 2,
                    'discount_reward_product_percentage' => str_replace(",", "", $_POST['promo']['diskon']),
                ]);
            }
            $diskon_reward_product['discount_reward_id'] = $diskon_reward['discount_reward_id'];
            $this->db->insert('m_discount_reward_product', $diskon_reward_product);
        }

        foreach ($_POST['input3']  as $input3) {
            if (isset($input3['product'])) {
                $this->db->insert('m_discount_reward', ['discount_id' => $diskon['discount_id']]);
                $diskon_reward['discount_reward_id'] = $this->db->insert_id();
                foreach ($input3['product'] as $product) {
                    $diskon_reward_product = ([
                        'discount_reward_id' => $diskon_reward['discount_reward_id'],
                        'discount_reward_product_type' => 3,
                        'product_kd' => $product,
                        'discount_reward_product_quantity' => $input3['quantity'],
                    ]);
                    $this->db->insert('m_discount_reward_product', $diskon_reward_product);
                }
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        redirect(base_url() . 'Diskon');
    }

    public function non_active($id)
    {
        $this->db->query("UPDATE m_discount SET discount_active = 0 WHERE discount_id = $id");
        redirect(base_url() . 'Diskon');
    }

    public function cek_code()
    {
        //CEK DOUBLE KODE DISKON BAYU
        $dc = $this->input->post("discount_code");
        $jml_code = $this->Model->get_table_where("discount_id","m_discount","discount_code = '".$dc."'")->num_rows();
        if ($jml_code>0) {
            echo json_encode(array("status"=>false, "message"=>"Kode diskon <b>".$this->input->post('discount_code')."</b> sudah digunakan!"));
        } else {
            echo json_encode(array("status"=>true, "message"=>"OK"));
        }
    }
}
