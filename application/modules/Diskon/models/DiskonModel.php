<?php
defined('BASEPATH') or exit('No direct script access allowed');
class DiskonModel extends CI_Model
{

    var $table_ = "m_discount";
    var $id_ = "discount_id";
    var $eng_ = "Diskon";
    var $url_ = "Diskon";

    function get_data_active()
    {
        $table = 'm_discount';
        $id = $this->id_;
        $field = array('discount_name', 'discount_code', 'discount_type', 'discount_start_date', 'discount_end_date', 'discount_active');
        $url = $this->url_;
        $arrwhere[] = "discount_active = 1";
        $action = '<a href="' . base_url() . 'Diskon/edit/xid" class="btn btn-info btn-xs"><span class="btn-label"><i class="fas fa-edit"></i></span></a>';
        $jfield = join(', ', $field);
        $start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
        $length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
        $limit = "LIMIT $start, $length";
        $join = "";
        if (@$arrjoin != "") {
            foreach ($arrjoin as $jkey => $jvalue) {
                $arrjoin2[] = $jvalue;
            }
            $join = join('', $arrjoin2);
        }
        $where = "";
        $where2 = "";
        $search = $_GET['search']['value'];
        $arrwhere2 = @$arrwhere;
        if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
        if (@$search != "") {
            foreach ($field as $key => $value) {
                $arrfield[] = "$value LIKE '%$search%'";
            }
            $arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
        }
        if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
        foreach (@$_GET['order'] as $key2 => $value2) {
            $arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
        }
        $order = 'ORDER BY ' . join(', ', $arrorder);
        $data = array();
        $jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
        $jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
        $result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
        $i = $start;
        $dataxy = array();
        foreach ($result as $keyr => $valuer) {
            $i++;
            $datax = array();
            $datax[] = $i;
            $valuer['discount_type'] = $valuer['discount_type'] == 2 ? 'Tiering' : 'Non-Tiering';
            if ($valuer['discount_active'] == 1) $valuer['discount_active'] = '<span class="kt-badge kt-badge--inline kt-badge--brand">Active</span>';
            elseif ($valuer['discount_active'] == 2) $valuer['discount_active'] = '<span class="kt-badge kt-badge--inline kt-badge--danger">Expired</span>';
            else $valuer['discount_active'] = '<span class="kt-badge kt-badge--inline kt-badge--warning">Non Active</span>';
            foreach ($field as $keyfield) {
                $datax[] = $valuer[$keyfield];
            }
            $datax[] = str_replace('xid', $valuer[$id], $action);
            $dataxy[] = $datax;
        }
        $data = array(
            'draw' => $_GET['draw'],
            'recordsTotal' => (int)$jum_all['jum'],
            'recordsFiltered' => (int)$jum_filter['jum'],
            'data' => @$dataxy
        );

        echo json_encode($data);
    }

    function get_data_inactive()
    {
        $table = 'm_discount';
        $id = $this->id_;
        $field = array('discount_name', 'discount_code', 'discount_type', 'discount_start_date', 'discount_end_date', 'discount_active');
        $url = $this->url_;
        $arrwhere[] = "discount_active = 0 ";
        $action = '<a href="' . base_url() . 'Diskon/edit/xid" class="btn btn-info btn-xs"><span class="btn-label"><i class="fas fa-edit"></i></span></a>';
        $jfield = join(', ', $field);
        $start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
        $length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
        $limit = "LIMIT $start, $length";
        $join = "";
        if (@$arrjoin != "") {
            foreach ($arrjoin as $jkey => $jvalue) {
                $arrjoin2[] = $jvalue;
            }
            $join = join('', $arrjoin2);
        }
        $where = "";
        $where2 = "";
        $search = $_GET['search']['value'];
        $arrwhere2 = @$arrwhere;
        if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
        if (@$search != "") {
            foreach ($field as $key => $value) {
                $arrfield[] = "$value LIKE '%$search%'";
            }
            $arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
        }
        if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
        foreach (@$_GET['order'] as $key2 => $value2) {
            $arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
        }
        $order = 'ORDER BY ' . join(', ', $arrorder);
        $data = array();
        $jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
        $jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
        $result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
        $i = $start;
        $dataxy = array();
        foreach ($result as $keyr => $valuer) {
            $i++;
            $datax = array();
            $datax[] = $i;
            $valuer['discount_type'] = $valuer['discount_type'] == 2 ? 'Tiering' : 'Non-Tiering';
            if ($valuer['discount_active'] == 1) $valuer['discount_active'] = '<span class="kt-badge kt-badge--inline kt-badge--brand">Active</span>';
            elseif ($valuer['discount_active'] == 2) $valuer['discount_active'] = '<span class="kt-badge kt-badge--inline kt-badge--danger">Expired</span>';
            else $valuer['discount_active'] = '<span class="kt-badge kt-badge--inline kt-badge--warning">Non Active</span>';
            foreach ($field as $keyfield) {
                $datax[] = $valuer[$keyfield];
            }
            $datax[] = str_replace('xid', $valuer[$id], $action);
            $dataxy[] = $datax;
        }
        $data = array(
            'draw' => $_GET['draw'],
            'recordsTotal' => (int)$jum_all['jum'],
            'recordsFiltered' => (int)$jum_filter['jum'],
            'data' => @$dataxy
        );

        echo json_encode($data);
    }

    function get_data_expired()
    {
        $table = 'm_discount';
        $id = $this->id_;
        $field = array('discount_name', 'discount_code', 'discount_type', 'discount_start_date', 'discount_end_date', 'discount_active');
        $url = $this->url_;
        $arrwhere[] = "discount_active = 2";
        $action = '<a href="' . base_url() . 'Diskon/edit/xid" class="btn btn-info btn-xs"><span class="btn-label"><i class="fas fa-edit"></i></span></a>';
        $jfield = join(', ', $field);
        $start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
        $length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
        $limit = "LIMIT $start, $length";
        $join = "";
        if (@$arrjoin != "") {
            foreach ($arrjoin as $jkey => $jvalue) {
                $arrjoin2[] = $jvalue;
            }
            $join = join('', $arrjoin2);
        }
        $where = "";
        $where2 = "";
        $search = $_GET['search']['value'];
        $arrwhere2 = @$arrwhere;
        if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
        if (@$search != "") {
            foreach ($field as $key => $value) {
                $arrfield[] = "$value LIKE '%$search%'";
            }
            $arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
        }
        if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
        foreach (@$_GET['order'] as $key2 => $value2) {
            $arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
        }
        $order = 'ORDER BY ' . join(', ', $arrorder);
        $data = array();
        $jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
        $jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
        $result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
        $i = $start;
        $dataxy = array();
        foreach ($result as $keyr => $valuer) {
            $i++;
            $datax = array();
            $datax[] = $i;
            $valuer['discount_type'] = $valuer['discount_type'] == 2 ? 'Tiering' : 'Non-Tiering';
            if ($valuer['discount_active'] == 1) $valuer['discount_active'] = '<span class="kt-badge kt-badge--inline kt-badge--brand">Active</span>';
            elseif ($valuer['discount_active'] == 2) $valuer['discount_active'] = '<span class="kt-badge kt-badge--inline kt-badge--danger">Expired</span>';
            else $valuer['discount_active'] = '<span class="kt-badge kt-badge--inline kt-badge--warning">Non Active</span>';
            foreach ($field as $keyfield) {
                $datax[] = $valuer[$keyfield];
            }
            $datax[] = str_replace('xid', $valuer[$id], $action);
            $dataxy[] = $datax;
        }
        $data = array(
            'draw' => $_GET['draw'],
            'recordsTotal' => (int)$jum_all['jum'],
            'recordsFiltered' => (int)$jum_filter['jum'],
            'data' => @$dataxy
        );

        echo json_encode($data);
    }

    function get_table_where($select, $table, $where)
    {
        $this->db->select($select);
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query;
    }
}
