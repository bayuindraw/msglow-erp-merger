<form method="post" action="<?= site_url($url . '/Diskon/simpan'); ?>" enctype="multipart/form-data" onsubmit="return confirm('Are you sure?')">
    <div class="row">
        <div class="col-lg-12 col-xl-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Total Pesanan & Syarat Quantity
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Nama Diskon</label>
                                <input type="text" class="form-control" placeholder="Nama Diskon" name="input_diskon[discount_name]" value="<?= @$row['discount_name'] ?>" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Diskon Code</label>
                                <input type="text" class="form-control" placeholder="Diskon Code" name="discount_code" id="discount_code" value="" onblur="cek_double()" oninput="this.value = this.value.replace(/[^A-Za-z0-9]/g, '');" required>
                                <span style="color: red; font-style: italic;" id="alert-code"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label>Tanggal Mulai</label>
                            <input type="text" id="tgl_awal" name="tgl_mulai" onchange="$('#tgl_akhir').val($(this).val());" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required>
                        </div>
                        <div class="col-sm-6">
                            <label>Tanggal Selesai</label>
                            <input type="text" id="tgl_akhir" name="tgl_selesai" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Tipe Diskon</label>
                                <div class="kt-radio-list">
                                    <label class="kt-radio">
                                        <input type="radio" name="discount_type" value="2" id="discount_type2" onchange="chk_discount_type()"> Tiering
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" name="discount_type" value="1" id="discount_type1" onchange="chk_discount_type()"> Non-Tiering
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <label>Promo Untuk:</label>
                    <div class="row"> 
                        <?php foreach($m_member_status as $index_m_status => $value_m_status){	?>
                        <div class="col-lg-2">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                <input type="checkbox" value="<?= $value_m_status['member_status_name']; ?>" name="for[<?= $value_m_status['member_status_name']; ?>]"> <?= $value_m_status['member_status_name']; ?>
                                <span></span>
                            </label>
                        </div>
                        <?php }	?>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12">
            <div class="kt-portlet tiering" style="display:none;">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Diskon
                        </h3>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <div id="totalPesanan">
                        <h5>Produk</h5>
                        <?php $index1 = 1 ?>
                        <div class="form-group row" id="totalPesanan1">
                            <div class="col-lg-6">

                                <h5><?= $index1 ?></h5><input type="hidden" value="<?= $index1 ?>" class="idtiering"> <select class="form-control pilihProduk" name="input1[1][product][]" multiple="multiple">
                                    <?= $arrproduct ?>
                                </select>
                            </div>
                            <div class="col-lg-1">
                                <br>
                                <button onclick="deleteTotalPesanan(1);" class="btn btn-danger">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="kt-form__actions">
                        <button type="button" class="btn btn-warning" onclick="addFormTotal();">Tambah Produk</button>
                    </div>
                    <br>

                </div>

                <div class="kt-portlet__body">
                    <h5>Tiering Produk</h5>
                    <div id="div-tiering">
                        <div id="totalPesanan1">
                            <div class="form-group row" >
                                <div class="col-lg-3">
                                    <input type="text" class="form-control numeric" placeholder="input jumlah" name="input1[1][quantity]" />
                                </div>
                                <div class="col-lg-1">

                                    <h5><?= $index1 ?></h5>
                                </div>
                                <div class="col-lg-4">

                                    <input type="text" class="form-control numeric" placeholder="input harga" name="input1[1][1][price]" />
                                </div>
                                <div class="col-lg-1">

                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control numeric" placeholder="Quantity Limit" name="input_limit" />
                    </div>

                    <div class="kt-form__actions">
                        <button type="button" class="btn btn-warning" onclick="addFormTiering();">Tambah Tiering</button>
                    </div>
                    <br>

                </div>
            </div> 
        </div>
        <div class="col-lg-12 col-xl-12">
            <div class="kt-portlet non-tiering" style="display:none;">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Syarat
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Kategori Diskon</label>
                            <div class="kt-radio-list">
                                <label class="kt-radio">
                                    <input type="radio" name="discount_category" value="1" id="discount_category1" onchange="chk_discount_category()"> Kelipatan
                                    <span></span>
                                </label>
                                <label class="kt-radio">
                                    <input type="radio" name="discount_category" value="2" id="discount_category2" onchange="chk_discount_category()" checked> Minimal Pembelian Per PCS
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <h5>Syarat Quantity</h5>
                        <div id="syaratQuantity">
                            <?php $index2 = 0 ?>
                            <div class="form-group row" id="syaratQuantity0">
                                <div class="col-lg-6">
                                    <label>Produk</label>
                                    <select class="form-control pilihProduk" name="input2[0][product][]" multiple="multiple">
                                        <?= $arrproduct ?>
                                    </select>
                                </div>
                                <div class="col-lg-5">
                                    <label>Jumlah</label>
                                    <input type="text" class="form-control numeric" placeholder="input jumlah" name="input2[0][quantity]" />
                                </div>
                                <div class="col-lg-1">
                                    <br>
                                    <button onclick="deleteSyaratQuantity(0);" class="btn btn-danger">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="kt-form__actions">
                            <button type="button" class="btn btn-warning" onclick="addFormQuantity();">Tambah Produk</button>
                        </div>
                        <br>
                    </div>
                </div>

                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Promo
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
                        <div class="form-group row" id="discount_percentage">

                            <div class="col-lg-12">
                                <label>Diskon (%)</label>
                                <input type="number" class="form-control" placeholder="input diskon" max="100" name="diskon" />
                            </div>
                        </div>
                        <div id="freeProduk">
                            <?php $index3 = 0 ?>
                            <div class="row col-lg-6">
                                <label style="font-size: 10pt; font-weight: 400;">Free Produk</label>
                            </div>
                            <div class="form-group" id="freeProduk0">
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <select class="form-control pilihProduk" name="input3[0][product][]" multiple="multiple">
                                            <?= $arrproduct ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control numeric" placeholder="input jumlah" name="input3[0][quantity]" />
                                    </div>
                                    <div class="col-lg-1">
                                        <button onclick="deleteFreeProduk(0);" class="btn btn-danger">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-form__actions">
                            <button type="button" class="btn btn-warning" onclick="addFreeProduk();">Tambah Produk</button>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
    <div class="kt-portlet col-lg-12 col-xl-12">
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                <a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    var index1 = <?= $index1 ?>;
    var index2 = <?= $index2 ?>;
    var index3 = <?= $index3 ?>;
    var indextiering = 1;

    $('.pilihProduk').select2({
        placeholder: "Pilih Produk",
    });

    function cek_double() {
        //CEK DOUBLE DISKON CODE BAYU
        var dc = document.getElementById("discount_code").value;
        $.ajax({
            type:'post',
            data:{
                'discount_code':dc
            },
            url: '<?php echo site_url('Diskon/cek_code') ?>',
            success: function(data){
                var json = $.parseJSON(data);
                if(!json.status){
                    document.getElementById("alert-code").innerHTML = json.message;
                    $("#alert-code").show();
                } else {
                    $("#alert-code").hide();                    
                }
            }
        });
    }

    function chk_discount_type(){

        if(document.getElementById("discount_type2").checked === true){
            $('.tiering').show();
            $('.non-tiering').hide();
        }else if(document.getElementById("discount_type1").checked === true){
            $('.tiering').hide();
            $('.non-tiering').show();
        }
    }

    function chk_discount_category(){

        if(document.getElementById("discount_category2").checked === true){
            $('#discount_percentage').show();
        }else if(document.getElementById("discount_category1").checked === true){
            $('#discount_percentage').hide();
        }
    }

    function addFormTotal() { 
        index1 = index1 + 1;
        var html = '<div class="form-group row" id="totalPesanan' + index1 + '"><div class="col-lg-6">   <h5>' + (index1) + '</h5><input type="hidden" value="' + (index1) + '" class="idtiering"> <select class="form-control pilihProduk" name="input1[' + index1 + '][product][]" multiple="multiple">        <?= $arrproduct ?>    </select></div><div class="col-lg-1"><br><button onclick="deleteTotalPesanan(' + index1 + ');" class="btn btn-danger">    <i class="fas fa-trash"></i></button></div></div>';
        $('#totalPesanan').append(html);
        $('.pilihProduk').select2({
            placeholder: "Pilih Produk",
        });
        $(".numeric").mask("#,##0", {
            reverse: true
        });
        indextiering = 1;
        var htmltiering = counttiering();
        $('#div-tiering').html(htmltiering);
    }

    function addFormTiering() { 
        indextiering = indextiering + 1;
        var htmltiering = counttiering();
        $('#div-tiering').append(htmltiering);
        $(".numeric").mask("#,##0", {
            reverse: true
        });
    }

    function counttiering(){
        var html = "";
        var first = 0;
        html = html + '<div id="FormtotalTiering'+ indextiering +'">';
        $('.idtiering').each(function() {

            if(first === 0){
                if(indextiering === 1){
                    html = html + '<div class="form-group row"><div class="col-lg-3"><input type="text" class="form-control numeric" placeholder="input jumlah" name="input1['+ indextiering +'][quantity]" /></div><div class="col-lg-1"><h5>'+ $(this).val() +'</h5></div><div class="col-lg-4"><input type="text" class="form-control numeric" placeholder="input harga" name="input1['+ indextiering +']['+ $(this).val() +'][price]" /></div><div class="col-lg-1"></div></div>';
                }else{
                    html = html + '<div class="form-group row"><div class="col-lg-3"><input type="text" class="form-control numeric" placeholder="input jumlah" name="input1['+ indextiering +'][quantity]" /></div><div class="col-lg-1"><h5>'+ $(this).val() +'</h5></div><div class="col-lg-4"><input type="text" class="form-control numeric" placeholder="input harga" name="input1['+ indextiering +']['+ $(this).val() +'][price]" /></div><div class="col-lg-1"><br><button onclick="deleteFormTiering('+ indextiering +');" class="btn btn-danger">    <i class="fas fa-trash"></i></button></div></div>';
                }
            }else{
                html = html + '<div class="form-group row"><div class="col-lg-3"></div><div class="col-lg-1"><h5>'+ $(this).val() +'</h5></div><div class="col-lg-4"><input type="text" class="form-control numeric" placeholder="input harga" name="input1['+ indextiering +']['+ $(this).val() +'][price]" /></div><div class="col-lg-1"></div></div>';
            }
            first++;
        });
        html = html + '</div>';
        return html;
    }

    function deleteFormTiering(id) {
        $('#FormtotalTiering' + id).remove();
    }

    function deleteTotalPesanan(id) {
        $('#totalPesanan' + id).remove();
        indextiering = 1;
        var htmltiering = counttiering();
        $('#div-tiering').html(htmltiering);
    }

    function addFormQuantity() {
        index2 = index2 + 1;
        var html = '<div class="form-group row" id="syaratQuantity' + index2 + '"><div class="col-lg-6">    <label>Produk</label>    <select class="form-control pilihProduk" name="input2[' + index2 + '][product][]" multiple="multiple">        <?= $arrproduct ?>    </select></div><div class="col-lg-5">    <label>Jumlah</label>    <input type="text" class="form-control numeric" placeholder="input jumlah" name="input2[' + index2 + '][quantity]" /></div><div class="col-lg-1"><br><button onclick="deleteSyaratQuantity(' + index2 + ');" class="btn btn-danger">    <i class="fas fa-trash"></i></button></div></div>';
        $('#syaratQuantity').append(html);
        $('.pilihProduk').select2({
            placeholder: "Pilih Produk",
        });
        $(".numeric").mask("#,##0", {
            reverse: true
        });
    }

    function deleteSyaratQuantity(id) {
        $('#syaratQuantity' + id).remove();
    }

    function addFreeProduk() {
        index3 = index3 + 1;
        var html = '<div class="form-group" id="freeProduk' + index3 + '"><div class="form-group row">    <div class="col-lg-6">        <select class="form-control pilihProduk" name="input3[' + index3 + '][product][]" multiple="multiple">            <?= $arrproduct ?>        </select>    </div>    <div class="col-lg-5">        <input type="text" class="form-control numeric" placeholder="input jumlah" name="input3[' + index3 + '][quantity]" />    </div>    <div class="col-lg-1"><button onclick="deleteFreeProduk(' + index3 + ');" class="btn btn-danger">            <i class="fas fa-trash"></i>        </button>    </div></div></div>';
        $('#freeProduk').append(html);
        $('.pilihProduk').select2({
            placeholder: "Pilih Produk",
        });
        $(".numeric").mask("#,##0", {
            reverse: true
        });
    }

    function deleteFreeProduk(id) {
        $('#freeProduk' + id).remove();
    }

    $(document).ready(function() {
        $("#alert-code").hide();
        var fd = '<?php echo $this->session->flashdata("gagal") ?>';
        if (fd!="") {
            alert(fd);
        }

        $('input[type="number"]').on('keyup', function() {
            v = parseInt($(this).val());
            min = parseInt($(this).attr('min'));
            max = parseInt($(this).attr('max'));

            /*if (v < min){
                $(this).val(min);
            } else */
            if (v > max) {
                $(this).val(max);
            }
        });
    });
</script>