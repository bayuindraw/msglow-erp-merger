<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                <?= $title ?>
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a href="<?= site_url() . $url . "/form" ?>" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-plus"></i>
                        Tambah Diskon
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <ul class="nav nav-tabs nav-tabs-line" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#active" role="tab">Active</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#inactive" role="tab">Non Active</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="active" role="tabpanel">
                <div class="form-group">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="table-active">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Kode</th>
                                <th>Type</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="inactive" role="tabpanel">
                <div class="form-group">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="table-inactive">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Kode</th>
                                <th>Type</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#table-active').DataTable({
            "pagingType": "full_numbers",
            scrollX: true,
            scrollCollapse: true,
            "processing": true,
            "serverSide": true,
            "ajax": "<?= site_url() ?>/<?= $url ?>/get_data_active"
        });

        $('#table-inactive').DataTable({
            "pagingType": "full_numbers",
            scrollCollapse: true,
            "processing": true,
            "serverSide": true,
            "ajax": "<?= site_url() ?>/<?= $url ?>/get_data_inactive"
        });
    });
</script>