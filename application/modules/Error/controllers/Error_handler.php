<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Error_handler extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view("error404");
	}

	function view($kode)
	{
		if ($kode=="404") {
			$data['kode'] = $kode;
			$data['msg'] = "Page Not Found";
			$this->load->view("error_handler", $data);
		} else {
			$data['kode'] = $kode;
			$data['msg'] = "Page Not Found";
			$this->load->view("error_handler", $data);
		}
	}
}