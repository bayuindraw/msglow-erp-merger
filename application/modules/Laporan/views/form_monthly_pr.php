<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN PR
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/print_monthly_pr'); ?>" method="Post" target="_blank">
          <div class="form-group">
            <label>Bulan</label>
            <select name="cBulan" id="pilihBulan" ng-model="cBulan" class="form-control">
              <option></option>
              <option value="01">Januari</option>
              <option value="02">Februari</option>
              <option value="03">Maret</option>
              <option value="04">April</option>
              <option value="05">Mei</option>
              <option value="06">Juni</option>
              <option value="07">Juli</option>
              <option value="08">Agustus</option>
              <option value="09">September</option>
              <option value="10">Oktober</option>
              <option value="11">November</option>
              <option value="12">Desember</option>
            </select>
          </div>
          <div class="form-group">
            <label>Tahun</label>
            <select name="cTahun" id="pilihTahun" class="form-control">
              <option></option>
              <option value="2019">2019</option>
              <option value="2020">2020</option>
              <option value="2021">2021</option>
            </select>
          </div>
          <div class="form-group">
            <label>Admin</label>
            <select name="cSales" id="admin" class="form-control" onchange="get_admin_member()">
              <option></option>
              <?php
                $query = $this->Model->Code("SELECT user_fullname , user_id , user_name FROM m_user WHERE role_id = '7'");
                foreach ($query as $key => $vaData) {
              ?>
              <option value="<?=$vaData['user_id']?>"><?=$vaData['user_fullname']?></option>
            <?php } ?>
            </select>
          </div>
          <div class="form-group">
             <label>Seller</label>
            <select name="cSeller" id="member" class="form-control" >
              <option></option>
              
            </select>
          </div>
         <!--  <div class="form-group">
            <label>Produk</label>
            <select class="form-control pilihProduk" name="product_id[]" id="products" multiple="multiple">
              <option></option><?= $product_list ?>
            </select>
          </div> -->
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
            <div class="row">
            
                <div class="col-6 col-md-6 col-lg-6 text-left">
                  <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                    <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span>
                  </button>
                </div>
              </div>
            <br />
            <br />
              <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                  <div id="preview_datatable"></div>
                </div>
              </div>
          </div>
        </form>
      </div>

    </div>
  </div>


</div>

<script>
  $('.pilihProduk').select2({
    placeholder: "Pilih Produk",
  });
  $('#admin').select2({
    placeholder: "Pilih Admin",
  });
  $('#member').select2({
    placeholder: "Pilih Seller",
  });
function get_admin_member() {
	
	var val = $('#admin option:selected').val();

	$.ajax({
		url: '<?= base_url() ?>Laporan/get_admin_member/' + val,
		success: function(result) {
			$('#member').html(result);
			$('#member').select2({
				placeholder: "Pilih Seller",
			});
		}
	});
}
</script>