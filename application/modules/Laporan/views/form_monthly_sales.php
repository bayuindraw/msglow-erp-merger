<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN PENJUALAN BULANAN
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/print_monthly_sales'); ?>" method="Post" target="_blank">
          <div class="form-group">
            <label>Bulan</label>
            <select name="cBulan" id="pilihBulan" ng-model="cBulan" class="form-control" required>
              <option></option>
              <option value="01">Januari</option>
              <option value="02">Februari</option>
              <option value="03">Maret</option>
              <option value="04">April</option>
              <option value="05">Mei</option>
              <option value="06">Juni</option>
              <option value="07">Juli</option>
              <option value="08">Agustus</option>
              <option value="09">September</option>
              <option value="10">Oktober</option>
              <option value="11">November</option>
              <option value="12">Desember</option>
            </select>
          </div>
          <div class="form-group">
            <label>Tahun</label>
            <select name="cTahun" id="pilihTahun" class="form-control" required>
              <option></option>
              <option value="2019">2019</option>
              <option value="2020">2020</option>
              <option value="2021">2021</option>
            </select>
          </div>
          <div class="form-group">
            <label>Tipe Seller</label>
            <select name="cTipe" id="cTipe" class="form-control pilihAkun" required>
              <option></option>
              <option value="RESELLER">RESELLER</option>
              <option value="MEMBER">MEMBER</option>
              <option value="AGEN">AGEN</option>
              <option value="DISTRIBUTOR">DISTRIBUTOR</option>
            </select>
          </div>
          <div class="form-group">
            <label>Brand</label>
            <select name="cBrand" id="cBrand" class="form-control pilihAkun" required>
              <option></option>
              <option value="1">MSGLOW BEAUTY</option>
              <option value="2">MSGLOW FOR MEN</option>
              <option value="3">MSSLIM</option>
              
            </select>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
            <div class="row">
                <div class="col-6 col-md-6 col-lg-6 text-left">
                  <button type="button" onclick="get_preview()" class="btn btn-warning waves-effect waves-light " title="Cetak Laporan">
                    <span class="m-l-10">Preview Laporan</span>
                  </button>
                </div>
                <div class="col-6 col-md-6 col-lg-6 text-right">
                  <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                    <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span>
                  </button>
                </div>
              </div>
<br />
<br />
              <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                  <div id="preview_datatable"></div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>


</div>

<script>

  function get_preview() {
    // var cTanggal = $('#cTanggal').val();
    var bulan = $('#pilihBulan').val();
    if (bulan == "") bulan = ''+<?= date('m') ?>+'';
    if(bulan.length == 1) bulan = '0'+ bulan;

    var tahun = $('#pilihTahun').val();
    if(tahun == "") tahun = ''+<?= date('Y') ?>+'';

    var tipe = $('#cTipe').val();
    if(tipe == "") {
      alert('Pilih Tipe Terlebih Dahulu!');
      return false;
    }

    var brand = $('#cBrand').val();

    $.ajax({
      type: "POST",
      data: "cBulan="+ bulan +
        "&cTahun=" + tahun +
        "&cTipe="+ tipe+
        "&cBrand="+ brand,
      url: "<?= site_url($url . '/preview_monthly_sales') ?>",
      cache: false,
      beforeSend: function() {
        $('#preview_datatable').html("Cek Data Ke Sistem .. ");
      },
      success: function(msg) {
        $("#preview_datatable").html(msg);
      }
    });
    // }
  }

</script>