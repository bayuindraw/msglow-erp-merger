<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN PENJUALAN PER SALES
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body"> 

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/print_monthlu_pic_sales'); ?>" method="Post" target="_blank">
          <div class="form-group">
            <label>Bulan</label>
            <select name="cBulan" id="pilihBulan" ng-model="cBulan" class="form-control">
        <option></option>
        <option value="01">Januari</option>
        <option value="02">Februari</option>
        <option value="03">Maret</option>
        <option value="04">April</option>
        <option value="05">Mei</option>
        <option value="06">Juni</option>
        <option value="07">Juli</option>
        <option value="08">Agustus</option>
        <option value="09">September</option>
        <option value="10">Oktober</option>
        <option value="11">November</option>
        <option value="12">Desember</option>
      </select> 
          </div>
      <div class="form-group">
            <label>Tahun</label>
            <select name="cTahun" id="pilihTahun" class="form-control">
        <option></option>
        <option value="2019">2019</option>
        <option value="2020">2020</option>
        <option value="2021">2021</option>
      </select> 
          </div>
      <div class="form-group">
            <label>Sales / PIC</label>
            <select name="cPic" id="pilihPic" class="form-control">
        <option></option>
        <option value="24">ERNA - BEAUTY</option>
        <option value="26">ZURA - BEAUTY</option>
        <option value="25">DEVY - BEAUTY</option>
        <option value="23">ALFI -  BEAUTY</option>
        <option value="22">SARIFA - BEAUTY</option>
        <option value="32">RATNA - MEN</option>
        <option value="31">ARI - MEN</option>
      </select> 
          </div>
      <div class="form-group">
            <label>Seller</label>
      <select name="seller_id" id="PilihDistributor" class="form-control md-static" required><option></option></select>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>


</div>