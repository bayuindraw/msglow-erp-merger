<?php
date_default_timezone_set('Asia/Jakarta');
$tgl_sekarang = date("Y-F-d");
$bulan_cetak = date("F");
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data_barang_keluar_" . strtoupper($type) . "_" . $tgl_sekarang . ".xls");
if ($cek_data->num_rows() > 0) {
?>
    <table>
        <thead>
            <tr>
                <td rowspan="2" colspan="7" style="background-color:#8CD3FF;"> LAPORAN <?= strtoupper($type) ?> BARANG KELUAR PT. KOSMETIKA CANTIK INDONESIA </td>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <br />
    <?php
    $x = 1;
    foreach ($arrdata as $index => $value) {
    ?>
        <table border="1">
            <thead>
                <tr>
                    <td colspan="2" style="background-color:#FFE4C4"> <?= $arrproduk[$index] ?></td>
                </tr>
                <tr>
                    <td style="background-color:#BAB86C"> Nama Seller </td>
                    <td style="background-color:#BAB86C"> Quantity</td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($value as $index2 => $value2) {
                ?>
                    <tr>
                        <td> <?= $value2['nama'] ?> </td>
                        <td> <?= $value2['package_detail_quantity'] ?> </td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <br />
        <br />
    <?php
    }
} else {
    ?>
    <table>
        <thead>
            <?php
            if ($type == "draft") {
            ?>
                <tr>
                    <td rowspan="2" colspan="7" style="background-color:#8CD3FF;"> LAPORAN <?= strtoupper($type) ?> BARANG KELUAR PT. KOSMETIKA CANTIK INDONESIA </td>
                </tr>
            <?php
            } else {
            ?>
                <tr>
                    <td colspan="7" style="background-color:#8CD3FF;"> LAPORAN <?= strtoupper($type) ?> BARANG KELUAR PT. KOSMETIKA CANTIK INDONESIA </td>
                </tr>
                <tr>
                    <!-- <td colspan="7" style="background-color:#8CD3FF;"> <?= $tgl_sekarang ?> </td> -->
                </tr>
            <?php
            }
            ?>

        </thead>
        <tbody>
        </tbody>
    </table>
    <br />
    <table border="1">
        <thead>
            <tr>
                <td colspan="2" style="background-color:#FFE4C4"> DATA NOT FOUND</td>
            </tr>
            <tr>
                <td style="background-color:#BAB86C"> Nama Seller </td>
                <td style="background-color:#BAB86C"> Quantity</td>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
<?php
}
?>