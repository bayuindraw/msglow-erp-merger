<?php
date_default_timezone_set('Asia/Jakarta');
$tgl_sekarang = date("Y-F-d");
$bulan_cetak = date("F");

// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=report_realisasi_total_pembagian" . $tgl_sekarang . ".xls");
?>
<table>
    <thead>
        <tr>
            <td rowspan="2" colspan="4" style="background-color:#8CD3FF; text-align: center;font-weight: bold;"> LAPORAN REALISASI TOTAL PEMBAGIAN </td>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<br />


<?php if (count($pembagian) > 0) { ?>
    <table border="1">
        <thead>
            <tr>
                <th style="background-color:#BAB86C"> NO </th>
                <th style="background-color:#BAB86C"> TANGGAL </th>
                <th style="background-color:#BAB86C"> PRODUK </th>
                <th style="background-color:#BAB86C"> JUMLAH </th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $total = 0;
            foreach ($pembagian as $key) { ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $key['package_date']; ?></td>
                    <td><?= $key['nama_produk']; ?></td>
                    <td><?= $key['jum_tot']; ?></td>
                </tr>
            <?php
                $total += $key['jum_tot'];
            } ?>
            <tr>
                <td style="background-color:#BAB86C;font-weight: bold;" colspan="3">TOTAL</td>
                <td style="background-color:#BAB86C;font-weight: bold;"><?= $total; ?></td>
            </tr>
        </tbody>
    </table>
<?php } else { ?>
    <table border="1">
        <thead>
            <tr>
                <th style="background-color:#BAB86C"> NO </th>
                <th style="background-color:#BAB86C"> TANGGAL </th>
                <th style="background-color:#BAB86C"> PRODUK </th>
                <th style="background-color:#BAB86C"> JUMLAH </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="4" style="background-color:#FFE4C4"> DATA NOT FOUND</td>
            </tr>
        </tbody>
    </table>
<?php } ?>