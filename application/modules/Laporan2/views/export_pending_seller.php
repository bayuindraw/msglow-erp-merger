<?php
date_default_timezone_set('Asia/Jakarta');
$tgl_sekarang = date("Y-F-d");
$bulan_cetak = date("F");
$nama_seller = "";
foreach ($header_seller as $vaNamaSeller) {
    $nama_seller = $vaNamaSeller['nama'];
}
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data_pendingan_" . $nama_seller . ".xls");

// Tambahkan table
?>

<!--begin: Datatable -->
<table border="1" style="width: 100%; border:black;">
    <thead>
        <tr>
            <td colspan="8" style="background-color:#8CD3FF;">REKAP PENDINGAN SELLER PT. KOSMETIKA CANTIK INDONESIA</td>
        </tr>
        <tr>
            <td colspan="8" style="background-color:#8CD3FF"><?= strtoupper($nama_seller) ?></td>
        </tr>
        <tr>
            <td>No</td>
            <td>Kode Seller</td>
            <td>Nama Seller</td>
            <td>Nama Produk</td>
            <td>Jumlah Pesanan</td>
            <td>Pendingan</td>
            <td>Realisasi/Kirim</td>
            <td>Sisa Pendingan</td>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0;
        foreach ($data_detail_product as $vaData) {
        ?>
            <tr>
                <td><?= ++$no ?></td>
                <td> <?= $vaData['kode_tempat'] ?> </td>
                <td> <?= $vaData['nama'] ?> </td>
                <td> <?= $vaData['nama_produk'] ?> </td>
                <td> <?= str_replace(',','.',$vaData['Jumlah_Pesanan'])  ?> </td>
                <td> <?= $vaData['Boleh_Dikirim']?> </td>
                <td> <?= $vaData['Terkirim']?> </td>
                <td>
                    <?php
                    $total_pending = $vaData['Boleh_Dikirim'] - $vaData['Terkirim'];
                    echo $total_pending;
                    ?>
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>
<!--end: Datatable -->