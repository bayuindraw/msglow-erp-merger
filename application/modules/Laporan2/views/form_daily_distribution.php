<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            <?= $title; ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/excel_daily_distribution'); ?>" method="Post" target="_blank">
		  <div class="row form-group">
            <div class="col-sm-6 form-group">
                  <label>Tanggal Awal</label>
                  <!--<input type="text" id="tgl_awal" name="input[package_date]" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d', strtotime(date('Y-m-d'). ' - 1 days')) ?>" required>--> 
                  <input type="text" name="tgl_awal" id="tgl_awal" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required> 
                </div>
            <div class="col-sm-6 form-group"> 
                  <label>Tanggal Akhir</label>
                  <input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required> 
                </div>
          </div>
		  <div class="col-lg-6 col-xl-6">
			<div class="form-group">
            <label>Seller</label>
            <select name="seller_id" id="PilihDistributor" class="form-control md-static" required><option></option></select> 
          </div>
		  </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>


</div>