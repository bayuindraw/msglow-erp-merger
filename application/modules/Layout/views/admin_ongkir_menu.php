<?php
$verifikasi_pembayaran_seller_1 = $this->db->query("select * from v_count_verifikasi_pembayaran_seller_1_open")->num_rows();

$verifikasi_pembayaran_seller_2 = $this->db->query("select * from v_count_verifikasi_pembayaran_seller_2_open")->num_rows();

$verifikasi_revisi_pembayaran_open = $this->db->query("select * from v_count_verifikasi_revisi_pembayaran_open")->num_rows();

$count_verifikasi = $verifikasi_pembayaran_seller_1 + $verifikasi_pembayaran_seller_2 + $verifikasi_revisi_pembayaran_open;
?>

<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_postage" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-shipping-fast"></i><span class="kt-menu__link-text">Ongkir</span></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-check-square"></i><span class="kt-menu__link-text">Verifikasi</span><span class="badge badge-warning badge-pill" style="padding-left: 1em;padding-right: 1em; padding-top: 0.7em;"><?php if ($count_verifikasi > 0) echo $count_verifikasi; ?></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/verification_transfer_seller") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Pembayaran Seller 1 <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_pembayaran_seller_1 > 0) echo $verifikasi_pembayaran_seller_1; ?></span></span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/verification_transfer_seller2") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Pembayaran Seller 2 <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_pembayaran_seller_2 > 0) echo $verifikasi_pembayaran_seller_2; ?></span></span></a></li>
            <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/get_finish_rev") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Revisi Pembayaran <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_revisi_pembayaran_open > 0) echo $verifikasi_revisi_pembayaran_open; ?></span></span></a></li> -->
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Tb_Stock_Produk_Detail") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Barang Masuk</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("T_postage") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Ongkir</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Laporan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("T_coa_transaction") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Jurnal Umum</span></a></li>
        </ul>
    </div>
</li>