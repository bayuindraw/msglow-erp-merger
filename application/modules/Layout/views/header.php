<?php
$xx = @$this->uri->segment(3, 0);
if($xx == "stock_opname_produk" && $xx != 0){ 
}else if (@!$_SESSION['logged_in'] && $allow != 1) {
	
	redirect(site_url() . "Login");
}
?>
<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->

<head>
	<base href="../../">
	<meta charset="utf-8" />
	<title>MSGLOW OFFICE</title>
	<meta name="description" content="Page with empty content">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--begin::Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

	<!--end::Fonts -->

	<!--begin::Page Vendors Styles(used by this page) -->
	<link href="<?= base_url() ?>web/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<!--end::Page Vendors Styles -->

	<!--begin::Global Theme Styles(used by all pages) -->
	<link href="<?= base_url() ?>web/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/css/style.bundle.css" rel="stylesheet" type="text/css" />

	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->
	<link href="<?= base_url() ?>web/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/css/skins/brand/dark.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/css/skins/aside/dark.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/plugins/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/plugins/select2/css/s2-docs.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>web/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Layout Skins -->
	<link rel="shortcut icon" href="https://msglow.app/upload/nitrogen.png" />
</head>
<?php if ($_SESSION['logged_in']) { ?>
	<script src="<?= base_url() ?>web/plugins/jquery.js" type="text/javascript"></script>
	<script src="<?= base_url() ?>web/js/knockout/knockout-min.js" type="text/javascript"></script>
	<!--<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" type="text/javascript"></script>-->
	<script src="<?= base_url() ?>web/plugins/custom/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
	<script src="<?= base_url() ?>web/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>

<?php } ?>

<script src="<?= base_url() ?>web/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

<script src="<?= base_url() ?>web/plugins/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?= base_url() ?>web/plugins/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>web/plugins/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script> -->
<link rel="stylesheet" href="<?= base_url('web/css/lightbox.css'); ?>">

<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

	<!-- begin:: Page -->

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="#">
				<img alt="Logo" src="https://msglow.app/upload/logo_nitromerah.png" style="max-height: 20px;" />
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
		</div>
	</div>

	<!-- end:: Header Mobile -->
	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

			<!-- begin:: Aside -->

			<!-- Uncomment this to display the close button of the panel
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
-->
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

	<!-- begin:: Aside -->
	<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand" style="background-color: rgb(0 76 153)">
		<div class="kt-aside__brand-logo">
			<a href="<?= site_url() ?>">
				<!-- <img alt="Logo" src="<?= base_url('assets/logo-01.gif'); ?>" style="width: 192px;" /> -->
				<img src="https://msglow.app/upload/logo_nitromerah.png" style="width: 192px;" alt="Logo">
			</a>
		</div>
		<div class="kt-aside__brand-tools">
		</div>
	</div>

	<!-- end:: Aside -->
	<?php if(@$this->uri->segment(3, 0) != 'stock_opname_produk' || $_SESSION['logged_in']) { ?>
		<!-- begin:: Aside Menu -->

		<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
			<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
				<ul class="kt-menu__nav ">

					<?php $this->load->view('menu'); ?>
					
				</ul>
			</div>
		</div>
	<?php } ?>
	<!-- end:: Aside Menu -->
</div>

<!-- end:: Aside -->
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

	<!-- begin:: Header -->
	<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " style="background-color: #399bff;">


		<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
			<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">

			</div>
		</div>

		<!-- end:: Header Menu -->

		<!-- begin:: Header Topbar -->
		<?php $this->load->view('header_topbar'); ?>

		<!-- end:: Header Topbar -->
	</div>
	<script src="<?= base_url() ?>web/plugins/general/raphael/raphael.js" type="text/javascript"></script>
	<script src="<?= base_url() ?>web/plugins/general/morris.js/morris.js" type="text/javascript"></script>
	<script src="<?= base_url() ?>web/plugins/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
	<script src="<?= base_url() ?>web/js/scripts.bundle.js" type="text/javascript"></script>
	<script src="<?= base_url() ?>web/js/pages/dashboard.js" type="text/javascript"></script>
