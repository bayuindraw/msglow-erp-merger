<?php
// Verifikasi
$verifikasi_pembayaran_seller_1 = $this->db->query("select * from v_count_verifikasi_pembayaran_seller_1_open")->num_rows();

$verifikasi_pembayaran_seller_2 = $this->db->query("select * from v_count_verifikasi_pembayaran_seller_2_open")->num_rows();

$verifikasi_revisi_pembayaran_open = $this->db->query("select * from v_count_verifikasi_revisi_pembayaran_open")->num_rows();

$count_verifikasi = $verifikasi_pembayaran_seller_1 + $verifikasi_pembayaran_seller_2 + $verifikasi_revisi_pembayaran_open;
//Pembayaran
$perubahan_verifikasi_pembayaran_not_on_process = '';
if ($_SESSION['role_id'] == '12') {
    $perubahan_verifikasi_pembayaran_not_on_process = $this->db->query("select * from v_count_perubahan_verifikasi_pembayaran_not_on_process where sales_category_id = '" . $_SESSION['sales_category_id'] . "'")->num_rows();
}
if ($_SESSION['role_id'] == '7') {
    $perubahan_verifikasi_pembayaran_not_on_process = $this->db->query("select * from v_count_perubahan_verifikasi_pembayaran_not_on_process where user_pic_id = '" . $_SESSION['user_id'] . "'")->num_rows();
}
?>

<div class="kt-header__topbar">

    <!--begin: User Bar -->
    <div class="kt-header__topbar-item kt-header__topbar-item--user">
        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
            <div class="kt-header__topbar-user">
                <!-- <i class="la la-user pr-2" style="color: #fff;font-size: 14pt;"></i> -->
                <span class="kt-header__topbar-welcome kt-hidden-mobile" style="color:white">Hi, </span>
                <span class="kt-header__topbar-username kt-hidden-mobile" style="color:white"><?= @$_SESSION['user_fullname']; ?></span>
                <span class="kt-header__topbar-username kt-hidden-mobile text-white">|</span>
                <span class="kt-header__topbar-username kt-hidden-mobile text-white">Sign Out</span>
                <span class="kt-header__topbar-username kt-hidden-mobile text-white">
                    <?php
                    if ($count_verifikasi > 0 && ($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 15)) { ?>
                        <span class="badge badge-warning badge-pill">
                            <i class="fas fa-bell"></i>
                        </span>
                    <?php }
                    if ($perubahan_verifikasi_pembayaran_not_on_process > 0 && $_SESSION['role_id'] == 12) { ?>
                        <span class="badge badge-warning badge-pill">
                            <i class="fas fa-bell"></i>
                        </span>
                    <?php } ?>
                </span>
            </div>
        </div>
        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

            <!--begin: Head -->
            <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(<?= base_url() ?>web/media/misc/bg-1.jpg)">
                <div class="kt-user-card__avatar">
                    <img class="kt-hidden" alt="Pic" src="<?= base_url() ?>web/media/users/300_25.jpg" />

                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                    <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold text-uppercase text-white" style="font-size:17px"><?= substr($_SESSION['user_fullname'], 0, 3); ?></span>
                </div>
                <div class="kt-user-card__name">
                    <?= $_SESSION['role_name']; ?> - <?= $_SESSION['user_fullname']; ?>
                </div>
                <div class="kt-user-card__badge">
                    <span class="btn btn-success btn-sm btn-bold btn-font-md">
                        <?php
                        if ($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 15) {
                            echo $count_verifikasi;
                        } else if ($_SESSION['role_id'] == 12) {
                            echo $perubahan_verifikasi_pembayaran_not_on_process;
                        } else {
                            echo 0;
                        }
                        ?> Notifikasi</span>
                </div>
            </div>

            <!--end: Head -->

            <!--begin: Navigation -->
            <div class="kt-notification">
                <?php if ($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 15) { ?>
                    <!-- verifikasi_pembayaran_seller_1 -->
                    <?php if ($verifikasi_pembayaran_seller_1 > 0) { ?>
                        <a href="<?= base_url('M_account2/verification_transfer_seller'); ?>" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <span class="badge badge-secondary"><?= $verifikasi_pembayaran_seller_1; ?></span>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Verifikasi
                                </div>
                                <div class="kt-notification__item-time">
                                    Pembayaran Seller 1
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                    <!-- end verifikasi_pembayaran_seller_1 -->
                    <!-- verifikasi_pembayaran_seller_2 -->
                    <?php if ($verifikasi_pembayaran_seller_2 > 0) { ?>
                        <a href="<?= base_url('M_account2/verification_transfer_seller2'); ?>" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <span class="badge badge-secondary"><?= $verifikasi_pembayaran_seller_2; ?></span>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Verifikasi
                                </div>
                                <div class="kt-notification__item-time">
                                    Pembayaran Seller 2
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                    <!-- end verifikasi_pembayaran_seller_2 -->
                    <!-- verifikasi_revisi_pembayaran_open -->
                    <?php if ($verifikasi_revisi_pembayaran_open > 0) { ?>
                        <a href="<?= base_url('M_account2/get_finish_rev'); ?>" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <span class="badge badge-secondary"><?= $verifikasi_revisi_pembayaran_open; ?></span>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Verifikasi
                                </div>
                                <div class="kt-notification__item-time">
                                    Revisi Pembayaran
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                    <!-- end verifikasi_revisi_pembayaran_open -->
                <?php } ?>

                <?php if ($_SESSION['role_id'] == 12) { ?>
                    <!-- verifikasi_revisi_pembayaran_open -->
                    <?php if ($perubahan_verifikasi_pembayaran_not_on_process > 0) { ?>
                        <a href="<?= base_url('M_account/edit_verif_pembayaran'); ?>" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <span class="badge badge-secondary"><?= $perubahan_verifikasi_pembayaran_not_on_process; ?></span>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Pembayaran
                                </div>
                                <div class="kt-notification__item-time">
                                    Perubahan Verifikasi Pembayaran
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                    <!-- end verifikasi_revisi_pembayaran_open -->
                <?php } ?>

                <?php if ($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 15) {
                    if ($count_verifikasi < 1) { ?>
                        <div class="text-center pt-5 pb-5">
                            <h5>No notification</h5>
                        </div>
                    <?php }
                } else if ($_SESSION['role_id'] == 12) {
                    if ($perubahan_verifikasi_pembayaran_not_on_process < 1) { ?>
                        <div class="text-center pt-5 pb-5">
                            <h5>No notification</h5>
                        </div>
                    <?php }
                } else { ?>
                    <div class="text-center pt-5 pb-5">
                        <h5>No notification</h5>
                    </div>
                <?php } ?>

                <div class="kt-notification__custom kt-space-between float-right">
                    <a href="<?= base_url(); ?>Login/logout" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>

                </div>
            </div>

            <!--end: Navigation -->
        </div>
    </div>

    <!--end: User Bar -->
</div>