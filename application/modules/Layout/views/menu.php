<?php
$verifikasi_pembayaran_seller_1 = $this->db->query("SELECT COUNT(A.account_detail_real_id) as jumlah FROM t_account_detail_real A JOIN m_account B ON B.account_id = A.account_id JOIN member C ON C.kode = B.seller_id LEFT JOIN m_sales_category D ON D.sales_category_id = C.sales_category_id WHERE A.account_detail_real_status = 1")->row_array()['jumlah']; //$this->db->query("select * from v_count_verifikasi_pembayaran_seller_1_open")->num_rows();

$verifikasi_pembayaran_seller_2 = $this->db->query("SELECT COUNT(A.account_detail_real_id) as jumlah FROM t_account_detail_real A JOIN m_account B ON B.account_id = A.account_id JOIN member C ON C.kode = B.seller_id LEFT JOIN m_sales_category D ON D.sales_category_id = C.sales_category_id WHERE A.account_detail_real_status = 3")->row_array()['jumlah']; //$this->db->query("select * from v_count_verifikasi_pembayaran_seller_2_open")->num_rows();

$verifikasi_revisi_pembayaran_open = $this->db->query("select * from v_count_verifikasi_revisi_pembayaran_open")->num_rows();

// $count_verifikasi = $verifikasi_pembayaran_seller_1 + $verifikasi_pembayaran_seller_2 + $verifikasi_revisi_pembayaran_open;
$count_verifikasi = $verifikasi_pembayaran_seller_1 + $verifikasi_pembayaran_seller_2;
?>

<?php if ($_SESSION['role_id'] == "4") { ?>
    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Master/index_pic" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-tachometer-alt"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
<?php } ?>

<?php if ($_SESSION['role_id'] == "5"  || $_SESSION['role_id'] == "10"|| $_SESSION['role_id'] == "23") { ?>
    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-tachometer-alt"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
<?php } ?>

<?php if ($_SESSION['role_id'] == "7" || $_SESSION['role_id'] == "17" || $_SESSION['role_id'] == "12") { ?>
    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-tachometer-alt"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "11") { ?>
    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Dashboard" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-tachometer-alt"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
<?php } ?>

<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text"><?= $_SESSION['role_name'] ?></h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "98") { ?>
    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_salesd" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-home"></i><span class="kt-menu__link-text">INPUT PENDINGAN</span></a></li>
<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "0" || $_SESSION['role_id'] == "4" || $_SESSION['role_id'] == "10" || $_SESSION['role_id'] == "5" || $_SESSION['role_id'] == "24" || $_SESSION['role_id'] == "7" || $_SESSION['role_id'] == "17" || $_SESSION['role_id'] == "12") { ?>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon fas fa-archive"></i><span class="kt-menu__link-text">Master</span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">

                <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "0") { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_action" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Action</span></a></li>

                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_icon" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Icon</span></a></li>

                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_menu" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Menu</span></a></li>

                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_department" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Department</span></a></li>

                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_user_role" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master User Role</span></a></li>

                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master User</span></a></li>
                <?php } ?>

                <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "10") { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_user/table_inventory" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">User</span></a></li>
                <?php } ?>

                <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "12") { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_user/table_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">User Sales</span></a></li>
                <?php } ?>

                <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "12" || $_SESSION['role_id'] == "17" || $_SESSION['role_id'] == "7" || $_SESSION['role_id'] == "0") { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Member') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Member</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_member_status" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Status Seller</span></a></li>
                <?php } ?>

                <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "24" || $_SESSION['role_id'] == "10" || $_SESSION['role_id'] == "5") { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_login_pwa" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">User PWA</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_line" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Line</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Factory" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pabrik</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Produk</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk_bundle" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Produk Bundling</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk_global" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_product_global_group" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU Group</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_brand" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Brand</span></a></li>
                <?php } ?>

                <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "0") { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Produk</span></a></li>

                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_account" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Akun</span></a></li>

                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_employee" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Pegawai</span></a></li>

                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Seller</span></a></li>

                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_member_status" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Status Seller</span></a></li>
                <?php } ?>

                <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "0" || $_SESSION['role_id'] == "4") { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Kemasan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Kemasan</span></a></li>

                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Supplier" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Supplier</span></a></li>
                <?php } ?>
            </ul>
        </div>
    </li>

    <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "0") { ?>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;">
            <a href="<?= base_url() ?>Terima_produk_accounting" class="kt-menu__link kt-menu__toggle">
                <i class="kt-menu__link-icon fas fa-hands"></i>
                <span class="kt-menu__link-text">Terima Produk</span>
            </a>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;">
            <a href="<?= base_url('M_account2/fifoProduct') ?>" class="kt-menu__link kt-menu__toggle">
                <i class="kt-menu__link-icon fas fa-calendar-times"></i><span class="kt-menu__link-text">Incomplete Sales Data</span>
            </a>
        </li>
    <?php } ?>
<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "10" || $_SESSION['role_id'] == "11" || $_SESSION['role_id'] == "6") { ?>
    <?php
    $verifikasi_pembayaran_seller_1 = $this->db->query("select * from v_count_verifikasi_pembayaran_seller_1_open")->num_rows();

    $verifikasi_pembayaran_seller_2 = $this->db->query("select * from v_count_verifikasi_pembayaran_seller_2_open")->num_rows();

    $verifikasi_revisi_pembayaran_open = $this->db->query("select * from v_count_verifikasi_revisi_pembayaran_open")->num_rows();

    $count_verifikasi = $verifikasi_pembayaran_seller_1 + $verifikasi_pembayaran_seller_2 + $verifikasi_revisi_pembayaran_open;
    ?>

    <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "11" || $_SESSION['role_id'] == "6") { ?>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                <i class="kt-menu__link-icon fas fa-chart-area"></i><span class="kt-menu__link-text">COA</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">COA 1</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA/coa2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">COA 2</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA/coa3" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">COA 3</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA/coa4" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">COA 4</span></a></li>
                    <?php if ($_SESSION['role_id'] == "6") { ?>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_coa_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Kas Pengeluaran</span></a></li>
                    <?php } ?>
                </ul>
            </div>
        </li>
    <?php } ?>

    <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "11") { ?>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
            <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-piggy-bank"></i><span class="kt-menu__link-text">Kas</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_coa_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Kas Pengeluaran</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_coa_user/table_realization" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Realisasi</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Transfer_account" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Transfer Antar Akun</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_coa_user/table_history" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Riwayat Pengeluaran Kas</span></a></li>
                </ul>
            </div>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
            <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-briefcase"></i><span class="kt-menu__link-text">Aset</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Aset_tetap/form" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Input Aset Tetap</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Aset_tetap" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History Aset Tetap</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Aset_lainnya/form" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Input Aset Lainnya</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Aset_lainnya" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History Aset Lainnya</span></a></li>
                </ul>
            </div>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
            <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-dollar-sign"></i><span class="kt-menu__link-text">Biaya Dibayar Dimuka</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Dibayar_dimuka/form" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Input Beban Dibayar Dimuka</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Dibayar_dimuka" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History Beban Dibayar Dimuka</span></a></li>
                </ul>
            </div>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
            <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-chart-line"></i><span class="kt-menu__link-text">Laporan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("T_coa_transaction") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Jurnal Umum</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/laporan_total_akun") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Total Akun</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan2/laporan_kas") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Kas</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan2/laporan_fifo") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan FIFO</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan2/laporan_laba_rugi") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Laba Rugi</span></a></li>
                </ul>
            </div>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
            <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-check-square"></i><span class="kt-menu__link-text">Verifikasi</span><span class="badge badge-warning badge-pill" style="padding-left: 1em;padding-right: 1em; padding-top: 0.7em;"><?php if ($count_verifikasi > 0) echo $count_verifikasi; ?></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/verification_transfer_seller") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Pembayaran Seller 1 <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_pembayaran_seller_1 > 0) echo $verifikasi_pembayaran_seller_1; ?></span></span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/verification_transfer_seller2") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Pembayaran Seller 2 <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_pembayaran_seller_2 > 0) echo $verifikasi_pembayaran_seller_2; ?></span></span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Tb_Stock_Produk_Detail") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Barang Masuk</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("T_postage") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Ongkir</span></a></li>
                </ul>
            </div>
        </li>
    <?php } ?>

    <li class="kt-menu__section ">
        <h4 class="kt-menu__section-text">Sales</h4>
        <i class="kt-menu__section-icon flaticon-more-v2"></i>
    </li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Sales</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Produk</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Bulanan</span></a></li>
                            <?php if ($_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "11" || $_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "6") { ?>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Sales</span></a>
                                </li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_payment') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembayaran Produk</span></a>
                                </li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Bulanan</span></a>
                                </li>
                            <?php } ?>
                            <?php if ($_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "10") { ?>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Seller</span></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Seller</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_seller') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller Bulanan</span></a></li>
                            <?php if ($_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "11") { ?>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_paid" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Mutasi Perorangan</span></a></li>
                            <?php } ?>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_user') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Bulanan</span></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </li>

    <li class="kt-menu__section ">
        <h4 class="kt-menu__section-text">Inventory</h4>
        <i class="kt-menu__section-icon flaticon-more-v2"></i>
    </li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">Inventory</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Bulanan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/division_daily" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_factory_in" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Terima Barang Bulanan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
                <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li> -->
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_product_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Harian</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Bulanan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Pendingan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan_per_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Pendingan Per Seller</span></a></li>
                <?php if ($_SESSION['role_id'] == "16") { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Chart</span></a></li>
                <?php } ?>
            </ul>
        </div>
    </li>

<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "3" || $_SESSION['role_id'] == 0) { ?>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">PO Barang Jadi</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_produk_pic" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Barang Jadi</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_history" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History PO Barang Jadi</span></a></li>
            </ul>
        </div>
    </li>
<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "4" || $_SESSION['role_id'] == 0 || $_SESSION['role_id'] == 3) { ?>
    <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "4" || $_SESSION['role_id'] == 0) { ?>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/stock_opname_kemasan" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-box-open"></i><span class="kt-menu__link-text">Stock Kemasan</span></a></li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"><a href="<?= base_url() ?>M_product_classification" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-box-open"></i><span class="kt-menu__link-text">Klasifikasi Produk</span></a></li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/kartu_stock_kemasan" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-id-card"></i><span class="kt-menu__link-text">Kartu Stock</span></a></li>
    <?php } ?>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">PO Kemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">

                <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "4" || $_SESSION['role_id'] == 0) { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Input PO Kemasan</span></a></li>
                <?php } ?>

                <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "3" || $_SESSION['role_id'] == 0) { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_pic" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Kemasan</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_print_pic" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Printing</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_history" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History PO Printing</span></a></li>
                <?php } ?>

                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_history" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">History PO Kemasan</span></a></li>
            </ul>
        </div>
    </li>

    <?php if ($_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "4" || $_SESSION['role_id'] == 0) { ?>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">List PO Kemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_active_pic" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Kemasan Active</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_active_pic_tidak" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Kemasan Selesai</span></a></li>
                </ul>
            </div>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">PO Print</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_print" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Input PO Print</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_history_print" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History PO Print</span></a></li>
                </ul>
            </div>
        </li>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">List PO Print</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_print_active_pic" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Print Active</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_active_pic_tidak" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Print Selesai</span></a></li>
                </ul>
            </div>
        </li>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">Pengeluaran Kemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/pengeluaran_kemasan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Input Pengeluaran Kemasan</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/data_pengeluaran_kemasan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Pengeluaran Kemasan</span></a></li>
                </ul>
            </div>
        </li>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book-open"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/laporan_po_kemasan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan PO</span></a></li>
                </ul>
            </div>
        </li>
    <?php } ?>
<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "24" || $_SESSION['role_id'] == "10" || $_SESSION['role_id'] == "5") { ?>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-undo"></i><span class="kt-menu__link-text">Line Quality Control</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Adjustment</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/form_masuk_opname" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Masuk</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/form_keluar_opname" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Keluar</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Tidak Sesuai SJ</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_retur" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pengembalian Retur</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_factory" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Retur Ke Pabrik</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_repair" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Repair</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_sample" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Sample</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_rusak" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Rusak</span></a></li>
            </ul>
        </div>
    </li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cubes"></i><span class="kt-menu__link-text">Managemen Barang Jadi</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock/po_produk_pic" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Verifikasi PO Barang Jadi</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/stock_opname_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Stock Barang Jadi</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/kartu_stock_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Kartu Stock</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/po_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Input PO Barang Jadi</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Produk/table_slow_moving" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>List Dead Stock</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>List PO</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk/index2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Selesai</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Trial PO</span></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Pengemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/list_pendingan" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pendingan</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Laporan2/list_boleh_kirim_vs_kirim" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pengiriman</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Keluar</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pembagian Produk</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Konfirmasi Pengiriman</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_transfer" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Konfirmasi Inventory Transfer</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_safeconduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Surat Jalan</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_safeconduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Surat Jalan Selesai</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package/table_postage_finish" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Ongkir</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>M_account2/fifoProduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Incomplete Sales Data</span></a></li>
            </ul>
        </div>
    </li>

    <?php if ($_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "10") { ?>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true">
            <a href="<?= base_url() ?>T_sales/table_member" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">List Penjualan</span></a>
        </li>
    <?php } ?>

    <?php if ($_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "10" || $_SESSION['role_id'] == "5") { ?>
        <li class="kt-menu__item" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
            <a href="<?= base_url() ?>Laporan2/report_all" class="kt-menu__link"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span></div>
        </li>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-chart-line"></i><span class="kt-menu__link-text">Laporan Keuangan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_sales_product_payment_acc") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount SO</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan2/monthly_pr") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount PR</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan2/monthly_do") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount DO</span></a></li>
                </ul>
            </div>
        </li>
    <?php } ?>

    <?php if ($_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "10") { ?>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true">
            <a href="<?= base_url() ?>Pendingan_manual" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-history"></i><span class="kt-menu__link-text">Pendingan Manual</span></a>
        </li>
    <?php } ?>
<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "6") { ?>
    <li class="kt-menu__section ">
        <h4 class="kt-menu__section-text">Kemasan</h4>
        <i class="kt-menu__section-icon flaticon-more-v2"></i>
    </li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Tb_Stock_Kemasan_Detail" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-file-invoice-dollar"></i><span class="kt-menu__link-text">Invoice Kemasan</span></a></li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>M_account/vendor" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-user-alt"></i><span class="kt-menu__link-text">Akun Supplier</span></a></li>

    <li class="kt-menu__section ">
        <h4 class="kt-menu__section-text">Barang Jadi</h4>
        <i class="kt-menu__section-icon flaticon-more-v2"></i>
    </li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Tb_Stock_Produk_Detail" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-file-invoice-dollar"></i><span class="kt-menu__link-text">Invoice Barang Jadi</span></a></li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>M_account/factory" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-user-alt"></i><span class="kt-menu__link-text">Akun Factory</span></a></li>

    <li class="kt-menu__section ">
        <h4 class="kt-menu__section-text">Print</h4>
        <i class="kt-menu__section-icon flaticon-more-v2"></i>
    </li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Tb_Stock_Print_Detail" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-file-invoice-dollar"></i><span class="kt-menu__link-text">Invoice Print</span></a></li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>M_account/print" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-user-alt"></i><span class="kt-menu__link-text">Akun Print</span></a></li>

    <li class="kt-menu__section ">
        <h4 class="kt-menu__section-text">Penjualan</h4>
        <i class="kt-menu__section-icon flaticon-more-v2"></i>
    </li>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>M_account/verification_transfer_seller" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-check"></i><span class="kt-menu__link-text">Verifikasi Transfer Seller</span></a></li>
<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "12" || $_SESSION['role_id'] == "17" || $_SESSION['role_id'] == "7" || $_SESSION['role_id'] == "0" || $_SESSION['role_id'] == "6" || $_SESSION['role_id'] == "10" || $_SESSION['role_id'] == "11") { ?>

    <?php
    $perubahan_verifikasi_pembayaran_not_on_process = '';
    if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == '12') {
        $perubahan_verifikasi_pembayaran_not_on_process = $this->db->query("select * from v_count_perubahan_verifikasi_pembayaran_not_on_process where sales_category_id = '" . $_SESSION['sales_category_id'] . "'")->num_rows();
    }
    if ($_SESSION['role_id'] == '7') {
        $perubahan_verifikasi_pembayaran_not_on_process = $this->db->query("select * from v_count_perubahan_verifikasi_pembayaran_not_on_process where user_pic_id = '" . $_SESSION['user_id'] . "'")->num_rows();
    }
    ?>

    <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "12" || $_SESSION['role_id'] == "17" || $_SESSION['role_id'] == "7" || $_SESSION['role_id'] == "0") { ?>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-hand-holding-usd"></i><span class="kt-menu__link-text">Penjualan Lunas</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('T_sales/Form/Tambah') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Input Penjualan</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('T_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Penjualan</span></a></li>
                </ul>
            </div>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-money-check-alt"></i><span class="kt-menu__link-text">Penjualan Utang</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Penjualan/Penjualan_utang') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Penjualan Utang</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Penjualan/Penjualan_utang/List') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Penjualan Utang</span></a></li>
                </ul>
            </div>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-puzzle-piece"></i><span class="kt-menu__link-text">Penjualan Parsial</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Penjualan/Penjualan_parsial') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Penjualan Parsial</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Penjualan/Penjualan_parsial/List') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Penjualan Parsial</span></a></li>
                </ul>
            </div>
        </li>
    <?php } ?>

    <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == 12) { ?>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('Diskon') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-percentage"></i><span class="kt-menu__link-text">Diskon</span></a></li>
    <?php } ?>

    <?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "12" || $_SESSION['role_id'] == "17" || $_SESSION['role_id'] == "7" || $_SESSION['role_id'] == "0") { ?>
        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('Laporan2/list_boleh_kirim_vs_kirim') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-alt"></i><span class="kt-menu__link-text">List Pengiriman</span></a></li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('T_package_trial2/list_pendingan') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-alt"></i><span class="kt-menu__link-text">List Pendingan Produk</span></a></li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('T_package_trial2/list_pendingan_seller') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-alt"></i><span class="kt-menu__link-text">List Pendingan Seller</span></a></li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cart-arrow-down"></i><span class="kt-menu__link-text">Pembayaran</span><span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em; padding-top: 0.7em;"><?php if ($perubahan_verifikasi_pembayaran_not_on_process > 0) echo $perubahan_verifikasi_pembayaran_not_on_process; ?></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_account/seller') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pembayaran</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_account/seller_parsial') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pembayaran Parsial</span></a></li>
                    <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_account/edit_verif_pembayaran') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Perubahan Verifikasi Pembayaran <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($perubahan_verifikasi_pembayaran_not_on_process > 0) echo $perubahan_verifikasi_pembayaran_not_on_process; ?></span></span></a></li> -->
                </ul>
            </div>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cubes"></i><span class="kt-menu__link-text">Produk</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Produk_global') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Produk</span></a></li> -->
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk_global" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_product_global_group" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU Group</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Administrator/Stock_Produk/stock_opname_produk') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Stock Hari Ini</span></a></li>
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Packing</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('T_package_trial2') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History Pengiriman</span></a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Gudang</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/monthly_all') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Bulanan</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/division_daily') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/monthly_factory_in') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pabrik Bulanan</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/daily_all') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/daily_out') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/daily_product_out') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Produk Harian</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_out') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Bulanan</span></a></li>
                                <?php if ($_SESSION['role_id'] == "16") { ?>
                                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/chart') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Chart</span></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-dolly"></i><span class="kt-menu__link-text">Inventory Transfer</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_inventory/transfer" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Inventory Transfer Order</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_inbound" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">In Bound</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_outbound" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Out Bound</span></a></li>
                </ul>
            </div>
        </li>

        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cubes"></i><span class="kt-menu__link-text">Stock Control</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/stock_opname_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Stock Barang Jadi</span></a></li>
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/kartu_stock_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Kartu Stock</span></a></li>
                </ul>
            </div>
        </li>
    <?php } ?>
<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "15") { ?>
    <?php
    $verifikasi_pembayaran_seller_1 = $this->db->query("select * from v_count_verifikasi_pembayaran_seller_1_open")->num_rows();

    $verifikasi_pembayaran_seller_2 = $this->db->query("select * from v_count_verifikasi_pembayaran_seller_2_open")->num_rows();

    $verifikasi_revisi_pembayaran_open = $this->db->query("select * from v_count_verifikasi_revisi_pembayaran_open")->num_rows();

    $count_verifikasi = $verifikasi_pembayaran_seller_1 + $verifikasi_pembayaran_seller_2 + $verifikasi_revisi_pembayaran_open;
    ?>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_postage" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-shipping-fast"></i><span class="kt-menu__link-text">Ongkir</span></a></li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-check-square"></i><span class="kt-menu__link-text">Verifikasi</span><span class="badge badge-warning badge-pill" style="padding-left: 1em;padding-right: 1em; padding-top: 0.7em;"><?php if ($count_verifikasi > 0) echo $count_verifikasi; ?></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/verification_transfer_seller") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Pembayaran Seller 1 <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_pembayaran_seller_1 > 0) echo $verifikasi_pembayaran_seller_1; ?></span></span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/verification_transfer_seller2") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Pembayaran Seller 2 <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_pembayaran_seller_2 > 0) echo $verifikasi_pembayaran_seller_2; ?></span></span></a></li>
                <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("M_account2/get_finish_rev") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Revisi Pembayaran <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($verifikasi_revisi_pembayaran_open > 0) echo $verifikasi_revisi_pembayaran_open; ?></span></span></a></li> -->
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Tb_Stock_Produk_Detail") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Barang Masuk</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("T_postage") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Verifikasi Ongkir</span></a></li>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Laporan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("T_coa_transaction") ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Jurnal Umum</span></a></li>
            </ul>
        </div>
    </li>
<?php } ?>


<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "21") { ?>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>R_barcode_pwa_generator" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-users"></i><span class="kt-menu__link-text">Barcode</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_delivery_order" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-users"></i><span class="kt-menu__link-text">Surat Jalan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "14" || $_SESSION['role_id'] == 0) { ?>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/po_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-tag"></i><span class="kt-menu__link-text">Input PO Barang Jadi</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">List PO</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">PO</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk/index2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">PO Selesai</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Retur Barang Jadi</span></a></li>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon  fas fa-gift"></i><span class="kt-menu__link-text">Barang Tidak Sesuai SJ</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_retur" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-backward"></i><span class="kt-menu__link-text">Pengembalian Retur</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_sample" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon  fas fa-gift"></i><span class="kt-menu__link-text">Sample</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">

                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_factory_in" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Pabrik Bulanan</span></a></li>

            </ul>
        </div>
    </li>
<?php } ?>

<?php if ($_SESSION['role_id'] == "23" || $_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == "18" || $_SESSION['role_id'] == 0) { ?>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">Barang Keluar</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Pembagian Produk</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Konfirmasi Pengiriman</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_transfer" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Konfirmasi Inventory Transfer</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">History</span></a></li>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/division_daily" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_product_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Keluar Produk Harian</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Keluar Bulanan</span></a></li>
            </ul>
        </div>
    </li>
<?php } ?>

<?php if ($_SESSION['role_id'] == "2" || $_SESSION['role_id'] == "1" || $_SESSION['role_id'] == 0) { ?>
    <li class="kt-menu__section ">
        <h4 class="kt-menu__section-text">Adjustment</h4>
        <i class="kt-menu__section-icon flaticon-more-v2"></i>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">Adjustment</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Inbound</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Terima_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Terima Produk</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Tidak_sesuai_sj" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Realisasi Tidak Sesuai SJ</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Po" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO</span></a></li>
                        </ul>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Outbound</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Konfirmasi_pengiriman" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Konfirmasi pengiriman</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Retur_pabrik" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Retur Pabrik</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Sample" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Sample</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Rusak" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Rusak</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Konfirmasi_inventory_transfer" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Konfirmasi Inventory Transfer</span></a></li>
                        </ul>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Sales</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Pr" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Purchase Request</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Payment" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Payment</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/So" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Sales Order</span></a></li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
    </li> 
    <!-- <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">Adjustment</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Terima_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Terima Produk</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Tidak_sesuai_sj" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Realisasi Tidak Sesuai SJ</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Adjustment/Tidak_sesuai_sj" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Realisasi Tidak Sesuai SJ</span></a></li>
            </ul>
        </div>
    </li> -->
<?php } ?>

<?php if ($_SESSION['role_id'] == "19" || $_SESSION['role_id'] == 0) { ?>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-archive"></i><span class="kt-menu__link-text">Master</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_login_pwa" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">User PWA</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_line" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Line</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Factory" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pabrik</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Produk</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk_global" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_product_global_group" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU Group</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_brand" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Brand</span></a></li>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Bulanan1</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/division_daily" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_factory_in" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Terima Barang Bulanan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_product_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Harian</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Bulanan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Pendingan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan_per_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Pendingan Per Seller</span></a></li>
                <?php if ($_SESSION['role_id'] == "16") { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Chart</span></a></li>
                <?php } ?>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cubes"></i><span class="kt-menu__link-text">Managemen Barang Jadi</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/stock_opname_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Stock Barang Jadi</span></a></li>

            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Pengemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/list_pendingan" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pendingan</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Laporan2/list_boleh_kirim_vs_kirim" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pengiriman</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Keluar</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pembagian Produk</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Konfirmasi Pengiriman</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/table_transfer" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Konfirmasi Inventory Transfer</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_transfer" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Transfer Antar Gudang</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_safeconduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Surat Jalan</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/table_safeconduct" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Surat Jalan Selesai</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package/table_postage_finish" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Ongkir</span></a></li>
            </ul>
        </div>
    </li> 
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"><a href="<?= base_url() ?>T_sales/table_member" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Penjualan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-handshake"></i><span class="kt-menu__link-text">Sales</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Produk</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Bulanan</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Sales</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Seller</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller Bulanan</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_paid" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Mutasi Perorangan</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Bulanan</span></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </li>

    <?php if ($_SESSION['role_id'] == "16") { ?>
        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Chart</span></a></li>
    <?php } ?>
<?php } ?>

<?php if ($_SESSION['role_id'] == "22" || $_SESSION['role_id'] == 0) { ?>

    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-archive"></i><span class="kt-menu__link-text">Master</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_login_pwa" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">User PWA</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_line" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Line</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Factory" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pabrik</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Produk</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk_global" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_product_global_group" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU Group</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_brand" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Brand</span></a></li>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Bulanan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/division_daily" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_factory_in" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Terima Barang Bulanan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
                <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li> -->
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_product_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Harian</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Bulanan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Pendingan</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan_per_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Pendingan Per Seller</span></a></li>
                <?php if ($_SESSION['role_id'] == "16") { ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Chart</span></a></li>
                <?php } ?>

                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/sample" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Sample</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/rusak" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Rusak</span></a></li>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cubes"></i><span class="kt-menu__link-text">Managemen Barang Jadi</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock/po_produk_pic" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Verifikasi PO Barang Jadi</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/stock_opname_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Stock Barang Jadi</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/kartu_stock_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Kartu Stock</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Administrator/Stock_Produk/po_produk" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Input PO Barang Jadi</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Produk/table_slow_moving" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>List Dead Stock</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>List PO</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Po_produk/index2" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">PO Selesai</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Trial PO</span></a></li>
                        </ul>
                    </div>
                </li>           
            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-undo"></i><span class="kt-menu__link-text">Line Quality Control</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i>Adjustment</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/form_masuk_opname" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Masuk</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_package_trial2/form_keluar_opname" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Keluar</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Terima_produk" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Barang Tidak Sesuai SJ</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_retur" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pengembalian Retur</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_return_factory" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Retur Ke Pabrik</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_repair" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Repair</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_sample" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Sample</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/form_rusak" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Rusak</span></a></li>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Pengemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>T_package_trial2/list_pendingan" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pendingan</span></a></li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Laporan2/list_boleh_kirim_vs_kirim" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Pengiriman</span></a></li>
                
            </ul>
        </div>
    </li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"><a href="<?= base_url() ?>T_sales" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-box"></i><span class="kt-menu__link-text">Penjualan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-handshake"></i><span class="kt-menu__link-text">Sales</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Produk</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/sample" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Sample</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/rusak" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Rusak</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Bulanan</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Sales</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Seller</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller Bulanan</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_paid" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Mutasi Perorangan</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Bulanan</span></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </li>

    <?php if ($_SESSION['role_id'] == "16") { ?>
        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Chart</span></a></li>
    <?php } ?>
</ul>
</div>
</li>
<?php } ?>