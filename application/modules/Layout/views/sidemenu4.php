<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">Master</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Kemasan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Kemasan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Supplier" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Supplier</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/stock_opname_kemasan" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-box-open"></i><span class="kt-menu__link-text">Stock Kemasan</span></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"><a href="<?= base_url() ?>M_product_classification" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-box-open"></i><span class="kt-menu__link-text">Klasifikasi Produk</span></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/kartu_stock_kemasan" class="kt-menu__link "><i class="kt-menu__link-icon fas fa-id-card"></i><span class="kt-menu__link-text">Kartu Stock</span></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">PO Kemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Input PO Kemasan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_history" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">History PO Kemasan</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">List PO Kemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_active_pic" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">PO Kemasan Active</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_active_pic_tidak" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">PO Kemasan Selesai</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">PO Print</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_print" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Input PO Print</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_history_print" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">History PO Print</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">List PO Print</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_print_active_pic" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">PO Print Active</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/po_kemasan_active_pic_tidak" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">PO Print Selesai</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-ul"></i><span class="kt-menu__link-text">Pengeluaran Kemasan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/pengeluaran_kemasan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Input Pengeluaran Kemasan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/data_pengeluaran_kemasan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Data Pengeluaran Kemasan</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book-open"></i><span class="kt-menu__link-text">Laporan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Administrator/Stock/laporan_po_kemasan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan PO</span></a></li>
        </ul>
    </div>
</li>