<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Kemasan</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Tb_Stock_Kemasan_Detail" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-file-invoice-dollar"></i><span class="kt-menu__link-text">Invoice Kemasan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>M_account/vendor" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-user-alt"></i><span class="kt-menu__link-text">Akun Supplier</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-chart-area"></i><span class="kt-menu__link-text">COA</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">COA 1</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA/coa2" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">COA 2</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA/coa3" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">COA 3</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>COA/coa4" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">COA 4</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>T_coa_user" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Kas Pengeluaran</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Barang Jadi</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Tb_Stock_Produk_Detail" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-file-invoice-dollar"></i><span class="kt-menu__link-text">Invoice Barang Jadi</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>M_account/factory" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-user-alt"></i><span class="kt-menu__link-text">Akun Factory</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Print</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>Tb_Stock_Print_Detail" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-file-invoice-dollar"></i><span class="kt-menu__link-text">Invoice Print</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>M_account/print" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-user-alt"></i><span class="kt-menu__link-text">Akun Print</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Penjualan</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url() ?>M_account/verification_transfer_seller" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-check"></i><span class="kt-menu__link-text">Verifikasi Transfer Seller</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a></li>