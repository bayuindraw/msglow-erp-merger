<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-chart-line"></i><span class="kt-menu__link-text">Laporan Keuangan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_sales_product_payment_acc") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount SO</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_pr") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount PR</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url("Laporan/monthly_do") ?>" class="kt-menu__link "><span></span></i><span class="kt-menu__link-text">Monthly Amount DO</span></a></li>
        </ul>
    </div>
</li>