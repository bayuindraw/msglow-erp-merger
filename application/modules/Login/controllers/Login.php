<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index($massage = "")
	{
		if (@$_SESSION['logged_in']) {
			redirect(site_url() . "M_user");
		} else if (@$_SESSION['user_id'] != "") {
			$data['url'] = 'beranda';
			$data['title'] = 'Halaman Beranda';
			$data['massage'] = $massage;
			$this->load->view('auth', $data);
		} else {
			$data['url'] = 'beranda';
			$data['title'] = 'Halaman Beranda';
			$data['massage'] = $massage;
			$this->load->view('index', $data);
		}
	}

	public function index2($massage = "")
	{
		if (@$_SESSION['logged_in']) {
			redirect(site_url() . "M_user");
		} else {
			$data['url'] = 'beranda';
			$data['title'] = 'Halaman Beranda';
			$data['massage'] = $massage;
			$this->load->view('index', $data);
		}
	}

	public function cek_google($massage = "")
	{
		if (@$_SESSION['logged_in']) {
			redirect(site_url() . "m_account");
		} else {
			$data['url'] = 'beranda';
			$data['title'] = 'Halaman Beranda';
			$data['massage'] = $massage;
			$this->load->view('cek_google', $data);
		}
	}

	public function verify()
	{
		if (isset($_POST['portal'])) {
			$pass = md5($_POST['pass']);
			$pass = hash('sha256', $pass);
		} else {
			$pass = md5($_POST['pass']);
		}
		$tgl = $this->db->query("SELECT * FROM r_barcode_pwa_generator")->row_array();
		if($tgl['barcode_pwa_generator_date'] != date('Y-m-d')){
			$this->db->query("UPDATE r_barcode_pwa_generator SET barcode_pwa_generator_date = '".date('Y-m-d')."', barcode_pwa_generator_counter = 0, barcode_pwa_generator_counter_last = 0");
			$this->db->query("DELETE FROM monthly_product_mv");
			$this->db->query("INSERT INTO monthly_product_mv (SELECT ZA.nama_produk, ZB.*, IFNULL(ZC.jum_kirim, 0) as jum_kirim, IFNULL(ZD.jum_bayar, 0) as jum_bayar FROM produk_global ZA JOIN (SELECT SUM(B.sales_detail_quantity) AS jum_order, C.kd_pd, REPLACE(LEFT(A.sales_date, 7), '-', '') AS bulan, A.seller_id, D.nama FROM t_sales A JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN produk C ON C.id_produk = B.product_id JOIN member D ON D.kode = A.seller_id GROUP BY A.seller_id, C.kd_pd, REPLACE(LEFT(A.sales_date, 7), '-', '')) ZB ON ZB.kd_pd = ZA.kode LEFT JOIN (SELECT SUM(B.package_detail_quantity) AS jum_kirim, C.kd_pd, REPLACE(LEFT(A.package_date, 7), '-', '') AS bulan, member_code FROM t_package_trial A JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN produk C ON C.id_produk = B.product_id GROUP BY A.member_code, C.kd_pd, REPLACE(LEFT(A.package_date, 7), '-', '')) ZC ON ZC.kd_pd = ZA.kode AND ZC.member_code = ZB.seller_id AND ZC.bulan = ZB.bulan LEFT JOIN (SELECT SUM(B.account_detail_sales_product_allow) AS jum_bayar, D.kd_pd, REPLACE(LEFT(A.account_detail_sales_date, 7), '-', '') AS bulan, seller_Id FROM t_account_detail_sales A JOIN t_account_detail_sales_product B ON B.account_detail_sales_id = A.account_detail_sales_id JOIN t_sales C ON C.sales_id = A.sales_id JOIN produk D ON D.id_produk = B.product_id GROUP BY C.seller_id, D.kd_pd, REPLACE(LEFT(A.account_detail_sales_date, 7), '-', '')) ZD ON ZD.kd_pd = ZA.kode AND ZD.seller_id = ZB.seller_id AND ZC.bulan = ZB.bulan ORDER BY ZB.seller_id, ZB.bulan);");
			$this->db->query("DROP TABLE sales_mv;");
			$this->db->query("CREATE TABLE sales_mv (member_name VARCHAR(128)  NOT NULL, member_kode    VARCHAR(128) NOT NULL, product_name   VARCHAR(128) NOT NULL, product_kd   VARCHAR(20) NOT NULL, total_order   FLOAT  NOT NULL, total_paid   FLOAT NOT NULL, total_connected   FLOAT NOT NULL, total_pending   FLOAT NOT NULL);");
			$this->db->query("INSERT INTO sales_mv SELECT C.nama, C.kode, E.nama_produk, E.kode, SUM(sales_detail_quantity) as pr, SUM(connected_quantity + pending_quantity) as bayar, SUM(connected_quantity) as terkirim, SUM(pending_quantity) as pending FROM t_sales A JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN member C ON C.kode = A.seller_id JOIN produk D ON D.id_produk = B.product_id JOIN produk_global E ON E.kode = D.kd_pd WHERE DATE_FORMAT(A.sales_date,'%Y%m') = DATE_FORMAT(NOW(),'%Y%m') GROUP BY A.seller_id, E.kode;");
		}
		$this->db->query("UPDATE m_discount SET discount_active = 1 WHERE discount_active != 2 AND DATE_FORMAT(NOW(), '%Y%m%d') BETWEEN REPLACE(discount_start_date, '-', '') AND REPLACE(discount_end_date, '-', '')");
		$this->db->query("UPDATE m_discount SET discount_active = 2 WHERE DATE_FORMAT(NOW(), '%Y%m%d') > REPLACE(discount_end_date, '-', '')");
		$chk_pending = $this->db->query("SELECT MAX(REPLACE(pending_date, '-', '')) as chk FROM r_pending")->row_array();
		if ($chk_pending['chk'] != date('Ymd')) {
			$jum_pending = $this->db->query("SELECT SUM(connected_quantity) as jum FROM t_sales_detail")->row_array();
			$xdat['pending_date'] = date('Y-m-d'); 
			$xdat['pending_quantity'] = $jum_pending['jum'];
			$this->db->insert('r_pending', $xdat);
		}
		// UPDATE QUERY BAYU PENGAMBILAN ROLE ID
		// $query = "SELECT * FROM m_user A LEFT JOIN m_role B ON B.role_id = A.role_id WHERE (A.user_name = '$_POST[username]' OR A.user_email = '$_POST[username]')";
		$query = "SELECT a.*, c.* FROM m_user a
		JOIN m_user_role b ON a.user_id = b.user_id
		JOIN m_role c ON b.role_id = c.role_id
		WHERE (a.user_name = '".$this->input->post('username')."' OR a.user_email = '".$this->input->post('username')."')
		LIMIT 1";
		$data = $this->db->query($query)->row_array();
		if($data<1){
			$query = "SELECT * FROM m_user A LEFT JOIN m_role B ON B.role_id = A.role_id WHERE (A.user_name = '$_POST[username]' OR A.user_email = '$_POST[username]')";
			$data = $this->db->query($query)->row_array();
		}
		//END UPDATE BAYU
		$arrmaxtgl = $this->db->query("SELECT MAX(tanggal) as tanggal FROM tb_stock_kemasan_history")->row_array();
		if ($arrmaxtgl['tanggal'] < date('Y-m') . "-01") {
			$this->db->query("INSERT tb_stock_kemasan_history (tanggal, id_barang, jumlah, min_jumlah, rusak, am_jumlah) SELECT '" . date('Y-m') . '-01' . "', id_barang, jumlah, min_jumlah, rusak, am_jumlah FROM tb_stock_kemasan");
			$this->db->query("INSERT t_coa_total_history (coa_total_date, coa_id, coa_code, coa_total_debit, coa_total_credit, coa_level, coa_name) SELECT '" . date('Y-m') . '-01' . "', coa_id, coa_code, coa_total_debit, coa_total_credit, coa_level, coa_name FROM tb_stock_kemasan");
		}
		
		if ((((@$data['user_password_sha'] == $pass || @$data['user_password'] == $pass) && @$data['user_password_random'] == "") || @$data['user_password_random'] == $_POST['pass']) && @$data['user_status'] == '1') {
			/*date_default_timezone_set("Asia/Jakarta");
			$this->load->library('GoogleAuthenticator');
			$pga = new GoogleAuthenticator();
			$code = $pga->getCode($data['user_secret']);
			if($code == $_POST['authentication_code']){*/
				$permitted_chars 	= '0123456789';
				$passowrd_reset 	= substr(str_shuffle($permitted_chars), 0, 6);
				if ($data['user_secret'] == "" && $data['role_id'] != "23" && $data['role_id'] != "24" && $data['role_id'] != "25") {
					$this->db->query("UPDATE m_user SET user_password_random = '$passowrd_reset' WHERE user_name = '$_POST[username]'");
				}
				$jumproduk_stock = $this->db->query("SELECT COUNT(id_stock) as jum FROM tb_stock_produk_history WHERE warehouse_id = '".$data['warehouse_id']."' and LEFT(tanggal, 7) = '" . date('Y-m') . "'")->row_array();
				if ($jumproduk_stock['jum'] < 1) {
					$this->db->query("INSERT INTO tb_stock_produk_history (tanggal, id_barang, jumlah, min_jumlah, rusak, nama_produk, warehouse_id) (SELECT '" . date('Y-m') . "-01', id_barang, jumlah, min_jumlah, rusak, nama_produk, warehouse_id FROM tb_stock_produk)");
				}
				if ($data['user_secret'] == "") {
					$data['logged_in'] = true;
				}

				$this->session->set_userdata($data);
				if ($data['role_id'] == '1' || $data['role_id'] == '2') {
					redirect(site_url() . "M_user");
				} else if ($data['role_id'] == '5') {
					redirect(site_url() . "Laporan2/chart");
				} else if ($data['role_id'] == '98') {
					redirect(site_url() . "T_salesd");
				} else if ($data['role_id'] == '11') {
					redirect(site_url() . "COA");
				} else {
					redirect(site_url() . "Laporan2/chart");
				}
			/*}else{
				echo "<script type='text/javascript'>alert('".$code."');</script>";
				$this->index("Kode Autentikasi salah");
			}*/
		} else {
			echo "<script type='text/javascript'>alert('Username atau Password salah');</script>";
			$this->index("Username atau Password salah");
		}
	}


	public function auth()
	{
		$query = "SELECT * FROM m_user A LEFT JOIN m_role B ON B.role_id = A.role_id WHERE (A.user_name = '$_SESSION[user_name]')";
		$data = $this->db->query($query)->row_array();

		date_default_timezone_set("Asia/Jakarta");
		$this->load->library('GoogleAuthenticator');
		$pga = new GoogleAuthenticator();
		$code = $pga->getCode($data['user_secret']);
		if ($code == $_POST['authentication_code']) {
			$jumproduk_stock = $this->db->query("SELECT COUNT(id_stock) as jum FROM tb_stock_produk_history WHERE LEFT(tanggal, 7) = '" . date('Y-m') . "'")->row_array();
			if ($jumproduk_stock['jum'] < 1) {
				$this->db->query("INSERT INTO tb_stock_produk_history (tanggal, id_barang, jumlah, min_jumlah, rusak, nama_produk) (SELECT '" . date('Y-m') . "-01', id_barang, jumlah, min_jumlah, rusak, nama_produk FROM tb_stock_produk)");
			}
			$data['logged_in'] = true;
			$this->session->set_userdata($data);
			if ($data['role_id'] == '1' || $data['role_id'] == '2') {
				redirect(site_url() . "M_user");
			} else if ($data['role_id'] == '5') {
				redirect(site_url() . "Laporan2/chart");
			} else if ($data['role_id'] == '98') {
				redirect(site_url() . "T_salesd");
			} else if ($data['role_id'] == '11') {
				redirect(site_url() . "COA");
			} else {
				redirect(site_url() . "Laporan2/chart");
			}
		} else { 
			echo "<script type='text/javascript'>alert('Kode Autentikasi $code');</script>";
			$this->index("Kode Autentikasi salah");
		}
	}

	public function logout()
	{
		session_destroy();
		//$_SESSION['logged_in'] = false;
		$this->index2();
	}

	public function verify2($id)
	{
		if ($_SESSION['role_id'] == '2') {
			$query = "SELECT * FROM m_user A LEFT JOIN m_role B ON B.role_id = A.role_id WHERE A.user_id = $id";
			$data = $this->db->query($query)->row_array();
			$data['logged_in'] = true;
			$this->session->set_userdata($data);
			redirect(site_url() . "M_user");
		}
	}
}
