<?php
if ($parameter == 'ubah' && $id != '' && $id_detail != '') {
	$this->db->where('account_detail_id', $id_detail);
	$row = $this->Model->get_detail($id)->row_array();
}
?>
							<div class="row">
								<div class="col-lg-12 col-xl-6">
									<div class="kt-portlet">
										<form method="post" action="<?= site_url($url . '/simpan_detail'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<?= input_hidden('id_detail', @$id_detail) ?>
											<?= input_hidden('transaction_type_last', ((@$row['account_detail_debit'] > 0)?'debit':'credit')) ?>
											<?= input_hidden('transaction_amount_last', ((@$row['account_detail_debit'] > 0)?@$row['account_detail_debit']:@$row['account_detail_credit'])) ?>
											<?= input_hidden('transaction_date_last', @$row['account_detail_date']) ?>
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>   
											</div>
										</div> 
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Jenis Transaksi</label>
													<select class="form-control" id="exampleSelect1" name="transaction_type" required>
														<option value="">Pilih Jenis Transaksi</option>
														<option value="debit" <?= (@$row['account_detail_debit'] > 0)?'selected':''; ?>>Debit</option>
														<option value="credit" <?= (@$row['account_detail_credit'] > 0)?'selected':''; ?>>Credit</option>
													</select>
												</div>
												<div class="form-group">
													<label>Kategori</label>
													<select class="form-control" id="exampleSelect1" name="input[account_detail_category_id]" required>
														<option value="">Pilih Kategori</option> 
														<?php
														foreach ($m_account_detail_category as $data) { 
															echo "<option value='" . $data->account_detail_category_id . "'".(($data->account_detail_category_id == @$row['account_detail_category_id'])?'selected':'').">" . $data->account_detail_category_name . "</option>";
														}
														?>
													</select>
												</div>
												<div class="form-group">
													<label>PIC</label>
													<input type="text" class="form-control" placeholder="PIC" name="input[account_detail_pic]" value="<?= @$row['account_detail_pic'] ?>" required>
												</div>
												<div class="form-group">
													<label>Nominal</label>
													<input type="number" class="form-control" placeholder="Nominal" name="transaction_amount" value="<?= @$row['account_detail_credit'] ?>" required>
												</div>
												<div class="form-group">
													<label>Catatan</label>
													<textarea class="summernote" name="input[account_detail_note]"><?= @$row['account_detail_note'] ?></textarea>
												</div>
												<div class="form-group">
													<label>Tanggal</label>
													<input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="input[account_detail_date]" value="<?= @$row['account_detail_date'] ?>">
												</div>
												<div class="form-group">
													<label class="kt-checkbox">
														<input type="checkbox" name="realisasi" <?= (@$row['account_detail_realization']==1)?'checked':'' ?>> Realisasi
														<span></span>
													</label>	
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>