<div class="row">
	<form method="post" action="<?= site_url($url . '/set_revisi_transfer_seller'); ?>" enctype="multipart/form-data">
		<div class="col-lg-12 col-xl-12">
			<div class="kt-portlet">
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Transfer
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Rekening Tujuan</th>
								<th>Nama Pengirim</th>
								<th>Tanggal</th>
								<th>Nominal</th>
								<th>Catatan</th>
								<th width="20%">Bukti Transfer</th>
								<th>Edit Transfer</th>
							</tr>
						</thead>
						<tbody id="x">
							<?php $i = 0;
							$total = 0;
							$cekRev = 0;
							foreach ($pembayarans as $pembayaran) { ?>
								<tr id="<?= $i ?>">
									<td><select class="form-control" id="exampleSelect1" disabled required>
											<option value="">Pilih Akun</option>
											<?php
											foreach ($m_account as $data) {
												echo "<option value='" . $data->account_id . "'" . (($data->account_name == $pembayaran['account_name']) ? 'selected' : '') . ">" . $data->account_name . "</option>";
											}
											?>
										</select></td>
									<input type="hidden" name="account_id[<?= $i ?>]" value="<?= $pembayaran['bank_id'] ?>" />

									<td><input type="text" class="form-control" placeholder="Nama Pengirim" value="<?= $pembayaran['account_detail_transfer_name'] ?>" readonly name="from[account_detail_transfer_name][<?= $i ?>]"></td>
									<td><input type="text" class="form-control date_picker" readonly placeholder="Select date" name="from[account_detail_date][<?= $i ?>]" value="<?= $pembayaran['account_detail_date'] ?>" required></td>
									<td><input type="text" class="form-control numeric paid_total" placeholder="Nominal" name="from[account_detail_credit][<?= $i ?>]" value="<?= str_replace(".00", "", (($pembayaran['coa_credit'] == 0) ? $pembayaran['coa_debit'] : $pembayaran['coa_credit'])) ?>" <?= ($pembayaran['coa_transaction_source'] == 1) ? $cekRev = 1 : 'readonly' ?> onkeyup="chk_total()" required></td>
									<td><input type="text" class="form-control" placeholder="Catatan" name="from[account_note][<?= $i ?>]" value="<?= $pembayaran['account_detail_note_accounting'] ?>" readonly required></td>
									<td><a href="<?= site_url('upload/TRANSFER/' . $pembayaran['account_detail_proof']) ?>" target="_blank"><img src="<?= site_url('upload/TRANSFER/' . $pembayaran['account_detail_proof']) ?>" style="max-width: 90%;" /></a></td>
									<input type="hidden" name="from[account_detail_proof][<?= $i ?>]" value="<?= $pembayaran['account_detail_proof'] ?>">
									<input type="hidden" name="from[account_detail_real_id][<?= $i ?>]" value="<?= $pembayaran['account_detail_real_id'] ?>" />
									<input type="hidden" name="account_id_past[<?= $pembayaran['account_detail_id'] ?>]" value="<?= $pembayaran['account_detail_id'] ?>" />
									<td><a href="<?= $url."/edit_transfer_proof/".$pembayaran['account_detail_real_id'] ?>">edit</a></td>
									
								</tr>
							<?php $i++;
								$total += $pembayaran['coa_credit'];
							} ?>
						</tbody>
					</table>
					<div class="form-group">
						<label>Deposit</label>
						<input type="text" class="form-control numeric" placeholder="Nominal" name="deposit" value="<?=  $arraccount['account_deposit'] ?>" id="deposit" readonly>
					</div>
					<div class="form-group">
						<label>Nominal</label>
						<input type="text" class="form-control numeric" placeholder="Nominal" name="nominal" value="<?= $total ?>" id="paid" required onkeyup="chk_total()" readonly>
					</div>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>No</th>
								<th>KODE SO</th>
								<th>TANGGAL</th>
								<th>TOTAL TAGIAN</th>
								<th>BAYAR</th>
								<th>KURANG BAYAR</th>
								<th>BAYAR</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$nox = 0;
							$deposit = 0;
							$cekPar = 0;
							$cekTotalKirim = 0;
							$xo = 0;
							foreach ($arrlist_account_sales as $key => $vaData) {
								
								$cekTotalKirim += $vaData['total_kurang_kirim'];
								if(@$vaData['sales_id'] != ""){
								if (@$vaData['account_detail_paid_type'] == '2') $cekPar = 1;
								$xo++;
							?>
								<tr>
									<td><?= ++$nox ?></td>
									<td onclick="$('#<?= $key ?>x').toggle();"><?= @$vaData['sales_code'] ?></td>
									<td><?= @$vaData['sales_date'] ?></td>
									<td>Rp <?= number_format(@$vaData['account_detail_debit']) ?></td>
									<td>Rp <?= number_format(@$vaData['account_detail_paid'] + @$vaData['account_detail_paid_draft'] - @$nominal_bayar[$vaData['sales_id']]['total_bayar']) ?></td>
									<td><input type="hidden" name="sales_type[<?= @$vaData['account_detail_real_id'] ?>]" value="<?= @$vaData['sales_type'] ?>" />Rp <?= number_format(@$vaData['account_detail_debit'] - @$vaData['account_detail_paid_draft'] - @$vaData['account_detail_paid'] + @$nominal_bayar[$vaData['sales_id']]['total_bayar']) ?></td>
									<td><input id="bayar<?= $key ?>" type="text" name="paid[<?= $vaData['account_detail_real_id'] ?>]" max="<?= $vaData['account_detail_debit'] - $vaData['account_detail_paid'] + @$nominal_bayar[$vaData['sales_id']]['total_bayar'] ?>" class="form-control md-static sum_total numeric" key="<?= $key ?>" value="<?= $vaData['account_detail_paid_draft'] ?>" onkeyup="chk_total()" <?= ($arraccount['schema_id'] == 1 || $arraccount['schema_id'] == 4 || $arraccount['schema_id'] == 5 || $arraccount['schema_id'] == 6) && $vaData['account_detail_paid_type'] == '1' ? 'readonly' : '' ?>><input type="hidden" name="sales[<?= $vaData['account_detail_real_id'] ?>]" value="<?= $vaData['sales_id']; ?>"></td>
									<?php if (@$nominal_bayar[$vaData['sales_id']]['total_bayar'] > 0) { ?>
										<input type="hidden" name="account_update[<?= $arrreal_id[$vaData['sales_id']]['account_detail_real_id'] ?>]" value="<?= $nominal_bayar[$vaData['sales_id']]['total_bayar'] ?>" />
									<?php $deposit += @$nominal_bayar[$vaData['sales_id']]['total_bayar'];
									} ?>
									<input type="hidden" name="sales_id_all[<?= @$vaData['sales_id']; ?>]" value="<?= @$vaData['sales_id']; ?>">
									<input type="hidden" name="old_paid[<?= @$vaData['sales_id']; ?>]" value="<?= @$vaData['account_detail_paid_draft'] ?>">
								</tr>
								<?php if (($arraccount['schema_id'] == 1 || $arraccount['schema_id'] == 4 || $arraccount['schema_id'] == 5 || $arraccount['schema_id'] == 6) && @$vaData['account_detail_paid_type'] == '1' && $vaData['total_kurang_kirim'] > 0) { ?>
									<tr id="<?= $key ?>x">
										<td></td> 
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>NAMA PRODUK</th>
														<th>JUMLAH PESANAN</th>
														<th>BOLEH KIRIM</th>
														<th>KURANG BOLEH KIRIM</th>
														<th>JUMLAH KIRIM</th>
														<th>TOTAL</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$no = 0;
													if (@$arraccount_detail_sales[$key] != null) {
														foreach (@$arraccount_detail_sales[$key] as $key2 => $vaData2) {
													?>
															<tr>
																<td><?= ++$no ?></td>
																<td><?= $vaData2['nama_produk'] ?> (@Rp <?= number_format($vaData2['sales_detail_price']) ?>) <input type="hidden" name="harga_produk[<?= $key ?>][<?= $key2 ?>]" value="<?= $vaData2['sales_detail_price'] ?>" /></td>
																<td><?= $vaData2['sales_detail_quantity'] ?></td>
																<td><?= $vaData2['already'] - $vaData2['send_allow'] ?></td>
																<td><?= $vaData2['sales_detail_quantity'] - $vaData2['already'] + $vaData2['send_allow'] ?></td>
																<td>
																	<input type="text" name="allow[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" max="<?= $vaData2['sales_detail_quantity'] - $vaData2['already'] + $vaData2['send_allow'] ?>" class="form-control md-static sum_total_<?= $key ?>" id_dex="<?= $key ?>D<?= $key2 ?>" id_debit="<?= $key ?>debit<?= $key2 ?>" price="<?= $vaData2['sales_detail_price'] ?>" value="<?= $vaData2['send_allow'] ?>" onkeyup="chk_total_<?= $key ?>(<?= $vaData2['sales_type'] ?>, <?= $i ?>); chk_total();" id_product="<?= $vaData2['product_id'] ?>" item_draft="<?= $vaData2['send_allow'] ?>" total_draft="<?= $vaData2['send_allow'] *  $vaData2['sales_detail_price'] ?>" required>
																	<input type="hidden" name="product_id[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" value="<?= $vaData2['product_id'] ?>">
																	<input type="hidden" name="ids[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" value="<?= @$vaData2['account_detail_sales_id'] ?>">
																	<input type="hidden" name="before[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" value="<?= $vaData2['send_allow'] ?>" />
																</td>
																<td>
																	<div id="<?= $key ?>D<?= $key2 ?>">Rp <?= number_format($vaData2['sales_detail_price'] *  $vaData2['send_allow']) ?></div>
																	<input class="total_bayar_<?= $key ?>" type="hidden" id="<?= $key ?>debit<?= $key2 ?>" value="<?= $vaData2['sales_detail_price'] *  $vaData2['send_allow'] ?>" />
																</td>
															</tr>
													<?php }

														$i++;
													} ?>

												</tbody>
											</table>
										</td>
									</tr>
									<div id="divlist_produk_sisa<?= $key ?>"></div>
									<div id="divdiskon<?= $key ?>"></div>
									<div id="divhoard<?= $key ?>"></div>
									<tr id="tableBonusProduk<?= $key ?>" style="display: none;">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>NAMA PRODUK</th>
														<th>BONUS</th>
													</tr>
												</thead>
												<tbody id="bodyBonusProduk<?= $key ?>">
												</tbody>
											</table>
										</td>
									</tr>
									<tr id="tableBonusPrice<?= $key ?>" style="display: none;">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>HARGA</th>
													</tr>
												</thead>
												<tbody id="bodyBonusPrice<?= $key ?>">
												</tbody>
											</table>
										</td>
									</tr>
									<tr id="tableBonusPersen<?= $key ?>" style="display: none;">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>Persen</th>
														<th>Potongan</th>
													</tr>
												</thead>
												<tbody id="bodyBonusPersen<?= $key ?>">
												</tbody>
											</table>
										</td>
									</tr>
								<?php } else if (@$vaData['account_detail_paid_type'] == '2' && $vaData['total_kurang_kirim'] > 0) { ?>
									<tr id="<?= $key ?>x">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>NAMA PRODUK</th>
														<th>JUMLAH PESANAN</th>
														<th>BOLEH KIRIM</th>
														<th>KURANG BOLEH KIRIM</th>
														<th>JUMLAH KIRIM</th>
														<th>TOTAL</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$no = 0;
													if (@$arraccount_detail_sales[$key] != null) {
														foreach (@$arraccount_detail_sales[$key] as $key2 => $vaData2) {
													?>
															<tr>
																<td><?= ++$no ?></td>
																<td><?= $vaData2['nama_produk'] ?> (@Rp <?= number_format($vaData2['sales_detail_price']) ?>) <input type="hidden" name="harga_produk[<?= $key ?>][<?= $key2 ?>]" value="<?= $vaData2['sales_detail_price'] ?>" /></td>
																<td><?= $vaData2['sales_detail_quantity'] ?></td>
																<td><?= $vaData2['already'] - $vaData2['send_allow'] ?></td>
																<td><?= $vaData2['sales_detail_quantity'] - $vaData2['already'] + $vaData2['send_allow'] ?></td>
																<td>
																	<input type="text" name="allow[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" max="<?= $vaData2['sales_detail_quantity'] - $vaData2['already'] + $vaData2['send_allow'] ?>" class="form-control md-static sum_total_parsial_<?= $key ?>" id_dex="<?= $key ?>D<?= $key2 ?>" id_debit="<?= $key ?>debit<?= $key2 ?>" price="<?= $vaData2['sales_detail_price'] ?>" value="<?= $vaData2['send_allow'] ?>" onkeyup="chk_total_<?= $key ?>(<?= $vaData2['sales_type'] ?>, <?= $i ?>); chk_total();" id_product="<?= $vaData2['product_id'] ?>" item_draft="<?= $vaData2['send_allow'] ?>" total_draft="<?= $vaData2['send_allow'] *  $vaData2['sales_detail_price'] ?>" required>
																	<input type="hidden" name="product_id[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" value="<?= $vaData2['product_id'] ?>">
																	<input type="hidden" name="ids[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" value="<?= @$vaData2['account_detail_sales_id'] ?>">
																	<input type="hidden" name="before[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" value="<?= $vaData2['send_allow'] ?>" />
																</td>
																<td>
																	<div id="<?= $key ?>D<?= $key2 ?>">Rp <?= number_format($vaData2['sales_detail_price'] *  $vaData2['send_allow']) ?></div>
																	<input class="total_bayar_<?= $key ?>" type="hidden" id="<?= $key ?>debit<?= $key2 ?>" value="<?= $vaData2['sales_detail_price'] *  $vaData2['send_allow'] ?>" />
																</td>
															</tr>
													<?php }

														$i++;
													} ?>

												</tbody>
											</table>
										</td>
									</tr>
									<div id="divlist_produk_sisa<?= $key ?>"></div>
									<div id="divdiskon<?= $key ?>"></div>
									<div id="divhoard<?= $key ?>"></div>
									<tr id="tableBonusProduk<?= $key ?>" style="display: none;">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>NAMA PRODUK</th>
														<th>BONUS</th>
													</tr>
												</thead>
												<tbody id="bodyBonusProduk<?= $key ?>">
												</tbody>
											</table>
										</td>
									</tr>
									<tr id="tableBonusPrice<?= $key ?>" style="display: none;">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>HARGA</th>
													</tr>
												</thead>
												<tbody id="bodyBonusPrice<?= $key ?>">
												</tbody>
											</table>
										</td>
									</tr>
									<tr id="tableBonusPersen<?= $key ?>" style="display: none;">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>Persen</th>
														<th>Potongan</th>
													</tr>
												</thead>
												<tbody id="bodyBonusPersen<?= $key ?>">
												</tbody>
											</table>
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
							<?php } ?>

						</tbody>
					</table>
					<div class="form-group">
						<label>Deposit </label>
						<input type="text" name="deposit2" id="deposit2" readonly class="form-control numeric" value="<?= $arraccount['account_deposit'] + $total - $total_sales ?>" required>
					</div>
					<div class="form-group">
						<label>Requested by</label>
						<div class="kt-radio-inline">
							<label class="kt-radio">
								<input type="radio" value="2" name="mistake_suspect_type" required> Sales
								<span></span>
							</label>
							<label class="kt-radio">
								<input type="radio" value="3" name="mistake_suspect_type"> Accounting
								<span></span>
							</label>
							<label class="kt-radio">
								<input type="radio" value="1" name="mistake_suspect_type"> Customer
								<span></span>
							</label>
						</div>
					</div>
					<div class="form-group">
						<label>Keterangan </label>
						<textarea class="form-control" name="mistake_note"></textarea>
					</div>
				</div>
				<!--<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Akun Tujuan
												</h3>  
											</div>
										</div>-->



				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
					<?php if($xo == 0){ ?>
						<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary" >Simpan</button>
					<?php }else{ ?>
					
						<?php if($cekTotalKirim == 0){ ?>
							<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary" style="display:none;">Simpan</button>
							<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
						<?php
						} else if (($arraccount['schema_id'] == 1 || $arraccount['schema_id'] == 4 || $arraccount['schema_id'] == 5 || $arraccount['schema_id'] == 6) && $cekRev == 0) { ?>
							<a onclick="get_discount()" id="cek_diskon"><span type="reset" class="btn btn-success">Cek Discount</span></a>
							<button style="display: none;" type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary" <?= (($total - $deposit) < 0) ? 'style="display:none;"' : ''; ?>>Simpan</button>
						<?php } else if ($cekTotalKirim > 0) { ?>
							<a onclick="get_discount()" id="cek_diskon"><span type="reset" class="btn btn-success">Cek Discount</span></a>
							<button style="display: none;" type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary" <?= (($total - $deposit) < 0) ? 'style="display:none;"' : ''; ?>>Simpan</button>
						<?php } else { ?>
							<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary" style="display:none;">Simpan</button>
							<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
						<?php } ?>
					<?php } ?>
					</div>
				</div>


			</div>
		</div>
	</form>
</div>

<script>
	var ii = 0;
	<?php
	if (@$arraccount_detail_sales != null) {
		foreach ($arraccount_detail_sales as $index => $arrvalue) { ?>

			function chk_total_<?= $index; ?>(val, ii) {


				var price = 0;
				var total_price = 0;
				var total_price2 = 0;
				var dex = "";
				var val_temp = "";
				$('.sum_total_' + <?= $index ?>).each(function() {

					val_temp = $(this).val();
					if (val_temp == "") {
						val_temp = 0;
					}
					price = $(this).attr("price") * parseFloat(val_temp);
					total_price = total_price + price;
					dex = $(this).attr("id_dex");
					debit = $(this).attr("id_debit");
					if (parseFloat(val_temp) > parseFloat($(this).attr("max"))) {
						$(this).val($(this).attr("max"));
						$('#' + dex).html('Rp ' + commafy($(this).attr("max") * $(this).attr("price")));
						$('#' + debit).val($(this).attr("max") * $(this).attr("price"));
						total_price2 += parseFloat($(this).attr("max") * $(this).attr("price"));

					} else {
						$('#' + dex).html('Rp ' + commafy(total_price));
						$('#' + debit).val(total_price);
						total_price2 += parseFloat(total_price);
					}
					total_price = "";
					dex = "";
					debit = "";

				});
				$('.sum_total_parsial_' + <?= $index ?>).each(function() {

					val_temp = $(this).val();
					if (val_temp == "") {
						val_temp = 0;
					}
					price = $(this).attr("price") * parseFloat(val_temp);
					total_price = total_price + price;
					dex = $(this).attr("id_dex");
					debit = $(this).attr("id_debit");
					if (parseFloat(val_temp) > parseFloat($(this).attr("max"))) {
						$(this).val($(this).attr("max"));
						$('#' + dex).html('Rp ' + commafy($(this).attr("max") * $(this).attr("price")));
						$('#' + debit).val($(this).attr("max") * $(this).attr("price"));
						total_price2 += parseFloat($(this).attr("max") * $(this).attr("price"));

					} else {
						$('#' + dex).html('Rp ' + commafy(total_price));
						$('#' + debit).val(total_price);
						total_price2 += parseFloat(total_price);
					}
					total_price = "";
					dex = "";
					debit = "";

				});
			}
		<?php } ?>
		ii++
	<?php } ?>

	function chk_total_bayar(key) {
		total = 0;
		$('#tableBonusProduk' + key).hide();
		$('#bodyBonusProduk' + key).html('');
		$('#tableBonusPrice' + key).hide();
		$('#bodyBonusPrice' + key).html('');
		$('#tableBonusPersen' + key).hide();
		$('#bodyBonusPersen' + key).html('');
		$('#divdiskon' + key).html('');
		$('#divlist_produk_sisa' + key).html('');
		$('#divhoard' + key).html('');
		var kode = [];
		var quantity = [];
		var price = [];
		var quantity_draft = [];
		var price_draft = [];
		$('.sum_total_' + key).each(function() {
			val = $('#' + $(this).attr('id_debit')).val();
			if (val == "") {
				val = 0;
			}
			total = parseFloat(total) + parseFloat(val);
			kode.push($(this).attr('id_product'));
			quantity.push($(this).val());
			quantity_draft.push($(this).attr('item_draft'));
			price_draft.push($(this).attr('total_draft'));
			price.push(val);
		});
		if (total >= 0) {
			$('#bayar' + key).val(commafy(total));
		}
		$('.sum_total_parsial_' + key).each(function() {
			val = $('#' + $(this).attr('id_debit')).val();
			if (val == "") {
				val = 0;
			}
			total = parseFloat(total) + parseFloat(val);
			kode.push($(this).attr('id_product'));
			quantity.push($(this).val());
			quantity_draft.push($(this).attr('item_draft'));
			price_draft.push($(this).attr('total_draft'));
			price.push(val);
		});

		$.ajax({
			type: 'post',
			url: '<?= base_url() ?>Test/cek_discount_bayar_revisi/' + key,
			data: {
				kode: kode,
				price: price,
				quantity: quantity,
				price_draft: price_draft,
				quantity_draft: quantity_draft,
			},
			success: function(result) {
				console.log(result)
				var result2 = JSON.parse(result);
				if (result2.list_diskon_quantity !== "") {
					$('#tableBonusProduk' + key).show();
					$('#bodyBonusProduk' + key).html(result2.list_diskon_quantity);
				}
				if (result2.list_diskon_harga !== "") {
					$('#tableBonusPrice' + key).show();
					$('#bodyBonusPrice' + key).html(result2.list_diskon_harga);
				}
				if (result2.list_diskon_persen !== "") {
					$('#tableBonusPersen' + key).show();
					$('#bodyBonusPersen' + key).html(result2.list_diskon_persen);
					$('#bodyBonusPersen' + key).append(result2.produk_persen);
				}
				if (result2.sisa_produk !== "") {
					$('#divlist_produk_sisa' + key).html(result2.sisa_produk);
				}
				if (result2.reward !== "") {
					$('#divdiskon' + key).html(result2.reward);
				}
				if (result2.tot_hoard !== "") {
					$('#divhoard' + key).html(result2.tot_hoard);
				}
				chk_total2();
			}
		});
	}

	function chk_total2() {

		var paid_all = 0;
		var val_x = "";
		$('.paid_total').each(function() {
			val_x = $(this).val();
			val_x = val_x.replaceAll(",", "");
			if (val_x == "") val_x = 0;
			paid_all += parseFloat(val_x);
		});
		$('#paid').val(commafy(paid_all));
		var paid = $('#paid').val();
		paid = paid.replaceAll(",", "");
		var sum = 0;
		var nilai_awal = 0;
		$('.sum_total').each(function() {
			nilai_awal = $(this).val();
			if (parseFloat(nilai_awal.replaceAll(",", "")) > parseFloat($(this).attr("max"))) {
				$(this).val(commafy($(this).attr("max")));
				sum += parseFloat($(this).attr("max"));
			} else {
				sum += parseFloat(nilai_awal.replaceAll(",", ""));
			}
			//if(parseFloat(this.value) > 0)(

			//}
		});
		var sum2 = 0;
		$('.price_deduction').each(function() {
			sum2 += parseFloat($(this).val());
			//if(parseFloat(this.value) > 0)(

			//}
		});
		//diskon = parseFloat($('#diskon').val());
		deposit = $('#deposit').val();
		deposit = deposit.replaceAll(',', '');
		$('#deposit2').val(commafy(parseFloat(deposit) + parseFloat(paid - sum + sum2)));
		if ((parseFloat(deposit) + parseFloat(paid - sum + sum2)) < 0) {
			$('#simpan').hide();
		} else {
			$('#simpan').show();
		}


	}

	it = 0;

	function myCreateFunction() {
		it++;
		var html = '<tr id="' + it + '"><td><select class="form-control" id="exampleSelect1" name="account_id[' + it + ']" required><option value="">Pilih Akun</option><?php foreach ($m_account as $data) {
																																											echo "<option value=\'" . $data->account_id . "\'>" . $data->account_name . "</option>";
																																										} ?></select></td><td><input type="text" class="form-control" placeholder="Nama Pengirim" name="from[account_detail_transfer_name][' + it + ']"></td><td><input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="from[account_detail_date][' + it + ']" required></td><td><input type="text" class="form-control numeric paid_total" placeholder="Nominal" name="from[account_detail_credit][' + it + ']" value="0" required onkeyup="chk_total()"></td><td><input type="file" class="form-control" name="transfer_' + it + '"></td><td><button onclick="myDeleteFunction(' + it + ')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$(".numeric").mask("#,##0", {
			reverse: true
		});
		$('.date_picker, #kt_datepicker_1_validate').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			format: "yyyy-mm-dd"
		});
	}

	function myDeleteFunction(id) {
		$('#' + id).remove();
	}

	function chk_total(val) {
		$('#simpan').hide();
		$('#cek_diskon').show();
		var paid_all = 0;
		var val_x = "";
		$('.paid_total').each(function() {
			val_x = $(this).val();
			val_x = val_x.replaceAll(",", "");
			if (val_x == "") val_x = 0;
			paid_all += parseFloat(val_x);
		});
		$('#paid').val(commafy(paid_all));
		var paid = $('#paid').val();
		paid = paid.replaceAll(",", "");
		var sum = 0;
		var nilai_awal = 0;
		$('.sum_total').each(function() {
			nilai_awal = $(this).val();
			if (parseFloat(nilai_awal.replaceAll(",", "")) > parseFloat($(this).attr("max"))) {
				$(this).val(commafy($(this).attr("max")));
				sum += parseFloat($(this).attr("max"));
			} else {
				sum += parseFloat(nilai_awal.replaceAll(",", ""));
			}
			//if(parseFloat(this.value) > 0)(

			//}
		});
		deposit = $('#deposit').val();
		deposit = deposit.replaceAll(',', '');
		$('#deposit2').val(commafy(parseFloat(deposit) + parseFloat(paid - sum)));
		<?php if ($arraccount['schema_id'] > 1 || $cekRev == 1) { ?>
			deposit = $('#deposit2').val().replaceAll(",", "");
			if (deposit < 0) {
				$('#simpan').hide();
			} else {
				$('#simpan').show();
			}
		<?php } ?>
	}

	function get_discount() {
		$('.sum_total').each(function() {
			chk_total_bayar($(this).attr('key'))
		})
		$('#cek_diskon').hide();
	}
</script>