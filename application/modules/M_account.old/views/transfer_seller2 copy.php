<div class="row">
	<form method="post" action="<?= site_url($url . '/set_transfer_seller2'); ?>" enctype="multipart/form-data">
		<div class="col-lg-12 col-xl-12">
			<div class="kt-portlet">

				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Riwayat Transfer <br>
							(<?= $arraccount['nama']; ?> : <?= $arraccount['kode']; ?>)
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">

					<div class="form-group">
						<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
							<thead>
								<tr>
									<th>No</th>
									<th>Tanggal</th>
									<th>Jumlah Transfer</th>
									<th>Rekening Tujuan</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 col-xl-12">
			<div class="kt-portlet">

				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Alamat Kirim
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">

					<div class="form-group row">
						<div class="col-lg-12">
							<label>Ekspedisi:</label>
							<select class="form-control kt-select2 select_ekspedisi" name="input2[ekspedisi]">
								<option value=""></option>
								<?php foreach ($deliveries as $delivery) { ?>
									<option value="<?= $delivery['delivery_instance_id'] ?>"><?= $delivery['delivery_instance_name'] ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-4">
							<label>Provinsi:</label>
							<select class="form-control kt-select2 select_provinsi" name="input2[provinsi]" id="select_provinsi" onchange="get_kota()" required>
								<?php foreach ($provinsis as $provinsi) { ?>
									<option value="<?= $provinsi['REGION_ID'] ?>"><?= $provinsi['NAME'] ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-lg-4">
							<label class="">Kota/Kabupaten:</label>
							<select class="form-control kt-select2 select_kota" id="select_kota" onchange="get_kecamatan()" name="input2[kota]" required>

							</select>
						</div>
						<div class="col-lg-4">
							<label>Kecamatan:</label>
							<select class="form-control kt-select2 select_kecamatan" id="select_kecamatan" name="input2[kecamatan]" required>

							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<label>Alamat Lengkap:</label>
							<textarea class="form-control" name="input2[full_address]"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>



		<script type="text/javascript">
			$(document).ready(function() {
				$('#kt_table_1').DataTable({
					"pagingType": "full_numbers",
					scrollY: "300px",
					scrollX: true,
					scrollCollapse: true,
					"processing": true,
					"serverSide": true,
					"ajax": "<?= site_url() ?>/<?= $url ?>/get_data_transfer/<?= $id ?>"
				});
			});
		</script>
		<div class="col-lg-12 col-xl-12">
			<div class="kt-portlet">

				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Form Transfer
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Rekening Tujuan</th>
								<th>Nama Pengirim</th>
								<th>Tanggal</th>
								<th>Nominal</th>
								<th>Bukti Transfer</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="x">

							<tr id="0">
								<td><select class="form-control" id="exampleSelect1" name="account_id[0]" required>
										<option value="">Pilih Akun</option>
										<?php
										foreach ($m_account as $data) {
											echo "<option value='" . $data->account_id . "'" . (($data->account_id == $role_id) ? 'selected' : '') . ">" . $data->account_name . "</option>";
										}
										?>
									</select></td>

								<td><input type="text" class="form-control" placeholder="Nama Pengirim" name="from[account_detail_transfer_name][0]"></td>
								<td><input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="from[account_detail_date][0]" required></td>

								<td><input type="text" class="form-control numeric paid_total" placeholder="Nominal" name="from[account_detail_credit][0]" value="0" required onkeyup="chk_total()"></td>
								<td><input type="file" class="form-control" name="transfer_0"></td>
								<td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
							</tr>
						</tbody>
					</table>
					<div class="kt-form__actions">
						<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Transaksi</button>
					</div>


					<br>
					<br>
					<div class="form-group">
						<label>Nominal</label>
						<input type="text" class="form-control numeric" placeholder="Nominal" name="nominal" value="0" id="paid" required onkeyup="chk_total()" readonly>
					</div>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>No</th>
								<th>KODE SO</th>
								<th>TANGGAL</th>
								<th>TOTAL TAGIAN</th>
								<th>BAYAR</th>
								<th>KURANG BAYAR</th>
								<th>BAYAR</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no_header = 0;
							$no = 0;
							$i = 0;
							foreach ($arrlist_account_sales as $key => $vaData) {

							?>
								<tr>
									<td><?= ++$no_header ?></td>
									<td><?= $vaData['sales_code'] ?></td>
									<td><?= $vaData['sales_date'] ?></td>
									<td>Rp <?= number_format($vaData['account_detail_debit']) ?></td>
									<td>Rp <?= number_format($vaData['account_detail_paid']) ?></td>
									<td><input type="hidden" name="sales_type[<?= $vaData['account_detail_real_id'] ?>]" value="<?= $vaData['sales_type'] ?>" />Rp <?= number_format($vaData['account_detail_debit'] - $vaData['account_detail_paid']) ?></td>
									<td><input id="bayar<?= $key ?>" type="text" <?= $arraccount['schema_id'] == 1 ? 'readonly' : '' ?> name="paid[<?= $vaData['account_detail_real_id'] ?>]" max="<?= $vaData['account_detail_debit'] - $vaData['account_detail_paid'] ?>" class="form-control md-static sum_total numeric" value="0" onchange="chk_total()"><input type="hidden" name="sales[<?= $vaData['account_detail_real_id'] ?>]" value="<?= $vaData['sales_id']; ?>"></td>
									<input type="hidden" name="sales_id_all[<?= $vaData['sales_id']; ?>]" value="<?= $vaData['sales_id']; ?>">
								</tr>
								<?php if ($arraccount['schema_id'] == 1) { ?>
									<tr id="<?= $key ?>x">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>NAMA PRODUK</th>
														<th>JUMLAH PESANAN</th>
														<th>KURANG BOLEH KIRIM</th>
														<th>BOLEH KIRIM</th>
														<th>JUMLAH KIRIM</th>
														<th>TOTAL</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$no = 0;
													if (@$arraccount_detail_sales != null) {
														foreach (@$arraccount_detail_sales[$key] as $key2 => $vaData2) {
													?>
															<tr>
																<td><?= ++$no ?></td>
																<td><?= $vaData2['nama_produk'] ?> (@Rp <?= number_format($vaData2['sales_detail_price']) ?>)</td>
																<td><?= $vaData2['sales_detail_quantity'] ?></td>
																<td><?= $vaData2['already'] ?></td>
																<td><?= $vaData2['sales_detail_quantity'] - $vaData2['already'] ?></td>
																<td>
																	<input type="text" name="allow[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" max="<?= $vaData2['sales_detail_quantity'] - $vaData2['already'] ?>" class="form-control md-static sum_total_<?= $key ?> input_produk" id_dex="<?= $key ?>D<?= $key2 ?>" id_debit="<?= $key ?>debit<?= $key2 ?>" price="<?= $vaData2['sales_detail_price'] ?>" value="0" onkeyup="chk_total_<?= $key ?>(<?= $vaData2['sales_type'] ?>, <?= $i ?>); chk_total_bayar(<?= $key ?>);chk_total();" id_product="<?= $vaData2['product_id'] ?>" required>
																	<input type="hidden" name="product_id[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" value="<?= $vaData2['product_id'] ?>">
																	<!--<input type="hidden" name="ids[<?= $vaData['account_detail_real_id'] ?>][<?= $key2 ?>]" value="<?= $vaData2['account_detail_sales_id'] ?>">-->
																</td>
																<td>
																	<div id="<?= $key ?>D<?= $key2 ?>"></div>
																	<input class="total_bayar_<?= $key ?>" type="hidden" id="<?= $key ?>debit<?= $key2 ?>" />
																</td>
															</tr>
													<?php }

														$i++;
													} ?>

												</tbody>
											</table>
										</td>
									</tr>
								<?php } ?>
							<?php } ?>

						</tbody>
					</table>
					<div class="form-group">
						<label>Deposit</label>
						<input type="text" name="deposit" id="deposit" readonly class="form-control numeric" value="0" required>
					</div>
					<div class="kt-form__actions">
						<button type="button" class="btn btn-warning" onclick="addTableDropship();">Tambah Dropship</button>
					</div>
					<div class="form-group" id="input_product_card">
					</div>
					<div class="form-group" id="input_product_total">
					</div>
					<div id="cardDropship" class="form-group">
					</div>
				</div>



				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>


			</div>
		</div>
	</form>
</div>

<script>
	var ii = 0;
	let fruits = [];
	var index_drop = 0;

	<?php
	if (@$arraccount_detail_sales != null) {
		foreach ($arraccount_detail_sales as $index => $arrvalue) { ?>

			function chk_total_<?= $index; ?>(val, ii) {
				var price = 0;
				var total_price = 0;
				var total_price2 = 0;
				var dex = "";
				var val_temp = "";
				$('.sum_total_' + <?= $index ?>).each(function() {

					val_temp = $(this).val();
					if (val_temp == "") {
						val_temp = 0;
					}
					price = $(this).attr("price") * parseFloat(val_temp);
					total_price = total_price + price;
					dex = $(this).attr("id_dex");
					debit = $(this).attr("id_debit");
					if (parseFloat(val_temp) > parseFloat($(this).attr("max"))) {
						$(this).val($(this).attr("max"));
						$('#' + dex).html('Rp ' + commafy($(this).attr("max") * $(this).attr("price")));
						$('#' + debit).val($(this).attr("max") * $(this).attr("price"));
						total_price2 += parseFloat($(this).attr("max") * $(this).attr("price"));

					} else {
						$('#' + dex).html('Rp ' + commafy(total_price));
						$('#' + debit).val(total_price);
						total_price2 += parseFloat(total_price);
					}
					total_price = "";
					dex = "";
					debit = "";

				});
			}
		<?php } ?>
		ii++
	<?php } ?>
	it = 0;
	var all_product = '';
	var option_product = '';

	function chk_total_bayar(key) {
		total = 0;
		$('.total_bayar_' + key).each(function() {
			val = $(this).val();
			if (val == "") {
				val = 0;
			}
			total = parseFloat(total) + parseFloat(val);
		});
		$('#bayar' + key).val(commafy(total));
	}

	function addTableDropship() {
		fruits.push([index_drop]);
		var html = '<div id="drop' + index_drop + '"><div class="form-group row"><div class="col-lg-4"><label>Nama:</label><input class="form-control" name="dropship[name][' + index_drop + ']" type="text" /></div><div class="col-lg-4" id="produk' + index_drop + '"><label>Produk:</label><div class="input-group" id="prodgrup' + index_drop + '0"> <select id="arrproduk' + index_drop + '0" name="dropship[produk][' + index_drop + '][0]" class="form-control md-static product_dropship" index="' + index_drop + '0" required></select><div class="input-group-append"> <a class="input-group-text" onclick="plusProduk(' + index_drop + ');" style="cursor: pointer"><i class="la la-plus"></i></a> </div></div></div><div class="col-lg-4" id="jumlah' + index_drop + '"><label>Jumlah:</label><input id="jumlah' + index_drop + '0" type="text" onkeyup="checkMaxDrop(' + index_drop + ', 0)" name="dropship[jumlah][' + index_drop + '][0]" class="form-control jumlah_produk md-static" required /></div></div><div class="form-group row"><div class="col-lg-3"><label>Ekspedisi:</label><select class="form-control kt-select2 select_ekspedisi" name="dropship[ekspedisi][' + index_drop + ']"><option value=""></option><?php foreach ($deliveries as $delivery) { ?><option value="<?= $delivery['delivery_instance_id'] ?>"><?= $delivery['delivery_instance_name'] ?></option><?php } ?></select></div><div class="col-lg-3"><label>Provinsi:</label><select class="form-control kt-select2 select_provinsi" name="dropship[provinsi][' + index_drop + ']" id="select_provinsi' + index_drop + '" onchange="get_kota_dropship(' + index_drop + ')" required><?php foreach ($provinsis as $provinsi) { ?><option value="<?= $provinsi['REGION_ID'] ?>"><?= $provinsi['NAME'] ?></option><?php } ?></select></div><div class="col-lg-3"><label class="">Kota/Kabupaten:</label><select class="form-control kt-select2 select_kota" id="select_kota' + index_drop + '" onchange="get_kecamatan_dropship(' + index_drop + ')" name="dropship[kota][' + index_drop + ']" required></select></div><div class="col-lg-3"><label>Kecamatan:</label><select class="form-control kt-select2 select_kecamatan" id="select_kecamatan' + index_drop + '" name="dropship[kecamatan][' + index_drop + ']" required></select></div></div><div class="form-group row"><div class="col-lg-12"><label>Alamat Lengkap:</label><textarea class="form-control" name="dropship[full_address][' + index_drop + ']"></textarea></div></div><div class="form-group row"><div class="col-lg-6 kt-align-right"><button onclick="deleteDrop(' + index_drop + ');" class="btn btn-danger"><i class="fas fa-trash"></i></button></div></div></div>';
		$('#cardDropship').append(html);
		index_drop = index_drop + 1;
		get_hidden_produk();
	}

	function deleteDrop(i) {
		$('#drop' + i).remove();
	}

	function myCreateFunction() {
		it++;
		var html = '<tr id="' + it + '"><td><select class="form-control" id="exampleSelect1" name="account_id[' + it + ']" required><option value="">Pilih Akun</option><?php foreach ($m_account as $data) {
																																											echo "<option value=\'" . $data->account_id . "\'>" . $data->account_name . "</option>";
																																										} ?></select></td><td><input type="text" class="form-control" placeholder="Nama Pengirim" name="from[account_detail_transfer_name][' + it + ']"></td><td><input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="from[account_detail_date][' + it + ']" required></td><td><input type="text" class="form-control numeric paid_total" placeholder="Nominal" name="from[account_detail_credit][' + it + ']" value="0" required onkeyup="chk_total()"></td><td><input type="file" class="form-control" name="transfer_' + it + '"></td><td><button onclick="myDeleteFunction(' + it + ')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$(".numeric").mask("#,##0", {
			reverse: true
		});
		$('.date_picker, #kt_datepicker_1_validate').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			format: "yyyy-mm-dd"
		});
	}

	function myDeleteFunction(id) {
		$('#' + id).remove();
	}

	function get_kota() {
		var val = $('#select_provinsi option:selected').val();

		$.ajax({
			url: '<?= base_url() ?>M_account/get_kota/' + val.substring(0, 2),
			success: function(result) {
				$('#select_kota').html(result);
			}
		});
		$('#select_kecamatan').html('');
	}

	function get_kota_dropship(id) {
		var val = $('#select_provinsi' + id + ' option:selected').val();

		$.ajax({
			url: '<?= base_url() ?>M_account/get_kota/' + val.substring(0, 2),
			success: function(result) {
				$('#select_kota' + id).html(result);
			}
		});
		$('#select_kecamatan' + id).html('');
	}

	function get_kecamatan() {
		var val = $('#select_kota option:selected').val();
		$.ajax({
			url: '<?= base_url() ?>M_account/get_kecamatan/' + val.substring(0, 4),
			success: function(result) {
				$('#select_kecamatan').html(result);
			}
		});
	}

	function get_kecamatan_dropship(id) {
		var val = $('#select_kota' + id + ' option:selected').val();
		$.ajax({
			url: '<?= base_url() ?>M_account/get_kecamatan/' + val.substring(0, 4),
			success: function(result) {
				$('#select_kecamatan' + id).html(result);
			}
		});
	}

	function chk_total(val) {
		var paid_all = 0;
		var val_x = "";
		$('.paid_total').each(function() {
			val_x = $(this).val();
			val_x = val_x.replaceAll(",", "");
			if (val_x == "") val_x = 0;
			paid_all += parseFloat(val_x);
		});
		$('#paid').val(commafy(paid_all));
		var paid = $('#paid').val();
		paid = paid.replaceAll(",", "");
		var sum = 0;
		var nilai_awal = 0;
		$('.sum_total').each(function() {
			nilai_awal = $(this).val();
			if (parseFloat(nilai_awal.replaceAll(",", "")) > parseFloat($(this).attr("max"))) {
				$(this).val(commafy($(this).attr("max")));
				sum += parseFloat($(this).attr("max"));
			} else {
				sum += parseFloat(nilai_awal.replaceAll(",", ""));
			}
			//if(parseFloat(this.value) > 0)(

			//}
		});
		$('#deposit').val(commafy(paid - sum));
		if ((paid - sum) < 0) {
			$('#simpan').hide();
		} else {
			$('#simpan').show();
		}
		get_hidden_produk();
	}

	function get_hidden_produk() {
		var html = '';
		all_product = '';
		let arrproduct = [];
		$('.input_produk').each(function() {
			if ($(this).val() > 0) {
				var val = $(this).val();
				var id_product = $(this).attr('id_product');
				if (typeof arrproduct[id_product] === 'undefined') {
					arrproduct.push([id_product]);
					arrproduct[id_product] = 0;
				}
				arrproduct[id_product] += parseFloat(val);
				html += '<input type="hidden" class="hidden_id_product product' + id_product + '" value="' + val + '" id_produk="' + id_product + '"/>';
				if (all_product == '') {
					all_product += id_product;
				} else {
					all_product += ' ' + id_product;
				}
			}
		})
		$('#input_product_total').html('');
		arrproduct.forEach(injectProduct);
		$('#input_product_card').html(html);
		$.ajax({
			url: '<?= base_url() ?>M_account/get_option_product/' + all_product,
			success: function(result) {
				option_product = result;
			}
		});
		$('.product_dropship').html(option_product);
		$('.jumlah_produk').val(0);
	}

	function injectProduct(item, index) {
		html = '<input type="hidden" name="total_product[' + index + ']" value="' + item + '"/>';
		$('#input_product_total').append(html);
	}

	function checkMaxDrop(id1, id2) {
		var product = $('#arrproduk' + id1 + '' + id2).val();
		var jumlah = $('#jumlah' + id1 + '' + id2).val();
		var jumlahAll = 0;
		var jumlahPakai = 0;
		$('.product' + product).each(function() {
			jumlahAll += parseFloat($(this).val());
		});
		$('.product_dropship').each(function() {
			if ($(this).val() == product && $(this).attr('index') != id1 + '' + id2) {
				jumlahPakai += parseFloat($('#jumlah' + $(this).attr('index')).val());
			}
		})
		totalJumlah = (parseFloat(jumlahPakai) + parseFloat(jumlah));
		if (totalJumlah > jumlahAll) {
			$('#jumlah' + id1 + '' + id2).val(jumlahAll - parseFloat(jumlahPakai));
		}
	}

	function plusProduk(id1) {
		id2 = ++fruits[id1];
		var html1 = '<div class="input-group" id="prodgrup' + id1 + '' + id2 + '"> <select id="arrproduk' + id1 + '' + id2 + '" name="dropship[produk][' + id1 + '][' + id2 + ']" class="form-control md-static product_dropship" index="' + id1 + '' + id2 + '" required></select><div class="input-group-append"> <button class="input-group-text" onclick="plusProduk(' + id1 + ');"><i class="la la-plus"></i></button> <button class="input-group-text" onclick="minusProduk(' + id1 + ',' + id2 + ');"><i class="la la-minus"></i></button></div></div>';
		var html2 = '<input id="jumlah' + id1 + '' + id2 + '" type="text" onkeyup="checkMaxDrop(' + id1 + ', ' + id2 + ')" name="dropship[jumlah][' + id1 + '][' + id2 + ']" class="form-control jumlah_produk md-static" required />';
		$('#produk' + id1).append(html1);
		$('#jumlah' + id1).append(html2);
		get_hidden_produk();
	}

	function minusProduk(i, y) {
		$('#prodgrup' + i + '' + y).remove();
		$('#jumlah' + i + '' + y).remove();
	}

	$('.select_provinsi').select2({
		allowClear: true,
		placeholder: 'Pilih Provinsi',
	});
	$('.select_ekspedisi').select2({
		allowClear: true,
		placeholder: 'Pilih Ekspedisi',
	});
	$('.select_kota').select2({
		allowClear: true,
		placeholder: 'Pilih Kota/kabupaten',
	});
	$('.select_kecamatan').select2({
		allowClear: true,
		placeholder: 'Pilih Kota/kecamatan',
	});
</script>