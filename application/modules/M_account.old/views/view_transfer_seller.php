<div class="kt-portlet kt-portlet--mobile">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <!--<span class="kt-portlet__head-icon"><i class="kt-font-brand flaticon2-line-chart"></i></span>-->
      <h3 class="kt-portlet__head-title">
        <table>
          <br>
          <tr>
            <td>Rekening Tujuan</td>
            <td>&nbsp;&nbsp;:</td>
            <td>&nbsp;&nbsp; <?= $arraccount_detail['nama_rek_tujuan'] ?> </td>
          </tr>
          <tr>
            <td>Tanggal</td>
            <td>&nbsp;&nbsp;:</td>
            <td>&nbsp;&nbsp; <?= $arraccount_detail['account_detail_date'] ?> </td>
          </tr>
          <tr>
            <td>Total Transfer</td>
            <td>&nbsp;&nbsp;:</td>
            <td>&nbsp;&nbsp;Rp <?= number_format($arraccount_detail['account_detail_credit']) ?> </td>
          </tr> <?php if($chk_account_detail == ""){ ?> <tr>
            <td>
              <a href="<?= site_url($url . '/form_sales_allow/'.$id); ?>">EDIT </a>
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
          </tr> <?php } ?>
        </table>
        <br>
      </h3>
    </div>
  </div>
  <div class="kt-portlet__body">
    <!--begin: Datatable -->
	<?php if (count($arraccount_detail_sales_product) > 0) {
		foreach($arraccount_detail_sales_product as $index => $arrvalue){ ?>
		<?php if($arraccount_detail_sales[$index]['account_detail_sales_amount'] > 0) : ?>
	<div class="kt-portlet__head">
      <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">
          <table>
            <tr>
              <td>Sales Order</td>
              <td>&nbsp;&nbsp;:</td>
              <td>&nbsp;&nbsp; <a href="
											<?= site_url('T_sales/table_detail/'.$index) ?>"> <?= $arraccount_detail_sales[$index]['sales_code']; ?> </a>
              </td>
            </tr>
            <tr>
              <td>Jumlah Bayar</td>
              <td>&nbsp;&nbsp;:</td>
              <td>&nbsp;&nbsp;Rp <?= number_format($arraccount_detail_sales[$index]['account_detail_sales_amount']); ?> </td>
            </tr>
          </table>
          <br>
        </h3>
      </div>
    </div>
    <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
      <thead>
        <tr>
          <th>No</th>
          <th>NAMA PRODUK</th>
          <th>JUMLAH PESANAN</th>
          <th>JUMLAH BOLEH KIRIM</th>
          <th>HARGA</th>
          <th>SUBTOTAL</th>
        </tr>
      </thead>
      <tbody>
	<?php
		$no = 0;
		foreach ($arrvalue as $key => $vaData) {
			if ($vaData['account_detail_sales_product_allow'] > 0) :
	?> <tr>
          <td> <?= ++$no ?> </td>
          <td> <?= $vaData['nama_produk'] ?> </td>
          <td> <?= $vaData['sales_detail_quantity'] ?> </td>
          <td> <?= $vaData['account_detail_sales_product_allow'] ?> </td>
          <td> <?= 'Rp ' . number_format($vaData['sales_detail_price']) ?> </td>
          <td> <?= 'Rp ' . number_format($vaData['sales_detail_price'] * $vaData['account_detail_sales_product_allow']) ?> </td>
        </tr
		 <?php endif; ?>
		<?php } ?>
	</tbody>
    </table>
	<?php endif; ?><?php } } ?>
  </div>
</div>