<?php 
$cDekDiskon = $this->db->query("SELECT * FROM m_discount WHERE discount_id = '".$id_diskon."' ");

foreach ($cDekDiskon->result_array() as $key => $vaCekDiskon) {
	$cCekTipeDiskon = $vaCekDiskon['discount_type'];
	$cCekKategoriDiskon = $vaCekDiskon['discount_category'];
	$cCekNamaDiskon     = $vaCekDiskon['discount_name'];
}
if($cCekTipeDiskon == 1){
?>



<?php 
	
	$jumlahTerm = $this->db->query("SELECT * FROM v_diskon_terms_produk_arga where discount_id = '".$id_diskon."' ")->num_rows();
	

	$dbQuery = $this->db->query("
			SELECT
	A.account_id,
	B.seller_id,
	C.product_global_id,
	C.product_global_name as nama_produk,
	C.sales_detail_quantity,
	C.sales_id ,m.discount_terms_product_quantity,m.discount_name,m.discount_type,m.discount_category,m.discount_name

FROM
	m_account A,t_account_detail ma,t_sales B,t_sales_detail C,v_diskon_terms_produk_arga m 
	
	WHERE  B.seller_id = A.seller_id and
	       C.sales_id = B.sales_id and
				 A.account_id = ma.account_id and 
				 ma.sales_id = B.sales_id and 
				 C.sales_detail_quantity >= m.discount_terms_product_quantity
				 and m.discount_id = '".$id_diskon."'
				 and A.account_id = '".$id_akun."' and C.sales_id  = '".$id_sales."'
		 GROUP BY C.sales_id,C.product_global_name	
		");
?>

<?php 
	if($dbQuery->num_rows() == $jumlahTerm){
?>
<center><h4>SELAMAT! Syarat Diskon Terpenuhi</h4></center>
<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
	<thead>
		<tr>
			<th>NAMA PRODUK TERDISKON</th>
			<th>JUMLAH SYARAT DISKON</th>
			<th>JUMLAH PEMESANAN</th>
			
		</tr>	
	</thead>
	<tbody>
		<?php 
		foreach ($dbQuery->result_array() as $key => $vaData) {
			# code...
		
		?>
		<tr>
			<td><?=$vaData['nama_produk']?></td>
			<td><?=$vaData['discount_terms_product_quantity']?></td>
			<td><?=$vaData['sales_detail_quantity']?></td>
		</tr>
	<?php } ?>
	</tbody>
	<tfoot>
		<?php
		$dbRewardIdentity = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$id_diskon."' LIMIT 1  ");
		foreach ($dbRewardIdentity->result_array()  as $key => $vaIdentityReward) {
				$cNamaDiskon	=	$vaIdentityReward['discount_name'];
				$cKategoriDiskon=	$vaIdentityReward['kategori_diskon'];
				$cTipeDiskon	=	$vaIdentityReward['discount_type'];
				$cIdKategoriDiskon	=	$vaIdentityReward['discount_category'];
			}
		?>

	<?php 

	if($vaData['discount_type'] == 1 and $vaData['discount_category'] == 2){

	?>

	<tr>
		<td colspan="3"><strong>Diskon & Promo Berupa</strong></td>
	</tr>
	
	<tr>
		<td>Diskon Persentase</td>
		<?php 
			
			$dbRewardPercentage = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$id_diskon."' LIMIT 1  ");
			

			foreach ($dbRewardPercentage->result_array() as $key => $vaRewardPercentage) {
	?>	
		<td colspan="2"><?=$vaRewardPercentage['discount_reward_product_percentage']?> % </td>
	<?php } ?>
	</tr>
	<tr>
		<td>Free Produk</td>
		<?php 
			$dbRewardProduk = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$id_diskon."'  ");
		?>	
		<td colspan="2">
			<table class="table">
				<thead>
					<tr>
						<th>Nama Produk</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					foreach ($dbRewardProduk->result_array() as $key => $vaRewardProduk) {
					?>
					<tr>
						<td><?=$vaRewardProduk['nama_produk']?></td>
						<td><?=$vaRewardProduk['discount_reward_product_quantity']?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="hidden" id="id_diskon" value="<?=$id_diskon?>">
			<input type="hidden" id="id_akun" value="<?=$id_akun?>">
			<input type="hidden" id="id_sales" value="<?=$id_sales?>">
		


			<input type="hidden" id="tipe_diskon" value="<?=$cTipeDiskon?>">
			<input type="hidden" id="kategori_diskon" value="<?=$cIdKategoriDiskon?>">

			<button type="button" class="btn btn-primary btn-block" onclick="return klaimDiskonPembelianPerPCS();">
				<i class="fa fa-tags"></i> Klaim Diskon <?=$cNamaDiskon?> - <?=$cKategoriDiskon?>
			</button>

			<script type="text/javascript">
				function getTableDiskon(id_akun){
					$.ajax({
						type: "GET",
						url: '<?= base_url() ?>M_account/get_apply_diskon/'+id_akun,
						cache: false,
						success:function(msg){
						  $('#table_apply_diskon').html(msg)
						}
					});
				}

				function klaimDiskonPembelianPerPCS(){
					var id_diskon		= $('#id_diskon').val();
					var id_akun			= $('#id_akun').val();
					var id_sales		= $('#id_sales').val();
					var tipe_diskon		= $('#tipe_diskon').val();
					var kategori_diskon	= $('#kategori_diskon').val();

					var tanya = confirm("Apakah Yakin Klaim Diskon ?");

					if (tanya == true) {
						$.ajax({
							data  :"id_diskon="+id_diskon+
							"&id_akun="+id_akun+
							"&id_sales="+id_sales+
							"&tipe_diskon="+tipe_diskon+
							"&kategori_diskon="+kategori_diskon,
							type: "POST",
							url: '<?= base_url() ?>M_account/simpan_diskon/',
							cache: false,
							success:function(msg){

								if(msg == 2){
									getTableDiskon(id_akun);
									window.location.hash = '#label_diskon';
									document.getElementById('label_diskon').scrollIntoView();
								}else{
									alert("Diskon Ini sudah di Apply.");
								}
								
							}
						});
					}else{
						return false;
					}
				}
			</script>
		</td>
	</tr>

	<?php }if($vaData['discount_type'] == 1 and $vaData['discount_category'] == 1){ ?>
	<tr>
		<td colspan="3"><strong>Diskon & Promo Berupa</strong></td>
	</tr>
	<tr>
		<td>Free Produk</td>
		<?php 
			$dbRewardProduk = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$id_diskon."'  ");
		?>	
		<td colspan="2">
			<table class="table">
				<thead>
					<tr>
						<th>Nama Produk</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					foreach ($dbRewardProduk->result_array() as $key => $vaRewardProduk) {
					?>
					<tr>
						<td><?=$vaRewardProduk['nama_produk']?></td>
						<td><?=$vaRewardProduk['discount_reward_product_quantity']?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="hidden" id="id_diskon" value="<?=$id_diskon?>">
			<input type="hidden" id="id_akun" value="<?=$id_akun?>">
			<input type="hidden" id="id_sales" value="<?=$id_sales?>">
			<input type="hidden" id="id_akun" value="<?=$id_akun?>">
			<input type="hidden" id="id_akun" value="<?=$id_akun?>">

			<input type="hidden" id="tipe_diskon" value="<?=$cTipeDiskon?>">
			<input type="hidden" id="kategori_diskon" value="<?=$cIdKategoriDiskon?>">
			<button type="button" class="btn btn-primary btn-block" onclick="return klaimDiskonPembelianKelipatan();">
				<i class="fa fa-tags"></i> Klaim Diskon <?=$cNamaDiskon?> - <?=$cKategoriDiskon?>
			</button>

			<script type="text/javascript">
				function getTableDiskon(id_akun){
					$.ajax({
						type: "GET",
						url: '<?= base_url() ?>M_account/get_apply_diskon/'+id_akun,
						cache: false,
						success:function(msg){
						  $('#table_apply_diskon').html(msg)
						}
					});
				}
				function klaimDiskonPembelianKelipatan(){
					var id_diskon		= $('#id_diskon').val();
					var id_akun			= $('#id_akun').val();
					var id_sales		= $('#id_sales').val();
					var tipe_diskon		= $('#tipe_diskon').val();
					var kategori_diskon	= $('#kategori_diskon').val();

					var tanya = confirm("Apakah Yakin Klaim Diskon ?");

					if (tanya == true) {
						$.ajax({
							data  :"id_diskon="+id_diskon+
							"&id_akun="+id_akun+
							"&id_sales="+id_sales+
							"&tipe_diskon="+tipe_diskon+
							"&kategori_diskon="+kategori_diskon,
							type: "POST",
							url: '<?= base_url() ?>M_account/simpan_diskon/',
							cache: false,
							success:function(msg){
								if(msg == 2){
									getTableDiskon(id_akun);
									window.location.hash = '#label_diskon';
								}else{
									alert("Diskon Ini sudah di Apply.");
								}

								
							}
						});
					}else{
						return false;
					}

					
				}
			</script>
		</td>
	</tr>
	<?php } ?>
	
	</tfoot>

<table>
<?php }else{ ?>
	<center><h4>Mohon Maaf Transaksi Anda tidak memenuhi syarat mendapatkan Diskon, Berikut List Produk yang perlu di tambahkan untuk mendapatkan diskon</h4></center>
	<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
	<thead>
		<tr>
			<th>NAMA PRODUK TERDISKON</th>
			<th>JUMLAH SYARAT DISKON</th>
			
		</tr>	
	</thead>
	<tbody>
		<?php 
		$dbTerm = $this->db->query("SELECT m.nama_produk,m.discount_terms_product_quantity 
			FROM v_diskon_terms_produk_arga m
			WHERE m.discount_id = '".$id_diskon."' and  m.product_global_id NOT IN (SELECT c.product_global_id FROM t_sales_detail c WHERE  c.sales_id = '".$id_sales."' and c.sales_detail_quantity >= m.discount_terms_product_quantity)");
		foreach ($dbTerm->result_array() as $key => $vaData2) {
			# code...
		
		?>
		<tr>
			<td><?=$vaData2['nama_produk']?></td>
			<td><?=$vaData2['discount_terms_product_quantity']?></td>
		</tr>
	<?php } ?>
	</tbody>
	
<table>
<?php  } ?>


<?php }else{ ?>
	
	<?php 
	$dbQueryTieringHeader = $this->db->query("SELECT m.discount_terms_id,SUM(t.sales_detail_quantity) as jumlah,
(
	CASE WHEN (SUM(t.sales_detail_quantity) >= m.discount_terms_product_quantity and 
	          SUM(t.sales_detail_quantity) <= m.discount_terms_product_quantity_limit)
						THEN 'Terpenuhi'
				ELSE 'Tidak Terpenuhi'
 END
) as `status`,m.discount_terms_product_quantity,m.discount_terms_product_quantity_limit
FROM m_discount_terms_product m,t_sales_detail t where
		 t.product_global_id = m.product_global_id and t.sales_id = '".$id_sales."' and m.discount_id = '".$id_diskon."'  GROUP BY m.discount_terms_id order by jumlah ASC");
	?>



	<table class="table table-bordered table-striped">
		<tr>
			<th>Group</th>
			<th>Range Tiering</th>
			<th>Quantity</th>
			<th>Status</th>
		</tr>
		<?php 
		foreach ($dbQueryTieringHeader->result_array() as $key => $value) {
			if($value['status'] == 'Terpenuhi'){
		?>
		<tr onclick="hideDetail(<?= $value['discount_terms_id'] ?>)">
			<td><?=$value['discount_terms_id']?></td>
			<td><?=$value['discount_terms_product_quantity']?> - <?=$value['discount_terms_product_quantity_limit']?>  </td>
			<td><?=$value['jumlah']?></td>
			<td><?=$value['status']?></td>
		</tr>
		<tr id="<?= $value['discount_terms_id'] ?>x" style="display: none;">

			<td colspan="4">
				<table class="table table-bordered table-striped">
					<th>Nama Produk</th>
					<th>Jumlah Pemesanan</th>
					<?php 
					$dbQueryDetailTiering = $this->db->query("select m.* ,(SELECT t.sales_detail_quantity FROM t_sales_detail t where t.product_global_id = m.product_global_id and t.sales_id = '".$id_sales."') as jumlah_pesan ,
						(SELECT t.product_global_name FROM t_sales_detail t where t.product_global_id = m.product_global_id and t.sales_id = '".$id_sales."') as nama_produk

						FROM m_discount_terms_product m where m.discount_terms_id = '".$value['discount_terms_id']."'");
					foreach ($dbQueryDetailTiering->result_array() as $key => $vaDetail) {

						?>
						<tr>
							<td><?=$vaDetail['nama_produk']?></td>
							<td><?=$vaDetail['jumlah_pesan']?></td>
						</tr>
					<?php } ?>
					
				</table>
			</td>

			
		</tr>
		<script type="text/javascript">
			function hideDetail(key){
				if($('#'+key+'x').attr('style') == 'display:none;'){
					$('#'+key+'x').attr('style', '')
				}else{
					$('#'+key+'x').attr('style', 'display:none;')
				}
			}
		</script>
	<?php } ?>
<?php } ?>
		<tbody>
						<tr>
						<td colspan="4">
								
		<?php 
		foreach ($dbQueryTieringHeader->result_array() as $key => $value) {
			if($value['status'] == 'Terpenuhi'){
		
		$dbMAx = $this->db->query("SELECT MAX(discount_terms_product_quantity) as discount_terms_product_quantity_max ,MAX(discount_terms_product_quantity_limit) as   discount_terms_product_quantity_limit_max
		 FROM m_discount_terms_product where discount_id = '".$id_diskon."'");
		$nTiering = 0;
		$nDiskonMax  = 0;
		$nDiskonMin  = 0;
		foreach ($dbMAx->result_array() as $key => $vaMax) {
			$nDiskonMax = $vaMax['discount_terms_product_quantity_max'];

			if($value['jumlah'] >= $vaMax['discount_terms_product_quantity_max'] ){
				$nTiering2 = $nDiskonMax;
			}else{
				$dbMin = $this->db->query("SELECT MIN(discount_terms_product_quantity) as discount_terms_product_quantity_min,MIN(discount_terms_product_quantity_limit) as  discount_terms_product_quantity_limit_min
		 		FROM m_discount_terms_product where discount_id = '".$id_diskon."'");
		 		foreach ($dbMin->result_array() as $key => $vaMin) {
		 			$nDiskonMin = $vaMin['discount_terms_product_quantity_min'];
		 		}
				$nTiering2 = $nDiskonMin;
			}
		}
		?>
		
		<?php 
		if($nTiering2 <= $nDiskonMax){
		?>
			<span style='font-size:14px;font-weight:bold'>Anda Mendapatkan Diskon Tiering <?=$nTiering2?> </span> <br/>
			
		    <input type="hidden" id="id_diskon" value="<?=$id_diskon?>">
			<input type="hidden" id="id_akun" value="<?=$id_akun?>">
			<input type="hidden" id="id_sales" value="<?=$id_sales?>">
			<input type="hidden" id="id_akun" value="<?=$id_akun?>">
			<input type="hidden" id="tiering" value="<?=$nTiering2?>">

			<input type="hidden" id="tipe_diskon" value="<?=$cCekTipeDiskon?>">
			<input type="hidden" id="kategori_diskon" value="<?=$cCekKategoriDiskon?>">
			<button type="button" class="btn btn-primary btn-block" onclick="return klaimdiskontiering();">
				<i class="fa fa-tags"></i> Klaim Diskon <?=$cCekNamaDiskon?> - Tiering
			</button>
		<?php break; } else{ ?>

			<span style='font-size:14px;font-weight:bold'>Anda Mendapatkan Diskon Tiering <?=$nDiskonMax?> </span> <br/>
			 <input type="hidden" id="id_diskon" value="<?=$id_diskon?>">
			<input type="hidden" id="id_akun" value="<?=$id_akun?>">
			<input type="hidden" id="id_sales" value="<?=$id_sales?>">
			<input type="hidden" id="id_akun" value="<?=$id_akun?>">
			<input type="hidden" id="tiering" value="<?=$nDiskonMax?>">

			<input type="hidden" id="tipe_diskon" value="<?=$cCekTipeDiskon?>">
			<input type="hidden" id="kategori_diskon" value="<?=$cCekKategoriDiskon?>">
			<button type="button" class="btn btn-primary btn-block" onclick="return klaimdiskontiering();">
				<i class="fa fa-tags"></i> Klaim Diskon <?=$cCekNamaDiskon?> - Tiering
			</button>
		<?php 
		}
		?>
		    

		<?php  }} ?>
							</td>
						</tr>
					</tbody>
	</table>

	<script type="text/javascript">
		function getTableDiskon(id_akun){
					$.ajax({
						type: "GET",
						url: '<?= base_url() ?>M_account/get_apply_diskon/'+id_akun,
						cache: false,
						success:function(msg){
						  $('#table_apply_diskon').html(msg)
						}
					});
				}
		function klaimdiskontiering(){
			var id_diskon		= $('#id_diskon').val();
			var id_akun			= $('#id_akun').val();
			var id_sales		= $('#id_sales').val();
			var tipe_diskon		= $('#tipe_diskon').val();
			var kategori_diskon	= $('#kategori_diskon').val();
			var tiering			= $('#tiering').val();

			var tanya = confirm("Apakah Yakin Klaim Diskon Tiering ?");

			if (tanya == true) {
				$.ajax({
					data  :"id_diskon="+id_diskon+
					"&id_akun="+id_akun+
					"&id_sales="+id_sales+
					"&tipe_diskon="+tipe_diskon+
					"&tiering="+tiering+
					"&kategori_diskon="+kategori_diskon,
					type: "POST",
					url: '<?= base_url() ?>M_account/simpan_diskon_tiering/',
					cache: false,
					success:function(msg){
						if(msg == 2){
							getTableDiskon(id_akun);
							window.location.hash = '#label_diskon';
						}else{
							alert("Diskon Ini sudah di Apply.");
						}


					}
				});
			}else{
				return false;
			}


		}
	</script>


<?php } ?>
