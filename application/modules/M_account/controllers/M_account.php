<?php
defined('BASEPATH') or exit('No direct script access allowed');


class M_account extends CI_Controller
{

	var $url_ = "M_account";
	var $url2_ = "M_account/table_detail";
	var $id_ = "account_id";
	var $id2_ = "account_detail_id";
	var $eng_ = "account";
	var $eng2_ = "account_detail";
	var $ind_ = "Pembayaran Seller";
	var $ind2_ = "Akun Detail";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function xxx()
	{
		$this->db->trans_begin();
		$data = $this->db->query("SELECT * FROM m_user")->result_array();
		foreach ($data as $index => $value) {
			$this->db->query("UPDATE m_user SET user_password_sha = '" . hash('sha256', $value['user_password']) . "' WHERE user_id = '" . $value['user_id'] . "'");
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		die();
	}

	public function xx($id = '')
	{
		$this->db->trans_begin();
		$data = $this->db->query("SELECT A.*, B.nama, B.kode FROM m_account A JOIN member B ON B.kode = A.seller_id WHERE seller_id IS NOT NULL AND account_code2 IS NULL")->result_array();
		$i = 11;
		$tanggal = "2021-03-09";
		foreach ($data as $index => $value) {
			$this->db->query("UPDATE m_account SET account_code2 = '" . $i . "' WHERE account_id = '" . $value['account_id'] . "'");
			$data3['kode'] = '100.1.3.' . $i;
			$data3['nama'] = 'Piutang ' . str_replace("'", "", $value['nama']) . ' (' . $value['seller_id'] . ')';
			$data3['coa3_id'] = 12;
			$this->db->insert('coa_4', $data3);
			$id_coa_1 = $this->db->insert_id();
			$data3['kode'] = '200.1.2.' . $i;
			$data3['nama'] = 'Deposit ' . str_replace("'", "", $value['nama']) . ' (' . $value['seller_id'] . ')';
			$data3['coa3_id'] = 24;
			$this->db->insert('coa_4', $data3);
			$id_coa_2 = $this->db->insert_id();
			$data4 = array();
			$data4['coa_name'] = 'Piutang ' .  str_replace("'", "", $value['nama']) . ' (' . $value['seller_id'] . ')';
			$data4['coa_code'] = '100.1.3.' . $i;
			$data4['coa_level'] = 4;
			$data4['coa_id'] = $id_coa_1;
			$this->db->insert('t_coa_total', $data4);
			$data4['coa_total_date'] = substr($tanggal, 0, 7) . "-01";
			$this->db->insert('t_coa_total_history', $data4);
			$data4 = array();
			$data4['coa_name'] = 'Deposit ' .  str_replace("'", "", $value['nama']) . ' (' . $value['seller_id'] . ')';
			$data4['coa_code'] = '200.1.2.' . $i;
			$data4['coa_level'] = 4;
			$data4['coa_id'] = $id_coa_2;
			$this->db->insert('t_coa_total', $data4);
			$data4['coa_total_date'] = substr($tanggal, 0, 7) . "-01";
			$this->db->insert('t_coa_total_history', $data4);
			$i++;
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		die();
	}

	public function table_sales($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_sales', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function vendor()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_vendor', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function seller_parsial()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_seller_parsial', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function verification_transfer_seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_verification_transfer_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function factory()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_factory', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_print', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function get_data_verification_transfer_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_verification_transfer_seller();
	}

	public function get_data_verification_transfer_seller2()
	{
		$datacontent['datatable'] = $this->Model->get_data_verification_transfer_seller2();
	}

	public function get_data_transfer($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_transfer($id);
	}

	public function get_data_sales($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_sales($id);
	}

	public function get_data_vendor()
	{
		$datacontent['datatable'] = $this->Model->get_data_vendor();
	}

	public function get_data_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_seller();
	}

	public function get_data_seller_parsial()
	{
		$datacontent['datatable'] = $this->Model->get_data_seller_parsial();
	}

	public function get_data_print()
	{
		$datacontent['datatable'] = $this->Model->get_data_print();
	}

	public function get_data_factory()
	{
		$datacontent['datatable'] = $this->Model->get_data_factory();
	}

	public function table_detail($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		//$datacontent['datatable'] = $this->Model->get_detail($id);
		$datacontent['id'] = $id;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_detail($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_detail($id);
	}

	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['id'] = $id;
		$datacontent['m_employee'] = $this->Model->get_list_employee();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail($parameter = '', $id = '', $id_detail = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account_detail_category'] = $this->Model->get_list_account_detail_category();
		$datacontent['id'] = $id;
		$datacontent['id_detail'] = $id_detail;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_sales_allow($id = '', $idAccount = '', $cekDep = '')
	{
		if ($this->Model->chk_account_detail_sales_product($id) != "") {
			redirect(site_url($this->url_ . "/view_transfer_seller/" . $id));
		}
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$arrdata = $this->Model->get_account_detail_sales($id);
		foreach ($arrdata as $index => $value) {
			$datacontent['arraccount_detail_sales'][$value['sales_id']][$value['sales_detail_id']] = $value;
			$datacontent['arraccount_detail_sales2'][$value['sales_id']] = $value;
		}
		$datacontent['id'] = $id;
		$datacontent['cekDep'] = $cekDep;
		$datacontent['idAccount'] = $idAccount;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_sales_allow', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_factory_invoice($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['arraccount_factory'] = $this->Model->get_account_factory($id);
		$datacontent['title'] = 'Input Invoice ' . $datacontent['arraccount_factory']['nama_factory'];
		$arrterima_produk = $this->Model->get_terima_produk($datacontent['arraccount_factory']['id_factory']);
		foreach ($arrterima_produk as $index => $value) {
			$datacontent['arrterima_produk'][$value['nomor_surat_jalan']][$value['id_barang'] . '|' . $value['tgl_terima']] = $value;
		}
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_factory_invoice', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function transfer_seller($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account_seller();
		$datacontent['arrlist_account_sales'] = $this->Model->get_list_account_sales($id);
		$arrdetail_sales = $this->Model->get_detail_sales($id);
		foreach ($arrdetail_sales as $index => $value) {
			$datacontent['arrdetail_sales'][$value['sales_id']][] = $value;
		}
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function transfer_seller2($parameter = '', $id = '')
	{
		$orang = $this->db->query("SELECT D.user_fullname FROM t_coa_transaction_temp A JOIN m_account B ON B.account_code2 = REPLACE(coa_code, '200.1.2.', '') JOIN member C ON C.kode = B.seller_id JOIN m_user D ON D.user_id = C.user_pic_id WHERE (coa_transaction_source = 2) AND coa_code LIKE '%200.1.2.%' GROUP BY D.user_fullname")->result_array();
		foreach($orang as $indexorang => $valueorang){
			$valuexorang[] = $valueorang['user_fullname'];
		}
		$valuejoinorang = @join(@$valuexorang, ", ");
		if($valuejoinorang != ""){ 
			echo '<script language="javascript">';
			echo 'alert("Mohon kepada '.$valuejoinorang.' agar segera merevisi pembayaran yang telah reject");';
			echo 'window.location.href = "'.site_url($this->url_."/edit_verif_pembayaran").'";';
			echo '</script>';
			die('adsaasdasxx'); 
			redirect(site_url($this->url_."/edit_verif_pembayaran")); 
		}
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Transfer ' . $this->ind_;
		$datacontent['parameter']	= $parameter;
		$datacontent['m_account'] 	= $this->Model->get_list_account_seller();
		$datacontent['arraccount'] 	= $this->Model->get_account($id);
		$arrlist_account_sales = $this->Model->get_list_account_sales($id);
		$datacontent['arrlist_account_sales'] = array();
		foreach ($arrlist_account_sales as $index => $value) {
			$datacontent['arrlist_account_sales'][$value['sales_id']] = $value;
			$datacontent['arrlist_account_sales'][$value['sales_id']]['total_kurang_kirim'] = 0;
		}
		$arrdetail_sales = $this->Model->get_detail_sales($id);
		$in = '';
		$inx = "''";
		$i = 0;
		foreach ($arrdetail_sales as $index => $value) {
			$datacontent['arrdetail_sales'][$value['sales_id']][] = $value;
			if ($i == 0) {
				$in .= $value['sales_id'];
			} else {
				$in .= ', ';
				$in .= $value['sales_id'];
			}
			$i++;
		}
		
		if ($in == '') $in = "''";
		$arrdata = $this->Model->get_account_detail_sales_all($in, $inx);
		foreach ($arrdata as $index => $value) {
			$datacontent['arraccount_detail_sales'][$value['sales_id']][$value['sales_detail_id']] = $value;
			$datacontent['arraccount_detail_sales2'][$value['sales_id']] = $value;
			if(@$datacontent['arrlist_account_sales'][$value['sales_id']] != ""){
				@$datacontent['arrlist_account_sales'][$value['sales_id']]['total_kurang_kirim'] += (@$value['sales_detail_quantity'] - @$value['already']);
			}
		}
		$datacontent['provinsis'] = $this->db->query("SELECT * FROM m_region WHERE REGION_ID LIKE '__00000000'")->result_array();
		$datacontent['deliveries'] = $this->db->query("SELECT * FROM m_delivery_instance")->result_array();

		$diskons = $this->db->query("SELECT * FROM m_discount_terms_product A JOIN m_discount B ON B.discount_id = A.discount_id WHERE B.discount_active = 1")->result_array();
		$datacontent['diskons'] = array();
		$datacontent['headDiskons'] = array();
		foreach ($diskons as $diskon) {
			$datacontent['diskons'][$diskon['discount_id']][$diskon['discount_terms_product_id']] = $diskon;
			$datacontent['headDiskons'][$diskon['discount_id']] = $diskon;
		}
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer_seller2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	
	public function edit_transfer_proof($id = '')
	{
		
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Edit Bukti Pembayaran';
		$datacontent['id'] = $id;
		$data['content'] = $this->load->view($this->url_ . '/edit_transfer_proof', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	
	public function set_edit_transfer_proof()
	{
		$id = $_POST['id'];
		$path = $_FILES['file']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		$config['upload_path']          = './upload/TRANSFER/';
		$config['allowed_types']        = '*';
		$config['file_name']        = $id . "." . $ext;
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('file')) {
			$error = array('error' => $this->upload->display_errors());
		} else {
			$dataxy['account_detail_user_update'] = $_SESSION['user_id'];
			$dataxy['account_detail_proof'] = $id . "." . $ext;
			$this->Model->update_detail($dataxy, ['account_detail_real_id' => $id]);
			$this->Model->update_detail($dataxy, ['account_detail_real_id' => ($id+1)]);
		}
		redirect(site_url($this->url_));
	}

	public function transfer_seller_parsial($parameter = '', $id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Transfer ' . $this->ind_;
		$datacontent['parameter']	= $parameter;
		$datacontent['m_account'] 	= $this->Model->get_list_account_seller();
		$datacontent['arraccount'] 	= $this->Model->get_account($id);
		$arrlist_account_sales = $this->Model->get_list_account_sales($id);
		$datacontent['arrlist_account_sales'] = array();
		foreach ($arrlist_account_sales as $index => $value) {
			$datacontent['arrlist_account_sales'][$value['sales_id']] = $value;
		}
		$arrdetail_sales = $this->Model->get_detail_sales($id);
		$in = '';
		$inx = "''";
		$i = 0;
		foreach ($arrdetail_sales as $index => $value) {
			$datacontent['arrdetail_sales'][$value['sales_id']][] = $value;
			if ($i == 0) {
				$in .= $value['sales_id'];
			} else {
				$in .= ', ';
				$in .= $value['sales_id'];
			}
			$i++;
		}
		if ($in == '') $in = "''";
		$arrdata = $this->Model->get_account_detail_sales_all($in, $inx);
		foreach ($arrdata as $index => $value) {
			$datacontent['arraccount_detail_sales'][$value['sales_id']][$value['sales_detail_id']] = $value;
			$datacontent['arraccount_detail_sales2'][$value['sales_id']] = $value;
		}
		$datacontent['provinsis'] = $this->db->query("SELECT * FROM m_region WHERE REGION_ID LIKE '__00000000'")->result_array();
		$datacontent['deliveries'] = $this->db->query("SELECT * FROM m_delivery_instance")->result_array();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer_seller_parsial', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function view_transfer_seller($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['arraccount_detail'] = $this->Model->get_account_detail($id);
		$datacontent['chk_account_detail'] = $this->Model->chk_account_detail_sales_product($id);
		$arrdata = $this->Model->get_account_detail_sales_product($id);
		foreach ($arrdata as $index => $value) {
			$datacontent['arraccount_detail_sales_product'][$value['sales_id']][$value['product_id']] = $value;
			$datacontent['arraccount_detail_sales'][$value['sales_id']] = $value;
			$account_detail_id = $value['account_detail_id'];
			$account_id = $value['account_id'];
		}
		$datacontent['arrtransfer'] = $this->db->query("SELECT A.*, B.account_name FROM t_account_detail A JOIN m_account B ON B.account_id = A.account_id WHERE A.account_detail_id = '$account_detail_id' AND A.account_id <> '$account_id'")->result_array();
		$datacontent['id'] = $id;
		$data['file'] = 'Detail Pembayaran';
		$data['content'] = $this->load->view($this->url_ . '/view_transfer_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_detail_id'] = $id;
				$from['account_detail_debit'] = 0;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_id'] = $from['account_id'];
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($from['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}

				$to['account_detail_id'] = $id;
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_credit'] = 0;
				$to['account_detail_debit'] = $from['account_detail_credit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function set_transfer_seller()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			$id2 = $this->input->post('id');
			$sales = $this->input->post('sales');
			if ($_POST['parameter'] == "tambah") {
				$from['account_detail_credit'] = str_replace(',', '', $from['account_detail_credit']);
				$from['account_id'] = $id2;
				$from['account_detail_pic'] = $_SESSION['user_fullname'];
				$from['account_detail_id'] = $id;
				$from['account_detail_debit'] = 0;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
				$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
				$exec = $this->Model->insert_detail($from);

				$path = $_FILES['transfer']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				$config['upload_path']          = './upload/TRANSFER/';
				$config['allowed_types']        = '*';
				$config['file_name']        = $exec;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('transfer')) {
					$error = array('error' => $this->upload->display_errors());
				} else {
					$this->upload->data();
					$dataxy['account_detail_proof'] = $exec . "." . $ext;
					$this->Model->update_detail($dataxy, ['account_detail_real_id' => $exec]);
				}
				foreach ($_POST['paid'] as $index_paid => $value_paid) {
					if ($value_paid > 0) {
						$value_paid = str_replace(',', '', $value_paid);
						$data_paid['account_detail_real_id'] = $exec;
						$data_paid['sales_id'] = $sales[$index_paid];
						$data_paid['account_detail_target_real_id'] = $index_paid;
						$data_paid['account_detail_sales_amount'] = $value_paid;
						$data_paid['account_detail_sales_date'] = $from['account_detail_date'];
						$this->Model->insert_account_detal_sales($data_paid, $value_paid, 0);
					}
				}
				$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');
				/*if($from['account_detail_deposit'] > 0){
					$datayyyx['account_detail_deposit_amount'] = $from['account_detail_deposit'];
					$datayyyx['account_detail_real_id'] = $insert_id;
					$datayyyx['account_id'] = $from['account_id'];
					$this->Model->insert_account_detail_deposit($datayyyx);
				}*/

				$to['account_detail_proof'] = $dataxy['account_detail_proof'];
				$to['account_detail_id'] = $id;
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_credit'] = 0;
				$to['account_detail_transfer_name'] = $from['account_detail_transfer_name'];
				$to['account_detail_debit'] = $from['account_detail_credit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_ . "/form_sales_allow2/" . $exec));
	}

	/*public function set_transfer_seller2()
	{
		$id = $this->Model->get_max_id();
		if ($this->input->post('simpan')) {
			$from2 = $this->input->post('from');
			$to = $this->input->post('to');
			$id2 = $this->input->post('id');
			$sales = $this->input->post('sales');
			$account_id = $this->input->post('account_id');
			if ($_POST['parameter'] == "tambah") {
				foreach($account_id as $index => $value){
					$from['account_detail_transfer_name'] = $from2['account_detail_transfer_name'][$index];  
					$from['account_detail_date'] = $from2['account_detail_date'][$index];  
					$from['account_detail_credit'] = $from2['account_detail_credit'][$index];  
					$from['account_detail_credit'] = str_replace(',', '', $from['account_detail_credit']);
					$from['account_id'] = $id2;
					$from['account_detail_pic'] = $_SESSION['user_fullname'];
					$from['account_detail_id'] = $id; 
					$from['account_detail_debit'] = 0;
					$from['account_detail_category_id'] = 1;
					$from['account_detail_type_id'] = 1;
					$from['account_detail_user_id'] = $_SESSION['user_id'];
					$from['account_detail_date_create'] = date('Y-m-d H:i:s');
					//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
					//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
					$exec = $this->Model->insert_detail($from);
					$insert_id = $this->db->insert_id();
					$path = $_FILES['transfer_'.$index]['name'];
					$ext = pathinfo($path, PATHINFO_EXTENSION);
					$config['upload_path']          = './upload/TRANSFER/';
					$config['allowed_types']        = '*';
					$config['file_name']        = $insert_id;
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('transfer_'.$index))
					{
							$error = array('error' => $this->upload->display_errors());
					}
					else
					{
							$this->upload->data();
							$dataxy['account_detail_proof'] = $insert_id.".".$ext;
							$this->Model->update_detail($dataxy, ['account_detail_real_id' => $insert_id]);
					}
					$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');
					
					$to['account_id'] = $value;
					$to['account_detail_proof'] = @$dataxy['account_detail_proof'];
					$to['account_detail_id'] = $id;
					$to['account_detail_pic'] = $from['account_detail_pic'];
					$to['account_detail_credit'] = 0;
					$to['account_detail_transfer_name'] = $from['account_detail_transfer_name'];
					$to['account_detail_debit'] = $from['account_detail_credit'];
					$to['account_detail_pic'] = $from['account_detail_pic'];
					$to['account_detail_date'] = $from['account_detail_date'];
					$to['account_detail_category_id'] = 2;
					$to['account_detail_type_id'] = 1;
					$to['account_detail_user_id'] = $from['account_detail_user_id'];
					$to['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($to);
					$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
				}
				
				foreach($_POST['paid'] as $index_paid => $value_paid){
					if($value_paid > 0){
						$value_paid = str_replace(',', '', $value_paid);
						$data_paid['account_detail_real_id'] = $insert_id;
						$data_paid['sales_id'] = $sales[$index_paid];
						$data_paid['account_detail_target_real_id'] = $index_paid;
						$data_paid['account_detail_sales_amount'] = $value_paid;
						$data_paid['account_detail_sales_date'] = $from['account_detail_date'];
						$data_paid['account_detail_id'] = $id;
						
						$this->Model->insert_account_detal_sales($data_paid);
					}
				}
				$arrx = $this->Model->get_account($id2);
				$account['account_deposit'] = ((int)@$arrx['account_deposit']) + str_replace(',', '', $_POST['deposit']);
				$this->Model->update($account, [$this->id_ => $id2]);
			} else {
				$data[$this->eng_.'_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_."/form_sales_allow/".$id));
	}*/

	// public function set_transfer_seller2()
	// {
	// 	$this->db->trans_begin();
	// 	$id = $this->Model->get_max_id();
	// 	if ($this->input->post('simpan')) {
	// 		$from2 = $this->input->post('from');

	// 		$to = $this->input->post('to');
	// 		$id2 = $this->input->post('id');
	// 		$sales = $this->input->post('sales');
	// 		$account_id = $this->input->post('account_id');
	// 		$account = $this->db->query("SELECT * FROM m_account WHERE account_id = '$id2'")->row_array();
	// 		$member = $this->db->query("SELECT * FROM member WHERE kode = '$account[seller_id]'")->row_array();
	// 		$piutang = $this->db->query("SELECT * FROM coa_4 WHERE kode = '100.1.3.$account[account_code2]'")->row_array();
	// 		$deposit = $this->db->query("SELECT * FROM coa_4 WHERE kode = '200.1.2.$account[account_code2]'")->row_array();
	// 		$this->db->query("UPDATE m_account SET account_deposit_draft = " . str_replace(',', '', $_POST['deposit']) . ", account_deposit_draft_activation = 1 WHERE account_id = " . $_POST['id'] . "");
	// 		// $data_eksped = ([
	// 		// 	'region_id' => $_POST['input2']['kecamatan'],
	// 		// 	'delivery_instance_id' => $_POST['input2']['ekspedisi'],
	// 		// 	'region_delivery_instance_cost' => ''
	// 		// ]);
	// 		// $this->db->insert('m_region_delivery_instance', $data_eksped);
	// 		// $data_eksped['region_delivery_instance_id'] = $this->db->insert_id();

	// 		$dataHeader = ([
	// 			'coa_transaction_date' => date('Y-m-d'),
	// 			'date_create' => date('Y-m-d H:i:s'),
	// 			'user_create' => $_SESSION['user_id'],
	// 			'coa_transaction_deposit' => str_replace(',', '', $_POST['deposit']),
	// 		]);

	// 		$this->db->insert('t_coa_transaction_header', $dataHeader);
	// 		$idHeader = $this->db->insert_id();

	// 		// if (str_replace(",", "", $_POST['diskon']) > 0) {
	// 		// 	$dataDiscountTot = ([
	// 		// 		'coa_group_id' => $idHeader,
	// 		// 		'discount_total_price' => str_replace(",", "", $_POST['diskon']),
	// 		// 		'account_detail_id' => $id,
	// 		// 		'seller_id' => $_POST['seller_id'],
	// 		// 	]);
	// 		// 	$dataDiscountTot['discount_total_id'] = $this->db->insert('t_discount_total', $dataDiscountTot);
	// 		// }

	// 		if ($_POST['parameter'] == "tambah") {
	// 			$i = 0;
	// 			$xi = 0;
	// 			if(isset($_POST['account_id'])){
	// 				foreach ($account_id as $index => $value) {
	// 					if (str_replace(',', '', $from2['account_detail_credit'][$index]) >= 0) { 
	// 						$from = array();
	// 						$data_paid3 = array();
	// 						$data_paid2 = array();
	// 						$exec = '';
	// 						$to = array();
	// 						$from['account_detail_transfer_name'] = $from2['account_detail_transfer_name'][$index];
	// 						$from['account_detail_date'] = $from2['account_detail_date'][$index];
	// 						$from['account_detail_credit'] = $from2['account_detail_credit'][$index];
	// 						$from['account_detail_credit'] = str_replace(',', '', $from['account_detail_credit']);
	// 						$from['account_id'] = $id2;
	// 						$from['account_detail_pic'] = $_SESSION['user_fullname'];
	// 						$from['account_detail_id'] = $id;
	// 						$from['account_detail_debit'] = 0;
	// 						$from['account_detail_category_id'] = 1;
	// 						$from['account_detail_type_id'] = 1;
	// 						$from['account_detail_user_id'] = $_SESSION['user_id'];
	// 						$from['account_detail_date_create'] = date('Y-m-d H:i:s');
	// 						$from['account_detail_note_accounting'] = $from2['account_note'][$index];
	// 						//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
	// 						//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
	// 						$exec = $this->Model->insert_detail($from);
	
	
	// 						$data_paid3['coa_id'] = $piutang['id'];
	// 						$data_paid3['coa_name'] = $piutang['nama'];
	// 						$data_paid3['coa_code'] = $piutang['kode'];
	// 						$data_paid3['coa_level'] = 4;
	// 						$data_paid3['coa_date'] = $from['account_detail_date'];
	// 						$data_paid3['coa_debit'] = $from['account_detail_credit'];
	// 						$data_paid3['coa_credit'] = 0;
	// 						$data_paid3['coa_transaction_note'] = 'Pembayaran ' . $account['account_name'];
	// 						$data_paid3['user_create'] = $_SESSION['user_id'];
	// 						$data_paid3['coa_transaction_source'] = 1;
	// 						$data_paid3['coa_transaction_source_id'] = $exec;
	// 						$data_paid3['coa_transaction_realization'] = 0;
	// 						$data_paid3['coa_group_id'] = $idHeader;
	// 						// $this->Model->insert_coa_transaction($data_paid3);
	// 						$this->db->insert('t_coa_transaction_temp', $data_paid3);
	
	// 						$data_paid2['coa_id'] = $deposit['id'];
	// 						$data_paid2['coa_name'] = $deposit['nama'];
	// 						$data_paid2['coa_code'] = $deposit['kode'];
	// 						$data_paid2['coa_level'] = 4;
	// 						$data_paid2['coa_date'] = $from['account_detail_date'];
	// 						$data_paid2['coa_debit'] = 0;
	// 						$data_paid2['coa_credit'] = $from['account_detail_credit'];
	// 						$data_paid2['coa_transaction_note'] = 'Pembayaran ' . $account['account_name'];
	// 						$data_paid2['user_create'] = $_SESSION['user_id'];
	// 						$data_paid2['coa_transaction_source'] = 1;
	// 						$data_paid2['coa_transaction_source_id'] = $exec;
	// 						$data_paid2['coa_transaction_realization'] = 0;
	// 						$data_paid2['coa_group_id'] = $idHeader;
	// 						// $this->Model->insert_coa_transaction($data_paid2);
	// 						$this->db->insert('t_coa_transaction_temp', $data_paid2);
	
	
	
	// 						$this->db->query("UPDATE t_coa_transaction_header SET coa_transaction_debit = coa_transaction_debit + $from[account_detail_credit], coa_transaction_credit = coa_transaction_credit + $from[account_detail_credit], coa_transaction_payment = coa_transaction_payment + $from[account_detail_credit] WHERE coa_transaction_header_id = '$idHeader'");
	// 						// $this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $from[account_detail_credit] WHERE coa_id = '$deposit[id]'");
	// 						// $this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $from[account_detail_credit] WHERE coa_id = '$deposit[id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $from['account_detail_date'] . "', '-', '')");
	// 						// $this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $from[account_detail_credit] WHERE coa_id = '$piutang[id]'");
	// 						// $this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $from[account_detail_credit] WHERE coa_id = '$piutang[id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $from['account_detail_date'] . "', '-', '')");
	
	// 						$path = $_FILES['transfer_' . $index]['name'];
	// 						$ext = pathinfo($path, PATHINFO_EXTENSION);
	// 						$config['upload_path']          = './upload/TRANSFER/';
	// 						$config['allowed_types']        = '*';
	// 						$config['file_name']        = $exec . "." . $ext;
	// 						//$this->upload->initialize($config);
	// 						if ($xi > 0) {
	// 							$this->upload->initialize($config);
	// 						} else {
	// 							$this->load->library('upload', $config);
	// 						}
	// 						$this->load->library('upload', $config);
	// 						if (!$this->upload->do_upload('transfer_' . $index)) {
	// 							//print_r($this->upload->display_errors());
	// 							//die();
	// 							$error = array('error' => $this->upload->display_errors());
	// 						} else {
	// 							$xi++;
	// 							//	print_r($this->upload->data());
	// 							$dataxy['account_detail_proof'] = $exec . "." . $ext;
	// 							$this->Model->update_detail($dataxy, ['account_detail_real_id' => $exec]);
	// 						}
	// 						$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');
	
	// 						$to['account_id'] = $value;
	// 						$to['account_detail_proof'] = @$dataxy['account_detail_proof'];
	// 						$to['account_detail_id'] = $id;
	// 						$to['account_detail_pic'] = $from['account_detail_pic'];
	// 						$to['account_detail_credit'] = 0;
	// 						$to['account_detail_transfer_name'] = $from['account_detail_transfer_name'];
	// 						$to['account_detail_debit'] = $from['account_detail_credit'];
	// 						$to['account_detail_pic'] = $from['account_detail_pic'];
	// 						$to['account_detail_date'] = $from['account_detail_date'];
	// 						$to['account_detail_category_id'] = 2;
	// 						$to['account_detail_type_id'] = 1;
	// 						$to['account_detail_user_id'] = $from['account_detail_user_id'];
	// 						$to['account_detail_date_create'] = $from['account_detail_date_create'];
	// 						$to_id = $this->Model->insert_detail($to);
	
	// 						$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
	// 					}
	// 				}
	// 			}

	// 			if ($this->input->post('allow') != '') {
	// 				$allow = $this->input->post('allow');
	// 				$ids = $this->input->post('ids');
	// 				$product_id = $this->input->post('product_id');
	// 			}
	// 			$cekEx = 0;
	// 			if (@$exec == '') {
	// 				$cekEx = 1;
	// 				$this->db->query("UPDATE m_account SET account_deposit = " . str_replace(',', '', $_POST['deposit']) . ", account_deposit_draft = 0, account_deposit_draft_activation = 0 WHERE account_id = " . $_POST['id'] . "");
	// 				$from = array();
	// 				$data_paid3 = array();
	// 				$data_paid2 = array();
	// 				$exec = '';
	// 				$to = array();
	// 				$from['account_detail_transfer_name'] = 'Deposit';
	// 				$from['account_detail_date'] = date('Y-m-d');
	// 				$from['account_detail_credit'] = 0;
	// 				$from['account_id'] = $id2;
	// 				$from['account_detail_pic'] = $_SESSION['user_fullname'];
	// 				$from['account_detail_id'] = $id;
	// 				$from['account_detail_debit'] = 0;
	// 				$from['account_detail_category_id'] = 1;
	// 				$from['account_detail_type_id'] = 1;
	// 				$from['account_detail_user_id'] = $_SESSION['user_id'];
	// 				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
	// 				//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
	// 				//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
	// 				$exec = $this->Model->insert_detail($from);
	// 			}
	// 			if(isset($_POST['paid'])){
	// 				foreach ($_POST['paid'] as $index_paid => $value_paid) {
	// 					$value_paid = str_replace(',', '', $value_paid);
	// 					//if ($value_paid > 0) {
	// 						$data_paid['account_detail_real_id'] = $exec;
	// 						$data_paid['sales_id'] = $sales[$index_paid];
	// 						$data_paid['account_detail_sales_amount'] = str_replace(',', '', $value_paid);
	// 						$data_paid['account_detail_sales_date'] = $from['account_detail_date'];
	// 						$data_paid['account_detail_id'] = $id;
	// 						$account_detail_sales_id = $this->Model->insert_account_detal_sales($data_paid, $value_paid, $cekEx);
	// 						if($cekEx == 1) $this->db->query("UPDATE t_account_detail SET account_detail_paid = account_detail_paid + $data_paid[account_detail_sales_amount] WHERE sales_id = $data_paid[sales_id]");
	// 						if ($this->input->post('allow') != '') {
	// 							if (isset($_POST['total_diskon_harga'])) $total_diskon_harga = $_POST['total_diskon_harga'];
	// 							else $total_diskon_harga = 0;
	// 							foreach ($allow[$index_paid] as $index2 => $value2) {
	// 								$prod_kd = $this->db->get_where('produk', ['id_produk' => $product_id[$index_paid][$index2]])->row_array();
	// 								$data = array();
	// 								$data['account_detail_id'] = $id;
	// 								$data['account_detail_real_id'] = $exec;
	// 								$data['sales_id'] = $sales[$index_paid];
	// 								$data['sales_detail_id'] = $index2;
	// 								$data['product_id'] = $product_id[$index_paid][$index2];
	// 								$data['account_detail_sales_product_allow'] = $value2;
	// 								$data['account_detail_sales_id'] = $account_detail_sales_id;
	// 								// $data['region_id'] = $_POST['input2']['kecamatan'];
	// 								// $data['region_delivery_instance_id'] = $data_eksped['region_delivery_instance_id'];
	// 								// $data['delivery_instance_id'] = $_POST['input2']['ekspedisi'];
	// 								// $data['account_detail_sales_product_address'] = $_POST['input2']['full_address'];
	// 								$data['account_detail_sales_product_delivery_cost'] = '';
	// 								if (@$_POST['diskon_harga'][$sales[$index_paid]][$index2] !== null) {
	// 									if ($_POST['discount_id'][$sales[$index_paid]][$index2] !== null) {
	// 										$discount = $_POST['diskon_harga'][$sales[$index_paid]][$index2];
	// 										$terms = $this->db->get_where('m_discount_terms_product', ['discount_id' => $_POST['discount_id'][$sales[$index_paid]][$index2], 'product_kd' => $prod_kd['kd_pd']])->row_array();
	// 										if ($terms !== null && $value2 > 0) {
	// 											$sales_discount_terms = $this->db->query("SELECT B.sales_discount_terms_id, A.sales_discount_id FROM t_sales_discount A JOIN t_sales_discount_terms B ON B.sales_discount_id = A.sales_discount_id AND B.product_kd = '$prod_kd[kd_pd]' WHERE A.sales_id = $sales[$index_paid] AND A.discount_id = $terms[discount_id]")->row_array();
	// 											if ($terms['discount_terms_product_quantity'] > 0) {
	// 												$disc_term = floor($value2 / $terms['discount_terms_product_quantity']);
	// 												$disc_mod = $value2 - ($terms['discount_terms_product_quantity'] * $disc_term);
	// 											} else if ($terms['discount_terms_product_price'] > 0) {
	// 												$disc_mod = fmod($terms['discount_terms_product_price'], $value2);
	// 												$disc_term = intdiv($terms['discount_terms_product_price'], $value2);
	// 												if ($disc_mod > 0) {
	// 													$disc_mod = $value2 - $disc_term;
	// 												}
	// 											} else {
	// 												$disc_term = 0;
	// 												$disc_mod = 0;
	// 											}
	// 											if ($disc_mod > 0) {
	// 												$data['account_detail_sales_product_discount'] = floor($discount / ($value2 - 1));
	// 												$data['account_detail_sales_product_allow'] = $value2 - 1;
	// 												$datax = ([
	// 													'account_detail_id' => $id,
	// 													'account_detail_real_id' => $exec,
	// 													'sales_id' => $sales[$index_paid],
	// 													'sales_detail_id' => $index2,
	// 													'product_id' => $product_id[$index_paid][$index2],
	// 													'account_detail_sales_product_allow' => 1,
	// 													'account_detail_sales_id' => $account_detail_sales_id,
	// 													// 'region_id' => $_POST['input2']['kecamatan'],
	// 													// 'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
	// 													// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
	// 													// 'account_detail_sales_product_address' => $_POST['input2']['full_address'],
	// 													'account_detail_sales_product_delivery_cost' => '',
	// 													'account_detail_sales_product_discount' => $discount - ($data['account_detail_sales_product_discount'] * ($value2 - 1)),
	// 													'account_detail_sales_product_discount_total' => $discount - ($data['account_detail_sales_product_discount'] * ($value2 - 1)),
	// 													'sales_discount_terms_id' => $sales_discount_terms['sales_discount_terms_id']
	// 												]);
	// 												$this->db->insert("t_account_detail_sales_product", $datax);
	// 												if ($cekEx == 1) {
	// 													$discountx = $this->db->get_where('t_discount_total', ['account_detail_id' => $datax['account_detail_id']])->row_array();
	// 													if ($discountx['discount_total_price'] == null) $discountx['discount_total_price'] = 0;
	// 													$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
	// 													$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow], discount_price = discount_price + $discountx[discount_total_price] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
	// 													$this->Model->check_fifo_alpha($datax['product_id']);
	// 													$this->Model->check_fifo_beta($datax['product_id']);
	// 													$this->Model->check_fifo_main($datax['product_id'], $account['seller_id']);
	// 													$this->Model->set_sales_detail();
	// 													$discountx = '';
	// 												}
	// 												$data['account_detail_sales_product_discount_total'] = $discount - $datax['account_detail_sales_product_discount_total'];
	// 											} else {
	// 												$data['account_detail_sales_product_discount'] = $discount / $value2;
	// 												$data['account_detail_sales_product_discount_total'] = $discount;
	// 											}

	// 											$data['account_detail_sales_product_terms_quantity'] = ($terms['discount_terms_product_quantity'] * $disc_term);
	// 											$data['account_detail_sales_product_terms_mod'] = $disc_mod;
	// 											$data['sales_discount_terms_id'] = $sales_discount_terms['sales_discount_terms_id'];
	// 										}
	// 									}
	// 								}
	// 								$total_harga_prod = $value2 * $_POST['harga_produk'][$sales[$index_paid]][$index2];
	// 								$hrgprd[$prod_kd['kd_pd']][$sales[$index_paid]] = $total_harga_prod;
	// 								if (@$_POST['diskon_persen'][$sales[$index_paid]][$index2] !== null && @$_POST['diskon_persen'][$sales[$index_paid]][$index2] > 0) {
	// 									if ($_POST['discount_id'][$sales[$index_paid]][$index2] !== '') {
	// 										$discount = $_POST['diskon_persen'][$sales[$index_paid]][$index2];
	// 										$terms = $this->db->get_where('m_discount_terms_product', ['discount_id' => $_POST['discount_id'][$sales[$index_paid]][$index2], 'product_kd' => $prod_kd['kd_pd']])->row_array();
	// 										if ($terms !== null && $value2 > 0) {
	// 											$sales_discount_terms = $this->db->query("SELECT B.sales_discount_terms_id, A.sales_discount_id FROM t_sales_discount A JOIN t_sales_discount_terms B ON B.sales_discount_id = A.sales_discount_id AND B.product_kd = '$prod_kd[kd_pd]' WHERE A.sales_id = $sales[$index_paid] AND A.discount_id = $terms[discount_id]")->row_array();
	// 											$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_active_draft = 1 WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]");
	// 											$tSalesDiscount = $this->db->query("SELECT * FROM  t_sales_discount WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]")->row_array();
	// 											if ($terms['discount_terms_product_quantity'] > 0) {
	// 												$disc_term = floor($value2 / $terms['discount_terms_product_quantity']);
	// 												$disc_mod = $value2 - ($terms['discount_terms_product_quantity'] * $disc_term);
	// 											} else if ($terms['discount_terms_product_price'] > 0) {
	// 												$disc_mod = fmod($terms['discount_terms_product_price'], $value2);
	// 												$disc_term = intdiv($terms['discount_terms_product_price'], $value2);
	// 												if ($disc_mod > 0) {
	// 													$disc_mod = $value2 - $disc_term;
	// 												}
	// 											} else {
	// 												$disc_term = 0;
	// 												$disc_mod = 0;
	// 											}
	// 											if($value2 > 1){
	// 												$data['account_detail_sales_product_discount'] = floor($discount / ($value2 - 1));
	// 											}else{
	// 												$data['account_detail_sales_product_discount'] = floor($discount / $value2);
	// 											}
	// 											if ($tSalesDiscount['sales_discount_percentage_hoard_draft'] > 0 || $tSalesDiscount['sales_discount_percentage_hoard'] > 0) {
	// 												$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard_draft = 0 WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]");
	// 												$data['account_detail_sales_product_discount_total'] = $data['account_detail_sales_product_discount'] * ($value2 - 1);
	// 												$data['account_detail_sales_product_allow'] = $value2 - 1;
	// 												$datax = ([
	// 													'account_detail_id' => $id,
	// 													'account_detail_real_id' => $exec,
	// 													'sales_id' => $sales[$index_paid],
	// 													'sales_detail_id' => $index2,
	// 													'product_id' => $product_id[$index_paid][$index2],
	// 													'account_detail_sales_product_allow' => 1,
	// 													'account_detail_sales_id' => $account_detail_sales_id,
	// 													// 'region_id' => $_POST['input2']['kecamatan'],
	// 													// 'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
	// 													// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
	// 													// 'account_detail_sales_product_address' => $_POST['input2']['full_address'],
	// 													'account_detail_sales_product_delivery_cost' => '',
	// 													'account_detail_sales_product_discount' => $discount - $data['account_detail_sales_product_discount_total'],
	// 													'account_detail_sales_product_discount_total' => $discount - $data['account_detail_sales_product_discount_total'],
	// 													'sales_discount_terms_id' => $sales_discount_terms['sales_discount_terms_id']
	// 												]);
	// 												$this->db->insert("t_account_detail_sales_product", $datax);
	// 												if ($cekEx == 1) {
	// 													$discountx = $this->db->get_where('t_discount_total', ['account_detail_id' => $datax['account_detail_id']])->row_array();
	// 													if ($discountx['discount_total_price'] == null) $discountx['discount_total_price'] = 0;
	// 													$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
	// 													$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow], discount_price = discount_price + $discountx[discount_total_price] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
	// 													$this->Model->check_fifo_alpha($datax['product_id']);
	// 													$this->Model->check_fifo_beta($datax['product_id']);
	// 													$this->Model->check_fifo_main($datax['product_id'], $account['seller_id']);
	// 													$this->Model->set_sales_detail();
	// 													$discountx = '';
	// 												}
	// 											}
	// 										}
	// 									}
	// 								}
	// 								$this->Model->insert_account_detail_sales_product($data);
	// 								if ($cekEx == 1) {
	// 									$discount = $this->db->get_where('t_discount_total', ['account_detail_id' => $data['account_detail_id']])->row_array();
	// 									if ($discount['discount_total_price'] == null) $discount['discount_total_price'] = 0;
	// 									$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $data[account_detail_sales_product_allow] WHERE product_id = $data[product_id] AND sales_detail_id = $data[sales_detail_id]");
	// 									$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $data[account_detail_sales_product_allow], discount_price = discount_price + $discount[discount_total_price] WHERE product_id = $data[product_id] AND sales_detail_id = $data[sales_detail_id]");
	// 									$this->Model->check_fifo_alpha($data['product_id']);
	// 									$this->Model->check_fifo_beta($data['product_id']);
	// 									$this->Model->check_fifo_main($data['product_id'], $account['seller_id']);
	// 									$this->Model->set_sales_detail();
	// 									$discount = '';
	// 								}
	// 							}
	// 						}
	// 					//}
	// 				}
	// 			}

	// 			if (isset($_POST['sisa_produk'])) {
	// 				foreach ($_POST['sisa_produk'] as $index1 => $sisa_produk) {
	// 					foreach ($sisa_produk as $index2 => $sisa) {
	// 						$sales_dc = $this->db->get_where('t_sales_discount_terms', ['sales_discount_terms_id' => $index2])->row_array();
	// 						$sales_disc = $this->db->get_where('t_sales_discount', ['sales_discount_id' => $sales_dc['sales_discount_id']])->row_array();
	// 						if ($sales_disc['sales_discount_percentage_active'] !== 1 && $hrgprd[$sales_dc['product_kd']][$sales_disc['sales_id']] !== null) {
	// 							if ($cekEx == 1) {
	// 								$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod = $sisa, sales_discount_terms_accumulation = " . $_POST['produk_accum'][$index1][$index2] . ", sales_discount_terms_mod_price = " . $hrgprd[$sales_dc['product_kd']][$sales_disc['sales_id']] . " WHERE sales_discount_terms_id = $index2");
	// 							} else {
	// 								$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod_draft = $sisa, sales_discount_terms_accumulation_draft = " . $_POST['produk_accum'][$index1][$index2] . ", sales_discount_terms_mod_price_draft = " . $hrgprd[$sales_dc['product_kd']][$sales_disc['sales_id']] . " WHERE sales_discount_terms_id = $index2");
	// 							}
	// 							$this->db->insert('t_account_detail_sales_terms', ['sales_discount_terms_id' => $index2, 'sales_discount_id' => $sales_dc['sales_discount_id'], 'account_detail_sales_product_terms_mod' => $sisa, 'account_detail_sales_product_terms_accumulation' => $_POST['produk_accum'][$index1][$index2]]);
	// 						}
	// 					}
	// 				}
	// 			}
	// 			if (isset($_POST['dikson_produk'])) {
	// 				foreach ($_POST['dikson_produk'] as $index => $dikson_produks) {
	// 					foreach ($dikson_produks as $index2 => $dikson_produk) {
	// 						$sales_product = $this->db->get_where('t_sales_discount_product', ['sales_detail_id' => $index2])->row_array();
	// 						$product = $this->db->get_where('produk', ['kd_pd' => $sales_product['product_kd']])->row_array();
	// 						$productPrice = $this->db->query("SELECT * FROM m_price WHERE produk_global_kd = '$sales_product[product_kd]' AND schema_id = '$member[schema_id]' AND price_unit_price > 0 ORDER BY price_unit_price ASC LIMIT 1")->row_array();
	// 						$detailSalesProd = ([
	// 							'account_detail_real_id' => $exec,
	// 							'sales_id' => $index,
	// 							'product_id' => $product['id_produk'],
	// 							'account_detail_sales_product_allow' => $dikson_produk,
	// 							'account_detail_sales_id' => $account_detail_sales_id,
	// 							'sales_detail_id' => $index2,
	// 							'account_detail_id' => $id,
	// 							// 'region_id' => $_POST['input2']['kecamatan'],
	// 							// 'region_delivery_instance_id' =>  $data_eksped['region_delivery_instance_id'],
	// 							// 'delivery_instance_id' =>  $_POST['input2']['ekspedisi'],
	// 							// 'account_detail_sales_product_address' =>  $_POST['input2']['full_address'],
	// 							'account_detail_sales_product_delivery_cost' =>  '',
	// 							// 'account_detail_sales_product_price' =>  $productPrice['price_unit_price'],
	// 							'account_detail_sales_product_discount' =>  $productPrice['price_unit_price'],
	// 							'account_detail_sales_product_discount_total' =>  $productPrice['price_unit_price'] * $dikson_produk,
	// 						]);
	// 						$this->db->insert('t_account_detail_sales_product', $detailSalesProd);
	// 						if ($cekEx == 1) {
	// 							$discount = $this->db->get_where('t_discount_total', ['account_detail_id' => $detailSalesProd['account_detail_id']])->row_array();
	// 							if ($discount['discount_total_price'] == null) $discount['discount_total_price'] = 0;
	// 							$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $detailSalesProd[account_detail_sales_product_allow] WHERE product_id = $detailSalesProd[product_id] AND sales_detail_id = $detailSalesProd[sales_detail_id]");
	// 							$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $detailSalesProd[account_detail_sales_product_allow], discount_price = discount_price + $discount[discount_total_price] WHERE product_id = $detailSalesProd[product_id] AND sales_detail_id = $detailSalesProd[sales_detail_id]");
	// 							$this->Model->check_fifo_alpha($detailSalesProd['product_id']);
	// 							$this->Model->check_fifo_beta($detailSalesProd['product_id']);
	// 							$this->Model->check_fifo_main($detailSalesProd['product_id'], $account['seller_id']);
	// 							$this->Model->set_sales_detail();
	// 							$discount = '';
	// 						}
	// 					}
	// 				}
	// 			}
	// 			if (isset($_POST['promo_product'])) {
	// 				if (isset($dataDiscountTot)) {
	// 				} else {
	// 					$dataDiscountTot = ([
	// 						'coa_group_id' => $idHeader,
	// 						'discount_total_price' => 0,
	// 						'account_detail_id' => $id,
	// 						'seller_id' => $_POST['seller_id'],
	// 					]);
	// 					$dataDiscountTot['discount_total_id'] = $this->db->insert('t_discount_total', $dataDiscountTot);
	// 				}
	// 				$dataSales['sales_code'] = $this->generate_code();
	// 				$dataSales['user_id'] = $this->session->userdata('user_id');
	// 				$dataSales['sales_date_create'] = date('Y-m-d H:i:s');
	// 				$dataSales['sales_category_id'] = $_SESSION['sales_category_id'];
	// 				$dataSales['sales_date'] = date('Y-m-d');
	// 				$dataSales['seller_id'] = $_POST['seller_id'];
	// 				//if($data['sales_address'] == "") $data['sales_address'] = $this->Model->get_seller_address($data['seller_id']); 
	// 				$this->Model->chk_account($dataSales['seller_id']);
	// 				$this->db->insert('T_sales', $dataSales);
	// 				$sales_id = $this->db->insert_id();
	// 				$data_paid = array();
	// 				$data_paid['account_detail_real_id'] = $exec;
	// 				$data_paid['sales_id'] = $dataSales['sales_id'];
	// 				$data_paid['account_detail_sales_amount'] = 0;
	// 				$data_paid['account_detail_sales_date'] = date('Y-m-d');
	// 				$data_paid['account_detail_id'] = $id;
	// 				$account_detail_sales_id = $this->Model->insert_account_detal_sales($data_paid, 0, 0);
	// 				foreach ($_POST['promo_product'] as $index => $promo_product) {
	// 					$product = $this->db->get_where('produk', ['kd_pd' => $index])->row_array();
	// 					$price_prod = $this->db->query("SELECT * FROM m_price WHERE produk_global_kd = $index AND schema_id = '$member[schema_id]' AND (price_quantity <= $promo_product OR price_quantity >= $promo_product) ORDER BY price_quantity ASC")->row_array();
	// 					$data = array();
	// 					$data['account_detail_id'] = $id;
	// 					$data['account_detail_real_id'] = $exec;
	// 					$data['sales_id'] = $sales_id;
	// 					// $data['sales_detail_id'] = $index2;
	// 					$data['product_id'] = $product['id_produk'];
	// 					$data['account_detail_sales_product_allow'] = $promo_product;
	// 					// $data['region_id'] = $_POST['input2']['kecamatan'];
	// 					// $data['region_delivery_instance_id'] = $data_eksped['region_delivery_instance_id'];
	// 					// $data['delivery_instance_id'] = $_POST['input2']['ekspedisi'];
	// 					// $data['account_detail_sales_product_address'] = $_POST['input2']['full_address'];
	// 					$data['account_detail_sales_product_delivery_cost'] = '';
	// 					$this->Model->insert_account_detail_sales_product($data);
	// 					$price = $price_prod['price_unit_price'] * $promo_product;
	// 					$this->db->query("UPDATE t_discount_total SET discount_total_price = discount_total_price + $price WHERE discount_total_id = $dataDiscountTot[discount_total_id]");
	// 				}
	// 			}
	// 			if (isset($_POST['tot_hoard'])) {
	// 				foreach ($_POST['tot_hoard'] as $index => $tot_hoard) {
	// 					if ($cekEx == 1) {
	// 						$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard = sales_discount_percentage_hoard + $tot_hoard WHERE sales_discount_id = $index");
	// 					} else {
	// 						$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard_draft = sales_discount_percentage_hoard_draft + $tot_hoard WHERE sales_discount_id = $index");
	// 					}
	// 				}
	// 			}

	// 			$arrx = $this->Model->get_account($id2);
	// 			if (isset($_POST['dropship'])) {

	// 				$productdropship = array();
	// 				foreach ($_POST['dropship']['name'] as $index => $dropship) {
	// 					$arrproductdropship = array();
	// 					$data = ([
	// 						'account_detail_id' => $id,
	// 						'account_detail_real_id' => $exec,
	// 						'region_id' => $_POST['dropship']['kecamatan'][$index],
	// 						'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
	// 						'delivery_instance_id' => $_POST['dropship']['ekspedisi'][$index],
	// 						'account_detail_delivery_address' => $_POST['dropship']['full_address'][$index],
	// 						'account_detail_delivery_cost' => '',
	// 					]);
	// 					$this->db->insert('t_account_detail_delivery', $data);
	// 					$data['account_detail_delivery_id'] = $this->db->insert_id();

	// 					foreach ($_POST['dropship']['produk'][$index] as $index2 => $produk) {
	// 						@$arrproductdropship[$produk] += $_POST['dropship']['jumlah'][$index][$index2];
	// 						// if ($productdropship[$produk] == null) {
	// 						// 	$productdropship[$produk] = 0;
	// 						// }
	// 						@$productdropship[$produk] += $_POST['dropship']['jumlah'][$index][$index2];
	// 					}
	// 					foreach ($arrproductdropship as $index3 => $productdrop) {
	// 						$data2 = ([
	// 							'account_detail_delivery_id' => $data['account_detail_delivery_id'],
	// 							'account_detail_id' => $id,
	// 							'account_detail_real_id' => $exec,
	// 							'product_id' => $index3,
	// 							'account_detail_delivery_product_quantity' => $productdrop,
	// 							'account_detail_delivery_product_cost' => '',
	// 						]);
	// 						$this->db->insert('t_account_detail_delivery_product', $data2);
	// 					}
	// 				}
	// 				$i = 0;
	// 				foreach ($productdropship as $index4 => $proddrop) {
	// 					if ($_POST['total_product'][$index4] > $proddrop) {
	// 						$i++;
	// 					}
	// 				}
	// 				if ($i > 0) {
	// 					$data3 = ([
	// 						'account_detail_id' => $id,
	// 						'account_detail_real_id' => $exec,
	// 						// 'region_id' => $_POST['input2']['kecamatan'],
	// 						'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
	// 						// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
	// 						// 'account_detail_delivery_address' => $_POST['input2']['full_address'],
	// 						'account_detail_delivery_cost' => '',
	// 					]);
	// 					$this->db->insert('t_account_detail_delivery', $data3);
	// 					$data3['account_detail_delivery_id'] = $this->db->insert_id();

	// 					foreach ($productdropship as $index5 => $proddrop) {
	// 						if ($_POST['total_product'][$index5] > $proddrop) {
	// 							$data4 = ([
	// 								'account_detail_delivery_id' => $data3['account_detail_delivery_id'],
	// 								'account_detail_id' => $id,
	// 								'account_detail_real_id' => $exec,
	// 								'product_id' => $index5,
	// 								'account_detail_delivery_product_quantity' => ($_POST['total_product'][$index5] - $proddrop),
	// 								'account_detail_delivery_product_cost' => '',
	// 							]);
	// 							$this->db->insert('t_account_detail_delivery_product', $data4);
	// 						}
	// 					}
	// 				}
	// 			} else {
	// 				$data3 = ([
	// 					'account_detail_id' => $id,
	// 					'account_detail_real_id' => $exec,
	// 					'region_id' => $_POST['input2']['kecamatan'],
	// 					'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
	// 					'delivery_instance_id' => $_POST['input2']['ekspedisi'],
	// 					'account_detail_delivery_address' => $_POST['input2']['full_address'],
	// 					'account_detail_delivery_cost' => '',
	// 				]);
	// 				$this->db->insert('t_account_detail_delivery', $data3);
	// 				$data3['account_detail_delivery_id'] = $this->db->insert_id();

	// 				foreach ($_POST['total_product'] as $index5 => $proddrop) {
	// 					if ($index5 > 0) {
	// 						$data4 = ([
	// 							'account_detail_delivery_id' => $data3['account_detail_delivery_id'],
	// 							'account_detail_id' => $id,
	// 							'account_detail_real_id' => $exec,
	// 							'product_id' => $index5,
	// 							'account_detail_delivery_product_quantity' => $proddrop,
	// 							'account_detail_delivery_product_cost' => '',
	// 						]);
	// 						$this->db->insert('t_account_detail_delivery_product', $data4);
	// 					}
	// 				}
	// 			}
	// 			// $accountx['account_deposit'] = ((int)@$arrx['account_deposit']) + str_replace(',', '', $_POST['deposit']);
	// 			// $this->Model->update($accountx, [$this->id_ => $id2]);

	// 			// $allow = $this->input->post('allow');
	// 			// $ids = $this->input->post('ids');
	// 			// print_r($ids);
	// 			// die;
	// 			// $product_id = $this->input->post('product_id');
	// 			// foreach ($allow as $index => $arrvalue) {
	// 			// 	foreach ($arrvalue as $index2 => $value2) {
	// 			// 		//if($value2 > 0){
	// 			// 		$data['account_detail_id'] = $id;
	// 			// 		$data['account_detail_real_id'] = $exec;
	// 			// 		$data['sales_id'] = $index;
	// 			// 		$data['sales_detail_id'] = $index2;
	// 			// 		$data['product_id'] = $product_id[$index][$index2];
	// 			// 		$data['account_detail_sales_product_allow'] = $value2;
	// 			// 		$data['account_detail_sales_id'] = $ids[$index][$index2];
	// 			// 		$this->Model->insert_account_detail_sales_product($data);
	// 			// 		//} 
	// 			// 	}
	// 			// }
	// 		} else {
	// 			$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
	// 			$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
	// 		}
	// 	}
	// 	//die();  
	// 	// foreach ($_POST['sales_id_all'] as $index_p => $update_p) {
	// 	// 	$this->db->query('UPDATE t_account_detail A SET account_detail_paid = IFNULL((SELECT SUM(account_detail_sales_amount) FROM t_account_detail_sales WHERE sales_id = "$update_p"), 0) WHERE A.sales_id = "$update_p"');
	// 	// }
	// 	if ($this->db->trans_status() === FALSE) {
	// 		$this->db->trans_rollback();
	// 	} else {
	// 		$this->db->trans_commit();
	// 	}
	// 	redirect(site_url($this->url_ . "/seller"));
	// }


	public function set_transfer_seller2()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		if ($this->input->post('simpan')) {
			$from2 = $this->input->post('from');

			$to = $this->input->post('to');
			$id2 = $this->input->post('id');
			$sales = $this->input->post('sales');
			$account_id = $this->input->post('account_id');
			$account = $this->db->query("SELECT * FROM m_account WHERE account_id = '$id2'")->row_array();
			$member = $this->db->query("SELECT * FROM member WHERE kode = '$account[seller_id]'")->row_array();
			$piutang = $this->db->query("SELECT * FROM coa_4 WHERE kode = '100.1.3.$account[account_code2]'")->row_array();
			$deposit = $this->db->query("SELECT * FROM coa_4 WHERE kode = '200.1.2.$account[account_code2]'")->row_array();
			$this->db->query("UPDATE m_account SET account_deposit_draft = " . str_replace(',', '', $_POST['deposit']) . ", account_deposit_draft_activation = 1 WHERE account_id = " . $_POST['id'] . "");
			// $data_eksped = ([
			// 	'region_id' => $_POST['input2']['kecamatan'],
			// 	'delivery_instance_id' => $_POST['input2']['ekspedisi'],
			// 	'region_delivery_instance_cost' => ''
			// ]);
			// $this->db->insert('m_region_delivery_instance', $data_eksped);
			// $data_eksped['region_delivery_instance_id'] = $this->db->insert_id();

			$dataHeader = ([
				'coa_transaction_date' => date('Y-m-d'),
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_deposit' => str_replace(',', '', $_POST['deposit']),
			]);

			$this->db->insert('t_coa_transaction_header', $dataHeader);
			$idHeader = $this->db->insert_id();

			// if (str_replace(",", "", $_POST['diskon']) > 0) {
			// 	$dataDiscountTot = ([
			// 		'coa_group_id' => $idHeader,
			// 		'discount_total_price' => str_replace(",", "", $_POST['diskon']),
			// 		'account_detail_id' => $id,
			// 		'seller_id' => $_POST['seller_id'],
			// 	]);
			// 	$dataDiscountTot['discount_total_id'] = $this->db->insert('t_discount_total', $dataDiscountTot);
			// }

			if ($_POST['parameter'] == "tambah") {
				$i = 0;
				$xi = 0;
				if(isset($_POST['account_id'])){
					foreach ($account_id as $index => $value) {
						if (str_replace(',', '', $from2['account_detail_credit'][$index]) >= 0) { 
							$from = array();
							$data_paid3 = array();
							$data_paid2 = array();
							$exec = '';
							$to = array();
							$from['account_detail_transfer_name'] = $from2['account_detail_transfer_name'][$index];
							$from['account_detail_date'] = $from2['account_detail_date'][$index];
							$from['account_detail_credit'] = $from2['account_detail_credit'][$index];
							$from['account_detail_credit'] = str_replace(',', '', $from['account_detail_credit']);
							$from['account_id'] = $id2;
							$from['account_detail_pic'] = $_SESSION['user_fullname'];
							$from['account_detail_id'] = $id;
							$from['account_detail_debit'] = 0;
							$from['account_detail_category_id'] = 1;
							$from['account_detail_type_id'] = 1;
							$from['account_detail_user_id'] = $_SESSION['user_id'];
							$from['account_detail_date_create'] = date('Y-m-d H:i:s');
							$from['account_detail_note_accounting'] = $from2['account_note'][$index];
							//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
							//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
							$exec = $this->Model->insert_detail($from);
							$lq = $this->db->last_query();


							$data_paid3['coa_id'] = $piutang['id'];
							$data_paid3['coa_name'] = $piutang['nama'];
							$data_paid3['coa_code'] = $piutang['kode'];
							$data_paid3['coa_level'] = 4;
							$data_paid3['coa_date'] = $from['account_detail_date'];
							$data_paid3['coa_debit'] = $from['account_detail_credit'];
							$data_paid3['coa_credit'] = 0;
							$data_paid3['coa_transaction_note'] = 'Pembayaran ' . $account['account_name'];
							$data_paid3['user_create'] = $_SESSION['user_id'];
							$data_paid3['coa_transaction_source'] = 1;
							$data_paid3['coa_transaction_source_id'] = $exec;
							$data_paid3['coa_transaction_realization'] = 0;
							$data_paid3['coa_group_id'] = $idHeader;
							// $this->Model->insert_coa_transaction($data_paid3);
							$this->db->insert('t_coa_transaction_temp', $data_paid3);

							$data_paid2['coa_id'] = $deposit['id'];
							$data_paid2['coa_name'] = $deposit['nama'];
							$data_paid2['coa_code'] = $deposit['kode'];
							$data_paid2['coa_level'] = 4;
							$data_paid2['coa_date'] = $from['account_detail_date'];
							$data_paid2['coa_debit'] = 0;
							$data_paid2['coa_credit'] = $from['account_detail_credit'];
							$data_paid2['coa_transaction_note'] = 'Pembayaran ' . $account['account_name'];
							$data_paid2['user_create'] = $_SESSION['user_id'];
							$data_paid2['coa_transaction_source'] = 1;
							$data_paid2['coa_transaction_source_id'] = $exec;
							$data_paid2['coa_transaction_realization'] = 0;
							$data_paid2['coa_group_id'] = $idHeader;
							// $this->Model->insert_coa_transaction($data_paid2);
							$this->db->insert('t_coa_transaction_temp', $data_paid2);



							$this->db->query("UPDATE t_coa_transaction_header SET coa_transaction_debit = coa_transaction_debit + $from[account_detail_credit], coa_transaction_credit = coa_transaction_credit + $from[account_detail_credit], coa_transaction_payment = coa_transaction_payment + $from[account_detail_credit] WHERE coa_transaction_header_id = '$idHeader'");
							// $this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $from[account_detail_credit] WHERE coa_id = '$deposit[id]'");
							// $this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $from[account_detail_credit] WHERE coa_id = '$deposit[id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $from['account_detail_date'] . "', '-', '')");
							// $this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $from[account_detail_credit] WHERE coa_id = '$piutang[id]'");
							// $this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $from[account_detail_credit] WHERE coa_id = '$piutang[id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $from['account_detail_date'] . "', '-', '')");

							$path = $_FILES['transfer_' . $index]['name'];
							$ext = pathinfo($path, PATHINFO_EXTENSION);
							$config['upload_path']          = './upload/TRANSFER/';
							$config['allowed_types']        = '*';
							$config['file_name']        = $exec . "." . $ext;
							//$this->upload->initialize($config);
							if ($xi > 0) {
								$this->upload->initialize($config);
							} else {
								$this->load->library('upload', $config);
							}
							$this->load->library('upload', $config);
							if (!$this->upload->do_upload('transfer_' . $index)) {
								//print_r($this->upload->display_errors());
								//die();
								$error = array('error' => $this->upload->display_errors());
							} else {
								$xi++;
								//	print_r($this->upload->data());
								$dataxy['account_detail_proof'] = $exec . "." . $ext;
								$this->Model->update_detail($dataxy, ['account_detail_real_id' => $exec]);
							}
							$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');

							$to['account_id'] = $value;
							$to['account_detail_proof'] = @$dataxy['account_detail_proof'];
							$to['account_detail_id'] = $id;
							$to['account_detail_pic'] = $from['account_detail_pic'];
							$to['account_detail_credit'] = 0;
							$to['account_detail_transfer_name'] = $from['account_detail_transfer_name'];
							$to['account_detail_debit'] = $from['account_detail_credit'];
							$to['account_detail_pic'] = $from['account_detail_pic'];
							$to['account_detail_date'] = $from['account_detail_date'];
							$to['account_detail_category_id'] = 2;
							$to['account_detail_type_id'] = 1;
							$to['account_detail_user_id'] = $from['account_detail_user_id'];
							$to['account_detail_date_create'] = $from['account_detail_date_create'];
							$to_id = $this->Model->insert_detail($to);

							$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');

							//LOG
							$di = array(
								'account_detail_header_id' => $id,
								'user_id' => $_SESSION['user_id'],
								'account_detail_log_status' => 1,
								'account_detail_log_detail' => 'Payment',
								'account_detail_log_query' => $lq,
								'account_detail_log_date_create' => date('Y-m-d H:i:s'),
								'account_detail_log_type' => 0,
								'account_detail_log_credit' => $from['account_detail_credit'],
								'account_detail_log_debit' => $from['account_detail_credit']
							);
							$this->Model->insert_log($di);

							//LOG SALES DETAIL
							$dt_amount = $this->Model->get_table_where("*","t_account_detail_sales","account_detail_id = '$id'");
							if (count($dt_amount)>0) {
								$amount = $dt_amount[0]['account_detail_sales_amount'];
								$sales_id = $dt_amount[0]['sales_id'];
							} else {
								$amount = 0;
								$sales_id = 0;
							}
							$di = array(
								'account_detail_header_id' => $id,
								'user_id' => $_SESSION['user_id'],
								'account_detail_sales_status' => 1,
								'account_detail_sales_log_detail' => 'SO Detail',
								'account_detail_sales_log_query' => $lq,
								'account_detail_sales_type' => 1,
								'account_detail_sales_date_create' => date('Y-m-d H:i:s'),
								'account_detail_sales_amount' => $amount,
								'sales_id' => $sales_id 
							);
							$this->Model->insert_log_detail($di);
						}
					}
				}

				if ($this->input->post('allow') != '') {
					$allow = $this->input->post('allow');
					$ids = $this->input->post('ids');
					$product_id = $this->input->post('product_id');
				}
				$cekEx = 0;
				if (@$exec == '') {
					$cekEx = 1;
					$this->db->query("UPDATE m_account SET account_deposit = " . str_replace(',', '', $_POST['deposit']) . ", account_deposit_draft = 0, account_deposit_draft_activation = 0 WHERE account_id = " . $_POST['id'] . "");
					$from = array();
					$data_paid3 = array();
					$data_paid2 = array();
					$exec = '';
					$to = array();
					$from['account_detail_transfer_name'] = 'Deposit';
					$from['account_detail_date'] = date('Y-m-d');
					$from['account_detail_credit'] = 0;
					$from['account_id'] = $id2;
					$from['account_detail_pic'] = $_SESSION['user_fullname'];
					$from['account_detail_id'] = $id;
					$from['account_detail_debit'] = 0;
					$from['account_detail_category_id'] = 1;
					$from['account_detail_type_id'] = 1;
					$from['account_detail_user_id'] = $_SESSION['user_id'];
					$from['account_detail_date_create'] = date('Y-m-d H:i:s');
					//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
					//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
					$exec = $this->Model->insert_detail($from);
				}
				if(isset($_POST['paid'])){
					foreach ($_POST['paid'] as $index_paid => $value_paid) {
						$value_paid = str_replace(',', '', $value_paid);
						//if ($value_paid > 0) {
						$data_paid['account_detail_real_id'] = $exec;
						$data_paid['sales_id'] = $sales[$index_paid];
						$data_paid['account_detail_sales_amount'] = str_replace(',', '', $value_paid);
						$data_paid['account_detail_sales_date'] = $from['account_detail_date'];
						$data_paid['account_detail_id'] = $id;
						$account_detail_sales_id = $this->Model->insert_account_detal_sales($data_paid, $value_paid, $cekEx);
						if($cekEx == 1) $this->db->query("UPDATE t_account_detail SET account_detail_paid = account_detail_paid + $data_paid[account_detail_sales_amount] WHERE sales_id = $data_paid[sales_id]");
						if ($this->input->post('allow') != '') {
							if (isset($_POST['total_diskon_harga'])) $total_diskon_harga = $_POST['total_diskon_harga'];
							else $total_diskon_harga = 0;
							foreach ($allow[$index_paid] as $index2 => $value2) {
								$prod_kd = $this->db->get_where('produk', ['id_produk' => $product_id[$index_paid][$index2]])->row_array();
								$data = array();
								$data['account_detail_id'] = $id;
								$data['account_detail_real_id'] = $exec;
								$data['sales_id'] = $sales[$index_paid];
								$data['sales_detail_id'] = $index2;
								$data['product_id'] = $product_id[$index_paid][$index2];
								$data['account_detail_sales_product_allow'] = $value2;
								$data['account_detail_sales_id'] = $account_detail_sales_id;
									// $data['region_id'] = $_POST['input2']['kecamatan'];
									// $data['region_delivery_instance_id'] = $data_eksped['region_delivery_instance_id'];
									// $data['delivery_instance_id'] = $_POST['input2']['ekspedisi'];
									// $data['account_detail_sales_product_address'] = $_POST['input2']['full_address'];
								$data['account_detail_sales_product_delivery_cost'] = '';

								if (@$_POST['diskon_harga'][$sales[$index_paid]][$index2] !== null) {
									if ($_POST['discount_id'][$sales[$index_paid]][$index2] !== null) {
										$discount = $_POST['diskon_harga'][$sales[$index_paid]][$index2];
										$terms = $this->db->get_where('m_discount_terms_product', ['discount_id' => $_POST['discount_id'][$sales[$index_paid]][$index2], 'product_kd' => $prod_kd['kd_pd']])->row_array();
										if ($terms !== null && $value2 > 0) {
											$sales_discount_terms = $this->db->query("SELECT B.sales_discount_terms_id, A.sales_discount_id FROM t_sales_discount A JOIN t_sales_discount_terms B ON B.sales_discount_id = A.sales_discount_id AND B.product_kd = '$prod_kd[kd_pd]' WHERE A.sales_id = $sales[$index_paid] AND A.discount_id = $terms[discount_id]")->row_array();
											if ($terms['discount_terms_product_quantity'] > 0) {
												$disc_term = floor($value2 / $terms['discount_terms_product_quantity']);
												$disc_mod = $value2 - ($terms['discount_terms_product_quantity'] * $disc_term);
											} else if ($terms['discount_terms_product_price'] > 0) {
												$disc_mod = fmod($terms['discount_terms_product_price'], $value2);
												$disc_term = intdiv($terms['discount_terms_product_price'], $value2);
												if ($disc_mod > 0) {
													$disc_mod = $value2 - $disc_term;
												}
											} else {
												$disc_term = 0;
												$disc_mod = 0;
											}
											if ($disc_mod > 0) {
												$data['account_detail_sales_product_discount'] = floor($discount / ($value2 - 1));
												$data['account_detail_sales_product_allow'] = $value2 - 1;
												$datax = ([
													'account_detail_id' => $id,
													'account_detail_real_id' => $exec,
													'sales_id' => $sales[$index_paid],
													'sales_detail_id' => $index2,
													'product_id' => $product_id[$index_paid][$index2],
													'account_detail_sales_product_allow' => 1,
													'account_detail_sales_id' => $account_detail_sales_id,
														// 'region_id' => $_POST['input2']['kecamatan'],
														// 'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
														// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
														// 'account_detail_sales_product_address' => $_POST['input2']['full_address'],
													'account_detail_sales_product_delivery_cost' => '',
													'account_detail_sales_product_discount' => $discount - ($data['account_detail_sales_product_discount'] * ($value2 - 1)),
													'account_detail_sales_product_discount_total' => $discount - ($data['account_detail_sales_product_discount'] * ($value2 - 1)),
													'sales_discount_terms_id' => $sales_discount_terms['sales_discount_terms_id']
												]);
												$this->db->insert("t_account_detail_sales_product", $datax);
												if ($cekEx == 1) {
													$discountx = $this->db->get_where('t_discount_total', ['account_detail_id' => $datax['account_detail_id']])->row_array();
													if ($discountx['discount_total_price'] == null) $discountx['discount_total_price'] = 0;
													$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
													$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow], discount_price = discount_price + $discountx[discount_total_price] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
													$this->Model->check_fifo_alpha($datax['product_id']);
													$this->Model->check_fifo_beta($datax['product_id']);
													$this->Model->check_fifo_main($datax['product_id'], $account['seller_id']);
													$this->Model->set_sales_detail();
													$discountx = '';
												}
												$data['account_detail_sales_product_discount_total'] = $discount - $datax['account_detail_sales_product_discount_total'];
											} else {
												$data['account_detail_sales_product_discount'] = $discount / $value2;
												$data['account_detail_sales_product_discount_total'] = $discount;
											}

											$data['account_detail_sales_product_terms_quantity'] = ($terms['discount_terms_product_quantity'] * $disc_term);
											$data['account_detail_sales_product_terms_mod'] = $disc_mod;
											$data['sales_discount_terms_id'] = $sales_discount_terms['sales_discount_terms_id'];
										}
									}
								}
								$total_harga_prod = $value2 * $_POST['harga_produk'][$sales[$index_paid]][$index2];
								$hrgprd[$prod_kd['kd_pd']][$sales[$index_paid]] = $total_harga_prod;
								if (@$_POST['diskon_persen'][$sales[$index_paid]][$index2] !== null && @$_POST['diskon_persen'][$sales[$index_paid]][$index2] > 0) {
									if ($_POST['discount_id'][$sales[$index_paid]][$index2] !== '') {
										$discount = $_POST['diskon_persen'][$sales[$index_paid]][$index2];
										$terms = $this->db->get_where('m_discount_terms_product', ['discount_id' => $_POST['discount_id'][$sales[$index_paid]][$index2], 'product_kd' => $prod_kd['kd_pd']])->row_array();
										if ($terms !== null && $value2 > 0) {
											$sales_discount_terms = $this->db->query("SELECT B.sales_discount_terms_id, A.sales_discount_id FROM t_sales_discount A JOIN t_sales_discount_terms B ON B.sales_discount_id = A.sales_discount_id AND B.product_kd = '$prod_kd[kd_pd]' WHERE A.sales_id = $sales[$index_paid] AND A.discount_id = $terms[discount_id]")->row_array();
											$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_active_draft = 1 WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]");
											$tSalesDiscount = $this->db->query("SELECT * FROM  t_sales_discount WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]")->row_array();
											if ($terms['discount_terms_product_quantity'] > 0) {
												$disc_term = floor($value2 / $terms['discount_terms_product_quantity']);
												$disc_mod = $value2 - ($terms['discount_terms_product_quantity'] * $disc_term);
											} else if ($terms['discount_terms_product_price'] > 0) {
												$disc_mod = fmod($terms['discount_terms_product_price'], $value2);
												$disc_term = intdiv($terms['discount_terms_product_price'], $value2);
												if ($disc_mod > 0) {
													$disc_mod = $value2 - $disc_term;
												}
											} else {
												$disc_term = 0;
												$disc_mod = 0;
											}
											if($value2 > 1){
												$data['account_detail_sales_product_discount'] = floor($discount / ($value2 - 1));
											}else{
												$data['account_detail_sales_product_discount'] = floor($discount / $value2);
											}
											if ($tSalesDiscount['sales_discount_percentage_hoard_draft'] > 0 || $tSalesDiscount['sales_discount_percentage_hoard'] > 0) {
												$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard_draft = 0 WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]");
												$data['account_detail_sales_product_discount_total'] = $data['account_detail_sales_product_discount'] * ($value2 - 1);
												$data['account_detail_sales_product_allow'] = $value2 - 1;
												$datax = ([
													'account_detail_id' => $id,
													'account_detail_real_id' => $exec,
													'sales_id' => $sales[$index_paid],
													'sales_detail_id' => $index2,
													'product_id' => $product_id[$index_paid][$index2],
													'account_detail_sales_product_allow' => 1,
													'account_detail_sales_id' => $account_detail_sales_id,
														// 'region_id' => $_POST['input2']['kecamatan'],
														// 'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
														// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
														// 'account_detail_sales_product_address' => $_POST['input2']['full_address'],
													'account_detail_sales_product_delivery_cost' => '',
													'account_detail_sales_product_discount' => $discount - $data['account_detail_sales_product_discount_total'],
													'account_detail_sales_product_discount_total' => $discount - $data['account_detail_sales_product_discount_total'],
													'sales_discount_terms_id' => $sales_discount_terms['sales_discount_terms_id']
												]);
												$this->db->insert("t_account_detail_sales_product", $datax);
												if ($cekEx == 1) {
													$discountx = $this->db->get_where('t_discount_total', ['account_detail_id' => $datax['account_detail_id']])->row_array();
													if ($discountx['discount_total_price'] == null) $discountx['discount_total_price'] = 0;
													$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
													$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow], discount_price = discount_price + $discountx[discount_total_price] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
													$this->Model->check_fifo_alpha($datax['product_id']);
													$this->Model->check_fifo_beta($datax['product_id']);
													$this->Model->check_fifo_main($datax['product_id'], $account['seller_id']);
													$this->Model->set_sales_detail();
													$discountx = '';
												}
											}
										}
									}
								}
								$this->Model->insert_account_detail_sales_product($data);
								$lq = $this->db->last_query();

								if (@$data['product_id']!="") {
									$dtprdglb = $this->Model->get_table_where("*","produk_global","kode = '".$prod_kd[0]['kd_pd']."'");
									if (count($dtprdglb)>0) {
										$product_global_id = $dtprdglb[0]['id'];
										$product_global_code = $dtprdglb[0]['kode'];
										$product_global_name = $dtprdglb[0]['nama_produk'];
									} else {
										$product_global_id = "";
										$product_global_code = "";
										$product_global_name = "";
									}
								} else {
									$product_global_id = "";
									$product_global_code = "";
									$product_global_name = "";
								}

								$dtharga = $this->Model->get_table_where("*","t_account_detail_sales_product","account_detail_sales_id = '".$account_detail_sales_id."' and product_id = '".@$data['product_id']."'");
								if (count($dtharga)>0) {
									$harga = $dtharga[0]['account_detail_sales_product_price'];
								} else {
									$harga = 0;
								}

								//LOG SALES PRODUCT
								$di = array(
									'account_detail_sales_id' => $account_detail_sales_id,
									'user_id' => $_SESSION['user_id'],
									'account_detail_sales_product_status' => 1,
									'account_detail_sales_product_log_detail' => 'SO Produk',
									'account_detail_sales_product_log_query' => $lq,
									'account_detail_sales_product_date_create' => date('Y-m-d H:i:s'),
									'account_detail_sales_product_type' => 1,
									'product_id' => @$data['product_id'],
									'product_global_id' => $product_global_id,
									'product_global_code' => $product_global_code,
									'product_global_name' => $product_global_name,
									'account_detail_sales_product_allow' => $value2,
									'account_detail_sales_product_price' => $harga,
									'account_detail_sales_product_total_price' => $harga*$value2
								);
								$this->Model->insert_log_detail_product($di);

								if ($cekEx == 1) {
									$discount = $this->db->get_where('t_discount_total', ['account_detail_id' => $data['account_detail_id']])->row_array();
									if ($discount['discount_total_price'] == null) $discount['discount_total_price'] = 0;
									$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $data[account_detail_sales_product_allow] WHERE product_id = $data[product_id] AND sales_detail_id = $data[sales_detail_id]");
									$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $data[account_detail_sales_product_allow], discount_price = discount_price + $discount[discount_total_price] WHERE product_id = $data[product_id] AND sales_detail_id = $data[sales_detail_id]");
									$this->Model->check_fifo_alpha($data['product_id']);
									$this->Model->check_fifo_beta($data['product_id']);
									$this->Model->check_fifo_main($data['product_id'], $account['seller_id']);
									$this->Model->set_sales_detail();
									$discount = '';
								}
							}
						}
						//}
					}
				}

				if (isset($_POST['sisa_produk'])) {
					foreach ($_POST['sisa_produk'] as $index1 => $sisa_produk) {
						foreach ($sisa_produk as $index2 => $sisa) {
							$sales_dc = $this->db->get_where('t_sales_discount_terms', ['sales_discount_terms_id' => $index2])->row_array();
							$sales_disc = $this->db->get_where('t_sales_discount', ['sales_discount_id' => $sales_dc['sales_discount_id']])->row_array();
							if ($sales_disc['sales_discount_percentage_active'] !== 1 && $hrgprd[$sales_dc['product_kd']][$sales_disc['sales_id']] !== null) {
								if ($cekEx == 1) {
									$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod = $sisa, sales_discount_terms_accumulation = " . $_POST['produk_accum'][$index1][$index2] . ", sales_discount_terms_mod_price = " . $hrgprd[$sales_dc['product_kd']][$sales_disc['sales_id']] . " WHERE sales_discount_terms_id = $index2");
								} else {
									$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod_draft = $sisa, sales_discount_terms_accumulation_draft = " . $_POST['produk_accum'][$index1][$index2] . ", sales_discount_terms_mod_price_draft = " . $hrgprd[$sales_dc['product_kd']][$sales_disc['sales_id']] . " WHERE sales_discount_terms_id = $index2");
								}
								$this->db->insert('t_account_detail_sales_terms', ['sales_discount_terms_id' => $index2, 'sales_discount_id' => $sales_dc['sales_discount_id'], 'account_detail_sales_product_terms_mod' => $sisa, 'account_detail_sales_product_terms_accumulation' => $_POST['produk_accum'][$index1][$index2]]);
							}
						}
					}
				}
				if (isset($_POST['dikson_produk'])) {
					foreach ($_POST['dikson_produk'] as $index => $dikson_produks) {
						foreach ($dikson_produks as $index2 => $dikson_produk) {
							$sales_product = $this->db->get_where('t_sales_discount_product', ['sales_detail_id' => $index2])->row_array();
							$product = $this->db->get_where('produk', ['kd_pd' => $sales_product['product_kd']])->row_array();
							$productPrice = $this->db->query("SELECT * FROM m_price WHERE produk_global_kd = '$sales_product[product_kd]' AND schema_id = '$member[schema_id]' AND price_unit_price > 0 ORDER BY price_unit_price ASC LIMIT 1")->row_array();
							$detailSalesProd = ([
								'account_detail_real_id' => $exec,
								'sales_id' => $index,
								'product_id' => $product['id_produk'],
								'account_detail_sales_product_allow' => $dikson_produk,
								'account_detail_sales_id' => $account_detail_sales_id,
								'sales_detail_id' => $index2,
								'account_detail_id' => $id,
								// 'region_id' => $_POST['input2']['kecamatan'],
								// 'region_delivery_instance_id' =>  $data_eksped['region_delivery_instance_id'],
								// 'delivery_instance_id' =>  $_POST['input2']['ekspedisi'],
								// 'account_detail_sales_product_address' =>  $_POST['input2']['full_address'],
								'account_detail_sales_product_delivery_cost' =>  '',
								// 'account_detail_sales_product_price' =>  $productPrice['price_unit_price'],
								'account_detail_sales_product_discount' =>  $productPrice['price_unit_price'],
								'account_detail_sales_product_discount_total' =>  $productPrice['price_unit_price'] * $dikson_produk,
							]);
							$this->db->insert('t_account_detail_sales_product', $detailSalesProd);
							if ($cekEx == 1) {
								$discount = $this->db->get_where('t_discount_total', ['account_detail_id' => $detailSalesProd['account_detail_id']])->row_array();
								if ($discount['discount_total_price'] == null) $discount['discount_total_price'] = 0;
								$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $detailSalesProd[account_detail_sales_product_allow] WHERE product_id = $detailSalesProd[product_id] AND sales_detail_id = $detailSalesProd[sales_detail_id]");
								$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $detailSalesProd[account_detail_sales_product_allow], discount_price = discount_price + $discount[discount_total_price] WHERE product_id = $detailSalesProd[product_id] AND sales_detail_id = $detailSalesProd[sales_detail_id]");
								$this->Model->check_fifo_alpha($detailSalesProd['product_id']);
								$this->Model->check_fifo_beta($detailSalesProd['product_id']);
								$this->Model->check_fifo_main($detailSalesProd['product_id'], $account['seller_id']);
								$this->Model->set_sales_detail();
								$discount = '';
							}
						}
					}
				}
				if (isset($_POST['promo_product'])) {
					if (isset($dataDiscountTot)) {
					} else {
						$dataDiscountTot = ([
							'coa_group_id' => $idHeader,
							'discount_total_price' => 0,
							'account_detail_id' => $id,
							'seller_id' => $_POST['seller_id'],
						]);
						$dataDiscountTot['discount_total_id'] = $this->db->insert('t_discount_total', $dataDiscountTot);
					}
					$dataSales['sales_code'] = $this->generate_code();
					$dataSales['user_id'] = $this->session->userdata('user_id');
					$dataSales['sales_date_create'] = date('Y-m-d H:i:s');
					$dataSales['sales_category_id'] = $_SESSION['sales_category_id'];
					$dataSales['sales_date'] = date('Y-m-d');
					$dataSales['seller_id'] = $_POST['seller_id'];
					//if($data['sales_address'] == "") $data['sales_address'] = $this->Model->get_seller_address($data['seller_id']); 
					$this->Model->chk_account($dataSales['seller_id']);
					$this->db->insert('T_sales', $dataSales);
					$sales_id = $this->db->insert_id();
					$data_paid = array();
					$data_paid['account_detail_real_id'] = $exec;
					$data_paid['sales_id'] = $dataSales['sales_id'];
					$data_paid['account_detail_sales_amount'] = 0;
					$data_paid['account_detail_sales_date'] = date('Y-m-d');
					$data_paid['account_detail_id'] = $id;
					$account_detail_sales_id = $this->Model->insert_account_detal_sales($data_paid, 0, 0);
					foreach ($_POST['promo_product'] as $index => $promo_product) {
						$product = $this->db->get_where('produk', ['kd_pd' => $index])->row_array();
						$price_prod = $this->db->query("SELECT * FROM m_price WHERE produk_global_kd = $index AND schema_id = '$member[schema_id]' AND (price_quantity <= $promo_product OR price_quantity >= $promo_product) ORDER BY price_quantity ASC")->row_array();
						$data = array();
						$data['account_detail_id'] = $id;
						$data['account_detail_real_id'] = $exec;
						$data['sales_id'] = $sales_id;
						// $data['sales_detail_id'] = $index2;
						$data['product_id'] = $product['id_produk'];
						$data['account_detail_sales_product_allow'] = $promo_product;
						// $data['region_id'] = $_POST['input2']['kecamatan'];
						// $data['region_delivery_instance_id'] = $data_eksped['region_delivery_instance_id'];
						// $data['delivery_instance_id'] = $_POST['input2']['ekspedisi'];
						// $data['account_detail_sales_product_address'] = $_POST['input2']['full_address'];
						$data['account_detail_sales_product_delivery_cost'] = '';
						$this->Model->insert_account_detail_sales_product($data);
						$price = $price_prod['price_unit_price'] * $promo_product;
						$this->db->query("UPDATE t_discount_total SET discount_total_price = discount_total_price + $price WHERE discount_total_id = $dataDiscountTot[discount_total_id]");
					}
				}
				if (isset($_POST['tot_hoard'])) {
					foreach ($_POST['tot_hoard'] as $index => $tot_hoard) {
						if ($cekEx == 1) {
							$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard = sales_discount_percentage_hoard + $tot_hoard WHERE sales_discount_id = $index");
						} else {
							$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard_draft = sales_discount_percentage_hoard_draft + $tot_hoard WHERE sales_discount_id = $index");
						}
					}
				}

				$arrx = $this->Model->get_account($id2);
				if (isset($_POST['dropship'])) {

					$productdropship = array();
					foreach ($_POST['dropship']['name'] as $index => $dropship) {
						$arrproductdropship = array();
						$data = ([
							'account_detail_id' => $id,
							'account_detail_real_id' => $exec,
							'region_id' => $_POST['dropship']['kecamatan'][$index],
							'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
							'delivery_instance_id' => $_POST['dropship']['ekspedisi'][$index],
							'account_detail_delivery_address' => $_POST['dropship']['full_address'][$index],
							'account_detail_delivery_cost' => '',
						]);
						$this->db->insert('t_account_detail_delivery', $data);
						$data['account_detail_delivery_id'] = $this->db->insert_id();

						foreach ($_POST['dropship']['produk'][$index] as $index2 => $produk) {
							@$arrproductdropship[$produk] += $_POST['dropship']['jumlah'][$index][$index2];
							// if ($productdropship[$produk] == null) {
							// 	$productdropship[$produk] = 0;
							// }
							@$productdropship[$produk] += $_POST['dropship']['jumlah'][$index][$index2];
						}
						foreach ($arrproductdropship as $index3 => $productdrop) {
							$data2 = ([
								'account_detail_delivery_id' => $data['account_detail_delivery_id'],
								'account_detail_id' => $id,
								'account_detail_real_id' => $exec,
								'product_id' => $index3,
								'account_detail_delivery_product_quantity' => $productdrop,
								'account_detail_delivery_product_cost' => '',
							]);
							$this->db->insert('t_account_detail_delivery_product', $data2);
						}
					}
					$i = 0;
					foreach ($productdropship as $index4 => $proddrop) {
						if ($_POST['total_product'][$index4] > $proddrop) {
							$i++;
						}
					}
					if ($i > 0) {
						$data3 = ([
							'account_detail_id' => $id,
							'account_detail_real_id' => $exec,
							// 'region_id' => $_POST['input2']['kecamatan'],
							'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
							// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
							// 'account_detail_delivery_address' => $_POST['input2']['full_address'],
							'account_detail_delivery_cost' => '',
						]);
						$this->db->insert('t_account_detail_delivery', $data3);
						$data3['account_detail_delivery_id'] = $this->db->insert_id();

						foreach ($productdropship as $index5 => $proddrop) {
							if ($_POST['total_product'][$index5] > $proddrop) {
								$data4 = ([
									'account_detail_delivery_id' => $data3['account_detail_delivery_id'],
									'account_detail_id' => $id,
									'account_detail_real_id' => $exec,
									'product_id' => $index5,
									'account_detail_delivery_product_quantity' => ($_POST['total_product'][$index5] - $proddrop),
									'account_detail_delivery_product_cost' => '',
								]);
								$this->db->insert('t_account_detail_delivery_product', $data4);
							}
						}
					}
				} else {
					$data3 = ([
						'account_detail_id' => $id,
						'account_detail_real_id' => $exec,
						// 'region_id' => $_POST['input2']['kecamatan'],
						'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
						// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
						// 'account_detail_delivery_address' => $_POST['input2']['full_address'],
						'account_detail_delivery_cost' => '',
					]);
					$this->db->insert('t_account_detail_delivery', $data3);
					$data3['account_detail_delivery_id'] = $this->db->insert_id();

					foreach ($_POST['total_product'] as $index5 => $proddrop) {
						if ($index5 > 0) {
							$data4 = ([
								'account_detail_delivery_id' => $data3['account_detail_delivery_id'],
								'account_detail_id' => $id,
								'account_detail_real_id' => $exec,
								'product_id' => $index5,
								'account_detail_delivery_product_quantity' => $proddrop,
								'account_detail_delivery_product_cost' => '',
							]);
							$this->db->insert('t_account_detail_delivery_product', $data4);
						}
					}
				}
				// $accountx['account_deposit'] = ((int)@$arrx['account_deposit']) + str_replace(',', '', $_POST['deposit']);
				// $this->Model->update($accountx, [$this->id_ => $id2]);

				// $allow = $this->input->post('allow');
				// $ids = $this->input->post('ids');
				// print_r($ids);
				// die;
				// $product_id = $this->input->post('product_id');
				// foreach ($allow as $index => $arrvalue) {
				// 	foreach ($arrvalue as $index2 => $value2) {
				// 		//if($value2 > 0){
				// 		$data['account_detail_id'] = $id;
				// 		$data['account_detail_real_id'] = $exec;
				// 		$data['sales_id'] = $index;
				// 		$data['sales_detail_id'] = $index2;
				// 		$data['product_id'] = $product_id[$index][$index2];
				// 		$data['account_detail_sales_product_allow'] = $value2;
				// 		$data['account_detail_sales_id'] = $ids[$index][$index2];
				// 		$this->Model->insert_account_detail_sales_product($data);
				// 		//} 
				// 	}
				// }
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		//die();  
		// foreach ($_POST['sales_id_all'] as $index_p => $update_p) {
		// 	$this->db->query('UPDATE t_account_detail A SET account_detail_paid = IFNULL((SELECT SUM(account_detail_sales_amount) FROM t_account_detail_sales WHERE sales_id = "$update_p"), 0) WHERE A.sales_id = "$update_p"');
		// }
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_ . "/seller"));
	}

	public function generate_code()
	{

		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWSO-";
		$dbDate	=	$this->db->query("SELECT COUNT(sales_id) as JumlahTransaksi FROM t_sales WHERE LEFT(sales_date_create, 7) = '" . date('Y-m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 2;
			}
		} else {
			$nJumlahTransaksi = 1;
		}
		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0000' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '000' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 4) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 5) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}
		return $cKode;
	}

	public function set_transfer_seller_parsial()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		if ($this->input->post('simpan')) {
			$from2 = $this->input->post('from');

			$to = $this->input->post('to');
			$id2 = $this->input->post('id');
			$sales = $this->input->post('sales');
			$account_id = $this->input->post('account_id');
			$account = $this->db->query("SELECT * FROM m_account WHERE account_id = '$id2'")->row_array();
			$this->db->query("UPDATE m_account SET account_deposit_draft = " . str_replace(",", "", $_POST['deposit2']) . ", account_deposit_draft_activation = 1 WHERE account_id = '$id2'");
			$piutang = $this->db->query("SELECT * FROM coa_4 WHERE kode = '100.1.3.$account[account_code2]'")->row_array();
			$deposit = $this->db->query("SELECT * FROM coa_4 WHERE kode = '200.1.2.$account[account_code2]'")->row_array();
			$data_eksped = ([
				'region_id' => $_POST['input2']['kecamatan'],
				'delivery_instance_id' => $_POST['input2']['ekspedisi'],
				'region_delivery_instance_cost' => ''
			]);
			$this->db->insert('m_region_delivery_instance', $data_eksped);
			$data_eksped['region_delivery_instance_id'] = $this->db->insert_id();
			$this->session->set_userdata(['region_delivery_instance_id' => $data_eksped['region_delivery_instance_id']]);
			$this->session->set_userdata(['full_address' => $_POST['input2']['full_address']]);

			$dataHeader = ([
				'coa_transaction_date' => date('Y-m-d'),
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_deposit' => str_replace(',', '', $_POST['deposit2']),
			]);
			$this->db->insert('t_coa_transaction_header', $dataHeader);
			$idHeader = $this->db->insert_id();
			if ($_POST['parameter'] == "tambah") {
				foreach ($account_id as $index => $value) {
					if ($from2['account_detail_credit'][$index] > 0) {
						$from = array();
						$data_paid3 = array();
						$data_paid2 = array();
						$exec = '';
						$to = array();
						$from['account_detail_transfer_name'] = $from2['account_detail_transfer_name'][$index];
						$from['account_detail_date'] = $from2['account_detail_date'][$index];
						$from['account_detail_credit'] = $from2['account_detail_credit'][$index];
						$from['account_detail_credit'] = str_replace(',', '', $from['account_detail_credit']);
						$from['account_id'] = $id2;
						$from['account_detail_pic'] = $_SESSION['user_fullname'];
						$from['account_detail_id'] = $id;
						$from['account_detail_debit'] = 0;
						$from['account_detail_category_id'] = 1;
						$from['account_detail_type_id'] = 1;
						$from['account_detail_user_id'] = $_SESSION['user_id'];
						$from['account_detail_date_create'] = date('Y-m-d H:i:s');
						//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
						//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
						$exec = $this->Model->insert_detail($from);

						$data_paid3['coa_id'] = $piutang['id'];
						$data_paid3['coa_name'] = $piutang['nama'];
						$data_paid3['coa_code'] = $piutang['kode'];
						$data_paid3['coa_level'] = 4;
						$data_paid3['coa_date'] = $from['account_detail_date'];
						$data_paid3['coa_debit'] = $from['account_detail_credit'];
						$data_paid3['coa_credit'] = 0;
						$data_paid3['coa_transaction_note'] = 'Pembayaran ' . $from['account_detail_transfer_name'];
						$data_paid3['user_create'] = $_SESSION['user_id'];
						$data_paid3['coa_transaction_source'] = 1;
						$data_paid3['coa_transaction_source_id'] = $exec;
						$data_paid3['coa_transaction_realization'] = 0;
						$data_paid3['coa_group_id'] = $idHeader;
						$this->db->insert('t_coa_transaction_temp', $data_paid3);

						$data_paid2['coa_id'] = $deposit['id'];
						$data_paid2['coa_name'] = $deposit['nama'];
						$data_paid2['coa_code'] = $deposit['kode'];
						$data_paid2['coa_level'] = 4;
						$data_paid2['coa_date'] = $from['account_detail_date'];
						$data_paid2['coa_debit'] = 0;
						$data_paid2['coa_credit'] = $from['account_detail_credit'];
						$data_paid2['coa_transaction_note'] = 'Pembayaran ' . $from['account_detail_transfer_name'];
						$data_paid2['user_create'] = $_SESSION['user_id'];
						$data_paid2['coa_transaction_source'] = 1;
						$data_paid2['coa_transaction_source_id'] = $exec;
						$data_paid2['coa_transaction_realization'] = 0;
						$data_paid2['coa_group_id'] = $idHeader;
						$this->db->insert('t_coa_transaction_temp', $data_paid2);

						$this->db->query("UPDATE t_coa_transaction_header SET coa_transaction_debit = coa_transaction_debit + $from[account_detail_credit], coa_transaction_credit = coa_transaction_credit + $from[account_detail_credit], coa_transaction_payment = coa_transaction_payment + $from[account_detail_credit] WHERE coa_transaction_header_id = '$idHeader'");


						$path = $_FILES['transfer_' . $index]['name'];
						$ext = pathinfo($path, PATHINFO_EXTENSION);
						$config['upload_path']          = './upload/TRANSFER/';
						$config['allowed_types']        = '*';
						$config['file_name']        = $exec;
						$this->load->library('upload', $config);
						if (!$this->upload->do_upload('transfer_' . $index)) {
							//print_r($this->upload->display_errors());
							//die();
							$error = array('error' => $this->upload->display_errors());
						} else {
							$this->upload->data();
							$dataxy['account_detail_proof'] = $exec . "." . $ext;
							$this->Model->update_detail($dataxy, ['account_detail_real_id' => $exec]);
						}
						$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');

						/*if($from['account_detail_deposit'] > 0){
						$datayyyx['account_detail_deposit_amount'] = $from['account_detail_deposit'];
						$datayyyx['account_detail_real_id'] = $insert_id;
						$datayyyx['account_id'] = $from['account_id'];
						$this->Model->insert_account_detail_deposit($datayyyx);
					}*/
					$to['account_id'] = $value;
					$to['account_detail_proof'] = @$dataxy['account_detail_proof'];
					$to['account_detail_id'] = $id;
					$to['account_detail_pic'] = $from['account_detail_pic'];
					$to['account_detail_credit'] = 0;
					$to['account_detail_transfer_name'] = $from['account_detail_transfer_name'];
					$to['account_detail_debit'] = $from['account_detail_credit'];
					$to['account_detail_pic'] = $from['account_detail_pic'];
					$to['account_detail_date'] = $from['account_detail_date'];
					$to['account_detail_category_id'] = 2;
					$to['account_detail_type_id'] = 1;
					$to['account_detail_user_id'] = $from['account_detail_user_id'];
					$to['account_detail_date_create'] = $from['account_detail_date_create'];
					$to_id = $this->Model->insert_detail($to);
					$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
				}
			}
			$cekEx = 'non';
			if (@$exec == '') {
				$cekEx = 'dep';
				$this->db->query("UPDATE m_account SET account_deposit = account_deposit_draft, account_deposit_draft = 0 WHERE account_id = '$id2'");
				$from = array();
				$data_paid3 = array();
				$data_paid2 = array();
				$exec = '';
				$to = array();
				$from['account_detail_transfer_name'] = 'Deposit';
				$from['account_detail_date'] = date('Y-m-d');
				$from['account_detail_credit'] = 0;
				$from['account_id'] = $id2;
				$from['account_detail_pic'] = $_SESSION['user_fullname'];
				$from['account_detail_id'] = $id;
				$from['account_detail_debit'] = 0;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
					//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
					//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
				$exec = $this->Model->insert_detail($from);
			}
			foreach ($_POST['paid'] as $index_paid => $value_paid) {
				if ($value_paid > 0) {
					$value_paid = str_replace(',', '', $value_paid);
					$data_paid['account_detail_real_id'] = $exec;
					$data_paid['sales_id'] = $sales[$index_paid];
					$data_paid['account_detail_target_real_id'] = $index_paid;
					$data_paid['account_detail_sales_amount'] = $value_paid;
					$data_paid['account_detail_sales_date'] = $from['account_detail_date'];
					$data_paid['account_detail_id'] = $id;
					$id_datapaid = $this->Model->insert_account_detal_sales($data_paid, $value_paid, $cekEx);
					if($cekEx == 'dep') $this->db->query("UPDATE t_account_detail SET account_detail_paid = account_detail_paid + $data_paid[account_detail_sales_amount] WHERE sales_id = $data_paid[sales_id]");
				}
			}

			$arrx = $this->Model->get_account($id2);
				// $accountx['account_deposit'] = ((int)@$arrx['account_deposit']) + str_replace(',', '', $_POST['deposit']);
				// $this->Model->update($accountx, [$this->id_ => $id2]);
		} else {
			$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
			$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
		}
	}
	if ($this->db->trans_status() === FALSE) {
		$this->db->trans_rollback();
	} else {
		$this->db->trans_commit();
	}
	redirect(site_url($this->url_ . "/form_sales_allow/" . $id . "/" . $id2 . "/" . $cekEx));
}

public function simpan_sales_allow()
{
	$this->db->trans_begin();
	$allow = $this->input->post('allow');
	$id = $this->input->post('id');
	$ids = $this->input->post('ids');
	$product_id = $this->input->post('product_id');
	$this->db->query("UPDATE m_account SET account_deposit_draft = account_deposit_draft + " . str_replace(',', '', $_POST['deposit']) . ", account_deposit_draft_activation = 1 WHERE account_id = '$_POST[idAccount]'");
		// $ekspedisi = $this->db->get_where('m_region_delivery_instance', ['region_delivery_instance_id' => $_SESSION['region_delivery_instance_id']])->row_array();
		// $full_address = $_SESSION['full_address'];
		// $this->session->unset_userdata($_SESSION['full_address']);
		// $this->session->unset_userdata($_SESSION['region_delivery_instance_id']);
	foreach ($allow as $index => $arrvalue) {
		if (isset($_POST['total_diskon_harga'])) $total_diskon_harga = $_POST['total_diskon_harga'];
		else $total_diskon_harga = 0;
		foreach ($arrvalue as $index2 => $value2) {
			$value2 = str_replace(",", "", $value2);
			$prod_kd = $this->db->get_where('produk', ['id_produk' => $product_id[$index][$index2]])->row_array();
			$account = $this->db->get_where('m_account', ['account_id' => $_POST['idAccount']])->row_array();
			$member = $this->db->get_where('member', ['kode' => $account['seller_id']])->row_array();
			$data = array();
			$data['account_detail_id'] = $id;
			$data['account_detail_real_id'] = $_POST['real_id'][$index][$index2];
			$data['sales_id'] = $index;
			$data['sales_detail_id'] = $index2;
			$data['product_id'] = $product_id[$index][$index2];
			$data['account_detail_sales_product_allow'] = $value2;
			$data['account_detail_sales_id'] = $ids[$index][$index2];
				// $data['region_id'] = $ekspedisi['region_id'];
				// $data['region_delivery_instance_id'] = $ekspedisi['region_delivery_instance_id'];
				// $data['delivery_instance_id'] = $ekspedisi['delivery_instance_id'];
				// $data['account_detail_sales_product_address'] = $full_address;
				// $data['account_detail_sales_product_delivery_cost'] = '';
			if (@$_POST['diskon_harga'][$index][$index2] !== null) {
				if ($_POST['discount_id'][$index][$index2] !== null) {
					$discount = $_POST['diskon_harga'][$index][$index2];
					$terms = $this->db->get_where('m_discount_terms_product', ['discount_id' => $_POST['discount_id'][$index][$index2], 'product_kd' => $prod_kd['kd_pd']])->row_array();
					if ($terms !== null && $value2 > 0) {
						$sales_discount_terms = $this->db->query("SELECT B.sales_discount_terms_id, A.sales_discount_id FROM t_sales_discount A JOIN t_sales_discount_terms B ON B.sales_discount_id = A.sales_discount_id AND B.product_kd = '$prod_kd[kd_pd]' WHERE A.sales_id = $index AND A.discount_id = $terms[discount_id]")->row_array();
						if ($terms['discount_terms_product_quantity'] > 0) {
							$disc_term = floor($value2 / $terms['discount_terms_product_quantity']);
							$disc_mod = $value2 - ($terms['discount_terms_product_quantity'] * $disc_term);
						} else if ($terms['discount_terms_product_price'] > 0) {
							$disc_mod = fmod($terms['discount_terms_product_price'], $value2);
							$disc_term = intdiv($terms['discount_terms_product_price'], $value2);
							if ($disc_mod > 0) {
								$disc_mod = $value2 - $disc_term;
							}
						} else {
							$disc_term = 0;
							$disc_mod = 0;
						}
						if ($disc_mod > 0) {
							$data['account_detail_sales_product_allow'] = $value2 - 1;
							$data['account_detail_sales_product_discount'] = floor($discount / ($value2 - 1));
							$datax = ([
								'account_detail_id' => $id,
								'account_detail_real_id' => $_POST['real_id'][$index][$index2],
								'sales_id' => $index,
								'sales_detail_id' => $index2,
								'product_id' => $product_id[$index][$index2],
								'account_detail_sales_product_allow' => 1,
								'account_detail_sales_id' => $ids[$index][$index2],
									// 'region_id' => $_POST['input2']['kecamatan'],
									// 'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
									// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
									// 'account_detail_sales_product_address' => $_POST['input2']['full_address'],
									// 'account_detail_sales_product_delivery_cost' => '',
								'account_detail_sales_product_discount' => $discount - ($data['account_detail_sales_product_discount'] * ($value2 - 1)),
								'account_detail_sales_product_discount_total' => $discount - ($data['account_detail_sales_product_discount'] * ($value2 - 1)),
								'sales_discount_terms_id' => $sales_discount_terms['sales_discount_terms_id']
							]);
							$this->db->insert("t_account_detail_sales_product", $datax);
							if ($_POST['cekDep'] == 'dep') {
								$discount = $this->db->get_where('t_discount_total', ['account_detail_id' => $datax['account_detail_id']])->row_array();
								if ($discount['discount_total_price'] == null) $discount['discount_total_price'] = 0;
								$account = $this->db->get_where('m_account', ['account_id' => $_POST['idAccount']])->row_array();
								$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
								$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow], discount_price = discount_price + $discount[discount_total_price] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
								$this->Model->check_fifo_alpha($datax['product_id']);
								$this->Model->check_fifo_beta($datax['product_id']);
								$this->Model->check_fifo_main($datax['product_id'], $account['seller_id']);
								$this->Model->set_sales_detail();
								$discount = '';
								$account = '';
							}
						} else {
							$data['account_detail_sales_product_discount'] = $discount / $value2;
						}
						$data['account_detail_sales_product_discount_total'] = $discount - $datax['account_detail_sales_product_discount_total'];
						$data['account_detail_sales_product_terms_quantity'] = ($terms['discount_terms_product_quantity'] * $disc_term);
						$data['account_detail_sales_product_terms_mod'] = $disc_mod;
						$data['sales_discount_terms_id'] = $sales_discount_terms['sales_discount_terms_id'];
					}
				}
			}
			$total_harga_prod = $value2 * $_POST['harga_produk'][$index][$index2];
			if (@$_POST['diskon_persen'][$index][$index2] !== null && @$_POST['diskon_persen'][$index][$index2] > 0) {
				if ($_POST['discount_id'][$index][$index2] !== '') {
					$discount = $_POST['diskon_persen'][$index][$index2];

					$terms = $this->db->get_where('m_discount_terms_product', ['discount_id' => $_POST['discount_id'][$index][$index2], 'product_kd' => $prod_kd['kd_pd']])->row_array();
					if ($terms !== null && $value2 > 0) {
						$sales_discount_terms = $this->db->query("SELECT B.sales_discount_terms_id, A.sales_discount_id FROM t_sales_discount A JOIN t_sales_discount_terms B ON B.sales_discount_id = A.sales_discount_id AND B.product_kd = '$prod_kd[kd_pd]' WHERE A.sales_id = $index AND A.discount_id = $terms[discount_id]")->row_array();
						$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_active_draft = 1 WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]");
						$tSalesDiscount = $this->db->query("SELECT * FROM  t_sales_discount WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]")->row_array();
						if ($terms['discount_terms_product_quantity'] > 0) {
							$disc_term = floor($value2 / $terms['discount_terms_product_quantity']);
							$disc_mod = $value2 - ($terms['discount_terms_product_quantity'] * $disc_term);
						} else if ($terms['discount_terms_product_price'] > 0) {
							$disc_mod = fmod($terms['discount_terms_product_price'], $value2);
							$disc_term = intdiv($terms['discount_terms_product_price'], $value2);
							if ($disc_mod > 0) {
								$disc_mod = $value2 - $disc_term;
							}
						} else {
							$disc_term = 0;
							$disc_mod = 0;
						}
						$data['account_detail_sales_product_discount'] = floor(@$_POST['list_prod_diskon'][$index][$prod_kd['kd_pd']] * $_POST['harga_produk'][$index][$index2] / 100);
						if ($tSalesDiscount['sales_discount_percentage_hoard_draft'] > 0) {
							$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard_draft = 0 WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]");
							$data['account_detail_sales_product_discount_total'] = $data['account_detail_sales_product_discount'] * ($value2 - 1);
							$data['account_detail_sales_product_allow'] = $value2 - 1;
							$datax = ([
								'account_detail_id' => $id,
								'account_detail_real_id' => $_POST['real_id'][$index][$index2],
								'sales_id' => $index,
								'sales_detail_id' => $index2,
								'product_id' => $product_id[$index][$index2],
								'account_detail_sales_product_allow' => 1,
								'account_detail_sales_id' => $ids[$index][$index2],
									// 'region_id' => $_POST['input2']['kecamatan'],
									// 'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
									// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
									// 'account_detail_sales_product_address' => $_POST['input2']['full_address'],
									// 'account_detail_sales_product_delivery_cost' => '',
								'account_detail_sales_product_discount' => $discount - $data['account_detail_sales_product_discount_total'],
								'account_detail_sales_product_discount_total' => $discount - $data['account_detail_sales_product_discount_total'],
								'sales_discount_terms_id' => $sales_discount_terms['sales_discount_terms_id']
							]);
							$this->db->insert("t_account_detail_sales_product", $datax);
							if ($_POST['cekDep'] == 'dep') {
								$discount = $this->db->get_where('t_discount_total', ['account_detail_id' => $datax['account_detail_id']])->row_array();
								if ($discount['discount_total_price'] == null) $discount['discount_total_price'] = 0;
								$account = $this->db->get_where('m_account', ['account_id' => $_POST['idAccount']])->row_array();
								$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
								$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow], discount_price = discount_price + $discount[discount_total_price] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
								$this->Model->check_fifo_alpha($datax['product_id']);
								$this->Model->check_fifo_beta($datax['product_id']);
								$this->Model->check_fifo_main($datax['product_id'], $account['seller_id']);
								$this->Model->set_sales_detail();
								$discount = '';
								$account = '';
							}
						} else {
							$data['account_detail_sales_product_discount_total'] = $data['account_detail_sales_product_discount'] * $value2;
						}
					}
				}
			}
			$this->Model->insert_account_detail_sales_product($data);
			if ($_POST['cekDep'] == 'dep') {
				$discount = $this->db->get_where('t_discount_total', ['account_detail_id' => $data['account_detail_id']])->row_array();
				if ($discount['discount_total_price'] == null) $discount['discount_total_price'] = 0;
				$account = $this->db->get_where('m_account', ['account_id' => $_POST['idAccount']])->row_array();
				$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $data[account_detail_sales_product_allow] WHERE product_id = $data[product_id] AND sales_detail_id = $data[sales_detail_id]");
				$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $data[account_detail_sales_product_allow], discount_price = discount_price + $discount[discount_total_price] WHERE product_id = $data[product_id] AND sales_detail_id = $data[sales_detail_id]");
				$this->Model->check_fifo_alpha($data['product_id']);
				$this->Model->check_fifo_beta($data['product_id']);
				$this->Model->check_fifo_main($data['product_id'], $account['seller_id']);
				$this->Model->set_sales_detail();
				$discount = '';
				$account = '';
			}
		}
	}
	if (isset($_POST['sisa_produk'])) {
		foreach ($_POST['sisa_produk'] as $index1 => $sisa_produk) {
			foreach ($sisa_produk as $index2 => $sisa) {
				$sales_dc = $this->db->get_where('t_sales_discount_terms', ['sales_discount_terms_id' => $index2])->row_array();
				$sales_disc = $this->db->get_where('t_sales_discount', ['sales_discount_id' => $sales_dc['sales_discount_id']])->row_array();
				if ($sales_disc['sales_discount_percentage_active'] !== 1) {
					if ($_POST['produk_accum'][$index1][$index2] == '') $_POST['produk_accum'][$index1][$index2] = 0;
					$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod_draft = $sisa, sales_discount_terms_accumulation_draft = " . $_POST['produk_accum'][$index1][$index2] . " WHERE sales_discount_terms_id = $index2");
					$this->db->insert('t_account_detail_sales_terms', ['sales_discount_terms_id' => $index2, 'sales_discount_id' => $sales_dc['sales_discount_id'], 'account_detail_sales_product_terms_mod' => $sisa, 'account_detail_sales_product_terms_accumulation' => $_POST['produk_accum'][$index1][$index2]]);
				}
			}
		}
	}

	if (isset($_POST['dikson_produk'])) {
		foreach ($_POST['dikson_produk'] as $index => $dikson_produks) {
			foreach ($dikson_produks as $index2 => $dikson_produk) {
				$sales_product = $this->db->get_where('t_sales_discount_product', ['sales_detail_id' => $index2])->row_array();
				$product = $this->db->get_where('produk', ['kd_pd' => $sales_product['product_kd']])->row_array();
				$productPrice = $this->db->query("SELECT * FROM m_price WHERE produk_global_kd = '$sales_product[product_kd]' AND schema_id = '$member[schema_id]' AND price_unit_price > 0 ORDER BY price_unit_price ASC LIMIT 1")->row_array();
				$detailSalesProd = ([
					'account_detail_real_id' =>  $data['account_detail_real_id'],
					'sales_id' => $index,
					'product_id' => $product['id_produk'],
					'account_detail_sales_product_allow' => $dikson_produk,
					'account_detail_sales_id' => $data['account_detail_sales_id'],
					'sales_detail_id' => $index2,
					'account_detail_id' => $id,
						// 'region_id' => $_POST['input2']['kecamatan'],
						// 'region_delivery_instance_id' =>  $data_eksped['region_delivery_instance_id'],
						// 'delivery_instance_id' =>  $_POST['input2']['ekspedisi'],
						// 'account_detail_sales_product_address' =>  $_POST['input2']['full_address'],
						// 'account_detail_sales_product_delivery_cost' =>  '',
						// 'account_detail_sales_product_price' =>  $productPrice['price_unit_price'],
					'account_detail_sales_product_discount' =>  $productPrice['price_unit_price'],
					'account_detail_sales_product_discount_total' =>  $productPrice['price_unit_price'] * $dikson_produk,
				]);
				$this->db->insert('t_account_detail_sales_product', $detailSalesProd);
				if ($_POST['cekDep'] == 'dep') {
					$discount = $this->db->get_where('t_discount_total', ['account_detail_id' => $detailSalesProd['account_detail_id']])->row_array();
					if ($discount['discount_total_price'] == null) $discount['discount_total_price'] = 0;
					$account = $this->db->get_where('m_account', ['account_id' => $_POST['idAccount']])->row_array();
					$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $detailSalesProd[account_detail_sales_product_allow] WHERE product_id = $detailSalesProd[product_id] AND sales_detail_id = $detailSalesProd[sales_detail_id]");
					$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $detailSalesProd[account_detail_sales_product_allow], discount_price = discount_price + $discount[discount_total_price] WHERE product_id = $detailSalesProd[product_id] AND sales_detail_id = $detailSalesProd[sales_detail_id]");
					$this->Model->check_fifo_alpha($detailSalesProd['product_id']);
					$this->Model->check_fifo_beta($detailSalesProd['product_id']);
					$this->Model->check_fifo_main($detailSalesProd['product_id'], $account['seller_id']);
					$this->Model->set_sales_detail();
					$discount = '';
					$account = '';
				}
			}
		}
	}

	if (isset($_POST['tot_hoard'])) {
		foreach ($_POST['tot_hoard'] as $index => $tot_hoard) {
			if ($_POST['cekDep'] == 'dep') {
				$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard = sales_discount_percentage_hoard + $tot_hoard WHERE sales_discount_id = $index");
			} else {
				$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard_draft = sales_discount_percentage_hoard_draft + $tot_hoard WHERE sales_discount_id = $index");
			}
		}
	}

	if ($this->db->trans_status() === FALSE) {
		$this->db->trans_rollback();
	} else {
		$this->db->trans_commit();
	}
	redirect(site_url($this->url_ . "/seller"));
}

public function simpan()
{
	if ($this->input->post('simpan')) {
		$data = $this->input->post('input');
		if ($_POST['parameter'] == "tambah") {
			$data[$this->eng_ . '_monthly_debit'] = $data[$this->eng_ . '_debit'];
			$data[$this->eng_ . '_monthly_credit'] = $data[$this->eng_ . '_credit'];
			$data[$this->eng_ . '_date_create'] = date('Y-m-d H:i:s');
			$exec = $this->Model->insert($data);
			$data2['account_id'] = $this->db->insert_id();
			$data2['account_reset_date'] = $data[$this->eng_ . '_date_reset'];
			$data2['account_reset_date_create'] = $data[$this->eng_ . '_date_create'];
			$data2['account_reset_debit'] = $data[$this->eng_ . '_monthly_debit'];
			$data2['account_reset_credit'] = $data[$this->eng_ . '_monthly_credit'];
			$exec = $this->Model->insert_reset($data2);
		} else {
			$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
			$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
		}
	}

	redirect(site_url($this->url_));
}
public function hapus($id = '')
{
	$this->Model->delete([$this->id_ => $id]);
	redirect($this->url_);
}

public function simpan_detail()
{
	$this->db->trans_begin();
	if ($this->input->post('simpan')) {
		$data = $this->input->post('input');
		if ($_POST['parameter'] == "tambah") {
			$id = $this->input->post('id');
			$id2 = $this->Model->get_max_id();
			$data[$this->id_] = $id;
			$data[$this->id2_] = $id2;
			$data[$this->eng2_ . '_type_id'] = 2;
			$data[$this->eng2_ . '_user_id'] = $_SESSION['user_id'];
			if ($_POST['realisasi'] == "on") {
				$data[$this->eng2_ . '_realization'] = 1;
			} else {
				$data[$this->eng2_ . '_realization'] = 0;
			}
			if ($_POST['transaction_type'] == "debit") {
				$type = "debit";
				$data[$this->eng2_ . '_debit'] = $_POST['transaction_amount'];
			} else if ($_POST['transaction_type'] == "credit") {
				$type = "credit";
				$data[$this->eng2_ . '_credit'] = $_POST['transaction_amount'];
			}
			$data[$this->eng2_ . '_date_create'] = date('Y-m-d H:i:s');
			$exec = $this->Model->insert_detail($data);
			$this->db->where('account_id', $id);
			$row = $this->Model->get()->row_array();
			if (@$row['account_date_reset'] > $data[$this->eng2_ . '_date']) {
				$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_ . '_date']);
			} else {
				$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
			}
		} else {
			$id = $this->input->post('id');
			$data[$this->eng2_ . '_user_id'] = $_SESSION['user_id'];
			if ($_POST['realisasi'] == "on") {
				$data[$this->eng2_ . '_realization'] = 1;
			} else {
				$data[$this->eng2_ . '_realization'] = 0;
			}
			if ($_POST['transaction_type'] == "debit") {
				$type = "debit";
				$data[$this->eng2_ . '_debit'] = $_POST['transaction_amount'];
				$data[$this->eng2_ . '_credit'] = 0;
			} else if ($_POST['transaction_type'] == "credit") {
				$type = "credit";
				$data[$this->eng2_ . '_credit'] = $_POST['transaction_amount'];
				$data[$this->eng2_ . '_debit'] = 0;
			}
			$data[$this->eng2_ . '_date_update'] = date('Y-m-d H:i:s');
			$this->Model->update_detail($data, [$this->id_ => $this->input->post('id'), $this->id2_ => $this->input->post('id_detail')]);
			$this->db->where('account_id', $id);
			$row = $this->Model->get()->row_array();
			if (@$row['account_date_reset'] > $this->input->post('transaction_date_last')) {
				$this->Model->update_balance($id,  -1 * ($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'), @$row['account_type_id'], $this->input->post('transaction_date_last'));
			} else {
				$this->Model->update_balance($id,  -1 * ($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'));
			}
			if (@$row['account_date_reset'] > $data[$this->eng2_ . '_date']) {
				$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_ . '_date']);
			} else {
				$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
			}
		}
	}
	if ($this->db->trans_status() === FALSE) {
		$this->db->trans_rollback();
	} else {
		$this->db->trans_commit();
	}
	redirect(site_url($this->url2_ . "/" . $id));
}
public function hapus_detail($id = '', $id_detail = '')
{
	$this->db->where('account_id', $id);
	$row = $this->Model->get()->row_array();


	$this->db->where('account_detail_id', $id_detail);
	$row2 = $this->Model->get_detail($id)->row_array();

	if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
		$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
	} else {
		$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
	}



	$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
	redirect(site_url($this->url2_ . "/$id"));
}

function approve_transfer_seller($id)
{
	$this->Model->update_realization_detail($id, '1');
	redirect(site_url($this->url_ . "/verification_transfer_seller"));
}

function reject_transfer_seller($id)
{
	$this->Model->update_realization_detail($id, '2');
	redirect(site_url($this->url_ . "/verification_transfer_seller"));
}

public function edit_verif_pembayaran()
{
	$datacontent['url'] = $this->url_;
	$datacontent['title'] = 'Data ' . $this->ind_;
	$data['file'] = $this->ind_;
	$data['content'] = $this->load->view($this->url_ . '/Edit_Verification', $datacontent, TRUE);
	$data['title'] = $datacontent['title'];
	$this->load->view('Layout/home', $data);
}

public function get_mutasi($id)
{
	$data['mutasis'] = $this->db->query("SELECT A.account_id, Y.seller_id, CONCAT('upload/TRANSFER/', A.account_detail_proof) as proof, Y.sales_category_id, A.account_detail_real_id, E.nama, C.account_name, A.account_detail_transfer_name, B.account_detail_date, B.account_detail_debit, A.account_detail_proof FROM t_account_detail A JOIN t_account_detail B ON B.account_detail_id = A.account_detail_id AND B.account_detail_real_id = (A.account_detail_real_id + 1) AND B.account_detail_debit = A.account_detail_credit JOIN m_account C ON C.account_id = B.account_id JOIN m_account D ON D.account_id = A.account_id JOIN member E ON E.kode = D.seller_id JOIN t_coa_transaction_temp X ON X.coa_transaction_source_id = A.account_detail_real_id JOIN m_account Y ON Y.account_id = A.account_id WHERE A.account_detail_realization = '0' AND X.coa_transaction_realization = '0' AND X.coa_transaction_source = '1' AND X.coa_code LIKE '%100.1.3%' AND A.account_id = $id")->result_array();
	$this->load->view('modal_mutasi', $data);
}

public function get_revisi($id)
{
	$datacontent['url'] 		= $this->url_;
	$datacontent['title'] 		= 'Transfer ' . $this->ind_;
	$datacontent['m_account'] 	= $this->Model->get_list_account_seller();
	$datacontent['arraccount'] 	= $this->Model->get_account($id);
	$arrdetail_sales 			= $this->Model->get_detail_sales($id);

	foreach ($arrdetail_sales as $index => $value) {
		$datacontent['arrdetail_sales'][$value['sales_id']][] = $value;
	}
	$datacontent['pembayarans'] = $this->db->query("SELECT X.coa_transaction_source, A.account_detail_note_accounting, A.account_detail_id, B.account_id as bank_id, A.account_id, Y.seller_id, CONCAT('upload/TRANSFER/', A.account_detail_proof) as proof, Y.sales_category_id, A.account_detail_real_id, E.nama, C.account_name, A.account_detail_transfer_name, B.account_detail_date, B.account_detail_debit, A.account_detail_proof, X.coa_credit, X.coa_debit FROM t_account_detail A JOIN t_account_detail B ON B.account_detail_id = A.account_detail_id AND B.account_detail_real_id = (A.account_detail_real_id + 1) AND B.account_detail_debit = A.account_detail_credit JOIN m_account C ON C.account_id = B.account_id JOIN m_account D ON D.account_id = A.account_id JOIN member E ON E.kode = D.seller_id JOIN t_coa_transaction_temp X ON X.coa_transaction_source_id = A.account_detail_real_id JOIN m_account Y ON Y.account_id = A.account_id WHERE A.account_detail_realization = '0' AND (((X.coa_transaction_source = '2' OR X.coa_transaction_source = '1') AND X.coa_code LIKE '%100.1.3%') OR ((X.coa_transaction_source = '2' OR X.coa_transaction_source = '3') AND X.coa_code LIKE '%200.1.2%')) AND A.account_id = $id")->result_array();
	$nominal = "";
	foreach ($datacontent['pembayarans'] as $index => $pembayaran) {
		if ($index > 0) $nominal .= ", ";
		$nominal .= "'" . $pembayaran['account_detail_real_id'] . "'";
	}
	$sales = $this->db->query("SELECT A.account_detail_real_id, B.sales_id, SUM(B.account_detail_sales_amount) FROM t_account_detail A JOIN t_account_detail_sales B ON B.account_detail_real_id = A.account_detail_real_id WHERE A.account_detail_real_id IN($nominal) GROUP BY B.sales_id")->result_array();
	$sales_id = "";
	foreach ($sales as $index => $sale) {
		if ($index > 0) $sales_id .= ", ";
		$sales_id .= "'" . $sale['sales_id'] . "'";
		$datacontent['arrreal_id'][$sale['sales_id']] = $sale;
	}
	if ($sales_id == "") $sales_id = "''";
	$arrlist_account_sales = $this->db->query("SELECT *, A.account_detail_real_id, A.account_detail_id FROM t_account_detail A LEFT JOIN t_sales C ON C.sales_id = A.sales_id WHERE A.sales_id IS NOT NULL AND ((A.account_detail_debit > A.account_detail_paid) OR A.sales_id IN($sales_id)) AND A.account_id = $id")->result_array();
	$datacontent['arrlist_account_sales'] = array();

	foreach ($arrlist_account_sales as $index => $value) {
		$datacontent['arrlist_account_sales'][$value['sales_id']] = $value;
		$datacontent['arrlist_account_sales'][$value['sales_id']]['total_bayar'] = 0;
		$datacontent['arrlist_account_sales'][$value['sales_id']]['total_kurang_kirim'] = 0;
	}

	if ($nominal == "") $nominal = "''";
	$nominal_bayar = $this->db->query("SELECT B.sales_id, SUM(B.account_detail_sales_amount) as total_bayar, A.account_detail_id FROM t_account_detail A JOIN t_account_detail_sales B ON B.account_detail_real_id = A.account_detail_real_id WHERE A.account_detail_real_id IN($nominal) GROUP BY B.sales_id")->result_array();

	$in2 = '';
	$i = 0;
	foreach ($nominal_bayar as $dataV) {
		$datacontent['nominal_bayar'][$dataV['sales_id']] = $dataV;
		if ($i == 0) {
			$in2 .= $dataV['account_detail_id'];
		}
		if ($i != 0  && $dataV['account_detail_id'] !== null) {
			$in2 .= ', ';
			$in2 .= $dataV['account_detail_id'];
		}
		$i++;
	}

	$nominal_bayarx = $this->db->query("SELECT B.sales_id, SUM(B.account_detail_sales_amount) as total_bayar, A.account_detail_id FROM t_account_detail A JOIN t_account_detail_sales B ON B.account_detail_real_id = A.account_detail_real_id WHERE A.account_detail_real_id IN($nominal) GROUP BY B.account_detail_id")->result_array();

	$in2 = '';
	$i = 0;
	foreach ($nominal_bayarx as $dataV) {
		if ($i == 0) {
			$in2 .= $dataV['account_detail_id'];
		}
		if ($i != 0  && $dataV['account_detail_id'] !== null) {
			$in2 .= ', ';
			$in2 .= $dataV['account_detail_id'];
		}
		$i++;
	}
	$arrdetail_sales = $this->Model->get_detail_sales_revisi($id);
	$in = '';
	$i = 0;
	foreach ($arrdetail_sales as $index => $value) {
		$datacontent['arrdetail_sales'][$value['sales_id']][] = $value;
		if ($i == 0) {
			$in .= $value['sales_id'];
		}
		if ($i != 0  && $value['sales_id'] !== null) {
			$in .= ', ';
			$in .= $value['sales_id'];
		}
		$i++;
	}
	$arrdata = array();
	if ($in2 !== '') {
		$arrdata = $this->Model->get_account_detail_sales_all_revisi($in, $in2);
	}
	$datacontent['total_sales'] = 0;
	foreach ($arrdata as $index => $value) {
		$datacontent['arraccount_detail_sales'][$value['sales_id']][$value['sales_detail_id']] = $value;

		if(@$datacontent['arrlist_account_sales'][@$value['sales_id']]['total_bayar'] == ""){
			$datacontent['arrlist_account_sales'][$value['sales_id']]['total_bayar'] = 0;
		}else{
			$datacontent['arrlist_account_sales'][$value['sales_id']]['total_bayar'] += (@$value['sales_detail_price'] *  @$value['send_allow']);
		}
		$datacontent['total_sales'] += ($value['sales_detail_price'] *  $value['send_allow']);
		$datacontent['arraccount_detail_sales2'][$value['sales_id']] = $value;
		@$datacontent['arrlist_account_sales'][$value['sales_id']]['total_kurang_kirim'] += ($value['sales_detail_quantity'] - $value['already'] + $value['send_allow']);
	}
	$datacontent['id'] = $id;
	$data['file'] = "Revisi ".$this->ind_;
	$data['content'] = $this->load->view($this->url_ . '/detail_revisi', $datacontent, TRUE);
	$data['title'] = $datacontent['title'];
	$this->load->view('Layout/home', $data);
}

public function set_revisi_transfer_seller()
{
	$this->db->trans_begin();
		// $i = 0;
		// foreach ($_POST['account_update'] as $index => $update) {

		// 	// $this->db->query("UPDATE t_account_detail SET account_detail_paid = account_detail_paid - $update WHERE account_detail_real_id = $index");
		// 	$uang = str_replace(",", "", $_POST['from']['account_detail_credit'][$i]);
		// 	$account = $_POST['account_id'][$i];
		// 	$this->db->query("UPDATE m_account SET account_credit = (account_credit - $uang) WHERE account_id = $_POST[id]");
		// 	$this->db->query("UPDATE m_account SET account_debit = (account_debit - $uang) WHERE account_id = $account");
		// 	$i++;
		// }
	foreach ($_POST['from']['account_detail_real_id'] as $index => $dataxxx) {
		$temp = $this->db->get_where('t_account_detail', ['account_detail_real_id' => $dataxxx])->row_array();
		$tempDebit[] = $temp['account_detail_credit'];
		$tempCredit[] = $temp['account_detail_debit'];

		$t_temp = $this->db->get_where('t_coa_transaction_temp', ['coa_transaction_source_id' => $dataxxx])->row_array();
		$this->db->query("DELETE FROM t_coa_transaction_temp WHERE coa_transaction_source_id = $dataxxx");
		$this->db->query("DELETE FROM t_coa_transaction_header WHERE coa_transaction_header_id = $t_temp[coa_group_id]");
	}
	$deposit = str_replace(",", "", $_POST['deposit2']);
	$this->db->query("UPDATE m_account SET account_deposit_draft = $deposit, account_deposit_draft_activation = 1 WHERE account_id = $_POST[id]");
	foreach ($_POST['account_id_past'] as $index0 => $valuex0) {
		$uangs = $this->db->query("SELECT account_detail_credit as uang FROM t_account_detail WHERE account_detail_id = $valuex0 AND account_detail_credit > 0")->result_array();
		$i = 0;
		foreach ($uangs as $uang) {
			$uang = $uang['uang'];
			$account = $_POST['account_id'][$i];
			$this->db->query("UPDATE m_account SET account_credit = (account_credit - $uang) WHERE account_id = $_POST[id]");
			$this->db->query("UPDATE m_account SET account_debit = (account_debit - $uang) WHERE account_id = $account");
			$i++;
		}
		$this->db->query("DELETE FROM t_account_detail WHERE account_detail_id = '$valuex0'");
		$this->db->query("DELETE FROM t_account_detail_sales WHERE account_detail_id = '$valuex0'");
		$this->db->query("DELETE FROM t_account_detail_sales_product WHERE account_detail_id = '$valuex0'");
	}

	$id = $this->Model->get_max_id();
	$from2 = $this->input->post('from');

	$to = $this->input->post('to');
	$id2 = $this->input->post('id');
	$sales = $this->input->post('sales');
	$account_id = $this->input->post('account_id');
	$account = $this->db->query("SELECT * FROM m_account WHERE account_id = '$id2'")->row_array();
	$member = $this->db->query("SELECT * FROM member WHERE kode = '$account[seller_id]'")->row_array();
	$piutang = $this->db->query("SELECT * FROM coa_4 WHERE kode = '100.1.3.$account[account_code2]'")->row_array();
	$deposit = $this->db->query("SELECT * FROM coa_4 WHERE kode = '200.1.2.$account[account_code2]'")->row_array();

	$dataHeader = ([
		'coa_transaction_date' => date('Y-m-d'),
		'date_create' => date('Y-m-d H:i:s'),
		'user_create' => $_SESSION['user_id'],
		'coa_transaction_deposit' => str_replace(',', '', $_POST['deposit2']),
	]);
	$this->db->insert('t_coa_transaction_header', $dataHeader);
	$idHeader = $this->db->insert_id();

	$t_mistake = ([
		'mistake_suspect_type' => $_POST['mistake_suspect_type'],
		'mistake_type' => 1,
		'mistake_note' => $_POST['mistake_note'],
		'mistake_create_user_id' => $_SESSION['user_id'],
		'mistake_create_date' => date('Y-m-d H:i:s'),
		'mistake_create_date' => date('Y-m-d H:i:s'),
	]);
	if ($_POST['mistake_suspect_type'] == 2) {
		$t_mistake['mistake_suspect_code'] = $_SESSION['user_id'];
		$t_mistake['mistake_suspect_name'] = $_SESSION['user_fullname'];
	} else if ($_POST['mistake_suspect_type'] == 1) {
		$t_mistake['mistake_suspect_code'] = $member['kode'];
		$t_mistake['mistake_suspect_name'] = $member['nama'];
	}

	$this->db->insert('t_mistake', $t_mistake);
	$t_mistake['mistake_id'] = $this->db->insert_id();

	$i = 0;
	foreach ($account_id as $index => $value) {
		$from = array();
		$data_paid3 = array();
		$data_paid2 = array();
		$exec = '';
		$to = array();
		$from['account_detail_transfer_name'] = $from2['account_detail_transfer_name'][$index];
		$from['account_detail_date'] = $from2['account_detail_date'][$index];
		$from['account_detail_credit'] = $from2['account_detail_credit'][$index];
		$from['account_detail_credit'] = str_replace(',', '', $from['account_detail_credit']);
		$from['account_id'] = $id2;
		$from['account_detail_pic'] = $_SESSION['user_fullname'];
		$from['account_detail_id'] = $id;
		$from['account_detail_debit'] = 0;
		$from['account_detail_category_id'] = 1;
		$from['account_detail_type_id'] = 1;
		$from['account_detail_user_id'] = $_SESSION['user_id'];
		$from['account_detail_date_create'] = date('Y-m-d H:i:s');
		$from['account_detail_note_accounting'] = $from2['account_note'][$index];
			//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
			//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
		$exec = $this->Model->insert_detail($from);

		$data_paid3['coa_id'] = $piutang['id'];
		$data_paid3['coa_name'] = $piutang['nama'];
		$data_paid3['coa_code'] = $piutang['kode'];
		$data_paid3['coa_level'] = 4;
		$data_paid3['coa_date'] = $from['account_detail_date'];
		$data_paid3['coa_debit'] = $from['account_detail_credit'];
		$data_paid3['coa_credit'] = 0;
		$data_paid3['coa_transaction_note'] = 'Pembayaran ' . $account['account_name'];
		$data_paid3['user_create'] = $_SESSION['user_id'];
		$data_paid3['coa_transaction_source'] = 0;
		$data_paid3['coa_transaction_source_id'] = $exec;
		$data_paid3['coa_transaction_realization'] = 0;
		$data_paid3['coa_group_id'] = $idHeader;
		$data_paid3['coa_past_debit'] = $tempDebit[$i];
		$data_paid3['coa_past_credit'] = $tempCredit[$i];
			// $this->Model->insert_coa_transaction($data_paid3);
		$this->db->insert('t_coa_transaction_temp', $data_paid3);
		$data_paid3['coa_transaction_id'] = $this->db->insert_id();

		$this->db->insert('t_coa_transaction_mistake', ['mistake_id' => $t_mistake['mistake_id'], 'coa_transaction_id' => $data_paid3['coa_transaction_id']]);

		$data_paid2['coa_id'] = $deposit['id'];
		$data_paid2['coa_name'] = $deposit['nama'];
		$data_paid2['coa_code'] = $deposit['kode'];
		$data_paid2['coa_level'] = 4;
		$data_paid2['coa_date'] = $from['account_detail_date'];
		$data_paid2['coa_debit'] = 0;
		$data_paid2['coa_credit'] = $from['account_detail_credit'];
		$data_paid2['coa_transaction_note'] = 'Pembayaran ' . $account['account_name'];
		$data_paid2['user_create'] = $_SESSION['user_id'];
		$data_paid2['coa_transaction_source'] = 0;
		$data_paid2['coa_transaction_source_id'] = $exec;
		$data_paid2['coa_transaction_realization'] = 0;
		$data_paid2['coa_group_id'] = $idHeader;
		$data_paid2['coa_past_debit'] = $tempDebit[$i];
		$data_paid2['coa_past_credit'] = $tempCredit[$i];

			// $this->Model->insert_coa_transaction($data_paid2);
		$this->db->insert('t_coa_transaction_temp', $data_paid2);
		$data_paid2['coa_transaction_id'] = $this->db->insert_id();

		$this->db->insert('t_coa_transaction_mistake', ['mistake_id' => $t_mistake['mistake_id'], 'coa_transaction_id' => $data_paid2['coa_transaction_id']]);

		$this->db->query("UPDATE t_coa_transaction_header SET coa_transaction_debit = coa_transaction_debit + $from[account_detail_credit], coa_transaction_credit = coa_transaction_credit + $from[account_detail_credit], coa_transaction_payment = coa_transaction_payment + $from[account_detail_credit] WHERE coa_transaction_header_id = '$idHeader'");
			// $this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $from[account_detail_credit] WHERE coa_id = '$deposit[id]'");
			// $this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $from[account_detail_credit] WHERE coa_id = '$deposit[id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $from['account_detail_date'] . "', '-', '')");
			// $this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $from[account_detail_credit] WHERE coa_id = '$piutang[id]'");
			// $this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $from[account_detail_credit] WHERE coa_id = '$piutang[id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $from['account_detail_date'] . "', '-', '')");


			/*$path = $_FILES['transfer_' . $index]['name'];
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			$config['upload_path']          = './upload/TRANSFER/';
			$config['allowed_types']        = '*';
			$config['file_name']        = $exec;
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('transfer_' . $index)) {
			
				$error = array('error' => $this->upload->display_errors());
			} else {
				$this->upload->data();
				$dataxy['account_detail_proof'] = $exec . "." . $ext;
				
			}*/
			$dataxy['account_detail_proof'] = $from2['account_detail_proof'][$index];
			$this->Model->update_detail($dataxy, ['account_detail_real_id' => $exec]);
			$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');
			/*if($from['account_detail_deposit'] > 0){
				$datayyyx['account_detail_deposit_amount'] = $from['account_detail_deposit'];
				$datayyyx['account_detail_real_id'] = $exec;
				$datayyyx['account_id'] = $from['account_id'];
				$this->Model->insert_account_detail_deposit($datayyyx);
			}*/
			$to['account_id'] = $value;
			$to['account_detail_proof'] = @$dataxy['account_detail_proof'];
			$to['account_detail_id'] = $id;
			$to['account_detail_pic'] = $from['account_detail_pic'];
			$to['account_detail_credit'] = 0;
			$to['account_detail_transfer_name'] = $from['account_detail_transfer_name'];
			$to['account_detail_debit'] = $from['account_detail_credit'];
			$to['account_detail_pic'] = $from['account_detail_pic'];
			$to['account_detail_date'] = $from['account_detail_date'];
			$to['account_detail_category_id'] = 2;
			$to['account_detail_type_id'] = 1;
			$to['account_detail_user_id'] = $from['account_detail_user_id'];
			$to['account_detail_date_create'] = $from['account_detail_date_create'];
			$this->Model->insert_detail($to);
			$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');

			$i++;
		}

		$allow = $this->input->post('allow');
		$ids = $this->input->post('ids');
		$product_id = $this->input->post('product_id');

		$cekEx = 0;
		if (@$exec == '') {
			$cekEx = 1;
			$this->db->query("UPDATE m_account SET account_deposit = " . str_replace(',', '', $_POST['deposit']) . ", account_deposit_draft = 0 WHERE account_id = " . $_POST['id'] . "");
			$from = array();
			$data_paid3 = array();
			$data_paid2 = array();
			$exec = '';
			$to = array();
			$from['account_detail_transfer_name'] = 'Deposit';
			$from['account_detail_date'] = date('Y-m-d');
			$from['account_detail_credit'] = 0;
			$from['account_id'] = $id2;
			$from['account_detail_pic'] = $_SESSION['user_fullname'];
			$from['account_detail_id'] = $id;
			$from['account_detail_debit'] = 0;
			$from['account_detail_category_id'] = 1;
			$from['account_detail_type_id'] = 1;
			$from['account_detail_user_id'] = $_SESSION['user_id'];
			$from['account_detail_date_create'] = date('Y-m-d H:i:s');
			//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
			//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
			$exec = $this->Model->insert_detail($from);
		}

		foreach ($_POST['paid'] as $index_paid => $value_paid) {
			//if ($value_paid > 0) {
			$value_paid = str_replace(',', '', $value_paid);
			$data_paid['account_detail_real_id'] = $exec;
			$data_paid['sales_id'] = $sales[$index_paid];
			$data_paid['account_detail_target_real_id'] = $index_paid;
			$data_paid['account_detail_sales_amount'] = $value_paid;
			$data_paid['account_detail_sales_date'] = $from['account_detail_date'];
			$data_paid['account_detail_id'] = $id;
			$account_detail_sales_id = $this->Model->insert_account_detal_sales($data_paid, $value_paid, $cekEx);
			if (@$_POST['diskon_persen'][$sales[$index_paid]] == null) {
				$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_active_draft = 0 WHERE sales_id = $sales[$index_paid]");
			}
			foreach ($allow[$index_paid] as $index2 => $value2) {
				$prod_kd = $this->db->get_where('produk', ['id_produk' => $product_id[$index_paid][$index2]])->row_array();
				$data = array();
				$data['account_detail_id'] = $id;
				$data['account_detail_real_id'] = $exec;
				$data['sales_id'] = $sales[$index_paid];
				$data['sales_detail_id'] = $index2;
				$data['product_id'] = $product_id[$index_paid][$index2];
				$data['account_detail_sales_product_allow'] = $value2;
				$data['account_detail_sales_id'] = $account_detail_sales_id;
				if (@$_POST['diskon_harga'][$sales[$index_paid]][$index2] !== null) {
					if ($_POST['discount_id'][$sales[$index_paid]][$index2] !== null) {
						$discount = $_POST['diskon_harga'][$sales[$index_paid]][$index2];
						$terms = $this->db->get_where('m_discount_terms_product', ['discount_id' => $_POST['discount_id'][$sales[$index_paid]][$index2], 'product_kd' => $prod_kd['kd_pd']])->row_array();
						if ($terms !== null && $value2 > 0) {
							$sales_discount_terms = $this->db->query("SELECT B.sales_discount_terms_id, A.sales_discount_id FROM t_sales_discount A JOIN t_sales_discount_terms B ON B.sales_discount_id = A.sales_discount_id AND B.product_kd = '$prod_kd[kd_pd]' WHERE A.sales_id = $sales[$index_paid] AND A.discount_id = $terms[discount_id]")->row_array();
							if ($terms['discount_terms_product_quantity'] > 0) {
								$disc_term = floor($value2 / $terms['discount_terms_product_quantity']);
								$disc_mod = $value2 - ($terms['discount_terms_product_quantity'] * $disc_term);
							} else if ($terms['discount_terms_product_price'] > 0) {
								$disc_mod = fmod($terms['discount_terms_product_price'], $value2);
								$disc_term = intdiv($terms['discount_terms_product_price'], $value2);
								if ($disc_mod > 0) {
									$disc_mod = $value2 - $disc_term;
								}
							} else {
								$disc_term = 0;
								$disc_mod = 0;
							}
							if ($disc_mod > 0) {
								$data['account_detail_sales_product_discount'] = floor($discount / ($value2 - 1));
								$data['account_detail_sales_product_allow'] = $value2 - 1;
								$datax = ([
									'account_detail_id' => $id,
									'account_detail_real_id' => $exec,
									'sales_id' => $sales[$index_paid],
									'sales_detail_id' => $index2,
									'product_id' => $product_id[$index_paid][$index2],
									'account_detail_sales_product_allow' => 1,
									'account_detail_sales_id' => $account_detail_sales_id,
										// 'region_id' => $_POST['input2']['kecamatan'],
										// 'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
										// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
										// 'account_detail_sales_product_address' => $_POST['input2']['full_address'],
									'account_detail_sales_product_delivery_cost' => '',
									'account_detail_sales_product_discount' => $discount - ($data['account_detail_sales_product_discount'] * ($value2 - 1)),
									'account_detail_sales_product_discount_total' => $discount - ($data['account_detail_sales_product_discount'] * ($value2 - 1)),
									'sales_discount_terms_id' => $sales_discount_terms['sales_discount_terms_id']
								]);
								$this->db->insert("t_account_detail_sales_product", $datax);
								if ($cekEx == 1) {
									$discountx = $this->db->get_where('t_discount_total', ['account_detail_id' => $datax['account_detail_id']])->row_array();
									if ($discountx['discount_total_price'] == null) $discountx['discount_total_price'] = 0;
									$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
									$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow], discount_price = discount_price + $discountx[discount_total_price] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
									$this->Model->check_fifo_alpha($datax['product_id']);
									$this->Model->check_fifo_beta($datax['product_id']);
									$this->Model->check_fifo_main($datax['product_id'], $account['seller_id']);
									$this->Model->set_sales_detail();
									$discountx = '';
								}
								$data['account_detail_sales_product_discount_total'] = $discount - $datax['account_detail_sales_product_discount_total'];
							} else {
								$data['account_detail_sales_product_discount'] = $discount / $value2;
								$data['account_detail_sales_product_discount_total'] = $discount;
							}
							$data['account_detail_sales_product_terms_quantity'] = ($terms['discount_terms_product_quantity'] * $disc_term);
							$data['account_detail_sales_product_terms_mod'] = $disc_mod;
							$data['sales_discount_terms_id'] = $sales_discount_terms['sales_discount_terms_id'];
						}
					}
				}
				$total_harga_prod = $value2 * $_POST['harga_produk'][$sales[$index_paid]][$index2];
				$hrgprd[$prod_kd['kd_pd']][$sales[$index_paid]] = $total_harga_prod;
				if (@$_POST['diskon_persen'][$sales[$index_paid]][$index2] !== null && @$_POST['diskon_persen'][$sales[$index_paid]][$index2] > 0) {
					if ($_POST['discount_id'][$sales[$index_paid]][$index2] !== '') {
						$discount = $_POST['diskon_persen'][$sales[$index_paid]][$index2];

						$terms = $this->db->get_where('m_discount_terms_product', ['discount_id' => $_POST['discount_id'][$sales[$index_paid]][$index2], 'product_kd' => $prod_kd['kd_pd']])->row_array();
						if ($terms !== null && $value2 > 0) {
							$sales_discount_terms = $this->db->query("SELECT B.sales_discount_terms_id, A.sales_discount_id FROM t_sales_discount A JOIN t_sales_discount_terms B ON B.sales_discount_id = A.sales_discount_id AND B.product_kd = '$prod_kd[kd_pd]' WHERE A.sales_id = $sales[$index_paid] AND A.discount_id = $terms[discount_id]")->row_array();
							$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_active_draft = 1 WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]");
							$tSalesDiscount = $this->db->query("SELECT * FROM  t_sales_discount WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]")->row_array();
							if ($terms['discount_terms_product_quantity'] > 0) {
								$disc_term = floor($value2 / $terms['discount_terms_product_quantity']);
								$disc_mod = $value2 - ($terms['discount_terms_product_quantity'] * $disc_term);
							} else if ($terms['discount_terms_product_price'] > 0) {
								$disc_mod = fmod($terms['discount_terms_product_price'], $value2);
								$disc_term = intdiv($terms['discount_terms_product_price'], $value2);
								if ($disc_mod > 0) {
									$disc_mod = $value2 - $disc_term;
								}
							} else {
								$disc_term = 0;
								$disc_mod = 0;
							}
							$data['account_detail_sales_product_discount'] = (@$_POST['list_prod_diskon'][$_POST['sales'][$index_paid]][$prod_kd['kd_pd']] * $_POST['harga_produk'][$_POST['sales'][$index_paid]][$index2] / 100);
							if ($tSalesDiscount['sales_discount_percentage_hoard_draft'] > 0) {
								$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard_draft = 0 WHERE sales_discount_id = $sales_discount_terms[sales_discount_id]");
								$data['account_detail_sales_product_discount_total'] = $data['account_detail_sales_product_discount'] * ($value2 - 1);
								$data['account_detail_sales_product_allow'] = $value2 - 1;
								$datax = ([
									'account_detail_id' => $id,
									'account_detail_real_id' => $exec,
									'sales_id' => $sales[$index_paid],
									'sales_detail_id' => $index2,
									'product_id' => $product_id[$index_paid][$index2],
									'account_detail_sales_product_allow' => 1,
									'account_detail_sales_id' => $account_detail_sales_id,
										// 'region_id' => $_POST['input2']['kecamatan'],
										// 'region_delivery_instance_id' => $data_eksped['region_delivery_instance_id'],
										// 'delivery_instance_id' => $_POST['input2']['ekspedisi'],
										// 'account_detail_sales_product_address' => $_POST['input2']['full_address'],
										// 'account_detail_sales_product_delivery_cost' => '',
									'account_detail_sales_product_discount' => $discount - $data['account_detail_sales_product_discount_total'],
									'account_detail_sales_product_discount_total' => $discount - $data['account_detail_sales_product_discount_total'],
									'sales_discount_terms_id' => $sales_discount_terms['sales_discount_terms_id']
								]);
								$this->db->insert("t_account_detail_sales_product", $datax);
								if ($cekEx == 1) {
									$discountx = $this->db->get_where('t_discount_total', ['account_detail_id' => $datax['account_detail_id']])->row_array();
									if ($discountx['discount_total_price'] == null) $discountx['discount_total_price'] = 0;
									$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
									$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $datax[account_detail_sales_product_allow], discount_price = discount_price + $discountx[discount_total_price] WHERE product_id = $datax[product_id] AND sales_detail_id = $datax[sales_detail_id]");
									$this->Model->check_fifo_alpha($datax['product_id']);
									$this->Model->check_fifo_beta($datax['product_id']);
									$this->Model->check_fifo_main($datax['product_id'], $account['seller_id']);
									$this->Model->set_sales_detail();
									$discountx = '';
								}
							} else {
								$data['account_detail_sales_product_discount_total'] = $data['account_detail_sales_product_discount'] * $value2;
							}
						}
					}
				}
				$this->Model->insert_account_detail_sales_product($data);
				if ($cekEx == 1) {
					$discount = $this->db->get_where('t_discount_total', ['account_detail_id' => $data['account_detail_id']])->row_array();
					if ($discount['discount_total_price'] == null) $discount['discount_total_price'] = 0;
					$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $data[account_detail_sales_product_allow] WHERE product_id = $data[product_id] AND sales_detail_id = $data[sales_detail_id]");
					$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity + $data[account_detail_sales_product_allow], discount_price = discount_price + $discount[discount_total_price] WHERE product_id = $data[product_id] AND sales_detail_id = $data[sales_detail_id]");
					$this->Model->check_fifo_alpha($data['product_id']);
					$this->Model->check_fifo_beta($data['product_id']);
					$this->Model->check_fifo_main($data['product_id'], $account['seller_id']);
					$this->Model->set_sales_detail();
					$discount = '';
				}
			}
			//}
		}

		if (isset($_POST['sisa_produk'])) {
			foreach ($_POST['sisa_produk'] as $index1 => $sisa_produk) {
				foreach ($sisa_produk as $index2 => $sisa) {
					$sales_dc = $this->db->get_where('t_sales_discount_terms', ['sales_discount_terms_id' => $index2])->row_array();
					$sales_disc = $this->db->get_where('t_sales_discount', ['sales_discount_id' => $sales_dc['sales_discount_id']])->row_array();
					if ($sales_disc['sales_discount_percentage_active'] !== 1) {
						$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod_draft = $sisa, sales_discount_terms_accumulation_draft = " . $_POST['produk_accum'][$index1][$index2] . ", sales_discount_terms_mod_price_draft = " . $hrgprd[$sales_dc['product_kd']][$sales_disc['sales_id']] . " WHERE sales_discount_terms_id = $index2");
						$this->db->insert('t_account_detail_sales_terms', ['sales_discount_terms_id' => $index2, 'sales_discount_id' => $sales_dc['sales_discount_id'], 'account_detail_sales_product_terms_mod' => $sisa, 'account_detail_sales_product_terms_accumulation' => $_POST['produk_accum'][$index1][$index2]]);
					}
				}
			}
		}

		if (isset($_POST['dikson_produk'])) {
			foreach ($_POST['dikson_produk'] as $index => $dikson_produks) {
				foreach ($dikson_produks as $index2 => $dikson_produk) {
					$sales_product = $this->db->get_where('t_sales_discount_product', ['sales_detail_id' => $index2])->row_array();
					$product = $this->db->get_where('produk', ['kd_pd' => $sales_product['product_kd']])->row_array();
					$productPrice = $this->db->query("SELECT * FROM m_price WHERE produk_global_kd = '$sales_product[product_kd]' AND schema_id = '$member[schema_id]' AND price_unit_price > 0 ORDER BY price_unit_price ASC LIMIT 1")->row_array();
					$detailSalesProd = ([
						'account_detail_real_id' => $exec,
						'sales_id' => $index,
						'product_id' => $product['id_produk'],
						'account_detail_sales_product_allow' => $dikson_produk,
						'account_detail_sales_id' => $account_detail_sales_id,
						'sales_detail_id' => $index2,
						'account_detail_id' => $id,
						// 'region_id' => $_POST['input2']['kecamatan'],
						// 'region_delivery_instance_id' =>  $data_eksped['region_delivery_instance_id'],
						// 'delivery_instance_id' =>  $_POST['input2']['ekspedisi'],
						// 'account_detail_sales_product_address' =>  $_POST['input2']['full_address'],
						// 'account_detail_sales_product_delivery_cost' =>  '',
						// 'account_detail_sales_product_price' =>  $productPrice['price_unit_price'],
						'account_detail_sales_product_discount' =>  $productPrice['price_unit_price'],
						'account_detail_sales_product_discount_total' =>  $productPrice['price_unit_price'] * $dikson_produk,
					]);
					$this->db->insert('t_account_detail_sales_product', $detailSalesProd);
				}
			}
		}

		if (isset($_POST['tot_hoard'])) {
			foreach ($_POST['tot_hoard'] as $index => $tot_hoard) {
				if ($cekEx == 1) {
					$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard = sales_discount_percentage_hoard + $tot_hoard WHERE sales_discount_id = $index");
				} else {
					$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard_draft = $tot_hoard WHERE sales_discount_id = $index");
				}
			}
		}
		$arrx = $this->Model->get_account($id2);
		// $accountx['account_deposit'] = ((int)@$arrx['account_deposit']) + str_replace(',', '', $_POST['deposit']);
		// $this->Model->update($accountx, [$this->id_ => $id2]);

		$old_paid = $_POST['old_paid'];
		foreach ($_POST['sales_id_all'] as $index_p => $update_p) {
			$this->db->query('UPDATE t_account_detail SET account_detail_paid_draft = account_detail_paid_draft - ' . $old_paid[$index_p] . ' WHERE sales_id = "' . $update_p . '"');
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_ . "/seller"));
	}

	public function get_kota($id)
	{
		$kotas = $this->db->query("SELECT * FROM m_region WHERE REGION_ID LIKE '" . $id . "__000000' AND SUBSTR(REGION_ID, 3, 2) != '00'")->result_array();
		$data = '';
		foreach ($kotas as $kota) {
			$data .= '<option value="' . $kota['REGION_ID'] . '">' . $kota['NAME'] . '</option>';
		}
		echo $data;
	}
	public function get_kecamatan($id)
	{
		$kecamatans = $this->db->query("SELECT * FROM m_region WHERE REGION_ID LIKE '" . $id . "__0000' AND SUBSTR(REGION_ID, 5, 2) != '00'")->result_array();
		$data = '';
		foreach ($kecamatans as $kecamatan) {
			$data .= '<option value="' . $kecamatan['REGION_ID'] . '">' . $kecamatan['NAME'] . '</option>';
		}
		echo $data;
	}

	public function get_option_product($in)
	{
		$data = '';
		$in = str_replace("%20", ", ", $in);
		$products = $this->db->query("SELECT * FROM produk WHERE id_produk IN($in)")->result_array();
		foreach ($products as $product) {
			$data .= '<option value="' . $product['id_produk'] . '">' . $product['nama_produk'] . '</option>';
		}
		echo $data;
	}
	public function get_option_diskon($id, $value, $price)
	{
		$product = $this->db->query("SELECT kd_pd FROM produk WHERE id_produk = $id")->row_array();
		$price = $price * $value;
		$chks = $this->db->query("SELECT * FROM m_discount_terms_product A JOIN m_discount B ON B.discount_id = A.discount_id WHERE B.discount_active = 1 AND A.product_kd = '$product[kd_pd]' AND ((discount_terms_product_quantity = $value OR discount_terms_product_quantity < $value) OR (discount_terms_product_price = $price OR discount_terms_product_price < $price))")->result_array();
		$data = '';
		foreach ($chks as $chk) {
			$chk_discounts = $this->db->query("SELECT * FROM m_discount_reward A JOIN m_discount_reward_product B ON A.discount_reward_id = B.discount_reward_id WHERE A.discount_id = $chk[discount_id]")->result_array();
			foreach ($chk_discounts as $discount) {
				if ($discount['discount_reward_product_type'] == 1) {
					if ($chk['discount_terms_product_type'] == 1) {
						$data .= '<option value="' . $discount['discount_id'] . '">Potongan Harga Rp ' . number_format($discount['discount_reward_product_deduction_cost']) . ' per ' . $chk['product_kd'] . ' ' . str_replace(".00x", "", $chk['discount_terms_product_quantity']) . ' pcs</option>';
					} else {
						$data .= '<option value="' . $discount['discount_id'] . '">Potongan Harga Rp ' . number_format($discount['discount_reward_product_deduction_cost']) . ' per ' . $chk['product_kd'] . ' Rp ' . number_format($chk['discount_terms_product_price']) . '</option>';
					}
				} else if ($discount['discount_reward_product_type'] == 2) {
					if ($chk['discount_terms_product_type'] == 1) {
						$data .= '<option value="' . $discount['discount_id'] . '">Potongan Harga ' . $discount['discount_reward_product_percentage'] . '% per ' . $chk['product_kd'] . ' ' . str_replace(".00", "", $chk['discount_terms_product_quantity']) . ' pcs</option>';
					} else {
						$data .= '<option value="' . $discount['discount_id'] . '">Potongan Harga ' . $discount['discount_reward_product_percentage'] . '% per ' . $chk['product_kd'] . ' Rp ' . number_format($chk['discount_terms_product_price']) . '</option>';
					}
				} else {
					if ($chk['discount_terms_product_type'] == 1) {
						$data .= '<option value="' . $discount['discount_id'] . '">Mendapat Produk ' . $discount['product_kd'] . ' ' . str_replace(".00", "", $discount['discount_reward_product_quantity']) . ' pcs per ' . $chk['product_kd'] . ' ' . str_replace(".00", "", $chk['discount_terms_product_quantity']) . ' pcs</option>';
					} else {
						$data .= '<option value="' . $discount['discount_id'] . '">Mendapat Produk ' . $discount['product_kd'] . ' ' . str_replace(".00", "", $discount['discount_reward_product_quantity']) . ' pcs per ' . $chk['product_kd'] . ' Rp ' . number_format($chk['discount_terms_product_price']) . '</option>';
					}
				}
			}
		}
		echo $data;
	}

	public function chk_discount($id, $value, $price)
	{
		$product = $this->db->query("SELECT kd_pd FROM produk WHERE id_produk = $id")->row_array();
		$chks = $this->db->query("SELECT * FROM m_discount_terms_product A JOIN m_discount B ON B.discount_id = A.discount_id WHERE B.discount_active = 1 AND A.product_kd = '$product[kd_pd]' AND ((discount_terms_product_quantity = $value OR discount_terms_product_quantity < $value) OR (discount_terms_product_price = $price OR discount_terms_product_price < $price))")->result_array();

		$discounts = array();
		$index = 0;
		foreach ($chks as $chk) {
			$chk_discounts = $this->db->query("SELECT * FROM m_discount_reward A JOIN m_discount_reward_product B ON A.discount_reward_id = B.discount_reward_id WHERE A.discount_id = $chk[discount_id]")->result_array();
			foreach ($chk_discounts as $discount) {
				$discounts[$discount['discount_reward_product_id']] = $discount;
				if ($chk['discount_terms_product_type'] == 1) {
					$kelipatan = floor($value / $chk['discount_terms_product_quantity']);
				} else {
					$kelipatan = floor($price / $chk['discount_terms_product_price']);
				}
				$discounts[$discount['discount_reward_product_id']]['discount_reward_product_deduction_cost'] = $discounts[$discount['discount_reward_product_id']]['discount_reward_product_deduction_cost'] * $kelipatan;
				if ($discount['discount_reward_product_type'] == 3) {
					$discounts[$discount['discount_reward_product_id']]['discount_reward_product_quantity'] = $discounts[$discount['discount_reward_product_id']]['discount_reward_product_quantity'] * $kelipatan;
				}
			}
		}


		echo JSON_encode($discounts);
	}
}
