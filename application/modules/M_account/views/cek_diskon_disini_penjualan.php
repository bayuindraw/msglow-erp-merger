
<?php 
	
    $jumlahTerm = $this->db->query("SELECT * FROM v_diskon_terms_produk_arga where discount_id = '".$id_diskon."' ")->num_rows();
    
    
    $dbQuery = $this->db->query("
                SELECT
        A.account_id,
        B.seller_id,
        C.product_global_id,
        C.product_global_name as nama_produk,
        C.sales_detail_quantity,
        C.sales_id ,m.discount_terms_product_quantity,m.discount_name,m.discount_type,m.discount_category,m.discount_name
    
    FROM
        m_account A,t_account_detail ma,t_sales B,t_sales_detail C,v_diskon_terms_produk_arga m 
        
        WHERE  B.seller_id = A.seller_id and
               C.sales_id = B.sales_id and
                     A.account_id = ma.account_id and 
                     ma.sales_id = B.sales_id and 
                     C.sales_detail_quantity >= m.discount_terms_product_quantity
                     and m.discount_id = '".$id_diskon."'
                     and A.account_id = '".$id_akun."' and C.sales_id  = '".$id_sales."'
             GROUP BY C.sales_id,C.product_global_name  
            ");
    ?>
    
    <?php 
    if($dbQuery->num_rows() == $jumlahTerm){
    ?>
    <center><h4>SELAMAT! Syarat Diskon Terpenuhi</h4></center>
    <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
    <thead>
        <tr>
            <th>NAMA PRODUK TERDISKON</th>
            <th>JUMLAH SYARAT DISKON</th>
            <th>JUMLAH PEMESANAN</th>
            
        </tr>	
    </thead>
    <tbody>
        <?php 
        foreach ($dbQuery->result_array() as $key => $vaData) {
            # code...
        
        ?>
        <tr>
            <td><?=$vaData['nama_produk']?></td>
            <td><?=$vaData['discount_terms_product_quantity']?></td>
            <td><?=$vaData['sales_detail_quantity']?></td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
        <?php
        $dbRewardIdentity = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$id_diskon."' LIMIT 1  ");
        foreach ($dbRewardIdentity->result_array()  as $key => $vaIdentityReward) {
                $cNamaDiskon	=	$vaIdentityReward['discount_name'];
                $cKategoriDiskon=	$vaIdentityReward['kategori_diskon'];
                $cTipeDiskon	=	$vaIdentityReward['discount_type'];
                $cIdKategoriDiskon	=	$vaIdentityReward['discount_category'];
            }
        ?>
    
    <?php 
    
    if($vaData['discount_type'] == 1 and $vaData['discount_category'] == 2){
    
    ?>
    
    <tr>
        <td colspan="3"><strong>Diskon & Promo Berupa</strong></td>
    </tr>
    
    <tr>
        <td>Diskon Persentase</td>
        <?php 
            
            $dbRewardPercentage = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$id_diskon."' LIMIT 1  ");
            
    
            foreach ($dbRewardPercentage->result_array() as $key => $vaRewardPercentage) {
    ?>	
        <td colspan="2"><?=$vaRewardPercentage['discount_reward_product_percentage']?> % </td>
    <?php } ?>
    </tr>
    <tr>
        <td>Free Produk</td>
        <?php 
            $dbRewardProduk = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$id_diskon."'  ");
        ?>	
        <td colspan="2">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nama Produk</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach ($dbRewardProduk->result_array() as $key => $vaRewardProduk) {
                    ?>
                    <tr>
                        <td><?=$vaRewardProduk['nama_produk']?></td>
                        <td><?=$vaRewardProduk['discount_reward_product_quantity']?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <input type="hidden" id="id_diskon" value="<?=$id_diskon?>">
            <input type="hidden" id="id_akun" value="<?=$id_akun?>">
            <input type="hidden" id="id_sales" value="<?=$id_sales?>">
        
    
    
            <input type="hidden" id="tipe_diskon" value="<?=$cTipeDiskon?>">
            <input type="hidden" id="kategori_diskon" value="<?=$cIdKategoriDiskon?>">
    
            <button type="button" class="btn btn-primary btn-block" onclick="return klaimDiskonPembelianPerPCS();">
                <i class="fa fa-tags"></i> Klaim Diskon Di Pembayaran
            </button>
    
            <script type="text/javascript">
                function klaimDiskonPembelianPerPCS(){
                 window.location.href='<?=base_url()?>M_account/transfer_seller2/tambah/<?=$id_akun?>'
                }
            </script>
        </td>
    </tr>
    
    <?php }if($vaData['discount_type'] == 1 and $vaData['discount_category'] == 1){ ?>
    <tr>
        <td colspan="3"><strong>Diskon & Promo Berupa</strong></td>
    </tr>
    <tr>
        <td>Free Produk</td>
        <?php 
            $dbRewardProduk = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$id_diskon."'  ");
        ?>	
        <td colspan="2">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nama Produk</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach ($dbRewardProduk->result_array() as $key => $vaRewardProduk) {
                    ?>
                    <tr>
                        <td><?=$vaRewardProduk['nama_produk']?></td>
                        <td><?=$vaRewardProduk['discount_reward_product_quantity']?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <input type="hidden" id="id_diskon" value="<?=$id_diskon?>">
            <input type="hidden" id="id_akun" value="<?=$id_akun?>">
            <input type="hidden" id="id_sales" value="<?=$id_sales?>">
            <input type="hidden" id="id_akun" value="<?=$id_akun?>">
            <input type="hidden" id="id_akun" value="<?=$id_akun?>">
    
            <input type="hidden" id="tipe_diskon" value="<?=$cTipeDiskon?>">
            <input type="hidden" id="kategori_diskon" value="<?=$cIdKategoriDiskon?>">
            <button type="button" class="btn btn-primary btn-block" onclick="return klaimDiskonPembelianKelipatan();">
                <i class="fa fa-tags"></i> Klaim Diskon Di Pembayaran
            </button>
    
            <script type="text/javascript">
                function klaimDiskonPembelianKelipatan(){
                    window.location.href='<?=base_url()?>M_account/transfer_seller2/tambah/<?=$id_akun?>'
                }
            </script>
        </td>
    </tr>
    <?php } ?>
    
    </tfoot>
    
    <table>
    <?php }else{ ?>
    <center><h4>Mohon Maaf Transaksi Anda tidak memenuhi syarat mendapatkan Diskon, Berikut List Produk yang perlu di tambahkan untuk mendapatkan diskon</h4></center>
    <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
    <thead>
        <tr>
            <th>NAMA PRODUK TERDISKON</th>
            <th>JUMLAH SYARAT DISKON</th>
            
        </tr>	
    </thead>
    <tbody>
        <?php 
        $dbTerm = $this->db->query("SELECT m.nama_produk,m.discount_terms_product_quantity 
            FROM v_diskon_terms_produk_arga m
            WHERE m.discount_id = '".$id_diskon."' and  m.product_global_id NOT IN (SELECT c.product_global_id FROM t_sales_detail c WHERE  c.sales_id = '".$id_sales."' and c.sales_detail_quantity >= m.discount_terms_product_quantity)");
        foreach ($dbTerm->result_array() as $key => $vaData2) {
            # code...
        
        ?>
        <tr>
            <td><?=$vaData2['nama_produk']?></td>
            <td><?=$vaData2['discount_terms_product_quantity']?></td>
        </tr>
    <?php } ?>
    </tbody>
    
    <table>
    <?php  } ?>