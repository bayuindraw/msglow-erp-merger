<div class="row">
	<div class="col-lg-12 col-xl-12">
		<div class="kt-portlet">
			<form method="post" action="<?= site_url($url . '/simpan_sales_allow'); ?>" enctype="multipart/form-data" onsubmit="return confirm('Are you sure?')">

				<?= input_hidden('id', $id) ?>
				<?= input_hidden('idAccount', $idAccount) ?>
				<?= input_hidden('cekDep', $cekDep) ?>
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Detail
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">

					<?php
					if (@count($arraccount_detail_sales) > 0) {
						foreach ($arraccount_detail_sales as $index => $arrvalue) { ?>
							<input type="hidden" id="bayar<?= $index ?>" class="sum_total" key="<?= $index ?>" value="<?= $arraccount_detail_sales2[$index]['account_detail_sales_amount'] ?>" />

							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">
										<table>
											<br>
											<tr>
												<td>Sales Order</td>
												<td>&nbsp;&nbsp;:</td>
												<td>&nbsp;&nbsp;<?= $arraccount_detail_sales2[$index]['sales_code']; ?></td>
											</tr>
											<tr>
												<td>Total Tagihan</td>
												<td>&nbsp;&nbsp;:</td>
												<td>&nbsp;&nbsp;Rp <?= number_format($arraccount_detail_sales2[$index]['target_account_detail_debit']); ?></td>
											</tr>
											<tr>
												<td>Total Terbayar</td>
												<td>&nbsp;&nbsp;:</td>
												<td>&nbsp;&nbsp;Rp <?= number_format($arraccount_detail_sales2[$index]['target_account_detail_paid'] - $arraccount_detail_sales2[$index]['account_detail_sales_amount']); ?></td>
											</tr>
											<tr>
												<td>Jumlah Bayar</td>
												<td>&nbsp;&nbsp;:</td>
												<td>&nbsp;&nbsp;Rp <?= number_format($arraccount_detail_sales2[$index]['account_detail_sales_amount']); ?></td>
											</tr>
										</table>
										<br>
									</h3>
								</div>
							</div>
							<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
								<thead>
									<tr>
										<th>No</th>
										<th>NAMA PRODUK</th>
										<th>JUMLAH PESANAN</th>
										<th>KURANG BOLEH KIRIM</th>
										<th>BOLEH KIRIM</th>
										<th>JUMLAH KIRIM</th>
										<th>TOTAL</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 0;
									foreach ($arrvalue as $key => $vaData) {
										if ($vaData['send_allow'] == "") $vaData['send_allow'] = 0;
									?>
										<input type="hidden" value="<?= $vaData['account_detail_real_id'] ?>" name="real_id[<?= $index ?>][<?= $key ?>]" />
										<tr>
											<td><?= ++$no ?></td>
											<td><?= $vaData['nama_produk'] ?> (@Rp <?= number_format($vaData['sales_detail_price']) ?>)<input type="hidden" name="harga_produk[<?= $index ?>][<?= $key ?>]" value="<?= $vaData['sales_detail_price'] ?>" /></td>
											<td><?= number_format($vaData['sales_detail_quantity']) ?></td>
											<td><?= ($vaData['sales_detail_quantity'] - $vaData['send_allow'] >= 0) ? number_format($vaData['sales_detail_quantity'] - $vaData['send_allow']) : 0 ?></td>
											<td><?= ($vaData['send_allow'] >= 0) ? number_format($vaData['send_allow']) : 0  ?></td>
											<td>
												<?php if ($arraccount_detail_sales2[$index]['target_account_detail_debit'] == $arraccount_detail_sales2[$index]['target_account_detail_paid']) { ?>
													<?= ($vaData['sales_detail_quantity'] - $vaData['send_allow'] >= 0) ? $vaData['sales_detail_quantity'] - $vaData['send_allow'] : 0 ?>
													<input type="hidden" name="allow[<?= $index ?>][<?= $key ?>]" max="<?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] ?>" class="form-control md-static sum_total_<?= $index ?>" value="<?= ($vaData['sales_detail_quantity'] - $vaData['send_allow'] >= 0) ? $vaData['sales_detail_quantity'] - $vaData['send_allow'] : 0 ?>" id_product="<?= $vaData['product_id'] ?>" id_debit="<?= $index ?>debit<?= $key ?>" onkeyup="chk_total()">
												<?php } else { ?>
													<input type="text" name="allow[<?= $index ?>][<?= $key ?>]" max="<?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] ?>" class="form-control md-static sum_total_<?= $index ?> numeric" id_dex="<?= $index ?>D<?= $key ?>" price="<?= $vaData['sales_detail_price'] ?>" value="0" id_product="<?= $vaData['product_id'] ?>" id_debit="<?= $index ?>debit<?= $key ?>" onkeyup="chk_total_<?= $index ?>(<?= $vaData['sales_type'] ?>)" required>
												<?php } ?>
												<input type="hidden" name="product_id[<?= $index ?>][<?= $key ?>]" value="<?= $vaData['product_id'] ?>">
												<input type="hidden" name="ids[<?= $index ?>][<?= $key ?>]" value="<?= $vaData['account_detail_sales_id'] ?>">
											</td>
											<td>
												<div id="<?= $index ?>D<?= $key ?>"></div>
												<input type="hidden" id="<?= $index ?>debit<?= $key ?>" value="0" />
											</td>
										</tr>
									<?php } ?>
									<div id="divlist_produk_sisa<?= $index ?>"></div>
									<div id="divdiskon<?= $index ?>"></div>
									<div id="divhoard<?= $index ?>"></div>
									<tr id="tableBonusProduk<?= $index ?>" style="display: none;">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>NAMA PRODUK</th>
														<th>BONUS</th>
													</tr>
												</thead>
												<tbody id="bodyBonusProduk<?= $index ?>">
												</tbody>
											</table>
										</td>
									</tr>
									<tr id="tableBonusPrice<?= $index ?>" style="display: none;">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>HARGA</th>
													</tr>
												</thead>
												<tbody id="bodyBonusPrice<?= $index ?>">
												</tbody>
											</table>
										</td>
									</tr>
									<tr id="tableBonusPersen<?= $index ?>" style="display: none;">
										<td></td>
										<td colspan="6">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>Persen</th>
														<th>Potongan</th>
													</tr>
												</thead>
												<tbody id="bodyBonusPersen<?= $index ?>">
												</tbody>
											</table>
										</td>
									</tr>

								</tbody>
							</table>
					<?php }
					} ?>

					<input type="text" class="form-control" value="0" name="deposit" id="deposit" />

				</div>




				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" style="display: none;" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<button type="button" name="cek_diskon" id="cek_diskon" value="cek_diskon" class="btn btn-success" onclick="get_discount()">Cek Diskon</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>


		</div>
	</div>
</div>

<script>
	<?php
	if (@count($arraccount_detail_sales) > 0) {
		foreach ($arraccount_detail_sales as $index => $arrvalue) { ?>

			function chk_total_<?= $index; ?>(val) {
				$('#simpan').hide();
				var harga = <?= $arraccount_detail_sales2[$index]['account_detail_sales_amount'] ?>;
				var price = 0;
				var total_price = 0;
				var total_price2 = 0;
				var dex = "";
				var val_temp = "";
				$('.sum_total_' + <?= $index ?>).each(function() {
					val_temp = $(this).val().replaceAll(",", "");
					if (val_temp == "") {
						val_temp = 0;
					}
					price = $(this).attr("price") * parseFloat(val_temp);
					total_price = total_price + price;
					dex = $(this).attr("id_dex");
					debit = $(this).attr("id_debit");
					if (parseFloat(val_temp) > parseFloat($(this).attr("max"))) {
						$(this).val($(this).attr("max"));
						$('#' + dex).html('Rp ' + commafy($(this).attr("max") * $(this).attr("price")));
						$('#' + debit).val(($(this).attr("max") * $(this).attr("price")));
						total_price2 += parseFloat($(this).attr("max") * $(this).attr("price"));

					} else {
						$('#' + dex).html('Rp ' + commafy(total_price));
						$('#' + debit).val(total_price);
						total_price2 += parseFloat(total_price);
					}
					total_price = "";
					dex = "";

				});
				if (total_price2 > harga) {
					if (val === 2) {
						$('#cek_diskon').show();
					} else {
						$('#cek_diskon').hide();
					}
				} else {
					$('#cek_diskon').show();
				}
			}
	<?php }
	} ?>

	function get_discount() {
		$('.sum_total').each(function() {
			chk_total_bayar($(this).attr('key'))
		})
		$('#cek_diskon').hide();
	}

	function chk_total_bayar(key) {
		total = 0;
		$('#tableBonusProduk' + key).hide();
		$('#bodyBonusProduk' + key).html('');
		$('#tableBonusPrice' + key).hide();
		$('#bodyBonusPrice' + key).html('');
		$('#tableBonusPersen' + key).hide();
		$('#bodyBonusPersen' + key).html('');
		$('#divdiskon' + key).html('');
		$('#divhoard' + key).html('');
		$('#divlist_produk_sisa' + key).html('');
		var kode = [];
		var quantity = [];
		var price = [];
		$('.sum_total_' + key).each(function() {
			val = $('#' + $(this).attr('id_debit')).val();
			if (val == "") {
				val = 0;
			}
			total = parseFloat(total) + parseFloat(val);
			kode.push($(this).attr('id_product'));
			quantity.push($(this).val().replaceAll(",", ""));
			price.push(val);
		});
		$('#bayar' + key).val(commafy(total));
		$.ajax({
			type: 'post',
			url: '<?= base_url() ?>Test/cek_discount_bayar/' + key,
			data: {
				kode: kode,
				price: price,
				quantity: quantity,
			},
			success: function(result) {
				console.log(result)
				var result2 = JSON.parse(result);
				if (result2.list_diskon_quantity !== "") {
					$('#tableBonusProduk' + key).show();
					$('#bodyBonusProduk' + key).html(result2.list_diskon_quantity);
				}
				if (result2.list_diskon_harga !== "") {
					$('#tableBonusPrice' + key).show();
					$('#bodyBonusPrice' + key).html(result2.list_diskon_harga);
				}
				if (result2.list_diskon_persen !== "") {
					$('#tableBonusPersen' + key).show();
					$('#bodyBonusPersen' + key).html(result2.list_diskon_persen);
					$('#bodyBonusPersen' + key).append(result2.produk_persen);
				}
				if (result2.sisa_produk !== "") {
					$('#divlist_produk_sisa' + key).html(result2.sisa_produk);
				}
				if (result2.reward !== "") {
					$('#divdiskon' + key).html(result2.reward);
				}
				if (result2.tot_hoard !== "") {
					$('#divhoard' + key).html(result2.tot_hoard);
				}
				chk_total2();
			}
		});
	}

	function chk_total2(val) {
		var paid_all = 0;
		var val_x = "";
		$('.sum_total').each(function() {
			val_x = $(this).val();
			if (val_x == "") val_x = 0;
			paid_all += parseFloat(val_x);
		});
		// $('#paid').val(commafy(paid_all));
		var paid = paid_all;
		var deposit = $('#deposit').val();
		deposit = parseFloat(deposit.replaceAll(",", ""));
		var sum = 0;
		var nilai_awal = 0;
		var sum2 = 0;
		var diskon = 0
		$('.price_deduction').each(function() {
			val_x = $(this).val();
			if (val_x == "") val_x = 0;
			diskon += parseFloat(val_x);
		});
		$('#deposit').val(commafy(diskon));
		$('#simpan').show();
	}
</script>