
<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>No</th>
				<th>Tanggal Apply</th>
				<th>Nama Diskon</th>
				<th>Tipe Diskon</th>
				<th>Kategori Diskon</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no = 0;
			foreach ($datadiskon as $key => $vaDiskon) {
				if($vaDiskon['kategori_diskon'] == 'Kelipatan'){
					$cKategoriDiskon = 'Free Produk';
				}else if($vaDiskon['kategori_diskon'] == 'Minimal Pembelian Per PCS'){
					$cKategoriDiskon = 'Free Produk & || Diskon Persentase)';
				}else if($vaDiskon['kategori_diskon'] == '0'){
					$cKategoriDiskon = 'Tiering '.$vaDiskon['tiering'].'';
				}
				?>
				<tr onclick="hideDetail(<?= $key ?>)">
					<td> <img onclick="hideDetail(<?= $key ?>)" src="https://media.istockphoto.com/vectors/plus-and-circle-vector-id1286838570?b=1&k=20&m=1286838570&s=170667a&w=0&h=RvDKabtdeNbn7LyRnEYxSNQ0bZ-yyoUIvubwgCeT9NU=" style="height: 20px"> <?=++$no?>  </td>
					<td><?=$vaDiskon['tanggal_apply_diskon']?></td>
					<td><?=$vaDiskon['discount_name']?></td>
					<td><?=$vaDiskon['tipe_diskon']?></td>
					<td><strong><?=($cKategoriDiskon)?></strong></td>
					<td> 
						<div id="hapus_diskon_<?=$vaDiskon['id_apply_diskon']?>">
					<a class="btn btn-danger"  onclick="return hapusApplyDiskon('<?=$vaDiskon['id_apply_diskon']?>')">
						<i class="fas fa-trash"></i>
					</a>
				</div>
					</td>
					</tr>

					<tr id="<?= $key ?>x" style="display: none;">
						<td colspan="5">
							<?php 
							if($vaDiskon['tipe_diskon'] == 'Non Tiering' and $vaDiskon['kategori_diskon'] == 'Kelipatan'){
								?>									
								<table style="width: 100%" class="table">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Produk</th>
											<th>Quantity</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$cNoKelipatan = 0;
										$dbRewardProduk = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$vaDiskon['id_diskon']."'  ");
										foreach ($dbRewardProduk->result_array() as $key => $vaRewardProduk) {
											?>
											<tr>
												<td><?=++$cNoKelipatan?></td>
												<td><?=$vaRewardProduk['nama_produk']?></td>
												<td><?=$vaRewardProduk['discount_reward_product_quantity']?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>

							<?php }else if($vaDiskon['tipe_diskon'] == 'Non Tiering' and $vaDiskon['kategori_diskon'] == 'Minimal Pembelian Per PCS'){ ?>

								<?php 
								$dbRewardPersen = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$vaDiskon['id_diskon']."' and discount_reward_product_percentage is NOT NULL LIMIT 0,1  ");
								foreach ($dbRewardPersen->result_array() as $key => $vaRewardPersen) {
									$nProsentase = $vaRewardPersen['discount_reward_product_percentage'];
								}
								?>


								<table style="width: 100%" class="table">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Produk</th>
											<th>Quantity</th>
											<th>Nominal Pembelian</th>
											<th>Total Diskon <?=$nProsentase?> % </th>
											<th>Total Setelah Di Diskon </th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$cNoProsentase = 0;
										$nTotalNominal = 0;
										$nTotalDiskon  = 0;
										$nTotalAfterDiskon = 0;
										$dbSalesDetail = $this->db->query("SELECT * FROM t_sales_detail WHERE sales_id = '".$vaDiskon['id_sales']."'  ");
										foreach ($dbSalesDetail->result_array() as $key => $vaSalesDetail) {
											$nDiskon = (($vaSalesDetail['sales_detail_price']*$vaSalesDetail['sales_detail_quantity']) *$nProsentase / 100);
											
											$nAfterDiskon= (($vaSalesDetail['sales_detail_price']*$vaSalesDetail['sales_detail_quantity']) - $nDiskon) ;
											
											$nTotalNominal += $vaSalesDetail['sales_detail_price'] * $vaSalesDetail['sales_detail_quantity'];

											$nTotalDiskon  += $nDiskon;
											$nTotalAfterDiskon += $nAfterDiskon;
											?>
											<tr>
												<td><?=++$cNoProsentase?></td>
												<td><?=$vaSalesDetail['product_global_name']?></td>
												<td><?=$vaSalesDetail['sales_detail_quantity']?></td>
												<td>Rp. <?=number_format($vaSalesDetail['sales_detail_price']*$vaSalesDetail['sales_detail_quantity'])?></td>
												<td>Rp. <?=number_format($nDiskon)?></td>
												<td>Rp. <?=number_format($nAfterDiskon)?></td>
											</tr>
										<?php } ?>
										<tfoot>
											<tr>
												<td colspan="3"></td>
												
												<td><strong>Rp. <?=number_format($nTotalNominal)?></strong></td>
												<td><strong>Rp. <?=number_format($nTotalDiskon)?></strong></td>
												<td><strong>Rp. <?=number_format($nTotalAfterDiskon)?></strong></td>
											</tr>
										</tfoot>
									</tbody>
								</table> <br/>
								<h4>Free Produk</h4>
								<table class="table">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Produk</th>
											<th>Quantity</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$cNoProsentaseProduk = 0;
										$dbRewardProduk = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$vaDiskon['id_diskon']."' and discount_reward_product_percentage is null  ");
										foreach ($dbRewardProduk->result_array() as $key => $vaRewardProduk) {
											?>
											<tr>
												<td><?=++$cNoProsentaseProduk?></td>
												<td><?=$vaRewardProduk['nama_produk']?></td>
												<td colspan="3"><?=$vaRewardProduk['discount_reward_product_quantity']?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							<?php }else if($vaDiskon['tipe_diskon'] == 'Tiering' and $vaDiskon['kategori_diskon'] == '0'){ ?>
								<table style="width: 100%" class="table">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Produk</th>
											<th>Quantity</th>
											<th>Harga Produk</th>
											<th>Harga Diskon tiering</th>
											<th>Total Harga Produk </th>
											<th>Total Diskon </th>
											<th>Total Bayar </th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$cNoTier = 0;
										$nTotalHarga = 0;
										$nTotalDiskon = 0;
										$nDiskon = 0;
										$dbTier = $this->db->query("SELECT s.product_global_name,s.sales_detail_quantity,s.sales_detail_price,m.discount_terms_product_price
											FROm
											t_sales_detail s,m_discount_terms_product m where s.product_global_id = m.product_global_id and 
												s.sales_id = '".$vaDiskon['id_sales']."' and 
												m.discount_terms_product_quantity = '".$vaDiskon['tiering']."' and m.discount_id = '".$vaDiskon['id_diskon']."'");
										foreach ($dbTier->result_array() as $key => $vaTier) { 
											$nTotalHarga += $vaTier['sales_detail_price']*$vaTier['sales_detail_quantity'];
											$nTotalDiskon += $vaTier['discount_terms_product_price']*$vaTier['sales_detail_quantity'];
											$nDiskon += ($vaTier['sales_detail_price']-$vaTier['discount_terms_product_price']) * $vaTier['sales_detail_quantity']
										?>
										<tr>
											<td><?=++$cNoTier?></td>
											<td><?=$vaTier['product_global_name']?></td>
											<td><?=$vaTier['sales_detail_quantity']?></td>
											<td><?=number_format($vaTier['sales_detail_price'])?></td>
											<td><strong><?=number_format($vaTier['discount_terms_product_price'])?></strong></td>
											<td><?=number_format($vaTier['sales_detail_price']*$vaTier['sales_detail_quantity'])?> </td>

											<td><?=number_format(
	($vaTier['sales_detail_price']-$vaTier['discount_terms_product_price']) * $vaTier['sales_detail_quantity'])?> </td>
											<td><?=number_format($vaTier['discount_terms_product_price']*$vaTier['sales_detail_quantity'])?> </td>
											
										</tr>
										<?php 
											}
										?>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5"></td>
											<td><strong><?=number_format($nTotalHarga)?></strong></td>
											<td><strong><?=number_format($nDiskon)?></strong></td>
											<td><strong><?=number_format($nTotalDiskon)?></strong></td>
										</tr>
									</tfoot>
								</table>
							<?php } ?>

						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>

		<script type="text/javascript">
			function hideDetail(key){
				if($('#'+key+'x').attr('style') == 'display:none;'){
					$('#'+key+'x').attr('style', '')
				}else{
					$('#'+key+'x').attr('style', 'display:none;')
				}
			}
		</script>