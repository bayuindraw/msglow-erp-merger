<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				Verifikasi Transfer Akun
			</h3>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Seller</th>
					<th>Rekening Tujuan</th>
					<th>Nama Pengirim</th>
					<th>Tanggal</th>
					<th>Nominal</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<!--end: Datatable -->
	</div>
</div> 

<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",
			scrollY: true,
			scrollX: true,
			scrollCollapse: true,
			"processing": true,
			"serverSide": true,
			"ajax": "<?= site_url() ?>/<?= $url ?>/get_data_verification_transfer_seller2"
		});
	});
</script>

<div class="modal fade" id="modal-paket" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">BUKTI TRANSFER</h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
 // function showKartuStokA($idbarang, $bulan, $tahun) {
 	function showKartuStokA(file) {
 		$("#modal-paket").modal('show');
    /*$.ajax({
      type: "POST",

      url: "<?php echo base_url() ?>Administrator/Stock_Produk/tampil_stock_produk_real/" + $idbarang + "/" + $bulan + "/" + $tahun,
      cache: false,
      success: function(msg) {
        $(".modal-body").html(msg);

      }
  });*/
  htmx = '<img src="<?= site_url()."upload/TRANSFER/"?>'+file+'" width="100%">';
  $(".modal-body").html(htmx);
}
</script>