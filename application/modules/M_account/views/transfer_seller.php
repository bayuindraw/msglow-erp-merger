
							<div class="row">
							
							<div class="col-lg-12 col-xl-12">
									<div class="kt-portlet">
										<form method="post" action="<?= site_url($url . '/set_transfer_seller'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Transfer 
												</h3>  
											</div>
										</div>
											<div class="kt-portlet__body">
												
												<div class="form-group">
																						<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>No</th>
												<th>Tanggal</th>
												<th>Jumlah Transfer</th>
												<th>Rekening Tujuan</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>

												</div>
											</div>
										</div>
									</div>
														

							
<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
		"processing": true,
		"serverSide": true,
		"ajax": "<?= site_url() ?>/<?= $url ?>/get_data_transfer/<?= $id ?>"
		});
	});
</script>
								<div class="col-lg-12 col-xl-12">
									<div class="kt-portlet">
										<form method="post" action="<?= site_url($url . '/set_transfer_seller'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Transfer 
												</h3>  
											</div>
										</div>
											<div class="kt-portlet__body">
												
												<div class="form-group">
													<label>Rekening Tujuan</label>
													<select class="form-control" id="exampleSelect1" name="to[account_id]" required>
														<option value="">Pilih Akun</option>
														<?php
														foreach ($m_account as $data) { 
															echo "<option value='" . $data->account_id . "'".(($data->account_id == $role_id)?'selected':'').">" . $data->account_name . "</option>";
														}
														?>
													</select>
												</div>
												<!--<div class="form-group">
													<label>PIC</label>
													<input type="text" class="form-control" placeholder="PIC" name="from[account_detail_pic]" value="<?= @$_SESSION['user_fullname'] ?>" required>
												</div>-->
												<div class="form-group">
													<label>Bukti Transfer</label>
													<input type="file" class="form-control" name="transfer">
												</div>
												<div class="form-group">
													<label>Nama Pengirim</label>
													<input type="text" class="form-control" placeholder="Nama Pengirim" name="from[account_detail_transfer_name]">
												</div>
												<div class="form-group">
													<label>Catatan</label>
													<textarea class="summernote" name="from[account_detail_note]"><?= @$row['account_detail_note'] ?></textarea>
												</div>
												<div class="form-group">
													<label>Tanggal</label>
													<input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="from[account_detail_date]" required>
												</div>
												
											
										<div class="form-group">
													<label>Nominal</label>
													<input type="text" class="form-control numeric" placeholder="Nominal" name="from[account_detail_credit]" value="0" id="paid" required onkeyup="chk_total()">
												</div>
										<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
							<th>No</th>
							<th>KODE SO</th>
							<th>TANGGAL</th>
							<th>TOTAL TAGIAN</th>
							<th>KURANG BAYAR</th>
							<th>BAYAR</th> 
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 0;
							foreach ($arrlist_account_sales as $key => $vaData) {
							?>
							<tr>
								<td><?= ++$no ?></td>
								<td onclick="$('#<?= $key ?>').toggle();"><?= $vaData['sales_code'] ?></td>
								<td><?= $vaData['sales_date'] ?></td>
								<td>Rp <?= number_format($vaData['account_detail_debit']) ?></td>
								<td>Rp <?= number_format($vaData['account_detail_debit'] - $vaData['account_detail_paid']) ?></td>	
								<td><input type="text" name="paid[<?= $vaData['account_detail_real_id'] ?>]" max="<?= $vaData['account_detail_debit'] - $vaData['account_detail_paid'] ?>" class="form-control md-static sum_total numeric" value="0" onkeyup="chk_total()"><input type="hidden" name="sales[<?= $vaData['account_detail_real_id'] ?>]" value="<?= $vaData['sales_id']; ?>"></td>
							</tr>
							<tr id="<?= $key ?>" style="display:none;">
									<td></td>
									<td colspan="5">
										<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
									<thead>
										<tr>
										<th>Nama Produk</th>
										<th>Jumlah Pesanan</th>  
										</tr>
									</thead>
									<tbody>
										<?php
										foreach ($arrdetail_sales[$vaData['sales_id']] as $keyx => $valuex) {
										?>
										<tr>
											<td><?= $valuex['nama_produk'] ?></td>
											<td><?= $valuex['sales_detail_quantity'] ?></td>
										</tr>
										<?php } ?>
							
									</tbody>
							</table></td>
						</tr>
							<?php } ?>
				
						</tbody>
					</table>
					<div class="form-group">
													<label>Deposit</label>
													<input type="text" name="deposit" id="deposit" readonly class="form-control numeric" value="0" required>
												</div>
					</div>
										<!--<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Akun Tujuan
												</h3>  
											</div>
										</div>-->
										

											
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>

<script>
	function chk_total(val){
		var paid = $('#paid').val();
		paid = paid.replaceAll(",", "");
		var sum = 0;
		var nilai_awal = 0;
		$('.sum_total').each(function(){
			nilai_awal = $(this).val();
			if(parseFloat(nilai_awal.replaceAll(",", "")) > parseFloat($(this).attr("max"))){
				$(this).val(commafy($(this).attr("max")));
				sum += parseFloat($(this).attr("max")); 
			}else{
				sum += parseFloat(nilai_awal.replaceAll(",", ""));
			}
				//if(parseFloat(this.value) > 0)(
					
			//}
		});
		$('#deposit').val(commafy(paid - sum));
		if((paid - sum) < 0){
			$('#simpan').hide();
		}else{
			$('#simpan').show();
		}
	}
</script>