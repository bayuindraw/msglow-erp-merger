<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-approve"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											<?= $title ?>
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">
						<?php if($data['user_secret'] != ""){ ?>
							<form method="post" action="<?= site_url($url . '/delete_authenticator'); ?>" enctype="multipart/form-data">
								<?= input_hidden('id', $id) ?>
								<?= input_hidden('secret', @$secret) ?>
										<div class="col-lg-12">
										<div class="alert alert-secondary" role="alert">
																	<div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
																	<div class="alert-text">
																		User tidak dapat mengautentikasi kode, jika kode autentikasi sebelumnya belum dihapus.
																	</div>
																</div>
															<label class="kt-option">
																
																<span class="kt-option__control">
																	<span class="kt-radio">
																		<input type="checkbox" name="check">
																		<span></span>
																	</span>
																</span>
																<span class="kt-option__label">
																	<span class="kt-option__head">
																		<span class="kt-option__title">
																			Hapus Kode Autentikasi
																		</span>
																		<span class="kt-option__focus">
																			
																		</span>
																	</span>
																	<span class="kt-option__body">
																		(&nbsp;Jika kode autentikasi dihapus maka user tidak dapat melakukan login ke aplikasi sebelum autentikasi ulang&nbsp;)
																	</span>
																</span>
															</label>
														</div>
														<hr>
									<div class="form-group">
										<button type="submit" name="simpan" value="true" class="btn btn-warning"><i class="fa fa-trash"></i>Hapus</button>
										<a href="<?= site_url($url) ?>" class="btn btn-danger"><i class="fa fa-reply"></i>Kembali</a>
									</div>
															</form>
	<?php }else{ ?>
								<form method="post" action="<?= site_url($url . '/cek_authentication_code'); ?>" enctype="multipart/form-data">
								<?= input_hidden('id', $id) ?>
								<?= input_hidden('secret', @$secret) ?>
								
									<div class="form-group">
										<img src="<?php echo $qr_code; ?>">
									</div>
									<div class="form-group">
										<label>Authentication Code</label>
										<?= input_text('authentication_code', @$user_name) ?>
									</div>
									<hr>
									<div class="form-group">
										<button type="submit" name="simpan" value="true" class="btn btn-info"><i class="fa fa-save"></i>Simpan</button>
										<a href="<?= site_url($url) ?>" class="btn btn-danger"><i class="fa fa-reply"></i>Kembali</a>
									</div>
								
								</form>
	<?php } ?>
</div>
</div>
