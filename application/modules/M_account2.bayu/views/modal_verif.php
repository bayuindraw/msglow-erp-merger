<form id="formID" action="<?= base_url() ?>M_account2/set_verif_rev" method="POST" enctype="multipart/form-data" onsubmit="return confirm('Are you sure?')">
    <div class="form-group">
        <input type="hidden" name="account_id" value="<?= $acc ?>" />
        <input type="hidden" name="mistake_id" value="<?= $mistake['mistake_id'] ?>" />
        <label>Requested by</label>
        <div class="kt-radio-inline">
            <label class="kt-radio">
                <input type="radio" value="2" <?= $mistake['mistake_suspect_type'] == 2 ? 'checked' : '' ?> name="mistake_suspect_type" required> Sales
                <span></span>
            </label>
            <label class="kt-radio">
                <input type="radio" value="3" <?= $mistake['mistake_suspect_type'] == 3 ? 'checked' : '' ?> name="mistake_suspect_type"> Accounting
                <span></span>
            </label>
            <label class="kt-radio">
                <input type="radio" value="1" <?= $mistake['mistake_suspect_type'] == 1 ? 'checked' : '' ?> name="mistake_suspect_type"> Customer
                <span></span>
            </label>
        </div>
        <label>Keterangan</label>
        <textarea class="form-control" name="mistake_note"><?= $mistake['mistake_note'] ?></textarea>
    </div>
    <button type="submit" class="btn btn-success waves-effect">Verification</button>
</form>

<script>
    $('button[type="submit"]').on('click', function() {
        this.disabled = true;
        this.value = 'Submitting...';
        this.disabled = false;
    });
    $(".numeric").mask("#,##0", {
        reverse: true
    });
</script>