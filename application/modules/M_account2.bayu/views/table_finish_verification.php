              <div class="kt-portlet kt-portlet--mobile">
              	<div class="kt-portlet__head kt-portlet__head--lg">
              		<div class="kt-portlet__head-label">
              			<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
              			<h3 class="kt-portlet__head-title">
              				Verifikasi Transfer Akun
              			</h3>
              		</div>
              	</div>
              	<div class="kt-portlet__body">

              		<!--begin: Datatable -->
              		<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
              			<thead>
              				<tr>
              					<th>No</th>
              					<th>Nama</th>
              					<th>Suspect</th>
              					<th>Jumlah Sebelum</th>
              					<th>Jumlah Revisi</th>
              					<th></th>
              				</tr>
              			</thead>
              			<tbody>
              				<?php foreach ($revisis as $index => $revisi) { ?>
              					<tr>
              						<td><?= $index + 1 ?></td>
              						<td><?= $revisi['nama'] ?></td>
              						<td><?= $revisi['mistake_suspect_name'] == null ? $_SESSION['user_fullname'] : $revisi['mistake_suspect_name'] ?></td>
              						<td>Rp <?= number_format($revisi['past_sum']) ?></td>
              						<td>Rp <?= number_format($revisi['sum']) ?></td>
              						<td><span onclick="showKartuForm(<?= $revisi['mistake_id'] ?>, <?= $revisi['account_id'] ?>)" class="btn btn-success"> <i class="fa fa-check"></i></span></td>
              					</tr>
              				<?php } ?>
              			</tbody>
              		</table>

              		<!--end: Datatable -->
              	</div>
              </div>

              <div class="modal fade" id="modal-form" tabindex="-1" role="dialog">
              	<div class="modal-dialog modal-lg" role="document">
              		<div class="modal-content">
              			<div class="modal-header">
              				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              					<span aria-hidden="true">&times;</span>
              				</button>
              				<h4 class="modal-title">Verifikasi</h4>
              			</div>
              			<div class="modal-body">
              			</div>
              			<div class="modal-footer">
              				<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

              			</div>
              		</div>
              	</div>
              </div>

              <script type="text/javascript">
              	$(document).ready(function() {
              		$('#kt_table_1').DataTable({
              			"pagingType": "full_numbers",
              			scrollY: "300px",
              			scrollX: true,
              			scrollCollapse: true,
              		});
              	});

              	function showKartuForm(file, acc) {
              		$("#modal-form").modal('show');
              		$.ajax({
              			type: "POST",

              			url: "<?php echo base_url() ?>M_account2/get_verif/" + file + "/" + acc,
              			cache: false,
              			success: function(msg) {
              				$(".modal-body").html(msg);
              			}
              		});

              	}
              </script>