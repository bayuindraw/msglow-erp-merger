
							<div class="row">
								<div class="col-lg-12 col-xl-12">
									<div class="kt-portlet">
										<form method="post" action="<?= site_url($url . '/simpan_sales_allow'); ?>" enctype="multipart/form-data">
											
											<?= input_hidden('id', $id) ?>
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Detail 
												</h3>  
											</div>
										</div>
											<div class="kt-portlet__body">
												
												<?php 
												if(@count($arraccount_detail_sales) > 0){
												foreach($arraccount_detail_sales as $index => $arrvalue){ ?>
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<table>
														<br>
														<tr>
															<td>Sales Order</td>
															<td>&nbsp;&nbsp;:</td>
															<td>&nbsp;&nbsp;<?= $arraccount_detail_sales2[$index]['sales_code']; ?></td>
														</tr>
														<tr>
															<td>Total Tagihan</td>
															<td>&nbsp;&nbsp;:</td>
															<td>&nbsp;&nbsp;Rp <?= number_format($arraccount_detail_sales2[$index]['target_account_detail_debit']); ?></td>
														</tr>
														<tr>
															<td>Total Terbayar</td>
															<td>&nbsp;&nbsp;:</td>
															<td>&nbsp;&nbsp;Rp <?= number_format($arraccount_detail_sales2[$index]['target_account_detail_paid'] - $arraccount_detail_sales2[$index]['account_detail_sales_amount']); ?></td>
														</tr>
														<tr>
															<td>Jumlah Bayar</td>
															<td>&nbsp;&nbsp;:</td>
															<td>&nbsp;&nbsp;Rp <?= number_format($arraccount_detail_sales2[$index]['account_detail_sales_amount']); ?></td>
														</tr>
													</table>
													<br>
												</h3>  
											</div>
										</div>
										<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
							<th>No</th>
							<th>NAMA PRODUK</th>
							<th>JUMLAH PESANAN</th>
							<th>KURANG BOLEH KIRIM</th>
							<th>BOLEH KIRIM</th>
							<th>JUMLAH KIRIM</th>
							<th>TOTAL</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 0;
							foreach ($arrvalue as $key => $vaData) {
							?>
							<tr>
								<td><?= ++$no ?></td>
								<td><?= $vaData['nama_produk'] ?> (@Rp <?= number_format($vaData['sales_detail_price'])?>)</td>
								<td><?= $vaData['sales_detail_quantity'] ?></td>	
								<td><?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] ?></td>	
								<td><?= $vaData['send_allow'] ?></td>	
								<td>
								<?php if($arraccount_detail_sales2[$index]['target_account_detail_debit'] == $arraccount_detail_sales2[$index]['target_account_detail_paid']){?>
									<?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] ?>
									<input type="hidden" name="allow[<?= $index ?>][<?= $key ?>]" max="<?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] ?>" class="form-control md-static sum_total_<?= $index ?>" value="<?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] ?>" onkeyup="chk_total()">
								<?php }else{ ?>
									<input type="text" name="allow[<?= $index ?>][<?= $key ?>]" max="<?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] ?>" class="form-control md-static sum_total_<?= $index ?>" id_dex="<?= $index ?>D<?= $key ?>" price="<?= $vaData['sales_detail_price'] ?>" value="0" onkeyup="chk_total_<?= $index?>()" required>
								<?php } ?> 
								<input type="hidden" name="product_id[<?= $index ?>][<?= $key ?>]" value="<?= $vaData['product_id'] ?>">
								<input type="hidden" name="ids[<?= $index ?>][<?= $key ?>]" value="<?= $vaData['account_detail_sales_id'] ?>">
								</td>
								<td ><div id="<?= $index ?>D<?= $key ?>"></div>
								</td>
							</tr>
							<?php } ?>
				
						</tbody>
					</table>
												<?php } 
												} ?>
												
					</div>
										
										

											
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>

<script>

<?php 
												if(@count($arraccount_detail_sales) > 0){
												foreach($arraccount_detail_sales as $index => $arrvalue){ ?>
	function chk_total_<?= $index; ?>(val){
		var harga = <?= $arraccount_detail_sales2[$index]['account_detail_sales_amount'] ?>;
		var price = 0;
		var total_price = 0;
		var total_price2 = 0;
		var dex = "";
		var val_temp = "";
		$('.sum_total_'+<?= $index ?>).each(function(){			
			val_temp = $(this).val();
			if(val_temp==""){
				val_temp = 0;
			}
			price = $(this).attr("price") * parseFloat(val_temp);
			total_price = total_price + price;
			dex = $(this).attr("id_dex");
			if(parseFloat(val_temp) > parseFloat($(this).attr("max"))){
				$(this).val($(this).attr("max"));
				$('#'+dex).html('Rp '+commafy($(this).attr("max") * $(this).attr("price")));
				total_price2 += parseFloat($(this).attr("max") * $(this).attr("price"));
				
			}else{
				$('#'+dex).html('Rp '+commafy(total_price));
				total_price2 += parseFloat(total_price);
			}
			total_price = "";
			dex = "";
			
		});
		if(total_price2 > harga){
			$('#simpan').hide();
		}else{
			$('#simpan').show();
		}
	}
												<?php }
												}?>
</script>