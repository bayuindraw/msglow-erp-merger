<div class="col-lg-12 col-xl-12">
    <div class="kt-portlet">
        <form method="post" action="<?= base_url() ?>M_account2/set_mutasi_bank" enctype="multipart/form-data">
        <input type="hidden" name="coa_id" value="<?= $coa['id'] ?>"/>
        <input type="hidden" name="coa_code" value="<?= $coa['kode'] ?>"/>
        <input type="hidden" name="coa_name" value="<?= $coa['nama'] ?>"/>
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Mutasi || <?= $coa['kode'] ?> - <?= $coa['nama'] ?>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Debit</th>
                            <th>Kredit</th>
                            <th>Keterangan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="table">

                    <?php $i = 0;
                     foreach($datas as $index=>$data){ ?>
                    
                        <tr id="table_<?= $index ?>">

                            <td><input type="text" class="form-control" placeholder="Tanggal" name="input[<?= $index ?>][coa_date]" value="<?= @$data['coa_date'] ?>" required></td>
                            <td><input type="text" class="form-control numeric paid_total" readonly="" placeholder="Input Debit" name="input[<?= $index ?>][coa_debit]" required value="<?= ($data['coa_debit'] == 00)?0: $data['coa_debit']?>"></td>
                            <td><input type="text" class="form-control numeric paid_total" readonly="" placeholder="Input Kredit" name="input[<?= $index ?>][coa_credit]" required value="<?= ($data['coa_credit'] == 00)?0: $data['coa_credit']?>"></td>
                            <td><input type="text" class="form-control" placeholder="Nominal" name="input[<?= $index ?>][coa_transaction_note]" value="<?= $data['coa_transaction_note'] ?>" required></td>
                            <td><button onclick="myDeleteFunction('<?= $index ?>')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
                        </tr>
                        <?php 
                        $i++;
                     } ?>
                    </tbody>
                </table>
                <div class="kt-form__actions">
                    <button type="button" class="btn btn-warning" onclick="add();">Tambah Transaksi</button>
                </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                    <a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
                </div>
            </div>
        </form>


    </div>
</div>

<script>
	var count = <?= $i; ?>;
    
    function add() {
		count = count + 1;
		var html = '<tr id="table_'+ count +'"><td><input type="text" class="form-control" placeholder="Tanggal" name="input['+ count +'][coa_date]" required></td><td><input type="text" class="form-control numeric paid_total" placeholder="Input Debit" name="input['+ count +'][coa_debit]" required></td><td><input type="text" class="form-control numeric paid_total"  placeholder="Input Kredit" name="input['+ count +'][coa_credit]" required></td><td><input type="text" class="form-control" placeholder="Nominal" name="input['+ count +'][coa_transaction_note]" required></td><td><button onclick="myDeleteFunction('+ count +')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#table').append(html);
	}

    function myDeleteFunction(id) {
		$('#table_' + id).remove();
	}

</script>