<?php
$name = "";
$email = "";
$remember_token = "";
$password = "";
$role_id = "";
if ($parameter == 'ubah' && $id != '') {
	$this->db->where('account_id', $id);
	$row = $this->Model->get()->row_array();
}
?>
<div class="row">
								<div class="col-lg-12 col-xl-6">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Kode</label>
													<input type="text" class="form-control" placeholder="Username" name="input[account_code]" value="<?= @$row['account_code'] ?>" required>
												</div>
												<div class="form-group">
													<label>Nama</label>
													<input type="text" class="form-control" placeholder="Nama" name="input[account_name]" value="<?= @$row['account_name'] ?>" required>
												</div>
												<div class="form-group">
													<label>Debit</label>
													<input type="number" class="form-control" placeholder="Debit" name="input[account_debit]" value="<?= @$row['account_debit'] ?>" required>
												</div>
												<div class="form-group">
													<label>Kredit</label>
													<input type="number" class="form-control" placeholder="Kredit" name="input[account_credit]" value="<?= @$row['account_credit'] ?>" required>
												</div>
												<div class="form-group">
													<label>PIC</label>
													<select class="form-control" id="exampleSelect1" name="input[account_pic_id]" required>
														<option value="">Pilih PIC</option> 
														<?php
														foreach ($m_employee as $data) { 
															echo "<option value='" . $data->employee_id . "'".(($data->employee_id == @$row['account_pic_id'])?'selected':'').">" . $data->employee_name . "</option>";
														}
														?>
													</select>
												</div> 
												<div class="form-group">
													<label>Tanggal</label>
													<input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="input[account_date_reset]" value="<?= @$row['account_date_reset'] ?>">
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>