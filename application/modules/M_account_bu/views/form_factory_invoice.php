
							<div class="row">
							
								<div class="col-lg-12 col-xl-12">
									<div class="kt-portlet">
										<form method="post" action="<?= site_url($url . '/set_transfer_seller'); ?>" enctype="multipart/form-data">
											<?= input_hidden('id', $id) ?>
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Invoice
												</h3>  
											</div>
										</div>
											<div class="kt-portlet__body">
												
												<div class="form-group">
													<label>Invoice</label>
													<input type="text" class="form-control" placeholder="Nama Pengirim" name="from[account_detail_transfer_name]">
												</div>
												<div class="form-group">
													<label>Tanggal</label>
													<input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="from[account_detail_date]" required>
												</div>
												<?php foreach ($arrterima_produk as $keyx => $vaDatax) { ?>
												<div class="form-group">
													<label><?= $keyx; ?></label>
												</div>
										<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
							<th>NO</th>
							<th>NAMA BARANG</th>
							<th>TANGGAL</th>
							<th>JUMLAH</th>
							<th>HPP</th>
							<th>TOTAL TAGIHAN</th> 
							<th>CHECK</th> 
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 0;
							foreach ($arrlist_account_sales as $key => $vaData) {
							?>
							<tr>
								<td><?= ++$no ?></td>
								<td><?= $vaData['sales_code'] ?></td>
								<td><?= $vaData['sales_date'] ?></td>
								<td>Rp <?= number_format($vaData['account_detail_debit']) ?></td>
								<td>Rp <?= number_format($vaData['account_detail_debit'] - $vaData['account_detail_paid']) ?></td>	
								<td><input type="text" name="paid[<?= $vaData['account_detail_real_id'] ?>]" max="<?= $vaData['account_detail_debit'] - $vaData['account_detail_paid'] ?>" class="form-control md-static sum_total numeric" value="0" onkeyup="chk_total()"><input type="hidden" name="sales[<?= $vaData['account_detail_real_id'] ?>]" value="<?= $vaData['sales_id']; ?>"></td>
							</tr>
							<?php } ?>
				
						</tbody>
					</table>
					<?php } ?>
					<div class="form-group">
													<label>Deposit</label>
													<input type="text" name="deposit" id="deposit" readonly class="form-control numeric" value="0" required>
												</div>
					</div>

											
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>

<script>
	function chk_total(val){
		var paid = $('#paid').val();
		paid = paid.replaceAll(",", "");
		var sum = 0;
		var nilai_awal = 0;
		$('.sum_total').each(function(){
			nilai_awal = $(this).val();
			if(parseFloat(nilai_awal.replaceAll(",", "")) > parseFloat($(this).attr("max"))){
				$(this).val(commafy($(this).attr("max")));
				sum += parseFloat($(this).attr("max")); 
			}else{
				sum += parseFloat(nilai_awal.replaceAll(",", ""));
			}
				//if(parseFloat(this.value) > 0)(
					
			//}
		});
		$('#deposit').val(commafy(paid - sum));
		if((paid - sum) < 0){
			$('#simpan').hide();
		}else{
			$('#simpan').show();
		}
	}
</script>