<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
    <thead>
    	<tr>
    		<th>Rekening Tujuan</th>
    		<th>Tanggal</th>
    		<th>Nama Pengirim</th>
    		<th>Bukti Transfer</th>
    		<th>Nominal (sales)</th>
    		<th>Verif Nominal</th>
    		<th>Detail</th>
    	</tr>
    </thead>
    <tbody>
    <?php foreach ($mutasis as $key => $mutasi) { ?>
        <input type="hidden" name="input[<?= $key ?>][source_id]" value="<?= $mutasi['account_detail_real_id'] ?>" />
        <tr>
            <td><input type="hidden" value="<?= $mutasi['account_name'] ?>" name="input[<?= $key ?>][account_name]"/><?= $mutasi['account_name'] ?></td>
            <td><input type="hidden" value="<?= $mutasi['account_detail_date'] ?>" name="input[<?= $key ?>][account_detail_date]"/><?= $mutasi['account_detail_date'] ?></td>
            <td><?= $mutasi['account_detail_transfer_name'] ?></td>
            <td><img src="<?= $mutasi['proof'] ?>" style="max-width: 90%;"/></td>
            <td><input type="hidden" value="<?= $mutasi['account_detail_debit'] ?>" name="input[<?= $key ?>][account_detail_debit]"/><?= $mutasi['account_detail_debit'] ?></td>
            <td><input type="number" name="input[<?= $key ?>][nominal]" value="<?= $mutasi['account_detail_debit'] ?>" readonly=""/></td>
            <td><a class="btn btn-primary" href="<?= base_url() ?>M_account/get_revisi/<?= $mutasi['account_detail_real_id'] ?>"><i class="fa fa-list"></i></a></td>
        </tr>
    <?php } ?>
    </tbody>
	</table>

<script>
</script>