<div class="row">
    <form method="post" action="<?= site_url($url . '/set_transfer_seller_parsial'); ?>" onsubmit="return confirm('Are you sure?')" enctype="multipart/form-data">
        <div class="col-lg-12 col-xl-12">
            <div class="kt-portlet">

                <?= input_hidden('parameter', $parameter) ?>
                <?= input_hidden('id', $id) ?>
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Form Transfer <br>
                            (<?= $arraccount['nama']; ?> : <?= $arraccount['kode']; ?>)
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <div class="form-group">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Jumlah Transfer</th>
                                    <th>Rekening Tujuan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#kt_table_1').DataTable({
                    "pagingType": "full_numbers",
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    "processing": true,
                    "serverSide": true,
                    "ajax": "<?= site_url() ?>/<?= $url ?>/get_data_transfer/<?= $id ?>"
                });
            });
        </script>
        <div class="col-lg-12 col-xl-12">
            <div class="kt-portlet">
                <?= input_hidden('parameter', $parameter) ?>
                <?= input_hidden('id', $id) ?>
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Transfer
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                        <thead>
                            <tr>
                                <th>Rekening Tujuan</th>
                                <th>Nama Pengirim</th>
                                <th>Tanggal</th>
                                <th>Nominal</th>
                                <th>Bukti Transfer</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="x">

                            <tr id="0">
                                <td><select class="form-control" id="exampleSelect1" name="account_id[0]">
                                        <option value="">Pilih Akun</option>
                                        <?php
                                        foreach ($m_account as $data) {
                                            echo "<option value='" . $data->account_id . "'" . (($data->account_id == $role_id) ? 'selected' : '') . ">" . $data->account_name . "</option>";
                                        }
                                        ?>
                                    </select></td>

                                <td><input type="text" class="form-control" placeholder="Nama Pengirim" name="from[account_detail_transfer_name][0]"></td>
                                <td><input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="from[account_detail_date][0]"></td>

                                <td><input type="text" class="form-control numeric paid_total" placeholder="Nominal" name="from[account_detail_credit][0]" value="0" onkeyup="chk_total()"></td>
                                <td><input type="file" class="form-control" name="transfer_0"></td>
                                <td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="kt-form__actions">
                        <button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Transaksi</button>
                    </div>


                    <br>
                    <br>
                    <div class="form-group">
                        <label>Deposit</label>
                        <input type="text" class="form-control numeric" placeholder="Deposit" name="deposit" id="deposit" value="<?= $arraccount['account_deposit'] ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Nominal</label>
                        <input type="text" class="form-control numeric" placeholder="Nominal" name="nominal" value="0" id="paid" required onkeyup="chk_total()" readonly>
                    </div>
                    <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>KODE SO</th>
                                <th>TANGGAL</th>
                                <th>TOTAL TAGIAN</th>
                                <th>BAYAR</th>
                                <th>KURANG BAYAR</th>
                                <th>BAYAR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0;
                            foreach ($arrlist_account_sales as $key => $vaData) {
                            ?>
                                <tr>
                                    <td><?= ++$no ?></td>
                                    <td onclick="$('#<?= $key ?>x').toggle();"><?= $vaData['sales_code'] ?></td>
                                    <td><?= $vaData['sales_date'] ?></td>
                                    <td>Rp <?= number_format($vaData['account_detail_debit']) ?></td>
                                    <td>Rp <?= number_format($vaData['account_detail_paid']) ?></td>
                                    <td>Rp <?= number_format($vaData['account_detail_debit'] - $vaData['account_detail_paid']) ?></td>
                                    <td><input type="text" name="paid[<?= $vaData['account_detail_real_id'] ?>]" max="<?= $vaData['account_detail_debit'] - $vaData['account_detail_paid'] ?>" class="form-control md-static sum_total numeric" value="0" onkeyup="chk_total()"><input type="hidden" name="sales[<?= $vaData['account_detail_real_id'] ?>]" value="<?= $vaData['sales_id']; ?>"></td>
                                </tr>
                                <tr id="<?= $key ?>x" style="display:none;">
                                    <td></td>
                                    <td colspan="6">
                                        <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                                            <thead>
                                                <tr>
                                                    <th>Nama Produk</th>
                                                    <th>Jumlah Pesanan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($arrdetail_sales[$vaData['sales_id']] as $keyx => $valuex) {
                                                ?>
                                                    <tr>
                                                        <td><?= $valuex['nama_produk'] ?></td>
                                                        <td><?= number_format($valuex['sales_detail_quantity']) ?></td>
                                                    </tr>
                                                <?php } ?>

                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                    <div class="form-group">
                        <label>Deposit</label>
                        <input type="text" name="deposit2" id="deposit2" readonly class="form-control numeric" value="0" required>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-12">
                    <div class="kt-portlet">

                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Alamat Kirim
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <label>Ekspedisi:</label>
                                    <select class="form-control kt-select2 select_ekspedisi" name="input2[ekspedisi]">
                                        <option value=""></option>
                                        <?php foreach ($deliveries as $delivery) { ?>
                                            <option value="<?= $delivery['delivery_instance_id'] ?>"><?= $delivery['delivery_instance_name'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <label>Provinsi:</label>
                                    <select class="form-control kt-select2 select_provinsi" name="input2[provinsi]" id="select_provinsi" onchange="get_kota()" required>
                                        <?php foreach ($provinsis as $provinsi) { ?>
                                            <option value="<?= $provinsi['REGION_ID'] ?>"><?= $provinsi['NAME'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="">Kota/Kabupaten:</label>
                                    <select class="form-control kt-select2 select_kota" id="select_kota" onchange="get_kecamatan()" name="input2[kota]" required>

                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label>Kecamatan:</label>
                                    <select class="form-control kt-select2 select_kecamatan" id="select_kecamatan" name="input2[kecamatan]" required>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <label>Alamat Lengkap:</label>
                                    <textarea class="form-control" name="input2[full_address]"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Akun Tujuan
												</h3>  
											</div>
										</div>-->



                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                        <a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
                    </div>
                </div>



            </div>
        </div>
    </form>
</div>

<script>
    it = 0;

    function myCreateFunction() {
        it++;
        var html = '<tr id="' + it + '"><td><select class="form-control" id="exampleSelect1" name="account_id[' + it + ']" required><option value="">Pilih Akun</option><?php foreach ($m_account as $data) {
                                                                                                                                                                            echo "<option value=\'" . $data->account_id . "\'>" . $data->account_name . "</option>";
                                                                                                                                                                        } ?></select></td><td><input type="text" class="form-control" placeholder="Nama Pengirim" name="from[account_detail_transfer_name][' + it + ']"></td><td><input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="from[account_detail_date][' + it + ']" required></td><td><input type="text" class="form-control numeric paid_total" placeholder="Nominal" name="from[account_detail_credit][' + it + ']" value="0" required onkeyup="chk_total()"></td><td><input type="file" class="form-control" name="transfer_' + it + '"></td><td><button onclick="myDeleteFunction(' + it + ')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
        $('#x').append(html);
        $(".numeric").mask("#,##0", {
            reverse: true
        });
        $('.date_picker, #kt_datepicker_1_validate').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            format: "yyyy-mm-dd"
        });
    }

    function myDeleteFunction(id) {
        $('#' + id).remove();
    }

    function chk_total(val) {
        var paid_all = 0;
        var val_x = "";
        $('.paid_total').each(function() {
            val_x = $(this).val();
            val_x = val_x.replaceAll(",", "");
            if (val_x == "") val_x = 0;
            paid_all += parseFloat(val_x);
        });
        $('#paid').val(commafy(paid_all));
        var paid = $('#paid').val();
        paid = paid.replaceAll(",", "");
        var sum = 0;
        var nilai_awal = 0;
        $('.sum_total').each(function() {
            nilai_awal = $(this).val();
            if (parseFloat(nilai_awal.replaceAll(",", "")) > parseFloat($(this).attr("max"))) {
                $(this).val(commafy($(this).attr("max")));
                sum += parseFloat($(this).attr("max"));
            } else {
                sum += parseFloat(nilai_awal.replaceAll(",", ""));
            }
            //if(parseFloat(this.value) > 0)(

            //}
        });
        deposit = $('#deposit').val().replaceAll(',', '');
        $('#deposit2').val(commafy(parseFloat(deposit) + parseFloat(paid - sum)));
        if ((parseFloat(deposit) + parseFloat(paid - sum)) < 0) {
            $('#simpan').hide();
        } else {
            $('#simpan').show();
        }
    }

    function get_kota() {
        var val = $('#select_provinsi option:selected').val();

        $.ajax({
            url: '<?= base_url() ?>M_account/get_kota/' + val.substring(0, 2),
            success: function(result) {
                $('#select_kota').html(result);
            }
        });
        $('#select_kecamatan').html('');
    }

    function get_kecamatan() {
        var val = $('#select_kota option:selected').val();
        $.ajax({
            url: '<?= base_url() ?>M_account/get_kecamatan/' + val.substring(0, 4),
            success: function(result) {
                $('#select_kecamatan').html(result);
            }
        });
    }

    $('.select_provinsi').select2({
        allowClear: true,
        placeholder: 'Pilih Provinsi',
    });
    $('.select_ekspedisi').select2({
        allowClear: true,
        placeholder: 'Pilih Ekspedisi',
    });
    $('.select_kota').select2({
        allowClear: true,
        placeholder: 'Pilih Kota/kabupaten',
    });
    $('.select_kecamatan').select2({
        allowClear: true,
        placeholder: 'Pilih Kota/kecamatan',
    });
</script>