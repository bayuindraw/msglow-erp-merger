<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_action extends CI_Controller
{
	
	var $url_ = "M_action";
	var $id_ = "action_id";
	var $eng_ = "Action";
	var $ind_ = "Action";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_.'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library("upload");
	}
	
	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	
	public function tambah()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Tambah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');

			$in = array(
				'action_name' => $data['action_name'],
				'action_code' => $data['action_code'],
				'action_date_create' => date('Y-m-d H:i:s'),
				'action_user_create' => $_SESSION['user_id']
			);
			$exec = $this->Model->insert($in);

			$this->session->set_flashdata('sukses', 'Tambah data berhasil');
		}

		redirect(site_url($this->url_));
	}

	function edit($id)
	{
		$datacontent['row'] = $this->Model->get_data_where("action_id = '".$id."'")->result_array();

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Ubah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form_edit', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan_edit($id)
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');

			$in = array(
				'action_name' => $data['action_name'],
				'action_code' => $data['action_code'],
				'action_date_create' => date('Y-m-d H:i:s'),
				'action_user_create' => $_SESSION['user_id']
			);
			$where = "action_id = '$id'";

			$exec = $this->Model->update($in,$where);

			$this->session->set_flashdata('sukses', 'Ubah data berhasil');
		}

		redirect(site_url($this->url_));
	}
	
	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}
}
