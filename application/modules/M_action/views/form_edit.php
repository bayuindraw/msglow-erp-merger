<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan_edit/'.$row[0]['action_id']); ?>" enctype="multipart/form-data">
				<div class="kt-portlet__body">
					<div class="form-group">
						<label>Kode Action</label>
						<input type="text" class="form-control" placeholder="Masukan Kode Action.." name="input[action_code]" value="<?php echo $row[0]['action_code'] ?>" required="">
					</div>
					<div class="form-group">
						<label>Nama Action</label>
						<input type="text" class="form-control" placeholder="Masukan Nama Action.." name="input[action_name]" value="<?php echo $row[0]['action_name'] ?>" required="">
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>