<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH.'libraries/fpdf/html2pdf.php');

class M_department extends CI_Controller
{
	
	var $url_ = "M_department";
	var $id_ = "department_id";
	var $eng_ = "Department";
	var $ind_ = "Department";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_.'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library("upload");
	}
	
	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	
	public function tambah()
	{
		$datacontent['menu'] = $this->Model->get_table_where("menu_id, menu_name","m_menu","menu_id != ''")->result_array();
		$datacontent['i'] = 0;

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Tambah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');

			$table = "m_department";
			$in = array(
				'department_name' => $data['department_name'],
				'department_status' => 1,
				'department_user_create' => $_SESSION['user_id']
			);
			$exec = $this->Model->insert($table, $in);

			$this->session->set_flashdata('sukses', 'Tambah data berhasil');
		}

		redirect(site_url($this->url_));
	}

	function edit($id)
	{
		$datacontent['dept'] = $this->Model->get_table_where("*","m_department","department_id = '$id'")->result_array();
		$datacontent['i'] = 0;

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Ubah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form_edit', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan_edit($id)
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');

			$table = "m_department";
			$in = array(
				'department_name' => $data['department_name'],
				'department_status' => 1,
				'department_user_create' => $_SESSION['user_id']
			);
			$where = array(
				'department_id' => $id
			);
			$exec = $this->Model->update($table, $in, $where);

			$this->session->set_flashdata('sukses', 'Tambah data berhasil');
		}

		redirect(site_url($this->url_));
	}
	
	// public function hapus($id = '')
	// {		
	// 	$in = array(
	// 		'transfer_status' => 2
	// 	);
	// 	$where = array(
	// 		'transfer_id' => $id
	// 	);
	// 	$exec = $this->Model->update("T_transfer",$in,$where);
	// 	redirect($this->url_."/transfer");
	// }

	public function hapus($id)
	{
		$where = array(
			'department_id' => $id
		);
		$this->Model->delete("m_department",$where);
		
		$dtm = $this->Model->get_table_where("*","m_department_menu","department_id = '".$id."'")->result_array();
		foreach ($dtm as $d) {
			$where = array(
				'department_menu_id' => $d['department_menu_id']
			);
			$exec = $this->Model->delete("m_department_menu",$where);
			$exec = $this->Model->delete("m_department_menu_action",$where);
		}

		redirect(site_url($this->url_));
	}

	function menu($id)
	{
		$datacontent['dept'] = $this->Model->get_table_where("*","m_department","department_id = '$id'")->result_array();
		$datacontent['menu'] = $this->Model->get_table("m_menu")->result_array();
		$datacontent['i'] = 0;

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Menu Department '.$datacontent['dept'][0]['department_name'];
		$datacontent['parameter'] = "";
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form_menu', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}

	public function tampil_isi_menu()
	{
		$dataisi['url'] = $this->url_;
		$dataisi['id'] = $this->input->post('id');
		$dataisi['level'] = $this->Model->get_level($this->input->post('id'));
		$dataisi['list_header'] = $this->Model->get_table_where("*","m_department_menu","department_id = '".$this->input->post('id')."'","department_menu_no")->result_array();

		$this->load->view('M_department/table_menu', $dataisi);
	}
	
	public function simpan_menu()
	{
		$id = $this->input->post("id");
		$menu = $this->input->post("menu_header");
		$dt_menu = $this->Model->get_table_where("*","m_menu","menu_id = '".$menu."'")->result_array();

		$dt_order = $this->Model->get_last_data("department_menu_order","m_department_menu","department_id = '$id' and department_menu_parent_id = 0",1)->result_array();
		if (count($dt_order)>0) {
			$order = $dt_order[0]['department_menu_order']+1;
		} else {
			$order = 1;
		}

		if (count($dt_menu)>0) {
			$in = array(
				'department_id' => $id,
				'menu_id' => $dt_menu[0]['menu_id'],
				'department_menu_parent_id' => 0,
				'department_menu_name' => $dt_menu[0]['menu_name'],
				'department_menu_code' => $dt_menu[0]['menu_code'],
				'department_menu_icon' => $dt_menu[0]['menu_icon'],
				'department_menu_url' => $dt_menu[0]['menu_url'],
				'department_menu_notification' => $dt_menu[0]['menu_notification'],
				'department_menu_notification_source' => $dt_menu[0]['menu_notification_source'],
				'department_menu_order' => $order,
				'department_menu_no' => $order,
				'department_menu_level' => 1,
				'view_id' => $dt_menu[0]['view_id'],
				'icon_id' => $dt_menu[0]['icon_id'],
				'department_menu_date_create' => date('Y-m-d H:i:s'),
				'department_menu_user_create' => $_SESSION['user_id']
			);
			$exec = $this->Model->insert("m_department_menu",$in);
			$department_menu_id = $this->db->insert_id();

			$dt_action = $this->Model->get_table_where("*","m_menu_action","menu_id = '".$dt_menu[0]['menu_id']."'")->result_array();
			foreach ($dt_action as $da) {
				$in = array(
					'department_menu_id' => $department_menu_id,
					'action_id' => $da['action_id'],
					'department_menu_action_date_create' => date('Y-m-d H:i:s'),
					'department_menu_action_user_create' => $_SESSION['user_id']
				);
				$exec = $this->Model->insert("m_department_menu_action",$in);
			}

		} else {
			$in = array(
				'department_id' => $id,
				'department_menu_parent_id' => 0,
				'department_menu_name' => $menu,
				'department_menu_order' => $order,
				'department_menu_no' => $order,
				'department_menu_level' => 1,
				'department_menu_date_create' => date('Y-m-d H:i:s'),
				'department_menu_user_create' => $_SESSION['user_id']
			);
			$exec = $this->Model->insert("m_department_menu",$in);
		}

		$this->session->set_flashdata('sukses', 'Simpan data berhasil');

		echo json_encode(array('status'=>true, 'message'=>'OK'));
	}
	
	public function simpan_sub_menu()
	{
		$department_id = $this->input->post("department_id");
		$parent_id = $this->input->post("parent_id");
		$menu = $this->input->post("menu_sub");
		$dt_menu = $this->Model->get_table_where("*","m_menu","menu_id = '".$menu."'")->result_array();
		
		$dt_order = $this->Model->get_last_data("department_menu_order","m_department_menu","department_id = '$department_id' and department_menu_parent_id = '$parent_id'",1)->result_array();
		if (count($dt_order)>0) {
			$order = $dt_order[0]['department_menu_order']+1;
		} else {
			$order = 1;
		}
		
		$dt_parent = $this->Model->get_last_data("department_menu_level, department_menu_no","m_department_menu","department_id = '$department_id' and department_menu_id = '$parent_id'",1)->result_array();
		if (count($dt_parent)>0) {
			$level = $dt_parent[0]['department_menu_level']+1;
			$no = $dt_parent[0]['department_menu_no'].'.'.$order;
		} else {
			$level = 1;
			$no = $order;
		}

		if (count($dt_menu)>0) {
			$in = array(
				'department_id' => $department_id,
				'menu_id' => $dt_menu[0]['menu_id'],
				'department_menu_parent_id' => $parent_id,
				'department_menu_name' => $dt_menu[0]['menu_name'],
				'department_menu_code' => $dt_menu[0]['menu_code'],
				'department_menu_icon' => $dt_menu[0]['menu_icon'],
				'department_menu_url' => $dt_menu[0]['menu_url'],
				'department_menu_notification' => $dt_menu[0]['menu_notification'],
				'department_menu_notification_source' => $dt_menu[0]['menu_notification_source'],
				'department_menu_order' => $order,
				'department_menu_level' => $level,
				'department_menu_no' => $no,
				'view_id' => $dt_menu[0]['view_id'],
				'icon_id' => $dt_menu[0]['icon_id'],
				'department_menu_date_create' => date('Y-m-d H:i:s'),
				'department_menu_user_create' => $_SESSION['user_id']
			);
			$exec = $this->Model->insert("m_department_menu",$in);
			$department_menu_id = $this->db->insert_id();

			$dt_action = $this->Model->get_table_where("*","m_menu_action","menu_id = '".$dt_menu[0]['menu_id']."'")->result_array();
			foreach ($dt_action as $da) {
				$in = array(
					'department_menu_id' => $department_menu_id,
					'action_id' => $da['action_id'],
					'department_menu_action_date_create' => date('Y-m-d H:i:s'),
					'department_menu_action_user_create' => $_SESSION['user_id']
				);
				$exec = $this->Model->insert("m_department_menu_action",$in);
			}
		} else {
			$in = array(
				'department_id' => $department_id,
				'department_menu_parent_id' => $parent_id,
				'department_menu_name' => $menu,
				'department_menu_order' => $order,
				'department_menu_level' => $level,
				'department_menu_no' => $no,
				'department_menu_date_create' => date('Y-m-d H:i:s'),
				'department_menu_user_create' => $_SESSION['user_id']
			);
			$exec = $this->Model->insert("m_department_menu",$in);
		}

		$this->session->set_flashdata('sukses', 'Simpan data berhasil');

		echo json_encode(array('status'=>true, 'message'=>'OK'));
	}

	public function hapus_menu()
	{
		$id = $this->input->post('id');
		$dt = $this->Model->get_table_where("department_id, department_menu_no","m_department_menu","department_menu_id = '$id'")->result_array();

		$dtm = $this->Model->get_table_where("*","m_department_menu","department_menu_no LIKE '".$dt[0]['department_menu_no']."%'")->result_array();
		foreach ($dtm as $d) {
			$where = array(
				'department_menu_id' => $d['department_menu_id']
			);
			$exec = $this->Model->delete("m_department_menu",$where);
			$exec = $this->Model->delete("m_department_menu_action",$where);
		}

		echo json_encode(array('status'=>true, 'message'=>$dt[0]['department_id']));
	}
}
