<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_departmentModel extends CI_Model
{
	var $table_ = "m_department";
	var $id_ = "department_id";
	var $eng_ = "department";
	var $url_ = "M_department";
	
	function get()
	{
		$data = $this->db->get($this->table_);
		return $data;
	}

	function get_data()
	{
		$table = $this->table_." a";
		$id = $this->id_;
		$field = array('department_name');
		$url = $this->url_;

		$action = '<a href="'.site_url($url."/menu/xid").'" class="btn btn-default"> <i class="fa fa-bars"></i> Menu</a> <a href="'.site_url($url."/edit/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . '/hapus/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';

		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);
		
		echo json_encode($data);
	}
	
	function get_level($department_id)
	{
		$query = "SELECT department_menu_level as level
		FROM m_department_menu a 
		WHERE department_id = '$department_id' and department_menu_level!=1
		GROUP BY department_menu_level
		ORDER BY department_menu_level ASC";
		$data = $this->db->query($query)->result_array();
		return $data;
	}
	
	function get_list_data($department_id, $parent_id)
	{
		// $query = "SELECT * FROM m_department_menu WHERE department_id = '$department_id' and department_menu_parent_id != 0 ORDER BY department_menu_order ASC";
		// $query = "SELECT *
		// FROM m_department_menu a 
		// WHERE department_id = '$department_id' and department_menu_parent_id != 0 and (department_menu_parent_id = '$parent_id' OR department_menu_parent_id IN 
		// (SELECT a.department_menu_id
		// FROM m_department_menu a 
		// WHERE department_id = '$department_id' and department_menu_id = '$parent_id')
		// )
		// ORDER BY department_menu_order ASC";
		$query = "SELECT *
		FROM m_department_menu a 
		WHERE department_id = '$department_id'
		ORDER BY department_menu_no ASC";
		$data = $this->db->query($query);
		return $data;
	}
	
	function get_action($id)
	{
		$query = "SELECT b.action_name
		FROM m_department_menu_action a 
		JOIN m_action b ON a.action_id = b.action_id
		WHERE a.department_menu_id = '$id'
		ORDER BY a.action_id ASC";
		$data = $this->db->query($query)->result_array();
		return $data;
	}

	function get_last_data($select, $table, $where, $limit)
	{
		$query = "SELECT $select FROM $table WHERE $where ORDER BY $select DESC LIMIT $limit";
		$data = $this->db->query($query);
		return $data;
	}
	
	function get_table_where($select, $table, $where, $order_by = '')
	{
		$this->db->select($select);
		$this->db->where($where);
		$this->db->order_by($order_by,"ASC");
		$data = $this->db->get($table);
		return $data;
	}
	
	function get_table($table)
	{
		$data = $this->db->get($table);
		return $data;
	}
	
	function insert($table, $data = array())
	{
		$this->db->insert($table, $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function update($table, $data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($table, $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function delete($table, $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($table);
		$info = '<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function delete2($table, $where)
	{
		$this->db->where($where);
		$this->db->delete($table);
		$info = '<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
}
