<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Menu header</label>
							<select name="menu_header" id="menu_header" class="PilihMenu form-control md-static" required>
								<?php foreach ($menu as $d) { ?>
								<option value="<?php echo $d['menu_id'] ?>"><?php echo $d['menu_name'] ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-8">
						<button type="button" name="simpan" value="simpan" class="btn btn-primary" style="margin-top: 25px;" onclick="simpan_menu('<?php echo $id ?>')">Tambah Menu</button>
					</div>

					<div class="col-md-12" style="margin-top: 20px;">
						<div class="tampil_isi"></div>
					</div>


				</div>
			</div>

		</div>
	</div>
</div>

<div id="subModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Sub Menu</h4>
				<button type="button" class="close" data-dismiss="modal"></button>
			</div>

			<div class="modal-body">

				<div class="form-group">
					<input type="hidden" name="parent_id" id="parent_id" value="">
					<input type="hidden" name="department_id" id="department_id" value="">
					<select name="menu_sub" id="menu_sub" class="PilihMenu form-control md-static" required>
						<?php foreach ($menu as $d) { ?>
						<option value="<?php echo $d['menu_id'] ?>"><?php echo $d['menu_name'] ?></option>
						<?php } ?>
					</select>
				</div>

			</div>

			<div class="modal-footer">
				<button onclick="simpan_sub_menu()" class="btn btn-primary" data-dismiss="modal">Simpan</button>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.PilihMenu').select2({
			allowClear: true,
			placeholder: 'Pilih Menu',
			tags: true
		});

		vw_isi_menu('<?php echo $id ?>');

		$('#subModal').on('show.bs.modal', function(e){
			var id = $(e.relatedTarget).data('id');
			var dept_id = $(e.relatedTarget).data('dept_id');
			document.getElementById("parent_id").value = id;
			document.getElementById("department_id").value = dept_id;
		});
	});

	function vw_isi_menu(id) {
		$.ajax({
			type:'post',
			data:{
				"id":id
			},
			url: '<?php echo site_url('M_department/tampil_isi_menu') ?>',
			success: function(data){
				$('.tampil_isi').html(data);
			}
		});
	}

	function simpan_menu(id) {
		var menu = document.getElementById('menu_header').value;
		$.ajax({
			type:'post',
			data:{
				'id':id,
				'menu_header':menu
			},
			url: '<?php echo site_url('M_department/simpan_menu') ?>',
			success: function(data){
				var json = $.parseJSON(data);
				if (json.status) {
					vw_isi_menu(id);
				}
			}
		});
	}

	function simpan_sub_menu() {
		var parent_id = document.getElementById('parent_id').value;
		var department_id = document.getElementById('department_id').value;
		var menu_sub = document.getElementById('menu_sub').value;
		$.ajax({
			type:'post',
			data:{
				'parent_id':parent_id,
				'department_id':department_id,
				'menu_sub':menu_sub
			},
			url: '<?php echo site_url('M_department/simpan_sub_menu') ?>',
			success: function(data){
				var json = $.parseJSON(data);
				if (json.status) {
					vw_isi_menu(department_id);
				} else {
					alert(json.message);
				}
			}
		});
	}

	var it = 0;
	function myCreateFunction() {
		it++;
		var html = '<tr id="'+it+'">'+
		'<td><select name="input2['+it+'][menu_id]" class="PilihMenu form-control md-static" required><option></option><?php foreach ($menu as $d) { ?><option value="<?php echo $d['menu_id'] ?>"><?php echo $d['menu_name'] ?></option><?php } ?></select></td>'+
		'<td><center><button onclick="myDeleteFunction('+ it +');" class="btn btn-success"><i class="fas fa-plus"></i></button> <button onclick="myDeleteFunction('+ it +');" class="btn btn-danger"><i class="fas fa-trash"></i></button></center></td>'+
		'<tr>';

		$('#x').append(html);
		$('.PilihMenu').select2({
			allowClear: true,
			placeholder: 'Pilih Menu',
		});
	}

	function myDeleteFunction(id) {
		$('#' + id).remove();
	}

	function hapus_menu(id) {
		$.ajax({
			type:'post',
			url: '<?php echo site_url('m_department/hapus_menu') ?>',
			data: {
				"id":id
			},
			success: function(data){
				var json = $.parseJSON(data);
				if (json.status) {
					vw_isi_menu(json.message);
				} else {
					alert(json.message);
				}
			}
		});
	}
</script>