<table class="table table-striped table-bordered nowrap" id="kt_table_1">
	<thead>
		<tr>
			<th>Menu</th>
			<?php for ($i=0; $i < count($level); $i++) { ?>
			<th>Sub Menu Lv. <?php echo ($level[$i]['level']-1); ?></th>
			<?php } ?>
			<th>Action</th>
			<th style="width: 20%;">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($list_header as $d) { ?>
		<tr>
			<?php if($d['department_menu_parent_id']==0){ ?>
			<td><span><?php echo $d['department_menu_name'] ?></span></td>
			<?php } else { ?>
			<td></td>
			<?php } ?>

			<?php for ($i=0; $i < count($level); $i++) { ?>
			<td><span><?php if($d['department_menu_level']==$level[$i]['level']) echo $d['department_menu_name']; ?></span></td>
			<?php } ?>

			<td>
				<?php $action = $this->Model->get_action($d['department_menu_id']) ?>
				<?php foreach ($action as $da) { ?>
				<!-- <div class="checkbox">
					<label><input type="checkbox" value="<?php echo $da['action_id'] ?>" id="cb_<?php echo $da['action_id'] ?>" <?php if($da['action_id']==$da['act_id']) echo 'checked'; ?> onchange="cek('<?php echo $d['action_id'] ?>','<?php echo $id ?>')"> <?php echo $d['action_name'] ?></label>
				</div> -->
				<?php echo '- '.$da['action_name'].'<br>'; ?>
				<?php } ?>
			</td>

			<td>
				<center>
					<a href="#subModal" class="btn btn-success" data-toggle="modal" data-id="<?php echo $d['department_menu_id'] ?>" data-dept_id="<?php echo $d['department_id'] ?>"><i class="fas fa-plus"></i>  SUB MENU</a>
					<button onclick="hapus_menu('<?php echo $d['department_menu_id'] ?>')" class="btn btn-danger"><i class="fas fa-trash"></i></button>
				</center>
			</td>
		</tr>

		<?php } ?>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",
			"ordering": false,
			scrollY: "300px",
			scrollX: true,
			scrollCollapse: true,
			"processing": true,
			"serverSide": false
		});
	});
</script>