<?php
$name = "";
$email = "";
$remember_token = "";
$password = "";
$role_id = "";
if ($parameter == 'ubah' && $id != '') {
	$this->db->where('employee_id', $id);
	$row = $this->Model->get()->row_array();
}
?>
<div class="row">
								<div class="col-lg-12 col-xl-6">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Nama</label>
													<input type="text" class="form-control" placeholder="Nama" name="input[employee_name]" value="<?= @$row['employee_name'] ?>" required>
												</div>
												<div class="form-group">
													<label>Alamat</label>
													<textarea class="summernote" name="input[employee_address]"><?= @$row['employee_address'] ?></textarea>
												</div>
												<div class="form-group">
													<label>Tanggal Lahir</label>
													<input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="input[employee_birth_date]" value="<?= @$row['employee_birth_date'] ?>">
												</div>
												<div class="form-group">
													<label>Telephone/HP</label>
													<input type="text" class="form-control" placeholder="Telephone/HP" name="input[employee_phone]" value="<?= @$row['employee_phone'] ?>" required>
												</div>
												<div class="form-group">
													<label>Email</label>
													<input type="text" class="form-control" placeholder="Email" name="input[employee_email]" value="<?= @$row['employee_email'] ?>" required>
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>