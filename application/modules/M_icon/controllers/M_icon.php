<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_icon extends CI_Controller
{
	
	var $url_ = "M_icon";
	var $id_ = "icon_id";
	var $eng_ = "Icon";
	var $ind_ = "Icon";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_.'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library("upload");
	}
	
	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	
	public function tambah()
	{
		$datacontent['dt_header_icon'] = $this->Model->get_data_header()->result_array();

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Tambah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');

			if(empty($_FILES['gambar']['name'])){

				$dt_header = $this->Model->get_data_header_where("m_icon_header = '".$data['icon_header_id']."'")->result_array();
				if (count($dt_header)>0) {

					$in = array(
						'icon_header_id' => $data['icon_header_id'],
						'icon_css' => $data['icon_css'],
						'icon_full_css' => $dt_header[0]['m_icon_header_css'].' '.$data['icon_css'],
						'icon_name' => $data['icon_name'],
						'icon_image' => ''
					);
					$exec = $this->Model->insert($in);
				}

			} else {
				$config['upload_path'] = './upload/icon/';
				$config['allowed_types'] = 'png';
				$config['max_width']  = '2000';
				$config['encrypt_name'] = true;

				$this->upload->initialize($config);

				if ( ! $this->upload->do_upload("gambar")){
					$error = $this->upload->display_errors();

					if(strlen($error)==89||strlen($error)==79){
						$error2 = "Size file gambar lebih dari 2MB";
					} elseif(strlen($error)==86){
						$error2 = "Lebar / tinggi file gambar melebihi ketentuan maksimum.";
					} elseif(strlen($error)==64){
						$error2 = "Type file bukan .jpg / .jpeg";
					}else {
						$error2 = $error;
					}

					$this->session->set_flashdata('gagal', $error2);
				} else{
					if(file_exists('./upload/icon/'.$nama_gambar)){
						unlink('./upload/icon/'.$nama_gambar);
					}
					$hasil_gambar = $this->upload->data('file_name');
					$in = array(
						'icon_header_id' => 4,
						'icon_css' => '',
						'icon_full_css' => '',
						'icon_name' => $data['icon_name'],
						'icon_image' => $hasil_gambar
					);
					$exec = $this->Model->insert($in);

					$this->session->set_flashdata('sukses', 'Tambah data berhasil');
				}
			}
		}

		redirect(site_url($this->url_));
	}
	
	public function simpan_edit()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');

			if ($this->input->post('jenis')==1) {

				$dt_header = $this->Model->get_data_header_where("m_icon_header = '".$data['icon_header_id']."'")->result_array();
				if (count($dt_header)>0) {
					$du = array(
						'icon_header_id' => $data['icon_header_id'],
						'icon_css' => $data['icon_css'],
						'icon_full_css' => $dt_header[0]['m_icon_header_css'].' '.$data['icon_css'],
						'icon_name' => $data['icon_name'],
						'icon_image' => ''
					);
					$exec = $this->Model->update($du,array("icon_id" => $this->input->post('id')));
				}

			} else {
				if(empty($_FILES['gambar']['name'])){

					$du = array(
						'icon_name' => $data['icon_name']
					);
					$exec = $this->Model->update($du,array("icon_id" => $this->input->post('id')));

				} else {

					$config['upload_path'] = './upload/icon/';
					$config['allowed_types'] = 'png';
					$config['max_width']  = '2000';
					$config['encrypt_name'] = true;

					$this->upload->initialize($config);

					if ( ! $this->upload->do_upload("gambar")){
						$error = $this->upload->display_errors();

						if(strlen($error)==89||strlen($error)==79){
							$error2 = "Size file gambar lebih dari 2MB";
						} elseif(strlen($error)==86){
							$error2 = "Lebar / tinggi file gambar melebihi ketentuan maksimum.";
						} elseif(strlen($error)==64){
							$error2 = "Type file bukan .jpg / .jpeg";
						}else {
							$error2 = $error;
						}

						$this->session->set_flashdata('gagal', $error2);
					} else{
						if(file_exists('./upload/icon/'.$nama_gambar)){
							unlink('./upload/icon/'.$nama_gambar);
						}
						$hasil_gambar = $this->upload->data('file_name');

						$du = array(
							'icon_name' => $data['icon_name'],
							'icon_image' => $hasil_gambar
						);
						$exec = $this->Model->update($du,array("icon_id" => $this->input->post('id')));

						$this->session->set_flashdata('sukses', 'Tambah data berhasil');
					}
				}
				
			}
		}

		redirect(site_url($this->url_));
	}

	function edit($id)
	{
		$datacontent['dt_header_icon'] = $this->Model->get_data_header()->result_array();

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Ubah Data '.$this->ind_;

		$datacontent['id'] = $id;
		$datacontent['row'] = $this->db->get_where('m_icon', ['icon_id' => $id])->result_array();
		$datacontent['jenis'] = 1;
		if ($datacontent['row'][0]['icon_image']!="") {
			$datacontent['jenis'] = 2;
		}

		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form_edit', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function hapus($id = '')
	{
		$dt = $this->Model->get_data_where("icon_id = '$id'")->result_array();
		if (count($dt)>0) {
			if(file_exists('./upload/icon/'.$dt[0]['icon_image'])){
				unlink('./upload/icon/'.$dt[0]['icon_image']);
			}
		}
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}
}
