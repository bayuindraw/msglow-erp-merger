<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__body">
					<div class="form-group">
						<label>Jenis</label>
						<div class="kt-radio-list">
							<label class="kt-radio">
								<input type="radio" name="jenis" value="1" id="jenis" onchange="chk_jenis(this.value)" checked=""> Icon
								<span></span>
							</label>
							<label class="kt-radio">
								<input type="radio" name="jenis" value="2" id="jenis" onchange="chk_jenis(this.value)"> Gambar
								<span></span>
							</label>
						</div>
					</div>

					<div class="form-group" id="vw_header">
						<label>Header</label>
						<select class="form-control" name="input[icon_header_id]">
							<?php foreach ($dt_header_icon as $d) { ?>
							<option value="<?php echo $d['m_icon_header'] ?>"><?php echo $d['m_icon_header_name'] ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group" id="vw_icon">
						<label>Icon CSS</label>
						<input type="text" class="form-control" placeholder="Masukan Icon CSS.." name="input[icon_css]" value="<?= @$row['icon_css'] ?>">
					</div>
					<div class="form-group">
						<label>Icon Name</label>
						<input type="text" class="form-control" placeholder="Masukan Nama Icon.." name="input[icon_name]" value="<?= @$row['icon_name'] ?>" required>
					</div>
					<div class="form-group" id="vw_upload">
						<label>Icon Image <span style="font-size: 8pt;">*Opsional</span></label>
						<input type="file" name="gambar" class="form-control">
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>


		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var jenis = 1;
		chk_jenis(jenis);
	});

	function chk_jenis(jns) {
		if (jns==1) {
			$("#vw_header").show();
			$("#vw_icon").show();
			$("#vw_upload").hide();
		} else {
			$("#vw_header").hide();
			$("#vw_icon").hide();
			$("#vw_upload").show();
		}
	}
</script>