<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan_edit'); ?>" enctype="multipart/form-data">
				<?= input_hidden('id', $id) ?>
				<?= input_hidden('jenis', $jenis) ?>
				<div class="kt-portlet__body">
					<div class="form-group">
						<label>Jenis</label><br>
						<span>
							<?php if($jenis==1) echo 'Icon'; else echo 'Gambar'; ?>
						</span>
					</div>

					<div class="form-group" id="vw_header">
						<label>Header</label>
						<select class="form-control" name="input[icon_header_id]">
							<?php foreach ($dt_header_icon as $d) { ?>
							<option value="<?php echo $d['m_icon_header'] ?>"><?php echo $d['m_icon_header_name'] ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group" id="vw_icon">
						<label>Icon CSS</label>
						<input type="text" class="form-control" placeholder="Masukan Icon CSS.." name="input[icon_css]" value="<?= @$row[0]['icon_css'] ?>">
					</div>
					<div class="form-group">
						<label>Icon Name</label>
						<input type="text" class="form-control" placeholder="Masukan Nama Icon.." name="input[icon_name]" value="<?= @$row[0]['icon_name'] ?>" required>
					</div>
					<div class="form-group" id="vw_upload">
						<label>Icon Image <span style="font-size: 8pt;">*Opsional</span></label><br>
						<img src="<?php echo base_url().'/upload/icon/'.$row[0]['icon_image'] ?>" style="max-height: 50px;"><br><br>
						<input type="file" name="gambar" class="form-control">
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>


		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var jenis = '<?php echo $jenis ?>';
		chk_jenis(jenis);
	});
	function chk_jenis(jns) {
		if (jns==1) {
			$("#vw_header").show();
			$("#vw_icon").show();
			$("#vw_upload").hide();
		} else {
			$("#vw_header").hide();
			$("#vw_icon").hide();
			$("#vw_upload").show();
		}
	}
</script>