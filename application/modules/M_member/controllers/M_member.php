<?php
defined('BASEPATH') or exit('No direct script access allowed');


class M_member extends CI_Controller
{

	var $url_ = "M_member";
	var $id_ = "member_id";
	var $eng_ = "member";
	var $ind_ = "Member";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function report()
	{
		if (@$_POST['export_pdf']) {
			$this->export_pdf();
		} else {
			$datacontent['url'] = $this->url_;
			$datacontent['search'] = $this->input->post('search');
			$datacontent['url2'] = '/' . $datacontent['search']['member_status'] . '|' . $datacontent['search']['all'];
			$datacontent['title'] = 'Member';
			$data['file'] = $this->ind_;
			$data['content'] = $this->load->view($this->url_ . '/table_report', $datacontent, TRUE);
			$data['title'] = $datacontent['title'];
			$this->load->view('Layout/home', $data);
		}
	}

	public function export_pdf()
	{
		ob_start();
		$datacontent['data'] = $this->Model->get_report()->result_array();
		$this->load->view('M_member/export_pdf', $datacontent);
		//$data['content'] = $this->load->view('m_member/table_report', $datacontent);
		//$data['siswa'] = $this->siswa_model->view_row();
		//$this->load->view('print', $data);
		$html = ob_get_contents();
		ob_end_clean();

		require './assets/html2pdf/autoload.php';

		$pdf = new Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
		$pdf->WriteHTML($html);
		$pdf->Output(date('Y-m-d') . '.pdf', 'D');
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function get_data_report($search = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_report($search);
	}

	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}

	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}

	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}

	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['provinsi'] = $this->Model->get_list_provinsi();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {

				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_credit'] = $from['account_detail_debit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_credit'], 'credit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_send()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function form_detail($parameter = '', $id = '', $id_detail = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account_detail_category'] = $this->Model->get_list_account_detail_category();
		$datacontent['id'] = $id;
		$datacontent['id_detail'] = $id_detail;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$files = $_FILES['upd'];
				$i = 1;

				foreach ($files['name'] as $key => $val) {

					$_FILES['upd'] = array(
						'name' => $files['name'][$key],
						'type' => $files['type'][$key],
						'tmp_name' => $files['tmp_name'][$key],
						'error' => $files['error'][$key],
						'size' => $files['size'][$key]
					);
					if ($key == 'TRANSFER2') {
						$key = 'TRANSFER';
						$np = '2';
					} else if ($key == 'TRANSFER') {
						$np = '1';
					} else {
						$np = '';
					}
					$dir_r = "./upload/$key";
					$dir = $dir_r . '/';
					$config['upload_path'] = $dir;
					//$config['allowed_types'] = $allowed; 
					$config['remove_spaces'] = TRUE;
					$config['allowed_types'] = '*';
					$config['max_size'] = '100000000000';
					$config['overwrite'] = TRUE;
					$this->load->library('upload', $config);
					$ar = explode(".", $_FILES["upd"]['name']);
					$ext = $ar[count($ar) - 1];
					$ext = strtolower($ext);
					$ext = trim($ext);
					if ($ext != "") {
						$config['file_name'] = date('YmdHis') . str_replace(' ', '_', strtolower($data['member_name'])) . $np;
						$this->upload->initialize($config);
						$this->upload->display_errors('', '');
						if (!$this->upload->do_upload("upd")) {
							$error = array('error' => $this->upload->display_errors());
							if ($error['error'] == "<p>The file you are attempting to upload is larger than the permitted size.</p>") {
								$ret = "msg#error#Simpan Foto Gagal dikarenakan melebihi ketentuan";
							} else {
								$ret = "msg#error#Simpan Foto Gagal, " . $error['error'];
							}
						} else {
							$i++;
							$up = $this->upload->data();
							$data['member_' . strtolower($key) . (($np == 2) ? '2' : '')] = $config['file_name'] . "." . $ext;
						}
					}
				}
				if (@$data['member_code'] != "") $data['member_date_approve'] = date('Y-m-d H:i:s');
				$data['member_date_update'] = date('Y-m-d H:i:s');
				$data['member_date'] = date('Y-m-d');
				$exec = $this->Model->insert($data);
			} else {
				$arrdata = $this->db->query("SELECT * FROM m_member WHERE member_id  = '$_POST[id]'")->row_array();
				$files = $_FILES['upd'];
				$i = 1;
				foreach ($files['name'] as $key => $val) {
					if ($val != "") {
						$keyawal = $key;
						$_FILES['upd'] = array(
							'name' => $files['name'][$key],
							'type' => $files['type'][$key],
							'tmp_name' => $files['tmp_name'][$key],
							'error' => $files['error'][$key],
							'size' => $files['size'][$key]
						);
						if ($key == 'TRANSFER2') {
							$key = 'TRANSFER';
							$np = '2';
						} else if ($key == 'TRANSFER') {
							$np = '1';
						} else {
							$np = '';
						}
						unlink("./upload/" . $key . "/" . $_POST[$keyawal]);
						$dir_r = "./upload/$key";
						$dir = $dir_r . '/';
						$config['upload_path'] = $dir;
						//$config['allowed_types'] = $allowed; 
						$config['remove_spaces'] = TRUE;
						$config['allowed_types'] = '*';
						$config['max_size'] = '100000000000';
						$config['overwrite'] = TRUE;
						$this->load->library('upload', $config);
						$ar = explode(".", $_FILES["upd"]['name']);
						$ext = $ar[count($ar) - 1];
						$ext = strtolower($ext);
						$ext = trim($ext);
						if ($ext != "") {
							$config['file_name'] = date('YmdHis') . str_replace(' ', '_', strtolower($data['member_name'])) . $np;
							$this->upload->initialize($config);
							$this->upload->display_errors('', '');
							if (!$this->upload->do_upload("upd")) {
								$error = array('error' => $this->upload->display_errors());
								if ($error['error'] == "<p>The file you are attempting to upload is larger than the permitted size.</p>") {
									$ret = "msg#error#Simpan Foto Gagal dikarenakan melebihi ketentuan";
								} else {
									$ret = "msg#error#Simpan Foto Gagal, " . $error['error'];
								}
							} else {
								$i++;
								$up = $this->upload->data();
								$data['member_' . strtolower($key) . (($np == 2) ? '2' : '')] = $config['file_name'] . "." . $ext;
							}
						}
					} else {
					}
				}
				foreach ($data as $key => $value) {
					if ($value != $arrdata[$key]) $data['member_date_update'] = date('Y-m-d H:i:s');
					if ($value != $arrdata[$key] && $key == "member_status_id") $data['member_date_membership'] = date('Y-m-d H:i:s');
					if ($arrdata['member_code'] == "" && $value != "" && $key == "member_code") $data['member_date_approve'] = date('Y-m-d H:i:s');
				}
				$this->Model->update($data, ['member_id' => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		if (@$_SESSION['logged_in']) {
			if ($data['member_code'] != "") {
				redirect(site_url('M_member/report'));
			} else {
				redirect(site_url('M_member'));
			}
		} else {
			$this->session->set_flashdata('msg', 'Data Berhasil diubah.');
			redirect(site_url('M_member/form/tambah'));
		}
	}

	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}

	public function simpan_detail()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$id = $this->input->post('id');
				$id2 = $this->Model->get_max_id();
				$data[$this->id_] = $id;
				$data[$this->id2_] = $id2;
				$data[$this->eng2_ . '_type_id'] = 2;
				$data[$this->eng2_ . '_user_id'] = $_SESSION['user_id'];
				if ($_POST['realisasi'] == "on") {
					$data[$this->eng2_ . '_realization'] = 1;
				} else {
					$data[$this->eng2_ . '_realization'] = 0;
				}
				if ($_POST['transaction_type'] == "debit") {
					$type = "debit";
					$data[$this->eng2_ . '_debit'] = $_POST['transaction_amount'];
				} else if ($_POST['transaction_type'] == "credit") {
					$type = "credit";
					$data[$this->eng2_ . '_credit'] = $_POST['transaction_amount'];
				}
				$data[$this->eng2_ . '_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($data);
				$this->db->where('account_id', $id);
				$row = $this->Model->get()->row_array();
				if (@$row['account_date_reset'] > $data[$this->eng2_ . '_date']) {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_ . '_date']);
				} else {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
				}
			} else {
				$id = $this->input->post('id');
				$data[$this->eng2_ . '_user_id'] = $_SESSION['user_id'];
				if ($_POST['realisasi'] == "on") {
					$data[$this->eng2_ . '_realization'] = 1;
				} else {
					$data[$this->eng2_ . '_realization'] = 0;
				}
				if ($_POST['transaction_type'] == "debit") {
					$type = "debit";
					$data[$this->eng2_ . '_debit'] = $_POST['transaction_amount'];
					$data[$this->eng2_ . '_credit'] = 0;
				} else if ($_POST['transaction_type'] == "credit") {
					$type = "credit";
					$data[$this->eng2_ . '_credit'] = $_POST['transaction_amount'];
					$data[$this->eng2_ . '_debit'] = 0;
				}
				$data[$this->eng2_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update_detail($data, [$this->id_ => $this->input->post('id'), $this->id2_ => $this->input->post('id_detail')]);
				$this->db->where('account_id', $id);
				$row = $this->Model->get()->row_array();
				if (@$row['account_date_reset'] > $this->input->post('transaction_date_last')) {
					$this->Model->update_balance($id,  -1 * ($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'), @$row['account_type_id'], $this->input->post('transaction_date_last'));
				} else {
					$this->Model->update_balance($id,  -1 * ($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'));
				}
				if (@$row['account_date_reset'] > $data[$this->eng2_ . '_date']) {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_ . '_date']);
				} else {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url2_ . "/" . $id));
	}
	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();


		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();

		if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
		} else {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
		}



		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_ . "/$id"));
	}

	public function list_kecamatan()
	{
		$id_kota = $this->input->post('id_kota');
		$stasiun = $this->Model->get_list_kecamatan($id_kota);
		$lists = "<option value=''>Pilih Kecamatan</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kecamatan . "'>" . $data->nama_kecamatan . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function list_kota()
	{
		$id_provinsi = $this->input->post('id_provinsi');
		$stasiun = $this->Model->get_list_kota($id_provinsi);
		$lists = "<option value=''>Pilih Kota</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kota . "'>" . $data->nama_kota . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}
}
