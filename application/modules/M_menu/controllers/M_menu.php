<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_menu extends CI_Controller
{
	
	var $url_ = "M_menu";
	var $id_ = "menu_id";
	var $eng_ = "Menu";
	var $ind_ = "Menu";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_.'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library("upload");
	}
	
	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	
	public function tambah()
	{
		$datacontent['dt_view'] = $this->Model->get_data_view()->result_array();
		$datacontent['dt_header'] = $this->Model->get_data_header()->result_array();
		$datacontent['action'] = $this->Model->get_action()->result_array();

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Tambah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}

	public function open_icon_modal()
	{
		$header = $this->input->post('header');
		$key = $this->input->post('key');

		$data['dt_icon'] = $this->Model->get_data_icon($header,$key)->result_array();
		$this->load->view('M_menu/isi_icon_modal', $data);
	}
	
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');

			$where = "icon_id = '".$data['icon_id']."'";
			$data_icon = $this->Model->get_data_icon_where($where)->result_array();

			$table = "m_menu";
			$kolom = "menu_code";
			$tgl = date('ymd');
			$sql = $this->db->query("SELECT * FROM $table WHERE SUBSTRING($kolom, 2, 6) = '$tgl' order by $kolom DESC limit 1")->result_array();
			if(count($sql)<1){
				$id = 'M'.date('ymd').'0001';
			} else {
				$temp = (int)substr($sql[0][$kolom], 7);
				$no = $temp+1;
				if($no<=9 && $no>1){
					$id = 'M'.date('ymd').'000'.$no;
				} elseif($no<=99 && $no>9){
					$id = 'M'.date('ymd').'00'.$no;
				} elseif($no<=999 && $no>99){
					$id = 'M'.date('ymd').'0'.$no;
				} else {
					$id = 'M'.date('ymd').''.$no;
				}
			}

			if ($data['view_id']!="") {
				$where = "view_id = '".$data['view_id']."'";
				$data_view = $this->Model->get_data_view_where($where)->result_array();

				$in = array(
					'menu_name' => $data['menu_name'],
					'menu_code' => $id,
					'menu_url' => $data['menu_url'],
					'icon_id' => $data['icon_id'],
					'menu_icon' => $data_icon[0]['icon_full_css'],
					'menu_image' => $data_icon[0]['icon_image'],
					'menu_notification' => 1,
					'view_id' => $data['view_id'],
					'menu_notification_source' => $data_view[0]['view_query'],
					'menu_date_create' => date('Y-m-d H:i:s'),
					'menu_user_create' => $_SESSION['user_id']
				);
			} else {	
				$in = array(
					'icon_id' => $data['icon_id'],
					'menu_name' => $data['menu_name'],
					'menu_code' => $id,
					'menu_url' => $data['menu_url'],
					'menu_icon' => $data_icon[0]['icon_full_css'],
					'menu_image' => $data_icon[0]['icon_image'],
					'menu_notification' => 0,
					'menu_date_create' => date('Y-m-d H:i:s'),
					'menu_user_create' => $_SESSION['user_id']
				);
			}

			$exec = $this->Model->insert($in);
			$menu_id = $this->db->insert_id();

			foreach ($this->input->post('action') as $act) {
				$data_act = ([
					'menu_id' =>  $menu_id,
					'action_id' => $act,
					'menu_action_user_create' => $_SESSION['user_id']
				]);
				$this->db->insert('m_menu_action', $data_act);
			}

			$this->session->set_flashdata('sukses', 'Tambah data berhasil');
		}

		redirect(site_url($this->url_));
	}

	function edit($id)
	{
		$datacontent['dt_view'] = $this->Model->get_data_view()->result_array();
		$datacontent['dt_header'] = $this->Model->get_data_header()->result_array();
		$datacontent['row'] = $this->Model->get_data_where("menu_id = '".$id."'")->result_array();

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Ubah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form_edit', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan_edit($id)
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');

			$where = "icon_id = '".$data['icon_id']."'";
			$data_icon = $this->Model->get_data_icon_where($where)->result_array();

			$table = "m_menu";

			if ($data['view_id']!="") {
				$where = "view_id = '".$data['view_id']."'";
				$data_view = $this->Model->get_data_view_where($where)->result_array();

				$in = array(
					'menu_name' => $data['menu_name'],
					'menu_url' => $data['menu_url'],
					'icon_id' => $data['icon_id'],
					'menu_icon' => $data_icon[0]['icon_full_css'],
					'menu_image' => $data_icon[0]['icon_image'],
					'menu_notification' => 1,
					'view_id' => $data['view_id'],
					'menu_notification_source' => $data_view[0]['view_query'],
					'menu_date_update' => date('Y-m-d H:i:s'),
					'menu_user_update' => $_SESSION['user_id']
				);
			} else {
				$in = array(
					'icon_id' => $data['icon_id'],
					'menu_name' => $data['menu_name'],
					'menu_url' => $data['menu_url'],
					'menu_icon' => $data_icon[0]['icon_full_css'],
					'menu_image' => $data_icon[0]['icon_image'],
					'menu_notification' => 0,
					'menu_date_update' => date('Y-m-d H:i:s'),
					'menu_user_update' => $_SESSION['user_id']
				);
			}
			$where = "menu_id = '$id'";

			$exec = $this->Model->update($in,$where);

			$this->session->set_flashdata('sukses', 'Ubah data berhasil');
		}

		redirect(site_url($this->url_));
	}
	
	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);

		redirect($this->url_);
	}
}
