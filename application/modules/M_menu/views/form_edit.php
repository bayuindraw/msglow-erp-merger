<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan_edit/'.$row[0]['menu_id']); ?>" enctype="multipart/form-data">
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__body">
					<div class="form-group">
						<label>Nama Menu</label>
						<input type="text" class="form-control" placeholder="Masukan Nama Menu.." name="input[menu_name]" value="<?php echo $row[0]['menu_name'] ?>" required="">
					</div>
					<div class="form-group">
						<label>URL</label>
						<input type="text" class="form-control" placeholder="Masukan URL.." name="input[menu_url]" value="<?php echo $row[0]['menu_url'] ?>" required>
					</div>
					<div class="form-group">
						<label>Icon</label><br>
						<?php if($row[0]['menu_image']!=""){ ?>
						<img src="<?php echo base_url().'/upload/icon/'.$row[0]['menu_image'] ?>" style="max-height: 50px;"><span style="margin-left: 10px;"><?php echo $row[0]['icon_name'] ?></span><br><br>
						<?php } else { ?>
						<i class="<?php echo $row[0]['menu_icon'] ?>" style="font-size: 20pt;"></i><span style="margin-left: 10px;"><?php echo $row[0]['icon_name'] ?></span><br><br>
						<?php } ?>
						<a href="#iconModal" class="btn btn-default" data-toggle="modal">Pilih Icon</a>
						<input type="hidden" name="input[icon_id]" id="icon_id" value="<?php echo $row[0]['icon_id'] ?>">
						<span style="margin-left: 15px; font-style: italic; font-weight: bold;" id="icon_dipilih"></span>
					</div>
					<div class="form-group">
						<label>View</label>
						<select class="form-control" name="input[view_id]">
							<option value="">-- Pilih View --</option>
							<?php foreach ($dt_view as $d) { ?>
							<option value="<?php echo $d['view_id'] ?>" <?php if($row[0]['view_id']==$d['view_id']) echo 'selected'; ?>><?php echo $d['view_name'] ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="iconModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xl">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Add Icon</h4>
				<button type="button" class="close" data-dismiss="modal"></button>
			</div>

			<div class="modal-body">

				<div class="row">
					<div class="col-md-9">
						<div class="form-group">
							<label>Jenis</label>
							<div class="row">
								<?php foreach ($dt_header as $d) { ?>
								<div class="col-md-2">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="jenis" id="jenis" value="<?php echo $d['m_icon_header'] ?>" onchange="list_icon()" checked> <?php echo $d['m_icon_header_name'] ?>
										</label>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<label>Search</label>
							<input type="text" name="search" id="search" placeholder="" class="form-control" onkeyup="list_icon()">
						</div>
					</div>

					<div class="col-md-12" style="margin-bottom: 20px; border-top: 1px solid #dedede;"></div>
				</div>

				<div class="tampil_icon"></div>

			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var jenis = 1;
		chk_jenis(jenis);

		$('#iconModal').on('show.bs.modal', function(e){
			list_icon();
		});
	});

	function list_icon() {
		var selected="";
		$('input[name="jenis"]:checked').each(function() {
			if(selected==""){
				selected = this.value.trim();
			} else {
				selected = selected+","+this.value.trim();
			}
		});
		var key = document.getElementById('search').value;

		$.ajax({
			type:'post',
			url: '<?php echo site_url('m_menu/open_icon_modal') ?>',
			data: {
				'header':selected,
				'key':key
			},
			success: function(data){
				$('.tampil_icon').html(data);
			}
		});
	}

	function pilih_icon(id_icon, nama_icon) {
		document.getElementById("icon_id").value = id_icon;
		document.getElementById("icon_dipilih").innerHTML = nama_icon;
	}

	function chk_jenis(jns) {
		if (jns==1) {
			$("#vw_header").show();
			$("#vw_icon").show();
			$("#vw_upload").hide();
		} else {
			$("#vw_header").hide();
			$("#vw_icon").hide();
			$("#vw_upload").show();
		}
	}
</script>