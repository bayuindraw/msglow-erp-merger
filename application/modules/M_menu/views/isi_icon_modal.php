<div class="row">
  <?php foreach ($dt_icon as $d) { ?>
  <div class="col-md-2 col-xs-6" data-id="">
    <button class="btn btn-default btn-block" onclick="pilih_icon('<?php echo $d['icon_id'] ?>','<?php echo $d['icon_name'] ?>');" data-dismiss="modal">
      <?php if($d['icon_image']!=""){ ?>
      <img src="<?php echo base_url().'/upload/icon/'.$d['icon_image'] ?>" style="max-height: 20px;"><br>
      <span><?php echo $d['icon_name'] ?></span>
      <?php } else { ?>
      <i class="<?php echo $d['icon_full_css'] ?>"></i><br>
      <span><?php echo $d['icon_name'] ?></span>
      <?php } ?>
    </button>
  </div>
  <?php } ?>
</div>