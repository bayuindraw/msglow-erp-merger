<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_menu_action extends CI_Controller
{
	
	var $url_ = "M_menu_action";
	var $id_ = "action_id";
	var $eng_ = "Action";
	var $ind_ = "Action";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_.'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library("upload");
	}
	
	public function index()
	{

	}
	
	public function get($id)
	{
		$datacontent['action'] = $this->Model->get_action($id)->result_array();

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form_edit', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan()
	{
		$status = $this->input->post('status');
		$action_id = $this->input->post('action_id');
		$menu_id = $this->input->post('menu_id');
		if ($status=="tambah") {
			$in = array(
				'action_id' => $action_id,
				'menu_id' => $menu_id,
				'menu_action_user_create' => $_SESSION['user_id']
			);
			$exec = $this->Model->insert($in);
		} else {
			$in = array(
				'action_id' => $action_id,
				'menu_id' => $menu_id
			);
			$exec = $this->Model->delete($in);
		}
	}
}
