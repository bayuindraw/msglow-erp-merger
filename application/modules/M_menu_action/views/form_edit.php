<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>

			<div class="kt-portlet__body">
				<?php foreach ($action as $d) { ?>
				<div class="checkbox">
					<label><input type="checkbox" value="<?php echo $d['action_id'] ?>" id="cb_<?php echo $d['action_id'] ?>" <?php if($d['action_id']==$d['act_id']) echo 'checked'; ?> onchange="cek('<?php echo $d['action_id'] ?>','<?php echo $id ?>')"> <?php echo $d['action_name'] ?></label>
				</div>
				<?php } ?>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	function cek(id,menu_id) {
		var status = "";
		if (document.getElementById('cb_'+id).checked == true) {
			status = "tambah";
		} else {
			status = "hapus";
		}

		$.ajax({
			type:'post',
			url: '<?php echo site_url('m_menu_action/simpan') ?>',
			data: {
				'status':status,
				'action_id':id,
				'menu_id':menu_id
			},
			success: function(data){
				console.log(status);
			}
		});
	}
</script>