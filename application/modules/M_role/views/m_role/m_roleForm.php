<?php
$role_name = "";
if ($parameter == 'ubah' && $id != '') {
	$this->db->where('role_id', $id);
	$row = $this->Model->get()->row_array();
	extract($row);
}
?>
<?= content_open('Form Role') ?>
<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
	<?= input_hidden('parameter', $parameter) ?>
	<?= input_hidden('id', $id) ?>
	<div class="form-group">
		<label>Provinsi</label>
		<?= input_text('input[role_name]', $role_name) ?>
	</div>
	<hr>
	<div class="form-group">
		<button type="submit" name="simpan" value="true" class="btn btn-info"><i class="fa fa-save"></i>Simpan</button>
		<a href="<?= site_url($url) ?>" class="btn btn-danger"><i class="fa fa-reply"></i>Kembali</a>

	</div>
</form>

<?= content_close() ?>
