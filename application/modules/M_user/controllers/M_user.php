<?php
defined('BASEPATH') or exit('No direct script access allowed');


class M_user extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'M_userModel'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}
	
	public function index()
	{
		$datacontent['url'] = 'M_user';
		$datacontent['title'] = 'Data User';
		//$datacontent['datatable'] = $this->Model->get();
		$data['file']   		= 'User' ;	
		$data['content'] = $this->load->view('M_user/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function login($id)
	{
		$query = "SELECT * FROM m_user A LEFT JOIN m_role B ON B.role_id = A.role_id WHERE (A.user_id = '$id')";
		$data = $this->db->query($query)->row_array();
		if ($data['user_secret'] == "") {
			$data['logged_in'] = true;
		}
		$this->session->set_userdata($data);
		if ($data['role_id'] == '1' || $data['role_id'] == '2') {
			redirect(site_url() . "M_user");
		} else if ($data['role_id'] == '5') {
			redirect(site_url() . "Laporan2/chart");
		} else if ($data['role_id'] == '98') {
			redirect(site_url() . "T_salesd");
		} else if ($data['role_id'] == '11') {
			redirect(site_url() . "COA");
		} else if ($data['role_id'] == '24' || $data['role_id'] == '25') {
			redirect(site_url() . "COA");
		} else {
			redirect(site_url() . "Laporan2/chart");
		}
	}
	
	public function table_sales()
	{
		$datacontent['url'] = 'M_user';
		$datacontent['title'] = 'Data User';
		//$datacontent['datatable'] = $this->Model->get();
		$data['file']   		= 'User' ;	
		$data['content'] = $this->load->view('M_user/table_sales', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function table_inventory()
	{
		$datacontent['url'] = 'M_user';
		$datacontent['title'] = 'Data User';
		//$datacontent['datatable'] = $this->Model->get();
		$data['file']   		= 'User' ;	
		$data['content'] = $this->load->view('M_user/table_inventory', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	
	public function get_data_sales()
	{
		$datacontent['datatable'] = $this->Model->get_data_sales();
	}
	
	public function get_data_inventory()
	{
		$datacontent['datatable'] = $this->Model->get_data_inventory();
	}
	
	public function form($parameter = '', $id = '')
	{
		if ($parameter=="tambah") {
			$datacontent['title'] = 'Tambah Data User';
		} else {
			$datacontent['title'] = 'Ubah Data User';
		}

		$where = "";
		if ($_SESSION['role_id']==10) {
			$datacontent['back'] = 'M_user/table_inventory';
			$where = "and department_id = '".$_SESSION['department_id']."'";
		} else {
			$datacontent['back'] = 'M_user';
		}

		$datacontent['parameter'] = $parameter;
		$datacontent['id'] = $id;
		// $datacontent['m_role'] = $this->Model->get_list_role();
		$datacontent['m_user'] = $this->Model->get_table_where("*","m_user","user_id = '$id'")->result_array();
		$datacontent['m_user_role'] = $this->Model->get_table_where("*","m_user_role","user_id = '$id'")->result_array();
		$datacontent['m_user_role_brand'] = $this->Model->get_table_where("*","m_user_role_brand","user_id = '$id'")->result_array();
		$datacontent['m_role'] = $this->Model->get_table_where("*","m_role","role_id != 0 ".$where)->result_array();
		$datacontent['m_brand'] = $this->Model->get_table_where("*","m_brand","brand_id != 0")->result_array();
		$datacontent['url'] = 'M_user';
		$data['file']   		= 'Data User Sales';
		$data['content'] = $this->load->view('M_user/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function form_sales($parameter = '', $id = '')
	{
		if ($parameter=="tambah") {
			$datacontent['title'] = 'Tambah Data User Sales';
		} else {
			$datacontent['title'] = 'Ubah Data User Sales';
		}
		$datacontent['parameter'] = $parameter;
		$datacontent['id'] = $id;
		$datacontent['m_role'] = $this->Model->get_list_role();
		$datacontent['url'] = 'M_user';
		$data['file']   		= 'Data User Sales';
		$data['content'] = $this->load->view('M_user/form_sales', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function barcode_verify($id = '')
	{
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library('GoogleAuthenticator');
		$pga = new GoogleAuthenticator();
		$secret = $pga->createSecret();
		$datacontent['title'] = 'Verify User';
		$datacontent['id'] = $id;
		$this->db->where('user_id', $id);
		$row = $this->Model->get('m_user')->row_array();
		
		$datacontent['data'] = $row;
		$datacontent['secret'] = $secret;
		$qr_code =  $pga->getQRCodeGoogleUrl($row['user_email'], $secret, 'MS GLOW');
		$datacontent['qr_code'] = $qr_code;
		$datacontent['url'] = 'M_user';
		$data['file']   		= 'User' ;
		$data['content'] = $this->load->view('M_user/barcode_verify', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function cek_authentication_code()
	{
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library('GoogleAuthenticator');
		$pga = new GoogleAuthenticator();
		$code = $pga->getCode($_POST['secret']);
		if($code == $_POST['authentication_code']){
			$info = '<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa fa-check"></i> Sukses!</h4> Sinkronisasi sukses </div>';
			$this->session->set_flashdata('info', $info);
			$data['user_date_update'] = date('Y-m-d H:i:s');
			$data['user_secret'] = $_POST['secret'];
			$this->Model->update($data, ['user_id' => $this->input->post('id')]);
			redirect(site_url('M_user'));
		}else{
			redirect(site_url('M_user/barcode_verify/'.$_POST['id']));
		}
	}
	
	public function delete_authenticator($id = '')
	{
		$data['user_date_update'] = date('Y-m-d H:i:s');
		$data['user_secret'] = '';
		$this->Model->update($data, ['user_id' => $this->input->post('id')]);
		redirect(site_url('M_user'));
	}
	
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$data['user_password'] = md5($data['user_password']);
				$data['user_password_sha'] = hash('sha256', $data['user_password']);
				$data['user_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert("m_user", $data);
				$user_id = $this->db->insert_id();
			} else {
				$user_id = $this->input->post('id');
				if($_POST['change_password'] == 'on' && $_POST['user_password'] != ""){
					$data['user_password'] = md5($_POST['user_password']);
					$data['user_password_sha'] = hash('sha256', $data['user_password']);
				}
				$data['user_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, ['user_id' => $this->input->post('id')]);

				$this->Model->delete("m_user_role", ['user_id' => $this->input->post('id')]);
				$this->Model->delete("m_user_role_brand", ['user_id' => $this->input->post('id')]);
			}

			$data2 = $this->input->post('input2');
			for ($i=0; $i < count($data2['role']); $i++) {
				$di = array(
					'user_id' => $user_id,
					'role_id' => $data2['role'][$i],
					'user_role_date_create' => date('Y-m-d H:i:s'),
					'user_role_user_create' => $_SESSION['user_id']
				);
				$exec = $this->Model->insert("m_user_role", $di);
				$user_role_id = $this->db->insert_id();

				$di = array(
					'user_role_id' => $user_role_id,
					'user_id' => $user_id,
					'role_id' => $data2['role'][$i],
					'brand_id' => $data2['brand'][$i],
					'user_role_brand_date_create' => date('Y-m-d H:i:s'),
					'user_role_brand_user_create' => $_SESSION['user_id']
				);
				$exec = $this->Model->insert("m_user_role_brand", $di);
			}
		}

		if($_SESSION['role_id'] == 12){
			redirect('M_user/table_sales');
		}else if($_SESSION['role_id'] == 10){
			redirect('M_user/table_inventory');
		}else{
			redirect(site_url('M_user'));
		}
	}
	public function hapus($id = '')
	{
		$data['user_status'] = 3;
		$this->Model->update($data, ['user_id' => $id]);
		if($_SESSION['role_id'] == 12){
			redirect('M_user/table_sales');
		}else{
			redirect(site_url('M_user'));
		}
	}
}
