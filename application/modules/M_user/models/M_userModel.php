<?php
defined('BASEPATH') or exit('No direct script access allowed');
class M_userModel extends CI_Model
{
	//BAYU
	function get_table_where($select, $table, $where, $order_by = '')
	{
		$this->db->select($select);
		$this->db->where($where);
		$this->db->order_by($order_by,"ASC");
		$data = $this->db->get($table);
		return $data;
	}
	//

	function get()
	{
		if($_SESSION['role_id'] == '2' || $_SESSION['role_id'] == '1') '';
		else $this->db->where("m_role.role_id NOT IN ('2', '1')");
		$this->db->join('m_role', 'm_role.role_id = m_user.role_id');
		$data = $this->db->get('m_user');
		return $data;
	}
	
	function get_data()
	{
		$table = 'm_user A';
		$id = 'user_id';
		$field = array('user_fullname', 'user_password_random', 'user_name', 'user_email', 'role_name');
		$arrjoin[] = 'LEFT JOIN m_role B ON B.role_id = A.role_id';
		// $arrwhere[] = 'A.user_status != 3';
		$url = 'M_user';
		$action = '<a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . "/barcode_verify/xid").'" class="btn btn-warning"> <i class="fa fa-code"></i></a> <a href="'.site_url($url . '/hapus/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a> <a href="'.site_url($url . '/login/xid').'" class="btn btn-danger" onclick="return confirm(\'Login?\')"> <i class="fa fa-key"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);
		
		echo json_encode($data);
	}
	
	function get_data_sales()
	{
		$table = 'm_user A';
		$id = 'user_id';
		$field = array('user_fullname', 'user_password_random', 'user_name', 'user_email', 'role_name');
		$arrjoin[] = 'LEFT JOIN m_role B ON B.role_id = A.role_id';
		$arrwhere[] = 'A.role_id = 7';
		$arrwhere[] = "A.sales_category_id = '".$_SESSION['sales_category_id']."'";
		$arrwhere[] = "A.user_status IN('1', '2')";
		$url = 'M_user';
		$action = '<a href="'.site_url($url."/form_sales/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . '/hapus/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);
		
		echo json_encode($data);
	}
	
	function get_data_inventory()
	{
		$table = 'm_user A';
		$id = 'user_id';
		$field = array('user_fullname', 'user_password_random', 'user_name', 'role_name');
		$arrjoin[] = 'LEFT JOIN m_role B ON B.role_id = A.role_id';
		$arrwhere[] = "B.department_id = '".$_SESSION['department_id']."'";
		$arrwhere[] = "A.user_status IN('1', '2')";
		$url = 'M_user';
		$action = '<a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . '/hapus/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);
		
		echo json_encode($data);
	}
	
	function get_list_role()
	{
		if($_SESSION['role_id'] == '2'){ 
		}else{ 
			$this->db->where("role_manager_id = ('".$_SESSION['role_id']."')");
		}
		return $this->db->get('m_role')->result();
	}
	
	function get_list_users_propinsi($id = ""){
		$this->db->where('users_id', $id);
		$data = $this->db->get('users_provinsi')->result_array();
		foreach($data as $index => $value){
			$newdata[$value['provinsi_id']] = $value['users_id']; 
		}
		return @$newdata;
	}
	
	function insert($table, $data = array())
	{
		$this->db->insert($table, $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update('m_user', $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function delete($table = '', $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		if ($table=="") {
			$table = "m_user";
		}
		$this->db->delete($table);
		$info = '<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
}
