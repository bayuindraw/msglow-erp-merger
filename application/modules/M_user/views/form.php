<!-- <?php
$name = "";
$email = "";
$remember_token = "";
$password = "";
$role_id = "";
if ($parameter == 'ubah' && $id != '') {
	$this->db->where('user_id', $id);
	$row = $this->Model->get()->row_array();
	extract($row);
}
?> -->
<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__body">
					<div class="form-group">
						<label>Username</label>
						<input type="text" class="form-control" placeholder="Username" name="input[user_name]" value="<?= @$m_user[0]['user_name'] ?>" required>
					</div>
					<div class="form-group">
						<label>Nama Lengkap</label>
						<input type="text" class="form-control" placeholder="Nama Lengkap" name="input[user_fullname]" value="<?= @$m_user[0]['user_fullname'] ?>" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Email" name="input[user_email]" value="<?= @$m_user[0]['user_email'] ?>" required>
					</div>
					<!-- <div class="form-group">
						<label>Role</label>
						<select class="form-control" id="exampleSelect1" name="input[role_id]" required>
							<option value="">Pilih Role</option>
							<?php foreach ($m_role as $d) { ?>
							<option value="<?php echo $d['role_id'] ?>"><?php echo $d['role_name'] ?></option>							
							<?php } ?>
						</select>
					</div> -->

					<?php if($parameter != 'ubah' && $id == ''){ ?>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="input[user_password]">
						</div>

						<input type="hidden" id="jmlrole" value="1">
						<div id="div-role">
							<div id="role1">
								<div class="form-group row">
									<div class="col-lg-6">
										<label>Role 1</label>
										<select class="form-control" name="input2[role][]" required>
											<option value="">Pilih Role</option>
											<?php foreach ($m_role as $d) { ?>
												<option value="<?php echo $d['role_id'] ?>"><?php echo $d['role_name'] ?></option>							
											<?php } ?>
										</select>
									</div>
									<div class="col-lg-5">
										<label>Brand 1</label>
										<select class="form-control" name="input2[brand][]" required>
											<option value="">Pilih Brand</option>
											<?php foreach ($m_brand as $d) { ?>
												<option value="<?php echo $d['brand_id'] ?>"><?php echo $d['brand_name'] ?></option>							
											<?php } ?>
										</select>
									</div>
									<div class="col-lg-1">
										<button onclick="deleteFormRole(1);" class="btn btn-danger" style="margin-top: 25px;"><i class="fas fa-trash"></i></button>
									</div>
								</div>
							</div>
						</div>

						<div class="kt-form__actions">
							<button type="button" class="btn btn-warning" onclick="addFormRole();">Tambah Role</button>
						</div><br>

					<?php }else{ ?>

						<div class="form-group">
							<div class="kt-radio-list">
								<label class="kt-radio">
									<input type="checkbox" name="change_password" onclick="$('#divpassword').toggle()"> Ubah Password
									<span></span>
								</label>
							</div>
						</div>
						<div class="form-group" style="display:none;" id="divpassword">
							<label for="exampleInputPassword1">Password</label>
							<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="user_password">
						</div>

						<input type="hidden" id="jmlrole" value="<?php echo count($m_user_role) ?>">
						<div id="div-role">
							<?php $no=1; for ($i=0; $i < count($m_user_role); $i++) { ?>
								<div id="role<?php echo $no ?>">
									<div class="form-group row">
										<div class="col-lg-6">
											<label>Role <?php echo $no ?></label>
											<select class="form-control" name="input2[role][]" required>
												<option value="">Pilih Role</option>
												<?php foreach ($m_role as $d) { ?>
													<option value="<?php echo $d['role_id'] ?>" <?php if($m_user_role[$i]['role_id']==$d['role_id']) echo 'selected'; ?>><?php echo $d['role_name'] ?></option>							
												<?php } ?>
											</select>
										</div>
										<div class="col-lg-5">
											<label>Brand <?php echo $no ?></label>
											<select class="form-control" name="input2[brand][]" required>
												<option value="">Pilih Brand</option>
												<?php foreach ($m_brand as $d) { ?>
													<option value="<?php echo $d['brand_id'] ?>" <?php if($m_user_role_brand[$i]['brand_id']==$d['brand_id']) echo 'selected'; ?>><?php echo $d['brand_name'] ?></option>							
												<?php } ?>
											</select>
										</div>
										<div class="col-lg-1">
											<button onclick="deleteFormRole('<?php echo $no ?>');" class="btn btn-danger" style="margin-top: 25px;"><i class="fas fa-trash"></i></button>
										</div>
									</div>
								</div>
								<?php $no++; } ?>
							</div>

							<div class="kt-form__actions">
								<button type="button" class="btn btn-warning" onclick="addFormRole();">Tambah Role</button>
							</div><br>
						<?php } ?>


					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
							<a href="<?= site_url($back); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
						</div>
					</div>
				</form>


			</div>
		</div>
	</div>

	<script type="text/javascript">


		function addFormRole() {
			var jmlrole = document.getElementById('jmlrole').value;
			var lengthForm = parseInt(jmlrole) + 1;
			var htmlrole = counttiering(lengthForm);
			$('#div-role').append(htmlrole);
		}

		function counttiering(lengthForm){
			console.log(lengthForm);
			document.getElementById('jmlrole').value = lengthForm;
			var html = "";
			var first = 0;
			var idx = 1;
			html = html + '<div id="role'+ lengthForm +'" class="tiering-form">';

			html = html + '<div class="form-group row">';

			html = html + '<div class="col-lg-6">';
			html = html + '<label for="exampleInputPassword1">Role '+ lengthForm +'</label>';
			html = html + '<select class="form-control" name="input2[role][]" required>';
			html = html + '<option value="">Pilih Role</option>';
			html = html + '<?php foreach ($m_role as $d) { ?>';
			html = html + '<option value="<?php echo $d['role_id'] ?>"><?php echo $d['role_name'] ?></option>';
			html = html + '<?php } ?>';
			html = html + '</select>';
			html = html + '</div>';

			html = html + '<div class="col-lg-5">';
			html = html + '<label for="exampleInputPassword1">Brand '+ lengthForm +'</label>';
			html = html + '<select class="form-control" name="input2[brand][]" required>';
			html = html + '<option value="">Pilih Brand</option>';
			html = html + '<?php foreach ($m_brand as $d) { ?>';
			html = html + '<option value="<?php echo $d['brand_id'] ?>"><?php echo $d['brand_name'] ?></option>';
			html = html + '<?php } ?>';
			html = html + '</select>';
			html = html + '</div>';

			html = html + '<div class="col-lg-1"><button onclick="deleteFormRole(' + lengthForm + ');" class="btn btn-danger" style="margin-top: 25px;"><i class="fas fa-trash"></i></button></div>';

			html = html + '</div>';

			html = html + '</div>';
			return html;
		}

		function deleteFormRole(id) {
			$('#role' + id).remove();
			if (id==1) {
				var htmlrole = counttiering(id);
				$('#div-role').append(htmlrole);
			}
		}
	</script>