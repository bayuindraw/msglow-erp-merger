<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user_role extends CI_Controller
{
	
	var $url_ = "M_user_role";
	var $id_ = "department_id";
	var $eng_ = "User Role";
	var $ind_ = "User Role";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_.'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library("upload");
	}
	
	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	
	public function tambah()
	{
		$datacontent['dept'] = $this->Model->get_table_where("*","m_department","department_id != '' OR department_id != 0")->result_array();

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Tambah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');

			$table = "m_role";
			$in = array(
				'role_name' => $data['role_name'],
				'department_id' => $data['department_id'],
				'role_date_create' => date('Y-m-d H:i:s'),
				'role_user_create' => $_SESSION['user_id']
			);
			$exec = $this->Model->insert($table, $in);
			$role_id = $this->db->insert_id();

			$dt_menu = $this->Model->get_table_where("*","m_department_menu","department_id = '".$data['department_id']."'")->result_array();
			foreach ($dt_menu as $d) {
				$in = array(
					'role_menu_parent_id' => $d['department_menu_parent_id'],
					'menu_id' => $d['menu_id'],
					'role_id' => $role_id,
					'role_menu_name' => $d['department_menu_name'],
					'role_menu_name' => $d['department_menu_name'],
					'role_menu_icon' => $d['department_menu_icon'],
					'role_menu_url' => $d['department_menu_url'],
					'role_menu_notification' => $d['department_menu_notification'],
					'role_menu_notification_source' => $d['department_menu_notification_source'],
					'role_menu_order' => $d['department_menu_order'],
					'role_menu_level' => $d['department_menu_level'],
					'role_menu_no' => $d['department_menu_no'],
					'view_id' => $d['view_id'],
					'icon_id' => $d['icon_id'],
					'role_menu_date_create' => date('Y-m-d H:i:s'),
					'role_menu_user_create' => $_SESSION['user_id']
				);
				$exec = $this->Model->insert("m_role_menu",$in);
				$role_menu_id = $this->db->insert_id();
				
				$dt_action = $this->Model->get_table_where("*","m_department_menu_action","department_menu_id = '".$d['department_menu_id']."'")->result_array();
				foreach ($dt_action as $da) {
					$in = array(
						'role_menu_id' => $role_menu_id,
						'action_id' => $da['action_id'],
						'role_menu_action_date_create' => date('Y-m-d H:i:s'),
						'role_menu_action_user_create' => $_SESSION['user_id']
					);
					$exec = $this->Model->insert("m_role_menu_action",$in);
				}
			}

			$this->session->set_flashdata('sukses', 'Tambah data berhasil');
		}

		redirect(site_url($this->url_));
	}

	function edit($id)
	{
		$datacontent['dept'] = $this->Model->get_table_where("*","m_department","department_id != '' OR department_id != 0")->result_array();
		$datacontent['role'] = $this->Model->get_table_where("*","m_role","role_id = '$id'")->result_array();
		$datacontent['i'] = 0;

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Ubah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form_edit', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan_edit($id)
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');

			$table = "m_role";
			$in = array(
				'role_name' => $data['role_name'],
				'department_id' => $data['department_id'],
				'role_date_update' => date('Y-m-d H:i:s'),
				'role_user_update' => $_SESSION['user_id']
			);
			$where = array(
				'role_id' => $id
			);
			$exec = $this->Model->update($table, $in, $where);

			$this->session->set_flashdata('sukses', 'Ubah data berhasil');
		}

		redirect(site_url($this->url_));
	}
	
	public function hapus($id = '')
	{
		$where = array(
			'role_id' => $id
		);
		$exec = $this->Model->delete("M_role",$where);

		$dtm = $this->Model->get_table_where("*","m_role_menu","role_id = '".$id."'")->result_array();
		foreach ($dtm as $d) {
			$where = array(
				'role_menu_id' => $d['role_menu_id']
			);
			$exec = $this->Model->delete("m_role_menu",$where);
			$exec = $this->Model->delete("m_role_menu_action",$where);
		}
		redirect($this->url_);
	}

	function menu($id)
	{
		$datacontent['role'] = $this->Model->get_table_where("*","m_role","role_id = '$id'")->result_array();
		$datacontent['menu'] = $this->Model->get_table("m_menu")->result_array();
		$datacontent['i'] = 0;

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Menu Role '.$datacontent['role'][0]['role_name'];
		$datacontent['parameter'] = "";
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form_menu', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}

	public function tampil_isi_menu()
	{
		$dataisi['url'] = $this->url_;
		$dataisi['id'] = $this->input->post('id');
		$dataisi['level'] = $this->Model->get_level($this->input->post('id'));
		$dataisi['m_action'] = $this->Model->get_table("m_action")->result_array();
		$dataisi['list_header'] = $this->Model->get_table_where("*","m_role_menu","role_id = '".$this->input->post('id')."'","role_menu_no")->result_array();

		$this->load->view('M_user_role/table_menu', $dataisi);
	}

	function cek_action()
	{
		$status = $this->input->post('status');
		$table = "m_role_menu_action";
		if ($status == true) {
			$in = array(
				'role_menu_id' => $this->input->post('role_menu_id'),
				'action_id' => $this->input->post('action_id'),
				'role_menu_action_date_create' => date('Y-m-d H:i:s'),
				'role_menu_action_user_create' => $_SESSION['user_id']
			);
			$this->Model->insert($table, $in);
		} else {
			$where = array(
				'role_menu_id' => $this->input->post('role_menu_id'),
				'action_id' => $this->input->post('action_id')
			);
			$this->Model->delete($table,$where);
		}

	}
}
