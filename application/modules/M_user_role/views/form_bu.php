<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Nama Department</label>
								<input type="text" id="department_name" name="input[department_name]" class="md-form-control md-static floating-label form-control" value="">
							</div>
						</div>

						<div class="col-md-12" style="margin-top: 20px;">
							<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
								<thead>
									<tr>
										<th>Menu</th>
										<th style="width: 15%;">Aksi</th>
									</tr>
								</thead>
								<tbody id="x">
									<tr id="<?= $i; ?>">
										<td>
											<select name="input2[<?= $i; ?>][menu_id]" class="PilihMenu form-control md-static" required>
												<option></option>
												<?php foreach ($menu as $d) { ?>
												<option value="<?php echo $d['menu_id'] ?>"><?php echo $d['menu_name'] ?></option>
												<?php } ?>
											</select>
										</td>
										<td>
											<center>
												<button onclick="myDeleteFunction('<?= $i ?>');" class="btn btn-success"><i class="fas fa-plus"></i></button>
												<button onclick="myDeleteFunction('<?= $i ?>');" class="btn btn-danger"><i class="fas fa-trash"></i></button>
											</center>
										</td>
									</tr>
									<tr>
										<td colspan="3">
											<button type="button" class="btn btn-warning" onclick="myCreateFunction();" style="margin-top: 5px;">Tambah Menu</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>


					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.PilihMenu').select2({
			allowClear: true,
			placeholder: 'Pilih Menu',
			tags: true
		});
	});

	var it = 0;
	function myCreateFunction() {
		it++;
		var html = '<tr id="'+it+'">'+
		'<td><select name="input2['+it+'][menu_id]" class="PilihMenu form-control md-static" required><option></option><?php foreach ($menu as $d) { ?><option value="<?php echo $d['menu_id'] ?>"><?php echo $d['menu_name'] ?></option><?php } ?></select></td>'+
		'<td><center><button onclick="myDeleteFunction('+ it +');" class="btn btn-success"><i class="fas fa-plus"></i></button> <button onclick="myDeleteFunction('+ it +');" class="btn btn-danger"><i class="fas fa-trash"></i></button></center></td>'+
		'<tr>';

		$('#x').append(html);
		$('.PilihMenu').select2({
			allowClear: true,
			placeholder: 'Pilih Menu',
		});
	}

	function myDeleteFunction(id) {
		$('#' + id).remove();
	}

</script>