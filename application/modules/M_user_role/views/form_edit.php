<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan_edit/'.$role[0]['role_id']); ?>" enctype="multipart/form-data">
				<div class="kt-portlet__body">
					<div class="row">

						<div class="col-md-12">
							<div class="form-group">
								<label>Nama User Role</label>
								<input type="text" id="role_name" name="input[role_name]" class="md-form-control md-static floating-label form-control" value="<?php echo $role[0]['role_name'] ?>" placeholder="Isi masukan user role..">
							</div>
							<div class="form-group">
								<label>Department Role</label>
								<select name="input[department_id]" class="md-form-control md-static floating-label form-control">
									<?php foreach ($dept as $d) { ?>
									<option value="<?php echo $d['department_id'] ?>" <?php if($d['department_id']==$role[0]['department_id']) ?>><?php echo $d['department_name'] ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>
