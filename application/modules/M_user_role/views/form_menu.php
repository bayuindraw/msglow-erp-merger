<div class="row">
	<div class="col-lg-12 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div class="row">
					<div class="col-md-12">
						<div class="tampil_isi"></div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		vw_isi_menu('<?php echo $id ?>');
	});

	function vw_isi_menu(id) {
		$.ajax({
			type:'post',
			data:{
				"id":id
			},
			url: '<?php echo site_url('M_user_role/tampil_isi_menu') ?>',
			success: function(data){
				$('.tampil_isi').html(data);
			}
		});
	}

	function cek(role_menu_id, action_id) {
		var cb = document.getElementById('cb_'+role_menu_id+'_'+action_id).checked;
		var status = 0;
		if (cb==true) {
			status = 1;
		}
		$.ajax({
			type:'post',
			data:{
				"role_menu_id":role_menu_id,
				"action_id":action_id,
				"status":status
			},
			url: '<?php echo site_url('M_user_role/cek_action') ?>',
			success: function(data){
				console.log("ok");
			}
		});
	}

</script>