<table class="table table-striped table-bordered nowrap" id="kt_table_1">
	<thead>
		<tr>
			<th>Menu</th>
			<?php for ($i=0; $i < count($level); $i++) { ?>
			<th>Sub Menu Lv. <?php echo ($level[$i]['level']-1); ?></th>
			<?php } ?>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($list_header as $d) { ?>
		<tr>
			<?php if($d['role_menu_parent_id']==0){ ?>
			<td><span><?php echo $d['role_menu_name'] ?></span></td>
			<?php } else { ?>
			<td></td>
			<?php } ?>

			<?php for ($i=0; $i < count($level); $i++) { ?>
			<td><span><?php if($d['role_menu_level']==$level[$i]['level']) echo $d['role_menu_name']; ?></span></td>
			<?php } ?>

			<td>
				<?php $action = $this->Model->get_action($d['role_menu_id']); ?>
				<!-- <?php print_r($action); ?> -->
				<?php foreach ($m_action as $da) { ?>
				<?php foreach ($action as $ma) {
					if ($da['action_id']==$ma['action_id']) {
						$on = "checked";
						break;
					} else {
						$on = "";
					}
				} ?>
				<div class="checkbox">
					<label><input type="checkbox" value="<?php echo $da['action_id'] ?>" id="cb_<?php echo $d['role_menu_id'] ?>_<?php echo $da['action_id'] ?>" <?php echo $on; ?> onchange="cek('<?php echo $d['role_menu_id'] ?>','<?php echo $da['action_id'] ?>','<?php echo $id ?>')"> <?php echo $da['action_name'] ?></label>
				</div>
				<?php } ?>
			</td>
		</tr>

		<?php } ?>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",
			"ordering": false,
			scrollY: "300px",
			scrollX: true,
			scrollCollapse: true,
			"processing": true,
			"serverSide": false
		});
	});
</script>