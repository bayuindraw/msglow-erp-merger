<?php
$name = "";
$email = "";
$remember_token = "";
$password = "";
$role_id = "";
if ($parameter == 'ubah' && $id != '') {
	$this->db->where('vendor_id', $id);
	$row = $this->Model->get()->row_array();
}
?>
<div class="row">
								<div class="col-lg-12 col-xl-6">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Nama</label>
													<input type="text" class="form-control" placeholder="Nama" name="input[vendor_name]" value="<?= @$row['vendor_name'] ?>" required>
												</div>
												<div class="form-group">
													<label>Alamat</label>
													<textarea class="summernote" name="input[vendor_address]"><?= @$row['vendor_address'] ?></textarea>
												</div>
												<div class="form-group">
													<label>Detail</label>
													<textarea class="summernote" name="input[vendor_detail]"><?= @$row['vendor_detail'] ?></textarea>
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>