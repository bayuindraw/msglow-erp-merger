<div class="row">
	<div class="col-lg-12 col-xl-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Form Input Member
					</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12 col-xl-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">

			<div class="kt-portlet__body">

				<form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan'); ?>" method="Post">
					<?= input_hidden('parameter', $parameter) ?>
					<div class="kt-portlet__body">
						<div class="form-group">
							<label>Nama</label>
							<input type="text" class="form-control" placeholder="Nama" name="input[nama]" required>
						</div>
						<div class="form-group">
							<label>Kode</label>
							<input type="text" class="form-control" placeholder="Kode" name="input[kode]" required>
						</div>
						<div class="form-group">
							<label>Kota</label>
							<input type="text" class="form-control" placeholder="Kota" name="input[kota]" required>
						</div>
						<div class="form-group">
							<label>No Telepon</label>
							<input type="tel" class="form-control" placeholder="No Telepon" name="input[telepon]" required>
						</div>
						<div class="form-group">
							<label>Status</label>
							<select class="form-control selectStatus" name="input[status]" required>
								<?php foreach($m_member_status as $index_m_status => $value_m_status){	?>
									<option value="<?= $value_m_status['member_status_name']; ?>"><?= $value_m_status['member_status_name']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<textarea class="summernote" name="input[alamat]"><?= @$row['account_detail_note'] ?></textarea>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
							<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>



</div>
</div>
</div>

<script>
	$('.selectStatus').select2({
		allowClear: true,
		placeholder: 'Pilih Status',
	});
</script>