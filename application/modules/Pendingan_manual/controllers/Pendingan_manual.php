<?php

// require 'vendor/autoload.php';

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Style\Border;

defined('BASEPATH') or exit('No direct script access allowed');

class Pendingan_manual extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		ob_start();
		/*error_reporting(0);         */
		$this->load->model('Global_model', 'gm');
		$this->load->library('session');
		$this->load->database();
		// $this->load->library('m_pdf');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download');
	}

	public function index($Aksi = "")
	{

		$dataHeader['title']		= "Pendingan Manual";
		$dataHeader['menu']   		= 'Master';
		$dataHeader['file']   		= 'Pendingan Manual';
		$dataHeader['link']			= 'pendingan_manual';
		$dataHeader['url']			= 'pendingan_manual';
		$data['title']				= "Pendingan Manual - ".date('m/Y');
		$data['action'] 			= $Aksi;
		$data['row']				= $this->gm->get_table_where("a.id , a.nama_produk","produk_global a","id != 0");
		$data['hari'] 				= cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
		$data['bln'] 				= date('m');
		$data['thn'] 				= date('Y');
		$data['bali']				= $this->db->query("SELECT tgl FROM pendingan_manual WHERE MONTH(tgl) = '".date('m')."' and YEAR(tgl) = '".date('Y')."' and grup = 'BALI' ORDER BY tgl DESC LIMIT 1")->result_array();
		$data['seller']				= $this->db->query("SELECT tgl FROM pendingan_manual WHERE MONTH(tgl) = '".date('m')."' and YEAR(tgl) = '".date('Y')."' and grup = 'SELLER' ORDER BY tgl DESC LIMIT 1")->result_array();

		// for ($i=0; $i < $data['hari']; $i++) { 
		// 	$table = "pendingan_manual";
		// 	$where = "MONTH(tgl) = '".date('m')."' and YEAR(tgl) = '".date('Y')."'";
		// 	$dt = $this->gm->get_table_where_rows($table,$where);
		// 	if ($dt<1) {
		// 		$di = array(
		// 			''
		// 		);
		// 	}
		// }

		$dataHeader['content'] = $this->load->view('table', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	public function form($Aksi = "")
	{

		$dataHeader['title']		= "Form Pendingan Manual";
		$dataHeader['menu']   		= 'Pendingan Manual';
		$dataHeader['file']   		= 'Form Pendingan Manual';
		$dataHeader['link']			= 'pendingan_manual/form';
		$dataHeader['url']			= 'pendingan_manual';
		$data['title']				= "Form Pendingan Manual";
		$data['action'] 			= $Aksi;
		$table = "pendingan_manual";
		$where = "tgl = '".$this->input->post('tgl')."'";
		$data['log_bali']				= $this->gm->get_table_where("*","pendingan_manual","grup = 'BALI' AND tgl = '".date('Y-m-d')."'");
		$data['log_seller']				= $this->gm->get_table_where("*","pendingan_manual","grup = 'SELLER' AND tgl = '".date('Y-m-d')."'");
		$data['produk']				= $this->gm->get_table_where("a.id , a.nama_produk","produk_global a","id != 0");

		$dataHeader['content'] = $this->load->view('form', $data, TRUE);
		$this->load->view('Layout/home', $dataHeader);
	}

	function set_value()
	{
		$bali				= $this->gm->get_table_where("IFNULL(qty,0) as qty","pendingan_manual","grup = 'BALI' AND tgl = '".$this->input->post('tgl')."' and produk_global_id = '".$this->input->post('id_produk')."'");
		$seller				= $this->gm->get_table_where("IFNULL(qty,0) as qty","pendingan_manual","grup = 'SELLER' AND tgl = '".$this->input->post('tgl')."' and produk_global_id = '".$this->input->post('id_produk')."'");
		if (count($bali)>0) {
			$qty_bali = $bali[0]['qty'];
		} else {
			$qty_bali = 0;
		}
		if (count($seller)>0) {
			$qty_seller = $seller[0]['qty'];
		} else {
			$qty_seller = 0;
		}
		echo json_encode(array('status' => TRUE, 'bali' => $qty_bali, 'seller' => $qty_seller));
	}

	function simpan()
	{
		$table = "pendingan_manual";
		$where = "tgl = '".$this->input->post('tgl')."' and grup = 'BALI' and produk_global_id = '".$this->input->post('produk_global_id')."'";
		$dt = $this->gm->get_table_where_rows($table,$where);
		if ($dt>0) {
			$du = array(
				'produk_global_id' => $this->input->post('produk_global_id'),
				'tgl' => $this->input->post('tgl'),
				'qty' => $this->input->post('qty_bali'),
				'grup' => "BALI",
				'insert_by' => $this->session->user_name,
				'insert_dt' => date('Y-m-d H:i:s')
			);
			$this->gm->update_table($table,$du,$where);
		} else {
			$di = array(
				'produk_global_id' => $this->input->post('produk_global_id'),
				'tgl' => $this->input->post('tgl'),
				'qty' => $this->input->post('qty_bali'),
				'grup' => "BALI",
				'insert_by' => $this->session->user_name,
				'insert_dt' => date('Y-m-d H:i:s')
			);
			$this->gm->insert_table($table,$di);
		}

		$table = "pendingan_manual";
		$where = "tgl = '".$this->input->post('tgl')."' and grup = 'SELLER' and produk_global_id = '".$this->input->post('produk_global_id')."'";
		$dt = $this->gm->get_table_where_rows($table,$where);
		if ($dt>0) {
			$du = array(
				'produk_global_id' => $this->input->post('produk_global_id'),
				'tgl' => $this->input->post('tgl'),
				'qty' => $this->input->post('qty_seller'),
				'grup' => "SELLER",
				'insert_by' => $this->session->user_name,
				'insert_dt' => date('Y-m-d H:i:s')
			);
			$this->gm->update_table($table,$du,$where);
		} else {
			$di = array(
				'produk_global_id' => $this->input->post('produk_global_id'),
				'tgl' => $this->input->post('tgl'),
				'qty' => $this->input->post('qty_seller'),
				'grup' => "SELLER",
				'insert_by' => $this->session->user_name,
				'insert_dt' => date('Y-m-d H:i:s')
			);
			$this->gm->insert_table($table,$di);
		}

		redirect('Pendingan_manual','refresh');
	}
}
