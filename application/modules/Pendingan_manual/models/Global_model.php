<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Global_model extends CI_Model{

	var $table_ = "pendingan_manual";
	var $id_ = "id";
	var $eng_ = "pendingan_manual";
	var $url_ = "Pendingan_manual";

	function VwTable($tahun, $bulan){

		$query = $this->db->query("SELECT a.*, b.nama_produk 
			FROM pendingan_manual a 
			JOIN produk_global b ON a.produk_global_id = b.id 
			WHERE YEAR(a.tgl) = '".date('Y')."' AND MONTH(a.tgl) = '".date('m')."'")->result_array();

		return $query;

	}

	function get_table($table){

		$query	= $this->db->get($table);

		return $query->result_array();

	}



	function get_table_rows($table){

		$query	= $this->db->get($table);

		return $query->num_rows();

	}



	function get_table_where($select = "*", $table, $where){

		$this->db->select($select);

		$this->db->where($where);

		$query	= $this->db->get($table);

		return $query->result_array();

	}

	function get_table_where_rows($table, $where){

		$this->db->where($where);

		$query	= $this->db->get($table);

		return $query->num_rows();

	}



	function get_table_select($select, $table, $where){

		$this->db->select($select);

		$this->db->where($where);

		$query	= $this->db->get($table);

		return $query->result_array();

	}



	function get_table_where_limit($table, $where, $limit){

		$this->db->where($where);

		$this->db->limit($limit);

		$query	= $this->db->get($table);

		return $query->result_array();

	}

	function get_table_where_limit_rows($table, $where, $limit){

		$this->db->where($where);

		$this->db->limit($limit);

		$query	= $this->db->get($table);

		return $query->num_rows();

	}



	function get_table_order_by($table, $order_by){

		$this->db->order_by($order_by, 'DESC');

		$query	= $this->db->get($table);

		return $query->result_array();

	}



	function get_table_order_by_rows($table, $order_by){

		$this->db->order_by($order_by, 'DESC');

		$query	= $this->db->get($table);

		return $query->num_rows();

	}



	function get_table_where_order_by($table, $where, $order_by){

		$this->db->where($where);

		$this->db->order_by($order_by, 'ASC');

		$query	= $this->db->get($table);

		return $query->result_array();

	}



	function get_table_where_order_by_rows($table, $where, $order_by){

		$this->db->where($where);

		$this->db->order_by($order_by, 'ASC');

		$query	= $this->db->get($table);

		return $query->num_rows();

	}



	function get_table_where_order_by2($table, $where, $order_by){

		$this->db->where($where);

		$this->db->order_by($order_by, 'DESC');

		$query	= $this->db->get($table);

		return $query->result_array();

	}



	function get_table_where_order_by2_rows($table, $where, $order_by){

		$this->db->where($where);

		$this->db->order_by($order_by, 'DESC');

		$query	= $this->db->get($table);

		return $query->num_rows();

	}



	function get_table_order_by_limit($table, $limit, $order_by){

		$this->db->order_by($order_by, 'DESC');

		$this->db->limit($limit);

		$query	= $this->db->get($table);

		return $query->result_array();

	}



	function get_table_order_by_limit_rows($table, $limit, $order_by){

		$this->db->order_by($order_by, 'DESC');

		$this->db->limit($limit);

		$query	= $this->db->get($table);

		return $query->num_rows();

	}



	function get_table_order_by_limit_where($table, $limit, $order_by, $where){

		$this->db->order_by($order_by, 'DESC');

		$this->db->where($where);

		$this->db->limit($limit);

		$query	= $this->db->get($table);

		return $query->result_array();

	}



	function get_table_order_by_limit_where_rows($table, $limit, $order_by, $where){

		$this->db->order_by($order_by, 'DESC');

		$this->db->where($where);

		$this->db->limit($limit);

		$query	= $this->db->get($table);

		return $query->num_rows();

	}



	function get_table_order_by_limit2_where($table, $limit, $order_by, $where){

		$this->db->order_by($order_by, 'DESC');

		$this->db->where($where);

		$this->db->limit($limit);

		$query	= $this->db->get($table);

		return $query->result_array();

	}



	function get_table_order_by_limit2_where_rows($table, $limit, $order_by, $where){

		$this->db->order_by($order_by, 'DESC');

		$this->db->where($where);

		$this->db->limit($limit);

		$query	= $this->db->get($table);

		return $query->num_rows();

	}

	function get_table_inner_join_limit($table1, $table2, $join, $order_by, $limit){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->join($table2, $join);
		$this->db->order_by($order_by, 'ASC');
		$this->db->limit($limit);
		$query = $this->db->get();

		return $query->result_array();
	}

	function get_table_inner_join_limit_rows($table1, $table2, $join, $order_by, $limit){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->join($table2, $join);
		$this->db->order_by($order_by, 'ASC');
		$this->db->limit($limit);
		$query = $this->db->get();

		return $query->num_rows();
	}

	function get_table_inner_join_where($table1, $where, $table2, $join){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->where($where);
		$this->db->join($table2, $join);
		$query = $this->db->get();

		return $query->result_array();
	}

	function get_table_inner_join_where_rows($table1, $where, $table2, $join){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->where($where);
		$this->db->join($table2, $join);
		$query = $this->db->get();

		return $query->num_rows();
	}

	function get_table_inner_join_where2($table1, $where, $table2, $join, $table3, $join2){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->where($where);
		$this->db->join($table2, $join);
		$this->db->join($table3, $join2);
		$query = $this->db->get();

		return $query->result_array();
	}

	function get_table_inner_join_where2_rows($table1, $where, $table2, $join, $table3, $join2){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->where($where);
		$this->db->join($table2, $join);
		$this->db->join($table3, $join2);
		$query = $this->db->get();

		return $query->num_rows();
	}

	function get_table_inner_join_group_by($table1, $where, $table2, $join, $group_by, $order_by){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->where($where);
		$this->db->join($table2, $join);
		$this->db->group_by($group_by);
		$this->db->order_by($order_by, 'ASC');
		$query = $this->db->get();

		return $query->result_array();
	}

	function get_table_inner_join_group_by_rows($table1, $where, $table2, $join, $group_by, $order_by){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->where($where);
		$this->db->join($table2, $join);
		$this->db->group_by($group_by);
		$this->db->order_by($order_by, 'ASC');
		$query = $this->db->get();

		return $query->num_rows();
	}

	function get_table_inner_join_group_by2($table1, $where, $table2, $join, $table3, $join2, $group_by, $order_by){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->where($where);
		$this->db->join($table2, $join);
		$this->db->join($table3, $join2);
		$this->db->group_by($group_by);
		$this->db->order_by($order_by, 'ASC');
		$query = $this->db->get();

		return $query->result_array();
	}

	function get_table_inner_join_group_by2_rows($table1, $where, $table2, $join, $table3, $join2, $group_by, $order_by){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->where($where);
		$this->db->join($table2, $join);
		$this->db->join($table3, $join2);
		$this->db->group_by($group_by);
		$this->db->order_by($order_by, 'ASC');
		$query = $this->db->get();

		return $query->num_rows();
	}

	function get_table_inner_join($table1, $where, $table2, $join){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->where($where);
		$this->db->join($table2, $join);
		$query = $this->db->get();

		return $query->result_array();
	}

	function get_table_inner_join_rows($table1, $where, $table2, $join){
		$this->db->select('*');    
		$this->db->from($table1);
		$this->db->where($where);
		$this->db->join($table2, $join);
		$query = $this->db->get();

		return $query->num_rows();
	}


	function get_table_join($from, $join, $where){

		$this->db->select('*');

		$this->db->from($from);

		$this->db->join($join,$where);

		$query	= $this->db->get();

		return $query->result_array();

	}



	function get_table_join_limit_order_by($from, $join, $where, $limit, $order_by){

		$this->db->select('*');

		$this->db->from($from);

		$this->db->join($join,$where);

		$this->db->limit($limit);

		$this->db->order_by($order_by, 'DESC');

		$query	= $this->db->get();

		return $query->result_array();

	}



	function get_table_group_by($from, $join, $join2, $group_by){

		$this->db->select('*');

		$this->db->from($from);

		$this->db->join($join,$join2);

		$this->db->group_by($group_by);

		$query	= $this->db->get();

		return $query->result_array();

	}



	function get_table_group_by_rows($from, $join, $join2, $group_by){

		$this->db->select('*');

		$this->db->from($from);

		$this->db->join($join,$join2);

		$this->db->group_by($group_by);

		$query	= $this->db->get();

		return $query->num_rows();

	}



	function get_table_group_by_where($from, $join, $join2, $group_by, $where){

		$this->db->select('*');

		$this->db->from($from);

		$this->db->where($where);

		$this->db->join($join,$join2);

		$this->db->group_by($group_by);

		$query	= $this->db->get();

		return $query->result_array();

	}



	function get_table_group_by_where_rows($from, $join, $join2, $group_by, $where){

		$this->db->select('*');

		$this->db->from($from);

		$this->db->where($where);

		$this->db->join($join,$join2);

		$this->db->group_by($group_by);

		$query	= $this->db->get();

		return $query->num_rows();

	}



	function get_table_group_by_limit($from, $join, $join2, $order_by, $group_by, $limit){

		$this->db->select('*');

		$this->db->from($from);

		$this->db->join($join,$join2);

		$this->db->order_by($order_by, 'DESC');

		$this->db->limit($limit);

		$this->db->group_by($group_by);

		$query	= $this->db->get();

		return $query->result_array();

	}



	function get_table_group_by_limit_where($from, $join, $join2, $order_by, $group_by, $limit, $where){

		$this->db->select('*');

		$this->db->from($from);

		$this->db->where($where);

		$this->db->join($join,$join2);

		$this->db->order_by($order_by, 'DESC');

		$this->db->limit($limit);

		$this->db->group_by($group_by);

		$query	= $this->db->get();

		return $query->result_array();

	}



	function get_table_group_by_limit_where_rows($from, $join, $join2, $order_by, $group_by, $limit, $where){

		$this->db->select('*');

		$this->db->from($from);

		$this->db->where($where);

		$this->db->join($join,$join2);

		$this->db->order_by($order_by, 'DESC');

		$this->db->limit($limit);

		$this->db->group_by($group_by);

		$query	= $this->db->get();

		return $query->num_rows();

	}



	function insert_table($table,$data){

		$query	= $this->db->insert($table,$data);

		return $query;

	}



	function insert_id_table($table,$data){

		$this->db->insert($table,$data);

		return $data;

	}



	function update_table($table,$data,$where){

		$this->db->where($where);

		$query	= $this->db->update($table,$data);

		return $query;

	}



	function delete_table($table,$where){

		$this->db->where($where);

		$query	= $this->db->delete($table);

		return $query;

	}

}