<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            <?php echo $title ?>
          </h3>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">

      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= base_url() ?>Pendingan_manual/simpan" method="Post">

          <div class="form-group">
            <i class="far fa-calendar-alt"></i>
            <label>Tanggal Pendingan</label>
            <br>
            <span style="font-weight: bold;"><?php echo date('Y-m-d') ?></span>
            <input type="hidden" id="tgl" name="tgl" value="<?php echo date('Y-m-d') ?>">
          </div>
          <div class="form-group">
            <i class="fas fa-tag"></i>
            <label>Nama Barang</label>
            <select name="produk_global_id" id="pilihproduk" class="md-form-control md-static" onchange="set_qty()" required>
              <option></option>
              <?php foreach ($produk as $d) { ?>
                <option value="<?= $d['id'] ?>"><?= $d['nama_produk'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <i class="fas fa-plus-circle"></i>
            <label>QTY Bali</label>
            <input type="number" id="qty_bali" name="qty_bali" class="form-control" value="0" min="0" class="md-form-control md-static" required>
          </div>
          <div class="form-group">
            <i class="fas fa-plus-circle"></i>
            <label>QTY Seller</label>
            <input type="number" id="qty_seller" name="qty_seller" class="form-control" value="0" min="0" class="md-form-control md-static" required>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light pull-right" title="Tambahkan Barang Jadi">
                <i class="fas fa-save"></i><span class="m-l-10"> Simpan</span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  set_qty();
  function set_qty() {
    var id_produk = document.getElementById("pilihproduk").value;
    var tgl = document.getElementById("tgl").value;
    if(id_produk==""){
      document.getElementById("qty_bali").value = 0;
      document.getElementById("qty_seller").value = 0;
    } else {
      $.ajax({
        type:'post',
        url: '<?php echo base_url().'Pendingan_manual/set_value' ?>',
        data: {
          "id_produk":id_produk,
          "tgl":tgl
        },
        success: function(data){
          var json = $.parseJSON(data);
          document.getElementById("qty_bali").value = json.bali;
          document.getElementById("qty_seller").value = json.seller;
        }
      });
    }
  }
</script>