<div class="kt-portlet kt-portlet--mobile">

  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        <?php echo $title ?>
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
          <!-- <a href="<?= base_url() ?>Laporan2/daily_stock" class="btn btn-primary waves-effect waves-light" target="_blank"> <i class="la la-print"></i> <span class="m-l-10">Cetak Stock</span></a> -->
          <a href="<?= base_url() ?>Pendingan_manual/form" class="btn btn-primary waves-effect waves-light"> <i class="fa fa-edit"></i> <span class="m-l-10">Update Pendingan</span></a>
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">

    <!--begin: Datatable -->
    <div content="table">
      <table class="table table-striped table-bordered nowrap" id="kt_table_1">
        <thead>
          <tr>
            <th rowspan="2" style="text-align: center;">Nama Produk</th>
            <th style="text-align: center;">Bali</th>
            <th style="text-align: center;">Terbayar</th>
          </tr>
          <tr>
            <?php if(count($bali)>0) $tgl_bali = $bali[0]['tgl']; else $tgl_bali = '-'; ?>
            <th style="text-align: center;"><?php echo $tgl_bali ?></th>
            <?php if(count($seller)>0) $tgl_seller = $seller[0]['tgl']; else $tgl_seller = '-'; ?>
            <th style="text-align: center;"><?php echo $tgl_seller ?></th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 0; foreach ($row as $d) { ?>
            <tr>
              <td><?= $d['nama_produk'] ?></td>
              <?php $dt = $this->db->query("SELECT IFNULL(qty,0) as qty FROM pendingan_manual WHERE produk_global_id = '".$d['id']."' and tgl = '".$tgl_bali."' and grup = 'BALI'")->result_array(); ?>
              <?php if(count($dt)>0) $qty = $dt[0]['qty']; else $qty = ''; ?>
              <td><?php echo $qty ?></td>

              <?php $dt = $this->db->query("SELECT IFNULL(qty,0) as qty FROM pendingan_manual WHERE produk_global_id = '".$d['id']."' and tgl = '".$tgl_seller."' and grup = 'SELLER'")->result_array(); ?>
              <?php if(count($dt)>0) $qty = $dt[0]['qty']; else $qty = ''; ?>
              <td><?php echo $qty ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#kt_table_1').DataTable({
      "pagingType": "full_numbers",
      // scrollY: "300px",
      scrollX: true,
      scrollCollapse: true,
      "processing": true,
      "paging": false,
      "ordering": false,
      "serverSide": false
    });
  });
</script>