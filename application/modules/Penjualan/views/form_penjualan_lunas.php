<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Form Input Sales Order
          </h3>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-12 col-xl-12"> 
    <!--begin::Portlet-->
    <div class="kt-portlet">

      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan'); ?>" method="Post">
			<?= input_hidden('parameter', $parameter) ?>
          <div class="row">
		  <div class="col-lg-3 col-xl-3">
			<div class="form-group">
				<label>Kode Purchase Order</label>
				<input type="text" id="cKodePo" name="input[sales_code]" class="form-control md-form-control md-static" readonly value="<?= $code ?>">
			</div>
		  </div>
		  <div class="col-lg-3 col-xl-3">
			<div class="form-group">
            <label>Tanggal Purchase Order</label>
            <input type="text" id="dTglPo" name="input[sales_date]" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= date('Y-m-d') ?>" required>
          </div>
		  </div>
		  <div class="col-lg-6 col-xl-6">
			<div class="form-group">
            <label>Seller</label>
            <select name="input[seller_id]" id="PilihDistributor" class="form-control md-static" required><option></option></select> 
          </div>
		  </div>
		  </div>
          <div class="form-group">
													<label>Alamat Pengiriman (Isi Jika Berbeda Degan Alamat Seller)</label>
													<textarea class="summernote" name="input[sales_address]"></textarea>
												</div>
		 <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <input type="hidden" name="supplier" value="<?= $this->session->userdata('supplier') ?>">
              <button type="submit" name="simpan" class="btn btn-success waves-effect waves-light" value="simpan">
                SIMPAN
              </button>
              <a href="<?= site_url($url); ?>" class="btn btn-warning waves-effect waves-light">
                BATAL
              </a>
            </div>
        </form>
      </div> 
    </div>
  </div>
</div>
</div>



</div>
</div>
</div>