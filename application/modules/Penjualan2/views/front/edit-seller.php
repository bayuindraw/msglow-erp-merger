<?php 
       foreach ($row as $key =>$vaData) { 
        $cIdMember = $vaData['id_member']; 
        $cMember = $vaData['kode']; 
        $cNama = $vaData['nama']; 
        $cKota = $vaData['kota']; 
        $cRekening = $vaData['rekening']; 
        $cTelepon = $vaData['telepon']; 
        $cInstagram = $vaData['instagram']; 
        $cStatus = $vaData['status']; 
        $cKtp = $vaData['ktp']; 
        $cBukti = $vaData['tf']; 
        $cFoto = $vaData['foto']; 
        $cAlamat = $vaData['alamat']; 
        $cFacebook = $vaData['facebook']; 
        $cTwiiter = $vaData['twitter']; 
        $cWeb = $vaData['id_add']; } 

    ?>
<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Subheader -->
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container kt-container--fluid">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Edit Data Pengiriman Seller</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Edit Data
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="col-sm-12">
                <table id="multi-colum-dt" class="table table-striped table-bordered nowrap">
                  <thead>
                  <tr>
                    <td>No</td>
                    <td>Nama Penerima</td>
                    <td>No Telepon Penerima</td>
                    <td>Alamat Terima</td>
                    <td>#</td>
                  </tr>
                  </thead>
                  <?php 
                    $no = 0;
                    foreach ($penerima as $key => $vaData) {
                  ?>
                  <tr>
                    <td><?=++$no?></td>
                    <td><?=$vaData['nama_terima']?></td>
                    <td><?=$vaData['telepon']?></td>
                    <td><?=$vaData['alamat']?></td>
                    <td></td>
                  </tr>
                <?php 
                    } 
                ?>
                </table>
              </div>
              <br>
              <h4>FORM PENERIMAN & ALAMAT</h4>
              <br>
                <form method="POST" action="<?=base_url()?>Penjualan/AlamatKirim/<?=$cMember?>">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label>Kode Seller</label>
                                <div class="input-group">
                                    <input type="text" id="cMember" name="cMember" class="form-control" placeholder="Id Member" value="<?=$cMember?>" style="text-transform: uppercase;" readonly />
                                </div>
                            </div>
                        </div>
                        <!-- /.col-form -->

                        <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                                <label>NAMA LENGKAP</label>
                                <div class="input-group">
                                    <input type="text" id="cNama" name="cNama" class="form-control" placeholder="Nama Lengkap" value="<?=$cNama?>" style="text-transform: uppercase;" readonly />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                                <label>NO TELEPON</label>
                                <div class="input-group">
                                    <input type="text" id="cTelepon" name="cTelepon" class="form-control" placeholder="No Telepon" value="<?=$cTelepon?>" style="text-transform: uppercase;" readonly />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>NAMA PENERIMA</label>
                                <div class="input-group">
                                    <input type="text" id="cAlamat" name="cNamaKirim" class="form-control" placeholder="NAMA PENERIMA" value="" style="text-transform: uppercase;" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>NO TELEPON</label>
                                <div class="input-group">
                                    <input type="text" id="cAlamat" name="cTelepKirim" class="form-control" placeholder="TELEPON" value="" style="text-transform: uppercase;"  />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label>ALAMAT KIRIM</label>
                                <div class="input-group">
                                    <input type="text" id="cAlamat" name="cAlamatKirim" class="form-control" placeholder="ALAMAT KIRIM" value="" style="text-transform: uppercase;" />
                                </div>
                            </div>
                        </div>
                        <!-- /.col-form -->
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-flat btn-primary" id="mySubmit"><i class="fa fa-check"></i> Tambah Alamat</button>
                        </div>
                    </div>
                    <!-- /.row -->
                </form>
            </div>
        </div>
    </div>
</div>
