<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode</th>
			<th>Tanggal</th>
			<th>Supplier</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",
			scrollY: "300px",
			scrollX: true,
			scrollCollapse: true,
			"processing": true,
			"serverSide": true,
			"ajax": "<?= site_url().'/'.$url.'/get_data/'.$status ?>"
		});
	});
</script>