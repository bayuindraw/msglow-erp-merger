<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ProdukModel extends CI_Model
{
	
	var $table_ = "produk";
	var $id_ = "id_produk";
	var $eng_ = "produk";
	var $url_ = "Produk";
	
	function get($id)
	{
		$data = $this->db->query("SELECT * FROM ".$this->table_." A LEFT JOIN tb_stock_kemasan B ON B.id_barang = A.id_kemasan WHERE A.id_kemasan = '$id'");
		return $data;
	}
	
	public function ViewASC($Table,$Order) {
		$Query = $this->db->query("SELECT * FROM ".$Table." ORDER BY ".$Order." ASC");
		return $Query->result_array();
	}
	
	public function get_product_classification_detail() {
		$Query = $this->db->query("SELECT *, CONCAT(product_classification_detail_name, ' (', product_classification_name, ')') AS name FROM m_product_classification_detail A LEFT JOIN m_product_classification B ON B.product_classification_id = A.product_classification_id ORDER BY B.product_classification_id, A.product_classification_detail_id ASC");
		return $Query->result_array();
	}
	
	public function get_kemasan_bahan() {
		$Query = $this->db->query("SELECT * FROM kemasan A WHERE (product_classification_detail_id > 0) ORDER BY A.id_kemasan ASC");
		return $Query->result_array();
	}
	
	public function get_product_classification(){
		$data = $this->db->get("m_product_classification");
		return $data->result_array();
	}
	
	function get_data()
	{ 
		$arrwhere[] = "(A.status = '1')";
		$table = $this->table_." A LEFT JOIN m_product_classification B ON B.product_classification_id = A.product_classification_id LEFT JOIN m_product_classification2 C ON C.product_classification2_id = B.product_classification2_id";
		$id = $this->id_;
		$field = array('nama_produk', 'product_classification2_name');
		$url = $this->url_;
		$action = '<a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . '/nonactive/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array(); 
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
					  'recordsTotal' => (int)$jum_all['jum'],
					  'recordsFiltered' => (int)$jum_filter['jum'],
					  'data' => @$dataxy
					  );
		
		echo json_encode($data);
	}
	
	function insert($data = array())
	{
		$this->db->insert($this->table_, $data);
		$id = $this->db->insert_id();
		$dat_stock = $_POST['stock'];
		
		$dat = $dat_stock;
		$dat['id_barang'] = $id;
		$dat['tanggal'] = date('Y-m-d');
		$dat['jumlah'] = 0;
		$dat['rusak'] = 0;
		$this->db->insert('tb_stock_kemasan', $dat);
		$dat['tanggal'] = date('Y-m')."-01";
		$this->db->insert('tb_stock_kemasan_history', $dat);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function update($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($this->table_, $data);
		$dat_stock = $_POST['stock'];
		$dat = $dat_stock;
		$this->db->where('id_barang', $value);
		$this->db->update('tb_stock_kemasan', $dat);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($this->table_);
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function nonactive($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$data['tanggal_nonaktif'] = date('Y-m-d H:i:s');
		$data['status'] = '2';
		$this->db->update($this->table_, $data);
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
}
