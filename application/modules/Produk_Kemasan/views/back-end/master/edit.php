<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

  <!-- begin:: Subheader -->
  <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
      <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
          <?= $file ?> </h3>
        <span class="kt-subheader__separator kt-hidden"></span>

      </div>

    </div>
  </div>

  <!-- end:: Subheader -->

  <!-- begin:: Content -->
  <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <form action="<?= base_url() ?>Produk_Kemasan/edit_act" method="post">
      <label for="nama_barang">Nama Barang</label>
      <div class="form-group">
        <div class="form-line">
          <input type="text" name="nama_barang" id="nama_barang" value="<?= $data['nama_barang'] ?>" class="form-control" placeholder="Nama Barang" required="">
        </div>
      </div>

      <label for="qty">QTY (PCS)</label>
      <div class="form-group">
        <div class="form-line">
          <input type="number" name="qty" id="qty" value="<?= $data['qty'] ?>" class="form-control" placeholder="QTY (PCS)" required="">
        </div>
      </div>

      <label for="supplier">Supplier</label>
      <div class="form-group">
        <div class="form-line">
          <input type="text" name="supplier" id="supplier" value="<?= $data['supplier'] ?>" class="form-control" placeholder="Supplier" required="">
        </div>
      </div>

      <label for="keterangan">Keterangan</label>
      <div class="form-group">
        <div class="form-line">
          <input type="text" name="keterangan" id="keterangan" value="<?= $data['keterangan'] ?>" class="form-control" placeholder="Keterangan" required="">
        </div>
      </div>

      <input type="number" name="no" value=<?= $data['no'] ?> hidden>

      <input type="text" name="tanggal_input" value="<?php echo date("Y-m-d"); ?>" hidden>

      <button type="submit" class="btn btn-primary m-t-15 waves-effect">Edit</button>
      <a href="<?php echo base_url('Produk_Kemasan') ?>" class="btn btn-danger">Back</button></a>
    </form>
  </div>

  <!-- end:: Content -->
</div>