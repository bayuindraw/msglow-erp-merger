<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Produk_bundle extends CI_Controller
{

	var $url_ = "Produk_bundle";
	var $id_ = "product_bundle_id";
	var $eng_ = "produk bundling";
	var $ind_ = "Produk Bundling";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data($status)
	{
		$datacontent['datatable'] = $this->Model->get_data($status);
	}

	public function tampil_isi()
	{
		$data['url'] = $this->url_;
		$data['status'] = $this->input->post('status');

		$this->load->view($this->url_.'/isi_table', $data);
	}

	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['id'] = $id;
		$products = $this->db->get('produk_global')->result_array();

		$datacontent['dtproduk'] = $products;
		$datacontent['arrproduct'] = '';
		foreach ($products as $product) {
			$datacontent['arrproduct'] .= '<option value="' . $product['kode'] . '">' . $product['nama_produk'] . '</option>';
		}
		if ($parameter=="ubah") {
			$datacontent['row'] = $this->Model->get_table_where("*","m_product_bundle", "product_bundle_id = '$id'")->result_array();
			$datacontent['detail'] = $this->Model->get_table_where("*","m_product_bundle_detail", "product_bundle_id = '$id'")->result_array();
		}
		// $datacon
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			if ($this->input->post('parameter')!="ubah") {
				$input = $this->input->post('input');
				$data_in = array(
					'product_bundle_name' => $input['product_bundle_name'],
					'product_bundle_code' => $input['product_bundle_code'],
					'product_bundle_create_date' => date('Y-m-d H:i:s'),
					'product_bundle_create_user' => $_SESSION['user_id'],
					'product_bundle_status' => 1
				);
				$this->db->insert('m_product_bundle', $data_in);
				$product_bundle_id = $this->db->insert_id();
				foreach ($this->input->post('input2') as $input2) {
					if (isset($input2['product'])) {
						$dt_prod_global = $this->Model->get_table_where("*","produk_global","kode = '".$input2['product']."'")->result_array();

						$bundle_product = array();
						$bundle_product = ([
							'product_bundle_id' => $product_bundle_id,
							'product_bundle_detail_quantity' => $input2['quantity'],
							'product_bundle_detail_create_date' => date('Y-m-d H:i:s'),
							'product_bundle_detail_create_user' => $_SESSION['user_id'],
							'product_global_id' => $dt_prod_global[0]['id'],
							'product_global_code' => $dt_prod_global[0]['kode'],
							'product_global_name' => $dt_prod_global[0]['nama_produk']
						]);
						$this->db->insert('m_product_bundle_detail', $bundle_product);
					}
				}
			} else {
				$input = $this->input->post('input');
				$data_in = array(
					'product_bundle_name' => $input['product_bundle_name'],
					'product_bundle_code' => $input['product_bundle_code'],
					'product_bundle_update_date' => date('Y-m-d H:i:s'),
					'product_bundle_update_user' => $_SESSION['user_id']
				);
				$where = array('product_bundle_id' => $this->input->post('id'));
				$this->db->update('m_product_bundle', $data_in, $where);
				if (count($this->input->post('input2'))>0) {
					$this->db->delete('m_product_bundle_detail', $where);
				}
				foreach ($this->input->post('input2') as $input2) {
					if (isset($input2['product'])) {
						$dt_prod_global = $this->Model->get_table_where("*","produk_global","kode = '".$input2['product']."'")->result_array();

						$bundle_product = array();
						$bundle_product = ([
							'product_bundle_id' => $this->input->post('id'),
							'product_bundle_detail_quantity' => $input2['quantity'],
							'product_bundle_detail_create_date' => date('Y-m-d H:i:s'),
							'product_bundle_detail_create_user' => $_SESSION['user_id'],
							'product_global_id' => $dt_prod_global[0]['id'],
							'product_global_code' => $dt_prod_global[0]['kode'],
							'product_global_name' => $dt_prod_global[0]['nama_produk']
						]);
						$this->db->insert('m_product_bundle_detail', $bundle_product);
					}
				}
			}

		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function hapus($id = '')
	{
		$data_in = array(
			'product_bundle_status' => 0,
			'product_bundle_update_date' => date('Y-m-d H:i:s'),
			'product_bundle_update_user' => $_SESSION['user_id']
		);
		$where = array('product_bundle_id' => $id);
		$this->db->update('m_product_bundle', $data_in, $where);
		redirect($this->url_);
	}
	
	public function active($id = '')
	{
		$data_in = array(
			'product_bundle_status' => 1,
			'product_bundle_update_date' => date('Y-m-d H:i:s'),
			'product_bundle_update_user' => $_SESSION['user_id']
		);
		$where = array('product_bundle_id' => $id);
		$this->db->update('m_product_bundle', $data_in, $where);
		redirect($this->url_);
	}	

	public function chk_kode_produk($kode)
	{
		$cek = $this->db->get_where('m_product_bundle', ['product_bundle_code' => $kode])->row_array();
		if ($cek == null) {
			echo 'true';
		} else {
			echo 'false';
		}
	}
}
