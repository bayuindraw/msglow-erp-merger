<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk_bundleModel extends CI_Model
{
	
	var $table_ = "m_product_bundle";
	var $id_ = "product_bundle_id";
	var $eng_ = "Produk Bundling";
	var $url_ = "Produk_bundle";
	
	function get($id)
	{
		$data = $this->db->query("SELECT *  FROM ".$this->table_." WHERE product_bundle_id = '$id'")->row_array();
		return $data;
	}
	
	public function ViewASC($Table,$Order) {
		$Query = $this->db->query("SELECT * FROM ".$Table." ORDER BY ".$Order." ASC");
		return $Query->result_array();
	}
	
	function get_data($status)
	{ 
		$arrwhere[] = "(product_bundle_status = '$status')";
		$table = $this->table_."";
		$id = $this->id_;
		$field = array('product_bundle_code', 'product_bundle_name', 'product_bundle_status');
		$url = $this->url_;

		if($_SESSION['role_id'] != 10){
			$action = '<center><a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i><br />Edit</a> <a href="'.site_url($url . '/hapus/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i><br />Non Aktif</a></center>';
		} else {
			if ($status==1) {
				$action = '<center><a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i><br />Edit</a> <a href="'.site_url($url . '/hapus/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i><br />Non Aktif</a></center>';
			} else {
				$action = '<center><a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i><br />Edit</a> <a href="'.site_url($url . '/active/xid').'" class="btn btn-success" onclick="return confirm(\'Aktifkan data?\')"> <i class="fa fa-unlock"></i><br />Aktifkan</a></center>';
			}
		}
		
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array(); 
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				if ($keyfield=="product_bundle_status") {
					if ($valuer[$keyfield]==1) {
						$datax[] = "AKTIF";
					} else {
						$datax[] = "NONAKTIF";						
					}
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);
		
		echo json_encode($data);
	}

	function get_table_where($select, $table, $where)
	{
		$this->db->select($select);
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query;
	}
}
