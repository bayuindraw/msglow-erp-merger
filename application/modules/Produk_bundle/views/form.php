<?php
if ($parameter == 'ubah' && $id != '') {
	//$this->db->where('A.id_kemasan', $id);
	$row = $this->Model->get($id);
}
?>
<div class="row">
	<div class="col-lg-12 col-xl-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Nama Bundling</label>
								<input type="text" class="form-control" placeholder="Nama" name="input[product_bundle_name]" value="<?= @$row['product_bundle_name'] ?>" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Kode Bundling</label>
								<input type="text" autocomplete="off" id="kode_unik_produk" class="form-control" placeholder="Kode" name="input[product_bundle_code]" value="<?= @$row['product_bundle_code'] ?>" required onkeyup="chk_kode_produk();">
							</div>
						</div>
					</div>
					<h5 style="margin-bottom: 10px;">List Produk</h5>
					<?php if($parameter!='ubah'){ ?>
						<div id="isiproduk">
							<?php $index2 = 0 ?>
							<div class="form-group row" id="isiproduk0">
								<div class="col-lg-6">
									<label>Produk</label>
									<select class="form-control pilihProduk" name="input2[0][product]">
										<?= $arrproduct ?>
									</select>
								</div>
								<div class="col-lg-5">
									<label>Jumlah</label>
									<input type="text" class="form-control numeric" placeholder="input jumlah" name="input2[0][quantity]" />
								</div>
								<div class="col-lg-1">
									<button onclick="deleteProduk(0);" class="btn btn-danger pull-right" style="margin-top: 25px;">
										<i class="fas fa-trash"></i>
									</button>
								</div>
							</div>
						</div>
					<?php } else { ?>
						<div id="isiproduk">
							<?php $index2 = count($detail)-1; $no =0; ?>
							<?php foreach ($detail as $d) { ?>
								<div class="form-group row" id="isiproduk<?php echo $no; ?>">
									<div class="col-lg-6">
										<label>Produk</label>
										<select class="form-control pilihProduk" name="input2[<?php echo $no; ?>][product]">
											<?php foreach ($dtproduk as $dp) { ?>
												<option value="<?php echo $dp['kode'] ?>" <?php if($dp['kode']==$d['product_global_code']) echo 'selected'; ?>><?php echo $dp['nama_produk'] ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-lg-5">
										<label>Jumlah</label>
										<input type="text" class="form-control numeric" placeholder="input jumlah" name="input2[<?php echo $no; ?>][quantity]" value="<?php echo $d['product_bundle_detail_quantity'] ?>" />
									</div>
									<div class="col-lg-1">
										<button onclick="deleteProduk(<?php echo $no; ?>);" class="btn btn-danger pull-right" style="margin-top: 25px;">
											<i class="fas fa-trash"></i>
										</button>
									</div>
								</div>
								<?php $no++; ?>
							<?php } ?>
						</div>
					<?php } ?>
					<div class="kt-form__actions">
						<button type="button" class="btn btn-warning" onclick="addProduk();">Tambah Produk</button>
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
							</div>
							<div class="col-lg-6">
								<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary pull-right">Batalkan</span></a>
							</div>
						</div>
					</div>
				</div>
			</form>


		</div>
	</div>
</div>
<script>
	var index2 = <?= $index2 ?>;

	$('.pilihProduk').select2({
		placeholder: "Pilih Produk",
	});

	function chk_kode_produk(){
		$.ajax({
			url: '<?= base_url() ?>/Produk_bundle/chk_kode_produk/'+$('#kode_unik_produk').val(),
			success: function(result) {
				if(result === 'false'){
					document.getElementById('kode_unik_produk').style.color = 'red'
					document.getElementById("simpan").disabled = true;
					document.getElementById('simpan').style.display = 'none'
				}else{
					document.getElementById('kode_unik_produk').style.color = 'black'
					document.getElementById("simpan").disabled = false;
					document.getElementById('simpan').style.display = 'block'
				}
			}
		});
	}

	function deleteProduk(id) {
		$('#isiproduk' + id).remove();
	}

	function addProduk() {
		index2 = index2 + 1;
		var html = '<div class="form-group" id="isiproduk' + index2 + '"><div class="form-group row">    <div class="col-lg-6">    <label>Produk</label>    <select class="form-control pilihProduk" name="input2[' + index2 + '][product]">            <?= $arrproduct ?>        </select>    </div>    <div class="col-lg-5">   <label>Jumlah</label>     <input type="text" class="form-control numeric" placeholder="input jumlah" name="input2[' + index2 + '][quantity]" />    </div>    <div class="col-lg-1"><button onclick="deleteProduk(' + index2 + ');" class="btn btn-danger pull-right" style="margin-top: 25px;">            <i class="fas fa-trash"></i>        </button>    </div></div></div>';
		$('#isiproduk').append(html);
		$('.pilihProduk').select2({
			placeholder: "Pilih Produk",
		});
		$(".numeric").mask("#,##0", {
			reverse: true
		});
	}

</script>