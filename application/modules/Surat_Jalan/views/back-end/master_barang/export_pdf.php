<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="shortcut icon" href="https://msglow.app/upload/nitrogen.png" />
   <title>Print Surat Jalan Barang Jadi</title>
   
<style>
table, th {
  border-collapse: collapse;
  
}
th {
  padding: 15px 0px 15px 0px;
}
td {
   text-align: center;
}
table {
  width: 100%;
}
.right { 
   text-align: right;
}
.border-th{
   border-top: 4px solid;
   border-bottom: 4px solid;
}
.align-left {
   text-align: left;
}
.align-right {
   text-align: right;
   padding-right: 8px;
}
</style>

</head>
<body id="ignorePDF">
   <hr style="border: 2px solid;">
   <h1>SURAT JALAN</h1>
   <h3 style="float:right; margin-top: -50px;"><i>MS GLOW</i></h3>
   <hr style="border: 2px solid;">
   <table>
      <tr>
         <td class="align-left">Nomor    : </td>
         <td class="align-left">Tanggal  : <?= date('d F Y'); ?></td>
      </tr>
      <tr>
         <td class="align-left">Nomor SO :</td>
         <td class="align-left">Up.</td>
      </tr>
      <tr>
         <td class="align-left">Kepada : RIKA WULANDARI</td>
         <td class="align-left">Mata Uang   :  IDR-(Rupiah)</td>
      </tr>
      <tr>
         <td class="align-left"></td>
         <td class="align-left">Term   :  Cash/Tunai</td>
      </tr>
   </table>
   <br>
   <br>
   <table>
      <tr class="border-th align-left">
         <th>NO</th>
         <th>Kode</th>
         <th>Nama Barang</th>
         <th class="align-right">Jumlah</th>
         <th>Unit</th>
      </tr>
      <?php
         $no = 1;
         foreach ($data as $key) {
      ?>
         <tr>
            <td style="width:30px"><?= $no++ ?></td>
            <td class="align-left"><?= $key->kode ?></td>
            <td class="align-left"><?= $key->nama_barang ?></td>
            <td class="align-right"><?= $key->jumlah ?></td>
            <td class="align-left"><?= $key->unit ?></td>
         </tr>
      <?php } ?>
   </table>
   <br>
   <br>
   <br>
   <br>
   <br>
   <p><hr style="border: 2px solid;"></p>
   <div class="ttd2" style="float:left;margin-top: -10px; margin-left: 15%;">
         <p><b><center>RIKA WULANDARI</center></b></p>
         <br><br><br>
         <p style="font-weight: bold;">___________________</p>
         <p><center>OWNER</center></p>
      </div>
   <div class="ttd2" style="float:right;margin-top: -10px; margin-right: 15%;">
         <p><b><center>MS GLOW</center></b></p>
         <br><br><br>
         <p style="font-weight: bold;">_________________</p>
      </div>
</body>
<script>
   var doc = new jsPDF();          
   var elementHandler = {
   '#ignorePDF': function (element, renderer) {
      return true;
   }
   };
   var source = window.document.getElementsByTagName("body")[0];
   doc.fromHTML(
      source,
      15,
      15,
      {
         'width': 180,'elementHandlers': elementHandler
      });

   doc.output("dataurlnewwindow");
</script>
</html>