<?php
defined('BASEPATH') or exit('No direct script access allowed');
class T_coa_transactionModel extends CI_Model
{

	var $table_ = "t_coa_transaction";
	var $id_ = "coa_transaction_id";
	var $eng_ = "journal";
	var $url_ = "T_coa_transaction";

	function get_factory()
	{
		$data = $this->db->get('factory');
		return $data->result_array();
	}

	function get_data_produk()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN t_package_trial_detail_draft B ON B.product_id = A.id_produk AND B.package_id IS NULL";
		$table = "produk A";
		$id = 'id_produk';
		$arrgroup[] = "id_produk";
		$arrorder[] = "COUNT(package_detail_id) DESC";
		$field = array('nama_produk', 'COUNT(package_detail_id)');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_produk_detail/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_seller()
	{
		$arrwhere[] = "(A.package_id IS NULL)";
		$arrjoin[] = "LEFT JOIN member C ON C.kode = A.seller_id";
		$table = "t_package_trial_detail_draft A";
		$id = 'seller_id';
		$arrgroup[] = "seller_id";
		//$arrorder[] = "seller_id";
		$field = array('kode', 'nama', 'kota');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_confirmation/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_seller2()
	{
		$arrwhere[] = "(A.package_id IS NULL)";
		$arrjoin[] = "LEFT JOIN member C ON C.kode = A.seller_id";
		$table = "t_package_trial_detail_draft A";
		$id = 'seller_id';
		$arrgroup[] = "seller_id";
		//$arrorder[] = "seller_id";
		$field = array('kode', 'nama', 'kota');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_confirmation2/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_package_trial_detail_draft()
	{
		$data = $this->db->query("SELECT A.package_detail_id, A.product_id, D.nama_produk, A.package_detail_quantity, GROUP_CONCAT(C.nama_phl SEPARATOR', ') AS pegawai FROM t_package_trial_detail_draft A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id LEFT JOIN produk D ON D.id_produk = A.product_id WHERE A.package_id = 238 GROUP BY A.package_detail_id")->result_array();
		return $data;
	}

	function get_package_trial_detail_pure($id)
	{
		$data = $this->db->query("SELECT * FROM t_package_trial_detail A WHERE A.package_id = '$id'")->result_array();
		return $data;
	}

	function get_package_trial_detail($id)
	{
		$data = $this->db->query("SELECT A.package_detail_id, A.product_id, D.nama_produk, A.package_detail_quantity, GROUP_CONCAT(C.nama_phl SEPARATOR', ') AS pegawai FROM t_package_trial_detail A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id LEFT JOIN produk D ON D.id_produk = A.product_id WHERE A.package_id = '$id' GROUP BY A.package_detail_id")->result_array();
		return $data;
	}

	function get_package_trial_detail2()
	{
		$data = $this->db->query("SELECT A.package_detail_id, A.product_id, D.nama_produk, A.package_detail_quantity, GROUP_CONCAT(C.nama_phl SEPARATOR', ') AS pegawai FROM t_package_trial_detail A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id LEFT JOIN produk D ON D.id_produk = A.product_id WHERE A.package_id = 238 GROUP BY A.package_detail_id")->result_array();
		return $data;
	}

	function get_data_sales()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN m_seller B ON B.seller_id = A.seller_id";
		$table = "t_sales A";
		$id = 'sales_id';
		$field = array('sales_code', 'seller_code', 'seller_name', 'sales_date', 'sales_status');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_package_trial($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN member B ON B.kode = A.member_code WHERE A.package_id = '$id'")->row_array();
		return $arrdata;
	}

	function get_package_trial_detail3($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial_detail_draft B LEFT JOIN member C ON C.kode = B.seller_id WHERE B.product_id = '$id' AND B.package_id IS NULL")->result_array();
		return $arrdata;
	}

	function get_package_trial_detail_draft2($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial_detail_draft B LEFT JOIN produk C ON C.id_produk = B.product_id WHERE B.seller_id = '$id' AND B.package_id IS NULL")->result_array();
		return $arrdata;
	}

	function insert_reset3($id)
	{
		$this->db->query("DELETE FROM t_package_trial_detail_draft WHERE product_id = '$id' AND package_id IS NULL");
		//$this->db->query("DELETE FROM t_package_trial_detail_draft WHERE product_id = '$id'");
		$this->db->query("DELETE FROM t_package_trial_employee WHERE product_id = '$id'");
		return '';
	}

	function update_confirm($id, $id2)
	{
		$this->db->query("UPDATE t_package_trial_detail_draft SET package_id = '$id' WHERE seller_id = '$id2'");
		return '';
	}

	function insert_reset($id)
	{
		$this->db->query("DELETE FROM t_package_trial_detail_draft WHERE package_id = '$id'");
		$this->db->query("DELETE FROM t_package_trial_employee WHERE package_id = '$id'");
		return '';
	}

	function get_package_trial_employee3($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial_detail_draft A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.product_id = '$id' AND A.package_id IS NULL")->result_array();
		return $arrdata;
	}

	function get_package_trial_employee4($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial_detail_draft A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.seller_id = '$id' AND A.package_id IS NULL")->result_array();
		return $arrdata;
	}

	function get_package_trial_employee($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_employee B ON B.package_id = A.package_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.package_id = '$id'")->result_array();
		return $arrdata;
	}

	function get_data_postage()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN t_sales B ON B.sales_id = A.sales_id";
		$arrjoin[] = "LEFT JOIN m_seller C ON C.seller_id = B.seller_id";
		$table = "t_package A";
		$id = 'package_id';
		$field = array('package_code', 'sales_code', 'seller_name', 'sales_address', 'package_postage');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_postage/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_package($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package WHERE package_id = '" . $id . "'")->row_array();
		return $arrdata;
	}

	function get_package_all($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package A LEFT JOIN t_sales B ON B.sales_id = A.sales_id LEFT JOIN m_seller C ON C.seller_id = B.seller_id LEFT JOIN m_account D ON D.seller_id = C.seller_id WHERE A.package_id = '" . $id . "'")->row_array();
		return $arrdata;
	}

	function get_sales_detail($id)
	{
		$arrdata = $this->db->query("SELECT SUM(total) as total, nama_produk, id_produk, sales_id FROM v_pack_product A WHERE sales_id = '" . $id . "' GROUP BY A.id_produk")->result_array();
		return $arrdata;
	}

	function get_package_detail_date($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_detail A LEFT JOIN t_package B ON B.package_id = A.package_id WHERE sales_id = '" . $id . "' GROUP BY package_date")->result_array();
		return $arrdata;
	}

	function get_package_detail_sum($id)
	{
		$arrdata = $this->db->query("SELECT SUM(package_detail_quantity) AS total, product_id, package_date FROM t_package_detail A LEFT JOIN t_package B ON B.package_id = A.package_id WHERE sales_id = '" . $id . "' GROUP BY package_date, product_id")->result_array();
		foreach ($arrdata as $key => $value) {
			$arrdatax[$value['package_date']][$value['product_id']] = $value['total'];
		}
		return @$arrdatax;
	}

	function get_package_detail($id)
	{
		$this->db->where('sales_id', $id);
		$this->db->join('t_package', 't_package.package_id = t_package_detail.package_id');
		$this->db->join('m_user', 'm_user.user_id = t_package.user_id');
		$this->db->join('produk', 'produk.id_produk = t_package_detail.product_id');
		$data = $this->db->get('t_package_detail');
		return $data->result_array();
	}

	function get_sales($id)
	{
		$this->db->where('sales_id', $id);
		$data = $this->db->get('t_sales');
		return $data->row_array();
	}

	function get()
	{
		$data = $this->db->get($this->table_);
		return $data;
	}

	function search_member($search)
	{
		$search = str_replace('%20', ' ', $search);
		//echo "SELECT nama, kode, kota FROM member WHERE nama LIKE '%$search%' OR kode LIKE '%$search%'";
		//sdie(); 
		$data = $this->db->query("SELECT nama, kode, kota FROM member WHERE nama LIKE '%$search%' OR kode LIKE '%$search%'")->result_array();
		$dat = array();
		foreach ($data as $index => $value) {
			$dat[$index] = array();
			$dat[$index]['id'] = $value["kode"];
			$dat[$index]['text'] = $value["nama"] . ' (' . $value["kode"] . ') (' . $value["kota"] . ')';
			//echo json_encode($dat[$index]);
		}
		$array = array(
			'results' => $dat,
			'pagination' => array('more' => true)
		);
		echo json_encode($array);
	}

	function search_member2($search)
	{
		$data = $this->db->query("SELECT nama, kode, kota FROM member WHERE nama LIKE '%$search%' OR kode LIKE '%$search%'")->result_array();
		echo '{
			 "results": [';
		$i = 0;
		foreach ($data as $index => $value) {
			if ($i > 0) {
				echo ',';
			}
			echo '
				{
					"id": "' . $value["kode"] . '",
					"text": "' . $value["nama"] . ' (' . $value["kode"] . ') (' . $value["kota"] . ')"
				}';
			$i++;
		}
		echo '],
  "pagination": {
    "more": true
  }
}';
	}

	function get_member2($id)
	{
		$this->db->where('kode', $id);
		$data = $this->db->get('member');
		return $data->row_array();
	}

	function get_member()
	{
		$data = $this->db->query("SELECT nama, kode FROM member LIMIT 10");
		return $data->result_array();
	}

	function get_seller()
	{
		$data = $this->db->get('m_seller');
		return $data->result_array();
	}

	function get_phl()
	{
		$data = $this->db->get('tb_phl');
		return $data->result_array();
	}

	function get_employee()
	{
		$data = $this->db->get('m_employee');
		return $data->result_array();
	}

	function get_produk_row($id)
	{
		//$this->db->join('tb_stock_produk', 'tb_stock_produk.id_stock = produk.id_produk');
		$data = $this->db->query("SELECT *, A.nama_produk FROM produk A LEFT JOIN tb_stock_produk B ON B.id_barang = A.id_produk WHERE A.id_produk = '$id'");
		return $data->row_array();
	}

	function get_produk()
	{
		//$this->db->join('tb_stock_produk', 'tb_stock_produk.id_stock = produk.id_produk');
		$data = $this->db->query('SELECT *, A.nama_produk FROM produk A LEFT JOIN tb_stock_produk B ON B.id_barang = A.id_produk');
		return $data->result_array();
	}

	function insert_id()
	{
		return $this->db->insert_id();
	}

	function get_product()
	{
		$data = $this->db->get('produk');
		return $data->result_array();
	}

	function get_data($tgl_awal, $tgl_akhir)
	{
		if ($tgl_awal != "" && $tgl_akhir != "") {
			$arrwhere[] = "(A.coa_date BETWEEN '$tgl_awal' AND '$tgl_akhir')";
		} else {
			//$tgl_awal = date('Y-m-d', strtotime(date('Y-m-d'). ' - 1 days'));
			$tgl_awal = date('Y-m-d');
			$tgl_akhir = date('Y-m-d');
			$arrwhere[] = "(A.coa_date BETWEEN '$tgl_awal' AND '$tgl_akhir')";
		}
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN coa_4 B ON B.id = A.coa_id";
		$arrjoin[] = "LEFT JOIN coa_3 C ON C.id = A.coa_id";
		$arrjoin[] = "LEFT JOIN coa_2 D ON D.id = A.coa_id";
		$arrjoin[] = "LEFT JOIN coa_1 E ON E.id = A.coa_id";
		$arrjoin[] = "LEFT JOIN coa_5 F ON F.id = A.coa_id";
		$table = $this->table_ . " A";
		$id = $this->id_;
		//$arrorder2[] = 'package_status_id ASC';
		$arrorder2 = array();
		$arrorder2[] = 'A.coa_date ASC, A.coa_transaction_id ASC';
		$field = array('CASE WHEN A.coa_level = 4 THEN B.nama WHEN A.coa_level = 3 THEN C.nama WHEN A.coa_level = 2 THEN D.nama WHEN A.coa_level = 1 THEN E.nama WHEN A.coa_level = 5 THEN F.nama END coa_name', 'CASE WHEN A.coa_level = 4 THEN B.kode WHEN A.coa_level = 3 THEN C.kode WHEN A.coa_level = 2 THEN D.kode WHEN A.coa_level = 1 THEN E.kode WHEN A.coa_level = 5 THEN F.kode END coa_code', 'coa_date', 'FORMAT(coa_debit, 2) AS coa_debit', 'FORMAT(coa_credit, 2) AS coa_credit', 'coa_transaction_note');
		$field2 = array('coa_date', 'coa_name', 'coa_code', 'coa_debit', 'coa_credit', 'coa_transaction_note');
		$field3 = array('B.nama', 'C.nama', 'D.nama', 'E.nama', 'F.nama', 'B.kode', 'C.kode', 'D.kode', 'E.kode', 'F.kode', 'coa_date', 'coa_debit', 'coa_credit', 'coa_transaction_note');

		$jfield = join(', ', $field);

		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field3 as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach ($arrorder2 as $index => $value) {
			$arrorder[] = $value;
		}
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}

		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		//echo "SELECT $id, $jfield FROM $table $join $where $order $limit";
		//die();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT A.coa_group_id, $id, $jfield FROM $table $join $where $order $limit")->result_array();

		$i = $start;
		$i++;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$header[$keyr] = $valuer['coa_group_id'];
			$datax = array();
			if ($keyr > 0) {
				if ($header[$keyr] == $header[$keyr - 1]) {
					$datax[] = "";
				} else {
					$datax[] = $i;
					$i++;
				}
			} else {
				$datax[] = $i;
				$i++;
			}
			foreach ($field2 as $keyfield) {
				if ($keyfield == 'package_status_id') {
					$datax[] = (($valuer[$keyfield] == "1") ? "Terkonfirmasi" : "Draft");
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			$dataxy[] = $datax;
		}
		foreach ($dataxy as $index => $dataxx) {
			if ($index > 0) {
				if ($header[$index] == $header[$index - 1]) {
					$dataxy[$index][1] = "";
					$dataxy[$index][6] = "";
				}
			}
			$dataxy[$index][4] = "Rp " . $dataxy[$index][4];
			$dataxy[$index][5] = "Rp " . $dataxy[$index][5];
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_jurnal($tgl_awal, $tgl_akhir)
	{
		if ($tgl_awal != "" && $tgl_akhir != "") {
			$arrwhere[] = "(A.coa_transaction_date BETWEEN '$tgl_awal' AND '$tgl_akhir')";
		} else {
			//$tgl_awal = date('Y-m-d', strtotime(date('Y-m-d'). ' - 1 days'));
			$tgl_awal = date('Y-m-d');
			$tgl_akhir = date('Y-m-d');
			$arrwhere[] = "(A.coa_transaction_date BETWEEN '$tgl_awal' AND '$tgl_akhir')";
		}
		$table = "t_coa_transaction_header A";
		$id = "A.coa_transaction_header_id";
		//$arrorder2[] = 'package_status_id ASC';
		$arrorder2 = array();
		$field = 'A.*';
		$field3 = array('A.coa_transaction_date', 'A.coa_transaction_debit', 'A.coa_transaction_credit', 'A.coa_transaction_payment');
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field3 as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach ($arrorder2 as $index => $value) {
			$arrorder[] = $value;
		}
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}

		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		//echo "SELECT $id, $jfield FROM $table $join $where $order $limit";
		//die();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $where")->row_array();
		echo "SELECT $field FROM $table $where $order $limit";
		die;
		$result = $this->db->query("SELECT $field FROM $table $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			$dataxy[] = $datax;
		}
		// print_r($dataxy);die;
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_safeconduct()
	{
		$arrdata = $this->db->query("SELECT * FROM m_safeconduct")->result_array();
		return $arrdata;
	}

	function get_package_safeconduct($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_safeconduct A LEFT JOIN m_safeconduct B ON B.safeconduct_id = A.safeconduct_id WHERE A.package_id = '$id'")->result_array();
		return $arrdata;
	}

	function get_data_safeconduct()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN member B ON B.kode = A.member_code";
		$table = $this->table_ . " A";
		$id = $this->id_;
		$arrorder2[] = 'package_date DESC';
		$field = array('package_code', 'nama', 'package_date');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_detail2/xid") . '" class="btn btn-danger"> <i class="fa fa-list"></i></a> <a href="' . site_url($url . "/cetaksuratjalankemasan2/xid") . '" class="btn btn-warning"> <i class="fa fa-print"></i></a> <a href="' . site_url($url . "/form_safeconduct/xid") . '" class="btn btn-success"> <i class="fa fa-edit"></i></a> <a href="' . site_url($url . "/cetaksuratjalandriver/xid") . '" class="btn btn-success" target="_blank"> <i class="fa fa-print"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach ($arrorder2 as $index => $value) {
			$arrorder[] = $value;
		}
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}

		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		//echo "SELECT $id, $jfield FROM $table $join $where $order $limit";
		//die();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_report($search = '')
	{
		if ($search != "") {
			$arrsearch = explode("%7C", $search);
			$arrwhere[] = "A.member_status_id = '" . $arrsearch[0] . "'";
			$arrwhere[] = "(A.member_code LIKE '%" . $arrsearch[1] . "%' OR A.member_name LIKE '%" . $arrsearch[1] . "%' OR A.member_phone LIKE '%" . $arrsearch[1] . "%' OR A.member_address LIKE '%" . $arrsearch[1] . "%' OR D.nama_kecamatan LIKE '%" . $arrsearch[1] . "%' OR C.nama_kota LIKE '%" . $arrsearch[1] . "%')";
		}
		$arrwhere[] = "A.member_code != ''";
		$arrjoin[] = "LEFT JOIN mst_kecamatan D ON D.id_kecamatan = A.district_id AND D.id_kota = A.city_id";
		$arrjoin[] = "LEFT JOIN mst_kota C ON C.id_kota = A.city_id";
		$arrjoin[] = "LEFT JOIN m_member_status B ON B.member_status_id = A.member_status_id";
		$table = $this->table_;
		$id = $this->id_;
		$field = array('member_code', 'member_name', 'member_phone', 'nama_kota', 'member_status_name', 'member_phone', 'member_date');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form/ubah/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="' . site_url($url . "/transfer/tambah/xid") . '" class="btn btn-info"> <i class="fa fa-list"></i></a> <a href="' . site_url($url . "/send/tambah/xid") . '" class="btn btn-info"> <i class="fa fa-download"></i></a> <a href="' . site_url($url . '/hapus/xid') . '" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}



	function get_detail($id)
	{
		$this->db->where($this->id_, $id);
		$this->db->join('m_account_detail_category B', 'B.account_detail_category_id = A.account_detail_category_id');
		$data = $this->db->get($this->table2_ . ' A');
		return $data;
	}

	function get_data_detail($idxx)
	{
		$table = $this->table2_ . ' A';
		$id = "CONCAT(account_id, '/', account_detail_id) AS id";
		$field = array('account_detail_category_name', 'account_detail_pic', 'account_detail_note', 'account_detail_debit', 'account_detail_credit', 'account_detail_realization');
		$arrjoin[] = 'LEFT JOIN m_account_detail_category B ON B.account_detail_category_id = A.account_detail_category_id';
		$url = $this->url_;
		$arrwhere[] = "account_id = $idxx";
		$action = '<a href="' . site_url($url . "/form_detail/ubah/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="' . site_url($url . '/hapus_detail/xid') . '" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(account_detail_id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(account_detail_id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer['id'], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function insert($data = array())
	{
		$this->db->insert($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_terima_produk($data = array())
	{
		$this->db->insert('terima_produk', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_package_safeconduct($data = array())
	{
		$this->db->insert('t_package_safeconduct', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_detail_draft($data = array())
	{
		$this->db->insert($this->table_ . "_detail_draft", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_detail($data = array())
	{
		$this->db->insert($this->table_ . "_detail", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_employee($data = array())
	{
		$this->db->insert($this->table_ . "_employee", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_employee_real($data = array())
	{
		$this->db->insert($this->table_ . "_employee_real", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_account($data = array())
	{
		$this->db->insert('m_account', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($this->table_);
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}

	function delete_package_safeconduct($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete('t_package_safeconduct');
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update_table($table, $data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($table, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update_detail($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update('t_account_detail', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete_detail($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete('t_account_detail');
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}

	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}

	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}

	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}

	function get_max_id()
	{
		$data = $this->db->query("SELECT (IFNULL(MAX(account_detail_id), 0) + 1) as id FROM t_account_detail")->row_array();
		return $data['id'];
	}

	function get_account_id($id)
	{
		$data = $this->db->query("SELECT account_id FROM m_account WHERE seller_id = '$id'")->row_array();
		return $data['account_id'];
	}

	function insert_account_detail($data = array())
	{
		$this->db->insert("t_account_detail", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
		return $this->db->insert_id();
	}

	function get_m_account()
	{
		$data = $this->db->get('m_account');
		return $data;
	}

	function update_balance($id, $balance, $type, $account_type = '', $monthly = '')
	{
		if ($type == "credit") {
			if ($monthly == '') {
				$this->db->query("UPDATE m_account SET account_credit = (account_credit + $balance) WHERE account_id = " . $id);
			} else {
				if ($account_type == '1') {
					$this->db->query("UPDATE m_account SET account_debit = (account_debit - ($balance)), account_monthly_debit = (account_monthly_debit - ($balance)) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit - ($balance)) WHERE account_reset_date > '$monthly'");
				} else {
					$this->db->query("UPDATE m_account SET account_credit = (account_credit + $balance), account_monthly_credit = (account_monthly_credit + $balance) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		} else if ($type == "debit") {
			if ($monthly == '') {
				$this->db->query("UPDATE m_account SET account_debit = (account_debit + $balance) WHERE account_id = " . $id);
			} else {
				if ($account_type == '2') {
					$this->db->query("UPDATE m_account SET account_credit = (account_credit - ($balance)), account_monthly_credit = (account_monthly_credit - ($balance)) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit - ($balance)) WHERE account_reset_date > '$monthly'");
				} else {
					$this->db->query("UPDATE m_account SET account_debit = (account_debit + $balance), account_monthly_debit = (account_monthly_debit + $balance) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}
		return '';
	}

	public function update_masuk($jml, $tanggal, $id_barang)
	{
		$Query = $this->db->query("UPDATE tb_stock_produk SET jumlah = (jumlah + " . $jml . ") WHERE id_barang = '" . $id_barang . "'");
		$Query = $this->db->query("UPDATE tb_stock_produk_history SET jumlah = (jumlah + " . $jml . ") WHERE id_barang = '" . $id_barang . "' AND tanggal > '" . $tanggal . "'");
		return $Query;
	}

	public function update_keluar($jml, $tanggal, $id_barang)
	{
		$Query = $this->db->query("UPDATE tb_stock_produk SET jumlah = (jumlah - " . $jml . ") WHERE id_barang = '" . $id_barang . "'");
		$Query = $this->db->query("UPDATE tb_stock_produk_history SET jumlah = (jumlah - " . $jml . ") WHERE id_barang = '" . $id_barang . "' AND tanggal > '" . $tanggal . "'");
		return $Query;
	}
}
