<?php
defined('BASEPATH') or exit('No direct script access allowed');


class T_coa_user extends CI_Controller
{
	var $url_ = "T_coa_user";
	var $id_  = "coa_id";
	var $eng_ = "COA";
	var $ind_ = "Kas Pengeluaran";
	var $ind2_ = "List Realisasi";
	var $ind3_ = "Transfer Antar Akun";
	var $ind4_ = "Riwayat Pengeluaran Kas";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_history()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind4_;
		$data['file'] = $this->ind4_;
		$data['content'] = $this->load->view($this->url_ . '/table_history', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_realization()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		$non_realisasi = $this->db->query("SELECT nama, coa_transaction_date, coa_debit FROM t_coa_transaction_header A JOIN t_coa_transaction B ON B.coa_transaction_source_id = A.coa_transaction_header_id AND B.coa_transaction_source = 4 AND B.coa_debit > 0 AND B.coa_transaction_realization = 0 JOIN coa_4 C ON C.id = B.coa_id WHERE A.coa_transaction_realization = 0")->result_array();
		$datacontent['non_realisasi'] = count($non_realisasi);
		$realisasi = $this->db->query("SELECT nama, coa_transaction_date, coa_debit FROM t_coa_transaction_header A JOIN t_coa_transaction B ON B.coa_transaction_source_id = A.coa_transaction_header_id AND B.coa_transaction_source = 4 AND B.coa_debit > 0 AND B.coa_transaction_realization = 1 JOIN coa_4 C ON C.id = B.coa_id WHERE A.coa_transaction_realization = 1 AND SUBSTR(coa_transaction_date, 1, 7) = '" . date('Y-m') . "'")->result_array();
		$datacontent['realisasi'] = count($realisasi);
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/table_realization', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function get_data_history($tgl_awal = '', $tgl_akhir = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_history($tgl_awal, $tgl_akhir);
	}

	public function get_data_realization()
	{
		$datacontent['datatable'] = $this->Model->get_data_realization();
	}

	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['akun'] = $this->db->get_where('coa_4', ['id' => $parameter])->row_array();
		$akuns = $this->db->query("SELECT id_4, nama_4, kode_4 FROM v_coa_234 WHERE kode_2 LIKE '600.%'")->result_array();
		$user = $this->db->query('SELECT id_pegawai, nama from tb_pegawai')->result_array();
		$datacontent['arrakun'] = "<option></option>";
		foreach ($akuns as $index => $value) {
			$datacontent['arrakun'] .= '<option value="' . $value['id_4'] . '">' . $value['nama_4'] . '</option>';
		}
		$datacontent['user'] = $user;
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_realization($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['akunSumber'] = $this->db->query("SELECT * FROM t_coa_transaction WHERE coa_group_id = $parameter AND (coa_code LIKE '%100.1.2%' OR coa_code LIKE '%100.1.1%')")->row_array();
		$datacontent['akunPic'] = $this->db->query("SELECT * FROM t_coa_transaction WHERE coa_group_id = $parameter AND coa_code LIKE '%100.1.4%'")->row_array();
		$datacontent['details'] = $this->db->get_where('t_coa_transaction_temp', ['coa_group_id' => $parameter])->result_array();
		$datacontent['title'] = $datacontent['akunSumber']['coa_name'] . ' | ' . $datacontent['akunSumber']['coa_code'];
		$datacontent['parameter'] = $parameter;
		$akuns = $this->db->query("SELECT id_4, nama_4, kode_4 FROM v_coa_234 WHERE kode_2 LIKE '600.%'")->result_array();
		$datacontent['arrakunx'] = "<option></option>";
		foreach ($datacontent['details'] as $index2 => $detail) {
			$datacontent['arrakun'][$index2] = '';
		}
		foreach ($akuns as $index => $value) {
			$datacontent['arrakunx'] .= '<option value="' . $value['id_4'] . '">' . $value['nama_4'] . '</option>';
			foreach ($datacontent['details'] as $index2 => $detail) {
				if ($detail['coa_id'] == $value['id_4']) {
					$datacontent['arrakun'][$index2] .= '<option value="' . $value['id_4'] . '" selected >' . $value['nama_4'] . '</option>';
				} else {
					$datacontent['arrakun'][$index2] .= '<option value="' . $value['id_4'] . '">' . $value['nama_4'] . '</option>';
				}
			}
		}
		$akuns2 = $this->db->query("SELECT * FROM coa_4 WHERE nama LIKE '%BCA%' OR nama LIKE '%mandiri%' OR nama LIKE '%kas kecil%'")->result_array();
		$datacontent['arrakun2'] = "<option></option>";
		foreach ($akuns2 as $index => $value) {
			$datacontent['arrakun2'] .= '<option value="' . $value['id'] . '">' . $value['nama'] . '</option>';
		}
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_realization', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_produk_hpp($id = '')
	{
		$datacontent['url'] = $this->url_;

		$datacontent['id'] = $id;
		$datacontent['arrfactory'] = $this->Model->get($id)->row_array();
		$datacontent['title'] = 'Data HPP Barang ' . $this->ind_ . ' ' . $datacontent['arrfactory']['nama_factory'];
		$datacontent['arrfactory_product'] = $this->Model->get_factory_product($id);
		$datacontent['arrproduct'] = $this->Model->get_produk();
		$datacontent['product_list'] = "";
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ')</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_produk_hpp', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan_produk_hpp()
	{
		if ($this->input->post('simpan')) {
			$data2 = $this->input->post('input2');
			$id = $_POST['id'];
			$this->Model->delete_product_hpp(['factory_id' => $id]);
			foreach ($data2 as $index => $value) {
				$value['factory_product_hpp'] = str_replace(',', '', $value['factory_product_hpp']);
				$data = $value;
				$data['factory_id'] = $id;
				$exec = $this->Model->insert_product_hpp($data);
			}
		}
		redirect(site_url($this->url_));
	}

	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {

				$from['account_detail_id'] = $id;
				$from['account_detail_debit'] = 0;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_id'] = $from['account_id'];
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($from['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}

				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_debit'] = $from['account_detail_credit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_send()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function form_detail($parameter = '', $id = '', $id_detail = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account_detail_category'] = $this->Model->get_list_account_detail_category();
		$datacontent['id'] = $id;
		$datacontent['id_detail'] = $id_detail;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan()
	{
		$this->db->trans_begin();
		$sumber = $this->db->get_where('coa_4', ['id' => $_POST['parameter']])->row_array();
		$pegawai = $this->db->get_where('tb_pegawai', ['id_pegawai' => $_POST['input']['pic']])->row_array();
		$pic = $this->db->query("SELECT * FROM coa_4 WHERE nama = 'Piutang Karyawan $pegawai[nama]'")->row_array();
		$nominal = 0;
		foreach ($_POST['input2'] as $input2) {
			$nominal += $input2['nominal'];
		}
		$coaHeader = ([
			'coa_transaction_date' => $_POST['input']['tanggal'],
			'date_create' => date('Y-m-d H:m:s'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_debit' => $nominal,
			'coa_transaction_credit' => $nominal
		]);
		$this->db->insert('t_coa_transaction_header', $coaHeader);
		$idHeader = $this->db->insert_id();
		if (isset($_POST['input']['is_biaya_admin'])) {
			$nominal += $_POST['input']['biaya_admin'];
			$biayaAdmin = $this->db->query("SELECT * FROM coa_4 WHERE nama = 'Biaya Administrasi Bank $sumber[nama]'")->row_array();
			if ($biayaAdmin == null) {
				$count = $this->db->query("SELECT * FROM `coa_4` where nama LIKE '%Biaya Administrasi Bank%'")->result_array();
				$biayaAdmin = ([
					'kode' => '600.3.1.' . (count($count) + 1),
					'nama' => 'Biaya Administrasi Bank ' . $sumber['nama'],
					'coa3_id' => 174,
				]);
				$this->db->insert('coa_4', $biayaAdmin);
				$idBiayaAdmin = $this->db->insert_id();
				$biayaAdmin = $this->db->get_where('coa_4', ['id' => $idBiayaAdmin])->row_array();
				$insertAdmin = ([
					'coa_id' => $idBiayaAdmin,
					'coa_code' => '600.3.1.' . (count($count) + 1),
					'coa_name' => 'Biaya Administrasi Bank ' . $sumber['nama'],
					'coa_level' => 4,
					'date_create' => date('Y-m-d H:i:s'),
				]);
				$this->db->insert('t_coa_total', $insertAdmin);
				$insertAdmin['coa_total_date'] = date('Y-m') . '-01';
				$this->db->insert('t_coa_total_history', $insertAdmin);
			}
			$dataAdmin = ([
				'coa_name' => $biayaAdmin['nama'],
				'coa_code' => $biayaAdmin['kode'],
				'coa_date' => $_POST['input']['tanggal'],
				'coa_level' => 4,
				'coa_debit' => $_POST['input']['biaya_admin'],
				'coa_credit' => 0,
				'coa_transaction_note' => $pic['nama'] . ' tanggal ' . $_POST['input']['tanggal'],
				'date_create' => date('Y-m-d H:m:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 4,
				'coa_transaction_source_id' => $idHeader,
				'coa_id' => $biayaAdmin['id'],
				'coa_transaction_realization' => 0,
				'coa_group_id' => $idHeader,
			]);
			$this->db->insert('t_coa_transaction', $dataAdmin);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $dataAdmin[coa_debit] WHERE coa_id = 174 AND coa_level = '$dataAdmin[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $dataAdmin[coa_debit] WHERE coa_id = 174 AND REPLACE(coa_total_date, '-', '') < REPLACE($dataAdmin[coa_date], '-', '') AND coa_level = '$dataAdmin[coa_level]'");
		}
		if ($pic == null) {
			$pin = $this->db->query("SELECT (IFNULL(max(coa_code2), 0) + 1) AS JUM FROM tb_pegawai")->row_array();
			$createPic = ([
				'kode' => '100.1.4.' . $pin['JUM'],
				'nama' => 'Piutang Karyawan ' . $pegawai['nama'],
				'coa3_id' => 13,
			]);
			$this->db->insert('coa_4', $createPic);
			$pic = $this->db->get_where('coa_4', ['nama' => 'Piutang Karyawan ' . $pegawai['nama']])->row_array();
		}
		$dataKas = ([
			'coa_name' => $sumber['nama'],
			'coa_code' => $sumber['kode'],
			'coa_date' => $_POST['input']['tanggal'],
			'coa_level' => 4,
			'coa_debit' => 0,
			'coa_credit' => $nominal,
			'coa_transaction_note' => $pic['nama'] . ' tanggal ' . $_POST['input']['tanggal'],
			'date_create' => date('Y-m-d H:m:s'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_source' => 4,
			'coa_transaction_source_id' => $idHeader,
			'coa_id' => $sumber['id'],
			'coa_transaction_realization' => 0,
			'coa_group_id' => $idHeader,
		]);
		$this->db->insert('t_coa_transaction', $dataKas);
		if ($this->db->get_where('t_coa_total', ['coa_id' => $sumber['id']])->row_array() == null) {
			$coaTotal = ([
				'coa_id' => $sumber['id'],
				'coa_code' => $sumber['kode'],
				'coa_total_credit' => 0,
				'coa_total_debit' => 0,
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $sumber['nama']
			]);
			$this->db->insert('t_coa_total', $coaTotal);
			$coaTotal['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $coaTotal);
		}
		$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $dataKas[coa_credit] WHERE coa_id = $sumber[id] AND coa_level = '4'");
		$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $dataKas[coa_credit] WHERE coa_id = $sumber[id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataKas[coa_date], '-', '') AND coa_level = '4'");

		$dataPiutang = ([
			'coa_name' => $pic['nama'],
			'coa_code' => $pic['kode'],
			'coa_date' => $_POST['input']['tanggal'],
			'coa_level' => 4,
			'coa_debit' => $nominal,
			'coa_credit' => 0,
			'coa_transaction_note' => $pic['nama'] . ' tanggal ' . $_POST['input']['tanggal'],
			'date_create' => date('Y-m-d H:m:s'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_source' => 4,
			'coa_transaction_source_id' => $idHeader,
			'coa_id' => $pic['id'],
			'coa_transaction_realization' => 0,
			'coa_group_id' => $idHeader,
		]);
		if (isset($_POST['input']['is_biaya_admin'])) {
			$dataPiutang['coa_debit'] -= $_POST['input']['biaya_admin'];
		}
		$this->db->insert('t_coa_transaction', $dataPiutang);
		$idPiutang = $this->db->insert_id();

		if ($this->db->get_where('t_coa_total', ['coa_id' => $pic['id']])->row_array() == null) {
			$coaTotal = ([
				'coa_id' => $pic['id'],
				'coa_code' => $pic['kode'],
				'coa_total_credit' => 0,
				'coa_total_debit' => 0,
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $pic['nama']
			]);
			$this->db->insert('t_coa_total', $coaTotal);
			$coaTotal['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $coaTotal);
		}

		$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $dataPiutang[coa_debit] WHERE coa_id = $pic[id] AND coa_level = '$dataPiutang[coa_level]'");
		$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $dataPiutang[coa_debit] WHERE coa_id = $pic[id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataPiutang[coa_date], '-', '') AND coa_level = '$dataPiutang[coa_level]'");

		foreach ($_POST['input2'] as $key => $row) {
			$akunBiaya = $this->db->get_where('coa_4', ['id' => $row['akun_id']])->row_array();
			$dataDetail = ([
				'coa_name' => $akunBiaya['nama'],
				'coa_code' => $akunBiaya['kode'],
				'coa_date' => $_POST['input']['tanggal'],
				'coa_level' => 4,
				'coa_debit' => $row['nominal'],
				'coa_credit' => 0,
				'coa_transaction_note' => $row['keterangan'],
				'date_create' => date('Y-m-d H:m:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 5,
				'coa_transaction_source_id' => $idPiutang,
				'coa_id' => $akunBiaya['id'],
				'coa_transaction_realization' => 0,
				'coa_group_id' => $idHeader,
				'coa_transaction_source_type' => 4,
			]);
			$this->db->insert('t_coa_transaction_temp', $dataDetail);
			$insert_id = $this->db->insert_id();
			// if($_FILES['input2'] !== null){
			// 	$path = $_FILES['input2'][$key]['name'];
			// 	$ext = pathinfo($path, PATHINFO_EXTENSION);
			// 	$config['upload_path']          = './upload/BUKTI/';
			// 	$config['allowed_types']        = '*';
			// 	$config['file_name']        = $insert_id;
			// 	$this->load->library('upload', $config);
			// 	$this->db->update('t_coa_transaction_temp', ['coa_transaction_file' => $insert_id . "." . $ext], ['coa_transaction_id' => $insert_id]);
			// }
			// if($this->db->get_where('t_coa_total', ['coa_id' => $akunBiaya['id']])->row_array() == null){
			// 	$coaTotal = ([
			// 		'coa_id' => $akunBiaya['id'],
			// 		'coa_code' => $akunBiaya['kode'],
			// 		'coa_total_credit' => 0,
			// 		'coa_total_debit' => 0,
			// 		'coa_level' => 4,
			// 		'date_create' => date('Y-m-d H:i:s'),
			// 		'coa_name' => $akunBiaya['nama']
			// 	]);
			// 	$this->db->insert('t_coa_total', $coaTotal);
			// 	$this->db->insert('t_coa_total_history', $coaTotal);
			// }

			// $this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $dataDetail[coa_debit] WHERE coa_id = $akunBiaya[id]");
			// $this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $dataDetail[coa_debit] WHERE coa_id = $akunBiaya[id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataDetail[coa_date], '-', '')");
		}
		// }
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		redirect(site_url($this->url_));
	}
	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}

	public function simpan_detail()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$id = $this->input->post('id');
				$id2 = $this->Model->get_max_id();
				$data[$this->id_] = $id;
				$data[$this->id2_] = $id2;
				$data[$this->eng2_ . '_type_id'] = 2;
				$data[$this->eng2_ . '_user_id'] = $_SESSION['user_id'];
				if ($_POST['realisasi'] == "on") {
					$data[$this->eng2_ . '_realization'] = 1;
				} else {
					$data[$this->eng2_ . '_realization'] = 0;
				}
				if ($_POST['transaction_type'] == "debit") {
					$type = "debit";
					$data[$this->eng2_ . '_debit'] = $_POST['transaction_amount'];
				} else if ($_POST['transaction_type'] == "credit") {
					$type = "credit";
					$data[$this->eng2_ . '_credit'] = $_POST['transaction_amount'];
				}
				$data[$this->eng2_ . '_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($data);
				$this->db->where('account_id', $id);
				$row = $this->Model->get()->row_array();
				if (@$row['account_date_reset'] > $data[$this->eng2_ . '_date']) {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_ . '_date']);
				} else {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
				}
			} else {
				$id = $this->input->post('id');
				$data[$this->eng2_ . '_user_id'] = $_SESSION['user_id'];
				if ($_POST['realisasi'] == "on") {
					$data[$this->eng2_ . '_realization'] = 1;
				} else {
					$data[$this->eng2_ . '_realization'] = 0;
				}
				if ($_POST['transaction_type'] == "debit") {
					$type = "debit";
					$data[$this->eng2_ . '_debit'] = $_POST['transaction_amount'];
					$data[$this->eng2_ . '_credit'] = 0;
				} else if ($_POST['transaction_type'] == "credit") {
					$type = "credit";
					$data[$this->eng2_ . '_credit'] = $_POST['transaction_amount'];
					$data[$this->eng2_ . '_debit'] = 0;
				}
				$data[$this->eng2_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update_detail($data, [$this->id_ => $this->input->post('id'), $this->id2_ => $this->input->post('id_detail')]);
				$this->db->where('account_id', $id);
				$row = $this->Model->get()->row_array();
				if (@$row['account_date_reset'] > $this->input->post('transaction_date_last')) {
					$this->Model->update_balance($id,  -1 * ($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'), @$row['account_type_id'], $this->input->post('transaction_date_last'));
				} else {
					$this->Model->update_balance($id,  -1 * ($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'));
				}
				if (@$row['account_date_reset'] > $data[$this->eng2_ . '_date']) {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_ . '_date']);
				} else {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url2_ . "/" . $id));
	}
	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();


		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();

		if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
		} else {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
		}



		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_ . "/$id"));
	}

	public function set_realization()
	{
		$this->db->trans_begin();
		$sumber = $this->db->query("SELECT * FROM t_coa_transaction WHERE coa_group_id = $_POST[parameter] AND (coa_code LIKE '%100.1.2%' OR coa_code LIKE '%100.1.1%')")->row_array();
		$pic = $_POST['input']['pic'];
		$pic = $this->db->query("SELECT * FROM coa_4 WHERE nama = '$pic'")->row_array();
		$this->db->query("UPDATE t_coa_transaction SET coa_transaction_realization = 1, date_update = NOW(), user_update = $_SESSION[user_id] WHERE coa_group_id = $_POST[parameter]");
		$this->db->query("UPDATE t_coa_transaction_temp SET coa_transaction_realization = 1, date_update = NOW(), user_update = $_SESSION[user_id] WHERE coa_group_id = $_POST[parameter]");
		$this->db->query("UPDATE t_coa_transaction_header SET coa_transaction_realization = 1, coa_transaction_realization_date = NOW() WHERE coa_transaction_header_id = $_POST[parameter]");
		$piutang = $this->db->query("SELECT * FROM t_coa_transaction WHERE coa_group_id = $_POST[parameter] AND coa_name LIKE '%piutang karyawan%'")->row_array();
		$kas = $this->db->query("SELECT * FROM t_coa_transaction WHERE coa_group_id = $_POST[parameter] AND (coa_code LIKE '%100.1.2%' OR coa_code LIKE '%100.1.1%')")->row_array();
		foreach ($_POST['input2'] as $detail) {
			$akun = $this->db->get_where('coa_4', ['id' => $detail['akun_id']])->row_array();
			$biaya = ([
				'coa_name' => $akun['nama'],
				'coa_code' => $akun['kode'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_debit' => $detail['nominal'],
				'coa_credit' => 0,
				'coa_transaction_note' => $detail['keterangan'],
				'date_create' => date('Y-m-d'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 5,
				'coa_transaction_source_id' => $piutang['coa_transaction_id'],
				'coa_id' => $akun['id'],
				'coa_transaction_realization' => 1,
				'coa_transaction_source_type' => 4,
				'coa_group_id' => $piutang['coa_group_id'],
			]);
			$this->db->insert('t_coa_transaction', $biaya);
			if ($this->db->get_where('t_coa_total', ['coa_id' => $biaya['coa_id']])->row_array() == null) {
				$coaTotal = ([
					'coa_id' => $biaya['coa_id'],
					'coa_code' => $biaya['coa_code'],
					'coa_total_credit' => 0,
					'coa_total_debit' => 0,
					'coa_level' => 4,
					'date_create' => date('Y-m-d H:i:s'),
					'coa_name' => $biaya['coa_name']
				]);
				$this->db->insert('t_coa_total', $coaTotal);
				$coaTotal['coa_total_date'] = date('Y-m') . '-01';
				$this->db->insert('t_coa_total_history', $coaTotal);
			}
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $biaya[coa_debit] WHERE coa_id = $akun[id] AND coa_level = '4'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $biaya[coa_debit] WHERE coa_id = $akun[id] AND REPLACE(coa_total_date, '-', '') < REPLACE($biaya[coa_date], '-', '') AND coa_level = '4'");
		}

		$piutang['coa_date'] = date('Y-m-d');
		$piutang['coa_credit'] = $piutang['coa_debit'];
		$piutang['coa_debit'] = 0;
		$piutang['coa_transaction_realization'] = 1;
		$piutang['coa_transaction_source_id'] = $piutang['coa_transaction_id'];
		unset($piutang['coa_transaction_id']);

		$this->db->insert('t_coa_transaction', $piutang);
		$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $piutang[coa_credit] WHERE coa_id = $piutang[coa_id] AND coa_level = '$piutang[coa_level]'");
		$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $piutang[coa_credit] WHERE coa_id = $piutang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($piutang[coa_date], '-', '') AND coa_level = '$piutang[coa_level]'");
		// print_r($_POST['input']);die;
		if ($piutang['coa_credit'] > $_POST['input']['total_bayar']) {
			$piutang['coa_credit'] =  ($piutang['coa_credit'] - $_POST['input']['total_bayar'] - @$_POST['input']['biaya_admin']);
			$piutang['coa_debit'] =  0;
			$this->db->insert('t_coa_transaction', $piutang);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $piutang[coa_credit] WHERE coa_id = $piutang[coa_id] AND coa_level = '$piutang[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $piutang[coa_credit] WHERE coa_id = $piutang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($piutang[coa_date], '-', '') AND coa_level = '$piutang[coa_level]'");

			unset($kas['coa_transaction_id']);
			$kas['coa_debit'] = $piutang['coa_credit'];
			$kas['coa_credit'] = 0;
			$kas['coa_transaction_realization'] = 1;
			$this->db->insert('t_coa_transaction', $kas);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $kas[coa_debit] WHERE coa_id = $kas[coa_id] AND coa_level = '$kas[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $kas[coa_debit] WHERE coa_id = $kas[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($kas[coa_date], '-', '') AND coa_level = '$kas[coa_level]'");
		} else if ($piutang['coa_credit'] < $_POST['input']['total_bayar']) {
			$piutang['coa_debit'] =  ($_POST['input']['total_bayar'] - $piutang['coa_credit'] - @$_POST['input']['biaya_admin']);
			$piutang['coa_credit'] =  0;
			$this->db->insert('t_coa_transaction', $piutang);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $piutang[coa_debit] WHERE coa_id = $piutang[coa_id] AND coa_level = '$piutang[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $piutang[coa_debit] WHERE coa_id = $piutang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($piutang[coa_date], '-', '') AND coa_level = '$piutang[coa_level]'");

			unset($kas['coa_transaction_id']);
			$kas['coa_debit'] = 0;
			$kas['coa_credit'] = $piutang['coa_debit'];
			$kas['coa_transaction_realization'] = 1;
			$this->db->insert('t_coa_transaction', $kas);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $kas[coa_credit] WHERE coa_id = $kas[coa_id] AND coa_level = '$kas[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $kas[coa_credit] WHERE coa_id = $kas[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($kas[coa_date], '-', '') AND coa_level = '$kas[coa_level]'");
		}


		if (isset($_POST['is_biaya_admin'])) {
			$biayaAdmin = $this->db->query("SELECT * FROM coa_4 WHERE nama = 'Biaya Administrasi Bank $sumber[coa_name]'")->row_array();
			if ($biayaAdmin == null) {
				$count = $this->db->query("SELECT * FROM `coa_4` where nama LIKE '%Biaya Administrasi Bank%'")->result_array();
				$biayaAdmin = ([
					'kode' => '600.3.1.' . (count($count) + 1),
					'nama' => 'Biaya Administrasi Bank ' . $sumber['coa_name'],
					'coa3_id' => 174,
				]);
				$this->db->insert('coa_4', $biayaAdmin);
				$idBiayaAdmin = $this->db->insert_id();
				$biayaAdmin = $this->db->get_where('coa_4', ['id' => $idBiayaAdmin])->row_array();
				$insertAdmin = ([
					'coa_id' => $idBiayaAdmin,
					'coa_code' => '600.3.1.' . (count($count) + 1),
					'coa_name' => 'Biaya Administrasi Bank ' . $sumber['coa_name'],
					'coa_level' => 4,
					'date_create' => date('Y-m-d H:i:s'),
				]);
				$this->db->insert('t_coa_total', $insertAdmin);
				$insertAdmin['coa_total_date'] = date('Y-m') . '-01';
				$this->db->insert('t_coa_total_history', $insertAdmin);
			}
			$dataAdmin = ([
				'coa_name' => $biayaAdmin['nama'],
				'coa_code' => $biayaAdmin['kode'],
				'coa_date' => $_POST['input']['tanggal'],
				'coa_level' => 4,
				'coa_debit' => $_POST['input']['biaya_admin'],
				'coa_credit' => 0,
				'coa_transaction_note' => $pic['nama'] . ' tanggal ' . $_POST['input']['tanggal'],
				'date_create' => date('Y-m-d H:m:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 4,
				'coa_transaction_source_id' => $piutang['coa_transaction_id'],
				'coa_id' => $biayaAdmin['id'],
				'coa_transaction_realization' => 1,
				'coa_group_id' => $piutang['coa_group_id'],
			]);
			$this->db->insert('t_coa_transaction', $dataAdmin);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $dataAdmin[coa_debit] WHERE coa_id = $biayaAdmin[id] AND coa_level = '$dataAdmin[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $dataAdmin[coa_debit] WHERE coa_id = $biayaAdmin[id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataAdmin[coa_date], '-', '') AND coa_level = '$dataAdmin[coa_level]'");
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('T_coa_user/table_realization');
	}
	public function get_modal_history($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$datacontent['akunPic'] = $this->db->query("SELECT * FROM t_coa_transaction WHERE coa_group_id = $id AND coa_code LIKE '%100.1.4%'")->row_array();
		$datacontent['histories'] = $this->db->query("SELECT A.*, B.nama as nama_coa FROM t_coa_transaction A LEFT JOIN coa_4 B ON A.coa_id = B.id WHERE coa_group_id = $id AND coa_code LIKE '%600.%'")->result_array();
		$this->load->view($this->url_ . '/modal_history', $datacontent);
	}
}
