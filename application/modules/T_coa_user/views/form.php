<div class="row">
	<div class="col-lg-12 col-xl-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__body">
					<div class="form-group">
						<label>Akun</label>
						<input type="text" class="form-control" placeholder="Nama" name="input[akun_nama]" value="<?= @$akun['nama'] ?>" readonly required>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">PIC</label>
						<div class="col-4">
							<select name="input[pic]" id="pilihPIC" class="form-control">
								<option></option>
								<?php
								foreach ($user as $key => $vData) {
								?>
									<option value="<?= $vData['id_pegawai'] ?>"><?= $vData['nama'] ?></option>
								<?php
								}
								?>
							</select>
						</div>
						<label class="col-2 col-form-label">Tanggal</label>
						<div class="col-4">
							<input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="input[tanggal]" readonly value="<?= date('Y-m-d') ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Biaya Admin</label>
						<div class="col-4">
							<label class="kt-checkbox kt-checkbox--success">
								<input type="checkbox" name="input[is_biaya_admin]" onchange="hideBiaya()"> Biaya Admin
								<span></span>
							</label>
						</div>
						<div id="biaya_admin" class="col-6" style="display: none;">
							<input type="number" class="form-control biaya_admin" placeholder="Biaya Admin" id="biaya_admin2" onkeyup="chk_total();" name="input[biaya_admin]" value="<?= @$akun['biaya_admin'] ?>">
						</div>
					</div>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Keterangan</th>
								<th>Akun</th>
								<th>Nominal</th>
								<th width="20%">Bukti</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="x">
							<?php
							$totalx = 0;
							$i = 0;
							?>
							<tr id="tr0">
								<td><input type="text" class="form-control" placeholder="keterangan" name="input2[0][keterangan]" required></td>
								<td><select name="input2[0][akun_id]" class="pilihAkun form-control md-static" required><?= $arrakun ?></select></td>
								<td><input type="number" class="form-control sum_total" placeholder="nominal" onkeyup="chk_total($(this).val()); " name="input2[0][nominal]" required></td>
								</td>
								<td><input type="file" class="form-control" placeholder="bukti" name="input2[0][bukti]"></td>
								<td><button onclick="myDeleteFunction(0);chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
							</tr>
							<?php $i++; ?>
						</tbody>
						<tbody>
							<tr>
								<td>Total Nominal : <span id="total"><?= $totalx ?></span></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();return false();">Tambah Barang</button>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<button type="submit" name="simpan" value="simpan" id="btn_submit" class="btn btn-primary" onsubmit="return confirm('Are you sure?')">Simpan</button>
							<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
						</div>
					</div>
			</form>
		</div>
	</div>
</div>

<script>
	var count = <?= $i; ?>;

	function myCreateFunction() {
		count = count + 1;
		var html = '<tr id="tr' + count + '"><td><input type="text" class="form-control" placeholder="keterangan" name="input2[' + count + '][keterangan]" required></td><td><select name="input2[' + count + '][akun_id]" class="pilihAkun' + count + ' form-control md-static" required><?= $arrakun ?></select></td><td><input type="number" class="form-control sum_total" placeholder="nominal" onkeyup="chk_total($(this).val()); " name="input2[' + count + '][nominal]" required></td></td><td><input type="file" class="form-control" placeholder="bukti" name="input2[' + count + '][bukti]"></td><td><button onclick="myDeleteFunction(' + count + '); chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$('.pilihAkun' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
	}

	function myDeleteFunction(id) {
		$('#tr' + id).remove();
	}

	function chk_total(val) {
		var sum = 0;
		var sum1 = 0;
		var val = 0;
		var val1 = $('#biaya_admin2').val();
		if (val1 == "") val1 = 0;
		$('.sum_total').each(function() {
			val = this.value;
			if (this.value == "") val = 0;
			sum += parseFloat(val);
		});
		sum1 = sum + parseFloat(val1);
		$('#total').html(sum1);
	}

	function hideReal() {
		var x = document.getElementById("real");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}

	function hideBiaya() {
		var x = document.getElementById("biaya_admin");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
</script>