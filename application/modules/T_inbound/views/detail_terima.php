<div class="row">
	<div class="col-lg-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
				<div class="kt-portlet__body">
					<div class="row">

						<div class="col-md-4">
							<div class="form-group">
								<label>Tanggal Diterima</label><br>
								<span><?php echo $dt_produk[0]['incoming_goods_date'] ?></span>
							</div>
						</div>

						<div class="col-md-12">
							<label>Detail Barang</label>
							<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
								<thead>
									<tr>
										<th>No</th>
										<th>Barang</th>
										<th>No. TO</th>
										<th>Jumlah SJ</th>
										<th>Jumlah Diterima</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0; foreach ($dt_produk as $d) { ?>
									<tr>
										<td><?php echo ($i+1) ?></td>
										<td><?php echo $d['product_name'] ?></td>
										<td><?php echo $d['transfer_code'] ?></td>
										<td><?php echo $d['package_detail_quantity'] ?></td>
										<td><?php echo $d['jumlah'] ?></td>
									</tr>
									<?php $i++; } ?>
								</tbody>
							</table>
						</div>


					</div>
				</div>
			</form>

		</div>
	</div>
</div>