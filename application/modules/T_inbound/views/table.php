<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				<?= $title ?>
			</h3>
		</div>
		<!-- <div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					<a href="<?= site_url().$url."/tambah" ?>" class="btn btn-brand btn-elevate btn-icon-sm">
						<i class="la la-plus"></i>
						Tambah
					</a>
				</div>
			</div>
		</div> -->
	</div>
	<div class="kt-portlet__body">

		<ul class="nav nav-tabs nav-tabs-line" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#" role="tab" onclick="vw_data(0)">Process</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#" role="tab" onclick="vw_data(1)">Accept</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="draft" role="tabpanel">
				<div class="form-group">
					<div class="tampil_isi"></div>
				</div>
			</div>

		</div>

		<!--end: Datatable -->
	</div>
</div> 

<script type="text/javascript">
	$(document).ready(function() {
		vw_data(0);
	});

	function vw_data(status) {
		$.ajax({
			type:'post',
			data:{
				"status":status
			},
			url: '<?php echo site_url('T_inbound/tampil_isi') ?>',
			success: function(data){
				$('.tampil_isi').html(data);
			}
		});
	}
</script>