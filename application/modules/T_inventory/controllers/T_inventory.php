<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH.'libraries/fpdf/html2pdf.php');

class T_inventory extends CI_Controller
{
	
	var $url_ = "T_inventory";
	var $id_ = "transfer_id";
	var $eng_ = "Inventory Transfer";
	var $ind_ = "Inventory Transfer Order";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_.'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library("upload");
	}
	
	public function index()
	{
	}

	public function transfer()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function get_data($status)
	{
		$datacontent['datatable'] = $this->Model->get_data($status);
	}

	public function tampil_isi()
	{
		$data['url'] = $this->url_;
		$data['status'] = $this->input->post('status');

		$this->load->view('T_inventory/isi_table', $data);
	}
	
	public function tambah()
	{
		$table = "t_transfer";
		$kolom = "transfer_code";
		$tgl = date('ymd');
		$sql = $this->db->query("SELECT * FROM $table WHERE SUBSTRING($kolom, 9, 6) = '$tgl' order by $kolom DESC limit 1")->result_array();
		if(count($sql)<1){
			$id = 'MSGLOWTO'.date('ymd').'0001';
		} else {
			$temp = (int)substr($sql[0][$kolom], 14);
			$no = $temp+1;
			if($no<=9 && $no>1){
				$id = 'MSGLOWTO'.date('ymd').'000'.$no;
			} elseif($no<=99 && $no>9){
				$id = 'MSGLOWTO'.date('ymd').'00'.$no;
			} elseif($no<=999 && $no>99){
				$id = 'MSGLOWTO'.date('ymd').'0'.$no;
			} else {
				$id = 'MSGLOWTO'.date('ymd').''.$no;
			}
		}
		$datacontent['transfer_id'] = $id;

		$datacontent['wh'] = $this->Model->get_table("m_warehouse")->result_array();
		$datacontent['deliveries'] = $this->db->query("SELECT * FROM m_delivery_instance")->result_array();
		$datacontent['produk'] = $this->Model->get_produk();
		$datacontent['i'] = 0;

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Tambah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function get_wh()
	{
		$wh_id = $this->input->post('warehouse_id');
		$data = $this->Model->get_table_where("m_warehouse","warehouse_id != '$wh_id'")->result_array();
		$datax = array();
		foreach($data as $index => $value){
			if($value['pic_id'] == $_SESSION['user_id']){
				$datax[] = $value;
			}
		}
		foreach($data as $index => $value){
			if($value['pic_id'] != $_SESSION['user_id']){
				$datax[] = $value;
			}
		}
		// print_r($data);
		echo json_encode(array("status"=>true, "data"=>$datax));
	}
	
	public function get_detail_wh()
	{
		$wh_id = $this->input->post('warehouse_id');
		$data = $this->Model->get_detail_wh($wh_id)->result_array();
		// print_r($data);
		echo json_encode(array("status"=>true, "data"=>$data));
	}
	
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$data2 = $this->input->post('input2');
			// print_r($data2)
			// echo $data2[0]['product_id'];
			// return;

			$table = "t_transfer";
			$kolom = "transfer_code";
			$tgl = date('ymd');
			$sql = $this->db->query("SELECT * FROM $table WHERE SUBSTRING($kolom, 9, 6) = '$tgl' order by $kolom DESC limit 1")->result_array();
			if(count($sql)<1){
				$id = 'MSGLOWTO'.date('ymd').'0001';
			} else {
				$temp = (int)substr($sql[0][$kolom], 14);
				$no = $temp+1;
				if($no<=9 && $no>1){
					$id = 'MSGLOWTO'.date('ymd').'000'.$no;
				} elseif($no<=99 && $no>9){
					$id = 'MSGLOWTO'.date('ymd').'00'.$no;
				} elseif($no<=999 && $no>99){
					$id = 'MSGLOWTO'.date('ymd').'0'.$no;
				} else {
					$id = 'MSGLOWTO'.date('ymd').''.$no;
				}
			}

			$in = array(
				'transfer_code' => $id,
				'transfer_date' => $data['transfer_date'],
				'transfer_date_send' => $data['transfer_date_send'],
				'delivery_instance_id' => $data['ekspedisi'],
				'warehouse_target_id' => $data['warehouse_target_id'],
				'warehouse_id' => $data['warehouse_id'],
				'warehouse_address' => $data['warehouse_address'],
				'warehouse_region_id' => $data['warehouse_region_id'],
				'transfer_status' => 0,
				'transfer_create_date' => date('Y-m-d H:i:s'),
				'transfer_create_user_id' => $_SESSION['user_id']
			);
			$exec = $this->Model->insert("t_transfer", $in);
			$id_transfer = $this->db->insert_id();

			for ($i=0; $i < count($data2); $i++) {
				$dt_produk = $this->Model->get_table_where("produk", "id_produk = '".$data2[$i]['product_id']."'")->result_array();
				$dt_brand = $this->Model->get_brand($data2[$i]['product_id']);
				$in = array(
					'transfer_id' => $id_transfer,
					'product_id' => $data2[$i]['product_id'],
					'product_name' => $dt_produk[0]['nama_produk'],
					'brand_id' => $dt_brand[0]['brand_id'],
					'brand_name' => $dt_brand[0]['brand_name'],
					'transfer_detail_quantity' => $data2[$i]['transfer_detail_quantity'],
					'user_id' => $_SESSION['user_id'],
					'transfer_detail_create_date' => date('Y-m-d H:i:s'),
					'transfer_detail_create_user_id' => $_SESSION['user_id']
				);
				$exec = $this->Model->insert("t_transfer_detail", $in);
			}

			$this->session->set_flashdata('sukses', 'Tambah data berhasil');
		}

		redirect(site_url($this->url_."/transfer"));
	}

	function edit($id)
	{
		$datacontent['wh'] = $this->Model->get_table("m_warehouse")->result_array();
		$datacontent['deliveries'] = $this->db->query("SELECT * FROM m_delivery_instance")->result_array();
		$datacontent['produk'] = $this->Model->get_produk();
		$datacontent['i'] = 0;

		$datacontent['dt_t'] = $this->Model->get_table_where("t_transfer","transfer_id = '".$id."'")->result_array();
		$datacontent['dt_t_d'] = $this->Model->get_table_where("t_transfer_detail","transfer_id = '".$id."'")->result_array();

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Ubah Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form_edit', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan_edit($id)
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$data2 = $this->input->post('input2');
			// print_r($data2)
			// echo $data2[0]['product_id'];
			// return;

			$table = "t_transfer";
			$in = array(
				'transfer_date' => $data['transfer_date'],
				'transfer_date_send' => $data['transfer_date_send'],
				'delivery_instance_id' => $data['ekspedisi'],
				'warehouse_target_id' => $data['warehouse_target_id'],
				'warehouse_id' => $data['warehouse_id'],
				'warehouse_address' => $data['warehouse_address'],
				'warehouse_region_id' => $data['warehouse_region_id'],
				'transfer_status' => 0,
				'transfer_create_date' => date('Y-m-d H:i:s'),
				'transfer_create_user_id' => $_SESSION['user_id']
			);
			$where = array(
				'transfer_id' => $id
			);
			$exec = $this->Model->update("t_transfer", $in, $where);

			$this->Model->delete("T_transfer_detail",$where);
			for ($i=0; $i < count($data2); $i++) {
				$dt_produk = $this->Model->get_table_where("produk", "id_produk = '".$data2[$i]['product_id']."'")->result_array();
				$dt_brand = $this->Model->get_brand($data2[$i]['product_id']);
				$in = array(
					'transfer_id' => $id,
					'product_id' => $data2[$i]['product_id'],
					'product_name' => $dt_produk[0]['nama_produk'],
					'brand_id' => $dt_brand[0]['brand_id'],
					'brand_name' => $dt_brand[0]['brand_name'],
					'transfer_detail_quantity' => $data2[$i]['transfer_detail_quantity'],
					'user_id' => $_SESSION['user_id'],
					'transfer_detail_create_date' => date('Y-m-d H:i:s'),
					'transfer_detail_create_user_id' => $_SESSION['user_id']
				);
				$exec = $this->Model->insert("t_transfer_detail", $in);
			}

			$this->session->set_flashdata('sukses', 'Tambah data berhasil');
		}

		redirect(site_url($this->url_."/transfer"));
	}
	
	public function hapus($id = '')
	{		
		$in = array(
			'transfer_status' => 2
		);
		$where = array(
			'transfer_id' => $id
		);
		$exec = $this->Model->update("T_transfer",$in,$where);
		redirect($this->url_."/transfer");
	}

	function verifikasi($id)
	{
		$datacontent['wh'] = $this->Model->get_table("m_warehouse")->result_array();
		$datacontent['deliveries'] = $this->db->query("SELECT * FROM m_delivery_instance")->result_array();
		$datacontent['produk'] = $this->Model->get_produk();
		$datacontent['i'] = 0;

		$datacontent['dt_t'] = $this->Model->get_table_where("t_transfer","transfer_id = '".$id."'")->result_array();

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Verifikasi Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/verifikasi', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan_verifikasi($id)
	{
		if ($this->input->post('setuju')) {
			$status = 1;
		} else {
			$status = 2;
		}

		$config['upload_path'] = './upload/TRANSFER_ORDER/';
		$config['allowed_types'] = 'pdf';
		$config['encrypt_name'] = true;

		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload("file")){
			$error = $this->upload->display_errors();
			$this->session->set_flashdata('gagal', $error);
		} else{
			$hasil_file = $this->upload->data('file_name');
			$in = array(
				'transfer_path' => APPPATH."../upload/TRANSFER_ORDER/".$hasil_file,
				'transfer_file' => $hasil_file,
				'transfer_note' => $this->input->post('ket'),
				'transfer_status' => $status,
				'transfer_approve_date' => date('Y-m-d H:i:s'),
				'transfer_approve_user_id' => $_SESSION['user_id']
			);
			$where = array(
				'transfer_id' => $id
			);
			$exec = $this->Model->update("t_transfer",$in,$where);

			if ($this->input->post('setuju')) {
				$in = array(
					'transfer_detail_status' => 1
				);
				$exec = $this->Model->update("t_transfer_detail",$in,$where);
			}
			$this->session->set_flashdata('sukses', 'Verifikasi data berhasil');
		}

		redirect(site_url($this->url_."/transfer"));
	}

	function download_pdf($id)
	{
		$dt_header = $this->Model->get_header($id);
		$pdf=new PDF('P','mm','A4');
		$pdf->AddPage();
		$pdf->SetTitle($dt_header[0]['transfer_code']);
		$pdf->SetLeftMargin(20);

		// Title
		$pdf->SetFont('Arial','B',13);
		$pdf->Cell(170,10,'FORM TRANSFER ORDER',0,0,'C');

		$pdf->SetLineWidth(0);
		$pdf->Line(20,20,190,20);
		$pdf->Ln(15);

		$pdf->SetFont('Arial','', 10);

		$pdf->Cell(25,5,'Transfer Order',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'C');
		$pdf->Cell(50,5,$dt_header[0]['transfer_code'],0,0,'L');

		$pdf->Cell(35,5,'Gudang Asal',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'C');
		$pdf->Cell(50,5,$dt_header[0]['warehouse_name'],0,0,'L');
		$pdf->Ln(5);

		$pdf->Cell(25,5,'Tanggal Order',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'C');
		$tgl_order = $dt_header[0]['transfer_date'];
		$bulan = explode("-", $tgl_order)[1];
		if($bulan=="01") $bln = 'Januari';
		elseif($bulan=="02") $bln = 'Februari';
		elseif($bulan=="03") $bln = 'Maret';
		elseif($bulan=="04") $bln = 'April';
		elseif($bulan=="05") $bln = 'Mei';
		elseif($bulan=="06") $bln = 'Juni';
		elseif($bulan=="07") $bln = 'Juli';
		elseif($bulan=="08") $bln = 'Agustus';
		elseif($bulan=="09") $bln = 'September';
		elseif($bulan=="10") $bln = 'Oktober';
		elseif($bulan=="11") $bln = 'November';
		else $bln = 'Desember';
		$tahun = explode("-", $tgl_order)[0];
		$tgl_order = date('d', strtotime($tgl_order)).' '.$bln.' '.$tahun;
		$pdf->Cell(50,5,$tgl_order,0,0,'L');

		$pdf->Cell(35,5,'Gudang Tujuan',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'C');
		$pdf->Cell(50,5,$dt_header[0]['wh_target'],0,0,'L');
		$pdf->Ln(5);

		$pdf->Cell(25,5,'Kargo',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'C');
		$pdf->Cell(50,5,$dt_header[0]['delivery_instance_name'],0,0,'L');

		$pdf->Cell(35,5,'Tgl. Permintaan Kirim',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'C');
		$tgl_send = $dt_header[0]['transfer_date_send'];
		$bulan = explode("-", $tgl_send)[1];
		if($bulan=="01") $bln = 'Januari';
		elseif($bulan=="02") $bln = 'Februari';
		elseif($bulan=="03") $bln = 'Maret';
		elseif($bulan=="04") $bln = 'April';
		elseif($bulan=="05") $bln = 'Mei';
		elseif($bulan=="06") $bln = 'Juni';
		elseif($bulan=="07") $bln = 'Juli';
		elseif($bulan=="08") $bln = 'Agustus';
		elseif($bulan=="09") $bln = 'September';
		elseif($bulan=="10") $bln = 'Oktober';
		elseif($bulan=="11") $bln = 'November';
		else $bln = 'Desember';
		$tahun = explode("-", $tgl_send)[0];
		$tgl_send = date('d', strtotime($tgl_send)).' '.$bln.' '.$tahun;
		$pdf->Cell(50,5,$tgl_send,0,0,'L');
		$pdf->Ln(5);

		$dt_alamat = $this->Model->get_wh_alamat($dt_header[0]['warehouse_id']);
		$pdf->SetWidths(array(25,5,140));
		$pdf->Row6(array(
			'Alamat',
			':',
			$dt_alamat[0]['alamat'].', '.$dt_alamat[0]['kel'].', '.$dt_alamat[0]['kec'].', '.$dt_alamat[0]['kota'].', '.$dt_alamat[0]['prov']
		),
		array('L','C','L'),5);
		$pdf->Ln(5);

		$dt_produk = $this->Model->get_table_where("t_transfer_detail","transfer_id = '$id'")->result_array();
		$pdf->SetWidths(array(100,70));
		$pdf->SetFont('Arial','B',10);
		$pdf->Row5(array(
			"Produk",
			"Jumlah"
		),
		array('L','C'),6);
		$pdf->SetFont('Arial','',10);
		foreach ($dt_produk as $d) {
			$pdf->Row5(array(
				$d['product_name'],
				$d['transfer_detail_quantity']
			),
			array('L','C'),6);
		}
		$pdf->Ln(5);

		$pdf->SetWidths(array(42.5,42.5,42.5,42.5));
		$pdf->SetFont('Arial','',10);
		$pdf->Row6(array(
			"Dibuat Oleh",
			"Disetujui Oleh",
			"Disetujui Oleh",
			"Disetujui Oleh"
		),
		array('C','C','C','C'),6);
		$pdf->Ln(20);

		$pdf->Row6(array(
			$dt_header[0]['user_fullname']."\n".$dt_header[0]['role_name'],
			"Mirza\nManager Sales",
			"Candra\nManager Product",
			"Sheila\nGeneral Manager"
		),
		array('C','C','C','C'),6);

		$pdf->Output('D','FORM INVENTORY TRANSFER ORDER - '.$dt_header[0]['transfer_code'].'.pdf');
		// $pdf->Output();
	}
	
	public function download_pdf_verifikasi($id){
		$this->load->helper(array('url','download'));	
		$dt_file = $this->Model->get_table_where("t_transfer","transfer_id = '$id'")->result_array();
		if (count($dt_file)>0) {
			force_download($dt_file[0]['transfer_path'],NULL);
		}
	}
}
