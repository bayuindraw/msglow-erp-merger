<div class="row">
	<div class="col-lg-6 col-xl-6">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan_verifikasi/'.$dt_t[0]['transfer_id']); ?>" enctype="multipart/form-data">
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Kode Transfer</label>
								<input type="text" id="transfer_code" name="input[transfer_code]" class="md-form-control md-static floating-label form-control" value="<?php echo $dt_t[0]['transfer_code'] ?>" readonly>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Upload File (.pdf)</label>
								<?php if(count($dt_t)>0 && $dt_t[0]['transfer_file']!=""){ ?>
									<a class="btn btn-default btn-lg" href="<?php echo base_url().'upload/TRANSFER_ORDER/'.$dt_t[0]['transfer_file'] ?>" target="_blank"><i class="fa fa-file"></i> <?php echo $dt_t[0]['transfer_file'] ?></a><br><br>
								<?php } ?>
								<input type="file" id="file" name="file" class="md-form-control md-static floating-label form-control" value="">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Keterangan</label>
								<textarea class="form-control" id="ket" name="ket" rows="3" placeholder="Keterangan.." required=""><?php if(count($dt_t)>0 && $dt_t[0]['transfer_note']!=""){ echo $dt_t[0]['transfer_note']; } ?></textarea>
							</div>
						</div>


					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="setuju" value="setuju" class="btn btn-success">Disetujui</button>
						<button type="submit" name="tolak" value="tolak" class="btn btn-danger pull-right">Ditolak</button>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var gagal = '<?php echo str_replace("</p>", "", str_replace("<p>", "", $this->session->flashdata('gagal'))) ?>';
		if (gagal!='') {
			alert(gagal);
		}
	});
</script>