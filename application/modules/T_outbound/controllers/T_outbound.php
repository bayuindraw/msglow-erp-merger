<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;

class T_outbound extends CI_Controller
{
	
	var $url_ = "T_outbound";
	var $id_ = "package_id";
	var $eng_ = "Out Bound";
	var $ind_ = "Out Bound";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'T_outboundModel'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
		$this->load->library("upload");
	}
	
	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function upload()
	{
		$inputFileName = $_FILES['excel']['tmp_name'];
		$spreadsheet = IOFactory::load($inputFileName);
		$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
			// print_r($sheetData);
			// return;
		$no=0;
		for ($i=1; $i <= count($sheetData); $i++) {
			if ($i>1) {
				if ($sheetData[$i]['A']=="Remarks") {
					break;
				}
				if ($sheetData[$i]['A']=="" && $sheetData[$i]['B']=="" && $sheetData[$i]['G']=="" && $sheetData[$i]['J']=="") {
					continue;
				}
					// echo $i.' - '.$sheetData[$i]['A'].' | '.$sheetData[$i]['B'].' | '.$sheetData[$i]['C'].' | '.$sheetData[$i]['D'].' | '.$sheetData[$i]['E'].' | '.$sheetData[$i]['F'].' | '.$sheetData[$i]['G'].' | '.$sheetData[$i]['H'].' | '.$sheetData[$i]['I'].' | '.$sheetData[$i]['J'].' | '.$sheetData[$i]['K'].' | '.$sheetData[$i]['L'].' | '.$sheetData[$i]['M'].' | '.$sheetData[$i]['N'].' | '.$sheetData[$i]['O'].' | '.$sheetData[$i]['P'].' | '.$sheetData[$i]['Q'].' | '.$sheetData[$i]['R'].' | '.$sheetData[$i]['S'].' | '.$sheetData[$i]['T'].' | '.$sheetData[$i]['U'].' | '.$sheetData[$i]['V'].' | '.$sheetData[$i]['W'].' | '.$sheetData[$i]['X'].'<br>';

				$data[$no]['A'] = $sheetData[$i]['A'];
				$data[$no]['B'] = $sheetData[$i]['B'];
				$data[$no]['G'] = $sheetData[$i]['G'];
				$data[$no]['J'] = $sheetData[$i]['J'];
				$data[$no]['K'] = $sheetData[$i]['K'];
				$data[$no]['M'] = $sheetData[$i]['M'];
				$data[$no]['O'] = $sheetData[$i]['O'];
				$data[$no]['T'] = $sheetData[$i]['T'];
				$data[$no]['U'] = str_replace(",", "", str_replace(".00", "", $sheetData[$i]['U']));
				$data[$no]['W'] = str_replace(",", "", str_replace(".00", "", $sheetData[$i]['W']));

				$no++;
			}
		}
		echo json_encode(array('status'=>true, 'message' => $data));
	}

	public function tampil_isi()
	{
		$data['url'] = $this->url_;
		$data['dtsheet'] = $this->input->post('data');

		$this->load->view('T_outbound/isi_table', $data);
	}
	
	public function simpan_upload()
	{
		if ($this->input->post('simpan')) {
			$dataA = $this->input->post('A');
			$dataB = $this->input->post('B');
			$dataG = $this->input->post('G');
			$dataJ = $this->input->post('J');
			$dataK = $this->input->post('K');
			$dataM = $this->input->post('M');
			$dataO = $this->input->post('O');
			$dataT = $this->input->post('T');
			$dataU = $this->input->post('U');
			$dataW = $this->input->post('W');

			for ($i=0; $i < count($dataA); $i++) {
				$dt_brand = $this->Model->get_table_where("m_brand","brand_name = '".$dataO[$i]."'")->result_array();
				$dt_produk = $this->Model->get_table_where("produk","acommerce_code = '".$dataM[$i]."'")->result_array();
				if (count($dt_brand)>0 && count($dt_produk)>0) {
					$table = "t_sales";
					$kolom = "sales_code";
					$tgl = date('ym');
					$sql = $this->db->query("SELECT $kolom FROM $table WHERE SUBSTRING($kolom, 10, 4) = '$tgl' order by $kolom DESC limit 1")->result_array();
					if(count($sql)<1){
						$id = 'MSGLOWSO-'.date('ym').'00001';
					} else {
						$temp = (int)substr($sql[0][$kolom], 13);
						$no = $temp+1;
						if($no<=9 && $no>1){
							$id = 'MSGLOWSO-'.date('ym').'0000'.$no;
						} elseif($no<=99 && $no>9){
							$id = 'MSGLOWSO-'.date('ym').'000'.$no;
						} elseif($no<=999 && $no>99){
							$id = 'MSGLOWSO-'.date('ym').'00'.$no;
						} elseif($no<=9999 && $no>999){
							$id = 'MSGLOWSO-'.date('ym').'0'.$no;
						} else {
							$id = 'MSGLOWSO-'.date('ym').''.$no;
						}
					}
					$in = array(
						'sales_code' => $id,
						'sales_date' => $dataA[$i],
						// 'saller_id' => $dataG[$i],
						'warehouse_id' => $_SESSION['warehouse_id'],
						'user_id' => $_SESSION['user_id'],
						'sales_status' => 1,
						'sales_type' => 1,
						'sales_receipt' => $dataJ[$i],
						'brand_id' => $dt_brand[0]['brand_id'],
						'sales_code_acommerce' => $dataK[$i]
					);
					$exec = $this->Model->insert($table, $in);
					$sales_id = $this->db->insert_id();

					$table = "t_sales_detail";
					$in = array(
						'sales_id' => $sales_id,
						'product_id' => $dt_produk[0]['id_produk'],
						'sales_detail_quantity' => $dataT[$i],
						'user_id' => $_SESSION['user_id'],
						'sales_detail_price' => 0
					);
					$exec = $this->Model->insert($table, $in);

					$table = "t_account_detail_header";
					$in = array(
						'account_id' => "",
						'account_detail_header_amount' => $dt_produk[0]['id_produk'],
						'sales_detail_quantity' => $dataT[$i],
						'user_id' => $_SESSION['user_id'],
						'sales_detail_price' => 0
					);
					$exec = $this->Model->insert($table, $in);


				}

			}

			$this->session->set_flashdata('sukses', 'Tambah data berhasil');
		}
	}
	
	public function hapus($id = '')
	{		
		$in = array(
			'transfer_status' => 2
		);
		$where = array(
			'transfer_id' => $id
		);
		$exec = $this->Model->update("T_transfer",$in,$where);
		redirect($this->url_."/transfer");
	}

	function verifikasi($id)
	{
		$datacontent['wh'] = $this->Model->get_table("m_warehouse")->result_array();
		$datacontent['deliveries'] = $this->db->query("SELECT * FROM m_delivery_instance")->result_array();
		$datacontent['produk'] = $this->Model->get_produk();
		$datacontent['i'] = 0;

		$datacontent['dt_t'] = $this->Model->get_table_where("t_transfer","transfer_id = '".$id."'")->result_array();

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Verifikasi Data '.$this->ind_;
		$datacontent['parameter'] = "";
		$datacontent['id'] = "";
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/verifikasi', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan_verifikasi($id)
	{
		if ($this->input->post('setuju')) {
			$status = 1;
		} else {
			$status = 2;
		}

		$config['upload_path'] = './upload/TRANSFER_ORDER/';
		$config['allowed_types'] = 'pdf';
		$config['encrypt_name'] = true;

		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload("file")){
			$error = $this->upload->display_errors();
			$this->session->set_flashdata('gagal', $error);
		} else{
			$hasil_file = $this->upload->data('file_name');
			$in = array(
				'transfer_path' => APPPATH."../upload/TRANSFER_ORDER/".$hasil_file,
				'transfer_file' => $hasil_file,
				'transfer_note' => $this->input->post('ket'),
				'transfer_status' => $status,
				'transfer_approve_date' => date('Y-m-d H:i:s'),
				'transfer_approve_user_id' => $_SESSION['user_id']
			);
			$where = array(
				'transfer_id' => $id
			);
			$exec = $this->Model->update("t_transfer",$in,$where);

			if ($this->input->post('setuju')) {
				$in = array(
					'transfer_detail_status' => 1
				);
				$exec = $this->Model->update("t_transfer_detail",$in,$where);
			}
			$this->session->set_flashdata('sukses', 'Verifikasi data berhasil');
		}

		redirect(site_url($this->url_."/transfer"));
	}
}
