
<div class="row"
<div class="col-lg-12 col-xl-12">

	<!--begin::Portlet-->
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					<?= $title ?>
				</h3>
			</div>
		</div>
		<form enctype="multipart/form-data" id="frm">
			<div class="kt-portlet__body">
				<div class="form-group">
					<label>File Outbound</label><br>
					<input type="file" name="excel">
				</div>
			</div>
			<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
					<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
				</div>
			</div>
		</form>

	</div>
</div>

<style type="text/css">
@media (min-width: 1024px) {
	.modal-lg,
	.modal-xl {
		max-width: 800px; 
	} 
	.modal-xxl {
		max-width: 90%; 
	} 
}

@media (min-width: 1399px) {
	.modal-xl {
		max-width: 1140px; 
	} 
	.modal-xxl {
		max-width: 90%; 
	} 
}
</style>
<div id="hasilModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xxl">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Hasil Upload</h4>
				<button type="button" class="close" data-dismiss="modal"></button>
			</div>

			<div class="tampil_isi"></div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#frm').submit(function(e){
			e.preventDefault(); 
			$.ajax({
				url:'<?= site_url($url . '/upload'); ?>',
				type:"post",
				data:new FormData(this),
				processData:false,
				contentType:false,
				cache:false,
				async:false,
				success: function(data){
					var json = $.parseJSON(data);
					if(!json.status){
						alert(json.message);
					} else {
						// console.log(json.message);
						// document.getElementById("tes").innerHTML = json.message[0]['A'];
						vw_data(json.message);
					}
				}
			});

		});

	});

	function vw_data(data) {
		$.ajax({
			type:'post',
			data:{
				"data":data
			},
			url: '<?php echo site_url('T_outbound/tampil_isi') ?>',
			success: function(data){
				$('#hasilModal').modal('show');
				$('.tampil_isi').html(data);
			}
		});
	}

</script>