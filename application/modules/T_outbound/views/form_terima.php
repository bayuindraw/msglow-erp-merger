<div class="row">
	<div class="col-lg-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
				<div class="kt-portlet__body">
					<div class="row">

						<div class="col-md-4">
							<div class="form-group">
								<label>Tanggal Diterima</label>
								<input type="text" id="incoming_goods_date" name="input[incoming_goods_date]" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= date('Y-m-d') ?>" required>
								<input type="hidden" id="warehouse_origin_id" name="input[warehouse_origin_id]" class="md-form-control md-static form-control" value="<?php echo @$dt_produk[0]['wh_asal'] ?>" required>
								<input type="hidden" id="package_id" name="input[package_id]" class="md-form-control md-static form-control" value="<?php echo @$dt_produk[0]['package_id'] ?>" required>
							</div>
						</div>

						<div class="col-md-12">
							<label>Detail Barang</label>
							<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
								<thead>
									<tr>
										<th>No</th>
										<th>Barang</th>
										<th>No. TO</th>
										<th>Jumlah SJ</th>
										<th>Jumlah Diterima</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0; foreach ($dt_produk as $d) { ?>
									<tr>
										<td><?php echo ($i+1) ?></td>
										<td><?php echo $d['product_name'] ?></td>
										<td><?php echo $d['transfer_code'] ?></td>
										<td><?php echo $d['package_detail_quantity'] ?></td>
										<td>
											<input type="hidden" name="input2[<?= $i ?>][id_barang]" value="<?php echo $d['product_id'] ?>">
											<input type="hidden" name="input2[<?= $i ?>][jumlah_sj]" value="<?php echo $d['package_detail_quantity'] ?>">
											<input type="hidden" name="input2[<?= $i ?>][package_trial_detail_id]" value="<?php echo $d['package_detail_id'] ?>">
											<input type="hidden" name="input2[<?= $i ?>][transfer_detail_id]" value="<?php echo $d['transfer_detail_id'] ?>">
											<input type="hidden" name="input2[<?= $i ?>][transfer_id]" value="<?php echo $d['transfer_id'] ?>">
											<input type="number" class="form-control" placeholder="Jumlah diterima" name="input2[<?= $i ?>][jumlah]" value="" required>
										</td>
									</tr>
									<?php $i++; } ?>
								</tbody>
							</table>
						</div>


					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>