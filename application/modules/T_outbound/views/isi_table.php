<form method="POST" action="<?php echo site_url($url.'/simpan_upload') ?>">
	<div class="modal-body">
		<div class="table-responsive">
			<table class="table table-striped- table-bordered table-hover" id="kt_table_1">
				<thead>
					<tr>
						<th style="width: 15px;">No</th>
						<th style="width: 20px;">Order Date</th>
						<th style="width: 20px;">Completed Date</th>
						<th style="width: 30px;">Sales Channel</th>
						<th style="width: 40px;">Partner Order ID</th>
						<th style="width: 40px;">aCommerce Order ID</th>
						<th style="width: 20px;">SKU</th>
						<th style="width: 20px;">Brand</th>
						<th style="width: 20px;">Quantity</th>
						<th style="width: 25px;">GMV</th>
						<th style="width: 25px;">RSP</th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1; foreach ($dtsheet as $d) { ?>
					<tr>
						<td style="width: 15px;"><?php echo $i; ?></td>
						<input type="hidden" name="A[]" class="form-control" value="<?php echo date('Y-m-d', strtotime($d['A'])); ?>">
						<td style="width: 20px;"><?php echo $d['A']; ?></td>
						<input type="hidden" name="B[]" class="form-control" value="<?php echo date('Y-m-d', strtotime($d['B'])); ?>">
						<td style="width: 20px;"><?php echo $d['B']; ?></td>
						<input type="hidden" name="G[]" class="form-control" value="<?php echo $d['G']; ?>">
						<td style="width: 30px;"><?php echo $d['G']; ?></td>
						<input type="hidden" name="J[]" class="form-control" value="<?php echo $d['J']; ?>">
						<td style="width: 40px;"><?php echo $d['J']; ?></td>
						<input type="hidden" name="K[]" class="form-control" value="<?php echo $d['K']; ?>">
						<td style="width: 40px;"><?php echo $d['K']; ?></td>
						<input type="hidden" name="M[]" class="form-control" value="<?php echo $d['M']; ?>">
						<td style="width: 20px;"><?php echo $d['M']; ?></td>
						<input type="hidden" name="O[]" class="form-control" value="<?php echo $d['O']; ?>">
						<td style="width: 20px;"><?php echo $d['O']; ?></td>
						<input type="hidden" name="T[]" class="form-control" value="<?php echo $d['T']; ?>">
						<td style="width: 20px;"><?php echo $d['T']; ?></td>
						<input type="hidden" name="U[]" class="form-control" value="<?php echo $d['U']; ?>">
						<td style="width: 25px;"><?php echo number_format($d['U'],0,'','.'); ?></td>
						<input type="hidden" name="W[]" class="form-control" value="<?php echo $d['W']; ?>">
						<td style="width: 25px;"><?php echo number_format($d['W'],0,'','.'); ?></td>
					</tr>
					<?php $i++; } ?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" name="simpan" class="btn btn-success pull-right" value="Simpan">Upload</button>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",
			"autowidth": true,
			"processing": true,
			"serverSide": false
		});
	});
</script>