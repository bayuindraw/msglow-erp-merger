<?php
defined('BASEPATH') or exit('No direct script access allowed');
class T_package_trialModel extends CI_Model
{
	
	var $table_ = "t_package_trial";
	var $id_ = "package_id";
	var $eng_ = "package";
	var $url_ = "T_package_trial";
	
	function get_package_trial_detail_draft(){
		$data = $this->db->query("SELECT A.package_detail_id, A.product_id, D.nama_produk, A.package_detail_quantity, GROUP_CONCAT(C.nama_phl SEPARATOR', ') AS pegawai FROM t_package_trial_detail_draft A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id LEFT JOIN produk D ON D.id_produk = A.product_id WHERE A.package_id = 238 GROUP BY A.package_detail_id")->result_array();
		return $data;
	}
	
	function get_package_trial_detail2(){
		$data = $this->db->query("SELECT A.package_detail_id, A.product_id, D.nama_produk, A.package_detail_quantity, GROUP_CONCAT(C.nama_phl SEPARATOR', ') AS pegawai FROM t_package_trial_detail A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id LEFT JOIN produk D ON D.id_produk = A.product_id WHERE A.package_id = 238 GROUP BY A.package_detail_id")->result_array();
		return $data;
	}
	
	function get_data_sales()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN m_seller B ON B.seller_id = A.seller_id";
		$table = "t_sales A";
		$id = 'sales_id';
		$field = array('sales_code', 'seller_code', 'seller_name', 'sales_date', 'sales_status');
		$url = $this->url_;
		$action = '<a href="'.site_url($url."/form/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
					  'recordsTotal' => (int)$jum_all['jum'],
					  'recordsFiltered' => (int)$jum_filter['jum'],
					  'data' => @$dataxy
					  );
		
		echo json_encode($data);
	}
	
	function get_package_trial($id){
		$arrdata = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN member B ON B.kode = A.member_code WHERE A.package_id = '$id'")->row_array();
		return $arrdata;
	}
	
	function get_package_trial_detail($id){
		$arrdata = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_detail_draft B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = B.product_id WHERE A.package_id = '$id'")->result_array();
		return $arrdata;
	}
	
	function insert_reset($id){
		$this->db->query("DELETE FROM t_package_trial_detail_draft WHERE package_id = '$id'");
		$this->db->query("DELETE FROM t_package_trial_employee WHERE package_id = '$id'");
		return '';
	}
	
	
	function get_package_trial_employee($id){
		$arrdata = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_employee B ON B.package_id = A.package_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.package_id = '$id'")->result_array();
		return $arrdata;
	}
	
	function get_data_postage()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN t_sales B ON B.sales_id = A.sales_id";
		$arrjoin[] = "LEFT JOIN m_seller C ON C.seller_id = B.seller_id";
		$table = "t_package A";
		$id = 'package_id';
		$field = array('package_code', 'sales_code', 'seller_name', 'sales_address', 'package_postage');
		$url = $this->url_;
		$action = '<a href="'.site_url($url."/form_postage/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
					  'recordsTotal' => (int)$jum_all['jum'],
					  'recordsFiltered' => (int)$jum_filter['jum'],
					  'data' => @$dataxy
					  );
		
		echo json_encode($data);
	}
	
	function get_package($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package WHERE package_id = '" . $id . "'")->row_array();
		return $arrdata;
	}
	
	function get_package_all($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package A LEFT JOIN t_sales B ON B.sales_id = A.sales_id LEFT JOIN m_seller C ON C.seller_id = B.seller_id LEFT JOIN m_account D ON D.seller_id = C.seller_id WHERE A.package_id = '" . $id . "'")->row_array();
		return $arrdata;
	}
	
	function get_sales_detail($id)
	{
		$arrdata = $this->db->query("SELECT SUM(total) as total, nama_produk, id_produk, sales_id FROM v_pack_product A WHERE sales_id = '" . $id . "' GROUP BY A.id_produk")->result_array();
		return $arrdata;
	}
	
	function get_package_detail_date($id){
		$arrdata = $this->db->query("SELECT * FROM t_package_detail A LEFT JOIN t_package B ON B.package_id = A.package_id WHERE sales_id = '" . $id . "' GROUP BY package_date")->result_array();
		return $arrdata;
	}
	
	function get_package_detail_sum($id){
		$arrdata = $this->db->query("SELECT SUM(package_detail_quantity) AS total, product_id, package_date FROM t_package_detail A LEFT JOIN t_package B ON B.package_id = A.package_id WHERE sales_id = '" . $id . "' GROUP BY package_date, product_id")->result_array();
		foreach($arrdata as $key => $value){
			$arrdatax[$value['package_date']][$value['product_id']] = $value['total'];
		}
		return @$arrdatax;
	}
	
	function get_package_detail($id)
	{
		$this->db->where('sales_id', $id);
		$this->db->join('t_package', 't_package.package_id = t_package_detail.package_id');
		$this->db->join('m_user', 'm_user.user_id = t_package.user_id');
		$this->db->join('produk', 'produk.id_produk = t_package_detail.product_id');
		$data = $this->db->get('t_package_detail');
		return $data->result_array();
	}
	
	function get_sales($id)
	{
		$this->db->where('sales_id', $id);
		$data = $this->db->get('t_sales');
		return $data->row_array();
	}
	
	function get()
	{
		$data = $this->db->get($this->table_);
		return $data;
	}
	
	function search_member($search)
	{
		$data = $this->db->query("SELECT nama, kode, kota FROM member WHERE nama LIKE '%$search%' OR kode LIKE '%$search%'")->result_array(); 
		echo '{
			 "results": [';
			 $i = 0;
		foreach($data as $index => $value){
			if($i > 0){
					echo ',';
				}
			echo '
				{
					"id": "'.$value["kode"].'",
					"text": "'.$value["nama"].' ('.$value["kode"].') ('.$value["kota"].')"
				}';
				$i++;
		}
		echo '],
  "pagination": {
    "more": true
  }
}';
	}
	
	function get_member()
	{
		$data = $this->db->query("SELECT nama, kode FROM member WHERE id_member >= '23060' OR nama LIKE '%ROICHANA%'
UNION
SELECT nama, kode FROM member
LIMIT 1000"); 
		return $data->result_array();
	}
	
	function get_seller()
	{
		$data = $this->db->get('m_seller');
		return $data->result_array();
	}
	
	function get_phl()
	{
		$data = $this->db->get('tb_phl');
		return $data->result_array();
	}
	
	function get_employee()
	{
		$data = $this->db->get('m_employee');
		return $data->result_array();
	}
	
	function get_produk()
	{
		//$this->db->join('tb_stock_produk', 'tb_stock_produk.id_stock = produk.id_produk');
		$data = $this->db->query('SELECT *, A.nama_produk FROM produk A LEFT JOIN tb_stock_produk B ON B.id_barang = A.id_produk');
		return $data->result_array();
	}
		
	function insert_id()
	{
		return $this->db->insert_id();
	}
	
	function get_product()
	{
		$data = $this->db->get('produk');
		return $data->result_array();
	}
	
	function get_data($tgl_awal, $tgl_akhir)
	{
		if($tgl_awal != "" && $tgl_akhir !=""){
			$arrwhere[] = "(A.package_date BETWEEN '$tgl_awal' AND '$tgl_akhir')";
		}else{
			$tgl_awal = date('Y-m-d', strtotime(date('Y-m-d'). ' - 1 days'));
			$tgl_akhir = date('Y-m-d');
			$arrwhere[] = "(A.package_date BETWEEN '$tgl_awal' AND '$tgl_akhir')";
		}
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN member B ON B.kode = A.member_code";
		$table = $this->table_." A";
		$id = $this->id_;
		$arrorder2[] = 'package_status_id ASC';
		$field = array('package_code', 'nama', 'package_status_id');
		$url = $this->url_;
		$action = '<a href="'.site_url($url."/form_detail2/xid").'" class="btn btn-danger"> <i class="fa fa-print"></i></a> <a href="'.site_url($url."/cetaksuratjalankemasan/xid").'" class="btn btn-warning"> <i class="fa fa-print"></i></a>';
		$action1 = '<a href="'.site_url($url."/form_employee/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url."/form_detail_draft/xid").'" class="btn btn-danger"> <i class="fa fa-print"></i></a> <a href="'.site_url($url."/form_confirmation/xid").'" class="btn btn-warning"> <i class="fa fa-key"></i></a>';
		
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach($arrorder2 as $index => $value){
			$arrorder[] = $value;
		}
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		//echo "SELECT $id, $jfield FROM $table $join $where $order $limit";
		//die();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				if($keyfield == 'package_status_id'){
					$datax[] = (($valuer[$keyfield]=="1")?"Terkonfirmasi":"Draft");
				}else{
					$datax[] = $valuer[$keyfield];
				} 
			}
			$datax[] = str_replace('xid', $valuer[$id], (($valuer['package_status_id'] == '1')?$action:$action1));
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
					  'recordsTotal' => (int)$jum_all['jum'],
					  'recordsFiltered' => (int)$jum_filter['jum'],
					  'data' => @$dataxy
					  );
		
		echo json_encode($data);
	}
	
	function get_data_report($search = '')
	{
		if($search != ""){
			$arrsearch = explode("%7C", $search);
			$arrwhere[] = "A.member_status_id = '".$arrsearch[0]."'";
			$arrwhere[] = "(A.member_code LIKE '%".$arrsearch[1]."%' OR A.member_name LIKE '%".$arrsearch[1]."%' OR A.member_phone LIKE '%".$arrsearch[1]."%' OR A.member_address LIKE '%".$arrsearch[1]."%' OR D.nama_kecamatan LIKE '%".$arrsearch[1]."%' OR C.nama_kota LIKE '%".$arrsearch[1]."%')";
		}
		$arrwhere[] = "A.member_code != ''";
		$arrjoin[] = "LEFT JOIN mst_kecamatan D ON D.id_kecamatan = A.district_id AND D.id_kota = A.city_id";
		$arrjoin[] = "LEFT JOIN mst_kota C ON C.id_kota = A.city_id";
		$arrjoin[] = "LEFT JOIN m_member_status B ON B.member_status_id = A.member_status_id";
		$table = $this->table_;
		$id = $this->id_;
		$field = array('member_code', 'member_name', 'member_phone', 'nama_kota', 'member_status_name', 'member_phone', 'member_date');
		$url = $this->url_;
		$action = '<a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . "/transfer/tambah/xid").'" class="btn btn-info"> <i class="fa fa-list"></i></a> <a href="'.site_url($url . "/send/tambah/xid").'" class="btn btn-info"> <i class="fa fa-download"></i></a> <a href="'.site_url($url . '/hapus/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
					  'recordsTotal' => (int)$jum_all['jum'],
					  'recordsFiltered' => (int)$jum_filter['jum'],
					  'data' => @$dataxy
					  );
		
		echo json_encode($data);
	}
	
	
	
	function get_detail($id)
	{
		$this->db->where($this->id_, $id);
		$this->db->join('m_account_detail_category B', 'B.account_detail_category_id = A.account_detail_category_id');
		$data = $this->db->get($this->table2_.' A');
		return $data;
	}
	
	function get_data_detail($idxx)
	{
		$table = $this->table2_.' A';
		$id = "CONCAT(account_id, '/', account_detail_id) AS id";
		$field = array('account_detail_category_name', 'account_detail_pic', 'account_detail_note', 'account_detail_debit', 'account_detail_credit', 'account_detail_realization');
		$arrjoin[] = 'LEFT JOIN m_account_detail_category B ON B.account_detail_category_id = A.account_detail_category_id';
		$url = $this->url_;
		$arrwhere[] = "account_id = $idxx";
		$action = '<a href="'.site_url($url."/form_detail/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . '/hapus_detail/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(account_detail_id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT(account_detail_id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer['id'], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
					  'recordsTotal' => (int)$jum_all['jum'],
					  'recordsFiltered' => (int)$jum_filter['jum'],
					  'data' => @$dataxy
					  );
		
		echo json_encode($data);
	}
	
	function insert($data = array())
	{
		$this->db->insert($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function insert_detail_draft($data = array())
	{
		$this->db->insert($this->table_."_detail_draft", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function insert_detail($data = array())
	{
		$this->db->insert($this->table_."_detail", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function insert_employee($data = array())
	{
		$this->db->insert($this->table_."_employee", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function insert_account($data = array())
	{
		$this->db->insert('m_account', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function update($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($this->table_);
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function update_detail($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update('t_account_detail', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete_detail($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete('t_account_detail');
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}
	
	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}
	
	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}
	
	function get_max_id()
	{
		$data = $this->db->query("SELECT (IFNULL(MAX(account_detail_id), 0) + 1) as id FROM t_account_detail")->row_array();
		return $data['id'];
	}
	
	function get_account_id($id)
	{
		$data = $this->db->query("SELECT account_id FROM m_account WHERE seller_id = '$id'")->row_array();
		return $data['account_id'];
	}
	
	function insert_account_detail($data = array())
	{
		$this->db->insert("t_account_detail", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
		return $this->db->insert_id();
	}
	
	function get_m_account()
	{
		$data = $this->db->get('m_account');
		return $data;
	}
	
	function update_balance($id, $balance, $type, $account_type = '', $monthly = '')
	{
		if($type == "credit"){
			if($monthly == ''){
				$this->db->query("UPDATE m_account SET account_credit = (account_credit + $balance) WHERE account_id = ".$id);
			}else{
				if($account_type == '1'){ 
					$this->db->query("UPDATE m_account SET account_debit = (account_debit - ($balance)), account_monthly_debit = (account_monthly_debit - ($balance)) WHERE account_id = ".$id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit - ($balance)) WHERE account_reset_date > '$monthly'");
				}else{
					$this->db->query("UPDATE m_account SET account_credit = (account_credit + $balance), account_monthly_credit = (account_monthly_credit + $balance) WHERE account_id = ".$id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}else if($type == "debit"){
			if($monthly == ''){
				$this->db->query("UPDATE m_account SET account_debit = (account_debit + $balance) WHERE account_id = ".$id);
			}else{
				if($account_type == '2'){
					$this->db->query("UPDATE m_account SET account_credit = (account_credit - ($balance)), account_monthly_credit = (account_monthly_credit - ($balance)) WHERE account_id = ".$id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit - ($balance)) WHERE account_reset_date > '$monthly'");
				}else{
					$this->db->query("UPDATE m_account SET account_debit = (account_debit + $balance), account_monthly_debit = (account_monthly_debit + $balance) WHERE account_id = ".$id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}
		return '';
	}
	
	public function update_keluar($jml, $tanggal, $id_barang) {
		$Query = $this->db->query("UPDATE tb_stock_produk SET jumlah = (jumlah - ".$jml.") WHERE id_barang = '".$id_barang."'");
		$Query = $this->db->query("UPDATE tb_stock_produk_history SET jumlah = (jumlah - ".$jml.") WHERE id_barang = '".$id_barang."' AND tanggal > '".$tanggal."'");
		return $Query;	
	}
	
}
