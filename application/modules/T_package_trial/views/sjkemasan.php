<table style="width: 100%;border-bottom: 2px solid black;font-size: 8px">
	<tr>
		<td style="width: 10%"><img src="<?= base_url() ?>assets/logo-cv-cantik.PNG" style="width:130px"></td>
		<td style="width: 18%">
			<b style="font-size: 20px;">CV. CANTIK INDONESIA</b>
			<hr>
			<span style="font-size: 13px">RUKO WOW SAWOJAJAR BLOK - NY 01</span><br>
			<span style="font-size: 13px">KOTA MALANG - JAWA TIMUR </span><br>
			<span style="font-size: 13px">TELP. 0341 - 302 2614</span> <br>
			<span style="font-size: 15px">www.msglowid.com</span>
		</td>
		<td style="width: 70%" align="right"><span style="font-size: 23px"><b>SURAT JALAN</b></span></td>
	</tr>
</table>

<table width="100%" style="font-size: 10px">
	<tr>
		<td width="60%">
			<table>
				<tr>
					<td>NO SURAT JALAN</td>
					<td>:</td>
					<td><b><?= @$noSj ?></b></td>
					<td></td>
				</tr>
				<tr>
					<td>TANGGAL SURAT JALAN</td>
					<td>:</td>
					<td><b><?= @$tglSJ ?></b></td>
				</tr>

			</table>
		</td>
		<td width="40%">
			<table>
				<tr>
					<td><br>DITUJUKAN KEPADA </td>
					<td></td>
					<td></td>

				</tr>
				<tr>
					<td colspan="3">
						<br>
						<b><?= @$arrpackage_trial['nama'] ?></b> <br>
						<?= @$arrpackage_trial['alamat']." ".@$arrpackage_trial['kota'] ?> <br>
						<?= @$arrpackage_trial['telepon'] ?> <br>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>



<h4 align="center"> DETAIL BARANG</h4>
<table style="width: 100%;font-size: 15px;border: 1px solid black;" border="1" class="table1">
	<tr style="color:green">
		<td align="center"><b>No</b></td>
		<td align="center"><b>Nama Kemasan</b></td>
		<td align="center"><b>Jumlah Kemasan</b></td>
	</tr>
	<?php

	$no = 0;
	
	foreach ($arrpackage_trial_detail as $key => $vaData) {
		//if($no <= 6){
	?>
		<tr>
			<td align="center"><?= ++$no ?></td>
			<td>&nbsp;&nbsp;<?= $vaData['nama_produk'] ?></td>
			<td align="center"><?= number_format($vaData['package_detail_quantity']) ?></td>
		</tr>
	<?php
		$no++;
	}
	?>
</table>

<br>
<h4>Catatan : </h4>
<br>

<table style="width:100%;font-size: 15px;" align="center">
	<tr>
		<td align="center">Dibuat</td>
		<td align="center">Diketahui</td>
		<td align="center">Driver</td>
		<td align="center">Diterima</td>
	</tr>
	<tr>
		<td align="center"><br><br><br> ................................</td>
		<td align="center"><br><br><br> ................................</td>
		<td align="center"><br><br><br> ................................</td>
		<td align="center"><br><br><br> ................................</td>
	</tr>
</table>
<script>
	window.print();
</script>