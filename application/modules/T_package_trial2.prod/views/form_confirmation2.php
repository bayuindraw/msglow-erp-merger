
<div class="row">
  <div class="col-lg-12 col-xl-12">
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            SELLER : <?= @$arrmember['nama']." (".@$arrmember['kode'].")" ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
        <h5>Rencana Pengiriman Produk : <?= $arrsales['sales_code'] ?></h5>
        <hr>
        <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_confirmation'); ?>" method="post" novalidate>
          <?= input_hidden('parameter', 'tambah') ?>
          <?= input_hidden('id', $id) ?>
		  <div class="form-group">
            <label>Kode</label><br>
            <?= (@$arrpackage_trial_detail[0]['package_code'] != "")?@$arrpackage_trial_detail[0]['package_code']:$code ?>
          </div>
		  <div class="form-group">
            <label>Tanggal Pengemasan</label><br>
            <?= (@$arrpackage_trial_detail[0]['package_date'] != "")?@$arrpackage_trial_detail[0]['package_date']:date('Y-m-d') ?> 
          </div>
		 <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>Produk</th>
                                <th>Jumlah</th>
                              </tr>
                            </thead>
                            <tbody id="x"> 
								<?php if(count(@$arrpackage_trial_detail) > 0){ 
									$i = 0;
									foreach($arrpackage_trial_detail as $indexx => $valuex){
								?>
									<tr id="<?= $i; ?>">
										
										<td>
								<?php
									foreach($arrproduk as $indexl => $valuel){
											echo (($valuex['id_produk'] == $valuel['id_produk'])?$valuel['nama_produk']:'');
											//echo '<option value="'.$valuel['id_phl'].'" '.(($valuex['employee_id'] == $valuel['id_phl'])?'selected':'').'>'.$valuel['nama_phl'].'</option>';
								?>
								<?php	
										} ?></td>
									<td><?= $valuex['package_detail_quantity']; ?></td>
									</tr>											
									<?php $i++;
									}
									
								}else{ ?>
									<tr id="0">
										<td><select name="input2[0][product_id]" class="PilihBarang form-control md-static" required><option></option><?= $product_list ?></select></td>
										<td><input type="number" class="form-control" placeholder="Jumlah" name="input2[0][package_detail_quantity]" required></td>
										<td><div id="td0"><div class="form-group row" id="employee0x0">
											<div class="col-10">
												<select name="input3[0][0][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select>
											</div>
											<label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee(0, 0);">-</label>
										</div>
										</div>
										<label onclick="add_employee(0);">+</label>
										</td>										
										<td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									</tr> 
								<?php } ?>
                            </tbody>    
                         </table>

          <!--<div class="form-group">
            <label>Nama Barang</label>
            <select name="NamaBarang" id="cIdStock" class="form-control md-static" required>

              <option></option>
              <?php
              /*$query = $this->model->ViewWhere('v_detail_produk_jual', 'kode_jual', $action);
              foreach ($query as $key => $vaKemasan) {
              ?>
                <option value="<?= $vaKemasan['id_barang'] ?>"><?= $vaKemasan['nama_produk'] ?></option>
              <?php }*/ ?>
            </select>
          </div>-->
        </form>
      </div>
    </div>
  </div>
</div>

<script>
it = <?= ((count(@$arrpackage_trial_detail) > 0)?count(@$arrpackage_trial_detail):1); ?>;
let fruits = [];

<?php if(count(@$arrpackage_trial_detail) > 0){
	$i = 0;
	foreach($arrpackage_trial_detail as $indexx => $valuex){	
			?>
				fruits.push([0]);
			<?php
			$i2 = 1;
		foreach($data_trial_employee[$valuex['package_detail_id']] as $indexxx => $valuexxx){
			?>
				fruits[<?= $i ?>]++;
			<?php 
			$i2++;
		}
		$i++;
	}
}else{ ?>
	
<?php } ?>
fruits.push([0]);
function myCreateFunction() {
	it++;
	fruits.push([0]);
	var html = '<tr id="'+it+'"><td><select name="input2['+it+'][product_id]" class="PilihBarang form-control md-static" required><option></option><?= $product_list ?></select></td><td><input type="number" class="form-control" placeholder="Jumlah" name="input2['+it+'][package_detail_quantity]" required></td><td><div id="td'+it+'"><div class="form-group row" id="employee'+it+'x0"><div class="col-10"><select name="input3['+it+'][0][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select></div><label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee('+it+', 0);">-</label></div></div><label onclick="add_employee('+it+');">+</label></td><td><button onclick="myDeleteFunction('+it+')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
	$('#x').append(html);
	$('.PilihBarang').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Barang',
	});
	$('.PilihPegawai').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Pegawai', 
	});
}
function add_employee(id) { 
	fruits[id]++;
	var html = '<div class="form-group row" id="employee'+id+'x'+fruits[id]+'"><div class="col-10"><select name="input3['+id+']['+fruits[id]+'][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select></div><label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee('+id+', '+fruits[id]+');">-</label></div>';
	$('#td'+id).append(html);
	$('.PilihPegawai').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Pegawai', 
	});
}

function myDeleteFunction(id) {
	$('#'+id).remove();
}

function delete_employee(id, id2) {
	$('#employee'+id+'x'+id2).remove();
}
</script>

