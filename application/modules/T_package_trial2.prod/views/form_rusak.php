<div class="row">
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Data Inputan <?= $title ?>
												</h3>
											</div>
										</div>
										<div class="col-lg-12">
											<br>
											<table class="table table-striped- table-bordered table-hover" id="mytable_barang" >
												<tr>
													<th>No</th>
													<th>Tanggal</th>
													<th>Tanggal Incoming</th>
													<th>Nama Product</th>
													<th>Jumlah</th>
													<th>Batch Number</th>
													<th>Ket Reject</th>
													<th>Status Product</th>
												</tr>
												<?php 
													$no=0;
													foreach ($valin as $key => $vaData) {
												?>
												<tr>
													<td><?= ++$no ?></td>
													<td><?=$vaData['package_date']?></td>
													<td><?=$vaData['tanggal_incoming']?></td>
													<td><?=$vaData['nama_produk']?></td>
													<td><?=$vaData['package_detail_quantity']?></td>
													<td><?=$vaData['package_detail_batch']?></td>
													<td><?=$vaData['keterangan_reject']?></td>
													<td><?=$vaData['status_barang']?></td>
												</tr>
												<?php } ?>
											</table>
									 	</div>
									</div>
								</div>
<div class="row"
								<div class="col-lg-12 col-xl-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan_rusak'); ?>" enctype="multipart/form-data">
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Tanggal</label>
													<input type="text" id="dTglPo" name="input[package_date]" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= date('Y-m-d') ?>" required>
												</div>
												<div class="form-group">
													<label>Product</label>
													<select name="input2[product_id]" class="PilihBarang form-control md-static" required><option></option>
													<?php
														foreach($arrproduct as $indexl => $valuel){
																echo '<option value="'.$valuel['id_produk'].'" '.(($valuex['product_id'] == $valuel['id_produk'])?'selected':'').'>'.$valuel['nama_produk'].' ('.$valuel['klasifikasi'].') (stock: '.$valuel['jumlah'].')</option>';
																//echo '<option value="'.$valuel['id_phl'].'" '.(($valuex['employee_id'] == $valuel['id_phl'])?'selected':'').'>'.$valuel['nama_phl'].'</option>';
													?>
												<?php	
													} 
												?>
												</select>
												</div>
												<div class="form-group">
													<label>Jumlah</label>
													<input type="text" id="cJumlah" name="input2[package_detail_quantity]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static" autocomplete="off">
												</div>
												<div class="form-group">
													<label>Batch</label>
													<input type="text" name="input2[package_detail_batch]" class="form-control static" placeholder="No Batch" required>
												</div>
												<!-- <div class="form-group">
													<label>Surat Jalan</label>
													<input type="text" name="input[package_delivery_number]" class="form-control static" placeholder="Surat Jalan">
												</div> -->
												<div class="form-group">
													<label>Keterangan Reject</label>
													<input type="text" name="input[keterangan_reject]" class="form-control static" placeholder="Keterangan Reject" required>
												</div>
												<div class="form-group">
													<label>Status Barang</label>
													<input type="text" name="input[status_barang]" class="form-control static" placeholder="Status Barang" required>
												</div>
												<div class="form-group">
													<label>Tanggal Incoming</label>
													<input type="text"  name="input[tanggal_incoming]" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= date('Y-m-d') ?>" required>
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>