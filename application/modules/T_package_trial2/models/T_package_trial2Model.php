<?php
defined('BASEPATH') or exit('No direct script access allowed');
class T_package_trial2Model extends CI_Model
{

	var $table_ = "t_package_trial";
	var $id_ = "package_id";
	var $eng_ = "package";
	var $url_ = "T_package_trial2";

	function get_factory()
	{
		$data = $this->db->get('factory');
		return $data->result_array();
	}

	function check_fifo_alpha($product_id)
	{
		$arrin = $this->db->query("SELECT * FROM terima_produk WHERE id_barang = '$product_id' AND pending_quantity > 0")->result_array();
		$arrout = $this->db->query("SELECT * FROM t_package_trial_detail WHERE product_id = '$product_id' AND pending_alpha_quantity > 0")->result_array();

		$hitung = 0;
		if (count(@$arrout) > 0) {
			foreach ($arrout as $indexout => $valueout) {
				if ($hitung == 0) {
					$hitung = $valueout['pending_alpha_quantity'];
					$valueout['package_detail_id'];
					$valueout['package_id'];
					if (count(@$arrin) > 0) {
						foreach ($arrin as $indexin => $valuein) {
							if ($hitung != 0 && $valuein['pending_quantity'] != 0) {
								$xdata = array();
								$xupdate = array();
								if ($hitung < $valuein['pending_quantity']) {

									if (@$temp_in[$indexin]['pending_quantity'] == "" || @$temp_in[$indexin]['pending_quantity'] == 0) $temp_in[$indexin]['pending_quantity'] = $valuein['connected_quantity'];

									$xupdate['connected_quantity'] = $temp_in[$indexin]['pending_quantity'] + $hitung;
									$temp_in[$indexin]['pending_quantity'] = $xupdate['connected_quantity'];
									$arrin[$indexin]['pending_quantity'] = $valuein['pending_quantity'] - $temp_in[$indexin]['pending_quantity'] + $valuein['connected_quantity']; 
									$alpha_quantity = $hitung;
									$xupdate['pending_quantity'] = $arrin[$indexin]['pending_quantity'];
									$hitung = 0;
								} else {
									$hitung = $hitung - $valuein['pending_quantity'];
									$alpha_quantity = $valuein['pending_quantity'];
									$arrin[$indexin]['pending_quantity'] = 0;
									$xupdate['pending_quantity'] = 0;
									$xupdate['connected_quantity'] = $valuein['connected_quantity'] + $valuein['pending_quantity'];
									$xupdate['connected_status'] = 2;
								}
								//print_r($xupdate);
								$this->update_global('terima_produk', $xupdate, ['id_terima_kemasan' => $valuein['id_terima_kemasan']]);


								$xdata['id_terima_kemasan'] = $valuein['id_terima_kemasan'];
								$xdata['package_id'] = $valueout['package_id'];
								$xdata['package_detail_id'] = $valueout['package_detail_id'];
								$xdata['bridge_alpha_quantity'] = $alpha_quantity;
								$xdata['bridge_alpha_hpp'] = $valuein['hpp'];
								$xdata['bridge_alpha_user_id'] = @$_SESSION['user_id'];
								$xdata['product_id'] = $product_id;
								$xdata['bridge_alpha_pending_quantity'] = $alpha_quantity;
								$this->insert_global('t_bridge_alpha', $xdata);

								//print_r($xdata);
							}
						}
					}
					$xupdate2 = array();
					$xupdate2['connected_alpha_quantity'] = $valueout['connected_alpha_quantity'] + $valueout['pending_alpha_quantity'] - $hitung;
					$xupdate2['pending_alpha_quantity'] = $hitung;
					if ($hitung == 0) $xupdate2['connected_alpha_status'] = 2;
					$this->update_global('t_package_trial_detail', $xupdate2, ['package_detail_id' => $valueout['package_detail_id']]);
				}
			}
		}
		//$arrin = $this->db->query("SELECT * FROM terima_produk WHERE id_barang = '$product_id' AND pending_quantity > 0")->result_array();
		//$arrout = $this->db->query("SELECT * FROM t_package_trial_detail WHERE product_id = '$product_id' AND pending_alpha_quantity = 0")->result_array();
		return '';
	}

	function check_fifo_beta($product_id)
	{
		$arrglobal = $this->db->query("SELECT * FROM produk A WHERE A.id_produk = '$product_id'")->row_array();
		$arrglobalkd = $arrglobal['kd_pd'];
		$arrin = $this->db->query("SELECT A.seller_id, B.*, D.sales_detail_price FROM t_sales A LEFT JOIN t_account_detail_delivery_product B ON B.sales_id = A.sales_id JOIN produk C ON C.id_produk = B.product_id JOIN t_sales_detail D ON D.sales_detail_id = B.sales_detail_id WHERE C.kd_pd = '$arrglobalkd' AND B.pending_quantity > 0 AND B.account_detail_delivery_product_status = 1 AND B.account_detail_delivery_product_realization = 0")->result_array();
		$arrinfix = array();

		foreach ($arrin as $indexinawal => $valueinawal) {
			$arrinfix[$valueinawal['seller_id']][] = $valueinawal;
		}
		$arrout = $this->db->query("SELECT A.member_code, B.* FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN produk C ON C.id_produk = B.product_id WHERE C.kd_pd = '$arrglobalkd' AND B.pending_beta_quantity > 0")->result_array();
		$arroutfix = array();
		foreach ($arrout as $indexoutawal => $valueoutawal) {
			$arroutfix[$valueoutawal['member_code']][] = $valueoutawal;
		}
		$hitung = 0;
		foreach ($arrinfix as $indexinfix => $valueinfix) {


			//echo @count(@$arroutfix[@$indexinfix]);
			if (!empty(@$arroutfix[$indexinfix]) > 0) {
				foreach ($arroutfix[$indexinfix] as $indexout => $valueout) {
					if ($hitung == 0) {
						$hitung = $valueout['pending_beta_quantity'];
						$valueout['package_detail_id'];
						$valueout['package_id'];
						foreach ($valueinfix as $indexin => $valuein) {

							if ($hitung != 0 && $valuein['pending_quantity'] != 0) {
								$xdata = array();
								$xupdate = array();
								if ($hitung < $valuein['pending_quantity']) {

									if (@$temp_in[$indexin]['pending_quantity'] == 0 || @$temp_in[$indexin]['pending_quantity'] == "") $temp_in[$indexin]['pending_quantity'] = $valuein['connected_quantity'];

									$xupdate['connected_quantity'] = $temp_in[$indexin]['pending_quantity'] + $hitung;
									$temp_in[$indexin]['pending_quantity'] = $xupdate['connected_quantity'];
									$arrin[$indexin]['pending_quantity'] = $valuein['pending_quantity'] - $temp_in[$indexin]['pending_quantity'] + $valuein['connected_quantity'];
									$alpha_quantity = $hitung;
									$xupdate['pending_quantity'] = $arrin[$indexin]['pending_quantity'];
									$hitung = 0;
								} else {
									$hitung = $hitung - $valuein['pending_quantity'];
									$alpha_quantity = $valuein['pending_quantity'];
									$arrin[$indexin]['pending_quantity'] = 0;
									$xupdate['pending_quantity'] = 0;
									$xupdate['connected_quantity'] = $valuein['connected_quantity'] + $valuein['pending_quantity'];
									$xupdate['connected_status'] = 2;
								}
								$valueinfix[$indexin]['pending_quantity'] = $xupdate['pending_quantity'];
								$valueinfix[$indexin]['connected_quantity'] = $xupdate['connected_quantity']; 
								//print_r($xupdate);
								$this->update_global('t_sales_detail', $xupdate, ['sales_detail_id' => $valuein['sales_detail_id']]);

								$xdata['sales_id'] = $valuein['sales_id'];
								$xdata['sales_detail_id'] = $valuein['sales_detail_id'];
								$xdata['package_id'] = $valueout['package_id'];
								$xdata['package_detail_id'] = $valueout['package_detail_id'];
								$xdata['bridge_beta_quantity'] = $alpha_quantity;
								$xdata['bridge_beta_price'] = $valuein['sales_detail_price'];
								$xdata['bridge_beta_user_id'] = @$_SESSION['user_id'];
								$xdata['product_kd'] = $arrglobalkd;
								$xdata['bridge_beta_pending_quantity'] = $alpha_quantity;
								//print_r($xdata);
								$this->insert_global('t_bridge_beta', $xdata);
							}
						}
						$xupdate2 = array();
						$xupdate2['connected_beta_quantity'] = $valueout['connected_beta_quantity'] + $valueout['pending_beta_quantity'] - $hitung;
						$xupdate2['pending_beta_quantity'] = $hitung;
						if ($hitung == 0) $xupdate2['connected_beta_status'] = 2;
						$this->update_global('t_package_trial_detail', $xupdate2, ['package_detail_id' => $valueout['package_detail_id']]);
					}
				}
			}
		}
		//die();
		//$arrin = $this->db->query("SELECT A.seller_id, B.* FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN produk C ON C.id_produk = B.product_id WHERE C.kd_pd = '$arrglobalkd' AND B.pending_quantity > 0")->result_array();
		//$arrout = $this->db->query("SELECT A.member_code, B.* FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN produk C ON C.id_produk = B.product_id WHERE C.kd_pd = '$arrglobalkd' AND B.pending_beta_quantity > 0")->result_array();
		return '';
	}

	function check_fifo_main($product_id, $kode_member, $schema_id, $seller_id)
	{
		$arrglobal = $this->db->query("SELECT * FROM produk A WHERE A.id_produk = '$product_id'")->row_array();
		$arrglobalkd = $arrglobal['kd_pd'];
		$arrin = $this->db->query("SELECT * FROM t_bridge_alpha A JOIN produk B ON B.id_produk = A.product_id WHERE B.kd_pd = '$arrglobalkd' AND A.bridge_alpha_pending_quantity > 0")->result_array();
		$arrinfix = array();
		foreach ($arrin as $indexinawal => $valueinawal) {
			$arrinfix[$valueinawal['package_detail_id']][] = $valueinawal;
		}
		$arrout = $this->db->query("SELECT * FROM t_bridge_beta A WHERE A.product_kd = '$arrglobalkd' AND A.bridge_beta_pending_quantity > 0")->result_array();
		$arroutfix = array();
		foreach ($arrout as $indexoutawal => $valueoutawal) {
			$arroutfix[$valueoutawal['package_detail_id']][] = $valueoutawal;
		}
		$hitung = 0;
		foreach ($arrinfix as $indexinfix => $valueinfix) {
			//echo @count(@$arroutfix[@$indexinfix]);
			if (!empty(@$arroutfix[$indexinfix]) > 0) {

				foreach ($arroutfix[$indexinfix] as $indexout => $valueout) {
					if ($hitung == 0) {

						$hitung = $valueout['bridge_beta_pending_quantity'];
						foreach ($valueinfix as $indexin => $valuein) {

							if ($hitung != 0 && $valuein['bridge_alpha_pending_quantity'] != 0) {
								$xdata = array();
								$xupdate = array();

								if ($hitung < $valuein['bridge_alpha_pending_quantity']) {

									if (@$temp_in[$indexin]['bridge_alpha_pending_quantity'] == 0 || @$temp_in[$indexin]['bridge_alpha_pending_quantity'] == "") $temp_in[$indexin]['bridge_alpha_pending_quantity'] = $valuein['bridge_alpha_connected_quantity'];

									$xupdate['bridge_alpha_connected_quantity'] = $temp_in[$indexin]['bridge_alpha_pending_quantity'] + $hitung;
									$temp_in[$indexin]['bridge_alpha_pending_quantity'] = $xupdate['bridge_alpha_connected_quantity'];
									$arrin[$indexin]['bridge_alpha_pending_quantity'] = $valuein['bridge_alpha_pending_quantity'] - $temp_in[$indexin]['bridge_alpha_pending_quantity'] + $valuein['bridge_alpha_connected_quantity'];
									$alpha_quantity = $hitung;
									$xupdate['bridge_alpha_pending_quantity'] = $arrin[$indexin]['bridge_alpha_pending_quantity'];
									$hitung = 0;
								} else {
									$hitung = $hitung - $valuein['bridge_alpha_pending_quantity'];
									$alpha_quantity = $valuein['bridge_alpha_pending_quantity'];
									$arrin[$indexin]['bridge_alpha_connected_quantity'] = 0;
									$xupdate['bridge_alpha_pending_quantity'] = 0;
									$xupdate['bridge_alpha_connected_quantity'] = $valuein['bridge_alpha_connected_quantity'] + $valuein['bridge_alpha_pending_quantity'];
									$xupdate['bridge_alpha_connected_status'] = 2;
								}
								if ($schema_id > 1) {
									$kodeProd = array();
									$kodeProd[] = $valuein['product_id'];
									$priceProd = array();
									$priceProd[] = $valueout['bridge_beta_price'] * $alpha_quantity;
									$qtyProd = array();
									$qtyProd[] = $alpha_quantity;
									$diskonawal = $this->cek_discount_bayar_clinic($valueout['sales_id'], $kodeProd, $priceProd, $qtyProd, $seller_id);
								}

								$discountProduct = $this->update_account_detail_sales_product($valueout['sales_detail_id'], $alpha_quantity);
								$discountProduct = $discountProduct + @$diskonawal;
								$this->update_global('t_bridge_alpha', $xupdate, ['bridge_alpha_id' => $valuein['bridge_alpha_id']]);
								$xdata['sales_id'] = $valueout['sales_id'];
								$xdata['id_terima_kemasan'] = $valuein['id_terima_kemasan'];
								$xdata['package_id'] = $valueout['package_id'];
								$xdata['package_detail_id'] = $valueout['package_detail_id'];
								$xdata['sales_id'] = $valueout['sales_id'];
								$xdata['sales_detail_id'] = $valueout['sales_detail_id'];
								$xdata['bridge_quantity'] = $alpha_quantity;
								$xdata['bridge_hpp'] = $valuein['bridge_alpha_hpp'];
								$xdata['bridge_price'] = $valueout['bridge_beta_price'];
								$xdata['bridge_user_id'] = @$_SESSION['user_id'];
								$xdata['product_kd'] = $arrglobalkd;
								$xdata['product_id'] = $valuein['product_id'];
								$xdata['bridge_alpha_id'] = $valuein['bridge_alpha_id'];
								$xdata['bridge_beta_id'] = $valueout['bridge_beta_id'];
								$id_t_bridge = $this->insert_global('t_bridge', $xdata);
								$nominal = $xdata['bridge_hpp'] * $xdata['bridge_quantity'];
								$utang = $xdata['bridge_price'] * $xdata['bridge_quantity'];
								$idHeader = $this->insert_coa($product_id, $id_t_bridge, $nominal);
								$sales = $this->db->get_where('t_sales', ['sales_id' => $valueout['sales_id']])->row_array();
								$sales_type = $sales['sales_type'];
								$this->input_utang($utang, $kode_member, $idHeader, $id_t_bridge, $sales_type, $discountProduct);
							}
						}

						$xupdate2 = array();
						$xupdate2['bridge_beta_connected_quantity'] = $valueout['bridge_beta_connected_quantity'] + $valueout['bridge_beta_pending_quantity'] - $hitung;
						$xupdate2['bridge_beta_pending_quantity'] = $hitung;
						if ($hitung == 0) $xupdate2['bridge_beta_connected_status'] = 2;
						$this->update_global('t_bridge_beta', $xupdate2, ['bridge_beta_id' => $valueout['bridge_beta_id']]);
					}
				}
			}
		}
		//die();
		//die();

		//$arrin = $this->db->query("SELECT * FROM t_bridge_alpha A JOIN produk B ON B.id_produk = A.product_id WHERE B.kd_pd = '$arrglobalkd'")->result_array();
		//$arrout = $this->db->query("SELECT * FROM t_bridge_beta A WHERE A.product_kd = '$arrglobalkd'")->result_array();
		return '';
	}

	function update_account_detail_sales_product($sales_detail_id, $quantity)
	{
		//echo "SELECT * FROM t_account_detail_sales_product A WHERE A.sales_detail_id = '$sales_detail_id' AND A.pending_quantity > 0"."<hr>";
		$arrdata = $this->db->query("SELECT * FROM t_account_detail_sales_product A WHERE A.sales_detail_id = '$sales_detail_id' AND A.pending_quantity > 0")->result_array();
		$jum_discount = 0;
		foreach ($arrdata as $index => $value) {
			if ($quantity > 0) {
				if ($quantity >= $value['account_detail_sales_product_allow']) {
					$quantity = $quantity - $value['account_detail_sales_product_allow'];
					$val_update = $value['account_detail_sales_product_allow'];
				} else {
					$val_update = $value['account_detail_sales_product_allow'] - $quantity;
					$quantity = 0;
				}
				$this->db->query("UPDATE t_account_detail_sales_product SET pending_quantity = pending_quantity - $val_update, connected_quantity = connected_quantity + $val_update WHERE account_detail_sales_product_id = '" . $value['account_detail_sales_product_id'] . "'");
				// if ($value['account_detail_sales_product_price'] != "") $jum_discount += ($val_update * $value['account_detail_sales_product_price']);
				if ($value['account_detail_sales_product_discount'] != "") $jum_discount += ($val_update * $value['account_detail_sales_product_discount']);
			}
			$account_detail = $this->db->query("SELECT * FROM t_account_detail WHERE account_detail_real_id = '$value[account_detail_real_id]'")->row_array();
			if ($account_detail['account_detail_paid_type'] == 2 && $value['account_detail_sales_product_discount'] != "") {
				$this->db->query("UPDATE m_account SET account_deposit = account_deposit + " . ($val_update * $value['account_detail_sales_product_discount']) . " WHERE account_id = $account_detail[account_id]");
			}
		}
		return $jum_discount;
	}

	function input_utang($total, $kode_member, $idHeader, $id_t_bridge, $sales_type, $discountProduct)
	{
		$this->chk_account($kode_member);
		$member = $this->db->query("SELECT A.*, B.nama, B.kode FROM m_account A JOIN member B ON B.kode = A.seller_id AND B.kode = '$kode_member'")->row_array();
		$coaPiutang = $this->db->get_where('t_coa_total', ['coa_name' => 'Piutang ' . str_replace("'", "", $member['nama']) . ' (' . $member['seller_id'] . ')'])->row_array();
		$coaUtang = $this->db->get_where('t_coa_total', ['coa_name' => 'Utang ' . str_replace("'", "", $member['nama']) . ' (' . $member['seller_id'] . ')'])->row_array();
		$coaPenjualan = $this->db->get_where('t_coa_total', ['coa_name' => 'Penjualan ' . str_replace("'", "", $member['nama']) . ' (' . $member['seller_id'] . ')'])->row_array();
		$coaDeposit = $this->db->get_where('t_coa_total', ['coa_name' => 'Deposit ' . str_replace("'", "", $member['nama']) . ' (' . $member['seller_id'] . ')'])->row_array();
		$coaDiskon = $this->db->get_where('t_coa_total', ['coa_name' => 'Diskon ' . str_replace("'", "", $member['nama']) . ' (' . $member['seller_id'] . ')'])->row_array();
		$coaPajak = $this->db->get_where('t_coa_total', ['coa_id' => 195, 'coa_level' => 3])->row_array();

		if ($coaUtang == null) {
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 23])->result_array();
			$coa3 = $this->db->get_where('coa_3', ['id' => 23])->row_array();
			$utang = ([
				'kode' => $coa3['kode'] . '.' . (count($coa4) + 1),
				'nama' => 'Utang ' . str_replace("'", "", $member['nama']) . ' (' . $member['seller_id'] . ')',
				'coa3_id' => 23,
			]);
			$this->db->insert('coa_4', $utang);
			$utang['id'] = $this->db->insert_id();
			$coaUtang = ([
				'coa_id' => $utang['id'],
				'coa_code' => $utang['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $utang['nama'],
			]);
			$this->db->insert('t_coa_total', $coaUtang);
			$id = $this->db->insert_id();
			$coaUtang['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $coaUtang);
			$coaUtang = $this->db->get_where('t_coa_total', ['coa_total_id' => $id])->row_array();
		}
		if ($coaPiutang == null) {
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 12])->result_array();
			$coa3 = $this->db->get_where('coa_3', ['id' => 12])->row_array();
			$piutang = ([
				'kode' => $coa3['kode'] . '.' . (count($coa4) + 1),
				'nama' => 'Piutang ' . str_replace("'", "", $member['nama']) . ' (' . $member['seller_id'] . ')',
				'coa3_id' => 23,
			]);
			$this->db->insert('coa_4', $piutang);
			$piutang['id'] = $this->db->insert_id();
			$coaPiutang = ([
				'coa_id' => $piutang['id'],
				'coa_code' => $piutang['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $piutang['nama'],
			]);
			$this->db->insert('t_coa_total', $coaPiutang);
			$id = $this->db->insert_id();
			$coaPiutang['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $coaPiutang);
			$coaPiutang = $this->db->get_where('t_coa_total', ['coa_total_id' => $id])->row_array();
		}
		if ($coaPenjualan == null) {
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 40])->result_array();
			$coa3 = $this->db->get_where('coa_3', ['id' => 40])->row_array();
			$penjualan = ([
				'kode' => $coa3['kode'] . '.' . (count($coa4) + 1),
				'nama' => 'Penjualan ' . str_replace("'", "", $member['nama']) . ' (' . $member['seller_id'] . ')',
				'coa3_id' => 40,
			]);
			$this->db->insert('coa_4', $penjualan);
			$penjualan['id'] = $this->db->insert_id();
			$coaPenjualan = ([
				'coa_id' => $penjualan['id'],
				'coa_code' => $penjualan['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $penjualan['nama'],
			]);
			$this->db->insert('t_coa_total', $coaPenjualan);
			$id = $this->db->insert_id();
			$coaPenjualan['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $coaPenjualan);
			$coaPenjualan = $this->db->get_where('t_coa_total', ['coa_total_id' => $id])->row_array();
		}
		if ($coaDeposit == null) {
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 24])->result_array();
			$coa3 = $this->db->get_where('coa_3', ['id' => 24])->row_array();
			$deposit = ([
				'kode' => $coa3['kode'] . '.' . (count($coa4) + 1),
				'nama' => 'Deposit ' . str_replace("'", "", $member['nama']) . ' (' . $member['seller_id'] . ')',
				'coa3_id' => 40,
			]);
			$this->db->insert('coa_4', $deposit);
			$deposit['id'] = $this->db->insert_id();
			$coaDeposit = ([
				'coa_id' => $deposit['id'],
				'coa_code' => $deposit['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $deposit['nama'],
			]);
			$this->db->insert('t_coa_total', $coaDeposit);
			$id = $this->db->insert_id();
			$coaDeposit['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $coaDeposit);
			$coaDeposit = $this->db->get_where('t_coa_total', ['coa_total_id' => $id])->row_array();
		}
		if ($coaDiskon == null) {
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 49])->result_array();
			$coa3 = $this->db->get_where('coa_3', ['id' => 49])->row_array();
			$diskon = ([
				'kode' => $coa3['kode'] . '.' . (count($coa4) + 1),
				'nama' => 'Diskon ' . str_replace("'", "", $member['nama']) . ' (' . $member['seller_id'] . ')',
				'coa3_id' => 40,
			]);
			$this->db->insert('coa_4', $diskon);
			$diskon['id'] = $this->db->insert_id();
			$coaDiskon = ([
				'coa_id' => $diskon['id'],
				'coa_code' => $diskon['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $diskon['nama'],
			]);
			$this->db->insert('t_coa_total', $coaDiskon);
			$id = $this->db->insert_id();
			$coaDiskon['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $coaDiskon);
			$coaDiskon = $this->db->get_where('t_coa_total', ['coa_total_id' => $id])->row_array();
		}


		$totalAll = $total;
		$totalAll -= $discountProduct;
		if ($totalAll > 0) {
			$ppn = $totalAll / 11;
		} else {
			$ppn = $discountProduct / 11;
		}
		$totPenjualan = $total - $ppn;
		$hitung = ($coaPiutang['coa_total_debit'] - $coaPiutang['coa_total_credit']) - ($coaUtang['coa_total_credit'] - $coaUtang['coa_total_debit']);
		$deposit = $coaDeposit['coa_total_credit'] - $coaDeposit['coa_total_debit'];
		if ($totalAll > 0) {
			if ($sales_type == 0) {
				$deposit = ([
					'coa_name' => $coaDeposit['coa_name'],
					'coa_code' => $coaDeposit['coa_code'],
					'coa_date' => date('Y-m-d'),
					'coa_level' => 4,
					'coa_debit' => $totalAll,
					'coa_transaction_note' => 'Barang Keluar ' . $member['nama'],
					'date_create' => date('Y-m-d H:i:s'),
					'user_create' => @$_SESSION['user_id'],
					'coa_transaction_source' => 11,
					'coa_transaction_source_id' => $id_t_bridge,
					'coa_id' => $coaDeposit['coa_id'],
					'coa_transaction_realization' => 0,
					'coa_group_id' => $idHeader,
				]);
				$this->db->insert('t_coa_transaction', $deposit);
				$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $deposit[coa_debit] WHERE coa_id = $deposit[coa_id] AND coa_level = '$deposit[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $deposit[coa_debit] WHERE coa_id = $deposit[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $deposit['coa_date'] . "', '-', '') AND coa_level = '$deposit[coa_level]'");

				$penjualan = ([
					'coa_name' => $coaPenjualan['coa_name'],
					'coa_code' => $coaPenjualan['coa_code'],
					'coa_date' => date('Y-m-d'),
					'coa_level' => 4,
					'coa_credit' => $totPenjualan,
					'coa_transaction_note' => 'Barang Keluar ' . $member['nama'],
					'date_create' => date('Y-m-d H:i:s'),
					'user_create' => @$_SESSION['user_id'],
					'coa_transaction_source' => 11,
					'coa_transaction_source_id' => $id_t_bridge,
					'coa_id' => $coaPenjualan['coa_id'],
					'coa_transaction_realization' => 0,
					'coa_group_id' => $idHeader,
				]);
				$this->db->insert('t_coa_transaction', $penjualan);
				$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $penjualan[coa_credit] WHERE coa_id = $penjualan[coa_id] AND coa_level = '$penjualan[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $penjualan[coa_credit] WHERE coa_id = $penjualan[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $penjualan['coa_date'] . "', '-', '') AND coa_level = '$penjualan[coa_level]'");
			} else if ($deposit > 0) {

				$akunDeposit = ([
					'coa_name' => $coaDeposit['coa_name'],
					'coa_code' => $coaDeposit['coa_code'],
					'coa_date' => date('Y-m-d'),
					'coa_level' => 4,
					'coa_transaction_note' => 'Barang Keluar ' . $member['nama'],
					'date_create' => date('Y-m-d H:i:s'),
					'user_create' => @$_SESSION['user_id'],
					'coa_transaction_source' => 11,
					'coa_transaction_source_id' => $id_t_bridge,
					'coa_id' => $coaDeposit['coa_id'],
					'coa_transaction_realization' => 0,
					'coa_group_id' => $idHeader,
				]);
				if ($totalAll > $deposit) {
					$akunDeposit['coa_debit'] = $deposit;
				} else {
					$akunDeposit['coa_debit'] = $deposit - $totalAll;
				}
				$this->db->insert('t_coa_transaction', $akunDeposit);
				$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $akunDeposit[coa_debit] WHERE coa_id = $akunDeposit[coa_id] AND coa_level = '$akunDeposit[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $akunDeposit[coa_debit] WHERE coa_id = $akunDeposit[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunDeposit['coa_date'] . "', '-', '') AND coa_level = '$akunDeposit[coa_level]'");
				if ($totalAll > $deposit) {
					$piutang = ([
						'coa_name' => $coaPiutang['coa_name'],
						'coa_code' => $coaPiutang['coa_code'],
						'coa_date' => date('Y-m-d'),
						'coa_level' => 4,
						'coa_transaction_note' => 'Barang Keluar ' . $member['nama'],
						'date_create' => date('Y-m-d H:i:s'),
						'user_create' => @$_SESSION['user_id'],
						'coa_transaction_source' => 11,
						'coa_transaction_source_id' => $id_t_bridge,
						'coa_id' => $coaPiutang['coa_id'],
						'coa_transaction_realization' => 0,
						'coa_group_id' => $idHeader,
					]);
					$piutang['coa_debit'] = $totalAll - $deposit;
					$this->db->insert('t_coa_transaction', $piutang);
					$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $piutang[coa_debit] WHERE coa_id = $piutang[coa_id] AND coa_level = '$piutang[coa_level]'");
					$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $piutang[coa_debit] WHERE coa_id = $piutang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $piutang['coa_date'] . "', '-', '') AND coa_level = '$piutang[coa_level]'");
				}

				$penjualan = ([
					'coa_name' => $coaPenjualan['coa_name'],
					'coa_code' => $coaPenjualan['coa_code'],
					'coa_date' => date('Y-m-d'),
					'coa_level' => 4,
					'coa_credit' => $akunDeposit['coa_debit'] + @$piutang['coa_debit'],
					'coa_transaction_note' => 'Barang Keluar ' . $member['nama'],
					'date_create' => date('Y-m-d H:i:s'),
					'user_create' => @$_SESSION['user_id'],
					'coa_transaction_source' => 11,
					'coa_transaction_source_id' => $id_t_bridge,
					'coa_id' => $coaPenjualan['coa_id'],
					'coa_transaction_realization' => 0,
					'coa_group_id' => $idHeader,
				]);
				$this->db->insert('t_coa_transaction', $penjualan);
				$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $penjualan[coa_credit] WHERE coa_id = $penjualan[coa_id] AND coa_level = '$penjualan[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $penjualan[coa_credit] WHERE coa_id = $penjualan[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $penjualan['coa_date'] . "', '-', '') AND coa_level = '$penjualan[coa_level]'");
			} else if ($sales_type == 1 || $sales_type == 2) {
				$piutang = ([
					'coa_name' => $coaPiutang['coa_name'],
					'coa_code' => $coaPiutang['coa_code'],
					'coa_date' => date('Y-m-d'),
					'coa_level' => 4,
					'coa_debit' => $totalAll,
					'coa_transaction_note' => 'Barang Keluar ' . $member['nama'],
					'date_create' => date('Y-m-d H:i:s'),
					'user_create' => @$_SESSION['user_id'],
					'coa_transaction_source' => 11,
					'coa_transaction_source_id' => $id_t_bridge,
					'coa_id' => $coaPiutang['coa_id'],
					'coa_transaction_realization' => 0,
					'coa_group_id' => $idHeader,
				]);
				$this->db->insert('t_coa_transaction', $piutang);
				$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $piutang[coa_debit] WHERE coa_id = $piutang[coa_id] AND coa_level = '$piutang[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $piutang[coa_debit] WHERE coa_id = $piutang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $piutang['coa_date'] . "', '-', '') AND coa_level = '$piutang[coa_level]'");

				$penjualan = ([
					'coa_name' => $coaPenjualan['coa_name'],
					'coa_code' => $coaPenjualan['coa_code'],
					'coa_date' => date('Y-m-d'),
					'coa_level' => 4,
					'coa_credit' => $totPenjualan,
					'coa_transaction_note' => 'Barang Keluar ' . $member['nama'],
					'date_create' => date('Y-m-d H:i:s'),
					'user_create' => @$_SESSION['user_id'],
					'coa_transaction_source' => 11,
					'coa_transaction_source_id' => $id_t_bridge,
					'coa_id' => $coaPenjualan['coa_id'],
					'coa_transaction_realization' => 0,
					'coa_group_id' => $idHeader,
				]);
				$this->db->insert('t_coa_transaction', $penjualan);
				$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $penjualan[coa_credit] WHERE coa_id = $penjualan[coa_id] AND coa_level = '$penjualan[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $penjualan[coa_credit] WHERE coa_id = $penjualan[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $penjualan['coa_date'] . "', '-', '') AND coa_level = '$penjualan[coa_level]'");
			}
		} else {
			$penjualan = ([
				'coa_name' => $coaPenjualan['coa_name'],
				'coa_code' => $coaPenjualan['coa_code'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_credit' => ($discountProduct / 11 * 10),
				'coa_transaction_note' => 'Barang Keluar ' . $member['nama'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => @$_SESSION['user_id'],
				'coa_transaction_source' => 11,
				'coa_transaction_source_id' => $id_t_bridge,
				'coa_id' => $coaPenjualan['coa_id'],
				'coa_transaction_realization' => 0,
				'coa_group_id' => $idHeader,
			]);
			$this->db->insert('t_coa_transaction', $penjualan);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $penjualan[coa_credit] WHERE coa_id = $penjualan[coa_id] AND coa_level = '$penjualan[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $penjualan[coa_credit] WHERE coa_id = $penjualan[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $penjualan['coa_date'] . "', '-', '') AND coa_level = '$penjualan[coa_level]'");
		}
		if ($discountProduct > 0) {
			$diskon_produk = ([
				'coa_name' => $coaDiskon['coa_name'],
				'coa_code' => $coaDiskon['coa_code'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_debit' => $discountProduct,
				'coa_transaction_note' => 'Barang Keluar ' . $member['nama'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => @$_SESSION['user_id'],
				'coa_transaction_source' => 11,
				'coa_transaction_source_id' => $id_t_bridge,
				'coa_id' => $coaDiskon['coa_id'],
				'coa_transaction_realization' => 0,
				'coa_group_id' => $idHeader,
			]);
			$this->db->insert('t_coa_transaction', $diskon_produk);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $diskon_produk[coa_debit] WHERE coa_id = $diskon_produk[coa_id] AND coa_level = '$diskon_produk[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $diskon_produk[coa_debit] WHERE coa_id = $diskon_produk[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $diskon_produk['coa_date'] . "', '-', '') AND coa_level = '$diskon_produk[coa_level]'");
		}
		$pajak = ([
			'coa_name' => $coaPajak['coa_name'],
			'coa_code' => $coaPajak['coa_code'],
			'coa_date' => date('Y-m-d'),
			'coa_level' => 3,
			'coa_credit' => $ppn,
			'coa_transaction_note' => 'Barang Keluar ' . $member['nama'],
			'date_create' => date('Y-m-d H:i:s'),
			'user_create' => @$_SESSION['user_id'],
			'coa_transaction_source' => 11,
			'coa_transaction_source_id' => $id_t_bridge,
			'coa_id' => $coaPajak['coa_id'],
			'coa_transaction_realization' => 0,
			'coa_group_id' => $idHeader,
		]);
		$this->db->insert('t_coa_transaction', $pajak);
		$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $pajak[coa_credit] WHERE coa_id = $pajak[coa_id] AND coa_level = '$pajak[coa_level]'");
		$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $pajak[coa_credit] WHERE coa_id = $pajak[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $pajak['coa_date'] . "', '-', '') AND coa_level = '$pajak[coa_level]'");
	}

	function chk_account($id)
	{
		$this->db->trans_begin();
		$arrchk = $this->db->query("SELECT seller_id FROM m_account WHERE seller_id = '$id'")->row_array();
		if (@$arrchk['seller_id'] == "") {
			$arrmember = $this->db->query("SELECT nama, sales_category_id FROM member WHERE kode = '$id'")->row_array();
			$create = date('Y-m-d H:i:s');
			$data2['seller_id'] = $id;
			$data2['account_code'] = '81.' . $id;
			$data2['account_name'] = 'Akun ' . $arrmember['nama'] . ' (seller)';
			$data2['account_type_id'] = 1;
			$data2['account_date_create'] = $create;
			$data2['account_date_reset'] = $create;
			$arrcode = $this->db->query("SELECT (IFNULL(max(account_code2), 0) + 1) AS JUM FROM m_account WHERE seller_id IS NOT NULL")->row_array();
			$data2['account_code2'] =  $arrcode['JUM'];
			//if($arrmember['sales_category_id'] == ""){
			//	$data2['sales_category_id'] = @$_SESSION['sales_category_id'];
			//}else{
			$data2['sales_category_id'] = $arrmember['sales_category_id'];
			//}
			$this->db->insert('m_account', $data2);
			$data3['kode'] = '100.1.3.' . $data2['account_code2'];
			$data3['nama'] = 'Piutang ' . $arrmember['nama'] . ' (' . $id . ')';
			$data3['coa3_id'] = 12;
			$this->db->insert('coa_4', $data3);
			$id_coa_1 = $this->db->insert_id();
			$data3['kode'] = '200.1.2.' . $data2['account_code2'];
			$data3['nama'] = 'Deposit ' . $arrmember['nama'] . ' (' . $id . ')';
			$data3['coa3_id'] = 24;
			$this->db->insert('coa_4', $data3);
			$id_coa_2 = $this->db->insert_id();
			$data4 = array();
			$data4['coa_name'] = 'Piutang ' . $arrmember['nama'] . ' (' . $id . ')';
			$data4['coa_code'] = '100.1.3.' . $data2['account_code2'];
			$data4['coa_level'] = 4;
			$data4['coa_id'] = $id_coa_1;
			$this->db->insert('t_coa_total', $data4);
			$data4['coa_total_date'] = substr($tanggal, 0, 7) . "-01";
			$this->db->insert('t_coa_total_history', $data4);
			$data4 = array();
			$data4['coa_name'] = 'Deposit ' . $arrmember['nama'] . ' (' . $id . ')';
			$data4['coa_code'] = '200.1.2.' . $data2['account_code2'];
			$data4['coa_level'] = 4;
			$data4['coa_id'] = $id_coa_2;
			$this->db->insert('t_coa_total', $data4);
			$data4['coa_total_date'] = substr($tanggal, 0, 7) . "-01";
			$this->db->insert('t_coa_total_history', $data4);
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
	}

	function insert_coa($id_product, $id_t_bridge, $nominal)
	{
		$product = $this->db->get_where('produk', ['id_produk' => $id_product])->row_array();
		$coaHPP = $this->db->get_where('coa_4', ['nama' => 'HPP ' . $product['nama_produk']])->row_array();
		if ($coaHPP == null) {
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 87])->result_array();
			$coa3 = $this->db->get_where('coa_3', ['id' => 87])->row_array();
			$coaHPP['kode'] = $coa3['kode'] . '.' . (count($coa4) + 1);
			$coaHPP['nama'] = 'HPP ' . $product['nama_produk'];
			$coaHPP['coa3_id'] = 87;
			$this->db->insert('coa_4', $coaHPP);
			$coaHPP['id'] = $this->db->insert_id();

			$akunTotal = ([
				'coa_id' => $coaHPP['id'],
				'coa_code' => $coaHPP['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $coaHPP['nama'],
			]);
			$this->db->insert('t_coa_total', $akunTotal);
			$akunTotal['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $akunTotal);
		}
		$coaPersediaan = $this->db->get_where('coa_4', ['nama' => 'Persediaan ' . $product['nama_produk']])->row_array();
		if ($coaPersediaan == null) {
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 17])->result_array();
			$coa3 = $this->db->get_where('coa_3', ['id' => 17])->row_array();
			$coaPersediaan['kode'] = $coa3['kode'] . '.' . (count($coa4) + 1);
			$coaPersediaan['nama'] = 'Persediaan ' . $product['nama_produk'];
			$coaPersediaan['coa3_id'] = 17;
			$this->db->insert('coa_4', $coaPersediaan);
			$coaPersediaan['id'] = $this->db->insert_id();

			$akunTotal = ([
				'coa_id' => $coaPersediaan['id'],
				'coa_code' => $coaPersediaan['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $coaPersediaan['nama'],
			]);
			$this->db->insert('t_coa_total', $akunTotal);
			$akunTotal['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $akunTotal);
		}

		$header = ([
			'coa_transaction_date' => date('Y-m-d'),
			'date_create' => date('Y-m-d H:i:s'),
			'user_create' => @$_SESSION['user_id'],
			'coa_transaction_debit' => $nominal,
			'coa_transaction_credit' => $nominal,
			'coa_transaction_payment' => $nominal,
		]);
		$this->db->insert('t_coa_transaction_header', $header);
		$idHeader = $this->db->insert_id();

		$hpp = ([
			'coa_name' => $coaHPP['nama'],
			'coa_code' => $coaHPP['kode'],
			'coa_date' => date('Y-m-d'),
			'coa_level' => 4,
			'coa_debit' => $nominal,
			'coa_transaction_note' => 'Pengiriman Produk ' . $product['nama_produk'],
			'date_create' => date('Y-m-d H:i:s'),
			'user_create' => @$_SESSION['user_id'],
			'coa_transaction_source' => 11,
			'coa_transaction_source_id' => $id_t_bridge,
			'coa_id' => $coaHPP['id'],
			'coa_transaction_realization' => 0,
			'coa_group_id' => $idHeader,
		]);
		$this->db->insert('t_coa_transaction', $hpp);
		$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $hpp[coa_debit] WHERE coa_id = $hpp[coa_id] AND coa_level = '$hpp[coa_level]'");
		$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $hpp[coa_debit] WHERE coa_id = $hpp[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $hpp['coa_date'] . "', '-', '') AND coa_level = '$hpp[coa_level]'");

		$persediaan = ([
			'coa_name' => $coaPersediaan['nama'],
			'coa_code' => $coaPersediaan['kode'],
			'coa_date' => date('Y-m-d'),
			'coa_level' => 4,
			'coa_credit' => $nominal,
			'coa_transaction_note' => 'Pengiriman Produk ' . $product['nama_produk'],
			'date_create' => date('Y-m-d H:i:s'),
			'user_create' => @$_SESSION['user_id'],
			'coa_transaction_source' => 11,
			'coa_transaction_source_id' => $id_t_bridge,
			'coa_id' => $coaPersediaan['id'],
			'coa_transaction_realization' => 0,
			'coa_group_id' => $idHeader,
		]);
		$this->db->insert('t_coa_transaction', $persediaan);
		$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $persediaan[coa_credit] WHERE coa_id = $persediaan[coa_id] AND coa_level = '$persediaan[coa_level]'");
		$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $persediaan[coa_credit] WHERE coa_id = $persediaan[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $persediaan['coa_date'] . "', '-', '') AND coa_level = '$persediaan[coa_level]'");
		return $idHeader;
	}

	function get_data_produk()
	{
		$arrwhere[] = "A.status = 1";
		$arrjoin[] = "LEFT JOIN t_package_trial_detail_draft B ON B.product_id = A.id_produk AND B.package_id IS NULL AND B.line_req IS NULL";
		$table = "produk A";
		$id = 'id_produk';
		$arrgroup[] = "id_produk";
		$arrorder[] = "COUNT(package_detail_id) DESC";
		$field = array('nama_produk', 'COUNT(package_detail_id)');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_produk_detail/xid") . '" class="btn btn-info btn-sm"> <i class="fa fa-edit"></i> Detail</a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_seller()
	{
		$arrwhere[] = "(A.package_id IS NULL)";
		$arrwhere[] = "A.package_detail_quantity > 0";
		$arrjoin[] = "LEFT JOIN member C ON C.kode = A.seller_id";
		$table = "t_package_trial_detail_draft A";
		$id = 'seller_id';
		$arrgroup[] = "seller_id";
		//$arrorder[] = "seller_id";
		$field = array('package_detail_id', 'nama', 'kota');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_confirmation/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i><br />Konfirmasi</a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}
	
	function get_data_transfer()
	{
		$arrwhere[] = "(A.package_id IS NULL)";
		$arrwhere[] = "A.package_detail_quantity > 0";
		$arrwhere[] = "A.seller_id = 'MSGLOWTRANSFER'";
		$arrjoin[] = "LEFT JOIN member C ON C.kode = A.seller_id";
		$table = "t_package_trial_detail_draft A";
		$id = 'seller_id';
		$arrgroup[] = "seller_id";
		//$arrorder[] = "seller_id";
		$field = array('kode', 'nama', 'warehouse_target_id');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_transferx/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i><br />Konfirmasi</a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer['warehouse_target_id'], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_seller2()
	{
		$arrwhere[] = "(A.package_id IS NULL)";
		$arrjoin[] = "LEFT JOIN member C ON C.kode = A.seller_id";
		$table = "t_package_trial_detail_draft A";
		$id = 'seller_id';
		$arrgroup[] = "seller_id";
		//$arrorder[] = "seller_id";
		$field = array('kode', 'nama', 'kota');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_confirmation2/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_package_trial_detail_draft()
	{
		$data = $this->db->query("SELECT A.package_detail_id, A.product_id, D.nama_produk, A.package_detail_quantity, GROUP_CONCAT(C.nama_phl SEPARATOR', ') AS pegawai FROM t_package_trial_detail_draft A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id LEFT JOIN produk D ON D.id_produk = A.product_id WHERE A.package_id = 238 GROUP BY A.package_detail_id")->result_array();
		return $data;
	}

	function get_package_trial_detail_pure($id)
	{
		$data = $this->db->query("SELECT * FROM t_package_trial_detail A WHERE A.package_id = '$id'")->result_array();
		return $data;
	}

	function get_package_trial_detail($id)
	{
		$data = $this->db->query("SELECT A.package_detail_id, A.product_id, D.nama_produk, A.package_detail_quantity, GROUP_CONCAT(C.nama_phl SEPARATOR', ') AS pegawai, F.account_detail_delivery_address AS address FROM t_package_trial_detail A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id LEFT JOIN produk D ON D.id_produk = A.product_id LEFT JOIN t_account_detail_delivery_product E ON E.account_detail_delivery_product_id = A.account_detail_delivery_product_id LEFT JOIN t_account_detail_delivery F ON F.account_detail_delivery_id = E.account_detail_delivery_id WHERE A.package_id = '$id' GROUP BY A.package_detail_id")->result_array();
		return $data;
	}

	function get_package_trial_detail_sj($id)
	{
		$data = $this->db->query("SELECT A.barcode, GROUP_CONCAT(CONCAT('- ', B.nama_produk, ' ', A.qty, ' pcs') SEPARATOR'<br>') as detail_produk, SUM(qty) as total_group FROM android_barcode_line A JOIN produk B ON B.id_produk = A.product_id WHERE confirmation = '$id' GROUP BY A.barcode")->result_array();
		return $data;
	}
	
	function get_package_trial_detail_sjx($id)
	{
		$data = $this->db->query("SELECT GROUP_CONCAT(CONCAT( B.nama_produk) SEPARATOR'<br>') as detail_produk, SUM(A.package_detail_quantity) as total_group FROM t_package_trial_detail A JOIN produk B ON B.id_produk = A.product_id WHERE A.package_id = '$id' GROUP BY A.product_id")->result_array();
		return $data;
	}


	function get_package_trial_detail2()
	{
		$data = $this->db->query("SELECT A.package_detail_id, A.product_id, D.nama_produk, A.package_detail_quantity, GROUP_CONCAT(C.nama_phl SEPARATOR', ') AS pegawai FROM t_package_trial_detail A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id LEFT JOIN produk D ON D.id_produk = A.product_id WHERE A.package_id = 238 GROUP BY A.package_detail_id")->result_array();
		return $data;
	}

	function get_data_sales()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN m_seller B ON B.seller_id = A.seller_id";
		$table = "t_sales A";
		$id = 'sales_id';
		$field = array('sales_code', 'seller_code', 'seller_name', 'sales_date', 'sales_status');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_package_trial($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN member B ON B.kode = A.member_code WHERE A.package_id = '$id'")->row_array();
		return $arrdata;
	}

	function get_package_trial_scan($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial_scan A LEFT JOIN member B ON B.kode = A.member_code WHERE A.package_id = '$id'")->row_array();
		return $arrdata;
	}

	function get_package_trial5($id)
	{
		$arrdata = $this->db->query("SELECT X.*, AX.product_id, SUM(AX.qty) as qty FROM (SELECT GROUP_CONCAT('', B.nama_produk) as produk, A.sellerid, A.barcode FROM android_barcode_line A JOIN produk B ON B.id_produk = A.product_id WHERE sellerid = 'MSGLOW056.04.51.13' GROUP BY A.barcode) X JOIN android_barcode_line AX ON AX.barcode = X.barcode GROUP BY X.produk, AX.product_id")->row_array();
		return $arrdata;
	}

	function get_package_trial_detail3($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial_detail_draft B LEFT JOIN member C ON C.kode = B.seller_id WHERE B.product_id = '$id' AND B.package_detail_confirm_quantity = 0")->result_array();
		return $arrdata;
	}

	function get_pembagian($id)
	{
		$arrdata = $this->db->query("SELECT SUM(package_detail_quantity) jum, seller_id FROM t_package_trial_detail_draft  WHERE package_detail_quantity > 0  AND product_id = '$id' AND line_req = 1 GROUP BY seller_id")->result_array();
		return $arrdata;
	}

	function get_package_trial_detail_draft2($id)
	{
		$arrdata = $this->db->query("SELECT B.*, C.*, SUM(D.qty) as qty_konfirmasi FROM t_package_trial_detail_draft B LEFT JOIN produk C ON C.id_produk = B.product_id LEFT JOIN android_barcode_line D ON D.sellerid = B.seller_id AND D.product_id = B.product_id AND D.shipping = 1 AND B.supplay = 1 AND shipping_time IS NOT NULL AND D.confirmation = 0 WHERE B.seller_id = '$id' AND B.package_id IS NULL GROUP BY B.product_id")->result_array();
		return $arrdata;
	}
	
	function get_package_trial_detail_draft2xx($id)
	{
		$arrdata = $this->db->query("SELECT B.*, C.* FROM t_package_trial_detail_draft B LEFT JOIN produk C ON C.id_produk = B.product_id WHERE B.seller_id = 'MSGLOWTRANSFER' AND B.warehouse_target_id = '$id' AND B.package_detail_quantity > 0")->result_array();
		return $arrdata;
	}
	
	function get_package_trial_detail_draft2x($id)
	{
		// $arrdata = $this->db->query("SELECT B.*, C.*, E.account_detail_delivery_address FROM t_package_trial_detail_draft B 
		// 	LEFT JOIN produk C ON C.id_produk = B.product_id 
		// 	JOIN t_account_detail_delivery_product D ON D.account_detail_delivery_product_id = B.account_detail_delivery_product_id 
		// 	JOIN t_account_detail_delivery E ON E.account_detail_delivery_id = D.account_detail_delivery_id 
		// 	WHERE B.seller_id = '$id' AND B.package_detail_quantity > 0")->result_array();

		$arrdata = $this->db->query("SELECT B.*, C.* FROM t_package_trial_detail_draft B 
			LEFT JOIN produk C ON C.id_produk = B.product_id 
			WHERE B.seller_id = '$id' AND B.package_detail_quantity > 0")->result_array();
		return $arrdata;
	}

	function insert_reset3($id)
	{
		//$this->db->query("DELETE FROM t_package_trial_employee WHERE product_id = '$id' AND package_detail_id IN(SELECT package_detail_id FROM t_package_trial_detail_draft WHERE package_id IS NULL AND line_req IS NULL)");
		//$this->db->query("DELETE FROM t_package_trial_detail_draft WHERE product_id = '$id' AND package_id IS NULL AND line_req IS NULL");
		$this->db->query("DELETE FROM t_package_trial_detail_draft WHERE product_id = '$id' AND (package_detail_confirm_quantity = 0 OR package_detail_quantity = 0)"); 
		//$this->db->query("DELETE FROM t_package_trial_detail_draft WHERE product_id = '$id'");
		return '';
	}

	function update_confirm($id, $id2)
	{
		$this->db->query("UPDATE t_package_trial_detail_draft SET package_id = '$id' WHERE seller_id = '$id2'");
		return '';
	}

	function insert_reset($id)
	{
		$this->db->query("DELETE FROM t_package_trial_detail_draft WHERE package_id = '$id'");
		$this->db->query("DELETE FROM t_package_trial_employee WHERE package_id = '$id'");
		return '';
	}

	function get_package_trial_employee3($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial_detail_draft A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.product_id = '$id' AND A.package_id IS NULL")->result_array();
		return $arrdata;
	}

	function get_package_trial_employee4($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial_detail_draft A LEFT JOIN t_package_trial_employee B ON B.package_detail_id = A.package_detail_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.seller_id = '$id' AND A.package_id IS NULL")->result_array();
		return $arrdata;
	}

	function get_package_trial_employee($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_employee B ON B.package_id = A.package_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.package_id = '$id'")->result_array();
		return $arrdata;
	}

	function get_data_postage()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN t_sales B ON B.sales_id = A.sales_id";
		$arrjoin[] = "LEFT JOIN m_seller C ON C.seller_id = B.seller_id";
		$table = "t_package A";
		$id = 'package_id';
		$field = array('package_code', 'sales_code', 'seller_name', 'sales_address', 'package_postage');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_postage/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_package($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package WHERE package_id = '" . $id . "'")->row_array();
		return $arrdata;
	}

	function get_package_all($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package A LEFT JOIN t_sales B ON B.sales_id = A.sales_id LEFT JOIN m_seller C ON C.seller_id = B.seller_id LEFT JOIN m_account D ON D.seller_id = C.seller_id WHERE A.package_id = '" . $id . "'")->row_array();
		return $arrdata;
	}

	function get_sales_detail($id)
	{
		$arrdata = $this->db->query("SELECT SUM(total) as total, nama_produk, id_produk, sales_id FROM v_pack_product A WHERE sales_id = '" . $id . "' GROUP BY A.id_produk")->result_array();
		return $arrdata;
	}

	function get_package_detail_date($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_detail A LEFT JOIN t_package B ON B.package_id = A.package_id WHERE sales_id = '" . $id . "' GROUP BY package_date")->result_array();
		return $arrdata;
	}

	function get_package_detail_sum($id)
	{
		$arrdata = $this->db->query("SELECT SUM(package_detail_quantity) AS total, product_id, package_date FROM t_package_detail A LEFT JOIN t_package B ON B.package_id = A.package_id WHERE sales_id = '" . $id . "' GROUP BY package_date, product_id")->result_array();
		foreach ($arrdata as $key => $value) {
			$arrdatax[$value['package_date']][$value['product_id']] = $value['total'];
		}
		return @$arrdatax;
	}

	function get_package_detail($id)
	{
		$this->db->where('sales_id', $id);
		$this->db->join('t_package', 't_package.package_id = t_package_detail.package_id');
		$this->db->join('m_user', 'm_user.user_id = t_package.user_id');
		$this->db->join('produk', 'produk.id_produk = t_package_detail.product_id');
		$data = $this->db->get('t_package_detail');
		return $data->result_array();
	}

	function get_sales($id)
	{
		$this->db->where('sales_id', $id);
		$data = $this->db->get('t_sales');
		return $data->row_array();
	}

	function get_sales_by_seller($id)
	{
		$this->db->where('seller_id', $id);
		$data = $this->db->get('t_sales');
		return $data->row_array();
	}

	function get()
	{
		$data = $this->db->get($this->table_);
		return $data;
	}

	function search_member($search)
	{
		$search = str_replace('%20', ' ', $search);
		//echo "SELECT nama, kode, kota FROM member WHERE nama LIKE '%$search%' OR kode LIKE '%$search%'";
		//sdie(); 
		$data = $this->db->query("SELECT nama, kode, kota FROM member WHERE nama LIKE '%$search%' OR kode LIKE '%$search%'")->result_array();
		$dat = array();
		foreach ($data as $index => $value) {
			$dat[$index] = array();
			$dat[$index]['id'] = $value["kode"];
			$dat[$index]['text'] = $value["nama"] . ' (' . $value["kode"] . ') (' . $value["kota"] . ')';
			//echo json_encode($dat[$index]);
		}
		$array = array(
			'results' => $dat,
			'pagination' => array('more' => true)
		);
		echo json_encode($array);
	}

	function search_member2($search)
	{
		$data = $this->db->query("SELECT nama, kode, kota FROM member WHERE nama LIKE '%$search%' OR kode LIKE '%$search%'")->result_array();
		echo '{
			"results": [';
			$i = 0;
			foreach ($data as $index => $value) {
				if ($i > 0) {
					echo ',';
				}
				echo '
				{
					"id": "' . $value["kode"] . '",
					"text": "' . $value["nama"] . ' (' . $value["kode"] . ') (' . $value["kota"] . ')"
				}';
				$i++;
			}
			echo '],
			"pagination": {
				"more": true
			}
		}';
	}

	function get_warehouse2($id)
	{
		$this->db->where('warehouse_id', $id);
		$data = $this->db->get('m_warehouse');
		return $data->row_array();
	}

	function get_member2($id)
	{
		$this->db->where('kode', $id);
		$data = $this->db->get('member');
		return $data->row_array();
	}

	function get_member()
	{
		$data = $this->db->query("SELECT nama, kode FROM member LIMIT 10");
		return $data->result_array();
	}

	function get_seller()
	{
		$data = $this->db->get('m_seller');
		return $data->result_array();
	}

	function get_phl()
	{
		$data = $this->db->query('SELECT A.* FROM m_line A LEFT JOIN m_line_product B ON B.line_id = A.line_id WHERE B.line_id IS NULL');
		return $data->result_array();
	}

	function get_employee()
	{
		$data = $this->db->get('m_employee');
		return $data->result_array();
	}

	function get_produk_row($id)
	{
		//$this->db->join('tb_stock_produk', 'tb_stock_produk.id_stock = produk.id_produk');
		$data = $this->db->query("SELECT *, A.nama_produk FROM produk A LEFT JOIN tb_stock_produk B ON B.id_barang = A.id_produk WHERE A.id_produk = '$id'");
		return $data->row_array();
	}

	function get_produk()
	{
		//$this->db->join('tb_stock_produk', 'tb_stock_produk.id_stock = produk.id_produk');
		$data = $this->db->query('SELECT *, A.nama_produk FROM produk A LEFT JOIN tb_stock_produk B ON B.id_barang = A.id_produk WHERE B.warehouse_id = "'.$_SESSION['warehouse_id'].'" AND A.status = 1');
		return $data->result_array();
	}
	
	function get_warehouse()
	{
		//$this->db->join('tb_stock_produk', 'tb_stock_produk.id_stock = produk.id_produk');
		$data = $this->db->query("SELECT * FROM m_warehouse WHERE warehouse_id != '".$_SESSION['warehouse_id']."'");
		return $data->result_array();
	}

	function insert_id()
	{
		return $this->db->insert_id();
	}

	function get_product()
	{
		$data = $this->db->get('produk');
		return $data->result_array();
	}

	function get_data($tgl_awal, $tgl_akhir)
	{
		if ($tgl_awal != "" && $tgl_akhir != "") {
			$arrwhere[] = "(A.package_date BETWEEN '$tgl_awal' AND '$tgl_akhir')";
		} else {
			$tgl_awal = date('Y-m-d', strtotime(date('Y-m-d') . ' - 1 days'));
			$tgl_akhir = date('Y-m-d');
			$arrwhere[] = "(A.package_date BETWEEN '$tgl_awal' AND '$tgl_akhir')";
		}
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN member B ON B.kode = A.member_code";
		$table = $this->table_ . " A";
		$id = $this->id_;
		$arrorder2[] = 'package_status_id ASC';
		$field = array('package_code', 'nama', 'package_date', 'package_status_id');
		$url = $this->url_;
		if (@$_SESSION['role_id'] == '5') {
			$action = '<a href="' . site_url($url . "/form_detail2/xid") . '" class="btn btn-danger"> <i class="fa fa-print"></i></a> <a href="' . site_url($url . "/cetaksuratjalankemasan/xid") . '" class="btn btn-warning"> <i class="fa fa-print"></i></a> <a href="' . site_url($url . "/cetaksuratjalankemasan2/xid") . '" class="btn btn-success" target="_blank"> <i class="fa fa-print"></i></a> <a href="' . site_url($url . "/form_confirmation_adjustment/xid") . '" class="btn btn-danger"> <i class="fa fa-edit"></i></a>';
		} /*else if (@$_SESSION['role_id'] == '19') {
			$action = '<a href="' . site_url($url . "/form_confirmation_adjustment/xid") . '" class="btn btn-danger"> <i class="fa fa-edit"></i></a>';
		}*/ else {
			$action = '<a href="' . site_url($url . "/form_detail2/xid") . '" class="btn btn-danger"> <i class="fa fa-eye"></i><br />View Detail</a>';
		}
		$action1 = '<a href="' . site_url($url . "/form_employee/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="' . site_url($url . "/form_detail_draft/xid") . '" class="btn btn-danger"> <i class="fa fa-print"></i></a> <a href="' . site_url($url . "/form_confirmation/xid") . '" class="btn btn-warning"> <i class="fa fa-key"></i></a>';

		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach ($arrorder2 as $index => $value) {
			$arrorder[] = $value;
		}
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}

		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		//echo "SELECT $id, $jfield FROM $table $join $where $order $limit";
		//die();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				if ($keyfield == 'package_status_id') {
					$datax[] = (($valuer[$keyfield] == "1") ? "Terkonfirmasi" : "Draft");
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			$datax[] = str_replace('xid', $valuer[$id], (($valuer['package_status_id'] == '1') ? $action : $action1));
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_delivery_order()
	{
		$arrwhere[] = "package_status_id = 0";
		$arrjoin[] = "LEFT JOIN member B ON B.kode = A.member_code";
		$table = $this->table_ . "_scan A";
		$id = $this->id_;
		$arrorder2[] = 'package_status_id ASC';
		$field = array('nama', 'package_code');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/print_sj/xid") . '" class="btn btn-danger"> <i class="fa fa-eye"></i><br />Print SJ</a>';

		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach ($arrorder2 as $index => $value) {
			$arrorder[] = $value;
		}
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}

		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		//echo "SELECT $id, $jfield FROM $table $join $where $order $limit";
		//die();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				if ($keyfield == 'package_status_id') {
					$datax[] = (($valuer[$keyfield] == "1") ? "Terkonfirmasi" : "Draft");
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_safeconduct()
	{
		$arrdata = $this->db->query("SELECT * FROM m_safeconduct")->result_array();
		return $arrdata;
	}

	function get_package_safeconduct($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_package_safeconduct A LEFT JOIN m_safeconduct B ON B.safeconduct_id = A.safeconduct_id WHERE A.package_id = '$id'")->result_array();
		return $arrdata;
	}

	function get_data_accept_transfer()
	{
		$arrwhere[] = "A.warehouse_target_id = '".$_SESSION['warehouse_id']."'";
		$arrwhere[] = "A.package_transfer_status = '0'";
		$arrjoin[] = "JOIN m_warehouse B ON B.warehouse_id = A.warehouse_id";
		$table = $this->table_ . " A";
		$id = $this->id_;
		$arrorder2[] = 'package_date ASC'; 
		$field = array('package_code', 'warehouse_name', 'package_date');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_detail2/xid") . '" class="btn btn-danger"> <i class="fa fa-list"></i><br />Terima Produk</a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach ($arrorder2 as $index => $value) {
			$arrorder[] = $value;
		}
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}

		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		//echo "SELECT $id, $jfield FROM $table $join $where $order $limit";
		//die();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_safeconduct()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN member B ON B.kode = A.member_code";
		$arrjoin[] = "JOIN m_user C ON C.user_id = A.user_id";
		$table = $this->table_ . " A";
		$id = $this->id_;
		$arrorder2[] = 'package_date DESC';
		$field = array('package_code', 'nama', 'user_fullname', 'package_date_create', 'package_date');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form_detail2/xid") . '" class="btn btn-danger"> <i class="fa fa-list"></i><br />Detail Pembagian</a> <a href="' . site_url($url . "/cetaksuratjalankemasan4/xid") . '" class="btn btn-error"> <i class="fa fa-print"></i><br />Cetak Surat Jalan</a> <!-- <a href="' . site_url($url . "/cetaksuratjalankemasan2/xid") . '" class="btn btn-warning"> <i class="fa fa-print"></i><br />Cetak Surat Jalan</a> --> <a href="' . site_url($url . "/form_safeconduct/xid") . '" class="btn btn-success"> <i class="fa fa-edit"></i><br />Form SJ Driver</a> <a href="' . site_url($url . "/cetaksuratjalandriver/xid") . '" class="btn btn-success" target="_blank"> <i class="fa fa-print"></i><br />Cetak SJ Driver</a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach ($arrorder2 as $index => $value) {
			$arrorder[] = $value;
		}
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}

		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		//echo "SELECT $id, $jfield FROM $table $join $where $order $limit";
		//die();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_list_pendingan()
	{
		$arrjoin[] = "JOIN t_sales_detail b ON b.sales_id = a.sales_id";
		$arrjoin[] = "JOIN produk c ON c.id_produk = b.product_id";
		$arrjoin[] = "JOIN produk_global d ON d.kode = c.kd_pd";
		$arrjoin[] = "left join (SELECT FORMAT(SUM(package_detail_quantity), 0) as pembagian_qty, seller_id, kd_pd FROM t_package_trial_detail_draft JOIN produk ON produk.id_produk = t_package_trial_detail_draft.product_id GROUP BY kd_pd) e ON e.kd_pd = d.kode";
		$table = "t_sales a";
		$id = 'a.sales_id';
		$arrgroup[] = "d.kode";
		$arrwhere[] = "b.pending_quantity > 0";
		if ($_SESSION['role_id'] == 12 || $_SESSION['role_id'] == 7) $arrwhere[] = "d.brand_id  = '" . $_SESSION['sales_category_id'] . "'";
		//$arrorder[] = "";
		$field = array('d.kode', 'd.nama_produk', "FORMAT(SUM(b.sales_detail_quantity), 0) as 'Jumlah Pesanan'", "e.pembagian_qty as 'Jumlah Pembagian'", "FORMAT((SUM(b.pending_quantity) + SUM(b.connected_quantity)), 0) as 'Boleh Dikirim'", "FORMAT(SUM(b.pending_quantity), 0) as 'Pending'", "FORMAT(SUM(b.connected_quantity), 0) as 'Terkirim'");
		$rfield = array('kode', 'nama_produk', "Jumlah Pesanan", "Jumlah Pembagian", "Boleh Dikirim", "Pending", "Terkirim");
		$searchField = array('d.kode', 'd.nama_produk');
		$url = $this->url_;
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($searchField as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			if($value2['column'] != ""){
				$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
			}
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {

			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($rfield as $keyfield) {
				if ($keyfield == 'kode' || $keyfield == 'nama_produk') {
					$datax[] = '<a href="' . site_url($url . "/detail_list_pendingan/" . $valuer['kode']) . '">' . $valuer[$keyfield] . '</a>';
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			// $datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_list_pendingan_seller()
	{
		$arrjoin[] = "JOIN t_sales_detail B ON B.sales_id = A.sales_id";
		$arrjoin[] = " JOIN member C ON C.kode = A.seller_id";
		$arrjoin[] = "LEFT JOIN t_package_trial_detail_draft D ON D.seller_id = C.kode and D.product_id = B.product_id";
		$table = "t_sales A";
		$id = 'A.sales_id';
		$arrgroup[] = "A.seller_id";
		$arrwhere[] = "B.pending_quantity > 0";
		if (@$_SESSION['role_id'] == "12") {
			$arrwhere[] = "C.schema_id = '" . @$_SESSION['sales_category_id'] . "'";
		} else if (@$_SESSION['role_id'] == "7") {
			$arrwhere[] = "C.schema_id = '" . @$_SESSION['sales_category_id'] . "'";
			$arrwhere[] = "C.user_pic_id = '" . @$_SESSION['user_id'] . "'";
		}
		$arrorder[] = "";
		$field = array('C.kode', 'C.nama', "FORMAT(SUM(B.sales_detail_quantity), 0) as 'Jumlah Pesanan'", "ifnull(SUM(D.package_detail_quantity), 0) as 'Jumlah Pembagian'", "FORMAT((SUM(B.pending_quantity) + SUM(B.connected_quantity)), 0) as 'Boleh Dikirim'", "FORMAT(SUM(B.pending_quantity), 0) as 'Pending'", "FORMAT(SUM(B.connected_quantity), 0) as 'Terkirim'");
		$rfield = array('kode', 'nama', "Jumlah Pesanan", "Jumlah Pembagian", "Boleh Dikirim", "Pending", "Terkirim");
		$searchField = array('C.kode', 'C.nama');
		$url = $this->url_;
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($searchField as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();

		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {

			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($rfield as $keyfield) {
				if ($keyfield == 'kode' || $keyfield == 'nama') {
					$datax[] = '<a href="' . site_url($url . "/detail_list_pendingan_seller/" . $valuer['kode']) . '">' . $valuer[$keyfield] . '</a>';
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			// $datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_detail_list_pendingan($kode)
	{
		$arrjoin[] = "JOIN t_sales B ON B.seller_id = A.kode JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd left join (SELECT ifnull(SUM(package_detail_quantity), 0) as pembagian_qty, seller_id, kd_pd FROM t_package_trial_detail_draft JOIN produk ON produk.id_produk = t_package_trial_detail_draft.product_id WHERE produk.kd_pd = '" . $kode . "' GROUP BY seller_id, kd_pd) F on F.seller_id = B.seller_id and F.kd_pd = E.kode";
		$table = "member A";
		$id = 'A.id_member';
		$arrwhere[] = "C.pending_quantity > 0";
		$arrwhere[] = "E.kode = '" . $kode . "' ";
		$arrgroup[] = "E.kode";
		$arrgroup[] = "B.seller_id";
		$arrorder[] = "";
		$field = array('B.seller_id', 'A.nama', "FORMAT(SUM(C.sales_detail_quantity),0) as 'Jumlah Pesanan'", "FORMAT(pembagian_qty,0) as 'Jumlah Pembagian'", "FORMAT(SUM(C.pending_quantity) + SUM(C.connected_quantity),0) as 'Boleh Dikirim'", "FORMAT(SUM(C.pending_quantity),0) as 'Pending'", "FORMAT(SUM(C.connected_quantity),0) as 'Terkirim'");
		$rfield = array('seller_id', 'nama', "Jumlah Pesanan", "Jumlah Pembagian", "Boleh Dikirim", "Pending", "Terkirim");
		$searchField = array('B.seller_id', 'A.nama');
		$url = $this->url_;
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($searchField as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {

			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($rfield as $keyfield) {
				if ($keyfield == 'seller_id' || $keyfield == 'nama') {
					$datax[] = '<a href="' . site_url($url . "/detail_account_list_pendingan/" . $kode . "/" . $valuer['seller_id']) . '">' . $valuer[$keyfield] . '</a>';
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			// $datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}


	function get_data_detail_list_pendingan_seller($kode)
	{
		$arrjoin[] = "JOIN t_sales_detail b ON b.sales_id = a.sales_id";
		$arrjoin[] = "JOIN produk c ON c.id_produk = b.product_id";
		$arrjoin[] = "JOIN produk_global d ON d.kode = c.kd_pd";
		$arrjoin[] = "LEFT JOIN t_package_trial_detail_draft e ON e.seller_id = a.seller_id and e.product_id = c.id_produk";
		$table = "t_sales a";
		$id = 'a.seller_id';
		$arrwhere[] = "b.pending_quantity > 0";
		$arrwhere[] = "a.seller_id = '" . $kode . "'";
		$arrgroup[] = "d.kode, a.seller_id";
		$arrorder[] = "";
		$field = array('d.kode', 'd.nama_produk', "SUM(b.sales_detail_quantity) as 'Jumlah Pesanan'",  "ifnull(SUM(e.package_detail_quantity), 0) as 'Jumlah Pembagian'", "SUM(b.pending_quantity) + SUM(b.connected_quantity) as 'Boleh Dikirim'", "SUM(b.pending_quantity) as 'Pending'", "SUM(b.connected_quantity) as 'Terkirim'");
		$rfield = array('kode', 'nama_produk', "Jumlah Pesanan", "Jumlah Pembagian", "Boleh Dikirim", "Pending", "Terkirim");
		$searchField = array('d.kode', 'd.nama_produk');
		$url = $this->url_;
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($searchField as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {

			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($rfield as $keyfield) {
				if ($keyfield == 'kode' || $keyfield == 'nama_produk') {
					$datax[] = '<a href="' . site_url($url . "/detail_account_list_pendingan/" . $valuer['kode'] . "/" . $kode) . '">' . $valuer[$keyfield] . '</a>';
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			// $datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}
	function get_data_detail_account_list_pendingan($product, $kode)
	{
		$kode = str_replace("%20", " ", $kode);
		$arrjoin[] = "JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id";
		$table = "t_sales B";
		$id = 'B.sales_id';
		$arrwhere[] = "D.kd_pd = '" . $product . "'";
		$arrwhere[] = "B.seller_id = '" . $kode . "'";
		$arrwhere[] = "C.pending_quantity > '0'";
		$arrorder[] = "";
		$field = array('B.sales_code', 'B.sales_date', "C.sales_detail_quantity as 'Jumlah Pesanan'", "C.pending_quantity + C.connected_quantity as 'Boleh Dikirim'", "C.pending_quantity as 'Pending'", "C.connected_quantity as 'Terkirim'");
		$rfield = array('sales_code', 'sales_date', "Jumlah Pesanan", "Boleh Dikirim", "Pending", "Terkirim");
		$searchField = array('B.sales_code', 'B.sales_date');
		$url = $this->url_;
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($searchField as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {

			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($rfield as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			// $datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_report($search = '')
	{
		if ($search != "") {
			$arrsearch = explode("%7C", $search);
			$arrwhere[] = "A.member_status_id = '" . $arrsearch[0] . "'";
			$arrwhere[] = "(A.member_code LIKE '%" . $arrsearch[1] . "%' OR A.member_name LIKE '%" . $arrsearch[1] . "%' OR A.member_phone LIKE '%" . $arrsearch[1] . "%' OR A.member_address LIKE '%" . $arrsearch[1] . "%' OR D.nama_kecamatan LIKE '%" . $arrsearch[1] . "%' OR C.nama_kota LIKE '%" . $arrsearch[1] . "%')";
		}
		$arrwhere[] = "A.member_code != ''";
		$arrjoin[] = "LEFT JOIN mst_kecamatan D ON D.id_kecamatan = A.district_id AND D.id_kota = A.city_id";
		$arrjoin[] = "LEFT JOIN mst_kota C ON C.id_kota = A.city_id";
		$arrjoin[] = "LEFT JOIN m_member_status B ON B.member_status_id = A.member_status_id";
		$table = $this->table_;
		$id = $this->id_;
		$field = array('member_code', 'member_name', 'member_phone', 'nama_kota', 'member_status_name', 'member_phone', 'member_date');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form/ubah/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="' . site_url($url . "/transfer/tambah/xid") . '" class="btn btn-info"> <i class="fa fa-list"></i></a> <a href="' . site_url($url . "/send/tambah/xid") . '" class="btn btn-info"> <i class="fa fa-download"></i></a> <a href="' . site_url($url . '/hapus/xid') . '" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}



	function get_detail($id)
	{
		$this->db->where($this->id_, $id);
		$this->db->join('m_account_detail_category B', 'B.account_detail_category_id = A.account_detail_category_id');
		$data = $this->db->get($this->table2_ . ' A');
		return $data;
	}

	function get_data_detail($idxx)
	{
		$table = $this->table2_ . ' A';
		$id = "CONCAT(account_id, '/', account_detail_id) AS id";
		$field = array('account_detail_category_name', 'account_detail_pic', 'account_detail_note', 'account_detail_debit', 'account_detail_credit', 'account_detail_realization');
		$arrjoin[] = 'LEFT JOIN m_account_detail_category B ON B.account_detail_category_id = A.account_detail_category_id';
		$url = $this->url_;
		$arrwhere[] = "account_id = $idxx";
		$action = '<a href="' . site_url($url . "/form_detail/ubah/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="' . site_url($url . '/hapus_detail/xid') . '" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(account_detail_id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(account_detail_id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer['id'], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function insert($data = array())
	{
		$this->db->insert($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_log($data = array())
	{
		$this->db->insert("l_outbound_log", $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function insert_trascend($data = array())
	{
		$this->db->insert($this->table_. "_trascend", $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_scan($data = array())
	{
		$this->db->insert($this->table_ . "_scan", $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_global($table, $data = array())
	{
		$this->db->insert($table, $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
		return $this->db->insert_id();
	}

	function insert_terima_produk($data = array())
	{
		$this->db->insert('terima_produk', $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_package_safeconduct($data = array())
	{
		$this->db->insert('t_package_safeconduct', $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_detail_draft($data = array())
	{
		$this->db->insert($this->table_ . "_detail_draft", $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_detail($data = array())
	{
		$this->db->insert($this->table_ . "_detail", $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function insert_detail_trascend($data = array())
	{
		$this->db->insert($this->table_ . "_detail_trascend", $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_employee($data = array())
	{
		$this->db->insert($this->table_ . "_employee", $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_employee_real($data = array())
	{
		$this->db->insert($this->table_ . "_employee_real", $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_account($data = array())
	{
		$this->db->insert('m_account', $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update_global($table, $data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($table, $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($this->table_);
		$info = '<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}

	function delete_package_safeconduct($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete('t_package_safeconduct');
		$info = '<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update_table($table, $data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($table, $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update_detail($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update('t_account_detail', $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete_detail($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete('t_account_detail');
		$info = '<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}

	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}

	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}

	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}

	function get_max_id()
	{
		$data = $this->db->query("SELECT (IFNULL(MAX(account_detail_id), 0) + 1) as id FROM t_account_detail")->row_array();
		return $data['id'];
	}

	function get_account_id($id)
	{
		$data = $this->db->query("SELECT account_id FROM m_account WHERE seller_id = '$id'")->row_array();
		return $data['account_id'];
	}

	function insert_account_detail($data = array())
	{
		$this->db->insert("t_account_detail", $data);
		$info = '<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
		return $this->db->insert_id();
	}

	function get_m_account()
	{
		$data = $this->db->get('m_account');
		return $data;
	}

	function update_balance($id, $balance, $type, $account_type = '', $monthly = '')
	{
		if ($type == "credit") {
			if ($monthly == '') {
				$this->db->query("UPDATE m_account SET account_credit = (account_credit + $balance) WHERE account_id = " . $id);
			} else {
				if ($account_type == '1') {
					$this->db->query("UPDATE m_account SET account_debit = (account_debit - ($balance)), account_monthly_debit = (account_monthly_debit - ($balance)) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit - ($balance)) WHERE account_reset_date > '$monthly'");
				} else {
					$this->db->query("UPDATE m_account SET account_credit = (account_credit + $balance), account_monthly_credit = (account_monthly_credit + $balance) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		} else if ($type == "debit") {
			if ($monthly == '') {
				$this->db->query("UPDATE m_account SET account_debit = (account_debit + $balance) WHERE account_id = " . $id);
			} else {
				if ($account_type == '2') {
					$this->db->query("UPDATE m_account SET account_credit = (account_credit - ($balance)), account_monthly_credit = (account_monthly_credit - ($balance)) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit - ($balance)) WHERE account_reset_date > '$monthly'");
				} else {
					$this->db->query("UPDATE m_account SET account_debit = (account_debit + $balance), account_monthly_debit = (account_monthly_debit + $balance) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}
		return '';   
	}

	public function update_masuk($jml, $tanggal, $id_barang)
	{
		$Query = $this->db->query("UPDATE tb_stock_produk SET jumlah = (jumlah + " . $jml . ") WHERE id_barang = '" . $id_barang . "' AND warehouse_id = '".$_SESSION['warehouse_id']."'");
		$Query = $this->db->query("UPDATE tb_stock_produk_history SET jumlah = (jumlah + " . $jml . ") WHERE id_barang = '" . $id_barang . "' AND REPLACE(tanggal, '-', '') > REPLACE('" . $tanggal . "', '-', '') AND warehouse_id = '".$_SESSION['warehouse_id']."'");
		return $Query;
	}

	public function update_keluar($jml, $tanggal, $id_barang)
	{
		$Query = $this->db->query("UPDATE tb_stock_produk SET jumlah = (jumlah - " . $jml . ") WHERE id_barang = '" . $id_barang . "' AND warehouse_id = '".$_SESSION['warehouse_id']."'");
		$Query = $this->db->query("UPDATE tb_stock_produk_history SET jumlah = (jumlah - " . $jml . ") WHERE id_barang = '" . $id_barang . "' AND REPLACE(tanggal, '-', '') > REPLACE('" . $tanggal . "', '-', '') AND warehouse_id = '".$_SESSION['warehouse_id']."'");
		return $Query;
	}

	function set_sales_detail()
	{
		$this->db->query("UPDATE t_sales A JOIN (SELECT sales_id, COUNT(sales_id) count FROM t_sales_detail GROUP BY sales_id) B ON B.sales_id = A.sales_id JOIN (SELECT sales_id, COUNT(sales_id) count FROM t_sales_detail WHERE connected_quantity = sales_detail_quantity GROUP BY sales_id) C ON C.sales_id = A.sales_id SET A.sales_status = 2 WHERE B.count = C.count");
	}

	function cek_discount_bayar_clinic($sales_id, $kode, $price, $quantity, $seller_id)
	{
		$rewardHarga = '';
		$rewardQuantity = '';
		$rewardPersen = '';
		$reward = '';
		$list_persen_product = '';
		$arrjoin = "";
		if (@count(@$kode) > 0) {
			$arrjoin = join("', '", @$kode);
		}

		$arrproduct = $this->db->query("SELECT kd_pd, id_produk FROM produk WHERE id_produk IN('$arrjoin')")->result_array();

		foreach ($arrproduct as $indexkd => $valuekd) {
			$arrdict[$valuekd['id_produk']] = $valuekd['kd_pd'];
		}

		if (@count(@$kode) > 0) {
			foreach (@$kode as $index_kode => $value_kode) {
				if (@$hrgbrg[$arrdict[$value_kode]] != "") $hrgbrg[$arrdict[$value_kode]] += $price[$index_kode];
				else $hrgbrg[$arrdict[$value_kode]] = $price[$index_kode];
				if (@$jumbrg[$arrdict[$value_kode]] != "") $jumbrg[$arrdict[$value_kode]] += $quantity[$index_kode];
				else $jumbrg[$arrdict[$value_kode]] = $quantity[$index_kode];
			}
		}
		$arrdiscount = $this->db->query("SELECT * FROM t_sales_discount WHERE sales_id = '$sales_id' AND sales_discount_active = 1")->result_array();
		foreach ($arrdiscount as $indexdisc => $valuedisc) {
			$arrdiscount2[$valuedisc['sales_discount_id']] = $valuedisc;
		}
		$arrdiscount_terms = $this->db->query("SELECT B.sales_id, A.* FROM t_sales_discount_terms A JOIN t_sales_discount B ON B.sales_discount_id = A.sales_discount_id WHERE B.sales_id = '$sales_id' AND B.sales_discount_active = 1")->result_array();

		foreach ($arrdiscount_terms as $indexterms => $valueterms) {
			$arrfdisc_terms[$valueterms['sales_discount_id']][$valueterms['discount_terms_product_id']][$valueterms['sales_discount_terms_id']] = $valueterms;
		}
		/*print_r($arrfdisc_terms);
		die();
		$arrdiscount_reward = $this->db->query("SELECT B.discount_id, C.*, D.nama_produk FROM m_discount_reward A JOIN m_discount B ON B.discount_id = A.discount_id JOIN m_discount_reward_product C ON C.discount_reward_id = A.discount_reward_id LEFT JOIN produk_global D ON D.kode = C.product_kd JOIN m_discount_member E ON E.discount_id = B.discount_id WHERE E.member_status = '" . $_POST['member_status'] . "' AND discount_active = 1")->result_array();
		foreach ($arrdiscount_reward as $indexreward => $valuereward) {
			$arrfdisc_reward[$valuereward['discount_id']][$valuereward['discount_reward_id']][$valuereward['discount_reward_product_id']] = $valuereward;
		}*/
		if (@count(@$arrfdisc_terms) > 0) {
			foreach ($arrfdisc_terms as $index1 => $value1) {
				$total_terms[$index1] = count($value1);
				$accepted_terms[$index1] = 0;
				$min = 0;
				$sementara = "";
				foreach ($value1 as $index2 => $value2) {
					$ix = 0;
					$jum[$index1][$index2] = 0;
					$jum_all[$index1][$index2] = 0;
					foreach ($value2 as $index3 => $value3) {
						if ($value3['sales_discount_terms_type'] == "2") {
							if (@$hrgbrg[$value3['product_kd']] == "") $hrgbrg[$value3['product_kd']] = 0;
							/*if($value3['sales_discount_terms_price'] < ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft'])){ 
								$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']) - $value3['sales_discount_terms_price'];
							}else{*/
								$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']);
							//}
								$jum[$index1][$index2] += ($hrgbrg[$value3['product_kd']] + $tambah[$index1][$index2][$index3]);
								$jum_all[$index1][$index2] = $value3['sales_discount_terms_price'];
							} else if ($value3['sales_discount_terms_type'] == "1") {
								if (@$jumbrg[$value3['product_kd']] == "") $jumbrg[$value3['product_kd']] = 0;
							/*if($value3['sales_discount_terms_quantity'] < ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft'])){ 
								$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']) - $value3['sales_discount_terms_quantity'];
							}else{*/
								$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']);
							//}
								$jum[$index1][$index2] += ($jumbrg[$value3['product_kd']] + $tambah[$index1][$index2][$index3]);
								$jum_all[$index1][$index2] = $value3['sales_discount_terms_quantity'];
							}
						}
						if ($ix == 0) {
							if ($value3['sales_discount_terms_type'] == "2") {
								if ($jum[$index1][$index2] >= $value3['sales_discount_terms_price']) {
									$ix = 1;
									$sementara = intdiv($jum[$index1][$index2], $value3['sales_discount_terms_price']);
									if ($min == 0 || $sementara < $min) $min = $sementara;
								}
							} else if ($value3['sales_discount_terms_type'] == "1") {
								if ($jum[$index1][$index2] >= $value3['sales_discount_terms_quantity']) {
									$ix = 1;
									$sementara = intdiv($jum[$index1][$index2], $value3['sales_discount_terms_quantity']);
									if ($min == 0 || $sementara < $min) $min = $sementara;
								}
							}
						}
						if ($ix == 1) $accepted_terms[$index1]++;
					}

				//echo @$accepted_terms[$index1];
				//echo $total_terms[$index1];
				//echo "<hr>";
					if ($arrdiscount2[$index1]['sales_discount_percentage_active_draft'] == 1 && $arrdiscount2[$index1]['sales_discount_percentage'] != "") {
						$accepted_discount[$index1] = $index1;
						$multiply[$index1] = 1;
					} else if (@$accepted_terms[$index1] == $total_terms[$index1]) {
						$accepted_discount[$index1] = $index1;
						if ($min > 0) $multiply[$index1] = $min;
					}
				}
			}
			$total_potongan = 0;
			$total_potongan2 = 0;
			$i = 0;
			$x = 0;
			$y = 0;
			$jum_syarat = 0;
			if (@count(@$accepted_discount) > 0) {
				foreach ($accepted_discount as $index_reward => $value_reward) {
					$arrdatasales_discount = $this->db->query("SELECT A.*, B.sales_detail_id FROM t_sales_discount A JOIN t_sales_discount_detail B ON B.sales_discount_id = A.sales_discount_id WHERE A.sales_discount_id = '" . $index_reward . "'")->row_array();

					foreach ($arrfdisc_terms[$index_reward] as $index1x => $value1x) {
						foreach ($value1x as $index2x => $value2x) {
						}
					}
					if ($arrdatasales_discount['sales_discount_price'] > 0) {
						$rewardHarga .= '<tr><td>' . ++$x . '<input type="hidden" name="diskon_harga[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" id="' . $sales_id . '" class="price_deduction" value="' . ($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]) . '"/></td><td>Rp ' . number_format($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]) . '</td></tr>';
						$total_potongan += ($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]);
						$reward .= '<input type="hidden" name="discount_id[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" value="' . $arrdatasales_discount['discount_id'] . '"/>';
					}

					if ($arrdatasales_discount['sales_discount_percentage'] > 0) {
						if ($arrdiscount2[$index_reward]['sales_discount_percentage_active'] == 0) {
						}
						$val = 0;
						foreach ($arrfdisc_terms[$index_reward] as $val1) {
							foreach ($val1 as $val2) {
								$val += ($hrgbrg[$val2['product_kd']] * $arrdatasales_discount['sales_discount_percentage'] / 100);
								$list_persen_product .= '<input type="hidden" name="list_prod_diskon[' . $sales_id . '][' . $val2['product_kd'] . ']" value="' . $arrdatasales_discount['sales_discount_percentage'] . '"/>';
							}
						}
						$rewardPersen .= '<tr><td>' . ++$y . '<input type="hidden" name="diskon_persen[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" id="' . $sales_id . '" class="price_deduction" value="' . $val . '"/></td><td>' . $arrdatasales_discount['sales_discount_percentage']  . '%</td><td>Rp ' . number_format($val) . '</td></tr>';
						$total_potongan2 += $val;
						$reward .= '<input type="hidden" name="discount_id[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" value="' . $arrdatasales_discount['discount_id'] . '"/>';
					}
					foreach ($arrfdisc_terms[$value_reward] as $indexterms2 => $valueterms2) {
						foreach ($valueterms2 as $indexterms3 => $valueterms3) {
							$jum_syarat += @$hrgbrg[$valueterms3['product_kd']];
						}
					}
				}
				if (@count(@$reward_kd) > 0) {
					$discount_produk = "";
					$discount_produk_input = "";
					foreach ($reward_kd as $index2x => $value2x) {
						$discount_produk .= $value2x . ' ' . $reward_product[$index2x] . ' pcs' . '<br>';
						$rewardQuantity .= '<tr><td>1</td><td>' . $value2x . '</td><td>' . $reward_product[$index2x] . '</td></tr>';
						$discount_produk_input .= '<input type="hidden" name="promo_product[' . $index2x . ']" value="' . $reward_product[$index2x] . '" />';
					}
				}
			}
			$sisa = array();
			if (@count(@$arrfdisc_terms) > 0) {
				foreach ($arrfdisc_terms as $index1 => $value1) {
					if (@$multiply[$index1] != "") $xmultiply = $multiply[$index1];
					else $xmultiply = 0;
					foreach ($value1 as $index2 => $value2) {
					//$jum_sem_awal = $jum_all[$index1][$index2] * $xmultiply;
						$jum_sem = $jum_all[$index1][$index2] * $xmultiply;

						foreach ($value2 as $index3 => $value3) {
						//if($jum_sem > 0){
							if ($value3['sales_discount_terms_type'] == "2") {
								$jumx = ($hrgbrg[$value3['product_kd']] + $tambah[$index1][$index2][$index3]);
							} else if ($value3['sales_discount_terms_type'] == "1") {
								$jumx = ($jumbrg[$value3['product_kd']] + $tambah[$index1][$index2][$index3]);
							}
							if ($jum_sem <= $jumx) {
								$jum_sem_awal[$index2][$index3] = $jum_sem;
								$sisa[$index2][$index3] = $jumx - $jum_sem;
								$jum_sem = 0;
							} else {
								$jum_sem_awal[$index2][$index3] = $jumx;
								$jum_sem = $jum_sem - $jumx;
								$sisa[$index2][$index3] = 0;
							}
							$accumulation[$index2][$index3] = $jum_sem;
						//}
						}
					}
				}
			}
			$sisa_product = '';
			foreach ($sisa as $index1 => $val1) {
				foreach ($val1 as $index2 => $val2) {
				//$sisa_product .= '<input type="hidden" name="sisa_produk[' . $index1 . '][' . $index2 . ']" value="' . $val2 . '" />';
				//$sisa_product .= '<input type="hidden" name="produk_accum[' . $index1 . '][' . $index2 . ']" value="' . $accumulation[$index1][$index2] . '" />';
					$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod = $val2, sales_discount_terms_accumulation = " . $accumulation[$index1][$index2] . " WHERE sales_discount_terms_id = '$index2'");
				}
			}
			$data = array(
				'potongan' => @$total_potongan,
				'potongan2' => @$total_potongan2,
			);

			$total = @$total_potongan + @$total_potongan2;
			$this->db->query("UPDATE m_account SET account_deposit = account_deposit + $total WHERE seller_id = '$seller_id'");
			return $total;
		// die();
		}

		public function Code($Query) {
			$Query = $this->db->query("  ".$Query."  ");
			return $Query->result_array();	
		}
	}
