
<div class="row">
  <div class="col-lg-12 col-xl-12">
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            PENGEMASAN PRODUK KODE PENJUALAN : <?= $arrsales['sales_code'] ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
        <h5>Form Pengiriman Produk : <?= $arrsales['sales_code'] ?></h5>
        <hr>
        <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan'); ?>" method="post" novalidate>
          <?= input_hidden('parameter', 'tambah') ?>
		  <div class="form-group">
            <label>Kode</label>
            <input type="text" id="cKodePo" name="input[package_code]" class="form-control md-form-control md-static" value="<?= $code ?>">
          </div>
		  <div class="form-group">
            <label>Tanggal Pengemasan</label>
            <input type="text" id="dTglPo" name="input[package_date]" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required> 
          </div>
		  <div class="form-group">
            <label>Seller</label>
            <select name="input[member_code]" id="PilihDistributor" class="form-control md-static" required><option></option><?= $seller_list ?></select> 
          </div>
		  <div class="form-group"> 
            <label>Dropship</label>
            <input type="text" name="input[package_dropship]" class="form-control md-form-control md-static">
          </div>
		 
          <!--<div class="form-group">
            <label>Nama Barang</label>
            <select name="NamaBarang" id="cIdStock" class="form-control md-static" required>

              <option></option>
              <?php
              /*$query = $this->model->ViewWhere('v_detail_produk_jual', 'kode_jual', $action);
              foreach ($query as $key => $vaKemasan) {
              ?>
                <option value="<?= $vaKemasan['id_barang'] ?>"><?= $vaKemasan['nama_produk'] ?></option>
              <?php }*/ ?>
            </select>
          </div>-->
          <div class="kt-portlet__foot" style="margin-top: 20px;">
            <div class="kt-form__actions">
              <button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;">
                <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
it = 1;
let fruits = [];
fruits.push([0]);
function myCreateFunction() {
	it++;
	fruits.push([0]);
	var html = '<tr id="'+it+'"><td><select name="input2['+it+'][product_id]" class="PilihBarang form-control md-static" required><option></option><?= $product_list ?></select></td><td><input type="number" class="form-control" placeholder="Jumlah" name="input2['+it+'][package_detail_quantity]" required></td><td><div id="td'+it+'"><div class="form-group row" id="employee'+it+'x0"><div class="col-10"><select name="input3['+it+'][0][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select></div><label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee('+it+', 0);">-</label></div></div><label onclick="add_employee('+it+');">+</label></td><td><button onclick="myDeleteFunction('+it+')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
	$('#x').append(html);
	$('.PilihBarang').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Barang',
	});
	$('.PilihPegawai').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Pegawai', 
	});
}
function add_employee(id) { 
	fruits[0]++;
	var html = '<div class="form-group row" id="employee'+id+'x'+fruits[0]+'"><div class="col-10"><select name="input3['+id+']['+fruits[0]+'][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select></div><label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee('+id+', '+fruits[0]+');">-</label></div>';
	$('#td'+id).append(html);
	$('.PilihPegawai').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Pegawai', 
	});
}

function myDeleteFunction(id) {
	$('#'+id).remove();
}

function delete_employee(id, id2) {
	$('#employee'+id+'x'+id2).remove();
}
</script>

