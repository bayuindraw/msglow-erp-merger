<div class="row">
	<div class="col-lg-12 col-xl-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						GUDANG : <?= @$arrwarehouse['warehouse_name']  ?>
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<h5>Form Pengiriman Produk : <?= @$arrsales['sales_code'] ?></h5>
				<hr>
				<form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_transfer2'); ?>" onsubmit="return confirm('Are You Sure?')" method="post" novalidate>
					<?= input_hidden('parameter', 'tambah') ?>
					<?= input_hidden('id', $id) ?>
					<div class="form-group">
						<label>Kode</label>
						<input type="text" id="cKodePo" name="input[package_code]" class="form-control md-form-control md-static" value="<?= (@$arrpackage_trial_detail[0]['package_code'] != "") ? @$arrpackage_trial_detail[0]['package_code'] : $code ?>">
					</div>
					<div class="form-group">
						<label>Tanggal Pengemasan</label>
						<input type="text" id="dTglPo" name="input[package_date]" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= (@$arrpackage_trial_detail[0]['package_date'] != "") ? @$arrpackage_trial_detail[0]['package_date'] : date('Y-m-d') ?>" required>
					</div>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Produk</th>
								<th>Jumlah Pembagian</th>
								<th>Jumlah Konfirmasi</th>
							</tr>
						</thead>
						<tbody id="x">
							<?php if (count(@$arrpackage_trial_detail) > 0) {
								$i = 0;
								foreach ($arrpackage_trial_detail as $indexx => $valuex) {
									?>
									<tr id="<?= $i; ?>">

										<td> 
											<input type="hidden" class="form-control" name="input2[<?= $i ?>][package_detail_id]" required value="<?= $valuex['package_detail_id']; ?>">
											<input type="hidden" class="form-control" name="input2[<?= $i ?>][transfer_detail_id]" required value="<?= $valuex['transfer_detail_id']; ?>">
											<input type="hidden" class="form-control kode pilihBarang" placeholder="Jumlah" name="input2[<?= $i ?>][product_id]" required value="<?= $valuex['id_produk']; ?>">
											<!--<?php $valuex['id_produk']; ?>
											<select name="input2[<?= $i; ?>][product_id]" class="PilihBarang form-control md-static" required><option></option>-->
												<?php
												foreach ($arrproduk as $indexl => $valuel) {
													if (($valuex['id_produk'] == $valuel['id_produk'])) {
														echo $valuel['nama_produk'] . ' (' . $valuel['klasifikasi'] . ') (stock: ' . $valuel['jumlah'] . ')';
													}
													/*echo '<option value="'.$valuel['id_produk'].'" '.(($valuex['id_produk'] == $valuel['id_produk'])?'selected':'').'>'.$valuel['nama_produk'].' ('.$valuel['klasifikasi'].') (stock: '.$valuel['jumlah'].')</option>';*/
												//echo '<option value="'.$valuel['id_phl'].'" '.(($valuex['employee_id'] == $valuel['id_phl'])?'selected':'').'>'.$valuel['nama_phl'].'</option>';
													?>
													<?php
												} ?>
											</td>
											<td>
												<!--<input type="number" class="form-control" placeholder="Jumlah" name="input2[<?= $i ?>][package_detail_quantity]" required value="<?= $valuex['package_detail_quantity']; ?>">--><?= $valuex['package_detail_quantity']; ?>
											</td>
											<!--<?= ($valuex['qty_konfirmasi'] == "") ? 0 : $valuex['qty_konfirmasi']; ?> -->
											<td><input type="text" class="form-control" placeholder="Jumlah" name="input2[<?= $i ?>][package_detail_quantity]" required value="<?= ($valuex['package_detail_quantity'] == "") ? 0 : $valuex['package_detail_quantity']; ?>"></td>
											
											<?php $i++;
										}
									} else { ?>
										<tr id="0">
											<td><select name="input2[0][product_id]" class="PilihBarang form-control md-static" required>
												<option></option><?= $product_list ?>
											</select></td>
											<td><input type="number" class="form-control" placeholder="Jumlah" name="input2[0][package_detail_quantity]" required></td>
											<td>
												<div id="td0">
													<div class="form-group row" id="employee0x0">
														<div class="col-10">
															<select name="input3[0][0][employee_id]" class="PilihPegawai form-control" required>
																<option></option><?= $employee_list ?>
															</select>
														</div>
														<label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee(0, 0);">-</label>
													</div>
												</div>
												<label onclick="add_employee(0);">+</label>
											</td>
											<td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
							<!--<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>-->
					<!--<div class="form-group">
            <label>Nama Barang</label>
            <select name="NamaBarang" id="cIdStock" class="form-control md-static" required>

              <option></option>
              <?php
				/*$query = $this->model->ViewWhere('v_detail_produk_jual', 'kode_jual', $action);
              foreach ($query as $key => $vaKemasan) {
              ?>
                <option value="<?= $vaKemasan['id_barang'] ?>"><?= $vaKemasan['nama_produk'] ?></option>
              <?php }*/ ?>
            </select>
        </div>-->
        <div class="kt-portlet__foot" style="margin-top: 20px;">
        	<div class="kt-form__actions">
        		<button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;">
        			<i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
        		</button>
							<!-- <button type="button" name="diskon" value="diskon" class="btn btn-success waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;" onclick="chk_diskon()">
								<i class="icofont icofont-ui-edit"></i><span class="m-l-10">Cek Diskon</span>
							</button> -->
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	it = <?= ((count(@$arrpackage_trial_detail) > 0) ? count(@$arrpackage_trial_detail) : 1); ?>;
	let fruits = [];

	<?php if (count(@$arrpackage_trial_detail) > 0) {
		$i = 0;
		foreach ($arrpackage_trial_detail as $indexx => $valuex) {
			?>
			fruits.push([0]);
			<?php
			$i2 = 1;
			foreach ($data_trial_employee[$valuex['package_detail_id']] as $indexxx => $valuexxx) {
				?>
				fruits[<?= $i ?>]++;
				<?php
				$i2++;
			}
			$i++;
		}
	} else { ?>

	<?php } ?>
	fruits.push([0]);

	function myCreateFunction() {
		it++;
		fruits.push([0]);
		var html = '<tr id="' + it + '"><td><select name="input2[' + it + '][product_id]" class="PilihBarang form-control md-static" required><option></option><?= $product_list ?></select></td><td><input type="number" class="form-control" placeholder="Jumlah" name="input2[' + it + '][package_detail_quantity]" required></td><td><div id="td' + it + '"><div class="form-group row" id="employee' + it + 'x0"><div class="col-10"><select name="input3[' + it + '][0][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select></div><label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee(' + it + ', 0);">-</label></div></div><label onclick="add_employee(' + it + ');">+</label></td><td><button onclick="myDeleteFunction(' + it + ')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$('.PilihBarang').select2({
			allowClear: true,
			placeholder: 'Pilih Barang',
		});
		$('.PilihPegawai').select2({
			allowClear: true,
			placeholder: 'Pilih Line',
		});
	}

	function add_employee(id) {
		fruits[id]++;
		var html = '<div class="form-group row" id="employee' + id + 'x' + fruits[id] + '"><div class="col-10"><select name="input3[' + id + '][' + fruits[id] + '][employee_id]" class="PilihPegawai form-control" required><option></option><?= $employee_list ?></select></div><label for="example-password-input" class="col-2 col-form-label" onclick="delete_employee(' + id + ', ' + fruits[id] + ');">-</label></div>';
		$('#td' + id).append(html);
		$('.PilihPegawai').select2({
			allowClear: true,
			placeholder: 'Pilih Line',
		});
	}

	function myDeleteFunction(id) {
		$('#' + id).remove();
	}

	function delete_employee(id, id2) {
		$('#employee' + id + 'x' + id2).remove();
	}

	function chk_diskon() {
		var kode = [];
		var quantity = [];
		var price = [];
		var member_status = $('#member_status').val();
		$('.PilihBarang').each(function() {
			kode.push(this.value);
		})
		$('.inputQuantity').each(function() {
			quantity.push(this.value.replaceAll(",", ""));
		})
		$('.pilihHarga').each(function(index, value) {
			value = quantity[index] * this.value;
			price.push(value);
		})
		$.ajax({
			type: 'post',
			url: '<?= base_url() ?>Test/cek_discount_bayar_clinic',
			data: {
				kode: kode,
				price: price,
				quantity: quantity,
				member_status: member_status
			},
			success: function(result) {
				var result2 = JSON.parse(result);
				$('#input_diskon').html(result2.reward);
				if (result2.list_diskon_harga !== '') {
					$('#potonganHarga').html(result2.list_diskon_harga);
					$('#tablePotHarga').show();
				}
				if (result2.list_diskon_persen !== '') {
					$('#potonganPersen').html(result2.list_diskon_persen)
					$('#tablePotPersen').show();
				}
				if (result2.list_diskon_quantity !== '') {
					$('#bonusProduk').html(result2.list_diskon_quantity)
					$('#tablePotProduk').show();
				}
				if (result2.produk_insert !== '') {
					$('#insertHiddenProd').html(result2.produk_insert)
				} else {
					$('#insertHiddenProd').html('')
				}
				$('#simpan').show();
				$('#diskon').hide();
			}
		});
	}
</script>