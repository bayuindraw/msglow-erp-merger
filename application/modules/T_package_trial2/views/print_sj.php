<?php $arrbln = array(
	'01' => 'Januari',
	'02' => 'Februari',
	'03' => 'Maret',
	'04' => 'April',
	'05' => 'Mei',
	'06' => 'Juni',
	'07' => 'Juli',
	'08' => 'Agustus',
	'09' => 'September',
	'10' => 'Oktober',
	'11' => 'November',
	'12' => 'Desember'
);
?>
<style>
	hr.new2 {
		border: 1px solid;
		margin-top: 20px;
		margin-bottom: 20px;
	}

	table.list td,
	table.list td * {
		vertical-align: top;
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
<html>

<body>
	<h2 style="text-align: center;padding-top: 40px; padding-bottom">SURAT JALAN</h2>
	<hr class="new2">
	<table border="0" width='100%'>
		<tr>
			<th width='15%'>Nomor</th>
			<td width='1%'>:</td>
			<td width='84%' colspan="3"><?= $arrpackage_trial['package_code'] ?></td>
		</tr>
		<tr>
			<th width='15%'>Tanggal</th>
			<td width='1%'>:</td>
			<td width='84%' colspan="3"><?= (int)substr($arrpackage_trial['package_date'], 8, 2) . " " . $arrbln[substr($arrpackage_trial['package_date'], 5, 2)] . " " . substr($arrpackage_trial['package_date'], 0, 4) ?></td>
		</tr>
		<tr>
			<th>Kepada</th>
			<td>:</td>
			<td><?= @$arrpackage_trial['nama'] ?></td>
			<td align="center">
				<p style="text-align: center;">Barcode:</p>
				<img src="<?= site_url('R_barcode_pwa_generator/generate/' . @$arrpackage_trial['package_barcode']); ?>" alt="Girl in a jacket" width="300" height="120">
			</td>
		</tr>
	</table>
	<hr class="new2">
	<table border=0 width='100%' class="list">
		<tr>
			<th width='5%' align="left">No.</th>
			<th width='55%' align="left">Nama Produk</th>
			<th width='20%' align="left">Pcs</th>
			<th width='20%' align="left">Koli</th>
		</tr>
		<?php

		$no = 1;
		$produk_before = "";
		foreach ($arrpackage_trial_scan as $key => $vaData) {
			//if($no <= 6){
		?>
			<tr>
				<?php if ($produk_before != $vaData['produk']) { ?>
					<td width='4%' <?= ($colspan[$vaData['produk']] > 1) ? "rowspan='" . $colspan[$vaData['produk']] . "'" : ""; ?>><?= $no ?></td>
				<?php
					$no++;
				} ?>
				<td><?= $vaData['nama_produk'] ?></td>
				<td><?= $vaData['qty'] ?> Pcs</td>
				<?php if ($produk_before != $vaData['produk']) { ?>
					<td <?= ($colspan[$vaData['produk']] > 1) ? "rowspan='" . $colspan[$vaData['produk']] . "'" : ""; ?>><?= number_format($vaData['jum_koli']) ?> Koli</td>
				<?php } ?>
			</tr>
		<?php
			$produk_before = $vaData['produk'];
		}
		?>
	</table>

	<hr class="new2">

	<table border="0" width='100%'>
		<tr>
			<th width='15%'>Total Box</th>
			<td width='1%'>:</td>
			<td width='84%'><?= $no - 1 ?> Box/Koli</td>
		</tr>
	</table>

	<hr class="new2">

	<table style="width:100%;font-size: 15px;padding-top:20px;" align="center">
		<tr>
			<th align="center">Penerima</th>
			<td align="center"></td>
			<td align="center"></td>
			<th align="center">MSGLOW</th>
		</tr>
		<tr style="padding-top: 20px;">
			<td align="center"><br><br><br> ................................</td>
			<td align="center"></td>
			<td align="center"></td>
			<td align="center"><br><br><br> ................................</td>
		</tr>
	</table>
</body>

</html>
<script>
	window.print();
</script>