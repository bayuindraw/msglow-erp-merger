<?php
defined('BASEPATH') or exit('No direct script access allowed');


class T_postage extends CI_Controller
{

	var $url_ = "T_postage";
	var $id_ = "postage_id";
	var $eng_ = "postage";
	var $ind_ = "Ongkos Kirim";

	public function __construct()
	{
		parent::__construct();
		ob_start();
		$this->load->library('m_pdf');
		$this->load->model('model');

		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function search_member($search = '')
	{
		$this->Model->search_member($search);
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['total_tagihan'] = $this->db->query("SELECT SUM(account_detail_debit) AS tagihan FROM t_account_detail WHERE account_detail_deposit_paid NOT IN(1) AND postage_id IS NOT NULL")->row_array();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function get_data_postage($idx)
	{
		$datacontent['datatable'] = $this->Model->get_data_postage($idx);
	}

	public function table_postage($id = "")
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_postage', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form($account_id = '', $id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		// $datacontent['parameter'] 	= $parameter;
		$datacontent['id']		 	= $id;
		$datacontent['account_id']	= $account_id;
		$datacontent['arrpostage_instance'] 	= $this->Model->get_postage_instance();
		$datacontent['arrseller'] 	= '<option></option>';
		$sellers = $this->db->query("SELECT * FROM m_account WHERE seller_id IS NOT NULL")->result_array();
		foreach ($sellers as $seller) {
			$datacontent['arrseller'] .= '<option value="' . $seller['account_id'] . '">' . str_replace("'", "", $seller['account_name']) . '</option>';
		}
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$post = $_POST;
			foreach ($_POST['input2'] as $index => $input) {


				$seller_id = $input['seller_id'];
				$bill = $input['tagihan'];
				$postage_receipt = $input['sales_code'];

				$account_id = $this->Model->chk_account($seller_id);

				$seller = $this->db->query("SELECT * FROM m_account WHERE seller_id IS NOT NULL AND account_id = '$account_id'")->row_array();
				$data = ([
					'seller_id' => $seller['account_code'],
					'postage_date' => $input['sales_date'],
					'postage_date_create' => date('Y-m-d H:i:s'),
					'postage_instance_id' => $_SESSION['supplier'],
					'postage_receipt' => $input['sales_code'],
					'user_id' => $_SESSION['user_id'],
				]);
				// @$path = $_FILES['transfer_'.$index]['name'];
				// if($_FILES['transfer_'.$index]['name'] != ""){
				// 	$ext = pathinfo($path, PATHINFO_EXTENSION);
				// 	$config['upload_path']          = './upload/POSTAGE/';
				// 	$config['allowed_types']        = '*';
				// 	$config['file_name']        = $data['postage_receipt'];
				// 	$this->load->library('upload', $config);
				// 	if ( ! $this->upload->do_upload('transfer_'.$index))
				// 	{
				// 		$error = array('error' => $this->upload->display_errors());
				// 	}
				// 	else
				// 	{
				// 		$this->upload->data();
				// 		$data['postage_receipt_file'] = $data['postage_receipt'].".".$ext;
				// 	}
				// }
				$this->Model->insert($data);
				$id = $this->Model->insert_id();
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $data['postage_date'];
				$datax['account_detail_category_id'] = '85';
				$datax['account_detail_realization'] = 1;
				//$datax['account_detail_quantity'] = ;
				$datax['postage_id'] = $id;
				$datax['account_detail_price'] = str_replace(',', '', $bill);
				$datax['account_detail_note'] = 'Ongkir Dengan Nomor Resi : ' . $data['postage_receipt'];
				$type = "debit";
				$datax['account_detail_debit'] = $datax['account_detail_price'];
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');
				$id_account_detail = $this->Model->insert_account_detail($datax);
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account($account_id)->row_array();
				if (@$row['account_date_reset'] > $datax['account_detail_date']) {
					$this->Model->update_balance($account_id,  $datax['account_detail_price'], $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $datax['account_detail_price'], $type);
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('T_postage');
	}


	public function form_pay_deposit($id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['id']		 	= $id;
		$datacontent['deposit'] 	= $this->Model->get_deposit($id);
		$datacontent['arraccount_detail'] 	= $this->Model->get_account_detail($id);
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view($this->url_ . '/form_pay_deposit', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_pending_product()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_pending_product', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_pending_product_detail($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_pending_product_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_pending_seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_pending_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_pending_product()
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_product();
	}

	public function get_data_pending_product_detail($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_product_detail($id);
	}

	public function get_data_pending_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_seller();
	}

	public function table_pending_seller_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrmember'] = $this->Model->get_member($id);
		$datacontent['arrpending_seller_detail'] = $this->Model->get_pending_seller_detail($id);
		$data['file'] = 'Data Pendingan Seller';
		$data['content'] = $this->load->view($this->url_ . '/table_pending_seller_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}




	public function form_edit_allow($id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['id']		 	= $id;
		$datacontent['sales_id'] = $this->Model->get_sales_id($id);
		$arraccount_detail_sales_product = $this->Model->get_account_detail_sales_product($id);
		foreach ($arraccount_detail_sales_product as $index => $value) {
			$datacontent['arraccount_detail_sales_product'][$value['product_id']] = $value['account_detail_sales_product_allow'];
			//$datacontent['sales_id'] = $value['sales_id'];
		}
		$datacontent['arrsales_detail'] 	= $this->Model->get_sales_detail2($datacontent['sales_id']);
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view($this->url_ . '/form_edit_allow', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		$datacontent['arrseller'] = $this->Model->get_seller($id);
		$datacontent['arrseller'] = $datacontent['arrseller'][0];
		$datacontent['arrproduct'] = $this->Model->get_produk();
		$datacontent['product_list'] = "";
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
			//$datacontent['product_list'] = $datacontent['product_list'].'<option value="'.$value['id_produk'].'">'.$value['nama_produk'].' ('.(($value['tipe']=="1")?"Barang Jadi":"Gudang").')</option>';
		}
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail2($id);
		$datacontent['deposit'] 	= $this->Model->get_deposit($id);
		$datacontent['arraccount_detail_sales'] = $this->Model->get_account_detail_sales($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function generate_code()
	{

		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWSO-";
		$dbDate	=	$this->db->query("SELECT COUNT(sales_id) as JumlahTransaksi FROM t_sales WHERE LEFT(sales_date, 7) = '" . date('Y-m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 1;
			}
		} else {
			$nJumlahTransaksi = 1;
		}
		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0000' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '000' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 4) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 5) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}
		return $cKode;
	}


	public function simpan_pay_deposit()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$id = $_POST['id'];
			$paid = $this->input->post('paid');
			foreach ($paid as $index => $value) {
				$this->Model->update_deposit($index, str_replace(',', '', $value));
			}
		}
		$post = $_POST;
		$this->simpan_pay_deposit_coa($post);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_postage/form_pay_deposit/' . $id));
	}

	public function simpan_pay_deposit_coa($post)
	{
		$id = $post['id'];
		$paids = $post['paid'];
		$dataHeader = ([
			'coa_transaction_date' => date('Y-m-d'),
			'date_create' => date('Y-m-d'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_realization' => 1,
			'coa_transaction_realization_date' => date('Y-m-d'),
			'coa_transaction_type' => 1,
		]);
		$this->db->insert('t_coa_transaction_header', $dataHeader);
		$idHeader = $this->db->insert_id();
		foreach ($post['account_detail_real_id'] as $index => $real_id) {
			if ($post['paid'][$index] > 0) {
				$ok = $post['postage_receipt'][$index];
				$acc_detail = $this->db->query("SELECT A.account_detail_real_id, A.account_detail_id FROM t_account_detail A LEFT JOIN t_postage B ON B.postage_id = A.postage_id WHERE A.account_id = '$id' AND B.postage_receipt = '$ok'")->row_array();
				$account = $this->db->get_where('m_account', ['account_id' => $id])->row_array();
				$coaDeposit = $this->db->get_where('coa_4', ['kode' => '200.1.2.' . $account['account_code2']])->row_array();
				$coaAset = $this->db->get_where('coa_4', ['id' => 3453])->row_array();

				$dataDeposit = ([
					'coa_name' => $coaDeposit['nama'],
					'coa_code' => $coaDeposit['kode'],
					'coa_date' => date('Y-m-d'),
					'coa_level' => 4,
					'coa_debit' => str_replace(",", "", $post['paid'][$index]),
					'coa_transaction_note' => 'Pembayaran Ongkir ' . $account['account_name'],
					'date_create' => date('Y-m-d H:i:s'),
					'user_create' => $_SESSION['user_id'],
					'coa_transaction_source' => 7,
					'coa_transaction_source_id' => $acc_detail['account_detail_real_id'],
					'coa_id' => $coaDeposit['id'],
					'coa_transaction_realization' => 1,
					'coa_transaction_source_type' => 7,
					'coa_group_id' => $idHeader,
				]);
				$this->db->insert('t_coa_transaction', $dataDeposit);
				$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $dataDeposit[coa_debit] WHERE coa_id = '$dataDeposit[coa_id]' AND coa_level = '$dataDeposit[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $dataDeposit[coa_debit] WHERE coa_id = '$dataDeposit[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $dataDeposit['coa_date'] . "', '-', '') AND coa_level = '$dataDeposit[coa_level]'");

				$dataAset = ([
					'coa_name' => $coaAset['nama'],
					'coa_code' => $coaAset['kode'],
					'coa_date' => date('Y-m-d'),
					'coa_level' => 4,
					'coa_credit' => str_replace(",", "", $post['paid'][$index]),
					'coa_transaction_note' => 'Pembayaran Ongkir ' . $account['account_name'],
					'coa_transaction_parent_id' => $acc_detail['account_detail_id'],
					'date_create' => date('Y-m-d H:i:s'),
					'coa_transaction_source' => 7,
					'coa_transaction_source_id' => $acc_detail['account_detail_real_id'],
					'coa_id' => $coaAset['id'],
					'coa_transaction_realization' => 1,
					'coa_group_id' => $idHeader,
				]);
				$this->db->insert('t_coa_transaction', $dataAset);
				$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $dataAset[coa_credit] WHERE coa_id = '$dataAset[coa_id]' AND coa_level = '$dataAset[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $dataAset[coa_credit] WHERE coa_id = '$dataAset[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $dataAset['coa_date'] . "', '-', '') AND coa_level = '$dataAset[coa_level]'");

				$this->db->query("UPDATE t_coa_transaction_header SET coa_transaction_debit = coa_transaction_debit + $dataDeposit[coa_debit], coa_transaction_credit = coa_transaction_credit + $dataAset[coa_credit]  WHERE coa_transaction_header_id = $idHeader");
			}
		}
	}

	public function simpan_edit_allow()
	{
		if ($this->input->post('simpan')) {
			$sales_id = $_POST['sales_id'];
			$id = $_POST['id'];
			$allow = $this->input->post('allow');
			foreach ($allow as $index => $value) {
				$data['account_detail_sales_product_allow'] = $value;
				$this->Model->update_account_detail_sales_product($data, ['account_detail_sales_id' => $id, 'sales_id' => $sales_id, 'product_id' => $index]);
			}
		}
		redirect(site_url('T_sales/table_detail/' . $sales_id));
	}

	public function simpan_detail()
	{
		if ($this->input->post('simpan')) {

			//$data = $this->input->post('input');
			$data2 = $this->input->post('input2');
			$id = $_POST['id'];
			$pending = $this->Model->reset_detail($_POST['id']);
			foreach ($data2 as $index => $value) {
				$value['sales_detail_price'] = str_replace(',', '', $value['sales_detail_price']);
				$value['sales_detail_quantity'] = str_replace(',', '', $value['sales_detail_quantity']);
				$data = $value;
				$data['sales_id'] = $_POST['id'];
				$data['user_id'] = $this->session->userdata('user_id');
				$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
				if (@$pending[$value['product_id']]['sales_detail_id'] != "") {
					if ($pending[$value['product_id']]['sales_detail_quantity_send'] < $value['sales_detail_quantity']) {
						$exec = $this->Model->update_detail_quantity($pending[$value['product_id']]['sales_detail_id'], $value['sales_detail_quantity']);
					}
					$dataxx['sales_detail_price'] = $value['sales_detail_price'];
					$this->Model->update_detail2($dataxx, ['sales_detail_id' => $pending[$value['product_id']]['sales_detail_id']]);
				} else {
					$exec = $this->Model->insert_detail($data);
				}
			}
			$total_price = $this->Model->get_sales_detail_total_price($id);
			$chk_account_detail = $this->Model->get_account_detail($id);
			if ($chk_account_detail['account_id'] != "") {
				$arrsales = $this->Model->get_sales($id);
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();

				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account($account_id)->row_array();
				$type = "debit";
				if (@$row['account_date_reset'] > $chk_account_detail['account_detail_date']) {
					$this->Model->reset_balance($account_id,  $chk_account_detail['account_detail_' . $type], $type, @$row['account_type_id'], $chk_account_detail['account_detail_date']);
				} else {
					$this->Model->reset_balance($account_id,  $chk_account_detail['account_detail_' . $type], $type);
				}
				//$this->Model->delete_account_detail($chk_account_detail['account_detail_real_id']);

				//$datax['account_id'] = $account_id;
				//$datax['account_detail_id'] = $id2;
				//$datax['account_detail_type_id'] = 3;
				//$datax['account_detail_user_id'] = $_SESSION['user_id'];
				//$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrsales['sales_date'];
				//$datax['account_detail_category_id'] = '81';
				//$datax['account_detail_realization'] = 1;
				//$datax['account_detail_quantity'] = ;
				//$datax['sales_id'] = $id;
				$datax['account_detail_price'] = $total_price;
				//$datax['account_detail_note'] = 'Pembelian dengan sales order : '.$arrsales['sales_code'];
				//$type = "debit";
				$datax['account_detail_debit'] = $total_price;
				//$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
				$id_account_detail = $this->Model->update_account_detail($datax, ['account_detail_real_id' => $chk_account_detail['account_detail_real_id']]);

				if (@$row['account_date_reset'] > $arrsales['sales_date']) {
					$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $total_price, $type);
				}
				//$data['account_detail_real_id'] = $id_account_detail;
			} else {
				$arrsales = $this->Model->get_sales($id);
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrsales['sales_date'];
				$datax['account_detail_category_id'] = '81';
				$datax['account_detail_realization'] = 1;
				//$datax['account_detail_quantity'] = ;
				$datax['sales_id'] = $id;
				$datax['account_detail_price'] = $total_price;
				$datax['account_detail_note'] = 'Pembelian dengan sales order : ' . $arrsales['sales_code'];
				$type = "debit";
				$datax['account_detail_debit'] = $total_price;
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');
				$id_account_detail = $this->Model->insert_account_detail($datax);
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account($account_id)->row_array();
				if (@$row['account_date_reset'] > $arrsales['sales_date']) {
					$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $total_price, $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			}
		}
		redirect(site_url('T_sales/table_detail/' . $id));
	}

	/*public function simpan_detail()
	{
		if ($this->input->post('simpan')) {		
			$data = $this->input->post('input');
			$data['sales_id'] = $_POST['id'];
			$data['user_id'] = $this->session->userdata('user_id');
			$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
			$arrproduct = $this->Model->get_product($data['product_id']);			
			
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $_POST['tanggal'];
				$datax['account_detail_category_id'] = '81';
				$datax['account_detail_realization'] = 1;
				$datax['account_detail_quantity'] = $data['sales_detail_quantity'];
				$datax['account_detail_price'] = $data['sales_detail_price'];
				$datax['account_detail_note'] = 'Pembelian '.$arrproduct['nama_produk'].' Rp. '.$data['sales_detail_price'].' * '.$data['sales_detail_quantity'].' Pcs (Rp. '.$data['sales_detail_price']*$data['sales_detail_quantity'].')';
				$type = "debit";
				$datax['account_detail_debit'] = $data['sales_detail_price']*$data['sales_detail_quantity'];
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
				$id_account_detail = $this->Model->insert_account_detail($datax); 
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account()->row_array();
				if(@$row['account_date_reset'] > $_POST['tanggal']){
					$this->Model->update_balance($account_id,  $data['sales_detail_price']*$data['sales_detail_quantity'], $type, @$row['account_type_id'], $datax['account_detail_date']);
				}else{
					$this->Model->update_balance($account_id,  $data['sales_detail_price']*$data['sales_detail_quantity'], $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			
			$exec = $this->Model->insert_detail($data);
		}
		redirect(site_url('T_sales/form_detail/'.$_POST['id']));
	}*/

	public function simpan_detail_all()
	{
		redirect(site_url('T_sales'));
	}

	public function report()
	{
		if (@$_POST['export_pdf']) {
			$this->export_pdf();
		} else {
			$datacontent['url'] = $this->url_;
			$datacontent['search'] = $this->input->post('search');
			$datacontent['url2'] = '/' . $datacontent['search']['member_status'] . '|' . $datacontent['search']['all'];
			$datacontent['title'] = 'Member';
			$data['file'] = $this->ind_;
			$data['content'] = $this->load->view($this->url_ . '/table_report', $datacontent, TRUE);
			$data['title'] = $datacontent['title'];
			$this->load->view('Layout/home', $data);
		}
	}

	public function export_pdf()
	{
		ob_start();
		$datacontent['data'] = $this->Model->get_report()->result_array();
		$this->load->view('M_member/export_pdf', $datacontent);
		//$data['content'] = $this->load->view('m_member/table_report', $datacontent);
		//$data['siswa'] = $this->siswa_model->view_row();
		//$this->load->view('print', $data);
		$html = ob_get_contents();
		ob_end_clean();

		require './assets/html2pdf/autoload.php';

		$pdf = new Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
		$pdf->WriteHTML($html);
		$pdf->Output(date('Y-m-d') . '.pdf', 'D');
	}



	public function get_data_report($search = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_report($search);
	}

	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}

	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}

	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}



	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {

				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_credit'] = $from['account_detail_debit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_credit'], 'credit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}

	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_send()
	{
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}

	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}

	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();


		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();

		if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
		} else {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
		}



		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_ . "/$id"));
	}

	public function list_kecamatan()
	{
		$id_kota = $this->input->post('id_kota');
		$stasiun = $this->Model->get_list_kecamatan($id_kota);
		$lists = "<option value=''>Pilih Kecamatan</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kecamatan . "'>" . $data->nama_kecamatan . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function list_kota()
	{
		$id_provinsi = $this->input->post('id_provinsi');
		$stasiun = $this->Model->get_list_kota($id_provinsi);
		$lists = "<option value=''>Pilih Kota</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kota . "'>" . $data->nama_kota . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function cetak_invoice($Aksi = "")
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['aksi'] = $Aksi;
		$data['title'] = $datacontent['title'];
		$data['arraccount_detail_sales'] = $this->Model->get_account_detail_sales2($Aksi);
		$this->load->view($this->url_ . '/inv-msglow', $data);
	}

	public function cetak_invoice_d($Aksi = "")
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$query = $this->model->code("SELECT * FROM t_sales WHERE sales_id = '" . $Aksi . "'");
		foreach ($query as $key => $vaData) {
			$kodeSales = $vaData['sales_code'];
		}
		$data['arraccount_detail_sales'] = $this->Model->get_account_detail_sales2($kodeSales);
		$data['aksi'] = $kodeSales;
		$data['title'] = $datacontent['title'];
		$this->load->view($this->url_ . '/inv-msglow', $data);
	}

	public function set($post, $id)
	{
		$this->db->trans_begin();
		$dataHeader = ([
			'coa_transaction_date' => date('Y-m-d'),
			'date_create' => date('Y-m-d'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_debit' => $post['input']['total'],
			'coa_transaction_credit' => $post['input']['total'],
			'coa_transaction_payment' => $post['input']['total'],
			'coa_transaction_realization' => 0,
		]);
		$this->db->insert('t_coa_transaction_header', $dataHeader);
		$idHeader = $this->db->insert_id();
		$ekspedisi = $this->db->get_where('m_postage_instance', ['postage_instance_id' => $post['input']['ekspedisi_id']])->row_array();
		$ekspedisi_coa = $this->db->query("SELECT * FROM coa_4 WHERE nama = 'Ongkir Dibayar Dimuka $ekspedisi[postage_instance_name]'")->row_array();
		if ($ekspedisi_coa == null) {
			$ekspedisi_coa = $this->Model->create_coa($ekspedisi);
		}
		$dataOngkir = ([
			'coa_name' => $ekspedisi_coa['nama'],
			'coa_code' => $ekspedisi_coa['kode'],
			'coa_date' => date('Y-m-d'),
			'coa_level' => 4,
			'coa_credit' => $post['input']['total'],
			'coa_transaction_note' => 'Ongkos Kirim ' . $ekspedisi['postage_instance_name'],
			'date_create' => date('Y-m-d'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_source' => 7,
			'coa_transaction_source_id' => $id,
			'coa_id' => $ekspedisi_coa['id'],
			'coa_group_id' => $idHeader,
			'coa_transaction_realization' => 0,
		]);

		foreach ($post['input2'] as $input) {
			$seller_coa = $this->db->query("SELECT * FROM coa_4 WHERE nama LIKE '%$input[seller_id]%'")->row_array();
			if ($seller_coa == null) {
				$seller_coa = $this->Model->create_coa_seller($seller);
			}
			$dataPiutang = ([
				'coa_name' => $seller_coa['nama'],
				'coa_code' => $seller_coa['kode'],
				'coa_date' => $input['sales_date'],
				'coa_level' => 4,
				'coa_debit' => $input['tagihan'],
				'coa_transaction_note' => 'Ongkos Kirim ' . $ekspedisi['postage_instance_name'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 7,
				'coa_transaction_source_id' => $id,
				'coa_group_id' => $idHeader,
				'coa_id' => $seller_coa['id'],
				'coa_transaction_realization' => 0,
			]);
			$this->db->insert('t_coa_transaction', $dataPiutang);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $dataPiutang[coa_debit] WHERE coa_id = $dataPiutang[coa_id] AND coa_level = '$dataPiutang[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $dataPiutang[coa_debit] WHERE coa_id = $dataPiutang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($input[sales_date], '-', '') AND coa_level = '$dataPiutang[coa_level]'");
		}

		$this->db->insert('t_coa_transaction', $dataOngkir);
		$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $dataOngkir[coa_credit] WHERE coa_id = $dataOngkir[coa_id] AND coa_level = '$dataOngkir[coa_level]'");
		$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $dataOngkir[coa_credit] WHERE coa_id = $dataOngkir[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataOngkir[coa_date], '-', '') AND coa_level = '$dataOngkir[coa_level]'");

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('T_postage');
	}

	public function set_jurnal_tagihan()
	{
		$this->db->trans_begin();
		$tagihan = $this->db->query("SELECT SUM(account_detail_debit) AS tagihan FROM t_account_detail WHERE account_detail_deposit_paid NOT IN(1) AND postage_id IS NOT NULL")->row_array();
		if ($tagihan['tagihan'] > 0) {
			$this->db->query("UPDATE t_account_detail SET account_detail_deposit_paid = 1 WHERE account_detail_deposit_paid NOT IN(1) AND postage_id IS NOT NULL");
			$dataHeader = ([
				'coa_transaction_date' => date('Y-m-d'),
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_debit' => $tagihan['tagihan'],
				'coa_transaction_credit' => $tagihan['tagihan'],
				'coa_transaction_payment' => $tagihan['tagihan'],
				'coa_transaction_realization' => 1,
				'coa_transaction_realization_date' => date('Y-m-d'),
			]);
			$this->db->insert('t_coa_transaction_header', $dataHeader);
			$dataHeader['coa_transaction_header_id'] = $this->db->insert_id();

			$coaKas = $this->db->get_where('coa_4', ['id' => 10])->row_array();
			$coaKasKeluar = $this->db->get_where('coa_4', ['id' => 3453])->row_array();

			$dataKasKeluar = ([
				'coa_name' => $coaKasKeluar['nama'],
				'coa_code' => $coaKasKeluar['kode'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_debit' => $tagihan['tagihan'],
				'coa_transaction_note' => 'Pembayaran tagihan ongkir',
				'coa_transaction_parent_id' => $dataHeader['coa_transaction_header_id'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 8,
				'coa_id' => $coaKasKeluar['id'],
				'coa_transaction_realization' => 1,
				'coa_transaction_source_type' => 7,
				'coa_group_id' => $dataHeader['coa_transaction_header_id'],
			]);
			$this->db->insert('t_coa_transaction', $dataKasKeluar);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $dataKasKeluar[coa_debit] WHERE coa_id = '$dataKasKeluar[coa_id]' AND coa_level = '$dataKasKeluar[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $dataKasKeluar[coa_debit] WHERE coa_id = '$dataKasKeluar[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $dataKasKeluar['coa_date'] . "', '-', '') AND coa_level = '$dataKasKeluar[coa_level]'");

			$dataKas = ([
				'coa_name' => $coaKas['nama'],
				'coa_code' => $coaKas['kode'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_credit' => $tagihan['tagihan'],
				'coa_transaction_note' => 'Pembayaran tagihan ongkir',
				'coa_transaction_parent_id' => $dataHeader['coa_transaction_header_id'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 8,
				'coa_id' => $coaKas['id'],
				'coa_transaction_realization' => 1,
				'coa_transaction_source_type' => 7,
				'coa_group_id' => $dataHeader['coa_transaction_header_id'],
			]);
			$this->db->insert('t_coa_transaction', $dataKas);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $dataKas[coa_credit] WHERE coa_id = '$dataKas[coa_id]' AND coa_level = '$dataKas[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $dataKas[coa_credit] WHERE coa_id = '$dataKas[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $dataKas['coa_date'] . "', '-', '') AND coa_level = '$dataKas[coa_level]'");
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('T_postage');
	}
}
