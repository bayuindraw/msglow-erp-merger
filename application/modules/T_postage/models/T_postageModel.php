<?php
defined('BASEPATH') or exit('No direct script access allowed');
class T_postageModel extends CI_Model
{

	var $table_ = "t_postage";
	var $id_ = "postage_id";
	var $eng_ = "postage";
	var $url_ = "T_postage";

	function get()
	{
		$data = $this->db->get($this->table_);
		return $data;
	}

	function search_member($search)
	{
		$search = str_replace('%20', ' ', $search);
		//echo "SELECT nama, kode, kota FROM member WHERE nama LIKE '%$search%' OR kode LIKE '%$search%'";
		//sdie(); 
		/*$data2 = $this->db->query("SELECT B.seller_id, SUM(A.sales_detail_quantity-A.sales_detail_quantity_send) AS total FROM t_sales_detail A LEFT JOIN t_sales B ON B.sales_id = A.sales_id WHERE A.sales_detail_quantity > A.sales_detail_quantity_send AND A.product_id = '$product_id' GROUP BY B.seller_id")->result_array(); 
		foreach($data2 as $index2 => $value2){
			$pending[$value2['seller_id']] = $value2['total'];  
		}*/
		$data = $this->db->query("SELECT nama, kode, kota FROM member WHERE nama LIKE '%$search%' OR kode LIKE '%$search%'")->result_array();
		$dat = array();
		foreach ($data as $index => $value) {
			$dat[$index] = array();
			$dat[$index]['id'] = $value["kode"];
			$dat[$index]['text'] = $value["nama"] . ' (' . $value["kode"] . ') (' . $value["kota"] . ')';
			//echo json_encode($dat[$index]);
		}
		$array = array(
			'results' => $dat,
			'pagination' => array('more' => true)
		);
		echo json_encode($array);
	}

	function get_postage_instance()
	{
		$arrdata = $this->db->query("SELECT * FROM m_postage_instance")->result_array();
		return $arrdata;
	}

	function chk_account($id)
	{
		$this->db->trans_begin();
		$arrchk = $this->db->query("SELECT account_id FROM m_account WHERE seller_id = '$id'")->row_array();
		$account_id = @$arrchk['account_id'];
		if (@$arrchk['account_id'] == "") {
			$arrmember = $this->db->query("SELECT nama, sales_category_id FROM member WHERE kode = '$id'")->row_array();
			$create = date('Y-m-d H:i:s');
			$data2['seller_id'] = $id;
			$data2['account_code'] = '81.' . $id;
			$data2['account_name'] = 'Akun ' . $arrmember['nama'] . ' (seller)';
			$data2['account_type_id'] = 1;
			$data2['account_date_create'] = $create;
			$data2['account_date_reset'] = $create;
			$arrcode = $this->db->query("SELECT (IFNULL(max(account_code2), 0) + 1) AS JUM FROM m_account WHERE seller_id IS NOT NULL")->row_array();
			$data2['account_code2'] =  $arrcode['JUM'];
			//if($arrmember['sales_category_id'] == ""){
			//	$data2['sales_category_id'] = $_SESSION['sales_category_id'];
			//}else{
			$data2['sales_category_id'] = $arrmember['sales_category_id'];
			//}
			$this->db->insert('m_account', $data2);
			$account_id = $this->db->insert_id();
			$data3['kode'] = '100.1.3.' . $data2['account_code2'];
			$data3['nama'] = 'Piutang ' . $arrmember['nama'] . ' (' . $id . ')';
			$data3['coa3_id'] = 12;
			$this->db->insert('coa_4', $data3);
			$id_coa_1 = $this->db->insert_id();
			$data3['kode'] = '200.1.2.' . $data2['account_code2'];
			$data3['nama'] = 'Deposit ' . $arrmember['nama'] . ' (' . $id . ')';
			$data3['coa3_id'] = 24;
			$this->db->insert('coa_4', $data3);
			$id_coa_2 = $this->db->insert_id();
			$data4 = array();
			$data4['coa_name'] = 'Piutang ' . $arrmember['nama'] . ' (' . $id . ')';
			$data4['coa_code'] = '100.1.3.' . $data2['account_code2'];
			$data4['coa_level'] = 4;
			$data4['coa_id'] = $id_coa_1;
			$this->db->insert('t_coa_total', $data4);
			$data4['coa_total_date'] = substr($tanggal, 0, 7) . "-01";
			$this->db->insert('t_coa_total_history', $data4);
			$data4 = array();
			$data4['coa_name'] = 'Deposit ' . $arrmember['nama'] . ' (' . $id . ')';
			$data4['coa_code'] = '200.1.2.' . $data2['account_code2'];
			$data4['coa_level'] = 4;
			$data4['coa_id'] = $id_coa_2;
			$this->db->insert('t_coa_total', $data4);
			$data4['coa_total_date'] = substr($tanggal, 0, 7) . "-01";
			$this->db->insert('t_coa_total_history', $data4);
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		return $account_id;
	}

	function get_data()
	{
		$table = "m_account A";
		$id = "account_id";
		$arrjoin[] = 'JOIN member B ON B.kode = A.seller_id';
		$field = array('kode', 'nama', 'account_deposit');
		$url = $this->url_;
		$arrwhere[] = "seller_id != ''";
		//$action = '<a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . "/table_detail/xid").'" class="btn btn-info"> <i class="fa fa-list"></i></a> <a href="'.site_url($url."/transfer_seller/tambah/xid").'" class="btn btn-info"> <i class="fa fa-random"></i></a>';
		$action = '<a href="' . site_url($url . "/form_pay_deposit/xid") . '" class="btn btn-info"> <i class="fa fa-list"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				if ($keyfield == 'account_debit' || $keyfield == 'account_credit' || $keyfield == 'account_debit - account_credit' || $keyfield == 'account_deposit') {

					$datax[] = "Rp " . number_format($valuer[$keyfield]);
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_postage($idx = "")
	{
		$table = "t_postage A";
		$id = "A.postage_id";
		$id2 = "postage_id";
		if ($idx != "") $arrwhere[] = "B.account_id = '$idx'";
		$arrjoin[] = 'JOIN t_account_detail B ON B.postage_id = A.postage_id';
		$arrorder[] = 'A.postage_date';
		$field = array('postage_receipt', 'postage_date', 'account_detail_debit', 'CASE WHEN (account_detail_debit-account_detail_paid) = 0 THEN "LUNAS" ELSE (account_detail_debit-account_detail_paid) END total');
		$field2 = array('postage_receipt', 'postage_date', 'account_detail_debit', 'total');
		$url = $this->url_;
		$arrwhere[] = "seller_id != ''";
		$action = '<a href="' . site_url($url . "/form_pay_deposit/xid") . '" class="btn btn-info"> <i class="fa fa-list"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'CASE WHEN (account_detail_debit-account_detail_paid) = 0 THEN "LUNAS" ELSE (account_detail_debit-account_detail_paid) END total') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field2 as $keyfield) {
				if ($keyfield == 'total') {
					if ($valuer[$keyfield] == "LUNAS") {
						$datax[] = $valuer[$keyfield];
					} else {
						$datax[] = "Rp " . number_format(($valuer[$keyfield] == "") ? 0 : $valuer[$keyfield]);
					}
				} else if ($keyfield == 'account_detail_debit') {
					$datax[] = "Rp " . number_format($valuer[$keyfield]);
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			$datax[] = str_replace('xid', $valuer[$id2], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_sales_detail_total_price($id)
	{
		$arrdata = $this->db->query("SELECT SUM(sales_detail_quantity * sales_detail_price) as total FROM t_sales_detail WHERE sales_id = '$id' GROUP BY sales_id")->row_array();
		return $arrdata['total'];
	}

	function get_deposit($id)
	{
		$arrdata = $this->db->query("SELECT account_deposit FROM m_account WHERE account_id = '$id'")->row_array();
		return $arrdata['account_deposit'];
	}

	function get_account_detail($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_account_detail A LEFT JOIN t_postage B ON B.postage_id = A.postage_id WHERE A.account_id = '$id' AND A.postage_id IS NOT NULL AND A.account_detail_debit > A.account_detail_paid")->result_array();
		return $arrdata;
	}

	function get_sales_detail2($id)
	{
		$arrdata = $this->db->query("SELECT A.*, B.*, SUM(account_detail_sales_product_allow) as send_allow FROM t_sales_detail A LEFT JOIN produk B ON B.id_produk = A.product_id LEFT JOIN t_account_detail_sales_product C ON C.sales_id = A.sales_id AND C.product_id = A.product_id WHERE A.sales_id = '$id' GROUP BY A.sales_id, A.product_id, A.sales_detail_price, A.sales_detail_id")->result_array();
		return $arrdata;
	}

	function get_sales_id($id)
	{
		$arrdata = $this->db->query("SELECT sales_id FROM t_account_detail_sales WHERE account_detail_sales_id = '$id'")->row_array();
		return $arrdata['sales_id'];
	}

	function get_account_detail_sales_product($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_account_detail_sales_product WHERE account_detail_sales_id = '$id'")->result_array();
		return $arrdata;
	}

	function get_sales($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_sales WHERE sales_id = '$id'")->row_array();
		return $arrdata;
	}

	function get_data_pending_product()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN produk B ON B.id_produk = A.id_barang";
		$arrjoin[] = "LEFT JOIN m_product_type C ON C.product_type_name = B.klasifikasi";
		$arrjoin[] = "LEFT JOIN t_sales_detail D ON D.product_id = A.id_barang AND D.sales_detail_quantity > D.sales_detail_quantity_send";
		$table = "tb_stock_produk A";
		$id = 'id_barang';
		$arrgroup[] = "A.id_barang";
		$arrorder[] = "C.product_type_order";
		$arrorder[] = "A.id_barang";
		$field = array('B.nama_produk', 'klasifikasi', 'jumlah', 'SUM(D.sales_detail_quantity-D.sales_detail_quantity_send)');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/table_pending_product_detail/xid") . '" class="btn btn-info"> <i class="fa fa-list"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'SUM(D.sales_detail_quantity-D.sales_detail_quantity_send)' && $value != 'jumlah') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				if ($keyfield == 'B.nama_produk') $keyfield = "nama_produk";
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_pending_product_detail($id)
	{
		$arrwhere[] = "(A.product_id = '$id')";
		$arrjoin[] = "LEFT JOIN t_sales B ON B.sales_id = A.sales_id";
		$arrjoin[] = "LEFT JOIN member C ON C.kode = B.seller_id";
		$table = "t_sales_detail A";
		$id = 'seller_id';
		$arrgroup[] = "B.seller_id";
		$arrorder[] = "SUM(A.sales_detail_quantity-A.sales_detail_quantity_send) DESC";
		$field = array('nama', 'kode', 'SUM(A.sales_detail_quantity-A.sales_detail_quantity_send)');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/table_pending_product_detail/xid") . '" class="btn btn-info"> <i class="fa fa-list"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'SUM(A.sales_detail_quantity-A.sales_detail_quantity_send)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				if ($keyfield == 'B.nama_produk') $keyfield = "nama_produk";
				$datax[] = $valuer[$keyfield];
			}
			//$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_sales_member($id)
	{
		$arrdata = $this->db->query("SELECT * FROM t_sales A LEFT JOIN member B ON B.kode = A.seller_id WHERE A.sales_id = '$id'")->row_array();
		return $arrdata;
	}

	function get_member($id)
	{
		$arrdata = $this->db->query("SELECT * FROM member WHERE kode = '$id'")->row_array();
		return $arrdata;
	}

	function get_pending_seller_detail($id)
	{
		$arrdata = $this->db->query("SELECT D.nama_produk, SUM(A.sales_detail_quantity-A.sales_detail_quantity_send) AS pendingan, D.klasifikasi FROM t_sales_detail A LEFT JOIN t_sales B ON B.sales_id = A.sales_id LEFT JOIN produk D ON D.id_produk = A.product_id WHERE A.sales_detail_quantity > A.sales_detail_quantity_send AND B.seller_id = '$id' GROUP BY A.product_id ORDER BY D.klasifikasi")->result_array();
		return $arrdata;
	}

	function get_data_pending_seller()
	{
		//$arrwhere[] = "(A.member_code IS NULL OR A.member_code = '')";
		$arrjoin[] = "LEFT JOIN t_sales B ON B.sales_id = A.sales_id";
		$arrjoin[] = "LEFT JOIN member C ON C.kode = B.seller_id";
		$table = "t_sales_detail A";
		$id = 'seller_id';
		$arrgroup[] = "B.seller_id";
		$arrorder[] = "B.seller_id";
		$field = array('nama', 'seller_id', 'SUM(A.sales_detail_quantity-A.sales_detail_quantity_send)');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/table_pending_seller_detail/xid") . '" class="btn btn-info"> <i class="fa fa-list"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value != 'SUM(A.sales_detail_quantity-A.sales_detail_quantity_send)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				//if($keyfield == 'B.nama_produk') $keyfield = "nama_produk"; 
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_seller($id = "")
	{
		if ($id != "") {
			$this->db->where('t_sales.sales_id', $id);
			$this->db->join('member', 't_sales.seller_id = member.kode');
			$this->db->join('m_account', 'm_account.seller_id = member.kode');
		}
		$data = $this->db->get('t_sales');
		return $data->result_array();
	}

	function get_product($id = "")
	{
		$this->db->where('id_produk', $id);
		$data = $this->db->get('v_sales_product');
		return $data->row_array();
	}

	function get_produk()
	{
		//$this->db->join('tb_stock_produk', 'tb_stock_produk.id_stock = produk.id_produk');
		$data = $this->db->query('SELECT *, A.nama_produk FROM produk A LEFT JOIN tb_stock_produk B ON B.id_barang = A.id_produk');
		return $data->result_array();
	}

	function get_sales_detail($id)
	{
		$this->db->where('sales_id', $id);
		$this->db->join('v_sales_product', 'v_sales_product.id_produk = t_sales_detail.product_id');
		$data = $this->db->get('t_sales_detail');
		return $data->result_array();
	}

	function get_account_detail_sales($id)
	{
		$this->db->where('A.sales_id', $id);
		//$this->db->join('t_account_detail B', 'B.account_detail_real_id = A.account_detail_real_id');
		$data = $this->db->get('t_account_detail_sales A');
		return $data->result_array();
	}

	function get_account_detail_sales2($code)
	{
		//$this->db->where('A.sales_id', $id);
		//$this->db->join('t_account_detail B', 'B.account_detail_real_id = A.account_detail_real_id');
		$data = $this->db->query("SELECT * FROM t_account_detail_sales A LEFT JOIN t_sales B ON B.sales_id = A.sales_id WHERE B.sales_code = '$code'");
		return $data->result_array();
	}

	function insert_id()
	{
		return $this->db->insert_id();
	}

	function get_sales_product()
	{
		$this->db->where('produk_status', '1');
		$data = $this->db->get('v_sales_product');
		return $data->result_array();
	}

	function get_data_report($search = '')
	{
		if ($search != "") {
			$arrsearch = explode("%7C", $search);
			$arrwhere[] = "A.member_status_id = '" . $arrsearch[0] . "'";
			$arrwhere[] = "(A.member_code LIKE '%" . $arrsearch[1] . "%' OR A.member_name LIKE '%" . $arrsearch[1] . "%' OR A.member_phone LIKE '%" . $arrsearch[1] . "%' OR A.member_address LIKE '%" . $arrsearch[1] . "%' OR D.nama_kecamatan LIKE '%" . $arrsearch[1] . "%' OR C.nama_kota LIKE '%" . $arrsearch[1] . "%')";
		}
		$arrwhere[] = "A.member_code != ''";
		$arrjoin[] = "LEFT JOIN mst_kecamatan D ON D.id_kecamatan = A.district_id AND D.id_kota = A.city_id";
		$arrjoin[] = "LEFT JOIN mst_kota C ON C.id_kota = A.city_id";
		$arrjoin[] = "LEFT JOIN m_member_status B ON B.member_status_id = A.member_status_id";
		$table = $this->table_;
		$id = $this->id_;
		$field = array('member_code', 'member_name', 'member_phone', 'nama_kota', 'member_status_name', 'member_phone', 'member_date');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/form/ubah/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="' . site_url($url . "/transfer/tambah/xid") . '" class="btn btn-info"> <i class="fa fa-list"></i></a> <a href="' . site_url($url . "/send/tambah/xid") . '" class="btn btn-info"> <i class="fa fa-download"></i></a> <a href="' . site_url($url . '/hapus/xid') . '" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}



	function get_detail($id)
	{
		$this->db->where($this->id_, $id);
		$this->db->join('m_account_detail_category B', 'B.account_detail_category_id = A.account_detail_category_id');
		$data = $this->db->get($this->table2_ . ' A');
		return $data;
	}

	function get_data_detail($idxx)
	{
		$table = $this->table2_ . ' A';
		$id = "CONCAT(account_id, '/', account_detail_id) AS id";
		$field = array('account_detail_category_name', 'account_detail_pic', 'account_detail_note', 'account_detail_debit', 'account_detail_credit', 'account_detail_realization');
		$arrjoin[] = 'LEFT JOIN m_account_detail_category B ON B.account_detail_category_id = A.account_detail_category_id';
		$url = $this->url_;
		$arrwhere[] = "account_id = $idxx";
		$action = '<a href="' . site_url($url . "/form_detail/ubah/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="' . site_url($url . '/hapus_detail/xid') . '" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(account_detail_id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(account_detail_id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer['id'], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_max_id()
	{
		$data = $this->db->query("SELECT (IFNULL(MAX(account_detail_id), 0) + 1) as id FROM t_account_detail")->row_array();
		return $data['id'];
	}

	function get_account_id($id)
	{
		$data = $this->db->query("SELECT account_id FROM m_account WHERE seller_id = '$id'")->row_array();
		return $data['account_id'];
	}

	function insert($data = array())
	{
		$this->db->insert($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_detail($data = array())
	{
		$this->db->insert($this->table_ . "_detail", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_account($data = array())
	{
		$this->db->insert('m_account', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function reset_detail($id)
	{
		$this->db->query("DELETE FROM t_sales_detail WHERE sales_detail_quantity_send = 0 AND sales_id = '$id'");
		$arrdata = $this->db->query("SELECT * FROM t_sales_detail WHERE sales_detail_quantity_send > 0 AND sales_id = '$id'")->result_array();
		foreach ($arrdata as $index => $value) {
			$pending[$value['product_id']] = $value;
		}
		return $pending;
	}

	function update_detail_quantity($id, $quantity)
	{
		$this->db->query("UPDATE t_sales_detail SET sales_detail_quantity = '$quantity' WHERE sales_detail_id = '$id'");
		return '';
	}

	function update_deposit($id, $quantity)
	{
		$arrdata = $this->db->query("SELECT account_id FROM t_account_detail WHERE postage_id = '" . $id . "'")->row_array();
		$this->db->query("UPDATE t_account_detail SET account_detail_paid = account_detail_paid + '" . $quantity . "', account_detail_deposit_paid = account_detail_deposit_paid + '" . $quantity . "' WHERE postage_id = '" . $id . "'");
		$this->db->query("UPDATE m_account SET account_deposit = account_deposit - '" . $quantity . "' WHERE account_id = '" . $arrdata['account_id'] . "'");
		return '';
	}

	function insert_account_detail_sales($data = array())
	{
		$this->db->insert("t_account_detail_sales", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_account_detail_sales_product($data = array())
	{
		$this->db->insert("t_account_detail_sales_product", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function insert_reset($data = array())
	{
		$this->db->insert($this->table_ . "_reset", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update_account_detail($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update('t_account_detail', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update_account_detail_sales_product($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update('t_account_detail_sales_product', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($this->table_);
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update_detail($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update('t_account_detail', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function update_detail2($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($this->table_ . "_detail", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}

	function delete_detail($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete('t_account_detail');
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}

	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}

	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}

	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}

	function insert_account_detail($data = array())
	{
		$this->db->insert("t_account_detail", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
		return $this->db->insert_id();
	}

	function get_m_account($id)
	{
		//$this->db->where('account_id', $id);
		$data = $this->db->get('m_account');
		return $data;
	}

	function update_balance($id, $balance, $type, $account_type = '', $monthly = '')
	{
		if ($type == "credit") {
			if ($monthly == '') {
				$this->db->query("UPDATE m_account SET account_credit = (account_credit + $balance) WHERE account_id = " . $id);
			} else {
				if ($account_type == '1') {
					$this->db->query("UPDATE m_account SET account_debit = (account_debit - ($balance)), account_monthly_debit = (account_monthly_debit - ($balance)) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit - ($balance)) WHERE account_reset_date > '$monthly'");
				} else {
					$this->db->query("UPDATE m_account SET account_credit = (account_credit + $balance), account_monthly_credit = (account_monthly_credit + $balance) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		} else if ($type == "debit") {
			if ($monthly == '') {
				$this->db->query("UPDATE m_account SET account_debit = (account_debit + $balance) WHERE account_id = " . $id);
			} else {
				if ($account_type == '2') {
					$this->db->query("UPDATE m_account SET account_credit = (account_credit - ($balance)), account_monthly_credit = (account_monthly_credit - ($balance)) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit - ($balance)) WHERE account_reset_date > '$monthly'");
				} else {
					$this->db->query("UPDATE m_account SET account_debit = (account_debit + $balance), account_monthly_debit = (account_monthly_debit + $balance) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}
		return '';
	}

	function reset_balance($id, $balance, $type, $account_type = '', $monthly = '')
	{
		if ($type == "credit") {
			if ($monthly == '') {
				$this->db->query("UPDATE m_account SET account_credit = (account_credit - $balance) WHERE account_id = " . $id);
			} else {
				if ($account_type == '1') {
					$this->db->query("UPDATE m_account SET account_debit = (account_debit + ($balance)), account_monthly_debit = (account_monthly_debit + ($balance)) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit + ($balance)) WHERE account_reset_date > '$monthly'");
				} else {
					$this->db->query("UPDATE m_account SET account_credit = (account_credit - $balance), account_monthly_credit = (account_monthly_credit - $balance) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit - ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		} else if ($type == "debit") {
			if ($monthly == '') {
				$this->db->query("UPDATE m_account SET account_debit = (account_debit - $balance) WHERE account_id = " . $id);
			} else {
				if ($account_type == '2') {
					$this->db->query("UPDATE m_account SET account_credit = (account_credit + ($balance)), account_monthly_credit = (account_monthly_credit + ($balance)) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit + ($balance)) WHERE account_reset_date > '$monthly'");
				} else {
					$this->db->query("UPDATE m_account SET account_debit = (account_debit - $balance), account_monthly_debit = (account_monthly_debit - $balance) WHERE account_id = " . $id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit - ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}
		return '';
	}

	function delete_account_detail($id)
	{
		$this->db->query("DELETE FROM t_account_detail WHERE account_detail_real_id = '$id'");
		return '';
	}

	public function create_coa($ekspedisi)
	{
		$row = $this->db->get_where('coa_4', ['coa3_id' => 175])->result_array();
		$ekspedisi_coa = ([
			'kode' => '100.1.12.' . (count($row) + 1),
			'nama' => 'Ongkir Dibayar Dimuka ' . $ekspedisi['postage_instance_name'],
			'coa3_id' => 175
		]);
		$this->db->insert('coa_4', $ekspedisi_coa);
		$ekspedisi_coa['id'] = $this->db->insert_id();
		$coaTot = ([
			'coa_id' => $ekspedisi_coa['id'],
			'coa_code' => $ekspedisi_coa['kode'],
			'coa_level' => 4,
			'date_create' => date('Y-m-d'),
			'coa_name' => $ekspedisi_coa['nama'],
		]);
		$this->db->insert('t_coa_total', $coaTot);
		$coaTot['coa_total_date'] = date('Y-m') . '-01';
		$this->db->insert('t_coa_total_history', $coaTot);

		return $ekspedisi_coa;
	}

	public function create_coa_seller($seller)
	{
		$row = $this->db->get_where('coa_4', ['coa3_id' => 12])->result_array();
		$seller_coa = ([
			'kode' => '100.1.3.' . (count($row) + 1),
			'nama' => 'Piutang ' . str_replace("Akun ", "", $seller['account_name']) . ' (' . $seller['seller_id'] . ')',
			'coa3_id' => 12
		]);
		$this->db->insert('coa_4', $seller_coa);
		$seller_coa['id'] = $this->db->insert_id();
		$coaTot = ([
			'coa_id' => $seller_coa['id'],
			'coa_code' => $seller_coa['kode'],
			'coa_level' => 4,
			'date_create' => date('Y-m-d'),
			'coa_name' => $seller_coa['nama'],
		]);
		$this->db->insert('t_coa_total', $coaTot);
		$coaTot['coa_total_date'] = date('Y-m') . '-01';
		$this->db->insert('t_coa_total_history', $coaTot);

		return $seller_coa;
	}
}
