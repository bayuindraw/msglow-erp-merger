<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH.'libraries/fpdf/html2pdf.php');

class T_sales_download extends CI_Controller
{

	var $url_ = "T_sales_download";

	public function __construct()
	{
		parent::__construct();
		ob_start();
		$this->load->model('model');

		$this->load->model(array(
			'T_salesModel'  =>  'Model',
		));

		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		# code...
	}

	function download_pdf($id)
	{
		$query = $this->model->code("SELECT * FROM t_sales WHERE sales_id = '" . $id . "'");
		foreach ($query as $key => $vaData) {
			$kodeSales = $vaData['sales_code'];
			$seller_id = $vaData['seller_id'];
		}
		$arraccount_detail_sales = $this->Model->get_account_detail_sales2($kodeSales);
		$member = $this->db->query("SELECT * FROM member WHERE kode = '$seller_id'")->row();

		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$arrsales = $this->model->code("SELECT sales_address FROM t_sales WHERE sales_code = '" . $kodeSales . "'");
		foreach ($arrsales as $key => $vaIsian) {
			$alamatTagih = $vaIsian['sales_address'];
		}
		$query = $this->model->code("SELECT * FROM v_sales_inv WHERE sales_code = '" . $kodeSales . "'");
		foreach ($query as $key => $vaData) {
			$namaTagih = $vaData['nama'];
			$kodeTagih = $vaData['seller_id'];
    		//$alamatTagih = $vaData['alamat'];
			$kotaTagih = $vaData['kota'];
			$invoceCode = $vaData['sales_code'];
			$salesDate = $vaData['sales_date'];
		}

		$pdf=new PDF('P','mm','A4');
		$pdf->SetTextColor(33, 37, 41, 1);
		$pdf->AddPage();
		$pdf->SetTitle("INVOICE ".$id);
		$pdf->SetLeftMargin(20);

		// $pdf->Image('https://msglow.work/assets/logo.png',20,12,40,0,'PNG');
		$pdf->Image(BASEPATH.'../assets/logo.png',40,12,50);
		// Title
		$pdf->SetFont('Arial','B',14);
		$pdf->Ln(2);
		$pdf->Cell(110);
		$pdf->Cell(60,7,'Invoice',0,0,'C');
		$pdf->Ln();
		$pdf->Cell(110);
		$pdf->SetFont('Arial','',11);
		$pdf->Cell(60,7,$invoceCode,0,0,'C');

		$pdf->SetDrawColor(237, 237, 237);
		$pdf->SetLineWidth(0.3);
		$pdf->Line(20,28,190,28);
		$pdf->Ln(12);

		$pdf->SetFont('Arial','B', 11);
		$pdf->SetWidths(array(90,80));
		$pdf->Row6(array(
			'Ditagihkan Untuk :',
			'Pembayaran Ke :'
		),
		array('L','L'),6);
		$pdf->SetFont('Arial','', 10);
		$pdf->Row6(array(
			$member->nama."\n".$member->kode."\n".str_replace("</p>", "", str_replace("<p>", "", $alamatTagih)),
			"PT. Kosmetika Cantik Indonesia\nJl. Komud ABD. Saleh No.58, Krajan, Asrikaton\nKec. Pakis, Malang, Jawa Timur"
		),
		array('L','L'),6);
		$pdf->Ln(4);
		$pdf->SetFont('Arial','B', 11);
		$pdf->Row6(array(
			'Cara Pembayaran :',
			'Tanggal Invoice :'
		),
		array('L','L'),6);
		$pdf->SetFont('Arial','', 10);
		$pdf->Row6(array(
			'-',
			(int)substr($salesDate, 8, 2) . " " . $arrbln[substr($salesDate, 5, 2)] . " " . substr($salesDate, 0, 4)
		),
		array('L','L'),6);
		$pdf->Ln(5);

		$pdf->SetFillColor(237, 237, 237);
		$pdf->SetFont('Arial','', 14);
		$pdf->Cell(3,10,"",0,0,'L', true);
		$pdf->Cell(167,10,"Invoice Detail",0,0,'L', true);
		$pdf->Ln(12);
		$pdf->SetFont('Arial','B', 10);
		$pdf->SetDrawColor(185, 185, 185, 1);
		$pdf->SetWidths(array(50,37,37,40));
		$pdf->Cell(3);
		$pdf->Row6(array(
			'Produk',
			'Qty',
			'Harga Satuan',
			'Sub Total'
		),
		array('L','C','L','R'),8,'BOTTOM');

		$pdf->SetFont('Arial','', 10);
		$no = 0;
		$total[] = 0;
		$queryInv = $this->model->code("SELECT A.*, C.nama_produk FROM t_sales_detail A JOIN t_sales B ON B.sales_id = A.sales_id JOIN produk C ON A.product_id = C.id_produk WHERE B.sales_code = '" . $kodeSales . "' ");
		foreach ($queryInv as $key => $vaDetail) {
			$pdf->Cell(3);
			$pdf->Row6(array(
				$vaDetail['nama_produk'],
				$vaDetail['sales_detail_quantity'].' Pcs',
				'Rp '.number_format($vaDetail['sales_detail_price']),
				'Rp '.number_format($total[] = $vaDetail['sales_detail_price'] * $vaDetail['sales_detail_quantity'])
			),
			array('L','C','L','R'),8,'BOTTOM');
		}

		$pdf->SetFont('Arial','B', 10);
		$pdf->Cell(3,8,"",0,0,'L');
		$pdf->Cell(124,8,"Sub Total",'B',0,'L', true);
		$pdf->SetFont('Arial','', 10);
		$pdf->Cell(40,8,"Rp ".number_format(array_sum($total)),'B',0,'R', true);
		$pdf->Ln();

		$total2 = 0;
		$pdf->SetWidths(array(124,40));
		foreach ($arraccount_detail_sales as $value) {
			$pdf->Cell(3);
			$pdf->Row6(array(
				(int)substr($value['account_detail_sales_date'], 8, 2) . " " . $arrbln[substr($value['account_detail_sales_date'], 5, 2)] . " " . substr($value['account_detail_sales_date'], 0, 4),
				'Rp '.number_format($value['account_detail_sales_amount'])
			),
			array('L','R'),8,'BOTTOM');
			$total2 += $value['account_detail_sales_amount'];
		}

		$pdf->SetFont('Arial','B', 10);
		$pdf->Cell(3,8,"",0,0,'L');
		$pdf->Cell(124,8,"Kurang Bayar",'B',0,'L', true);
		$pdf->SetFont('Arial','', 10);
		$pdf->Cell(40,8,"Rp ".number_format(array_sum($total) - $total2),'B',0,'R', true);
		$pdf->Ln(12);
		$pdf->SetFont('Arial','B', 8);
		$pdf->Cell(45,8,"NOTE : ",0,0,'R');
		$pdf->SetFont('Arial','', 8);
		$pdf->Cell(125,8,"This is computer generated receipt and does not require physical signature.",0,0,'L');

		$pdf->Output('D','INVOICE_'.$invoceCode.'.pdf');
		// $pdf->Output();
	}
}