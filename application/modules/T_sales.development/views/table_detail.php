<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											<table>
												<tr>
													<td>Nomor SO</td>
													<td>&nbsp;&nbsp;:</td>
													<td>&nbsp;&nbsp;<?= $kodeSO = $arrsales_member['sales_code'] ?></td>
												</tr>
												<tr>
													<td>Seller</td>
													<td>&nbsp;&nbsp;:</td>
													<td>&nbsp;&nbsp;<?= $arrsales_member['nama'].' ('.$arrsales_member['kode'].')' ?></td>
												</tr>
											</table>
											
										</h3>
									</div>
									<!--<div class="kt-portlet__head-toolbar xhide">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<a href="<?= site_url().$url."/form_pay_deposit/".$id ?>" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-money"></i>
													Deposit (Rp <?= @number_format($deposit) ?>)
												</a>
											</div>
										</div>
									</div>-->
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<div class="col-sm-12">
										<!--
									<label>Cek Diskon Disini:</label>
										<select class="form-control kt-select2" name="cek_diskon_disini" 
											id="cek_diskon_disini"
											 onchange="return cek_diskon_disini(this.value)">
											<option>--Pilih Diskon--</option>
											
											<?php 
											$dbComboTipeDiskon = $this->db->query("SELECT discount_id,discount_name FROM v_diskon_member_arga WHERE 
												member_status = '".$arrsales_member['kode']."' and discount_active = 1  GROUP BY discount_name ");

												foreach ($dbComboTipeDiskon->result_array() as $key => $vaTipeDiskon) {
													
													$dbComboKategoriDiskon =  $this->db->query("SELECT discount_id,discount_category,kategori_diskon,tipe_diskon FROM v_diskon_member_arga WHERE 
												member_status = '".$arrsales_member['kode']."' and discount_id = '".$vaTipeDiskon['discount_id']."'  ");
											?>
											<optgroup label="<?=$vaTipeDiskon['discount_name']?>">
												<?php 
												foreach ($dbComboKategoriDiskon->result_array()  as $key => $vaKategoriDiskon) {
												?>
    											<option value="<?=$id?>-<?=$vaKategoriDiskon['discount_id']?>">
    												<?=$vaKategoriDiskon['tipe_diskon']?> - <?=$vaKategoriDiskon['kategori_diskon']?></option>
    										<?php } ?>
  											</optgroup>
  											<?php } ?> 

										</select>
										<hr/>
									-->

								</div>
								<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Produk</th>
											<th>Jumlah</th>
											<th>Boleh Kirim</th>
											<th>Sudah Kirim</th>
											<th>Harga Satuan</th>  
											<th>Sub Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$total[] = 0;
										$no = 0;
										foreach ($arrsales_detail as $key => $vaData) {
											?>
											<tr>
												<td><?= ++$no ?></td>
												<td><?= $vaData['nama_produk'] ?></td>
												<td><?= $vaData['sales_detail_quantity'] ?> Pcs</td>
												<td><?= $vaData['connected_quantity'] + $vaData['pending_quantity'] ?> Pcs</td>
												<td><?= $vaData['connected_quantity'] ?> Pcs</td>
												<td align="right">Rp. <?= number_format($vaData['sales_detail_price']) ?> </td>
												<td align="right">Rp. <?= number_format($total[] = $vaData['sales_detail_price']*$vaData['sales_detail_quantity']) ?></td>
											</tr>
										<?php } ?>

									</tbody>
									<tfoot>
										<tr>
											<td colspan="6" align="right" style="font-weight: 900">Total Nominal Sales Order</td>
											<td style="font-weight: 900" align="right">Rp. <?=number_format(array_sum($total))?></td>
										</tr>
									</tfoot>
								</table>
								

								<?php 
								$dbCekDiskon = $this->db->query("SELECT * FROM v_apply_diskon_arga where id_sales = '".$id."' GROUP BY id_sales,kategori_diskon ");
								if($dbCekDiskon->num_rows() > 0){
									?>
									<h4>Diskon</h4>		
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>No</th>
												<th>Tanggal Apply</th>
												<th>Nama Diskon</th>
												<th>Tipe Diskon</th>
												<th>Kategori Diskon</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$no = 0;
											foreach ($dbCekDiskon->result_array() as $key => $vaDiskon) {
												if($vaDiskon['kategori_diskon'] == 'Kelipatan'){
													$cKategoriDiskon = 'Free Produk';
												}else if($vaDiskon['kategori_diskon'] == 'Minimal Pembelian Per PCS'){
													$cKategoriDiskon = 'Free Produk & || Diskon Persentase)';
												}else if($vaDiskon['kategori_diskon'] == '0'){
					$cKategoriDiskon = 'Tiering '.$vaDiskon['tiering'].'';
				}
												?>
												<tr onclick="hideDetail(<?= $key ?>)">
													<td> <img onclick="hideDetail(<?= $key ?>)" src="https://media.istockphoto.com/vectors/plus-and-circle-vector-id1286838570?b=1&k=20&m=1286838570&s=170667a&w=0&h=RvDKabtdeNbn7LyRnEYxSNQ0bZ-yyoUIvubwgCeT9NU=" style="height: 20px"> <?=++$no?>  </td>
													<td><?=$vaDiskon['tanggal_apply_diskon']?></td>
													<td><?=$vaDiskon['discount_name']?></td>
													<td><?=$vaDiskon['tipe_diskon']?></td>
													<td><strong><?=($cKategoriDiskon)?></strong></td>
													<td> <img onclick="hideDetail(<?= $key ?>)" 
														src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQeCSlagshsHAKeN0DX6tQqdchj35atCH1C8S-oR2w9g3qXA6hTH1hS0dAcS7XWO38dju4&usqp=CAU" style="height: 20px">   </td>
												</tr>

												<tr id="<?= $key ?>x" style="display: none;">
													<td colspan="5">
				<?php 
				if($vaDiskon['tipe_diskon'] == 'Non Tiering' and $vaDiskon['kategori_diskon'] == 'Kelipatan'){
				?>									
				<table style="width: 100%" class="table">
					<thead>
					<tr>
						<th>No</th>
						<th>Nama Produk</th>
						<th>Quantity</th>
					</tr>
					</thead>
					<tbody>
						<?php 
						$cNoKelipatan = 0;
						$dbRewardProduk = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$vaDiskon['id_diskon']."'  ");
						foreach ($dbRewardProduk->result_array() as $key => $vaRewardProduk) {
							?>
							<tr>
								<td><?=++$cNoKelipatan?></td>
								<td><?=$vaRewardProduk['nama_produk']?></td>
								<td><?=$vaRewardProduk['discount_reward_product_quantity']?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>

			<?php }else if($vaDiskon['tipe_diskon'] == 'Non Tiering' and $vaDiskon['kategori_diskon'] == 'Minimal Pembelian Per PCS'){ ?>

				<?php 
						$dbRewardPersen = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$vaDiskon['id_diskon']."' and discount_reward_product_percentage is NOT NULL LIMIT 0,1  ");
						foreach ($dbRewardPersen->result_array() as $key => $vaRewardPersen) {
							$nProsentase = $vaRewardPersen['discount_reward_product_percentage'];
						}
				?>


				<table style="width: 100%" class="table">
					<thead>
					<tr>
						<th>No</th>
						<th>Nama Produk</th>
						<th>Quantity</th>
						<th>Nominal Pembelian</th>
						<th>Total Diskon <?=$nProsentase?> % </th>
						<th>Total Setelah Di Diskon </th>
					</tr>
					</thead>
					<tbody>
						<?php 
						$cNoProsentase = 0;
						$nTotalNominal = 0;
						$nTotalDiskon  = 0;
						$nTotalAfterDiskon = 0;
						$dbSalesDetail = $this->db->query("SELECT * FROM t_sales_detail WHERE sales_id = '".$vaDiskon['id_sales']."'  ");
						foreach ($dbSalesDetail->result_array() as $key => $vaSalesDetail) {
							$nDiskon = (($vaSalesDetail['sales_detail_price']*$vaSalesDetail['sales_detail_quantity']) *$nProsentase / 100);
							
							$nAfterDiskon= (($vaSalesDetail['sales_detail_price']*$vaSalesDetail['sales_detail_quantity']) - $nDiskon) ;
							
							$nTotalNominal += $vaSalesDetail['sales_detail_price'] * $vaSalesDetail['sales_detail_quantity'];

							$nTotalDiskon  += $nDiskon;
							$nTotalAfterDiskon += $nAfterDiskon;
							?>
							<tr>
								<td><?=++$cNoProsentase?></td>
								<td><?=$vaSalesDetail['product_global_name']?></td>
								<td><?=$vaSalesDetail['sales_detail_quantity']?></td>
								<td>Rp. <?=number_format($vaSalesDetail['sales_detail_price']*$vaSalesDetail['sales_detail_quantity'])?></td>
								<td>Rp. <?=number_format($nDiskon)?></td>
								<td>Rp. <?=number_format($nAfterDiskon)?></td>
							</tr>
						<?php } ?>
						<tfoot>
							<tr>
								<td colspan="3"></td>
							
								<td><strong>Rp. <?=number_format($nTotalNominal)?></strong></td>
								<td><strong>Rp. <?=number_format($nTotalDiskon)?></strong></td>
								<td><strong>Rp. <?=number_format($nTotalAfterDiskon)?></strong></td>
							</tr>
						</tfoot>
					</tbody>
				</table> <br/>
				<h4>Free Produk</h4>
				<table class="table">
					<thead>
					<tr>
						<th>No</th>
						<th>Nama Produk</th>
						<th>Quantity</th>
					</tr>
					</thead>
					<tbody>
						<?php 
						$cNoProsentaseProduk = 0;
						$dbRewardProduk = $this->db->query("SELECT * FROM v_diskon_terms_reward_arga WHERE discount_id = '".$vaDiskon['id_diskon']."' and discount_reward_product_percentage is null  ");
						foreach ($dbRewardProduk->result_array() as $key => $vaRewardProduk) {
							?>
							<tr>
								<td><?=++$cNoProsentaseProduk?></td>
								<td><?=$vaRewardProduk['nama_produk']?></td>
								<td colspan="3"><?=$vaRewardProduk['discount_reward_product_quantity']?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			<?php }else if($vaDiskon['tipe_diskon'] == 'Tiering' and $vaDiskon['kategori_diskon'] == '0'){ ?>
								
								<table style="width: 100%" class="table">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Produk</th>
											<th>Quantity</th>
											<th>Harga Produk</th>
											<th>Harga Diskon tiering</th>
											<th>Total Harga Produk </th>
											<th>Total Diskon </th>
											<th>Total Bayar </th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$cNoTier = 0;
										$nTotalHarga = 0;
										$nTotalDiskon = 0;
										$nDiskon = 0;
										$dbTier = $this->db->query("SELECT s.product_global_name,s.sales_detail_quantity,s.sales_detail_price,m.discount_terms_product_price
											FROm
											t_sales_detail s,m_discount_terms_product m where s.product_global_id = m.product_global_id and 
												s.sales_id = '".$vaDiskon['id_sales']."' and 
												m.discount_terms_product_quantity = '".$vaDiskon['tiering']."' and m.discount_id = '".$vaDiskon['id_diskon']."'");
										foreach ($dbTier->result_array() as $key => $vaTier) { 
											$nTotalHarga += $vaTier['sales_detail_price']*$vaTier['sales_detail_quantity'];
											$nTotalDiskon += $vaTier['discount_terms_product_price']*$vaTier['sales_detail_quantity'];
											$nDiskon += ($vaTier['sales_detail_price']-$vaTier['discount_terms_product_price']) * $vaTier['sales_detail_quantity']
										?>
										<tr>
											<td><?=++$cNoTier?></td>
											<td><?=$vaTier['product_global_name']?></td>
											<td><?=$vaTier['sales_detail_quantity']?></td>
											<td><?=number_format($vaTier['sales_detail_price'])?></td>
											<td><strong><?=number_format($vaTier['discount_terms_product_price'])?></strong></td>
											<td><?=number_format($vaTier['sales_detail_price']*$vaTier['sales_detail_quantity'])?> </td>

											<td><?=number_format(
	($vaTier['sales_detail_price']-$vaTier['discount_terms_product_price']) * $vaTier['sales_detail_quantity'])?> </td>
											<td><?=number_format($vaTier['discount_terms_product_price']*$vaTier['sales_detail_quantity'])?> </td>
											
										</tr>
										<?php 
											}
										?>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5"></td>
											<td><strong><?=number_format($nTotalHarga)?></strong></td>
											<td><strong><?=number_format($nDiskon)?></strong></td>
											<td><strong><?=number_format($nTotalDiskon)?></strong></td>
										</tr>
									</tfoot>
								</table>
							<?php } ?>

													</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>

									<script type="text/javascript">
										function hideDetail(key){
											if($('#'+key+'x').attr('style') == 'display:none;'){
												$('#'+key+'x').attr('style', '')
											}else{
												$('#'+key+'x').attr('style', 'display:none;')
											}
										}
									</script>
								<?php }else{ ?>
									<h4>Diskon</h4>	
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>No</th>
												<th>Tanggal Apply</th>
												<th>Nama Diskon</th>
												<th>Tipe Diskon</th>
												<th>Kategori Diskon</th>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="5"><h4>Tidak Ada Diskon Yang Di Klaim</h4></td>
											</tr>
										</tbody>
									</table>
								<?php } ?>
								<br/>


								<a href="<?=base_url()?>T_sales/cetak_invoice/<?=$kodeSO?>" target="_blank" class="btn btn-primary waves-effect waves-light"> <i class="fa fa-print"></i> Cetak Invoice</a>	
							</div>
						</div> 
						<?php $total_discount_sales = ((@count($arrsales_discount) != "")?count($arrsales_discount):0);
						if($total_discount_sales > 0){ ?>
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											Diskon 
											
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">
									* B = Boleh Kirim, T = Terkirim, P = Pending
									<!--begin: Datatable -->
									<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
										<thead>
											<tr>
												<th>Nama</th>
												<th>Gratis Barang</th>
												<th>Detail Diskon Harga</th>
												<th>Total Diskon Harga</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$total[] = 0;
											$no = 0;
											foreach ($arrsales_discount as $key => $vaData2) {
												?>
												<tr> 
													<td><?= $vaData2['discount_name'] ?></td>
													<td>
														<?php if($vaData2['sales_discount_product'] == "1"){ 
															foreach($arrsales_discount_product[$vaData2['sales_discount_id']] as $indexsales_discount_product => $valuesales_discount_product){ ?>
																- <?= $valuesales_discount_product['sales_discount_product_target'].' '.$valuesales_discount_product['nama_produk']." (B = ".($valuesales_discount_product['connected_quantity']+$valuesales_discount_product['pending_quantity'])." | T = ".$valuesales_discount_product['connected_quantity']." | P = ".$valuesales_discount_product['pending_quantity']."".")" ?> <br>
															<?php } 
														} ?>
													</td>
												</tr>
											<?php } ?>

										</tbody>
									</table>	
								</div>
							</div> 
						<?php } ?>
						<?php 
						$total_detail_sales = ((@count($arraccount_detail_sales_success) != "")?count($arraccount_detail_sales_success):0);
						if($total_detail_sales > 0){  
							?> 
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											History Pembayaran
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">
									<ul class="nav nav-tabs nav-tabs-line" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#success" role="tab">Success</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#rejected" role="tab">Rejected</a>
										</li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="success" role="tabpanel">
											<!--begin: Datatable -->
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>Tanggal</th>
														<!-- <th>Nama Seller</th> -->
                                                        <th style="text-align:right">Total Harga</th>
														<th style="text-align:right">Total Diskon Tiering</th>
                                                        <th style="text-align:right">Total Diskon Percentage</th>
                                                        <th style="text-align:right">Total Bayar</th>
														<th>History Pembayaran</th>
														<th class="xhide">Aksi</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$total[] = 0;
													$no = 0;
                                                    $nTotalDiskon = 0;
                                                    $nTotalBayar = 0;
                                                    $nTotalHarga = 0;
                                                    $nTotalDiskonTier = 0;
                                                    $nTotalDiskonPercen=0;
													foreach ($arraccount_detail_sales_success as $key => $vaData2) {
                                
                                $dbCekDiskon = $this->db->query("SELECT * FROM v_apply_diskon_arga where 
								id_sales = '".$id."' and
								tipe_diskon = 'Tiering'
								");
								if($dbCekDiskon->num_rows() > 0){
								foreach ($dbCekDiskon->result_array() as $key => $vaDiskon3) {
									$nTermTiering = $vaDiskon3['tiering'];
									$cIdDiskon    = $vaDiskon3['id_diskon'];

								}

							$dbTier = $this->db->query("
                            SELECT 
                            s.account_detail_sales_id,
                            (SUM(s.account_detail_sales_product_allow*d.sales_detail_price) - 
                             SUM(s.account_detail_sales_product_allow*m.discount_terms_product_price)) as total_diskon
									FROM
									m_discount_terms_product m,t_account_detail_sales_product s,t_sales_detail d where
									d.sales_id = s.sales_id and
									d.product_global_id = s.product_global_id and 
									d.sales_detail_id = s.sales_detail_id and
									s.product_global_id = m.product_global_id and 
									s.sales_id = '".$id."' and 
									m.discount_terms_product_quantity = '".$nTermTiering."' and 
									m.discount_id = '".$cIdDiskon."' and
									s.account_detail_sales_id = '".$vaData2['account_detail_sales_id']."'
                                    GROUP BY s.account_detail_sales_id
									");
							foreach ($dbTier->result_array() as $key => $vaTier) {
								
							//$nTotalDiskon += ($vaTier['sales_detail_price']*
							//$vaTier['account_detail_sales_product_allow']) - ($vaTier['discount_terms_product_price'] * 
							//$vaTier['account_detail_sales_product_allow']);

                            $nTotalDiskon = str_replace(".00","",$vaTier['total_diskon']);
							}
						}else{
                            $nTotalDiskon = 0;
                        }
						?>

                        <?php 
							$nDiskonPercentageHeader = 0 ;
                            $cPersentaseHeader = 0;
							$dbCekDiskon2Header = $this->db->query("SELECT * FROM v_apply_diskon_arga where 
								id_sales = '".$id."' and
								tipe_diskon = 'Non Tiering' and 
								kategori_diskon = 'Minimal Pembelian Per PCS' and 
								discount_reward_product_percentage IS NOT NULL LIMIT 0,1
								");
								if($dbCekDiskon2Header->num_rows() > 0){
									foreach ($dbCekDiskon2Header->result_array() as $key => $vaDiskon2Header) {
										$cPersentaseHeader = $vaDiskon2Header['discount_reward_product_percentage'];
										$nDiskonPercentageHeader = $total2[] = $vaData2['account_detail_sales_amount'] * $cPersentaseHeader / 100 ;
									}
								}else{
									$nDiskonPercentageHeader = 0 ;
								}

                                $nTotalBayar += $total2[] = $vaData2['account_detail_sales_amount']-$nTotalDiskon-$nDiskonPercentageHeader;
                                $nTotalHarga += $total2[] = $vaData2['account_detail_sales_amount'];
                                $nTotalDiskonTier+= $nTotalDiskon;
                                $nTotalDiskonPercen+=$nDiskonPercentageHeader;
                            ?>
														<tr>
															<td><?= ++$no ?></td>
															<td><?= $vaData2['account_detail_sales_date'] ?></td>
															<!-- <td><?= $vaData2['account_detail_sales_date'] ?></td> -->
                                                            <td align="right">Rp. <?= number_format($total2[] = $vaData2['account_detail_sales_amount']) ?></td>
                                                            <td align="right">Rp. <?= number_format($nTotalDiskon) ?></td>
                                                            <td align="right"><?=$cPersentaseHeader?>% - Rp. <?= number_format($nDiskonPercentageHeader) ?></td>
														    <td align="right">Rp. <?= number_format($total2[] = $vaData2['account_detail_sales_amount']-$nTotalDiskon-$nDiskonPercentageHeader) ?></td>
															<td class="xhide"><a href="<?= site_url().$url."/history_pembayaran/".$id."/".$vaData2['account_detail_id'] ?>" class="btn btn-xs btn-success"> <i class="fa fa-list"></i></a></td>
															<td class="xhide"><a href="<?= site_url().$url."/form_edit_allow/".$vaData2['account_detail_sales_id'] ?>" class="btn btn-xs btn-info"> <i class="fa fa-search"></i></a></td>
														</tr>
													<?php } ?>
												</tbody>
												<tfoot>
													<tr>
														<td colspan="2" align="right" style="font-weight: 900">Total</td>
                                                        <td  style="font-weight: 900"align="right">Rp.<?=number_format(($nTotalHarga)) ?></td>
                                                        <td  style="font-weight: 900"align="right">Rp.<?=number_format(($nTotalDiskonTier)) ?></td>
                                                        <td style="font-weight: 900" align="right">Rp.<?=number_format(($nTotalDiskonPercen)) ?></td>
														<td style="font-weight: 900" align="right">Rp. <?=number_format($nTotalBayar)?></td>
														
													</tr>	
                                                    
													<tr>
														<td colspan="5" align="right" style="font-weight: 900">Kurang Bayar</td>
														<td style="font-weight: 900" align="right">Rp. <?=number_format(array_sum($total)) ?> - Rp. <?=number_format(($nTotalBayar+$nTotalDiskonTier+$nTotalDiskonPercen))?></td>
														<td colspan="2" style="font-weight: 900" align="left">Rp. <?=number_format(array_sum($total)-($nTotalBayar+$nTotalDiskonTier+$nTotalDiskonPercen))?></td>
													</tr>
                            
												</tfoot>
											</table>	
										</div>
										<div class="tab-pane" id="rejected" role="tabpanel">
											<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
												<thead>
													<tr>
														<th>No</th>
														<th>Tanggal</th>
														<!-- <th>Nama Seller</th> -->
														<th>Jumlah</th>
														<th>History Pembayaran</th>
														<th class="xhide">Aksi</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$total[] = 0;
													$no = 0;
													foreach ($arraccount_detail_sales_rejected as $key => $vaData2) {
														?>
														<tr>
															<td><?= ++$no ?></td>
															<td><?= $vaData2['account_detail_sales_date'] ?></td>
															<!-- <td><?= $vaData2['account_detail_sales_date'] ?></td> -->
															<td align="right">Rp. <?= number_format($total2[] = $vaData2['account_detail_sales_amount']) ?></td>
															<td class="xhide"><a href="<?= site_url().$url."/history_pembayaran/".$id."/".$vaData2['account_detail_id'] ?>" class="btn btn-xs btn-success"> <i class="fa fa-list"></i></a></td>
															<td class="xhide"><a href="<?= site_url().$url."/form_edit_allow/".$vaData2['account_detail_sales_id'] ?>" class="btn btn-xs btn-info"> <i class="fa fa-search"></i></a></td>
														</tr>
													<?php } ?>
												</tbody>
												<!-- <tfoot>
													<tr>
														<td colspan="2" align="right" style="font-weight: 900">Total Bayar</td>
														<td style="font-weight: 900" align="right">Rp. <?=number_format(array_sum($total2))?></td>
														
													</tr>	
													<tr>
														<td align="right" style="font-weight: 900">Kurang Bayar</td>
														<td style="font-weight: 900" align="right">Rp. <?=number_format(array_sum($total)) ?> - Rp. <?=number_format(array_sum($total2))?></td>
														<td style="font-weight: 900" align="right">Rp. <?=number_format(array_sum($total)-array_sum($total2))?></td>
													</tr>
												</tfoot> -->
											</table>	
										</div>
									</div>
								</div>
							</div> 
						<?php } ?>
						<script>
							function cek_diskon_disini(id){

								var getSalesId = id.split("-");

//window.alert(getSalesId[0]);
//$('#tb_diskon_'+getSalesId[0]).html("OK"+getSalesId[0]);

$.ajax({
	type: 'get',
	url: '<?= base_url() ?>Test/cek_diskon_disini_penjualan/'+id+'/<?=$arrsales_account['account_id']?>',
	beforeSend:function(){
		$('#tb_diskon').html("<div class='text-center'><img src='<?= base_url() ?>web/media/loading.gif' style='height:50px'></div>" );  
	},
	success: function(result) {
		$('#tb_diskon').html(result);
	}
});

}
</script>