<?php
defined('BASEPATH') or exit('No direct script access allowed');


class T_sales extends CI_Controller
{

	var $url_ = "T_sales";
	var $id_ = "sales_id";
	var $eng_ = "sales";
	var $ind_ = "Pendingan";
	var $ind2_ = "Penjualan";

	public function __construct()
	{
		parent::__construct();
		ob_start();
		// $this->load->library('m_pdf');
		$this->load->model('model');

		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function search_member($search = '')
	{
		$this->Model->search_member($search);
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_member()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/table_member', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_member_sales($kode)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		$datacontent['kode'] = $kode;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/table_member_sales', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_pending_product()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_ . ' Product';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_pending_product', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_pending_product_detail($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_pending_product_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_pending_seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_ . ' Seller';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_pending_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_pending_product2($product_id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$datacontent['product_id'] = $product_id;
		$data['content'] = $this->load->view($this->url_ . '/table_pending_product2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_member()
	{
		$datacontent['datatable'] = $this->Model->get_data_member();
	}

	public function get_data_member_sales($kode)
	{
		$datacontent['datatable'] = $this->Model->get_data_member_sales($kode);
	}

	public function get_data_pending_product()
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_product();
	}

	public function get_data_pending_product2($product_id)
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_product2($product_id);
	}

	public function get_data_pending_product_detail($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_product_detail($id);
	}

	public function get_data_pending_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_seller();
	}

	public function table_pending_seller_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrmember'] = $this->Model->get_member($id);
		$datacontent['arrpending_seller_detail'] = $this->Model->get_pending_seller_detail_newAFIF($id);
		$data['file'] = 'Data Pendingan Seller';
		$data['content'] = $this->load->view($this->url_ . '/table_pending_seller_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function detail_pending_seller_detail($id = '', $idproduk = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrpending_seller_detail'] = $this->Model->detail_pending_seller_detail($id, $idproduk);
		$data['file'] = 'Data Delivery Order';
		$data['content'] = $this->load->view($this->url_ . '/table_pending_seller_detail_all', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function detail_so_seller_detail($id = '', $idproduk = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrpending_seller_detail'] = $this->Model->detail_so_sales_seller_detail($id, $idproduk);
		$data['file'] = 'Data Sales Order ';
		$data['content'] = $this->load->view($this->url_ . '/table_so_seller_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['parameter'] 	= $parameter;
		$datacontent['id']		 	= $id;
		$datacontent['code'] 		= $this->generate_code();
		$datacontent['arrseller'] 	= $this->Model->get_seller();
		$data['file'] 				= $this->ind2_;
		$data['content'] 			= $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_pay_deposit($id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['id']		 	= $id;
		$datacontent['deposit'] 	= $this->Model->get_deposit($id);
		$datacontent['arraccount_detail'] 	= $this->Model->get_account_detail($id);
		$datacontent['arrsales_detail'] 	= $this->Model->get_sales_detail2($id);
		$datacontent['provinsis'] = $this->db->query("SELECT * FROM m_region WHERE REGION_ID LIKE '__00000000'")->result_array();
		$datacontent['deliveries'] = $this->db->query("SELECT * FROM m_delivery_instance")->result_array();
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view($this->url_ . '/form_pay_deposit', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_edit_allow($id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['id']		 	= $id;
		$datacontent['arraccount_detail_sales'] = $this->Model->get_account_detail_sales3($id);
		$arraccount_detail_sales_product = $this->Model->get_account_detail_sales_product($id);
		foreach ($arraccount_detail_sales_product as $index => $value) {
			$datacontent['arraccount_detail_sales_product'][$value['sales_detail_id']] = $value['account_detail_sales_product_allow'];
			//$datacontent['sales_id'] = $value['sales_id'];
		}
		$datacontent['arrsales_detail'] 	= $this->Model->get_sales_detail2($datacontent['arraccount_detail_sales']['sales_id']);
		$data['file'] 				= 'Data Boleh Kirim';
		$data['content'] 			= $this->load->view($this->url_ . '/form_edit_allow', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function history_pembayaran($table_detail_id = '', $id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['id']		 	= $id;
		$datacontent['table_detail_id']		 	= $table_detail_id;
		$datacontent['rows_history'] = $this->Model->get_history_pembayaran($id);
		$data['file'] 				= 'Data History Pembayaran';
		$data['content'] 			= $this->load->view($this->url_ . '/list_history_pembayaran', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		$datacontent['arrseller'] = $this->Model->get_seller($id);
		$datacontent['limit'] = $this->db->query("SELECT SUM(C.account_detail_sales_product_allow) as minim, C.product_id FROM t_account_detail_sales A LEFT JOIN t_account_detail B ON B.account_detail_real_id = A.account_detail_real_id JOIN t_account_detail_sales_product C ON C.account_detail_sales_id = A.account_detail_sales_id WHERE A.sales_id = '$id' AND B.coa_transaction_confirm = 1 GROUP BY C.product_id")->result_array();
		$datacontent['arrseller'] = $datacontent['arrseller'][0];
		$datacontent['arrproduct'] = $this->Model->get_produk();
		$datacontent['product_list'] = "";
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
			//$datacontent['product_list'] = $datacontent['product_list'].'<option value="'.$value['id_produk'].'">'.$value['nama_produk'].' ('.(($value['tipe']=="1")?"Barang Jadi":"Gudang").')</option>';
		}
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail($id);
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail_utang($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		$datacontent['arrseller'] = $this->Model->get_seller($id);
		$datacontent['limit'] = $this->db->query("SELECT SUM(C.account_detail_sales_product_allow) as minim, C.product_id FROM t_account_detail_sales A LEFT JOIN t_account_detail B ON B.account_detail_real_id = A.account_detail_real_id JOIN t_account_detail_sales_product C ON C.account_detail_sales_id = A.account_detail_sales_id WHERE A.sales_id = '$id' AND B.coa_transaction_confirm = 1 GROUP BY C.product_id")->result_array();
		$datacontent['arrseller'] = $datacontent['arrseller'][0];
		$datacontent['arrproduct'] = $this->Model->get_produk();
		$datacontent['product_list'] = "";
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
			//$datacontent['product_list'] = $datacontent['product_list'].'<option value="'.$value['id_produk'].'">'.$value['nama_produk'].' ('.(($value['tipe']=="1")?"Barang Jadi":"Gudang").')</option>';
		}
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail_utang', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		$datacontent['id'] = $id;
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		
		
		$datacontent['arrsales_account'] = $this->Model->get_acoount_member($id);


		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail2($id);
		$datacontent['arrsales_discount'] = $this->Model->get_sales_discount($id);
		$arrx = array();
		foreach ($datacontent['arrsales_discount'] as $indexchkdiskonproduct => $valuechkdiskonproduct) {
			if ($valuechkdiskonproduct['sales_discount_product'] == '1') {
				$arrx[] = $valuechkdiskonproduct['sales_discount_id'];
			}
		}
		$arrdiscount_product = join("', '", $arrx);
		$arrsales_discount_product = $this->Model->get_sales_discount_product($arrdiscount_product);
		foreach ($arrsales_discount_product as $indexsales_discount_product => $valuesales_discount_product) {
			$datacontent['arrsales_discount_product'][$valuesales_discount_product['sales_discount_id']][] = $valuesales_discount_product;
		}
		$datacontent['deposit'] 	= $this->Model->get_deposit($id);
		$datacontent['arraccount_detail_sales_success'] = $this->Model->get_account_detail_sales_success($id);
		$datacontent['arraccount_detail_sales_rejected'] = $this->Model->get_account_detail_sales_rejected($id);
		// print_r($datacontent['arraccount_detail_sales']);
		// die;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function generate_code()
	{

		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWSO-";
		$dbDate	=	$this->db->query("SELECT (MAX(RIGHT(sales_code, 5)) + 1) as JumlahTransaksi FROM t_sales WHERE LEFT(sales_date_create, 7) = '" . date('Y-m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 2;
			}
		} else {
			$nJumlahTransaksi = 1;
		}
		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0000' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '000' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 4) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 5) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}
		return $cKode;
	}

	public function simpan()
	{

		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "Tambah") {
				$data['sales_code'] = $this->generate_code();
				$data['user_id'] = $this->session->userdata('user_id');
				$data['sales_date_create'] = date('Y-m-d H:i:s');
				$data['sales_category_id'] = $_SESSION['sales_category_id'];

				if ($_SESSION['role_id'] == '7') $this->db->query("UPDATE member SET user_pic_id = '" . $_SESSION['user_id'] . "' WHERE kode = '" . $data['seller_id'] . "'");
				//if($data['sales_address'] == "") $data['sales_address'] = $this->Model->get_seller_address($data['seller_id']); 
				$this->Model->chk_account($data['seller_id']);

				$this->Model->insert_trascend($data);
				$exec = $this->Model->insert($data);
				$id = $this->Model->insert_id();
			}
		}
		redirect(site_url('T_sales/form_detail/' . $id));
	}

	public function simpan_utang()
	{
		if ($this->input->post('simpan')) {

			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$data['user_id'] = $this->session->userdata('user_id');
				$data['sales_date_create'] = date('Y-m-d H:i:s');
				$data['sales_category_id'] = $_SESSION['sales_category_id'];
				//if($data['sales_address'] == "") $data['sales_address'] = $this->Model->get_seller_address($data['seller_id']); 
				$this->Model->chk_account($data['seller_id']);
				$exec = $this->Model->insert($data);
				$id = $this->Model->insert_id();
			}
		}
		redirect(site_url('T_sales/form_detail_utang/' . $id));
	}

	public function simpan_pay_deposit()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$id = $_POST['id'];
			$account_detail_real_id = $_POST['account_detail_real_id'];
			$paid = str_replace(',', '', $_POST['paid']);
			$date = $_POST['date'];
			$data_eksped = ([
				'region_id' => $_POST['input2']['kecamatan'],
				'delivery_instance_id' => $_POST['input2']['ekspedisi'],
				'region_delivery_instance_cost' => ''
			]);
			$this->db->insert('m_region_delivery_instance', $data_eksped);
			$data_eksped['region_delivery_instance_id'] = $this->db->insert_id();
			if ($_POST['schema_id'] == 1) {
				$allow = str_replace(',', '', $this->input->post('allow'));
				$max = $this->input->post('max');
				$product_max = $this->input->post('product_max');
				$sales_detail_id = $this->input->post('sales_detail_id');
				$product_id = $this->input->post('product_id');
			}
			$data_detail_sales['sales_id'] = $id;
			$data_detail_sales['account_detail_target_real_id'] = $account_detail_real_id;
			$data_detail_sales['account_detail_sales_amount'] = $paid;
			$data_detail_sales['account_detail_sales_date'] = $date;
			//$data_detail_sales['account_detail_id'] = $date;
			$this->Model->insert_account_detail_sales($data_detail_sales);
			$detail_sales_id = $this->Model->insert_id();
			if ($_POST['schema_id'] == 1) {
				foreach ($allow as $index => $value) {
					//if($value > 0){
					$data_detail_sales_product['sales_id'] = $id;
					$data_detail_sales_product['product_id'] = $product_id[$index];
					$data_detail_sales_product['sales_detail_id'] = $sales_detail_id[$index];
					if ($max == $paid) $data_detail_sales_product['account_detail_sales_product_allow'] = $product_max[$index];
					else $data_detail_sales_product['account_detail_sales_product_allow'] = $value;
					$data_detail_sales_product['account_detail_sales_id'] = $detail_sales_id;
					$data_detail_sales_product['region_id'] = $_POST['input2']['kecamatan'];
					$data_detail_sales_product['region_delivery_instance_id'] = $data_eksped['region_delivery_instance_id'];
					$data_detail_sales_product['delivery_instance_id'] = $_POST['input2']['ekspedisi'];
					$data_detail_sales_product['account_detail_sales_product_address'] = $_POST['input2']['full_address'];
					$data_detail_sales_product['account_detail_sales_product_delivery_cost'] = '';
					$this->Model->insert_account_detail_sales_product($data_detail_sales_product);
					$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $data_detail_sales_product[account_detail_sales_product_allow] WHERE product_id = $data_detail_sales_product[product_id] AND sales_detail_id = $data_detail_sales_product[sales_detail_id]");
					//}
				}
			}
			$this->Model->update_deposit($id, $paid);
			$this->Model->insert_coa_deposit($id, $account_detail_real_id, $paid);
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_sales/table_detail/' . $id));
	}

	public function simpan_edit_allow()
	{
		if ($this->input->post('simpan')) {
			$sales_id = $_POST['sales_id'];
			$id = $_POST['id'];
			$allow = $this->input->post('allow');
			foreach ($allow as $index => $value) {
				$data['account_detail_sales_product_allow'] = $value;
				$this->Model->update_account_detail_sales_product($data, ['account_detail_sales_id' => $id, 'sales_id' => $sales_id, 'sales_detail_id' => $index]);
			}
		}
		redirect(site_url('T_sales/table_detail/' . $sales_id));
	}

	public function simpan_detail()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			//$data = $this->input->post('input');
			$data2 = $this->input->post('input2');
			$id = $_POST['id'];
			$pending = $this->Model->reset_detail($_POST['id']);
			$kd_pd = '';
			$nomer = 0;
			foreach ($data2 as $index => $value) {
				$value['sales_detail_price'] = str_replace(',', '', $value['sales_detail_price']);
				$value['sales_detail_quantity'] = str_replace(',', '', $value['sales_detail_quantity']);
				$data = $value;
				$data['sales_id'] = $_POST['id'];
				$data['user_id'] = $this->session->userdata('user_id');
				$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
				if (@$pending[$value['product_id']]['sales_detail_id'] != "") {
					if ($pending[$value['product_id']]['sales_detail_quantity_send'] < $value['sales_detail_quantity']) {
						$exec = $this->Model->update_detail_quantity($pending[$value['product_id']]['sales_detail_id'], $value['sales_detail_quantity']);
					}
					$dataxx['sales_detail_price'] = $value['sales_detail_price'];
					$this->Model->update_detail2($dataxx, ['sales_detail_id' => $pending[$value['product_id']]['sales_detail_id']]);
				} else {
					$arrprod = $this->db->query("SELECT * FROM produk WHERE id_produk = '" . $data['product_id'] . "'")->row_array();
					$this->Model->insert_detail_trascend($data);
					$ecc = $this->Model->insert_detail($data);
					$arrdetail_kode[$arrprod['kd_pd']][$ecc] = $ecc;
					if ($nomer > 0) $kd_pd .= ", '" . $arrprod['kd_pd'] . "'";
					else $kd_pd .= "'" . $arrprod['kd_pd'] . "'";
					$nomer++;
				}
			}
			$total_price = $this->Model->get_sales_detail_total_price($id);
			$chk_account_detail = $this->Model->get_account_detail($id);
			if ($chk_account_detail['account_id'] != "") {
				$arrsales = $this->Model->get_sales($id);
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();

				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account($account_id)->row_array();
				$type = "debit";
				if (@$row['account_date_reset'] > $chk_account_detail['account_detail_date']) {
					$this->Model->reset_balance($account_id, (int)$chk_account_detail['account_detail_' . $type], $type, @$row['account_type_id'], $chk_account_detail['account_detail_date']);
				} else {
					$this->Model->reset_balance($account_id, (int)$chk_account_detail['account_detail_' . $type], $type);
				}
				//$this->Model->delete_account_detail($chk_account_detail['account_detail_real_id']);

				//$datax['account_id'] = $account_id;
				//$datax['account_detail_id'] = $id2;
				//$datax['account_detail_type_id'] = 3;
				//$datax['account_detail_user_id'] = $_SESSION['user_id'];
				//$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrsales['sales_date'];
				//$datax['account_detail_category_id'] = '81';
				//$datax['account_detail_realization'] = 1;
				//$datax['account_detail_quantity'] = ;
				//$datax['sales_id'] = $id;
				$datax['account_detail_price'] = $total_price;
				//$datax['account_detail_note'] = 'Pembelian dengan sales order : '.$arrsales['sales_code'];
				//$type = "debit";
				$datax['account_detail_debit'] = $total_price;
				//$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
				$id_account_detail = $this->Model->update_account_detail($datax, ['account_detail_real_id' => $chk_account_detail['account_detail_real_id']]);

				if (@$row['account_date_reset'] > $arrsales['sales_date']) {
					$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $total_price, $type);
				}
				//$data['account_detail_real_id'] = $id_account_detail;
			} else {
				$arrsales = $this->Model->get_sales($id);
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrsales['sales_date'];
				$datax['account_detail_category_id'] = '81';
				$datax['account_detail_realization'] = 1;
				//$datax['account_detail_quantity'] = ;
				$datax['sales_id'] = $id;
				$datax['account_detail_price'] = $total_price;
				$datax['account_detail_note'] = 'Pembelian dengan sales order : ' . $arrsales['sales_code'];
				$type = "debit";
				$datax['account_detail_debit'] = $total_price;
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');
				$id_account_detail = $this->Model->insert_account_detail($datax);
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account($account_id)->row_array();
				if (@$row['account_date_reset'] > $arrsales['sales_date']) {
					$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $total_price, $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			}
		}
		if (isset($_POST['discount_id'])) {
			$discounts = array();
			foreach ($_POST['discount_id'] as $discount_id) {
				$discounts[$discount_id] = $discount_id;
			}
			foreach ($discounts as $discount_id) {
				$discount = $this->db->query("SELECT sum(B.discount_reward_product_deduction_cost) as discount_reward_product_deduction_cost, SUM(B.discount_reward_product_percentage) as discount_reward_product_percentage, SUM(B.discount_reward_product_quantity) as discount_reward_product_quantity FROM m_discount_reward A JOIN m_discount_reward_product B ON A.discount_reward_id = B.discount_reward_id WHERE A.discount_id = $discount_id GROUP BY A.discount_id")->row_array();
				$total_price -= $discount['discount_reward_product_deduction_cost'];
				$total_price -= ($discount['discount_reward_product_percentage'] / 100 * $total_price);
				$salesDisc = ([
					'sales_id' => $id,
					'discount_id' => $discount_id,
					'sales_discount_date' => $arrsales['sales_date'],
					'sales_discount_create_date' => date('Y-m-d H:i:s'),
					'sales_discount_create_user_id' => $_SESSION['user_id'],
					'sales_discount_percentage_active' => 0,
					'sales_discount_price' => $discount['discount_reward_product_deduction_cost'],
					'sales_discount_percentage' => $discount['discount_reward_product_percentage'],
					'sales_discount_price_total_accumulated' => 0,
					'sales_discount_product_price_target' => $total_price,
				]);
				if ($discount['discount_reward_product_quantity'] > 0 && $discount['discount_reward_product_quantity'] !== '') {
					$salesDisc['sales_discount_product'] = 1;
				} else {
					$salesDisc['sales_discount_product'] = 0;
				}
				$this->db->insert('t_sales_discount', $salesDisc);
				$salesDisc['sales_discount_id'] = $this->db->insert_id();
				if ($discount['discount_reward_product_quantity'] > 0 && $discount['discount_reward_product_quantity'] !== '') {
					$discountProds = $this->db->query("SELECT * FROM m_discount_reward A JOIN m_discount_reward_product B ON A.discount_reward_id = B.discount_reward_id AND B.product_kd IS NOT NULL WHERE A.discount_id = $discount_id")->result_array();
					foreach ($discountProds as $discountProd) {
						$produk = $this->db->get_where('produk', ['kd_pd' => $discountProd['product_kd']])->row_array();
						$price = $this->db->query("SELECT * FROM m_price WHERE produk_global_kd = '$discountProd[product_kd]' ORDER BY price_unit_price ASC LIMIT 1")->row_array();
						$sales_detail = ([
							'product_id' => $produk['id_produk'],
							'sales_detail_quantity' => $_POST['promo_product'][$discountProd['product_kd']],
							'sales_detail_price' => 0,
							'sales_id' => $_POST['id'],
							'user_id' => $_SESSION['user_id'],
							'sales_detail_date_create' => date('Y-m-d H:i:s'),
							'sales_detail_discount' => 1,
						]);

						$this->Model->insert_detail_trascend($sales_detail);
						$this->Model->insert_detail($sales_detail);
						$sales_detail['sales_detail_id'] = $this->db->insert_id();
						$salesDiscProd = ([
							'product_kd' => $discountProd['product_kd'],
							'sales_discount_product_quantity' => $discountProd['discount_reward_product_quantity'],
							'sales_discount_product_accumulated' => 0,
							'sales_discount_product_target' => $_POST['promo_product'][$discountProd['product_kd']],
							'sales_discount_id' => $salesDisc['sales_discount_id'],
							'discount_reward_product_id' => $discountProd['discount_reward_product_id'],
							'sales_discount_product_draft' => 0,
							'sales_detail_id' => $sales_detail['sales_detail_id'],
							'sales_discount_product_price' => $price['price_unit_price'],
						]);
						$this->db->insert('t_sales_discount_product', $salesDiscProd);
					}
				}
				$discTerms = $this->db->query("SELECT * FROM m_discount_terms A JOIN m_discount_terms_product B ON A.discount_terms_id = B.discount_terms_id WHERE A.discount_id = $discount_id AND B.product_kd IN ($kd_pd)")->result_array();
				foreach ($discTerms as $discTerm) {
					$salesDiscTerm = ([
						'sales_discount_id' => $salesDisc['sales_discount_id'],
						'product_kd' => $discTerm['product_kd'],
						'sales_discount_terms_quantity' => $discTerm['discount_terms_product_quantity'],
						'discount_terms_product_id' => $discTerm['discount_terms_product_id'],
						'sales_discount_terms_type' => $discTerm['discount_terms_product_type'],
						'sales_discount_terms_price' => $discTerm['discount_terms_product_price'],
					]);
					$this->db->insert('t_sales_discount_terms', $salesDiscTerm);
					$sales_discount_terms_id = $this->db->insert_id();
					foreach ($arrdetail_kode[$discTerm['product_kd']] as $index_terms => $value_terms) {
						$data = ([
							'sales_discount_id' => $salesDisc['sales_discount_id'],
							'sales_detail_id' => $value_terms,
							'sales_discount_terms_id' => $sales_discount_terms_id,
							'sales_id' => $_POST['id'],
						]);
						$this->db->insert('t_sales_discount_detail', $data);
					}
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_sales/table_detail/' . $id));
	}

	public function simpan_detail_utang()
	{
		if ($this->input->post('simpan')) {
			print_r($_POST);
			die;
			//$data = $this->input->post('input');
			$data2 = $this->input->post('input2');
			$id = $_POST['id'];
			$pending = $this->Model->reset_detail($_POST['id']);
			foreach ($data2 as $index => $value) {
				$value['sales_detail_price'] = str_replace(',', '', $value['sales_detail_price']);
				$value['sales_detail_quantity'] = str_replace(',', '', $value['sales_detail_quantity']);
				$data = $value;
				$data['sales_id'] = $_POST['id'];
				$data['user_id'] = $this->session->userdata('user_id');
				$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
				if (@$pending[$value['product_id']]['sales_detail_id'] != "") {
					if ($pending[$value['product_id']]['sales_detail_quantity_send'] < $value['sales_detail_quantity']) {
						$exec = $this->Model->update_detail_quantity($pending[$value['product_id']]['sales_detail_id'], $value['sales_detail_quantity']);
					}
					$dataxx['sales_detail_price'] = $value['sales_detail_price'];
					$this->Model->update_detail2($dataxx, ['sales_detail_id' => $pending[$value['product_id']]['sales_detail_id']]);
				} else {
					$exec = $this->Model->insert_detail($data);
				}
			}
			$total_price = $this->Model->get_sales_detail_total_price($id);
			$chk_account_detail = $this->Model->get_account_detail($id);
			if ($chk_account_detail['account_id'] != "") {
				$arrsales = $this->Model->get_sales($id);
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();

				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account($account_id)->row_array();
				$type = "debit";
				if (@$row['account_date_reset'] > $chk_account_detail['account_detail_date']) {
					$this->Model->reset_balance($account_id, (int)$chk_account_detail['account_detail_' . $type], $type, @$row['account_type_id'], $chk_account_detail['account_detail_date']);
				} else {
					$this->Model->reset_balance($account_id, (int)$chk_account_detail['account_detail_' . $type], $type);
				}
				//$this->Model->delete_account_detail($chk_account_detail['account_detail_real_id']);

				//$datax['account_id'] = $account_id;
				//$datax['account_detail_id'] = $id2;
				//$datax['account_detail_type_id'] = 3;
				//$datax['account_detail_user_id'] = $_SESSION['user_id'];
				//$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrsales['sales_date'];
				//$datax['account_detail_category_id'] = '81';
				//$datax['account_detail_realization'] = 1;
				//$datax['account_detail_quantity'] = ;
				//$datax['sales_id'] = $id;
				$datax['account_detail_price'] = $total_price;
				//$datax['account_detail_note'] = 'Pembelian dengan sales order : '.$arrsales['sales_code'];
				//$type = "debit";
				$datax['account_detail_debit'] = $total_price;
				//$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
				$id_account_detail = $this->Model->update_account_detail($datax, ['account_detail_real_id' => $chk_account_detail['account_detail_real_id']]);

				if (@$row['account_date_reset'] > $arrsales['sales_date']) {
					$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $total_price, $type);
				}
				//$data['account_detail_real_id'] = $id_account_detail;
			} else {
				$arrsales = $this->Model->get_sales($id);
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrsales['sales_date'];
				$datax['account_detail_category_id'] = '81';
				$datax['account_detail_realization'] = 1;
				//$datax['account_detail_quantity'] = ;
				$datax['sales_id'] = $id;
				$datax['account_detail_price'] = $total_price;
				$datax['account_detail_note'] = 'Pembelian dengan sales order : ' . $arrsales['sales_code'];
				$type = "debit";
				$datax['account_detail_debit'] = $total_price;
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');
				$id_account_detail = $this->Model->insert_account_detail($datax);
				$dataxx = ([
					'account_detail_real_id' => $id_account_detail,
					'sales_id' => $id,
					'account_detail_target_real_id' => $id_account_detail,
					'account_detail_sales_amount' => $total_price,
					'account_detail_sales_date' => date('Y-m-d'),
					'account_detail_id' => $id2,
				]);
				$id_account_detail_sales = $this->Model->insert_account_detail_sales($dataxx);
				foreach ($_POST['input2'] as $input2) {
					$dataxxx = ([
						'account_detail_real_id' => $id_account_detail,
						'sales_id' => $id,
						'product_id' => $input2['product_id'],
						'account_detail_sales_product_allow' => str_replace(",", "", $input2['sales_detail_quantity']),
						'account_detail_sales_id' => $id,
						'sales_detail_id' => $id_account_detail_sales,
						'account_detail_id' => $id_account_detail,
					]);
					$this->db->insert('t_account_detail_sales_product', $dataxxx);
				}
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account($account_id)->row_array();
				if (@$row['account_date_reset'] > $arrsales['sales_date']) {
					$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $total_price, $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			}
		}
		redirect(site_url('T_sales/table_detail/' . $id));
	}

	/*public function simpan_detail()
	{
		if ($this->input->post('simpan')) {		
			$data = $this->input->post('input');
			$data['sales_id'] = $_POST['id'];
			$data['user_id'] = $this->session->userdata('user_id');
			$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
			$arrproduct = $this->Model->get_product($data['product_id']);			
			
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $_POST['tanggal'];
				$datax['account_detail_category_id'] = '81';
				$datax['account_detail_realization'] = 1;
				$datax['account_detail_quantity'] = $data['sales_detail_quantity'];
				$datax['account_detail_price'] = $data['sales_detail_price'];
				$datax['account_detail_note'] = 'Pembelian '.$arrproduct['nama_produk'].' Rp. '.$data['sales_detail_price'].' * '.$data['sales_detail_quantity'].' Pcs (Rp. '.$data['sales_detail_price']*$data['sales_detail_quantity'].')';
				$type = "debit";
				$datax['account_detail_debit'] = $data['sales_detail_price']*$data['sales_detail_quantity'];
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
				$id_account_detail = $this->Model->insert_account_detail($datax); 
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account()->row_array();
				if(@$row['account_date_reset'] > $_POST['tanggal']){
					$this->Model->update_balance($account_id,  $data['sales_detail_price']*$data['sales_detail_quantity'], $type, @$row['account_type_id'], $datax['account_detail_date']);
				}else{
					$this->Model->update_balance($account_id,  $data['sales_detail_price']*$data['sales_detail_quantity'], $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			
			$exec = $this->Model->insert_detail($data);
		}
		redirect(site_url('T_sales/form_detail/'.$_POST['id']));
	}*/

	public function simpan_detail_all()
	{
		redirect(site_url('T_sales'));
	}

	public function report()
	{
		if (@$_POST['export_pdf']) {
			$this->export_pdf();
		} else {
			$datacontent['url'] = $this->url_;
			$datacontent['search'] = $this->input->post('search');
			$datacontent['url2'] = '/' . $datacontent['search']['member_status'] . '|' . $datacontent['search']['all'];
			$datacontent['title'] = 'Member';
			$data['file'] = $this->ind_;
			$data['content'] = $this->load->view($this->url_ . '/table_report', $datacontent, TRUE);
			$data['title'] = $datacontent['title'];
			$this->load->view('Layout/home', $data);
		}
	}

	public function export_pdf()
	{
		ob_start();
		$datacontent['data'] = $this->Model->get_report()->result_array();
		$this->load->view('M_member/export_pdf', $datacontent);
		//$data['content'] = $this->load->view('m_member/table_report', $datacontent);
		//$data['siswa'] = $this->siswa_model->view_row();
		//$this->load->view('print', $data);
		$html = ob_get_contents();
		ob_end_clean();

		require './assets/html2pdf/autoload.php';

		$pdf = new Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
		$pdf->WriteHTML($html);
		$pdf->Output(date('Y-m-d') . '.pdf', 'D');
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function get_data_report($search = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_report($search);
	}

	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}

	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}

	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}



	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {

				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_credit'] = $from['account_detail_debit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_credit'], 'credit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}

	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_send()
	{
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}

	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}

	public function delete_pr($id = '')
	{
		$this->db->trans_begin();
		$arrdata = $this->db->query("SELECT * FROM (SELECT A.sales_id, C.account_id, IFNULL(SUM(B.account_detail_sales_amount), 0) as tot_bayar, C.account_detail_real_id, IFNULL(C.account_detail_debit, 0) as account_detail_debit, A.sales_type FROM t_sales A LEFT JOIN t_account_detail_sales B ON B.sales_id = A.sales_id LEFT JOIN t_account_detail C ON C.sales_id = A.sales_id GROUP BY A.sales_id) X WHERE X.tot_bayar = 0 AND X.sales_type = 0 AND X.sales_id = '$id'")->row_array();
		if (@$arrdata['account_id'] != "") {
			$this->db->query("UPDATE m_account SET account_debit = account_debit - " . $arrdata['account_detail_debit'] . " WHERE account_id = '" . $arrdata['account_id'] . "'");
			$this->db->query("DELETE FROM t_account_detail WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_detail WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_discount WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_discount_detail WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_discount_product WHERE sales_discount_id IN(SELECT sales_discount_id FROM t_sales_discount_detail WHERE sales_id = '" . $arrdata['sales_id'] . "')");
			$this->db->query("DELETE FROM t_sales_discount_terms WHERE sales_discount_id IN(SELECT sales_discount_id FROM t_sales_discount_detail WHERE sales_id = '" . $arrdata['sales_id'] . "')");
		} else if (@$arrdata['sales_id'] != "") {
			$this->db->query("DELETE FROM t_sales WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_detail WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_discount WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_discount_detail WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_discount_product WHERE sales_discount_id IN(SELECT sales_discount_id FROM t_sales_discount_detail WHERE sales_id = '" . $arrdata['sales_id'] . "')");
			$this->db->query("DELETE FROM t_sales_discount_terms WHERE sales_discount_id IN(SELECT sales_discount_id FROM t_sales_discount_detail WHERE sales_id = '" . $arrdata['sales_id'] . "')");
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect($this->url_);
	}

	public function delete($id = '')
	{
		$this->db->trans_begin();
		$arrdata = $this->db->query("SELECT * FROM (SELECT A.sales_id, C.account_id, IFNULL(SUM(B.account_detail_sales_amount), 0) as tot_bayar, C.account_detail_real_id, IFNULL(C.account_detail_debit, 0) as account_detail_debit, A.sales_type FROM t_sales A LEFT JOIN t_account_detail_sales B ON B.sales_id = A.sales_id LEFT JOIN t_account_detail C ON C.sales_id = A.sales_id GROUP BY A.sales_id) X WHERE X.tot_bayar = 0 AND X.sales_type = 0 AND X.sales_id = '$id'")->row_array();
		if (@$arrdata['account_id'] != "") {
			$this->db->query("UPDATE m_account SET account_debit = account_debit - " . $arrdata['account_detail_debit'] . " WHERE account_id = '" . $arrdata['account_id'] . "'");
			$this->db->query("DELETE FROM t_account_detail WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_detail WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_discount WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_discount_detail WHERE sales_id = '" . $arrdata['sales_id'] . "'");
			$this->db->query("DELETE FROM t_sales_discount_product WHERE sales_discount_id IN(SELECT sales_discount_id FROM t_sales_discount_detail WHERE sales_id = '" . $arrdata['sales_id'] . "')");
			$this->db->query("DELETE FROM t_sales_discount_terms WHERE sales_discount_id IN(SELECT sales_discount_id FROM t_sales_discount_detail WHERE sales_id = '')");
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect($this->url_);
	}

	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();


		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();

		if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
		} else {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
		}



		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_ . "/$id"));
	}

	public function list_kecamatan()
	{
		$id_kota = $this->input->post('id_kota');
		$stasiun = $this->Model->get_list_kecamatan($id_kota);
		$lists = "<option value=''>Pilih Kecamatan</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kecamatan . "'>" . $data->nama_kecamatan . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function list_kota()
	{
		$id_provinsi = $this->input->post('id_provinsi');
		$stasiun = $this->Model->get_list_kota($id_provinsi);
		$lists = "<option value=''>Pilih Kota</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kota . "'>" . $data->nama_kota . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function cetak_invoice($Aksi = "")
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['aksi'] = $Aksi;
		$data['title'] = $datacontent['title'];
		$data['arraccount_detail_sales'] = $this->Model->get_account_detail_sales2($Aksi);
		$this->load->view($this->url_ . '/inv-msglow', $data);
	}

	public function cetak_invoice_d($Aksi = "")
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$query = $this->model->code("SELECT * FROM t_sales WHERE sales_id = '" . $Aksi . "'");
		foreach ($query as $key => $vaData) {
			$kodeSales = $vaData['sales_code'];
			$seller_id = $vaData['seller_id'];
		}
		$data['arraccount_detail_sales'] = $this->Model->get_account_detail_sales2($kodeSales);
		$data['aksi'] = $kodeSales;
		$data['title'] = $datacontent['title'];
		$data['member'] = $this->db->query("SELECT * FROM member WHERE kode = '$seller_id'")->row();
		$this->load->view($this->url_ . '/inv-msglow', $data);
	}

	public function get_harga()
	{
		$data = array();
		foreach ($_POST['index'] as $indexa => $valuea) {

			$xid = $_POST['produk'][$indexa];
			$arrdat_group = $this->db->query("SELECT C.* FROM produk A JOIN produk_global B ON B.kode = A.kd_pd JOIN m_product_global_group C ON C.product_global_group_id = B.product_global_group_id WHERE A.id_produk = '$xid'")->row_array();
			$produk_group[$indexa] = $arrdat_group['product_global_group_id'];
			if (@$qgroup[$arrdat_group['product_global_group_id']] == "") {
				$qgroup[$arrdat_group['product_global_group_id']] = str_replace(",", "", $_POST['quantity'][$indexa]);
			} else {
				$qgroup[$arrdat_group['product_global_group_id']] += str_replace(",", "", $_POST['quantity'][$indexa]);
			}
		}
		foreach ($_POST['index'] as $i => $index) {
			$id = $_POST['produk'][$i];
			$schema_type = $_POST['schema_type'][$i];
			$schema_id = $_POST['schema_id'][$i];
			$qtt = str_replace(",", "", $_POST['quantity'][$i]);
			if ($_SESSION['role_id'] == 12) {
				$produks = $this->db->query("SELECT C.* FROM produk A JOIN produk_global B ON A.kd_pd = B.kode JOIN m_price C ON C.produk_global_id = B.id WHERE A.id_produk = '$id' AND C.price_active = 1 GROUP BY C.price_unit_price ORDER BY C.price_quantity DESC")->result_array();
			} else {
				if ($schema_type == 1) {
					$produks = $this->db->query("SELECT C.* FROM produk A JOIN produk_global B ON A.kd_pd = B.kode JOIN m_price C ON C.produk_global_id = B.id AND C.price_quantity <= '" . ($qgroup[$produk_group[$i]] + 1) . "' WHERE A.id_produk = '$id' AND C.price_active = 1 AND C.schema_id = '$schema_id'  ORDER BY C.price_quantity DESC")->result_array();
				} else if ($schema_type == 2) {
					$produks = $this->db->query("SELECT C.* FROM produk A JOIN produk_global B ON A.kd_pd = B.kode JOIN m_price C ON C.produk_global_id = B.id WHERE A.id_produk = '$id' AND C.price_active = 1 AND C.schema_id = '$schema_id' ORDER BY C.price_quantity DESC")->result_array();
				}
			}
			$data[$index] = '';
			foreach ($produks as $produk) {
				$data[$index] .= '<option value="' . $produk['price_unit_price'] . '">Rp ' . number_format($produk['price_unit_price']) . '</option>';
			}
			if ($produks !== null) $data[$index] .= '<option value="0">Rp 0</option>';
		}
		echo json_encode($data);
	}
}
