<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://msglow.app/upload/nitrogen.png" rel="icon" />
  <title>INVOICE MS GLOW</title>
  <meta name="author" content="harnishdesign.net">

  <!-- Web Fonts
======================= -->
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Stylesheet
======================= -->
  <link rel="stylesheet" type="text/css" href="https://demo.harnishdesign.net/html/koice/vendor/bootstrap/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="https://demo.harnishdesign.net/html/koice/vendor/font-awesome/css/all.min.css" />
  <link rel="stylesheet" type="text/css" href="https://demo.harnishdesign.net/html/koice/css/stylesheet.css" />
</head>

<body>
  <?php
  $arrbln = array(
    '01' => 'Januari',
    '02' => 'Februari',
    '03' => 'Maret',
    '04' => 'April',
    '05' => 'Mei',
    '06' => 'Juni',
    '07' => 'Juli',
    '08' => 'Agustus',
    '09' => 'September',
    '10' => 'Oktober',
    '11' => 'November',
    '12' => 'Desember'
  );
  $arrsales = $this->model->code("SELECT sales_address FROM t_sales WHERE sales_code = '" . $aksi . "'");
  foreach ($arrsales as $key => $vaIsian) {
    $alamatTagih = $vaIsian['sales_address'];
  }
  $query = $this->model->code("SELECT * FROM v_sales_inv WHERE sales_code = '" . $aksi . "'");
  foreach ($query as $key => $vaData) {
    $namaTagih = $vaData['nama'];
    $kodeTagih = $vaData['seller_id'];
    //$alamatTagih = $vaData['alamat'];
    $kotaTagih = $vaData['kota'];
    $invoceCode = $vaData['sales_code'];
    $salesDate = $vaData['sales_date'];
  }

  ?><div class="container-fluid invoice-container">
    <!-- Header -->
    <header>
      <div class="row align-items-center">
        <div class="col-sm-7 text-center text-sm-left mb-3 mb-sm-0"> <img id="logo" src="https://msglow.work/assets/logo.png" title="Koice" alt="Koice" /> </div>
        <div class="col-sm-5 text-center text-sm-right">
          <h4 class="mb-0">Invoice</h4>
          <p class="mb-0"><?= $invoceCode ?></p>
        </div>
      </div>
      <hr>
    </header>
    <!-- Main Content -->
    <main>
      <div class="row">
        <div class="col-sm-6 text-sm-right order-sm-1"> <strong>Pembayaran Ke:</strong>
          <address>
            PT. Kosmetika Cantik Indonesia<br />
            Jl. Komud ABD. Saleh No.58, Krajan, Asrikaton<br />
            Kec. Pakis, Malang, Jawa Timur
          </address>
        </div>
        <div class="col-sm-6 order-sm-0"> <strong>Di Tagihkan Untuk:</strong>
          <address>
            <?= $member->nama; ?> 
            <br><?= $member->kode; ?> <br>
            <?= $alamatTagih; ?>
          </address>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6"> <strong>Cara Pembayaran :</strong><br>
          <span> - </span> <br />
          <br />
        </div>
        <div class="col-sm-6 text-sm-right"> <strong>Tanggal Invoice:</strong><br>
          <span> <?= (int)substr($salesDate, 8, 2) . " " . $arrbln[substr($salesDate, 5, 2)] . " " . substr($salesDate, 0, 4) ?><br>
            <br>
          </span>
        </div>
      </div>
      <div class="card">
        <div class="card-header"> <span class="font-weight-600 text-4">Invoice Details</span> </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <td class="border-top-0" style="width:40%"><strong>Produk</strong></td>
                  <td class="text-center border-top-0"><strong>Qty</strong></td>
                  <td class="text-right border-top-0"><strong>Harga Satuan</strong></td>
                  <td class="text-right border-top-0"><strong>Sub Total</strong></td>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 0;
                $total[] = 0;
                $queryInv = $this->model->code("SELECT A.*, C.nama_produk FROM t_sales_detail A JOIN t_sales B ON B.sales_id = A.sales_id JOIN produk C ON A.product_id = C.id_produk WHERE B.sales_code = '" . $aksi . "' ");
                foreach ($queryInv as $key => $vaDetail) {

                ?>
                  <tr>
                    <td><?= $vaDetail['nama_produk'] ?></td>
                    <td class="text-center"><?= $vaDetail['sales_detail_quantity'] ?> Pcs</td>
                    <td class="text-right">Rp. <?= number_format($vaDetail['sales_detail_price']) ?></td>
                    <td class="text-right">Rp. <?= number_format($total[] = $vaDetail['sales_detail_price'] * $vaDetail['sales_detail_quantity']) ?></td>
                  </tr>
                <?php } ?>

                <tr>
                  <td colspan="3" class="bg-light-2 text-right"><strong>Sub Total</strong></td>
                  <td class="bg-light-2 text-right">Rp. <?= number_format(array_sum($total)) ?></td>
                </tr>
                <?php $total2 = 0;
                if (@count($arraccount_detail_sales) > 0) {
                  foreach ($arraccount_detail_sales as $index => $value) { ?>
                    <tr>
                      <td colspan="3" class="text-right"><?= (int)substr($value['account_detail_sales_date'], 8, 2) . " " . $arrbln[substr($value['account_detail_sales_date'], 5, 2)] . " " . substr($value['account_detail_sales_date'], 0, 4) ?></td>
                      <td class="text-right">Rp. <?= number_format($value['account_detail_sales_amount']) ?></td>
                    </tr>

                <?php $total2 += $value['account_detail_sales_amount'];
                  }
                } ?>
                <!-- <tr>
                <td colspan="3" class="bg-light-2 text-right"><strong>Pajak</strong></td>
                <td class="bg-light-2 text-right">$0.00</td>
              </tr> -->
                <tr>
                  <td colspan="3" class="bg-light-2 text-right"><strong>Kurang Bayar</strong></td>
                  <td class="bg-light-2 text-right">Rp. <?= number_format(array_sum($total) - $total2) ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <br>
      <!-- <div class="table-responsive d-print-none">
      <table class="table table-bordered">
        <thead>
          <tr>
            <td class="text-center"><strong>Transaction Date</strong></td>
            <td class="text-center"><strong>Gateway</strong></td>
            <td class="text-center"><strong>Transaction ID</strong></td>
            <td class="text-center"><strong>Amount</strong></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-center">05/12/2019</td>
            <td class="text-center">Credit Card</td>
            <td class="text-center">3912912704</td>
            <td class="text-center">$93.10 USD</td>
          </tr>
        </tbody>
      </table>
    </div> -->
    </main>
    <!-- Footer -->
    <footer class="text-center">
      <p class="text-1"><strong>NOTE :</strong> This is computer generated receipt and does not require physical signature.</p>
      <div class="btn-group btn-group-sm d-print-none"> <a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none"><i class="fa fa-print"></i> Print</a> <a href="<?php echo site_url('T_sales/T_sales_download/download_pdf/'.$id) ?>" target="_blank" class="btn btn-light border text-black-50 shadow-none"><i class="fa fa-download"></i> Download</a> </div>
    </footer>
  </div>
  <!-- Back to My Account Link -->
  <p class="text-center d-print-none"><a href="<?= base_url() ?>T_sales">&laquo; Back to My Account</a></p>
</body>

</html>