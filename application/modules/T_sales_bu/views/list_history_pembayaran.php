<div class="row">
   <div class="col-lg-12 col-xl-12">
      <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <table>
                    <br>
                    <tr>
                        <td>Payment Status</td>
                        <!-- <td>&nbsp;&nbsp;:</td> -->
                        <!-- <td>&nbsp;&nbsp; </td> -->
                    </tr>
                    </table>
                    <br>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 0;
                        if (count($rows_history) > 0) {
                        foreach ($rows_history as $row) {
                            $dt = explode(" ", $row['account_detail_real_date_create']);
                            $date = $dt[0];
                            $time = $dt[1];
                    ?>
                    <tr>
                        <td><?= ++$no ?></td>
                        <td><?= $date ?></td>
                        <td><?= $time; ?></td>
                        <td><?= $row['account_detail_real_log_note'] ?></td>
                    </tr>
                    <?php } } ?>
                </tbody>
            </table>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <a href="<?= site_url($url."/table_detail/".$table_detail_id); ?>"><span type="reset" class="btn btn-secondary">Kembali</span></a>
            </div>
        </div>
      </div>
   </div>
</div>