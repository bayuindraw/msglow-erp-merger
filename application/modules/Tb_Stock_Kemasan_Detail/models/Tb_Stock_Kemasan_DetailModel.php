<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Tb_Stock_Kemasan_DetailModel extends CI_Model
{
	
	var $table_ = "m_account";
	var $table4_ = "terima_kemasan";
	var $table3_ = "tb_stock_kemasan_detail";
	var $table2_ = "t_account_detail";
	var $id_ = "account_id";
	var $id4_ = "id_terima_kemasan";
	var $id2_ = "account_detail_id";
	var $eng_ = "account";
	var $url_ = "Tb_Stock_Kemasan_Detail";
	
	function get()
	{
		$data = $this->db->get('tb_stock_kemasan_detail');
		return $data;
	}
	
	function update_balance($id, $balance, $type, $account_type = '', $monthly = '')
	{
		if($type == "credit"){
			if($monthly == ''){
				$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_credit = (".$this->eng_."_credit + $balance) WHERE ".$this->id_." = ".$id);
			}else{
				if($account_type == '1'){ 
					$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_debit = (".$this->eng_."_debit - ($balance)), ".$this->eng_."_monthly_debit = (".$this->eng_."_monthly_debit - ($balance)) WHERE ".$this->id_." = ".$id);
					$this->db->query("UPDATE ".$this->table_."_reset SET account_reset_debit = (account_reset_debit - ($balance)) WHERE account_reset_date > '$monthly'");
				}else{
					$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_credit = (".$this->eng_."_credit + $balance), ".$this->eng_."_monthly_credit = (".$this->eng_."_monthly_credit + $balance) WHERE ".$this->id_." = ".$id);
					$this->db->query("UPDATE ".$this->table_."_reset SET account_reset_credit = (account_reset_credit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}else if($type == "debit"){
			if($monthly == ''){
				$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_debit = (".$this->eng_."_debit + $balance) WHERE ".$this->id_." = ".$id);
			}else{
				if($account_type == '2'){
					$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_credit = (".$this->eng_."_credit - ($balance)), ".$this->eng_."_monthly_credit = (".$this->eng_."_monthly_credit - ($balance)) WHERE ".$this->id_." = ".$id);
					$this->db->query("UPDATE ".$this->table_."_reset SET account_reset_credit = (account_reset_credit - ($balance)) WHERE account_reset_date > '$monthly'");
				}else{
					$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_debit = (".$this->eng_."_debit + $balance), ".$this->eng_."_monthly_debit = (".$this->eng_."_monthly_debit + $balance) WHERE ".$this->id_." = ".$id);
					$this->db->query("UPDATE ".$this->table_."_reset SET account_reset_debit = (account_reset_debit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}
		return '';
	}
	
	function insert_detail($data = array())
	{
		$this->db->insert($this->table2_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
		return $this->db->insert_id();
	}
	
	function get_m_account()
	{
		$data = $this->db->get('m_account');
		return $data;
	}
	
	function get_max_id()
	{
		$data = $this->db->query("SELECT (IFNULL(MAX(".$this->id2_."), 0) + 1) as id FROM ".$this->table2_)->row_array();
		return $data['id'];
	}
	
	function get_terima_kemasan($id)
	{
		$data = $this->db->query("SELECT * FROM terima_kemasan A LEFT JOIN kemasan B ON B.id_kemasan = A.id_barang LEFT JOIN po_kemasan C ON C.kode_po = A.kode_po LEFT JOIN supplier D ON D.id_supplier = C.supplier LEFT JOIN m_account E ON E.vendor_id = C.supplier WHERE id_terima_kemasan = '$id'")->row_array();
		return $data;
	}
	
	function get_data()
	{
		$table = $this->table4_." A LEFT JOIN kemasan B ON B.id_kemasan = A.id_barang LEFT JOIN po_kemasan C ON C.kode_po = A.kode_po LEFT JOIN supplier D ON D.id_supplier = C.supplier";
		$id = $this->id4_;
		$field = array('tgl_terima', 'nama_supplier', 'nama_kemasan', 'jumlah', 'rusak');
		$url = $this->url_;
		$arrwhere[] = "invoice IS NULL";
		$action = '<a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				if($keyfield == "rusak"){
					$valuer[$keyfield] = ($valuer[$keyfield]=="")?0:$valuer[$keyfield];
				}
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
					  'recordsTotal' => (int)$jum_all['jum'],
					  'recordsFiltered' => (int)$jum_filter['jum'],
					  'data' => @$dataxy
					  );
		
		echo json_encode($data);
	}
	
	function insert_account_detail_product($data = array())
	{
		$this->db->insert('t_account_detail_product', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function insert($data = array())
	{
		$this->db->insert($this->table3_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function update_terima_kemasan($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($this->table4_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function delete($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($this->table3_);
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
}
