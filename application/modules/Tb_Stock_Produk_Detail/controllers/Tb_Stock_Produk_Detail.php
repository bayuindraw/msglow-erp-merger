<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Tb_Stock_Produk_Detail extends CI_Controller
{
	
	var $url_ = "Tb_Stock_Produk_Detail";
	var $id_  = "id_terima_kemasan";
	var $eng_ = "Terima Produk";
	var $ind_ = "Terima Produk";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_.'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}
	
	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	
	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['id'] = $id;
		if ($parameter == 'ubah' && $id != '') {
			$this->db->where('id_terima_kemasan', $id);
			$datacontent['row'] = $this->Model->get()->result_array();
		}
		$datacontent['data_terima_produk'] = $this->Model->get_terima_produk($id);
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer '.$this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function set_transfer()
	{
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				
				$from['account_detail_id'] = $id;
				$from['account_detail_debit'] = 0;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');
				
				if($this->input->post('transfer_fee_chk') == "on"){
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_id'] = $from['account_id'];
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($from['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
				
				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_debit'] = $from['account_detail_credit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2; 
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
			} else {
				$data[$this->eng_.'_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}
	
	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang '.$this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data); 
	}
	
	public function set_send()
	{
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');
				
				if($this->input->post('transfer_fee_chk') == "on"){
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_.'_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}
	
	public function form_detail($parameter = '', $id = '', $id_detail = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind2_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account_detail_category'] = $this->Model->get_list_account_detail_category();
		$datacontent['id'] = $id;
		$datacontent['id_detail'] = $id_detail;
		$data['file'] = $this->ind2_ ;
		$data['content'] = $this->load->view($this->url_.'/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$create = date('Y-m-d H:i:s');
				$exec = $this->Model->insert($data);
				$data2['factory_id'] = $this->db->insert_id();
				$data2['account_code'] = '72.'.$data2['factory_id'];
				$data2['account_name'] = 'Akun '.$data['nama_factory'].' (factory)';
				$data2['account_type_id'] = 1;
				$data2['account_date_create'] = $create;
				$data2['account_date_reset'] = $create;
				$exec = $this->Model->insert_account($data2);
			} else {
				$id = $_POST['id'];
				$data2 = $_POST['input2'][1];
				$jumlah = $data2['baik'] + $data2['rusak'];
				$awalbaik = $data2['baik'];
				$awalrusak = $data2['rusak'];
				$awaljumlah = $jumlah;
				$arrlistclassification = $this->Model->get_list_product_classification($_POST['id_barang']);
				foreach($arrlistclassification as $indexx => $valuex){
					$arrproduct_classification_detail_id[$valuex['product_classification_detail_id']] = $valuex['product_classification_detail_id'];
				}
				$classification_detail_id = join(', ', $arrproduct_classification_detail_id);
				$arrstockkemasandetail = $this->Model->get_list_tb_stock_kemasan_detail($classification_detail_id, $_POST['factory_id']);
				foreach($arrstockkemasandetail as $indexy => $valuexy){
					$arrstock_kemasan[$valuexy['product_classification_detail_id']][] = $valuexy;
					if(@$ing_id != $valuexy['ingredient_id']){
						$arrstock_factory[$valuexy['product_classification_detail_id']][] = array($valuexy['ingredient_id'] => $valuexy['factory_stock_amount']);
					}
					$ing_id = $valuexy['ingredient_id'];
				}
				$awal = 0;
				$i = 0;
				$rusaky = 0;
				$sisa = $jumlah;
				while($i < $jumlah) {
					$resetcomparasion = 0;
					foreach($arrproduct_classification_detail_id as $indexcid => $valuecid){
						if($awal == 0){
							$count[$indexcid] = 0;
							$count_factory[$indexcid] = 0;
						}
						if(@$arrstock_kemasan[$indexcid][$count[$indexcid]]['baik'] < @$arrstock_factory[$indexcid][$count_factory[$indexcid]][@$arrstock_kemasan[$indexcid][$count[$indexcid]]['ingredient_id']]){
							$stockkecil = @$arrstock_kemasan[$indexcid][$count[$indexcid]]['baik'];
						}else{
							$stockkecil = @$arrstock_factory[$indexcid][$count_factory[$indexcid]][@$arrstock_kemasan[$indexcid][$count[$indexcid]]['ingredient_id']];
						}
						if($resetcomparasion == 0){ 
							$resetcomparasion = @$stockkecil;
						}else if($resetcomparasion > @$stockkecil && @$stockkecil != ""){ 
							$resetcomparasion = @$stockkecil;
						}
					}
					if($sisa < $resetcomparasion){
						$resetcomparasion = $sisa;
					}
					$cek = 0;
					$hpp = 0;
					foreach($arrproduct_classification_detail_id as $indexcid => $valuecid){
						if(@$arrstock_kemasan[$indexcid][$count[$indexcid]]['baik'] > 0 && @$arrstock_factory[$indexcid][$count_factory[$indexcid]][@$arrstock_kemasan[$indexcid][$count[$indexcid]]['ingredient_id']] != 0){
							$hpp = $hpp + @$arrstock_kemasan[$indexcid][$count[$indexcid]]['hpp'];
							@$arrstock_kemasan[$indexcid][$count[$indexcid]]['baik'] = @$arrstock_kemasan[$indexcid][$count[$indexcid]]['baik'] - $resetcomparasion;
							@$arrstock_factory[$indexcid][$count_factory[$indexcid]][@$arrstock_kemasan[$indexcid][$count[$indexcid]]['ingredient_id']] = @$arrstock_factory[$indexcid][$count_factory[$indexcid]][@$arrstock_kemasan[$indexcid][$count[$indexcid]]['ingredient_id']] - $resetcomparasion;
							if(@$arrstock_factory[$indexcid][$count_factory[$indexcid]][@$arrstock_kemasan[$indexcid][$count[$indexcid]]['ingredient_id']] == 0){
								$count_factory[$indexcid]++;
								$ingredient_idx = @$arrstock_kemasan[$indexcid][$count[$indexcid]]['ingredient_id'];
								if(@$arrstock_kemasan[$indexcid][$count[$indexcid]]['baik'] == 0){ 
									$count[$indexcid]++;
								}
								if(@$arrstock_kemasan[$indexcid][$count[$indexcid]]['ingredient_id'] != ""){
									while(@$arrstock_kemasan[$indexcid][$count[$indexcid]]['ingredient_id'] != $ingredient_idx){
										$count[$indexcid]++;
									}
								}
							}else if(@$arrstock_kemasan[$indexcid][$count[$indexcid]]['baik'] == 0){
								$count[$indexcid]++;
							}
							$cek = 1;
						}
					}
					if($resetcomparasion != ""){
						$awaljumlah = $awaljumlah - $resetcomparasion;
						if($rusaky == 0){
							if($data2['baik'] < $resetcomparasion){
								$baik2 = $data2['baik'];
								$rusak2 = ($resetcomparasion - $data2['baik']);
								$rusaky = 1;
							}else if($data2['baik'] >= $resetcomparasion){
								$baik2 = $resetcomparasion;
								$rusak2 = 0;
								if(($data2['baik'] - $resetcomparasion) == 0){
									$rusaky = 1;
								}
							}
						}else{
							$baik2 = 0;
							$rusak2 = $resetcomparasion;
						}
						$valuey['hpp'] = $data2['hpp'] + $hpp;
						$valuey['baik'] = $baik2;
						$valuey['rusak'] = $rusak2;
						$valuey['tanggal'] = $_POST['tanggal']; 
						$valuey['id_barang'] = $_POST['id_barang']; 
						$valuey['id_terima_kemasan'] = $id; 
						$exec = $this->Model->insert($valuey);
					}
					if($cek == 0){
						$i = $jumlah;
					}else{
						$sisa = $sisa - $resetcomparasion;
						if($sisa == 0){
							$i = $jumlah;
						}
					}
					$awal++;
				}
				
				if($awaljumlah > 0){
					$resetcomparasion = $awaljumlah;
					if($rusaky == 0){
						if($data2['baik'] < $resetcomparasion){
							$baik2 = $data2['baik'];
							$rusak2 = ($resetcomparasion - $data2['baik']);
							$rusaky = 1;
						}else if($data2['baik'] >= $resetcomparasion){
							$baik2 = $resetcomparasion;
							$rusak2 = 0;
							if(($data2['baik'] - $resetcomparasion) == 0){
								$rusaky = 1;
							}
						}
					}else{
						$baik2 = 0;
						$rusak2 = $resetcomparasion;
					}
					$valuey['hpp'] = $data2['hpp'];
					$valuey['baik'] = $baik2;
					$valuey['rusak'] = $rusak2;
					$valuey['tanggal'] = $_POST['tanggal']; 
					$valuey['id_barang'] = $_POST['id_barang']; 
					$valuey['id_terima_kemasan'] = $id; 
					$exec = $this->Model->insert($valuey);
				}
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();
				$data['account_id'] = $account_id;
				$data['account_detail_id'] = $id2;
				$data['account_detail_type_id'] = 2;
				$data['account_detail_user_id'] = $_SESSION['user_id'];
				$data['account_detail_pic'] = $_SESSION['user_fullname'];
				$data['account_detail_date'] = $_POST['tanggal'];
				$data['account_detail_date'] = $_POST['tanggal'];
				$data['account_detail_category_id'] = '72';
				$data['account_detail_realization'] = 1;
				$data['account_detail_code'] = $_POST['invoice'];
				$type = "debit";
				$data['account_detail_debit'] = $_POST['total_tagihan'];
				$data['account_detail_date_create'] = date('Y-m-d H:i:s');  
				$id_account_detail = $this->Model->insert_detail($data); 
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account()->row_array();
				if(@$row['account_date_reset'] > $_POST['tanggal']){
					$this->Model->update_balance($account_id,  $_POST['total_tagihan'], $type, @$row['account_type_id'], $data['account_detail_date']);
				}else{
					$this->Model->update_balance($account_id,  $_POST['total_tagihan'], $type);
				}
				$this->Model->delete(['id_terima_kemasan' => $id]);
				//foreach($data2 as $inde2x => $value2x){
					$value2x = $data2;
					$value2x['tanggal'] = $_POST['tanggal']; 
					$value2x['id_barang'] = $_POST['id_barang']; 
					$value2x['id_terima_kemasan'] = $id; 
					//$exec = $this->Model->insert($value2);
					$valuex = $value2x;
					$valuex['account_id'] = $account_id; 
					$valuex['account_detail_real_id'] = $id_account_detail;
					$exec = $this->Model->insert_account_detail_product($valuex);
				//}
				$datax['hpp'] = $value2['hpp'];
				$datax['total_tagihan'] = $_POST['total_tagihan'];
				$datax['invoice'] = $_POST['invoice'];
				$this->Model->update_terima_produk($datax, [$this->id_ => $id]);
			}
		}

		redirect(site_url($this->url_));
	}
	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}
	
	public function simpan_detail()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$id = $this->input->post('id');
				$id2 = $this->Model->get_max_id();
				$data[$this->id_] = $id;
				$data[$this->id2_] = $id2;
				$data[$this->eng2_.'_type_id'] = 2;
				$data[$this->eng2_.'_user_id'] = $_SESSION['user_id'];
				if($_POST['realisasi'] == "on"){
					$data[$this->eng2_.'_realization'] = 1;
				}else{
					$data[$this->eng2_.'_realization'] = 0;
				}
				if($_POST['transaction_type'] == "debit"){
					$type = "debit";
					$data[$this->eng2_.'_debit'] = $_POST['transaction_amount'];
				}else if($_POST['transaction_type'] == "credit"){
					$type = "credit";
					$data[$this->eng2_.'_credit'] = $_POST['transaction_amount'];
				}
				$data[$this->eng2_.'_date_create'] = date('Y-m-d H:i:s');  
				$exec = $this->Model->insert_detail($data);
				$this->db->where('account_id', $id);
				$row = $this->Model->get()->row_array();
				if(@$row['account_date_reset'] > $data[$this->eng2_.'_date']){
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_.'_date']);
				}else{
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
				}
			} else {
				$id = $this->input->post('id');
				$data[$this->eng2_.'_user_id'] = $_SESSION['user_id'];
				if($_POST['realisasi'] == "on"){
					$data[$this->eng2_.'_realization'] = 1;
				}else{
					$data[$this->eng2_.'_realization'] = 0;
				}
				if($_POST['transaction_type'] == "debit"){
					$type = "debit";
					$data[$this->eng2_.'_debit'] = $_POST['transaction_amount'];
					$data[$this->eng2_.'_credit'] = 0;
				}else if($_POST['transaction_type'] == "credit"){
					$type = "credit";
					$data[$this->eng2_.'_credit'] = $_POST['transaction_amount'];
					$data[$this->eng2_.'_debit'] = 0;
				}
				$data[$this->eng2_.'_date_update'] = date('Y-m-d H:i:s'); 
				$this->Model->update_detail($data, [$this->id_ => $this->input->post('id'), $this->id2_ => $this->input->post('id_detail')]);
				$this->db->where('account_id', $id);
				$row = $this->Model->get()->row_array();
				if(@$row['account_date_reset'] > $this->input->post('transaction_date_last')){
					$this->Model->update_balance($id,  -1*($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'), @$row['account_type_id'], $this->input->post('transaction_date_last'));
				}else{ 
					$this->Model->update_balance($id,  -1*($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'));
				}
				if(@$row['account_date_reset'] > $data[$this->eng2_.'_date']){
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_.'_date']);
				}else{
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
				}
			}
		}

		redirect(site_url($this->url2_."/".$id));
	}
	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();
		
		
		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();
		
		if(@$row['account_date_reset'] > $row2[$this->eng2_.'_date']){
			$this->Model->update_balance($id, -1*(($row2['account_detail_debit'] > 0)?$row2['account_detail_debit']:$row2['account_detail_credit']), ($row2['account_detail_debit'] > 0)?'debit':'credit', @$row['account_type_id'], $row2[$this->eng2_.'_date']);
		}else{
			$this->Model->update_balance($id, -1*(($row2['account_detail_debit'] > 0)?$row2['account_detail_debit']:$row2['account_detail_credit']), ($row2['account_detail_debit'] > 0)?'debit':'credit');
		}
		
		
		
		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_."/$id"));
	}
}
