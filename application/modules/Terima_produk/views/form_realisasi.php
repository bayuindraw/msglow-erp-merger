<div class="row">
	<div class="col-lg-12 col-xl-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title; ?>
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_realisasi'); ?>" method="Post">
					<?= input_hidden('id_factory', $id_factory) ?>
					<?= input_hidden('sj', $sj) ?>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead> 
							<tr>
								<th>Produk</th>
								<th>Kode PO</th>
								<th>Tanggal</th>
								<th>Jumlah SJ</th>
								<th>Jumlah Realisasi</th>
								<th>Total Jumlah</th>
							</tr>
						</thead>
						<tbody id="x">
							<?php $status_simpan = 1; ?>
							<?php foreach($arrterima_produk as $index => $value){ ?>
								<tr>
									<td><?= $value['nama_produk'] ?></td>
									<td>
										<?php foreach($arrdetail_terima_produk[$index] as $index2 => $value2){ ?>
											<div style="height:39px;"><?= $value2['kode_po'] ?></div><hr>
										<?php } ?>
									</td>
									<td>
										<?php 
										$jum = 0;
										foreach($arrdetail_terima_produk[$index] as $index2 => $value2){ 
											$jum += $value2['jumlah'];
											?>
											<?php echo $value2['realisasi_date']; ?>
											<?php if(substr($value2['realisasi_date'],0,4)!="0000" || $value2['realisasi_date']!=""){ ?>
												<div style="height:39px;"><?= "masuk ".$value2['tgl_terima'] ?></div>
												<input type="hidden" name="id_barang[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['id_barang'] ?>">
												<input type="hidden" name="tgl[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['tgl_terima'] ?>"><hr>
											<?php } else { ?>
												<input type="hidden" name="id_barang[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['id_barang'] ?>">
												<input type="text" name="tgl[<?= $value2['id_terima_kemasan'] ?>]" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= $value2['tgl_terima'] ?>" required><hr>
											<?php } ?>
										<?php } ?>
									</td>
									<td>
										<?php foreach($arrdetail_terima_produk[$index] as $index2 => $value2){ ?>
											<div style="height:39px;"><?= $value2['jumlah_sj'] ?></div><hr>
										<?php } ?>
									</td>
									<td>
										<?php 
										$jum = 0;
										foreach($arrdetail_terima_produk[$index] as $index2 => $value2){
											$jum += $value2['jumlah'];
											?>
											<?php if(substr($value2['realisasi_date'],0,4)!="0000"){ ?>
												<?php $status_simpan = 0; ?>
												<div style="height:39px;"><?= $value2['jumlah'] ?></div><hr>
												<input type="hidden" id="cJumlah" name="jml[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['jumlah']; ?>">
											<?php } else { ?>
												<?php $status_simpan = 1; ?>
												<input type="text" id="cJumlah" name="jml[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['jumlah']; ?>" onkeyup="get_total(<?= $index ?>);" class="form-control static sum_total<?= $index ?>" autocomplete="off"><hr>
											<?php } ?>
											<input type="hidden" name="jml_asli[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['jumlah']; ?>">
											<input type="hidden" name="tgl_asli[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['tgl_terima'] ?>">
										<?php } ?>
									</td>
									<td id="<?= $index ?>"><?= $jum ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>  
					<!--<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th>Total Jumlah</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id="x"> 
								<?php if(@count(@$arrsales_detail) > 0){ 
									$i = 0;
									foreach($arrsales_detail as $indexx => $valuex){
									
								?>
									<tr id="<?= $i; ?>">
										
										<td><select name="input2[<?= $i; ?>][product_id]" class="PilihBarang form-control md-static" required><option></option>
								<?php
									foreach($arrproduct as $indexl => $valuel){
											echo '<option value="'.$valuel['id_produk'].'" '.(($valuex['product_id'] == $valuel['id_produk'])?'selected':'').'>'.$valuel['nama_produk'].' ('.$valuel['klasifikasi'].') (stock: '.$valuel['jumlah'].')</option>';
											//echo '<option value="'.$valuel['id_phl'].'" '.(($valuex['employee_id'] == $valuel['id_phl'])?'selected':'').'>'.$valuel['nama_phl'].'</option>';
								?>
								<?php	
										} ?>
									<td><input type="text" class="form-control" placeholder="Jumlah" name="input2[<?= $i ?>][sales_detail_quantity]" required value="<?= $valuex['sales_detail_quantity']; ?>"></td>
									<td><input type="text" class="form-control numeric" placeholder="Harga" name="input2[<?= $i ?>][sales_detail_price]" required value="<?= $valuex['sales_detail_price']; ?>"></td>
																				
										<td><button onclick="myDeleteFunction('<?= $i ?>')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									<?php $i++;
									}
									
								}else{ ?>
									<tr id="0">
										<td><select name="input2[0][product_id]" class="PilihBarang form-control md-static" required><option></option><?= $product_list ?></select></td>
										<td><input type="text" id="cJumlah" name="input2[0][sales_detail_quantity]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static"></td>
										<td><input type="text" id="cJumlah" name="input2[0][sales_detail_price]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static numeric"></td>										
										<td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									</tr> 
								<?php } ?>
                            </tbody>    
                        </table> -->

                        <?php if($status_simpan==1){ ?>
                        	<div class="kt-portlet__foot" style="margin-top: 20px;">
                        		<div class="kt-form__actions">
                        			<button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;">
                        				<i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
                        			</button>
                        		</div>
                        	</div>
                        <?php } ?>
                    </form>


                </div>
            </div>
        </div>
    </div>


    <script>
//it = <?= ((@count(@$arrsales_detail) > 0)?count(@$arrsales_detail):1); ?>;	
function get_total(id) {
	var total_price = 0;
	$('.sum_total'+id).each(function(){			
		val_temp = $(this).val();
		if(val_temp==""){
			val_temp = 0;
		}
		total_price += parseFloat(val_temp);
	});
	$('#'+id).html(total_price);
}

</script>