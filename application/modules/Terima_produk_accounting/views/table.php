							<div class="kt-portlet">
								<div class="kt-portlet__body  kt-portlet__body--fit">
									<div class="row row-no-padding row-col-separator-lg">
										<div class="col-md-12 col-lg-6 col-xl-6">

											<!--begin::Total Profit-->
											<div class="kt-widget24">
												<div class="kt-widget24__details">
													<div class="kt-widget24__info">
														<h4 class="kt-widget24__title">
															Total Sudah Terima
														</h4>
													</div>
													<span class="kt-widget24__stats kt-font-brand">
														<?= $sudah_terima ?>
													</span>
												</div>
											</div>

											<!--end::Total Profit-->
										</div>
										<div class="col-md-12 col-lg-6 col-xl-6">

											<!--begin::New Feedbacks-->
											<div class="kt-widget24">
												<div class="kt-widget24__details">
													<div class="kt-widget24__info">
														<h4 class="kt-widget24__title">
															Total Belum Terima
														</h4>
													</div>
													<span class="kt-widget24__stats kt-font-warning">
														<?= $belum_terima ?>
													</span>
												</div>
											</div>

											<!--end::New Feedbacks-->
										</div>
									</div>
								</div>
							</div>
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											<?= $title ?>
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama Pabrik</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div> 
							
<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
		"processing": true,
		"serverSide": true,
		"ajax": "<?= site_url() ?>/<?= $url ?>/get_data"
		});
	});
</script>