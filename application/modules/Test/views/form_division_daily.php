<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN BARANG KELUAR HARIAN
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body"> 

        <form id="main" class="form-horizontal" action="<?= str_replace('http://', 'https://', site_url($url . '/mpdf_division_daily')); ?>" method="Post" target="_blank">
          <div class="form-group">
            <label>Pilih Tanggal</label>
            <input type="text" id="cTanggal" name="cTanggal" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= $this->session->userdata('tanggal') ?>" placeholder="Pilih Tanggal Penjualan">
          </div>
		  <div class="form-group">
            <label>Klasifkasi</label>
			<select name="product_type" class="PilihKlasifikasi form-control md-static" required><option></option><?= $product_type_list ?></select>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " name="draft" value="draft" title="Cetak Laporan">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan Draft</span>
				</button> &nbsp; <button type="submit" class="btn btn-warning waves-effect waves-light " name="realisasi" value="realisasi" title="Cetak Laporan">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan Realisasi</span>
				</button>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>


</div>