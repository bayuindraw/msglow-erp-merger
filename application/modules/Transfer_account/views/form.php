<div class="row">
	<div class="col-lg-12 col-xl-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
				<input type="hidden" name="input[coa_level_1]" id="coa_level_1" />
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label class="col-2 col-form-label">Akun</label>
						<div class="col-4">
							<select name="input[akun]" class="form-control pilihAkun" id="selectLevel" onchange="select_level()">
								<?= $arrakun ?>
							</select>
						</div>
						<label class="col-2 col-form-label">Tanggal</label>
						<div class="col-4">
							<input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="input[tanggal]" readonly value="<?= date('Y-m-d') ?>">
						</div>
					</div>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Keterangan</th>
								<th>Akun</th>
								<th>Nominal</th>
								<th width="20%">Bukti</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="x">
							<?php
							$totalx = 0;
							$i = 0;
							?>
							<tr id="tr0">
								<td><input type="text" class="form-control" placeholder="keterangan" name="input2[0][keterangan]" required></td>
								<td><input type="hidden" name="input2[0][coa_level_2]" id="coaLevel0" /><select name="input2[0][akun_id]" class="pilihAkun form-control md-static" id="selectLevels0" onchange="select_level2(0)" required><?= $arrakun ?></select></td>
								<td><input type="number" class="form-control sum_total" placeholder="nominal" onkeyup="chk_total($(this).val()); " name="input2[0][nominal]" required></td>
								</td>
								<td><input type="file" class="form-control" placeholder="bukti" name="input2[0][bukti]"></td>
								<td><button onclick="myDeleteFunction(0);chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
							</tr>
							<?php $i++; ?>
						</tbody>
						<tbody>
							<tr>
								<td>Total Nominal : <span id="total"><?= $totalx ?></span></td>
								<td><input type="hidden" name="input[total]" id="total1" /></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<button type="submit" name="simpan" value="simpan" id="btn_submit" class="btn btn-primary" onsubmit="return confirm('Are you sure?')">Simpan</button>
							<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
						</div>
					</div>
			</form>
		</div>
	</div>
</div>

<script>
	var count = <?= $i; ?>;

	function chk_total(val) {
		var sum = 0;
		$('.sum_total').each(function() {
			val = this.value;
			if (this.value == "") val = 0;
			sum += parseFloat(val);
		});
		$('#total').html(sum);
		$('#total1').val(sum);
	}

	function select_level() {
		$('#coa_level_1').val($('#selectLevel option:selected').attr('level'));
	}

	function select_level2(id) {
		$('#coaLevel' + id + '').val($('#selectLevels' + id + ' option:selected').attr('level'));
	}

	function myCreateFunction() {
		count = count + 1;
		var html = '<tr id="tr' + count + '"><td><input type="text" class="form-control" placeholder="keterangan" name="input2[' + count + '][keterangan]" required></td><td><input type="hidden" name="input2[' + count + '][coa_level_2]" id="coaLevel' + count + '"/><select name="input2[' + count + '][akun_id]" class="pilihAkun' + count + ' form-control md-static" id="selectLevels' + count + '" onchange="select_level2(' + count + ')" required><?= $arrakun ?></select></td><td><input type="number" class="form-control sum_total" placeholder="nominal" onkeyup="chk_total($(this).val()); " name="input2[' + count + '][nominal]" required></td></td><td><input type="file" class="form-control" placeholder="bukti" name="input2[' + count + '][bukti]"></td><td><button onclick="myDeleteFunction(' + count + '); chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$('.pilihAkun' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
	}

	function myDeleteFunction(id) {
		$('#tr' + id).remove();
	}



	function hideReal() {
		var x = document.getElementById("real");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}

	function hideBiaya() {
		var x = document.getElementById("biaya_admin");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
</script>