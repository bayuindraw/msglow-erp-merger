<?php
function convertDate($tgl){
    $cDate = explode("-", $tgl);

    $nTahun = $cDate[0];
    $nBulan = $cDate[1];
    $nHari  = $cDate[2];

    if($nBulan == "01"){
        $cBulan = "Jan";
    }else if($nBulan == "02"){
        $cBulan = "Feb";
    }else if($nBulan == "03"){
        $cBulan = "Mar";
    }else if($nBulan == "04"){
        $cBulan = "Apr";
    }else if($nBulan == "05"){
        $cBulan = "Mei";
    }else if($nBulan == "06"){
        $cBulan = "Juni";
    }else if($nBulan == "07"){
        $cBulan = "Juli";
    }else if($nBulan == "08"){
        $cBulan = "Agst";
    }else if($nBulan == "09"){
        $cBulan = "Spt";
    }else if($nBulan == "10"){
        $cBulan = "Okt";
    }else if($nBulan == "11"){
        $cBulan = "Nov";
    }else if($nBulan == "12"){
        $cBulan = "Des";
    }

    $cFix = $nHari." ".$cBulan;
    return $cFix;

}
?>


<link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />

<div id="kt_morris_3" style="height:500px;"></div>


<script src="<?=base_url()?>web/plugins/jquery.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/plugins/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<!--begin::Page Vendors(used by this page)-->
        <script src="//www.amcharts.com/lib/3/amcharts.js"></script>
        <script src="//www.amcharts.com/lib/3/serial.js"></script>
        <script src="//www.amcharts.com/lib/3/radar.js"></script>
        <script src="//www.amcharts.com/lib/3/pie.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
        <script src="//www.amcharts.com/lib/3/themes/light.js"></script>
        <!--end::Page Vendors-->


<script type="text/javascript">

"use strict";

// Class definition
var KTamChartsChartsDemo = function() {

  

    var demo6 = function() {
        var chart = AmCharts.makeChart("kt_morris_3", {
            "type": "serial",
            "theme": "light",
            "marginRight": 40,
            "marginLeft": 40,
            "autoMarginOffset": 20,
            "mouseWheelZoomEnabled": true,
            "dataDateFormat": "YYYY-MM-DD",
            "valueAxes": [{
                "id": "v1",
                "axisAlpha": 0,
                "position": "left",
                "ignoreAxisWidth": true
            }],
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [{
                "id": "g1",
                "balloon": {
                    "drop": true,
                    "adjustBorderColor": false,
                    "color": "#ffffff"
                },
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 5,
                "hideBulletsCount": 50,
                "lineThickness": 2,
                "title": "red line",
                "useLineColorForBulletBorder": true,
                "valueField": "value",
                "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 80,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 1,
                "cursorColor": "#258cbb",
                "limitToGraph": "g1",
                "valueLineAlpha": 0.2,
                "valueZoomable": true
            },
            "valueScrollbar": {
                "oppositeAxis": false,
                "offset": 50,
                "scrollbarHeight": 10
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true
            },
            "export": {
                "enabled": true
            },
            "dataProvider": [

            <?php 
             if($row->num_rows() > 0){
             foreach ($row->result_array() as $key => $vaData) { 
            ?>
            {
                "date": "<?=$vaData['account_detail_sales_date']?>",
                "value": <?=$vaData['amount']?>
            },
        <?php } ?> 
        <?php }else{ ?>
            {
                "date": "<?=date("Y-m-D")?>",
                "value": 0
            }, 
        <?php } ?>


            ]
        });

        chart.addListener("rendered", zoomChart);

        zoomChart();

        function zoomChart() {
            chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
        }
    }

   

    return {
        // public functions
        init: function() {
           
            demo6();
           
        }
    };
}();

jQuery(document).ready(function() {
    KTamChartsChartsDemo.init();
});
</script>