<?php
function convertDate($tgl){
    $cDate = explode("-", $tgl);

    $nTahun = $cDate[0];
    $nBulan = $cDate[1];
    $nHari  = $cDate[2];

    if($nBulan == "01"){
        $cBulan = "Jan";
    }else if($nBulan == "02"){
        $cBulan = "Feb";
    }else if($nBulan == "03"){
        $cBulan = "Mar";
    }else if($nBulan == "04"){
        $cBulan = "Apr";
    }else if($nBulan == "05"){
        $cBulan = "Mei";
    }else if($nBulan == "06"){
        $cBulan = "Juni";
    }else if($nBulan == "07"){
        $cBulan = "Juli";
    }else if($nBulan == "08"){
        $cBulan = "Agst";
    }else if($nBulan == "09"){
        $cBulan = "Spt";
    }else if($nBulan == "10"){
        $cBulan = "Okt";
    }else if($nBulan == "11"){
        $cBulan = "Nov";
    }else if($nBulan == "12"){
        $cBulan = "Des";
    }

    $cFix = $nHari." ".$cBulan;
    return $cFix;

}
?>


<link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />

<div id="kt_morris_12" style="height:500px;"></div>


<script src="<?=base_url()?>web/plugins/jquery.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/plugins/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<!--begin::Page Vendors(used by this page)-->
        <script src="//www.amcharts.com/lib/3/amcharts.js"></script>
        <script src="//www.amcharts.com/lib/3/serial.js"></script>
        <script src="//www.amcharts.com/lib/3/radar.js"></script>
        <script src="//www.amcharts.com/lib/3/pie.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js"></script>
        <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
        <script src="//www.amcharts.com/lib/3/themes/light.js"></script>
        <!--end::Page Vendors-->


<script type="text/javascript">

"use strict";

// Class definition
var KTamChartsChartsDemo = function() {

  

    var demo10 = function() {
        var chart = AmCharts.makeChart("kt_morris_12", {
            "rtl": KTUtil.isRTL(),
            "type": "serial",
            "theme": "light",
            "dataProvider": [

             <?php 
                if($row->num_rows() > 0){
                     foreach ($row->result_array() as $key => $vaData9) {               
                ?>
            {
                "country": "<?=$vaData9['nama_member']?>",
                "visits": <?=$vaData9['amount']?>
            }, 

        <?php } ?>
    <?php }else{ ?>
    	{
                "country": "None",
                "visits": 0
            }, 
    <?php } ?>
		

		],
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": true
            }

        });
    }

   

    return {
        // public functions
        init: function() {
           
            demo10();
           
        }
    };
}();

jQuery(document).ready(function() {
    KTamChartsChartsDemo.init();
});
</script>