<?php
//$xx = @$this->uri->segment(3, 0);
//if($xx == "stock_opname_produk" && $xx != 0){ 
//}else if (@!$_SESSION['logged_in'] && $allow != 1) {
	
//	redirect(site_url() . "Login");
//}

function convertDate($tgl){
	$cDate = explode("-", $tgl);

	$nTahun = $cDate[0];
	$nBulan	= $cDate[1];
	$nHari  = $cDate[2];

	if($nBulan == "01"){
		$cBulan = "Jan";
	}else if($nBulan == "02"){
		$cBulan = "Feb";
	}else if($nBulan == "03"){
		$cBulan = "Mar";
	}else if($nBulan == "04"){
		$cBulan = "Apr";
	}else if($nBulan == "05"){
		$cBulan = "Mei";
	}else if($nBulan == "06"){
		$cBulan = "Juni";
	}else if($nBulan == "07"){
		$cBulan = "Juli";
	}else if($nBulan == "08"){
		$cBulan = "Agst";
	}else if($nBulan == "09"){
		$cBulan = "Spt";
	}else if($nBulan == "10"){
		$cBulan = "Okt";
	}else if($nBulan == "11"){
		$cBulan = "Nov";
	}else if($nBulan == "12"){
		$cBulan = "Des";
	}

	$cFix = $nHari." ".$cBulan;
	return $cFix;

}
?>
<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
	<base href="../../../">
	<meta charset="utf-8" />
	<title>MSGLOW OFFICE</title>
	<meta name="description" content="Amcharts charts examples">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--begin::Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

	<!--end::Fonts -->

	<!--begin::Page Vendors Styles(used by this page) -->
	<link href="<?=base_url()?>web/plugins/general/morris.js/morris.css" rel="stylesheet" type="text/css" />

	<!--end::Page Vendors Styles -->

	<!--begin::Global Theme Styles(used by all pages) -->

	<!--begin:: Vendor Plugins -->
	
	<link href="<?=base_url()?>web/plugins/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />

	<link href="<?=base_url()?>web/plugins/general/plugins/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>web/plugins/general/plugins/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>web/plugins/general/plugins/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url()?>web/plugins/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />

	<!--end:: Vendor Plugins -->
	<link href="<?=base_url()?>web/css/style.bundle.dashboard.css" rel="stylesheet" type="text/css" />

	<!--begin:: Vendor Plugins for custom pages -->
	<link href="<?=base_url()?>web/plugins/custom/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	

	<!--end:: Vendor Plugins for custom pages -->

	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->

	<!--end::Layout Skins -->
	<link rel="shortcut icon" href="https://msglow.app/upload/nitrogen.png" />
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">

	<!-- begin:: Page -->

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="#">
				<img alt="Logo" src="https://msglow.app/upload/logo_nitromerah.png" style="max-height: 20px;" />
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
		</div>
	</div>

	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

			<!-- begin:: Aside -->
			<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
			<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

				<!-- begin:: Aside Menu -->
				<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
					<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
						<ul class="kt-menu__nav ">
							<li class="kt-menu__item " aria-haspopup="true"><a href="<?=base_url()?>" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-protection"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
							
						</ul>
					</div>
				</div>

				<!-- end:: Aside Menu -->
			</div>

			<!-- end:: Aside -->
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

				<!-- begin:: Header -->
				<div id="kt_header" class="kt-header kt-grid kt-grid--ver  kt-header--fixed ">

					<!-- begin:: Aside -->
					<div class="kt-header__brand kt-grid__item  " id="kt_header_brand">
						<div class="kt-header__brand-logo">
							<a href="index.html">
								<img alt="Logo" src="<?=base_url()?>web/media/logos/logo-6.png" />
							</a>
						</div>
					</div>

					<!-- end:: Aside -->

					<!-- begin:: Title -->
					<h3 class="kt-header__title kt-grid__item">
						Dashboard
					</h3>

					<!-- end:: Title -->

					<!-- begin: Header Menu -->
					<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
					<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
						<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
							<ul class="kt-menu__nav ">
								<li class="kt-menu__item " aria-haspopup="true"><a href="<?=base_url()?>" class="kt-menu__link "><span class="kt-menu__link-text">Home</span></a></li>
							</ul>
						</div>
					</div>
				</div>

				<!-- end:: Header -->
				<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
					<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
					
					<br/>
					<div class="row">
	<div class="col-lg-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Purchase Requisition & Sales Order; By Amount
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_2" role="tab">
									Filter By Monthly & Yearly
								</a>
							</li>
							<!--
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_3" role="tab">
									Yearly
								</a>
							</li>
						-->
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2">
							<div class="kt-subheader-search" style="background: color:blue">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter
										<span class="kt-subheader-search__desc">
											PO & SO
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Report Category, Month & Year</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															
															<select name="cKategoriPoSoJoin" id="cKategoriPoSoJoin" class="form-control">
																<option></option>
																<option value="bulan" selected>Monthly</option>
																<option value="tahun" >Yearly</option>
																
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-cogs"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<br/>
															<select name="cBulanPoSoJoin" id="cBulanPoSoJoin" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span  class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<br/>
															<select name="cTahunPoSoJoin" id="cTahunPoSoJoin" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetPoSoJoin();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
													</div>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetPoSoJoin(){
								var cBulanPoSoJoin = $('#cBulanPoSoJoin').val();
								var cTahunPoSoJoin = $('#cTahunPoSoJoin').val();
								$('#div_po_so_join').html(cBulanPoSoJoin+cTahunPoSoJoin);
							}
						</script>
						<div class="tab-pane" id="kt_portlet_tab_1_3">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter
										<span class="kt-subheader-search__desc">
											Yearly
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Yearly</label>
														</div>
													</div>
													
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunPoSoJoin2" id="cTahunPoSoJoin2" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12"><br/><button onclick="return GetPoSoJoin2();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Yearly Report</button>
													</div>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						
					</div>
					<script type="text/javascript">
							function GetPoSoJoin2(){
								
								var cTahunPoSoJoin2 = $('#cTahunPoSoJoin2').val();
								$('#div_po_so_join').html(cTahunPoSoJoin2);
							}
						</script>
					<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
							<div id="div_po_so_join">
								<div id="kt_morris_2" style="height:500px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			
		</div>
		
	</div>
</div>
<!-- --->

<div class="row">
	<div class="col-lg-4">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						How many Purchase Requisition (unpaid)
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_2_2" role="tab">
									Filter By Month & Yearly
								</a>
							</li>
							
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2_2">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Monthly
										<span class="kt-subheader-search__desc">
											 Purchase Requisition (Unpaid)
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Report Category, Month & Year</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															
															<select onchange="filterkategoriunpaid()" name="cKategoriPoUnpaid" id="cKategoriPoUnpaid" class="form-control">
																<option></option>
																<option value="bulan" selected>Monthly</option>
																<option value="tahun" >Yearly</option>
																
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-cogs"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12" id="bulan_unpaid">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<br/>
															<select name="cBulanPoUnpaid" id="cBulanPoUnpaid" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>

													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunPoUnpaid" id="cTahunPoUnpaid" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetPoUnpaidMonthly();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button> 
													</div>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetPoUnpaidMonthly(){
								var cBulanPoUnpaid = $('#cBulanPoUnpaid').val();
								var cTahunPoUnpaid = $('#cTahunPoUnpaid').val();
								var cKategoriPoUnpaid = $('#cKategoriPoUnpaid').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetPoUnpaidMonthly')?>/"+cBulanPoUnpaid+'/'+cTahunPoUnpaid+'/'+cKategoriPoUnpaid,
									cache: false,
									beforeSend:function(){
										$('#div_po_unpaid').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
										$('#div_po_unpaid').html(msg);
									}
								});  
								
							}

							function filterkategoriunpaid(){
								var cKategoriPoUnpaid = $('#cKategoriPoUnpaid').val();

								if(cKategoriPoUnpaid == 'tahun'){
								document.getElementById("bulan_unpaid").style.display = "none";
								}else if(cKategoriPoUnpaid == 'bulan'){
									document.getElementById("bulan_unpaid").style.display = "block";
								}
							}
						</script>
						<div class="tab-pane" id="kt_portlet_tab_1_3_2">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Yearly
										<span class="kt-subheader-search__desc">
											Purchase Requisition (Unpaid)
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Yearly</label>
														</div>
													</div>
													
													<div class="col-lg-12 col-sm-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunPoUnpaid2" id="cTahunPoUnpaid2" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
												
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetPoUnpaidYearly();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Yearly Report</button>
													</div>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetPoUnpaidYearly(){
								var cTahunPoUnpaid = $('#cTahunPoUnpaid2').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetPoUnpaidYearly')?>/"+cTahunPoUnpaid,
									cache: false,
									beforeSend:function(){
										$('#div_po_unpaid').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
										$('#div_po_unpaid').html(msg);
									}
								});  
							}
						</script>
						<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
							<div id="div_po_unpaid">
								<div id="kt_morris_3" style="height:500px;"></div>
							</div>
						</div>
					</div>
					</div>
					
				</div>
			</div>

			
		</div>
		
	</div>
	<div class="col-lg-4">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						How many PR become Sales Order (paid)
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_2_3" role="tab">
									Filter By Monthly & Yearly
								</a>
							</li>
							
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2_3">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Monthly
										<span class="kt-subheader-search__desc">
											PR become Sales Order (paid)
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Report Category, Month & Year</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															
															<select onchange="return filterkategoripaid();" name="cKategoriPopaid" id="cKategoriPopaid" class="form-control">
																<option></option>
																<option value="bulan" selected>Monthly</option>
																<option value="tahun" >Yearly</option>
																
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-cogs"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12" id="bulan_paid">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<br/>
															<select name="cBulanPopaid" id="cBulanPopaid" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunPoPaid" id="cTahunPoPaid" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetPopaidMonthly();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search  Report</button>
													</div>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetPopaidMonthly(){
								var cBulanPopaid = $('#cBulanPopaid').val();
								var cTahunPopaid = $('#cTahunPoPaid').val();
								var cKategoriPopaid = $('#cKategoriPopaid').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetPoPaidMonthly')?>/"+cBulanPopaid+'/'+cTahunPopaid+'/'+cKategoriPopaid,
									cache: false,
									beforeSend:function(){
									$('#div_po_paid').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
										$('#div_po_paid').html(msg);
									}
								});  
							}

							function filterkategoripaid(){
								var cKategoriPopaid = $('#cKategoriPopaid').val();

								if(cKategoriPopaid == 'tahun'){
								document.getElementById("bulan_paid").style.display = "none";
								}else if(cKategoriPopaid == 'bulan'){
									document.getElementById("bulan_paid").style.display = "block";
								}
							}

							
						</script>
						<div class="tab-pane" id="kt_portlet_tab_1_3_3">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Yearly
										<span class="kt-subheader-search__desc">
											PR become Sales Order (paid)
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Yearly</label>
														</div>
													</div>
													
													<div class="col-lg-12 col-sm-12">
													
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunPoPaid2" id="cTahunPoPaid2" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
												
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetPopaidYearly();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Yearly Report</button>
													</div>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetPopaidYearly(){
								var cTahunPoPaid2 = $('#cTahunPoPaid2').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetPoPaidYearly')?>/"+cTahunPoPaid2,
									cache: false,
									beforeSend:function(){
									$('#div_po_paid').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
										$('#div_po_paid').html(msg);
									}
								});  
							}
						</script>
						<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
						   <div id="div_po_paid">
							<div id="kt_morris_4" style="height:500px;"></div>
						   </div>
						</div>
					</div>
					</div>
					
				</div>
			</div>

			
		</div>
		
	</div>
	<div class="col-lg-4">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						How many Sales Order based on Quantity 
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_2_4" role="tab">
									Filter By Monthly & Yearly
								</a>
							</li>
							<!--
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_3_4" role="tab">
									Yearly
								</a>
							</li>
						-->
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2_4">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Monthly
										<span class="kt-subheader-search__desc">
											Sales Order based on Quantity
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Report Category, Month & Year</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															
															<select name="cKategoriPoSo" id="cKategoriPoSo" onchange="return filterkategoriposo();" class="form-control">
																<option></option>
																<option value="bulan" selected>Monthly</option>
																<option value="tahun" >Yearly</option>
																
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-cogs"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12" id="bulan_poso">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<br/>
															<select name="cBulanPoSo" id="cBulanPoSo" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunPoSo" id="cTahunPoSo" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetPoSo();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Report</button>
													</div>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetPoSo(){
								var cBulanPoSo = $('#cBulanPoSo').val();
								var cTahunPoSo = $('#cTahunPoSo').val();
								var cKategoriPoSo = $('#cKategoriPoSo').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByQtyMonthly')?>/"+cBulanPoSo+'/'+cTahunPoSo+'/'+cKategoriPoSo,
									cache: false,
									beforeSend:function(){
									$('#div_po_so').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_po_so').html(msg);
									}
								});  
							}

							function filterkategoriposo(){
								var cKategoriPoSo = $('#cKategoriPoSo').val();

								if(cKategoriPoSo == 'tahun'){
								document.getElementById("bulan_poso").style.display = "none";
								}else if(cKategoriPoSo == 'bulan'){
									document.getElementById("bulan_poso").style.display = "block";
								}
							}
						</script>
						<div class="tab-pane" id="kt_portlet_tab_1_3_4">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Yearly
										<span class="kt-subheader-search__desc">
											Sales Order based on Quantity
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Yearly</label>
														</div>
													</div>
													
													<div class="col-lg-12 col-sm-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunPoSo2" id="cTahunPoSo2" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
												
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetPoSo2();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Yearly Report</button>
													</div>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetPoSo2(){
								
								var cTahunPoSo2 = $('#cTahunPoSo2').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByQtyYearly')?>/"+cTahunPoSo2,
									cache: false,
									beforeSend:function(){
									$('#div_po_so').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_po_so').html(msg);
									}
								});  
							}
						</script>
						<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
							<div id="div_po_so">
								<div id="kt_morris_5" style="height:500px;"></div>
							</div>
						</div>
					</div>
					</div>
					
				</div>
			</div>

			
		</div>
		
	</div>
	<div class="col-lg-6">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Sales Order Quantity, By Category and Brand - Bar Chart
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_2_6" role="tab">
									Category
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_3_6" role="tab">
									Brand
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2_6">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter By Category
										<span class="kt-subheader-search__desc">
											SO Quantity By Category
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Category</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cBulanKategori1" id="cBulanKategori1" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunKategori1" id="cTahunKategori1" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>

													<div class="col-lg-12">
														<br/>
														<button  type="button" onclick="return GetSoKategori1();" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Category Report</button>
													</div>
													
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetSoKategori1(){
								var cBulanKategori1 = $('#cBulanKategori1').val();
								var cTahunKategori1 = $('#cTahunKategori1').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByCategoryMonthlyBar')?>/"+cBulanKategori1+'/'+cTahunKategori1,
									cache: false,
									beforeSend:function(){
									$('#div_so_brand_kategori_1_1').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_so_brand_kategori_1_1').html(msg);
									}
								});  
							}
						</script>
						<div class="tab-pane" id="kt_portlet_tab_1_3_6">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter By Brand
										<span class="kt-subheader-search__desc">
											SO Quantity By  Brand
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Brand</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cBulanBrand1" id="cBulanBrand1" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12 col-sm-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunBrand1" id="cTahunBrand1" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
												
													<div class="col-lg-12">
														<br/>
														<button type="button" onclick="return GetSoBrand1();" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Brand Report</button>
													</div>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetSoBrand1(){
								var cBulanBrand1 = $('#cBulanBrand1').val();
								var cTahunBrand1 = $('#cTahunBrand1').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByBrandMonthlyBar')?>/"+cBulanBrand1+'/'+cTahunBrand1,
									cache: false,
									beforeSend:function(){
									$('#div_so_brand_kategori_1_1').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_so_brand_kategori_1_1').html(msg);
									}
								});  
							}
						</script>
						<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
							<div id="div_so_brand_kategori_1_1">
								<div id="kt_morris_7" style="height:500px;"></div>
							</div>
						</div>
					</div>
					</div>
					
				</div>
			</div>

			
		</div>
		
	</div>
	<div class="col-lg-6">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Sales Order Quantity, By Category and Brand - Line Chart 
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_2_5" role="tab">
									Category
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_3_5" role="tab">
									Brand
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2_5">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter By Category
										<span class="kt-subheader-search__desc">
											SO Quantity By Category
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Category</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cBulanKategori2" id="cBulanKategori2" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12 col-sm-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunKategori2" id="cTahunKategori2" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetSoKategori2();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Category Report</button>
													</div>
													
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetSoKategori2(){
								var cBulanKategori2 = $('#cBulanKategori2').val();
								var cTahunKategori2 = $('#cTahunKategori2').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByCategoryMonthlyLine')?>/"+cBulanKategori2+'/'+cTahunKategori2,
									cache: false,
									beforeSend:function(){
									$('#div_so_brand_kategori_2').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_so_brand_kategori_2').html(msg);
									}
								});  
								
							}
						</script>
						<div class="tab-pane" id="kt_portlet_tab_1_3_5">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter By Brand
										<span class="kt-subheader-search__desc">
											SO Quantity By Brand
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Brand</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cBulanBrand2" id="cBulanBrand2" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12 col-sm-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunBrand2" id="cTahunBrand2" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
												
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetSoBrand2();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Brand Report</button>
													</div>
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetSoBrand2(){
								var cBulanBrand2 = $('#cBulanBrand2').val();
								var cTahunBrand2 = $('#cTahunBrand2').val();

								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoByBrandMonthlyLine')?>/"+cBulanBrand2+'/'+cTahunBrand2,
									cache: false,
									beforeSend:function(){
									$('#div_so_brand_kategori_2').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_so_brand_kategori_2').html(msg);
									}
								});  
							}
						</script>
						<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
						 <div id="div_so_brand_kategori_2">
							<div id="kt_morris_6" style="height:500px;"></div>
						 </div>
						</div>
					</div>
					</div>
					
				</div>
			</div>

			
		</div>
		
	</div>

	<!-- -->
	<div class="col-lg-6">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Sales Order Quantity, By Seller - Bar Chart 
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_2_7" role="tab">
									Seller
								</a>
							</li>
							
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2_7">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Seller 
										<span class="kt-subheader-search__desc">
											Sales Order Quantity, By Seller - Bar Chart
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Month, Year dan Seller</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cBulanSeller1" id="cBulanSeller1" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunSeller1" id="cTahunSeller1" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cMemberSeller1" id="cMemberSeller1" class="form-control">
																<option>All</option>
																
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetSoSeller1()" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Seller Report</button>
													</div>
													
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetSoSeller1(){
								var cBulanSeller1 = $('#cBulanSeller1').val();
								var cTahunSeller1 = $('#cTahunSeller1').val();
								

								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoSellerBar')?>/"+cBulanSeller1+'/'+cTahunSeller1,
									cache: false,
									beforeSend:function(){
									$('#div_so_seller_1').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_so_seller_1').html(msg);
									}
								});  

								
							}
						</script>
						
						
						<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
						   <div id="div_so_seller_1">
							<div id="kt_morris_8" style="height:500px;"></div>
						   </div>
						 
						</div>
					</div>
					</div>
					
				</div>
			</div>

			
		</div>
		
	</div>
	<div class="col-lg-6">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Sales Order Quantity, By Seller - Line Chart
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_2_8" role="tab">
									Seller
								</a>
							</li>
							
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2_8">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Seller 
										<span class="kt-subheader-search__desc">
											Sales Order Quantity, By Seller - Line Chart
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Month, Year dan Seller</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cBulanSeller2" id="cBulanSeller2" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunSeller2" id="cTahunSeller2" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cMemberSeller2" id="cMemberSeller2" class="form-control">
																<option>--Pilih Seller--</option>
																
																<option value="Member" >Member</option>
																<option value="RESELLER" >RESELLER</option>
																<option value="AGEN" >AGEN</option>
																<option value="DISTRIBUTOR" >DISTRIBUTOR</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<button onclick="return GetSoSeller2()" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Seller Report</button>
													</div>
													
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function GetSoSeller2(){
								var cBulanSeller2 = $('#cBulanSeller2').val();
								var cTahunSeller2 = $('#cTahunSeller2').val();
								var cMemberSeller2 = $('#cMemberSeller2').val();
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSoSellerLine')?>/"+cBulanSeller2+'/'+cTahunSeller2+'/'+cMemberSeller2,
									cache: false,
									beforeSend:function(){
									$('#div_so_seller_2').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_so_seller_2').html(msg);
									}
								});  
								
							}
						</script>
						<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
							<div id="div_so_seller_2">
								<div id="kt_morris_9" style="height:500px;"></div>
							</div>
						</div>
					</div>
					</div>
					
				</div>
			</div>

			
		</div>
		
	</div>


	<!-- -->

	<div class="col-lg-6">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Top 10 Best Seller Products  
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_2_9" role="tab">
									Monthly
								</a>
							</li>
							
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2_9">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Monthly 
										<span class="kt-subheader-search__desc">
											Top 10 Best Seller Products
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Month</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<br/>
															<select name="cBulanFastMoving" id="cBulanFastMoving" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunFastMoving" id="cTahunFastMoving" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<button onclick="return FastMoving();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search 10 Best Seller Products</button>
													</div>
													
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						
						<script type="text/javascript">
							function FastMoving(){
								var cBulanFastMoving = $('#cBulanFastMoving').val();
								var cTahunFastMoving = $('#cTahunFastMoving').val();
				
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetFastMoving')?>/"+cBulanFastMoving+'/'+cTahunFastMoving,
									cache: false,
									beforeSend:function(){
									$('#div_fast_moving').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_fast_moving').html(msg);
									}
								});  
								
							}
						</script>
						<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
						  <div id="div_fast_moving">
							<div id="kt_morris_10" style="height:500px;"></div>
						  </div>
						</div>
					</div>
					</div>
					
				</div>
			</div>

			
		</div>
		
	</div>
	<div class="col-lg-6">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Top 10 Slow Moving Products 
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_2_8" role="tab">
									Monthly
								</a>
							</li>
							
							
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2_8">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Monthly 
										<span class="kt-subheader-search__desc">
											Top 10 Slow Moving Products
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Monthly</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<br>
															<select name="cBulanSlowMoving" id="cBulanSlowMoving" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunSlowMoving" id="cTahunSlowMoving" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<button onclick="return SlowMoving();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Monthly Report</button>
													</div>
													
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						
						<script type="text/javascript">
							function SlowMoving(){
								var cBulanSlowMoving = $('#cBulanSlowMoving').val();
								var cTahunSlowMoving = $('#cTahunSlowMoving').val();
				
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/GetSlowMoving')?>/"+cBulanSlowMoving+'/'+cTahunSlowMoving,
									cache: false,
									beforeSend:function(){
									$('#div_slow_moving').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_slow_moving').html(msg);
									}
								});  
								
							}
						</script>
						<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
						  <div id="div_slow_moving">
							<div id="kt_morris_11" style="height:500px;"></div>
						  </div>
						</div>
					</div>
					</div>
					
				</div>
			</div>

			
		</div>
		
	</div>


	<div class="col-lg-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-hidden">
						<i class="la la-gear"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Top Seller - Monthly
					</h3>
				</div>
			</div>

			<div class="kt-portlet kt-portlet--tabs">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">

					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
							
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_2_10" role="tab">
									Seller
								</a>
							</li>
							
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<div class="tab-pane active" id="kt_portlet_tab_1_2_10">
							<div class="kt-subheader-search ">
								<div class="kt-container  kt-container--fluid ">
									<h3 class="kt-subheader-search__title">
										Report Filter Seller 
										<span class="kt-subheader-search__desc">
											Top Seller - Monthly
										</span>
									</h3>
									<form class="kt-form">
										<div class="kt-grid kt-grid--desktop kt-grid--ver-desktop">
											<div class="kt-grid__item kt-grid__item--middle">
												<div class="row kt-margin-r-10">
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<label style="color: white">Choose Month, Year and Seller</label>
														</div>
													</div>
													<div class="col-lg-12">
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<br/>
															<select name="cBulanTopSeller" id="cBulanTopSeller" class="form-control">
																<option></option>
																<option value="01" >Januari</option>
																<option value="02" >Februari</option>
																<option value="03" >Maret</option>
																<option value="04" >April</option>
																<option value="05" >Mei</option>
																<option value="06" >Juni</option>
																<option value="07" >Juli</option>
																<option value="08" >Agustus</option>
																<option value="09" >September</option>
																<option value="10" >Oktober</option>
																<option value="11" selected>November</option>
																<option value="12" >Desember</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTahunTopSeller" id="cTahunTopSeller" class="form-control">
																<option></option>
																<option value="2019" >2019</option>
																<option value="2020" >2020</option>
																<option value="2021" selected>2021</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<div class="kt-input-icon kt-input-icon--pill kt-input-icon--right">
															<select name="cTopSeller" id="cTopSeller" class="form-control">
																<option>--Choose Seller--</option>
																<option value="All" >All</option>
																<option value="Member" >Member</option>
																<option value="RESELLER" >RESELLER</option>
																<option value="AGEN" >AGEN</option>
																<option value="DISTRIBUTOR" >DISTRIBUTOR</option>
															</select> 
															<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i style="color:white" class="la la-calendar-check-o"></i></span></span>
														</div>
													</div>
													<div class="col-lg-12">
														<br/>
														<button onclick="return TopSeller();" type="button" class="btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn">Search Seller Report</button>
													</div>
													
												</div>
											</div>
											
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<script type="text/javascript">
							function TopSeller(){
								var cBulanTopSeller = $('#cBulanTopSeller').val();
								var cTahunTopSeller = $('#cTahunTopSeller').val();
								var cTopSeller 		= $('#cTopSeller').val();
				
								$.ajax({
									type   : "GET",
									url    : "<?=site_url('dashboardA/Chart/TopSeller')?>/"+cBulanTopSeller+'/'+cTahunTopSeller+'/'+cTopSeller,
									cache: false,
									beforeSend:function(){
									$('#div_top_seller').html("<img src='<?=base_url()?>web/media/loading.gif' style='height:50px'>" );  
									},
									success:function(msg){
									$('#div_top_seller').html(msg);
									}
								});  
								
							}
						</script>
						<div class="kt-portlet__body">
						<div style="overflow-x:auto;"  class="box-body">
						   <div id="div_top_seller">
								<div id="kt_morris_12" style="height:500px;"></div>
							</div>
						</div>
					</div>
					</div>
					
				</div>
			</div>

			
		</div>
		
	</div>
	
</div>
			
					</div>
				</div>

										<!-- begin:: Footer -->
										<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
											<div class="kt-container  kt-container--fluid ">
												<div class="kt-footer__copyright">
													2019&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
												</div>
												
											</div>
										</div>

										<!-- end:: Footer -->
									</div>
								</div>
							</div>

							<!-- end:: Page -->

							

							<!-- begin::Scrolltop -->
							<div id="kt_scrolltop" class="kt-scrolltop">
								<i class="fa fa-arrow-up"></i>
							</div>

							<!-- end::Scrolltop -->



							<!-- begin::Global Config(global config for global JS sciprts) -->
							<script>
								var KTAppOptions = {
									"colors": {
										"state": {
											"brand": "#22b9ff",
											"light": "#ffffff",
											"dark": "#282a3c",
											"primary": "#5867dd",
											"success": "#34bfa3",
											"info": "#36a3f7",
											"warning": "#ffb822",
											"danger": "#fd3995"
										},
										"base": {
											"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
											"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
										}
									}
								};
							</script>

							<!-- end::Global Config -->

							<!--begin::Global Theme Bundle(used by all pages) -->

							<!--begin:: Vendor Plugins -->
							<script src="<?=base_url()?>web/plugins/jquery.js" type="text/javascript"></script>

							<script src="<?=base_url()?>web/plugins/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>

							<script src="<?=base_url()?>web/plugins/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
							<script src="<?=base_url()?>web/plugins/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>

							

						

							<!--end:: Vendor Plugins -->
							<script src="<?=base_url()?>web/js/scripts.bundle.js" type="text/javascript"></script>

							<!--begin:: Vendor Plugins for custom pages -->
							<script src="<?=base_url()?>web/plugins/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>


						<script src="<?=base_url()?>web/plugins/general/raphael/raphael.js" type="text/javascript"></script>
		<script src="<?=base_url()?>web/plugins/general/morris.js/morris.js" type="text/javascript"></script>
		<script src="<?=base_url()?>web/plugins/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>

							<!--end::Page Vendors -->

							<!--begin::Page Scripts(used by this page) -->
							<script type="text/javascript">
								"use strict";
// Class definition
var KTMorrisChartsDemo = function() {

  

    var demo2 = function() {
        // AREA CHART
        new Morris.Area({
            element: 'kt_morris_2',
            data: [{
                    y: '01 Nov',
                    a: 20000000,
                    b: 19000000
                },
                {
                    y: '02 Nov',
                    a: 22000000,
                    b: 20000000
                },
                {
                    y: '03 Nov',
                    a: 25000000,
                    b: 24000000
                },
                {
                    y: '04 Nov',
                    a: 24000000,
                    b: 24000000
                },
                {
                    y: '05 Nov',
                    a: 18000000,
                    b: 17000000
                },
                {
                    y: '06 Nov',
                    a: 32000000,
                    b: 30000000
                },
                {
                    y: '07 Nov',
                    a: 28000000,
                    b: 25000000
                }, 
                {
                    y: '08 Nov',
                    a: 34000000,
                    b: 32000000
                },
                {
                    y: '09 Nov',
                    a: 37000000,
                    b: 36500000
                },
                {
                    y: '10 Nov',
                    a: 28000000,
                    b: 28000000
                },
                {
                    y: '11 Nov',
                    a: 18000000,
                    b: 18000000
                }, 
                {
                    y: '12 Nov',
                    a: 38000000,
                    b: 35000000
                },
                {
                    y: '13 Nov',
                    a: 25000000,
                    b: 25000000
                },
                {
                    y: '14 Nov',
                    a: 27000000,
                    b: 27000000
                },
                {
                    y: '15 Nov',
                    a: 30000000,
                    b: 27000000
                },
            ],
            xkey: 'y',
            ykeys: ['a', 'b'],
            parseTime: false,
            labels: ['PR', 'SO'],
            lineColors: ['#8effad', '#af87ff'],
            pointFillColors: ['#fe3995','#fe3995']
        });
    }

   

    var poUnpaid = function() {
        // LINE CHART
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_3',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
            	<?php 
                if($PoUnpaidMonthly->num_rows() > 0){
                     foreach ($PoUnpaidMonthly->result_array() as $key => $vaData1) {               
                ?>
                {
                    y: '<?=convertDate($vaData1['account_detail_sales_date'])?>',
                    a: '<?=$vaData1['amount']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'None',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['PR unPaid'],
            lineColors: ['#007bff']
        });
    }

    var poPaid = function() {
        // LINE CHART
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_4',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                <?php 
                if($PoPaidMonthly->num_rows() > 0){
                     foreach ($PoPaidMonthly->result_array() as $key => $vaData2) {               
                ?>
                {
                    y: '<?=convertDate($vaData2['account_detail_sales_date'])?>',
                    a: '<?=$vaData2['amount']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'None',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['PR Paid'],
            lineColors: ['#dc3545']
        });
    }

    var soQTY = function() {
        // LINE CHART
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_5',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                <?php 
                if($SoByQtyMonthly->num_rows() > 0){
                     foreach ($SoByQtyMonthly->result_array() as $key => $vaData3) {               
                ?>
                {
                    y: '<?=convertDate($vaData3['account_detail_sales_date'])?>',
                    a: '<?=$vaData3['so_qty']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'None',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['QTY'],
            lineColors: ['#28a745']
        });
    }

    var soQTYCategoryBrand1 = function() {
        // LINE CHART
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_6',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                <?php 
                if($SoByKategoriMonthlyLine->num_rows() > 0){
                     foreach ($SoByKategoriMonthlyLine->result_array() as $key => $vaData4) {               
                ?>
                {
                    y: '<?=$vaData4['kategori']?>',
                    a: '<?=$vaData4['so_by_kategori']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'None',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['QTY'],
            lineColors: ['#ffc107']
        });
    }

    var soQTYCategoryBrand2 = function() {
    	var barColorsArray = ['#3498DB', '#34495E','#26B99A', '#DE8244'];
		var colorIndex = 0;
        // LINE CHART
        new Morris.Bar({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_7',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                <?php 
                if($SoByKategoriMonthlyBar->num_rows() > 0){
                     foreach ($SoByKategoriMonthlyBar->result_array() as $key => $vaData5) {               
                ?>
                {
                    y: '<?=$vaData5['kategori']?>',
                    a: '<?=$vaData5['so_by_kategori']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'None',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['QTY'],
            barColors: function (row, series, type) {
console.log("--> "+row.label, series, type);
if(row.label == "Additional") return "#ff4a6d";
else if(row.label == "Basic") return "#ffca40";
else if(row.label == "Men") return "#42e2e2";

else if(row.label == "MS Kids") return "#1AB244";
else return "#1AB244";
}
        });
    }



    var soQTYSeller1 = function() {
        // LINE CHART
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_9',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                <?php 
                if($member->num_rows() > 0){
                     foreach ($member->result_array() as $key => $vaDataMember) {               
                ?>
                {
                    y: '<?=$vaDataMember['status']?>',
                    a: '<?=$vaDataMember['member']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'Member',
                    a: '0',
                    
                },
            <?php } ?>


            <?php 
                if($reseller->num_rows() > 0){
                     foreach ($reseller->result_array() as $key => $vaDataReseller) {               
                ?>
                {
                    y: '<?=$vaDataReseller['status']?>',
                    a: '<?=$vaDataReseller['member']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'Reseller',
                    a: '0',
                    
                },
            <?php } ?>


            <?php 
                if($agen->num_rows() > 0){
                     foreach ($agen->result_array() as $key => $vaDataAgen) {               
                ?>
                {
                    y: '<?=$vaDataAgen['status']?>',
                    a: '<?=$vaDataAgen['member']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'Agen',
                    a: '0',
                    
                },
            <?php } ?>


            <?php 
                if($distributor->num_rows() > 0){
                     foreach ($distributor->result_array() as $key => $vaDataDistributor) {               
                ?>
                {
                    y: '<?=$vaDataDistributor['status']?>',
                    a: '<?=$vaDataDistributor['member']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'Distributor',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Qty'],
            lineColors: ['#17a2b8']
        });
    }

    var soQTYSeller2 = function() {
    	var barColorsArray = ['#3498DB', '#34495E','#26B99A', '#DE8244'];
		var colorIndex = 0;
        // LINE CHART
        new Morris.Bar({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_8',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                <?php 
                if($member->num_rows() > 0){
                     foreach ($member->result_array() as $key => $vaDataMember) {               
                ?>
                {
                    y: '<?=$vaDataMember['status']?>',
                    a: '<?=$vaDataMember['member']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'Member',
                    a: '0',
                    
                },
            <?php } ?>


            <?php 
                if($reseller->num_rows() > 0){
                     foreach ($reseller->result_array() as $key => $vaDataReseller) {               
                ?>
                {
                    y: '<?=$vaDataReseller['status']?>',
                    a: '<?=$vaDataReseller['member']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'Reseller',
                    a: '0',
                    
                },
            <?php } ?>


            <?php 
                if($agen->num_rows() > 0){
                     foreach ($agen->result_array() as $key => $vaDataAgen) {               
                ?>
                {
                    y: '<?=$vaDataAgen['status']?>',
                    a: '<?=$vaDataAgen['member']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'Agen',
                    a: '0',
                    
                },
            <?php } ?>


            <?php 
                if($distributor->num_rows() > 0){
                     foreach ($distributor->result_array() as $key => $vaDataDistributor) {               
                ?>
                {
                    y: '<?=$vaDataDistributor['status']?>',
                    a: '<?=$vaDataDistributor['member']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'Distributor',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['QTY'],
            barColors: function (row, series, type) {
console.log("--> "+row.label, series, type);
if(row.label == "MEMBER") return "#1dc9b7";
else if(row.label == "RESELLER") return "#2786fb";
else if(row.label == "AGEN") return "#ffb822";

else if(row.label == "DISTRIBUTOR") return "#1AB244";
else return "#1AB244";
}
        });
    }


    var FastMoving = function() {
    	var barColorsArray = ['#3498DB', '#34495E','#26B99A', '#DE8244'];
		var colorIndex = 0;
        // LINE CHART
        new Morris.Bar({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_10',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                <?php 
                if($FastMoving->num_rows() > 0){
                     foreach ($FastMoving->result_array() as $key => $vaData7) {
                     if($vaData7['jumlah_terjual'] >= 5000){                
                ?>
                {
                    y: '<?=$vaData7['nama_produk']?>',
                    a: '<?=$vaData7['jumlah_terjual']?>',
                    
                },
            <?php }} ?>
            <?php }else{ ?>
                {
                    y: 'None',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Qty'],
           	horizontal: true,
            barColors: function (row, series, type) {
console.log("--> "+row.label, series, type);
if(row.label == "01 Nov") return "#ff4a6d";
else if(row.label == "02 Nov") return "#ffca40";
else if(row.label == "03 Nov") return "#42e2e2";

else if(row.label == "04 Nov") return "#ff4a6d";
else if(row.label == "05 Nov") return "#ffca40";
else if(row.label == "06 Nov") return "#42e2e2";

else if(row.label == "07 Nov") return "#ff4a6d";
else if(row.label == "08 Nov") return "#ffca40";
else if(row.label == "09 Nov") return "#42e2e2";

else if(row.label == "10 Nov") return "#ff4a6d";
else if(row.label == "11 Nov") return "#ffca40";
else if(row.label == "12 Nov") return "#42e2e2";

else if(row.label == "13 Nov") return "#ff4a6d";
else if(row.label == "14 Nov") return "#ffca40";
else if(row.label == "15 Nov") return "#42e2e2";
else return "#1AB244";
}
        });
    }

    var SlowMoving = function() {
    	var barColorsArray = ['#3498DB', '#34495E','#26B99A', '#DE8244'];
		var colorIndex = 0;
        // LINE CHART
        new Morris.Bar({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_11',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                <?php 
                if($SlowMoving->num_rows() > 0){
                     foreach ($SlowMoving->result_array() as $key => $vaData8) {
                     $nTerjual = (!empty($vaData['jumlah_terjual'])) ? $vaData['jumlah_terjual'] : "0";    
                      if($vaData8['jumlah_terjual'] < 5000 and $vaData8['jumlah_terjual'] != 0){            
                ?>
                {
                    y: '<?=$vaData8['nama_produk']?>',
                    a: '<?=$vaData8['jumlah_terjual']?>',
                    
                },
            <?php }} ?>
            <?php }else{ ?>
                {
                    y: 'None',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Qty'],
            horizontal: true,
            barColors: function (row, series, type) {
console.log("--> "+row.label, series, type);
if(row.label == "01 Nov") return "#1dc9b7";
else if(row.label == "02 Nov") return "#2786fb";
else if(row.label == "03 Nov") return "#ffb822";

else if(row.label == "04 Nov") return "#1dc9b7";
else if(row.label == "05 Nov") return "#2786fb";
else if(row.label == "06 Nov") return "#ffb822";

else if(row.label == "07 Nov") return "#1dc9b7";
else if(row.label == "08 Nov") return "#2786fb";
else if(row.label == "09 Nov") return "#ffb822";

else if(row.label == "10 Nov") return "#1dc9b7";
else if(row.label == "11 Nov") return "#2786fb";
else if(row.label == "12 Nov") return "#ffb822";

else if(row.label == "13 Nov") return "#1dc9b7";
else if(row.label == "14 Nov") return "#2786fb";
else if(row.label == "15 Nov") return "#ffb822";
else return "#007bff";
}
        });
    }

  var TopSeller = function() {
    	var barColorsArray = ['#3498DB', '#34495E','#26B99A', '#DE8244'];
		var colorIndex = 0;
        // LINE CHART
        new Morris.Bar({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_12',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
               <?php 
                if($TopSeller2->num_rows() > 0){
                     foreach ($TopSeller2->result_array() as $key => $vaData9) {               
                ?>
                {
                    y: '<?=$vaData9['status']?>',
                    a: '<?=$vaData9['amount']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'None',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Amount'],
            barColors: function (row, series, type) {
console.log("--> "+row.label, series, type);
if(row.label == "AGEN") return "#ff4a6d";
else if(row.label == "DISTRIBUTOR") return "#ffca40";
else if(row.label == "MEMBER") return "#42e2e2";

else if(row.label == "RESELLER") return "#1AB244";

else return "#1AB244";
}
        });
    }

    return {
        // public functions
        init: function() {
         
             demo2();
             poPaid();
             poUnpaid();
             soQTY();
             soQTYCategoryBrand1();
             soQTYCategoryBrand2();
             soQTYSeller1();
             soQTYSeller2();
             FastMoving();
             SlowMoving()
             TopSeller();
         
        }
    };
}();

jQuery(document).ready(function() {
    KTMorrisChartsDemo.init();
});
							</script>

							<!--end::Page Scripts -->
						</body>

						<!-- end::Body -->
						</html>