<li class="">
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Master/index_accounting"><i class="icon-speedometer"></i><span> Dashboard</span></a>
            </li>
            <li class="nav-level">Pembayaran</li>
            <li class="treeview <?php if($file == 'pokemasan_active' || $file == 'pokemasan_mati'){?>active<?php } ?>" >
              <a class="waves-effect waves-dark" href="index.html">
                <i class="icon-plus"></i><span> Pembayaran Seller</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li class="<?php if($file == 'pokemasan_active'){?>active<?php } ?>">
                      <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Accounting/list_pembayaran_day">
                        <i class="icon-arrow-right"></i><span>Pembayaran Hari Ini</span>
                      </a>
                    </li>
                    <li class="<?php if($file == 'pokemasan_active'){?>active<?php } ?>">
                      <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Accounting/list_pembayaran_selectday">
                        <i class="icon-arrow-right"></i><span>Pembayaran Seller All</span>
                      </a>
                    </li>
                    <li class="<?php if($file == 'pokemasan_active'){?>active<?php } ?>">
                      <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Accounting/list_pembayaran_pending">
                        <i class="icon-arrow-right"></i><span>Pending Pembayaran</span>
                      </a>
                    </li>
                    
                </ul>
            </li>
            <li class="treeview <?php if($file == 'kluarkemasan' || $file == 'datakluarkemas'){?>active<?php } ?>">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span> Pengeluaran Kemasan</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li class="<?php if($file == 'kluarkemasan'){?>active<?php } ?>"><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/pengeluaran_kemasan"><i class="icon-arrow-right"></i><span>Input Pengeluaran</span></a></li>
                    <li class="<?php if($file == 'datakluarkemas'){?>active<?php } ?>"><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/data_pengeluaran_kemasan"><i class="icon-arrow-right"></i><span>Data Pengeluaran</span></a></li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="icon-arrow-right"></i><span>Retur Kemasan</span></a></li>
                </ul>
            </li>
            <li class="treeview <?php if($file == 'laporan_po' || $file == 'laporan_kluar' || $file == 'laporan_retur' || $file == 'laporan_actual' || $file == 'laporan_kemasan_rusak'){?>active<?php } ?>">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span> Laporan</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li class="<?php if($file == 'laporan_po'){?>active<?php } ?>"><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/laporan_po_kemasan"><i class="icon-arrow-right"></i><span>Laporan PO</span></a></li>
                    <!-- <li class="<?php if($file == 'laporan_kluar'){?>active<?php } ?>"><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/data_pengeluaran_kemasan"><i class="icon-arrow-right"></i><span>Laporan Kluar Kemasan</span></a></li>
                    <li class="<?php if($file == 'laporan_retur'){?>active<?php } ?>"><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/data_pengeluaran_kemasan"><i class="icon-arrow-right"></i><span>Laporan Retur Kemasan</span></a></li>
                    <li class="<?php if($file == 'laporan_actual'){?>active<?php } ?>"><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/data_pengeluaran_kemasan"><i class="icon-arrow-right"></i><span>Laporan Stock Actual</span></a></li>
                    <li class="<?php if($file == 'laporan_kemasan_rusak'){?>active<?php } ?>"><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/data_pengeluaran_kemasan"><i class="icon-arrow-right"></i><span>Laporan Kemasan Rusak</span></a></li> -->
                </ul>
            </li>