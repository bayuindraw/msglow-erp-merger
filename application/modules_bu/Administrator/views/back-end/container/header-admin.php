<!-- Sidebar Menu-->
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>"><i class="icon-speedometer"></i><span> Dashboard</span></a>
            </li>
            <li class="nav-level">Master</li>
            
            <li class="treeview">
              <a class="waves-effect waves-dark" href="index.html">
                <i class="icon-list"></i><span> Master Data</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Master/Kemasan"><i class="icon-arrow-right"></i><span>Master Kemasan</span></a></li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Master/Produk"><i class="icon-arrow-right"></i><span>Master Produk</span></a></li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Master/Supplier"><i class="icon-arrow-right"></i><span>Master Supplier</span></a></li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Master/Pabrik"><i class="icon-arrow-right"></i><span>Master Pabrik</span></a></li>
                </ul>
            </li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="index.html">
                <i class="icon-handbag"></i><span> Master Seller</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Master/seller">
                      <i class="icon-arrow-right"></i><span>Data Seller</span></a>
                    </li>
                    
                </ul>
            </li>
            <li class="nav-level">Kemasan</li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/stock_opname_kemasan"><i class="icon-list"></i></i><span> Stock Kemasan</span></a>
            </li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/kartu_stock_kemasan"><i class="icon-screen-desktop"></i></i><span> Kartu Stock</span></a>
            </li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="index.html">
                <i class="icon-plus"></i><span> List PO</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/po_kemasan_active">
                      <i class="icon-arrow-right"></i><span>Po Active</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/po_kemasan_tidak"><i class="icon-arrow-right"></i><span>Po Selesai</span></a></li>
                    <!-- <li><a class="waves-effect waves-dark" href="#"><i class="icon-arrow-right"></i><span>Retur Kemasan</span></a></li> -->
                </ul>
            </li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span> Pengeluaran Kemasan</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock/data_pengeluaran_kemasan"><i class="icon-arrow-right"></i><span>Data Pengeluaran</span></a></li>
                </ul>
            </li>
            
            <li class="nav-level">Barang jadi</li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock_Produk/stock_opname_produk"><i class="icon-list"></i></i><span> Stock Barang Jadi</span></a>
            </li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock_Produk/kartu_stock_produk"><i class="icon-screen-desktop"></i></i><span> Kartu Stock</span></a>
            </li>
            
            <li class="treeview">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span> List PO</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <!-- <li><a class="waves-effect waves-dark" href="#"><i class="icon-arrow-right"></i><span>Pembayaran PO</span></a></li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="icon-arrow-right"></i><span>Penerimaan PO</span></a></li> -->
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Stock_Produk/po_produk_active">
                      <i class="icon-arrow-right"></i><span>Po Active</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="icon-arrow-right"></i><span>Po Selesai</span></a></li>
                    
                </ul>
            </li>
            

            <li class="nav-level">Penjualan Seller</li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span>Penjualan</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/list_penjualan_day">
                        <i class="icon-arrow-right"></i><span>List Penjualan Hari Ini</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/list_penjualan_selectday">
                        <i class="icon-arrow-right"></i><span>List Penjualan</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/list_penjualanpending_selectday">
                        <i class="icon-arrow-right"></i><span>Pendingan Penjualan</span></a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span>Pembayaran Penjualan</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/list_pembayaran_day">
                        <i class="icon-arrow-right"></i><span>Pembayaran Hari Ini</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/list_pembayaran_selectday">
                        <i class="icon-arrow-right"></i><span>Pembayaran Seller</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/Penjualan_Seller">
                        <i class="icon-arrow-right"></i><span>Termin Pembayaran</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/Penjualan_Seller">
                        <i class="icon-arrow-right"></i><span>Saldo Seller</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="#">
                        <i class="icon-arrow-right"></i><span>Laporan Pembayaran</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-level">Pengemasan</li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/input_kemas"><i class="icon-basket"></i><span>Data Penjualan</span></a>
            </li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span> Data Kemas</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/kemas_pending">
                        <i class="icon-arrow-right"></i><span>List Kemas Pending</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/Penjualan_Seller">
                        <i class="icon-arrow-right"></i><span>List Kemas Approved</span></a>
                    </li>
                    
                </ul>
            </li>
            <li class="nav-level">Laporan</li>