            <li class="">
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Master/index_marketing"><i class="icon-speedometer"></i><span> Dashboard</span></a>
            </li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="index.html">
                <i class="icon-handbag"></i><span> Master Seller</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Master/seller">
                      <i class="icon-arrow-right"></i><span>Data Seller</span></a>
                    </li>
                    
                </ul>
            </li>
            <li class="nav-level">WHOLESALE</li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/penjualan_po_wajah"><i class="icon-basket"></i><span>Input Penjualan PAKET</span></a>
            </li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/penjualan_po"><i class="icon-basket"></i><span>Input Penjualan ECER</span></a>
            </li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span> Data Penjualan</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/Penjualan_Seller">
                        <i class="icon-arrow-right"></i><span>List Penjualan</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/Penjualan_Seller_Bayar">
                        <i class="icon-arrow-right"></i><span>Terima Pembayaran</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="#">
                        <i class="icon-arrow-right"></i><span>Pendingan Penjualan</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-level">RETAIL</li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/penjualan_ecer"><i class="icon-basket"></i><span>Input Penjualan RETAIL</span></a>
            </li>
            <li class="treeview">
              <a class="waves-effect waves-dark" href="#">
                <i class="icon-plus"></i><span> Data Penjualan</span><i class="icon-arrow-down"></i>
              </a>
                <ul class="treeview-menu">
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/Penjualan_Seller">
                        <i class="icon-arrow-right"></i><span>List Penjualan</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/Penjualan_Seller_Bayar">
                        <i class="icon-arrow-right"></i><span>Terima Pembayaran</span></a>
                    </li>
                    <li><a class="waves-effect waves-dark" href="#">
                        <i class="icon-arrow-right"></i><span>Pendingan Penjualan</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-level">Laporan</li>
            <li>
              <a class="waves-effect waves-dark" href="<?=base_url()?>Administrator/Penjualan/laporan_marketing"><i class="icon-printer"></i><span>LAPORAN MARKETING</span></a>
            </li>