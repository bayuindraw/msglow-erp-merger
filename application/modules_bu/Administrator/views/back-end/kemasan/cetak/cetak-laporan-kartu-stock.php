<link rel="stylesheet" type="text/css" href="http://165.22.105.2/assets/css/main.css">
<style type="text/css">
  .table1 {
    font-family: sans-serif;
    color: black;
    border-collapse: collapse;
  }
   
  .table1, th, td {
      border: 1px solid black;
      padding: 3px 10px;
  }
</style>
<?php
    
  $bulant = array(
                '01' => 'JANUARI',
                '02' => 'FEBRUARI',
                '03' => 'MARET',
                '04' => 'APRIL',
                '05' => 'MEI',
                '06' => 'JUNI',
                '07' => 'JULI',
                '08' => 'AGUSTUS',
                '09' => 'SEPTEMBER',
                '10' => 'OKTOBER',
                '11' => 'NOVEMBER',
                '12' => 'DESEMBER',
        );

   function DateTimeStamp() {
        date_default_timezone_set("Asia/Jakarta");
      $Data = date("d-m-Y h:i:s");
      return $Data ;
    } 
?>
<?php 
function String2Date($dTgl){
      //return 22-11-2012  
      list($cYear,$cMount,$cDate) = explode("-",$dTgl) ;
      if(strlen($cYear) == 4){
        $dTgl = $cDate . "-" . $cMount . "-" . $cYear ;
      } 
      return $dTgl ;  
    }
?>
<?php 

	$query = $this->model->code("SELECT * FROM v_kemasan_new WHERE id_kemasan = '".$barang."'");
	foreach ($query as $key => $vaData) {
		$namaKemasan = $vaData['nama_kemasan'];

	}


?>

<h2 align="center">Laporan Stock <?=$namaKemasan?> (<?=$vaData['kode_sup']?>)<br>Per Bulan <?=$bulant[$bulan]?> Tahun <?=$tahun?></h2>
Tanggal Cetak : <?php echo DateTimeStamp()?>
<table style="width:100%" class="table1">
  <thead>
      <tr>
        <th align="center">Tanggal</th>
        <th align="center">Jumlah Awal</th>
        <th align="center">Pemasukan</th>
        <th align="center">Pengeluaran</th>
        <th align="center">Jumlah Akhir</th>
      </tr>
    </thead>
    <tbody>
      <?php 
          $nJumlahHari = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
          for($i=1;$i<=$nJumlahHari;$i++){
            if($i > 9){
              $cNol = "";
            }else{
              $cNol = "0";
            }
          $date = $tahun."-".$bulan."-".$cNol.$i ;
          $no=0;

         
      ?>
       <tr>
         <td><?=String2Date($date)?></td>
         <td>
           <?php 
               $queryStock = $this->model->code("SELECT sum(jumlah) as total FROM v_terima_kemasan WHERE tgl_terima < '".String2Date($date)."' AND id_barang = '".$barang."'");
                foreach ($queryStock as $key => $vaAwal) {
                  $pembelian = $vaAwal['total'];
                }

              $queryKluar = $this->model->code("SELECT sum(jumlah) as total FROM detail_pengeluaran_kemasan WHERE tanggal < '".String2Date($date)."' AND id_barang = '".$barang."'");
              foreach ($queryKluar as $key => $vaAkhir) {
                  $pengeluaran = $vaAkhir['total'];
              }

              
              $total =  $pembelian-$pengeluaran;
           ?>
           <?php 
            $besok = mktime(0, 0, 0, date("m"), date("d")+1, date("Y"));
            if($date <  date('Y-m-d', $besok)){?>
              <b><?= $total ?></b>
           <?php }?>
         </td>
         <td>
           <?php 

             $queryAwal = $this->model->code("SELECT * FROM v_terima_kemasan WHERE tgl_terima = '".String2Date($date)."' AND id_barang = '".$barang."'");
              foreach ($queryAwal as $key => $vaAwal) {
                echo "".$vaAwal['nama_supplier']." : ".$vaAwal['jumlah']."<br>";
              }

              $queryTambah = $this->model->code("SELECT  sum(jumlah) as total FROM v_terima_kemasan WHERE tgl_terima = '".String2Date($date)."' AND id_barang = '".$barang."'");
              foreach ($queryTambah as $key => $vaAwal) {
                $pemasukanHari = $vaAwal['total'];
              }

           ?>
         </td>
         <td><?php 

             $queryAkhir = $this->model->code("SELECT * FROM detail_pengeluaran_kemasan WHERE tanggal = '".String2Date($date)."' AND id_barang = '".$barang."'");
              foreach ($queryAkhir as $key => $vaAkhir) {
                echo "".$vaAkhir['kode_factory']." : ".$vaAkhir['jumlah']."<br>";
              }

             $queryStockAkhir = $this->model->code("SELECT sum(jumlah) as total FROM detail_pengeluaran_kemasan WHERE tanggal = '".String2Date($date)."' AND id_barang = '".$barang."'");
              foreach ($queryStockAkhir as $key => $vaAkhir) {
                 $akhirStock = $vaAkhir['total'];
              }

           ?></td>
         <td> 
         	<?php 
            $besok = mktime(0, 0, 0, date("m"), date("d")+1, date("Y"));
            if($date <  date('Y-m-d', $besok)){?>
              <b><?= ($total + $pemasukanHari) - $akhirStock?></b>
           <?php }?></td>
       </tr>                  
      <?php } ?>
    </tbody>
  </table>