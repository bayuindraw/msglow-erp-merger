<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Form Input Pengeluaran Kemasan Print
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Stock_Act/add_edit_kemasan_detail_po_print/<?= $id_pokemasan ?>" method="Post">
                    <div class="form-group">
                        <label>Kode Purchase Order</label>
                        <input type="text" id="cKodePo" name="KodePurchaseOrder" class="form-control static" value="<?= $kode_pb ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Purchase Order</label>
                        <input type="text" id="dTglPo" name="TanggalOrder" class="form-control static floating-label 4IDE-date date_picker" value="<?= $tanggal ?>">
                    </div>
                    <div class="form-group">
                        <label>Pilih Supplier</label>
                        <?php
                        if ($num_rows > 0) { ?>
                            <?php
                            $query = $this->model->ViewWhere('supplier', 'id_supplier', $supplier);
                            foreach ($query as $key => $vaData) {
                                $vaSupplier = $vaData['nama_supplier'];
                            }
                            ?>
                            <input type="text" id="" name="" class="form-control static" value="<?= $vaSupplier ?>" readonly>
                            <input type="text" id="cKodePo" name="Supplier" class="form-control static" value="<?= $supplier ?>" hidden>
                        <?php } else { ?>
                            <select name="Supplier" id="pilihSupplier" class="form-control md-form-control md-static" required onchange="return changeValue(this.value)">

                                <option></option>
                                <?php

                                $query = $this->model->ViewAsc('supplier', 'id_supplier');
                                foreach ($query as $key => $vaSupplier) {
                                ?>
                                    <option value="<?= $vaSupplier['id_supplier'] ?>" <?php if ($supplier == $vaSupplier['id_supplier']) { ?> selected <?php } ?>><?= $vaSupplier['nama_supplier'] ?></option>

                                <?php } ?>
                            </select>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>Nama Barang</label>
                        <select name="NamaBarang" id="cIdStock" class="form-control static">

                            <option></option>
                            <?php
                            $query = $this->model->ViewAsc('kemasan', 'id_kemasan');
                            foreach ($query as $key => $vaKemasan) {
                            ?>
                                <option value="<?= $vaKemasan['id_kemasan'] ?>"><?= $vaKemasan['nama_kemasan'] ?> (<?= $vaKemasan['supplier'] ?>)</option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Jumlah Pengiriman (PCS)</label>
                        <input type="text" id="cJumlah" name="JumlahPo" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static">
                    </div>
                    <div class="md-input-wrapper">
                        <button type="submit" class="btn btn-primary waves-effect waves-light " title="Tambahkan Kemasan">
                            <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Tambahkan PO Kemasan</span>
                        </button>
                    </div>
                </form>

                <br>

                <div dir id="dir" content="table_stock">
                    <table class="table table-striped table-bordered nowrap" style="font-size: 13px">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Kemasan</th>
                                <th>Jumlah Order</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($row as $key => $vaData) {
                            ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $vaData['nama_kemasan'] ?></td>
                                    <td><?= number_format($vaData['jumlah']) ?> Pcs</td>
                                    <td>
                                        <a type="button" class="btn btn-danger waves-effect waves-light" href="<?= base_url() ?>Administrator/Stock_Act/hapus_detail_po_print/<?= $vaData['id_pokemasan'] ?>/<?= $id_pokemasan ?>"><i class="icofont icofont-ui-delete flaticon2-trash"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div id="btn-pb">
                    <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Stock_Act/history_print" method="Post">
                        <input type="hidden" name="tanggalpo" value="<?= $this->session->userdata('tanggal_po') ?>">
                        <input type="hidden" name="pabrik" value="<?= $this->session->userdata('factory') ?>">
                        <button type="submit" class="btn btn-success waves-effect waves-light">
                            SIMPAN PO KEMASAN
                        </button>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>