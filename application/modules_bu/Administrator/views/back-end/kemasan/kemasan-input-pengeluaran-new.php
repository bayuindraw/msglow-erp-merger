<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Form Input Pengeluaran Kemasan
          </h3>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">



      <div class="kt-portlet__body">


        <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Stock_Act/add_pengeluaran_detail_kemasan_new/<?= $action; ?>" method="Post">

          <div class="form-group">
            <label>Kode Purchase Order</label>
            <input type="text" id="cKodePo" name="KodePurchaseOrder" class="form-control md-form-control md-static" value="<?= $kodepo ?>">
          </div>
          <div class="form-group">
            <label>Tanggal Purchase Order</label>
            <input type="text" id="dTglPo" name="TanggalOrder" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= $this->session->userdata('tanggal_po') ?>" required>
            <input type="hidden" id="cIdOutlet" name="NamaOutlet" class="md-form-control md-static floating-label" value="1">
          </div>
          <div class="form-group">
            <label>Pilih Pabrik</label>
            <select name="Supplier" id="pilihPabrik" class="md-form-control md-static">

              <option></option>
              <?php
              $query = $this->model->ViewAsc('factory', 'id_factory');
              foreach ($query as $key => $vaSupplier) {
              ?>
                <option value="<?= $vaSupplier['id_factory'] ?>" <?php if ($this->session->userdata('factory') == $vaSupplier['id_factory']) { ?> selected <?php } ?>><?= $vaSupplier['nama_factory'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Nama Barang</label>
            <?php

            if (empty($this->session->userdata('supplier'))) {

            ?>
              <select name="NamaBarang" id="cIdStock" class="md-form-control md-static subkategori" required>

                <option></option>
                <?php
                $query = $this->model->ViewAsc('v_stock_kemasan_new', 'id_barang');
                foreach ($query as $key => $vaKemasan) {
                ?>
                  <option value="<?= $vaKemasan['id_barang'] ?>"><?= $vaKemasan['nama_kemasan'] ?> (<?= $vaKemasan['kode_sup'] ?>)</option>
                <?php } ?>
              </select>
            <?php } else { ?>
              <select name="NamaBarang" id="cIdStock" class="md-form-control md-static subkategori" required>

                <option></option>
                <?php
                $query = $this->model->ViewWhere('v_stock_kemasan_new', 'id_supplier', $this->session->userdata('supplier'));
                foreach ($query as $key => $vaKemasan) {
                ?>
                  <option value="<?= $vaKemasan['id_barang'] ?>"><?= $vaKemasan['nama_kemasan'] ?> (<?= $vaKemasan['kode_sup'] ?>)</option>
                <?php } ?>
              </select>
            <?php } ?>
          </div>
          <div class="form-group">
            <label>Jumlah Purchase Order (PCS)</label>
            <input type="text" id="cJumlah" name="JumlahPo" class="form-control" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="md-form-control md-static" required>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " title="Tambahkan Kemasan">
                <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Tambahkan Pengeluaran Kemasan</span>
              </button>
            </div>
          </div>
        </form>
      </div>



    </div>
  </div>
  <div class="col-lg-6 col-xl-6">
    <!--begin::Portlet-->
    <div class="kt-portlet">



      <div class="kt-portlet__body">


        <div dir id="dir" content="table_stock">
          <table class="table table-striped table-bordered nowrap" style="font-size: 13px">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kemasan</th>
                <th>Jumlah Order</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 0;
              foreach ($row as $key => $vaData) {
              ?>
                <tr>
                  <td><?= ++$no ?></td>
                  <td><?= $vaData['nama_kemasan'] ?></td>
                  <td><?= number_format($vaData['jumlah']) ?> Pcs</td>
                  <td>
                    <a type="button" class="btn btn-danger waves-effect waves-light" href="<?= base_url() ?>Administrator/Stock_Act/hapus_detail_kluar/<?= $vaData['id_pokemasan'] ?>/<?= $idkluar ?>"><i class="icofont icofont-ui-delete flaticon2-trash"></i></a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div id="btn-pb">
          <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Stock_Act/add_kluar_kemas/<?= $action; ?>" method="Post">
            <input type="hidden" name="tanggalpo" value="<?= $this->session->userdata('tanggal_po') ?>">
            <input type="hidden" name="pabrik" value="<?= $this->session->userdata('factory') ?>">
            <button type="submit" class="btn btn-success waves-effect waves-light">
              SIMPAN PENGELUARAN KEMASAN
            </button>
          </form>
        </div>
        <input type="hidden" name="aksi" value="simpan" id="aksi" name="aksi">
        <input type="hidden" name="hapus" value="" id="hapus" name="hapus">
      </div>



    </div>
  </div>
</div>