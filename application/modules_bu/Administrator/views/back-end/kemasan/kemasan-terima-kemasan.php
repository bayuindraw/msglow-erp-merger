<?php

foreach ($po as $key => $vaPO) {
  $totaluang = $vaPO['total_biaya'];
  $supplier = $vaPO['nama_supplier'];
}

/*$query = $this->model->code("SELECT sum(jumlah) as totalbayar FROM bayar_kemasan WHERE kode_po = '".$action."'");
      foreach ($query as $key => $vaBayar) {
        $totalbayar = $vaBayar['totalbayar'];
      }*/

$queryJum = $this->model->code("SELECT sum(jumlah) as totalbayar FROM terima_kemasan WHERE kode_po = '" . $action . "'");
foreach ($queryJum as $key => $vaBayar) {
  $totalterima = $vaBayar['totalbayar'];
}

?>
<div class="row">
  <div class="col-lg-12 col-xl-12">
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            PENERIMAAN KEMASAN DARI KODE PO : <?= $action ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
        <h6>Detail Purchase Order : </h6>
        <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
          <thead>
            <tr>
              <th>No</th>
              <th>Supplier</th>
              <th>Nama Kemasan</th>
              <th>Jumlah Pembelian</th>
              <th>Total Terima</th>
              <th>Kurang Terima</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 0;
            foreach ($row as $key => $vaData) {
              $queryJum = $this->model->code("SELECT sum(jumlah) as totalbayar FROM terima_kemasan WHERE kode_po = '" . $action . "' AND id_barang =  '" . $vaData['id_barang'] . "'");
              foreach ($queryJum as $key => $vaBayar) {
                $totalterima = $vaBayar['totalbayar'];
              }
            ?>
              <tr>
                <td><?= ++$no ?></td>
                <td><?= $vaData['nama_supplier'] ?></td>
                <td><?= $vaData['nama_kemasan'] ?></td>
                <td><?= number_format($vaData['jumlah']) ?> Pcs</td>
                <td><?= number_format($totalterima) ?> Pcs</td>
                <td><?= number_format($vaData['jumlah'] - $totalterima) ?> Pcs</td>
              </tr>
            <?php } ?>

          </tbody>
        </table>
        <hr>
        <h6>Detail Penerimaan Purchase Order : </h6>
        <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal Terima</th>
              <th>Nama Kemasan</th>
              <th>Jumlah</th>
              <th>Rusak</th>
              <th>Baik</th>
              <th>User</th>
              <th>#</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 0;
            foreach ($bayar as $key => $vaData) {
            ?>
              <tr>
                <td><?= ++$no ?></td>
                <td><?= $vaData['tgl_terima'] ?></td>
                <td><?= $vaData['nama_kemasan'] ?></td>
                <td><?= number_format($vaData['jumlah']) ?> Pcs</td>
                <td><?= number_format($vaData['rusak']) ?> Pcs</td>
                <td><?= number_format($vaData['baik']) ?> Pcs</td>
                <td><?= $vaData['user'] ?></td>
                <td>
                  <button class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" onclick="showEditKemasan('<?= $vaData['id_terima_kemasan'] ?>')">
                    <i class="fas fa-pencil-alt"></i><span class="m-l-10">Edit</span>
                  </button>
                </td>
              </tr>
            <?php } ?>

          </tbody>
        </table>
        <hr>
        <h5>Form Penerimaan Barang Dari Kode PO : <?= $action ?> | Supplier : <?= $supplier ?></h5>
        <hr>
        <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Stock_Act/terima_kemasan" method="post" novalidate>
          <div class="form-group">
            <i class="fas fa-barcode"></i>
            <label>Kode Purchase Order</label>
            <input type="text" id="cKodePo" name="KodePurchaseOrder" class="form-control md-static" value="<?= $action ?>" readonly>
          </div>
          <div class="form-group">
            <i class="far fa-calendar-alt"></i>
            <label>Tanggal Terima</label>
            <input type="text" id="dTglPo" name="TanggalOrder" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required>
          </div>
          <div class="form-group">
            <i class="fas fa-tag"></i>
            <label>Nama Barang</label>
            <select name="NamaBarang" id="cIdStock" class="form-control md-static" required>

              <option></option>
              <?php
              $query = $this->model->ViewWhere('v_detail_po_kemas', 'kode_pb', $action);
              foreach ($query as $key => $vaKemasan) {
              ?>
                <option value="<?= $vaKemasan['id_barang'] ?>"><?= $vaKemasan['nama_kemasan'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <i class="fas fa-tag"></i>
            <label>Jumlah Terima</label>
            <input type="text" class="form-control md-static" name="JumlahBayar" id="JumlahBayar" required>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Terima Kemasan">
                <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
              </button>
              <a href="<?= base_url() ?>Administrator/Stock_Act/selesai_kemasan/<?= $action; ?>"><button type="button" class="btn btn-warning waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Selesaikan PO">
                  <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Selesaikan PO</span>
                </button></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-paket" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Edit Penerimaan Stock Kemasan</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function showEditKemasan($idTerima) {
    $("#modal-paket").modal('show');
    $.ajax({
      type: "POST",

      url: "<?php echo base_url() ?>Administrator/Stock/tampil_edit_stock/" + $idTerima,
      cache: false,
      success: function(msg) {
        $(".modal-body").html(msg);

      }
    });
  }
</script>