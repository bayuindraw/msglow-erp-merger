<div class="row">
							<div class="col-lg-12 col-xl-12">							
									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Cetak Laporan Purchase Order Kemasan
												</h3>
											</div>
										</div>
</div>										
</div>										
								<div class="col-lg-6 col-xl-6">							
									<!--begin::Portlet-->
									<div class="kt-portlet">
										 
		
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													CETAK LAPORAN DETAIL ITEM PO KEMASAN
												</h3>
											</div>
										</div>
											<div class="kt-portlet__body">
									
									
										<form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock/cetak_laporan_po_kemasan_detail" method="Post" target="_blank">
										
												<div class="form-group">
													<select name="cBulan" id="pilihBulan" class="form-control">
                                 <option></option>
                                 <option value="01">Januari</option>
                                 <option value="02">Februari</option>
                                 <option value="03">Maret</option>
                                 <option value="04">April</option>
                                 <option value="05">Mei</option>
                                 <option value="06">Juni</option>
                                 <option value="07">Juli</option>
                                 <option value="08">Agustus</option>
                                 <option value="09">September</option>
                                 <option value="10">Oktober</option>
                                 <option value="11">November</option>
                                 <option value="12">Desember</option>
                               </select>
												</div>
												<div class="form-group">
													<select name="cTahun" id="pilihTahun" class="form-control" >
                                 <option></option>
                                 <option value="2019">2019</option>
                                 <option value="2020">2020</option>
                                 <option value="2021">2021</option>
                               </select>
												</div>
												
												<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                                 <i class="fas flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span>
												</div>
											</div>
												</form>
											</div>
											
											
		
</div>
</div>
<div class="col-lg-6 col-xl-6">							
									<!--begin::Portlet-->
									<div class="kt-portlet">
										 
		
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													CETAK LAPORAN KEMASAN KELUAR
												</h3>
											</div>
										</div>
											<div class="kt-portlet__body">
									
									
										<form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock/cetak_laporan_kluar_kemasan" method="Post" target="_blank">
										
												<div class="form-group">
													<select name="cBulan" id="pilihBulanDua" class="form-control">
                                 <option></option>
                                 <option value="01">Januari</option>
                                 <option value="02">Februari</option>
                                 <option value="03">Maret</option>
                                 <option value="04">April</option>
                                 <option value="05">Mei</option>
                                 <option value="06">Juni</option>
                                 <option value="07">Juli</option>
                                 <option value="08">Agustus</option>
                                 <option value="09">September</option>
                                 <option value="10">Oktober</option>
                                 <option value="11">November</option>
                                 <option value="12">Desember</option>
                               </select>
												</div>
												<div class="form-group">
													<select name="cTahun" id="pilihTahunDua" class="form-control" >
                                 <option></option>
                                 <option value="2019">2019</option>
                                 <option value="2020">2020</option>
                                 <option value="2021">2021</option>
                               </select>
												</div>
												
												<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-success waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Tampilkan Stock">
                                 <i class="fas flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span></button>
												</div>
											</div>
												</form>
											</div>
											
											
		
</div>
</div>
<div class="col-lg-6 col-xl-6">							
									<!--begin::Portlet-->
									<div class="kt-portlet">
										 
		
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													CETAK LAPORAN PENERIMAAN KEMASAN
												</h3>
											</div>
										</div>
											<div class="kt-portlet__body">
									
									
										<form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock/cetak_laporan_penerimaan" method="Post" target="_blank">
										
												<div class="form-group">
													<select name="cSupplier" id="pilihSupplier" class="form-control">
                                 <option value=""></option>
                                 <option value="ALL">SEMUA SUPPLIER</option>
                                 <?php
                                    $query = $this->model->ViewAsc('supplier','id_supplier');
                                    foreach ($query as $key => $vaSupplier) {
                                  ?>
                                  <option value="<?=$vaSupplier['id_supplier']?>" <?php if($this->session->userdata('supplier') == $vaSupplier['id_supplier']){?> selected <?php } ?>><?=$vaSupplier['nama_supplier']?></option>
                                   
                                 <?php } ?>
                               </select>
												</div>
												<div class="form-group">
													<select name="cBulan" id="pilihBulanDua" class="form-control pilihbulan">
                                 <option></option>
                                 <option value="01">Januari</option>
                                 <option value="02">Februari</option>
                                 <option value="03">Maret</option>
                                 <option value="04">April</option>
                                 <option value="05">Mei</option>
                                 <option value="06">Juni</option>
                                 <option value="07">Juli</option>
                                 <option value="08">Agustus</option>
                                 <option value="09">September</option>
                                 <option value="10">Oktober</option>
                                 <option value="11">November</option>
                                 <option value="12">Desember</option>
                               </select>
												</div>
												<div class="form-group">
													<select name="cTahun" id="pilihTahunDua" class="form-control pilihtahun" >
                                 <option></option>
                                 <option value="2019">2019</option>
                                 <option value="2020">2020</option>
                                 <option value="2021">2021</option>
                               </select>
												</div>
												
												<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-warning waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Tampilkan Stock" onclick="showKartuStok()">
                                 <i class="fas flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span></button>
												</div>
											</div>
												</form>
											</div>
											
											
		
</div>
</div>
</div>