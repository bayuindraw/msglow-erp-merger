
<!DOCTYPE html>
<html lang="en">
<head>
	<title>MSGLOW ERP SISTEM</title>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="description" content="Phoenixcoded">
	<meta name="keywords"
		  content=", Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
	<meta name="author" content="Phoenixcoded">

	<!-- Favicon icon -->
	<link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
	<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

	<!-- Google font-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!--ico Fonts-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/icon/icofont/css/icofont.css">

	<!-- Required Fremwork -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap.min.css">

	<!-- waves css -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/plugins/waves/css/waves.min.css">

	<!-- Style.css -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/main.css">

	<!-- Responsive.css-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/responsive.css">

	<!--color css-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/color/color-1.css" id="color"/>

</head>
<body>
<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
	<!-- Container-fluid starts -->
	<div class="container-fluid">
		<div class="row">

			<div class="col-sm-12">
				<div class="login-card card-block">
					
						<div class="text-center">
							<img src="<?=base_url()?>assets/images/logo-kotak.png" style="width: 50%;">
						</div>
						<h3 class="text-center txt-primary">
							ERP SYSTEM MSGLOW
						</h3>
						<form method="post" action="<?=base_url()?>Administrator/Master_Act/signin">
							<div class="md-input-wrapper">
								<input type="text" class="md-form-control" name="username" />
								<label>Username</label>
							</div>
							<div class="md-input-wrapper">
								<input type="password" class="md-form-control" name="pass" />
								<label>Password</label>
								<br>
								<label><?=$notif?></label>
								<br>
							</div>
							
							<div class="row">
								<div class="col-xs-10 offset-xs-1">
									<button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">LOGIN</button>
								</div>
							</div>
						<!-- <div class="card-footer"> -->
						</form>

						
					<!-- end of form -->
				</div>
				<!-- end of login-card -->
			</div>
			<!-- end of col-sm-12 -->
		</div>
		<!-- end of row -->
	</div>
	<!-- end of container-fluid -->
</section>


<!-- Warning Section Ends -->
<!-- Required Jqurey -->
<script src="<?=base_url()?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery-ui.min.js"></script>
<!-- tether.js -->
<script src="<?=base_url()?>assets/js/tether.min.js"></script>
<!-- waves effects.js -->
<script src="<?=base_url()?>assets/plugins/waves/js/waves.min.js"></script>
<!-- Required Framework -->
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="<?=base_url()?>assets/pages/elements.js"></script>



</body>
</html>