<style type="text/css">
  .table1 {
    font-family: sans-serif;
    color: black;
    border-collapse: collapse;
  }
   
  .table1, th, td {
      border: 1px solid black;
      padding: 3px 10px;
  }
</style>
<?php 
function String2Date($dTgl){
      //return 22-11-2012  
      list($cYear,$cMount,$cDate) = explode("-",$dTgl) ;
      if(strlen($cYear) == 4){
        $dTgl = $cDate . "-" . $cMount . "-" . $cYear ;
      } 
      return $dTgl ;
    }
?>
<h3 align="center">LAPORAN PENJUALAN HARIAN</h3>
<h5 align="center">Penjualan Produk Pada Tanggal : <?=$tanggal?></h5>

<table style="width:100%;font-size: 12px" class="table1">
	<tr style="background-color: #95fffd" align="center">
		<td>No</td>
		<td>Kode Jual</td>
		<td>Nama Customer</td>
		<td>Nama Barang</td>
		<td>Jumlah</td>
		<td>Harga</td>
		<td>Ongkir</td>
		<td>Total</td>
		<td>Bank</td>
		<td>Keterangan</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<?php
		$no = 0;
		foreach ($row as $key => $vaData) {
	?>
	<tr style="background-color: #95fffd">
		<td><?=++$no?></td>
		<td><?=$vaData['kode_penjualan']?></td>
		<td><?=$vaData['nama']?></td>
		<td></td>
		<td></td>
		<td></td>
		<td><?=$vaData['ongkir']?></td>
		<td>Rp. <?=number_format($vaData['total_bayar'])?></td>
		<td><?=$vaData['bank']?></td>
		<td></td>
	</tr>
		<?php 
			$query =  $this->model->code("SELECT * FROM v_detail_jual WHERE kode_penjualan = '".$vaData['kode_penjualan']."'");
			foreach ($query as $key => $vaRow) {
		?>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td><?=$vaRow['nama_produk']?></td>
			<td><?=$vaRow['jumlah']?></td>
			<td>Rp. <?=number_format($vaRow['harga_jadi'])?></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<?php 
			}
		?>	
	<?php 
		}
	?>
</table>

<script>
	window.print();
</script>