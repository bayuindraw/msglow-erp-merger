<style type="text/css">
  .table1 {
    font-family: sans-serif;
    color: black;
    border-collapse: collapse;
  }
   
  .table1, th, td {
      border: 1px solid black;
      padding: 3px 10px;
  }
</style>
<?php 
	function String2Date($dTgl){
      //return 22-11-2012  
      list($cYear,$cMount,$cDate) = explode("-",$dTgl) ;
      if(strlen($cYear) == 4){
        $dTgl = $cDate . "-" . $cMount . "-" . $cYear ;
      } 
      return $dTgl ;  
    }
    $bulan = array(
                '01' => 'JANUARI',
                '02' => 'FEBRUARI',
                '03' => 'MARET',
                '04' => 'APRIL',
                '05' => 'MEI',
                '06' => 'JUNI',
                '07' => 'JULI',
                '08' => 'AGUSTUS',
                '09' => 'SEPTEMBER',
                '10' => 'OKTOBER',
                '11' => 'NOVEMBER',
                '12' => 'DESEMBER',
        );
?>
<h3 align="center">LAPORAN PENJUALAN SELLER <?=$seller?></h3>
<h5 align="center">PERIODE <?=$bulan[$mbulan]?> Tahun <?=$tahun?></h5>

<table style="width:100%;font-size: 11px" class="table1">
	<tr style="background-color: #95fffd" align="center">
		<td>No</td>
		<td>Kode Penjualan</td>
		<td>Nama Barang</td>
		<td>Harga</td>
		<td>Diskon</td>
		<td>Harga Jadi</td>
		<td>Jumlah</td>
	</tr>
	<?php 
		$no=0;
		foreach ($row as $key => $vaData) {
	?>
		<tr>
		<td><?=++$no?></td>
		<td><?=$vaData['kode_jual']?></td>
		<td><?=$vaData['nama_produk']?></td>
		<td>Rp. <?=number_format($vaData['harga_satuan'])?></td>
		<td>Rp. <?=number_format($vaData['diskon'])?></td>
		<td>Rp. <?=number_format($vaData['harga_jadi'])?></td>
		<td><?=number_format($vaData['jumlah'])?></td>
		</tr>
	<?php } ?>
</table>

<script>
  window.print();
</script>