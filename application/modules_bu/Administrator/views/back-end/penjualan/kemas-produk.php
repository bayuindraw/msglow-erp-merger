<?php

foreach ($po as $key => $vaPO) {
  $totaluang = $vaPO['total_biaya'];
  $supplier = $vaPO['nama_supplier'];
}

$query = $this->model->code("SELECT sum(jumlah) as totalbayar FROM bayar_kemasan WHERE kode_po = '" . $action . "'");
foreach ($query as $key => $vaBayar) {
  $totalbayar = $vaBayar['totalbayar'];
}

?>

<div class="row">
  <div class="col-lg-12 col-xl-12">
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            PENGEMASAN PRODUK KODE PENJUALAN : <?= $action ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
        <h6>Detail Pembelian Produk : </h6>
        <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Produk</th>
              <th>Jumlah Pembelian</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 0;
            foreach ($po as $key => $vaData) {
            ?>
              <tr>
                <td><?= ++$no ?></td>
                <td><?= $vaData['nama_produk'] ?></td>
                <td><?= number_format($vaData['jumlah']) ?> Pcs</td>
              </tr>
            <?php } ?>

          </tbody>
        </table>
        <hr>
        <h6>Detail Pengiriman Termin Penjualan : <?= $action ?></h6>
        <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal Pengiriman</th>
              <th>Nama Produk</th>
              <th>Jumlah</th>
              <th>User</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 0;
            foreach ($detail as $key => $vaData) {
            ?>
              <tr>
                <td><?= ++$no ?></td>
                <td><?= $vaData['tanggal'] ?></td>
                <td><?= $vaData['nama_produk'] ?></td>
                <td><?= number_format($vaData['jumlah']) ?> Pcs</td>
                <td><?= $vaData['created_by'] ?></td>
              </tr>
            <?php } ?>

          </tbody>
        </table>
        <hr>
        <h5>Form Pengiriman Produk : <?= $action ?></h5>
        <hr>
        <form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Penjualan_Act/packing_produk" method="post" novalidate>
          <div class="form-group">
            <label>Tanggal Pengemasan</label>
            <input type="text" id="dTglPo" name="TanggalOrder" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required>
          </div>
          <div class="form-group">
            <label>Nama Barang</label>
            <select name="NamaBarang" id="cIdStock" class="form-control md-static" required>

              <option></option>
              <?php
              $query = $this->model->ViewWhere('v_detail_produk_jual', 'kode_jual', $action);
              foreach ($query as $key => $vaKemasan) {
              ?>
                <option value="<?= $vaKemasan['id_barang'] ?>"><?= $vaKemasan['nama_produk'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Jumlah Kemas</label>
            <input type="text" class="form-control md-static" name="JumlahBayar" id="JumlahBayar" required>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk">
                <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-paket" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Edit Penerimaan Stock Barang Jadi</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function showEditKemasan($idTerima) {
    $("#modal-paket").modal('show');
    $.ajax({
      type: "POST",

      url: "<?php echo base_url() ?>Administrator/Stock/tampil_edit_stock_jadi/" + $idTerima,
      cache: false,
      success: function(msg) {
        $(".modal-body").html(msg);

      }
    });
  }
</script>