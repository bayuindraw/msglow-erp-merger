<div class="kt-portlet kt-portlet--mobile">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Data Penjualan
      </h3>
    </div>
  </div>
  <div class="kt-portlet__body">

    <!--begin: Datatable -->
    <div dir id="dir" content="table">
      <table class="table table-striped table-bordered nowrap">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Penjualan</th>
            <th>Kode Seller</th>
            <th>Nama Seller</th>
            <th>Tanggal</th>
            <th>Item Produk</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 0;
          foreach ($row as $key => $vaData) {

            $query = $this->model->code("SELECT count(id_detail_jual) as total FROM pejualan_detail_seller WHERE kode_jual = '" . $vaData['kode_penjualan'] . "'");
            foreach ($query as $key => $vaTotal) {
              $total = $vaTotal['total'];
            }
          ?>
            <tr>
              <td><?= ++$no ?></td>
              <td><?= $vaData['kode_penjualan'] ?></td>
              <td><?= $vaData['kode'] ?></td>
              <td><?= $vaData['nama'] ?></td>
              <td><?= $vaData['tgl_jual'] ?></td>
              <td><?= $total ?> Produk</td>
              <td>
                <?php

                if ($vaData['approved_by']  == '') {
                  $kemasan  = 'PENDING';
                  $label    = 'btn btn-danger';
                } else {
                  $kemasan  = 'DITERIMA';
                  $label    = 'btn btn-success';
                }

                ?>
                <strong class="label <?= $label ?>"><?= $kemasan ?></strong>
              </td>

              <td>

                <?php
                if (!empty($vaData['approved_by'])) {
                ?>
                  <button type="button" class="btn btn-success waves-effect waves-light" title="Terima Order"><i class="fas fa-print"></i></button>
                <?php } else { ?>
                  <button type="button" class="btn btn-primary waves-effect waves-light" title="Terima Order" onclick="hapus(<?= $vaData['id_pengiriman'] ?>)"><i class="fas fa-check"></i></button>
                <?php } ?>
              </td>

            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <!--end: Datatable -->
  </div>
</div>

<script type="text/javascript">
  function hapus(id){
  swal({
      title: "Apakah Yakin Konfirmasi Pengemasan Ini  ?",
      text: "Pastikan data yang di Konfirmasi benar",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Ya , Konfirmasi Penjualan",
      closeOnConfirm: false
    },function () {
      $.ajax({
        type: "GET",  
        url: "<?=base_url()?>Administrator/Penjualan_Act/kemasan_konfirmasi/"+id,
        cache: false,
        success:function(data){
          swal.close();
          $('#hapus').val('true');
          toastr.success('Sukses!','Berhasil Pengemasan Produk');
          setTimeout(function(){// wait for 5 secs(2)
           location.reload(); // then reload the page.(3)
            }, 1000); 
        },
        complete: function() {
          $('#aksi').val('simpan');
          //window.alert('sukses');
        }
      });
   },
   function(){});
  
}
</script>