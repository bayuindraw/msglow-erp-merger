<div class="kt-portlet kt-portlet--mobile">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Data Penjualan
      </h3>
    </div>
  </div>
  <div class="kt-portlet__body">

    <!--begin: Datatable -->
    <div dir id="dir" content="table">
      <table class="table table-striped table-bordered nowrap">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Penjualan</th>
            <th>Kode Seller</th>
            <th>Nama Seller</th>
            <th>Tanggal</th>
            <th>Item Produk</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 0;
          foreach ($row as $key => $vaData) {

            $query = $this->model->code("SELECT count(id_detail_jual) as total FROM pejualan_detail_seller WHERE kode_jual = '" . $vaData['kode_penjualan'] . "'");
            foreach ($query as $key => $vaTotal) {
              $total = $vaTotal['total'];
            }
          ?>
            <tr>
              <td><?= ++$no ?></td>
              <td><?= $vaData['kode_penjualan'] ?></td>
              <td><?= $vaData['kode'] ?></td>
              <td><?= $vaData['nama'] ?></td>
              <td><?= $vaData['tgl_jual'] ?></td>
              <td><?= $total ?> Produk</td>
              <td>
                <?php

                if ($vaData['approved_by']  == '') {
                  $kemasan  = 'PENDING';
                  $label    = 'btn btn-danger';
                } else {
                  $kemasan  = 'DITERIMA';
                  $label    = 'btn btn-success';
                }

                ?>
                <strong class="label <?= $label ?>"><?= $kemasan ?></strong>
              </td>
              <td><a href="<?= base_url() ?>Administrator/Penjualan/kemas_produk/<?= $vaData['kode_penjualan'] ?>" class="btn btn-primary waves-effect waves-light"><i class="fa flaticon2-delivery-package"></i>Kemas</a></td>

            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <!--end: Datatable -->
  </div>
</div>