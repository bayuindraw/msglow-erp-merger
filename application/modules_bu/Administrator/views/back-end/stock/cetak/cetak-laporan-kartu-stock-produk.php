<link rel="stylesheet" type="text/css" href="http://165.22.105.2/assets/css/bootstrap.min.css">

<style type="text/css">
	body {
    	font-family: sans-serif;
	}
	table {
    border-collapse: collapse;
	}
	thead {
	    vertical-align: bottom;
	    text-align: center;
	    font-weight: bold;
	}
	tfoot {
	    text-align: center;
	    font-weight: bold;
	}
	th {
	    text-align: left;
	    padding-left: 0.35em;
	    padding-right: 0.35em;
	    padding-top: 0.35em;
	    padding-bottom: 0.35em;
	    vertical-align: top;
	}
	td {
	    padding-left: 0.35em;
	    padding-right: 0.35em;
	    padding-top: 0.35em;
	    padding-bottom: 0.35em;
	    vertical-align: top;
	}
</style>
<?php 
function String2Date($dTgl){
      //return 22-11-2012  
      list($cYear,$cMount,$cDate) = explode("-",$dTgl) ;
      if(strlen($cYear) == 4){
        $dTgl = $cDate . "-" . $cMount . "-" . $cYear ;
      } 
      return $dTgl ;  
    }
?>
<?php 

	$query = $this->model->code("SELECT * FROM produk WHERE id_produk = '".$barang."'");
	foreach ($query as $key => $vaData) {
		$namaKemasan = $vaData['nama_produk'];

	}


?>

<h2 align="center">Laporan Stock <?=$namaKemasan?> (<?=$vaData['kode_sup']?>)<br>Per Bulan <?=$bulan?> Tahun <?=$tahun?></h2>
Tanggal Cetak : <?php echo date('d-m-Y h:i:s')?>
<table style="width:100%" class="table dt-responsive nowrap table-small-font table-bordered table-striped" style="color:black;border-color: black;font-size: 11px">
  <thead>
      <tr>
        <th align="center">Tanggal</th>
        <th align="center">Jumlah Awal</th>
        <th align="center">Pemasukan</th>
        <th align="center">Pengeluaran</th>
        <th align="center">Jumlah Akhir</th>
      </tr>
    </thead>
    <tbody>
      <?php 
          $nJumlahHari = cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);
          for($i=1;$i<=$nJumlahHari;$i++){
            if($i > 9){
              $cNol = "";
            }else{
              $cNol = "0";
            }
          $date = $tahun."-".$bulan."-".$cNol.$i ;
          $no=0;

         
      ?>
       <tr>
         <td><?=String2Date($date)?></td>
         <td>
           <?php 
               $queryStock = $this->model->code("SELECT sum(jumlah) as total FROM v_terima_kemasan WHERE tgl_terima < '".String2Date($date)."' AND id_barang = '".$barang."'");
                foreach ($queryStock as $key => $vaAwal) {
                  $pembelian = $vaAwal['total'];
                }

              $queryKluar = $this->model->code("SELECT sum(jumlah) as total FROM detail_pengeluaran_kemasan WHERE tanggal < '".String2Date($date)."' AND id_barang = '".$barang."'");
              foreach ($queryKluar as $key => $vaAkhir) {
                  $pengeluaran = $vaAkhir['total'];
              }

              
              $total =  $pembelian-$pengeluaran;
           ?>
           <?php 
            $besok = mktime(0, 0, 0, date("m"), date("d")+1, date("Y"));
            if($date <  date('Y-m-d', $besok)){?>
              <b><?= $total ?></b>
           <?php }?>
         </td>
         <td>
           <?php 

             $queryAwal = $this->model->code("SELECT * FROM v_terima_kemasan WHERE tgl_terima = '".String2Date($date)."' AND id_barang = '".$barang."'");
              foreach ($queryAwal as $key => $vaAwal) {
                echo "".$vaAwal['nama_supplier']." : ".$vaAwal['jumlah']."<br>";
              }

              $queryTambah = $this->model->code("SELECT  sum(jumlah) as total FROM v_terima_kemasan WHERE tgl_terima = '".String2Date($date)."' AND id_barang = '".$barang."'");
              foreach ($queryTambah as $key => $vaAwal) {
                $pemasukanHari = $vaAwal['total'];
              }

           ?>
         </td>
         <td><?php 

             $queryAkhir = $this->model->code("SELECT * FROM detail_pengeluaran_kemasan WHERE tanggal = '".String2Date($date)."' AND id_barang = '".$barang."'");
              foreach ($queryAkhir as $key => $vaAkhir) {
                echo "".$vaAkhir['kode_factory']." : ".$vaAkhir['jumlah']."<br>";
              }

             $queryStockAkhir = $this->model->code("SELECT sum(jumlah) as total FROM detail_pengeluaran_kemasan WHERE tanggal = '".String2Date($date)."' AND id_barang = '".$barang."'");
              foreach ($queryStockAkhir as $key => $vaAkhir) {
                 $akhirStock = $vaAkhir['total'];
              }

           ?></td>
         <td> 
         	<?php 
            $besok = mktime(0, 0, 0, date("m"), date("d")+1, date("Y"));
            if($date <  date('Y-m-d', $besok)){?>
              <b><?= ($total + $pemasukanHari) - $akhirStock?></b>
           <?php }?></td>
       </tr>                  
      <?php } ?>
    </tbody>
  </table>