
<link rel="stylesheet" type="text/css" href="http://165.22.105.2/assets/css/main.css">
<style type="text/css">
	.table1 {
    font-family: sans-serif;
    color: black;
    border-collapse: collapse;
	}
	 
	.table1, th, td {
	    border: 1px solid black;
	    padding: 3px 10px;
	}
</style>
<?php
		
	$bulan = array(
                '01' => 'JANUARI',
                '02' => 'FEBRUARI',
                '03' => 'MARET',
                '04' => 'APRIL',
                '05' => 'MEI',
                '06' => 'JUNI',
                '07' => 'JULI',
                '08' => 'AGUSTUS',
                '09' => 'SEPTEMBER',
                '10' => 'OKTOBER',
                '11' => 'NOVEMBER',
                '12' => 'DESEMBER',
        );

	 function DateTimeStamp() {
   			date_default_timezone_set("Asia/Jakarta");
			$Data = date("d-m-Y h:i:s");
			return $Data ;
		} 
?>
<h2 align="center" style="color:black">LAPORAN STOK BAHAN KEMAS MS GLOW</h2>
<h3 align="center" style="color:black"><i>PERIODE <?=$bulan[date('m')]?></i></h3>

<span align="right">Tanggal Cetak : <?=DateTimeStamp()?></span>
<table border="1" style="width:100%" class="table1">
	<tr>
		<td align="center" style="width:50%">Nama Barang</td>
		<td align="center" style="width:15%">Klasifikasi</td>
		<td align="center" style="width:15%">Stock Barang Rusak</td>
		<td align="center" style="width:15%">Sisa Stock</td>
	</tr>
	<?php 

		foreach ($row as $key => $vaData) {
		if($vaData['jumlah'] < '5000') {
			$color = '#f08080';
		}else{
			$color= 'white';
		}	
	?>
	<tr>
		<td><?=$vaData['nama_kemasan']?> (<?=$vaData['kode_sup']?>)</td>
		<td align="center"><?=$vaData['kategori']?></td>
		<td align="center"><?=number_format($vaData['rusak'])?></td>
		<td align="center" style="background-color: <?=$color?>"><?=number_format($vaData['jumlah'])?></td>
	</tr>
   <?php }?>
</table>