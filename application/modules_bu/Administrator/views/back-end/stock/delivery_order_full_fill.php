<!--<div class="main-header">
              <h4><?= $menu ?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Stock</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Stock Opname Kemasan</a>
                    </li>
                </ol>
            </div>-->
<div class="kt-portlet kt-portlet--mobile">


  <!--<div class="card-header"><h5 class="card-header-text">STOCK REAL KEMASAN</h5></div>-->
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <!--<span class="kt-portlet__head-icon">
                      <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>-->
      <h3 class="kt-portlet__head-title">
       TOTAL DELIVERY PRODUCT BULAN <?=strtoupper(date('M'))?>
      </h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
          <!-- <a href="<?= base_url() ?>Laporan2/daily_stock" class="btn btn-primary waves-effect waves-light" target="_blank"> <i class="la la-print"></i> <span class="m-l-10">Cetak Stock</span></a> -->
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">

    <!--begin: Datatable -->
    <div dir id="dir" content="table">
      
      <table class="table table-striped table-bordered nowrap" id="mytable_barang">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Produk</th>
            <th>Nama Produk</th>
            <th>Total Pengiriman</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 0;
          
          foreach ($row as $key => $vaData) {
           
          ?>
            <tr>
              <td><?= ++$no ?></td>
              <td><?= $vaData['product_code'] ?></td>
              <td><a href="<?=base_url()?>Administrator/Stock_Produk/delivery_order_fullfill_tingkat/<?= $vaData['product_id'] ?>"><?= $vaData['sku_name'] ?></td>
              <td><?= number_format($vaData['send_quantity']) ?></td>

            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <!--end: Datatable -->
  </div>
</div>