<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$file?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">UI Elements</a>
                    </li>
                    <li class="breadcrumb-item"><a href="panels-wells.html">Panels Wells</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <section class="panels-wells">
                <!-- Row start -->
                <div class="row">
                  <div class="col-lg-12">
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-header"><h5 class="card-header-text">Form Input & Data <?=$file?></h5></div>
                          <div class="card-block ">
                            <div class=" col-xs-12 col-sm-3">
                             <div class="panel panel-primary">
                              <div class="panel-heading bg-primary">
                                Pilih Kategori
                              </div>
                              <div class="panel-body">
                              
                                <div class="md-input-wrapper">
                               <select name="cBulan" id="pilihBulan" ng-model="cBulan"  class="form-control">
                                 <option></option>
                                 <option value="01">Januari</option>
                                 <option value="02">Februari</option>
                                 <option value="03">Maret</option>
                                 <option value="04">April</option>
                                 <option value="05">Mei</option>
                                 <option value="06">Juni</option>
                                 <option value="07">Juli</option>
                                 <option value="08">Agustus</option>
                                 <option value="09">September</option>
                                 <option value="10">Oktober</option>
                                 <option value="11">November</option>
                                 <option value="12">Desember</option>
                               </select>
                              </div>
                              <div class="md-input-wrapper">
                               <select name="cTahun" id="pilihTahun" class="form-control" >
                                 <option></option>
                                 <option value="2019">2019</option>
                                 <option value="2020">2020</option>
                                 <option value="2021">2021</option>
                               </select>
                              </div>
                              </div>
                              <div class="panel-footer txt-primary">
                                 <button type="button" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Tampilkan Stock" onclick="showKartuStok()">
                                 <i class="icon-book-open"></i><span class="m-l-10">Tampilkan Stock </span>
                              </div>
                            </div>
                           </div>
                           <div class=" col-xs-12 col-sm-9">
                             <div class="panel panel-primary">
                              <div class="panel-heading bg-primary">
                                Data Stock Barang
                              </div>
                              <div class="panel-body">
                                <div content="table" id="show_kartu">
                                  
                                </div> 
                              </div>
                              <div class="panel-footer txt-primary">
                                Monitoring
                              </div>
                            </div>
                           </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Row end -->
                </section>
            </div>
          </div>
        </div>
      </div>
      <script type="text/javascript">
        function showKartuStok(){
          var cBulan   = $('#pilihBulan').val();
          var cTahun   = $('#pilihTahun').val();
          $.ajax({
                  type:"POST",
                  data:"bulan="+cBulan+
                       "&tahun="+cTahun,
                  url: "<?php echo site_url('Administrator/Stock/tampil_stock')?>",
                  cache: false,
                  success:function(msg){
                    $('#show_kartu').html(msg);
                  }
              });
          }
       </script>