<div class="content-wrapper">
        <!-- Container-fluid starts -->
       <div class="container-fluid">
      <!-- Main content starts -->
      <div>
        <div class="row">
          <div class="col-xl-12 p-0">
            <div class="main-header">
              <h4><?=$menu?></h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="#"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Laporan</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Kemasan</a>
                    </li>
                </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <section class="panels-wells">
                <!-- Row start -->
                <div class="row">
                  <div class="col-lg-12">
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-header"><h5 class="card-header-text">Cetak Laporan Purchase Order Kemasan</h5></div>
                          <div class="card-block ">
                            <div class="row">
                            <div class=" col-xs-6 col-sm-6">
                             <div class="panel panel-primary">
                              <div class="panel-heading bg-primary">
                                CETAK LAPORAN DETAIL ITEM PO KEMASAN
                              </div>
                              <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock/cetak_laporan_po_kemasan_detail" method="Post" target="_blank">
                              <div class="panel-body">
                              
                                <div class="md-input-wrapper">
                               <select name="cBulan" id="pilihBulan" class="form-control">
                                 <option></option>
                                 <option value="01">Januari</option>
                                 <option value="02">Februari</option>
                                 <option value="03">Maret</option>
                                 <option value="04">April</option>
                                 <option value="05">Mei</option>
                                 <option value="06">Juni</option>
                                 <option value="07">Juli</option>
                                 <option value="08">Agustus</option>
                                 <option value="09">September</option>
                                 <option value="10">Oktober</option>
                                 <option value="11">November</option>
                                 <option value="12">Desember</option>
                               </select>
                              </div>
                              <div class="md-input-wrapper">
                               <select name="cTahun" id="pilihTahun" class="form-control" >
                                 <option></option>
                                 <option value="2019">2019</option>
                                 <option value="2020">2020</option>
                                 <option value="2021">2021</option>
                               </select>
                              </div>
                              </div>
                              <div class="panel-footer txt-primary">
                                 <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                                 <i class="icon-printer "></i><span class="m-l-10">Cetak Laporan</span>
                              </div>
                              </form>
                            </div>
                           </div>
                            <div class=" col-xs-6 col-sm-6">
                             <div class="panel panel-success">
                              <div class="panel-heading bg-success">
                                CETAK LAPORAN KEMASAN KLUAR
                              </div>
                              <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock/cetak_laporan_kluar_kemasan" method="Post" target="_blank">
                              <div class="panel-body">
                                <div class="md-input-wrapper">
                               <select name="cBulan" id="pilihBulanDua" class="form-control">
                                 <option></option>
                                 <option value="01">Januari</option>
                                 <option value="02">Februari</option>
                                 <option value="03">Maret</option>
                                 <option value="04">April</option>
                                 <option value="05">Mei</option>
                                 <option value="06">Juni</option>
                                 <option value="07">Juli</option>
                                 <option value="08">Agustus</option>
                                 <option value="09">September</option>
                                 <option value="10">Oktober</option>
                                 <option value="11">November</option>
                                 <option value="12">Desember</option>
                               </select>
                              </div>
                              <div class="md-input-wrapper">
                               <select name="cTahun" id="pilihTahunDua" class="form-control" >
                                 <option></option>
                                 <option value="2019">2019</option>
                                 <option value="2020">2020</option>
                                 <option value="2021">2021</option>
                               </select>
                              </div>
                              </div>
                              <div class="panel-footer txt-success">
                                 <button type="submit" class="btn btn-success waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Tampilkan Stock" onclick="showKartuStok()">
                                 <i class="icon-printer "></i><span class="m-l-10">Cetak Laporan</span></button>
                              </div>
                            </form>
                            </div>
                           </div>
                          </div>
                          <div class="row" style="margin-top:10px">
                           <div class=" col-xs-6 col-sm-6">
                             <div class="panel panel-success">
                              <div class="panel-heading bg-success">
                                CETAK LAPORAN PENERIMAAN KEMASAN
                              </div>
                              <form id="main" class="form-horizontal" action="<?=base_url()?>Administrator/Stock/cetak_laporan_kluar_kemasan" method="Post" target="_blank">
                              <div class="panel-body">
                                <div class="md-input-wrapper">
                               <select name="cBulan" id="pilihBulanDua" class="form-control">
                                 <option></option>
                                 <option value="01">Januari</option>
                                 <option value="02">Februari</option>
                                 <option value="03">Maret</option>
                                 <option value="04">April</option>
                                 <option value="05">Mei</option>
                                 <option value="06">Juni</option>
                                 <option value="07">Juli</option>
                                 <option value="08">Agustus</option>
                                 <option value="09">September</option>
                                 <option value="10">Oktober</option>
                                 <option value="11">November</option>
                                 <option value="12">Desember</option>
                               </select>
                              </div>
                              <div class="md-input-wrapper">
                               <select name="cTahun" id="pilihTahunDua" class="form-control" >
                                 <option></option>
                                 <option value="2019">2019</option>
                                 <option value="2020">2020</option>
                                 <option value="2021">2021</option>
                               </select>
                              </div>
                              </div>
                              <div class="panel-footer txt-success">
                                 <button type="submit" class="btn btn-success waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Tampilkan Stock" onclick="showKartuStok()">
                                 <i class="icon-printer "></i><span class="m-l-10">Cetak Laporan</span></button>
                              </div>
                            </form>
                            </div>
                           </div>
                         </div>
                          </div>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
                <!-- Row end -->
                </section>
            </div>
          </div>
        </div>
      </div>
      