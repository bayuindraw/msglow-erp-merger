<form id="main" class="form-horizontal" action="<?= base_url() ?>Administrator/Stock_Act/chk_confirmation_password/<?= $id_pokemasan ?>" method="post">

    <div class="md-group-add-on p-relative">
        <span class="md-add-on">
            <i class="icofont icofont-ui-calendar"></i>
        </span>
        <div class="md-input-wrapper">
            <label for="Password">Password</label>
            <input type="password" id="dPassword" name="Password" class="form-control md-static floating-label 4IDE-date" value="">
            <span class="messages"></span>

        </div>
    </div>
    <br>
    <div class="md-input-wrapper">
        <button type="submit" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="{{cValueButton}}">
            <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Konfirmasi</span>
        </button>
    </div>
</form>