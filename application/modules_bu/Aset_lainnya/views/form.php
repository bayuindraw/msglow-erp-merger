<div class="row">
	<div class="col-lg-12 col-xl-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" onsubmit="return confirm('Are you sure?')" enctype="multipart/form-data">
				<div class="kt-portlet__body" id="x">
					<?php
					$totalx = 0;
					$i = 0;
					?>
					<div id="body0">
						<div class="form-group row">
							<div class="col-lg-4">
								<label>Nama Aset:</label>
								<input type="text" class="form-control" placeholder="Nama Aset" name="input2[0][material_name]" required>
							</div>
							<div class="col-lg-4">
								<label class="">Akun:</label>
								<select name="input2[0][coa_asset_id]" class="pilihAkun form-control md-static" required><?= $arrakun ?></select>
							</div>
							<div class="col-lg-4">
								<label class="">Tanggal Beli:</label>
								<input type="text" class="form-control date_picker" autocomplete="off" placeholder="Tanggal" name="input2[0][material_date]" required>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4">
								<label>Harga:</label>
								<input type="text" class="form-control numeric" placeholder="Harga" id="harga0" onkeyup="cekMax(0)" autocomplete="off" required name="input2[0][material_value]">
							</div>
							<div class="col-lg-4">
								<label class="">Dibayar:</label>
								<input type="text" class="form-control numeric" placeholder="Dibayar" id="bayar0" onkeyup="cekMax(0)" autocomplete="off" required name="input2[0][material_paid]">
							</div>
							<div class="col-lg-4">
								<label class="">Akun Pembayaran:</label>
								<select id="material_type0" name="input2[0][material_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(0);" required>
									<option value=""></option>
									<option value="1">KAS</option>
									<option value="2">BANK</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4">
								<label class="">Pilihan Akun:</label>
								<select id="arrakuncoa40" name="input2[0][coa4_pakai]" class="pilihAkun form-control md-static" required></select>
							</div>
						</div>
						<!-- <div class="form-group row">
							<div class="col-lg-6 kt-align-right">
								<button onclick="myDeleteFunction(0);chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button>
							</div>
						</div> -->
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="simpan" value="simpan" id="btn_submit" class="btn btn-primary">Simpan</button>
								<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
							</div>
							<!-- <div class="col-lg-6 kt-align-right">
								<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
							</div> -->
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	var count = <?= $i; ?>;

	function myCreateFunction() {
		count = count + 1;
		var html = '<div id="body' + count + '"><div class="form-group row">	<div class="col-lg-4">		<label>Nama Aset:</label>		<input type="text" class="form-control" placeholder="Nama Aset" name="input2[' + count + '][material_name]" required>	</div>	<div class="col-lg-4">		<label class="">Akun:</label>		<select name="input2[' + count + '][coa_asset_id]" class="pilihAkun form-control md-static" required><?= $arrakun ?></select>	</div>	<div class="col-lg-4">		<label class="">Tanggal Beli:</label>		<input type="text" class="form-control date_picker" autocomplete="off" placeholder="Tanggal" name="input2[' + count + '][material_date]" required>	</div></div><div class="form-group row">	<div class="col-lg-4">		<label>Harga:</label>		<input type="text" class="form-control numeric" placeholder="Harga" name="input2[' + count + '][material_value]">	</div>	<div class="col-lg-4">		<label class="">Dibayar:</label>		<input type="text" class="form-control numeric" placeholder="Dibayar" name="input2[' + count + '][material_paid]">	</div>	<div class="col-lg-4">		<label class="">Akun Pembayaran:</label>		<select id="material_type' + count + '" name="input2[' + count + '][material_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(' + count + ');" required>			<option value=""></option>			<option value="1">KAS</option>			<option value="2">BANK</option>		</select>	</div></div><div class="form-group row">	<div class="col-lg-4">		<label>Akun Pembayaran:</label>		<select id="asset_type' + count + '" name="input2[' + count + '][asset_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(' + count + ');" required>			<option value=""></option>			<option value="1">KAS</option>			<option value="2">BANK</option>			<option value="3">Modal Disetor</option>			<option value="4">Aset Lainnya</option>		</select>	</div>	<div class="col-lg-4">		<label class="">Pilihan Akun:</label>		<select id="arrakuncoa4' + count + '" name="input2[' + count + '][coa4_pakai]" class="pilihAkun form-control md-static" required></select>	</div></div><div class="form-group row">	<div class="col-lg-6 kt-align-right">		<button onclick="myDeleteFunction(' + count + ');chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button>	</div></div></div>';
		$('#x').append(html);
		$('.pilihAkun' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('#material_type' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('#arrakuncoa4' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
		$('.date_picker, #kt_datepicker_1_validate').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: "bottom left",
			format: "yyyy-mm-dd"
		});
		$(".numeric").mask("#,##0", {
			reverse: true
		});
	}

	function myDeleteFunction(id) {
		$('#body' + id).remove();
	}

	function cekCoa4(id) {
		$.ajax({
			url: '<?= base_url() ?>Aset_tetap/get_coa4/' + $('#material_type' + id + ' option:selected').val(),
			success: function(result) {
				$("#arrakuncoa4" + id + "").html(result);
			}
		});
	}

	function cekMax(i) {
		// if ($('#bayar' + i).val() == '') $('#bayar' + i).val(0);
		// if ($('#harga' + i).val() == '') $('#harga' + i).val(0);
		bayar = $('#bayar' + i).val().replaceAll(",", "");
		harga = $('#harga' + i).val().replaceAll(",", "");
		console.log(bayar)
		console.log(harga)
		if (parseFloat(bayar) > parseFloat(harga)) {
			$('#bayar' + i).val($('#harga' + i).val());
		}
	}
</script>