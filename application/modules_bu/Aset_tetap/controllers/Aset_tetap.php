<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Aset_tetap extends CI_Controller
{

	var $url_ = "Aset_tetap";
	var $id_ = "asset_id";
	var $eng_ = "Asset";
	var $ind_ = "Aset";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function form()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['arrakun'] = "<option></option>";
		$datacontent['arrakuncoa4'] = "";
		$arrakuns = $this->db->query("SELECT * FROM coa_3 WHERE coa2_id = 1 AND nama NOT LIKE '%akum%' AND nama != 'Aset Tetap'")->result_array();
		foreach ($arrakuns as $arrakun) {
			$datacontent['arrakun'] .= "<option value=" . $arrakun['id'] . ">" . $arrakun['nama'] . "</option>";
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_coa4($id)
	{
		$data = "<option></option>";
		if ($id == 1) {
			foreach ($this->db->get_where('coa_4', ['coa3_id' => 10])->result_array() as $value) {
				$data .= "<option value=" . $value['id'] . ">" . $value['nama'] . "</option>";
			}
		} else if ($id == 2) {
			foreach ($this->db->get_where('coa_4', ['coa3_id' => 11])->result_array() as $value) {
				$data .= "<option value=" . $value['id'] . ">" . $value['nama'] . "</option>";
			}
		} else if ($id == 4) {
			$arrdata = $this->db->query("SELECT B.nama, SUM(A.coa_debit) as total, A.coa_id FROM t_coa_transaction A JOIN coa_4 B ON B.id = A.coa_id WHERE B.coa3_id = '180' AND A.coa_transaction_realization = 0 GROUP BY A.coa_id")->result_array();
			foreach ($arrdata as $value) {
				$data .= "<option value=" . $value['coa_id'] . " total=" . $value['total'] . ">" . $value['nama'] . "</option>";
			}
		}
		echo $data;
	}

	public function simpan()
	{
		$this->db->trans_begin();
		foreach ($_POST['input2'] as $input) {
			if ($input['asset_type'] == 4) {
				$this->db->query("UPDATE t_coa_transaction SET coa_transaction_realization = 1 WHERE coa_id = $input[coa4_pakai]");
			}

			$kode = $this->db->get_where('coa_4', ['coa3_id' => $input['coa3_id']])->result_array();
			$kode = count($kode) + 1;
			$coaAset = $this->Model->insert_coa_aset($input['coa3_id'], $input['asset_name'], $kode);
			$coaBeban = $this->Model->insert_coa_beban($input['coa3_id'], $input['asset_name'], $kode);
			$coaAkum = $this->Model->insert_coa_akum($input['coa3_id'], $input['asset_name'], $kode);

			if ($input['asset_type'] == 3) {
				$coaBank = $this->db->get_where('coa_3', ['id' => 29])->row_array();
			} else {
				$coaBank = $this->db->get_where('coa_4', ['id' => $input['coa4_pakai']])->row_array();
			}

			$t_asset = ([
				'asset_name' => $input['asset_name'],
				'asset_date' => $input['asset_date'],
				'asset_span' => $input['asset_span'],
				'asset_value' => str_replace(",", "", $input['asset_value']),
				'asset_paid' => str_replace(",", "", $input['asset_paid']),
				'asset_type' => $input['coa3_id'],
				'asset_seller' => $input['asset_seller'],
				'pic_id' => $_SESSION['user_id'],
				'asset_create_date' => date('Y-m-d H:i:s'),
				'asset_create_user' => $_SESSION['user_id'],
				'coa_last_code' => $kode,
				'coa_id' => $coaAset['id'],
				'coa_shrink_id' => $coaBeban['id'],
				'coa_shrink_accumulation_id' => $coaAkum['id'],
			]);
			$this->db->insert('t_asset', $t_asset);
			$t_asset['asset_id'] = $this->db->insert_id();

			$header = ([
				'coa_transaction_date' => $input['asset_date'],
				'date_create' => date('Y-m-d'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_debit' => $t_asset['asset_value'],
				'coa_transaction_credit' => $t_asset['asset_value'],
				'coa_transaction_payment' => $t_asset['asset_value'],
				'coa_transaction_realization' => 1,
				'coa_transaction_realization_date' => date('Y-m-d'),
			]);
			$this->db->insert('t_coa_transaction_header', $header);
			$idHeader = $this->db->insert_id();

			$akunAset = ([
				'coa_name' => $coaAset['nama'],
				'coa_code' => $coaAset['kode'],
				'coa_date' => $input['asset_date'],
				'coa_level' => 4,
				'coa_debit' => $t_asset['asset_value'],
				'coa_transaction_note' => 'Pembelian Aset By ' . $_SESSION['user_fullname'],
				'date_create' => date('Y-m-d'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 13,
				'coa_transaction_source_id' => $t_asset['asset_id'],
				'coa_group_id' => $idHeader,
				'coa_id' => $coaAset['id'],
				'coa_transaction_realization' => 1,
			]);
			$this->db->insert('t_coa_transaction', $akunAset);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $akunAset[coa_debit] WHERE coa_id = '$akunAset[coa_id]' AND coa_level = '$akunAset[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $akunAset[coa_debit] WHERE coa_id = '$akunAset[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunAset['coa_date'] . "', '-', '') AND coa_level = '$akunAset[coa_level]'");

			if ($t_asset['asset_paid'] > 0) {
				$akunBank = ([
					'coa_name' => $coaBank['nama'],
					'coa_code' => $coaBank['kode'],
					'coa_date' => $input['asset_date'],
					'coa_credit' => $t_asset['asset_paid'],
					'coa_transaction_note' => 'Pembelian Aset By ' . $_SESSION['user_fullname'],
					'date_create' => date('Y-m-d'),
					'user_create' => $_SESSION['user_id'],
					'coa_transaction_source' => 13,
					'coa_transaction_source_id' => $t_asset['asset_id'],
					'coa_group_id' => $idHeader,
					'coa_id' => $coaBank['id'],
					'coa_transaction_realization' => 1,
				]);
				if ($input['asset_type'] == 3) {
					$akunBank['coa_level'] = 3;
				} else {
					$akunBank['coa_level'] = 4;
				}
				$this->db->insert('t_coa_transaction', $akunBank);
				$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $akunBank[coa_credit] WHERE coa_id = '$akunBank[coa_id]' AND coa_level = '$akunBank[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $akunBank[coa_credit] WHERE coa_id = '$akunBank[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunBank['coa_date'] . "', '-', '') AND coa_level = '$akunBank[coa_level]'");
			}

			if ($t_asset['asset_value'] > $t_asset['asset_paid']) {
				$coaUtang = $this->Model->insert_coa_utang($input['asset_seller']);
				$akunUtang = ([
					'coa_name' => $coaUtang['nama'],
					'coa_code' => $coaUtang['kode'],
					'coa_date' => $input['asset_date'],
					'coa_level' => 4,
					'coa_credit' => $t_asset['asset_value'] - $t_asset['asset_paid'],
					'coa_transaction_note' => 'Pembelian Aset By ' . $_SESSION['user_fullname'],
					'date_create' => date('Y-m-d'),
					'user_create' => $_SESSION['user_id'],
					'coa_transaction_source' => 13,
					'coa_transaction_source_id' => $t_asset['asset_id'],
					'coa_group_id' => $idHeader,
					'coa_id' => $coaUtang['id'],
					'coa_transaction_realization' => 1,
				]);
				$this->db->insert('t_coa_transaction', $akunUtang);
				$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $akunUtang[coa_credit] WHERE coa_id = '$akunUtang[coa_id]' AND coa_level = '$akunUtang[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $akunUtang[coa_credit] WHERE coa_id = '$akunUtang[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunUtang['coa_date'] . "', '-', '') AND coa_level = '$akunUtang[coa_level]'");
			}
			if (date('Y-m') != substr($input['asset_date'], 0, 7) && $input['coa3_id'] != 181) {
				$this->input_old_coa($input['asset_date'], $t_asset, $coaAkum, $coaBeban, $idHeader, $input['asset_span']);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(base_url() . 'Aset_tetap/form');
	}

	public function input_old_coa($date, $t_asset, $coaAkum, $coaBeban, $idHeader, $span)
	{
		$tgl1 = $date;
		$tgl2 = date('Y-m-d');
		$awal1 = substr(str_replace('-', '', $tgl1), 0, 6);
		$akhir1 = substr(str_replace('-', '', $tgl1), 6, 8);
		$awal2 = substr(str_replace('-', '', $tgl2), 0, 6);
		$akhir2 = substr(str_replace('-', '', $tgl2), 6, 8);
		$tgl_akhir1 = date("t", strtotime($tgl1));
		$cekspan = 1;
		for ($i = ($awal1 + 1); $i <= $awal2; $i++) {
			if ($cekspan <= $span) {

				if (substr($i, 4, 6) == 13) {
					$i = (substr($i, 0, 4) + 1) . '01';
				}
				$tgl_akhir = date("Y-m-t", strtotime(substr($i, 0, 4) . '-' . substr($i, 4, 6) . '-01'));
				if ($awal2 == $i) {
					if ($akhir1 > substr($tgl_akhir, 8, 10)) {
						if ($akhir1 <= $akhir2 || ($akhir1 == $tgl_akhir1 && $akhir2 == substr($tgl_akhir, 8, 10))) {
							$this->Model->input_old_coa($tgl_akhir, $t_asset, $coaAkum, $coaBeban, $idHeader);
						}
					} else {
						if ($akhir1 <= $akhir2) {
							$this->Model->input_old_coa(substr($tgl_akhir, 0, 8) . $akhir1, $t_asset, $coaAkum, $coaBeban, $idHeader);
						}
					}
				} else {
					if ($akhir1 > substr($tgl_akhir, 8, 10)) {
						$this->Model->input_old_coa(substr($tgl_akhir, 0, 8) . substr($tgl_akhir, 8, 10), $t_asset, $coaAkum, $coaBeban, $idHeader);
					} else {
						$this->Model->input_old_coa(substr($tgl_akhir, 0, 8) . $akhir1, $t_asset, $coaAkum, $coaBeban, $idHeader);
					}
				}
				echo "<br>";
				if (substr($i, 4, 6) == 12) $i += 88;
				$cekspan++;
			}
		}
		return '';
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function get_data_detail($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_detail($id);
	}

	public function detail($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['id'] = $id;
		$datacontent['title'] = 'Data Penyusutan';
		$datacontent['card'] = $this->db->query("SELECT A.asset_value, A.asset_date, sum(B.coa_credit) as coa_credit FROM t_asset A left JOIN t_coa_transaction B ON B.coa_id = A.coa_shrink_id WHERE A.asset_id = '$id' group by asset_id")->row_array();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
}
