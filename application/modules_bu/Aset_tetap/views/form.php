<div class="row">
	<div class="col-lg-12 col-xl-12">
		<!--begin::Portlet-->
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>

			<!--begin::Form-->
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" onsubmit="return confirm('Are you sure?')" enctype="multipart/form-data">
				<div class="kt-portlet__body" id="x">
					<?php
					$totalx = 0;
					$i = 0;
					?>
					<div id="body0">
						<div class="form-group row">
							<div class="col-lg-4">
								<label>Nama Aset:</label>
								<input type="text" class="form-control" placeholder="Nama Aset" name="input2[0][asset_name]" required>
							</div>
							<div class="col-lg-4">
								<label class="">Akun:</label>
								<select name="input2[0][coa3_id]" class="pilihAkun form-control md-static" required><?= $arrakun ?></select>
							</div>
							<div class="col-lg-4">
								<label class="">Tanggal Mulai Pemakaian:</label>
								<input type="text" class="form-control date_picker" placeholder="Tanggal" autocomplete="off" name="input2[0][asset_date]" required>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4">
								<label>Umur Ekonomis (Bulan):</label>
								<input type="number" class="form-control" autocomplete="off" placeholder="Umur Ekonomis" name="input2[0][asset_span]">
							</div>
							<div class="col-lg-4">
								<label class="">Harga:</label>
								<input type="text" class="form-control numeric" id="harga0" autocomplete="off" placeholder="Harga" onkeyup="cekMax(0)" required name="input2[0][asset_value]">
							</div>
							<div class="col-lg-4">
								<label class="">Total Pembayaran:</label>
								<input type="text" class="form-control numeric" id="totalbayar0" autocomplete="off" placeholder="Total Bayar" required onkeyup="cekMax(0)" name="input2[0][asset_paid]">
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg-4">
								<label>Akun Pembayaran:</label>
								<select id="asset_type0" name="input2[0][asset_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(0);" required>
									<option value=""></option>
									<option value="1">KAS</option>
									<option value="2">BANK</option>
									<option value="3">Modal Disetor</option>
									<option value="4">Aset Lainnya</option>
								</select>
							</div>
							<div class="col-lg-4">
								<label class="">Pilihan Akun:</label>
								<select onchange="chkcode(0);" id="arrakuncoa40" name="input2[0][coa4_pakai]" class="pilihAkun form-control md-static" required></select>
							</div>
							<div class="col-lg-4">
								<label class="">Nama Penjual:</label>
								<input type="text" class="form-control" placeholder="Nama Penjual" name="input2[0][asset_seller]">
							</div>
						</div>
						<!-- <div class="form-group row">
							<div class="col-lg-6 kt-align-right">
								<button onclick="myDeleteFunction(0);chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button>
							</div>
						</div> -->
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="simpan" value="simpan" id="btn_submit" class="btn btn-primary">Simpan</button>
								<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
							</div>
							<!-- <div class="col-lg-6 kt-align-right">
								<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
							</div> -->
						</div>
					</div>
				</div>
			</form>

			<!--end::Form-->
		</div>

		<!--end::Portlet-->
	</div>

	<script>
		var count = <?= $i; ?>;

		function myCreateFunction() {
			count = count + 1;
			var html = '<div id="body' + count + '"><div class="form-group row"><div class="col-lg-4"><label>Nama Aset:</label><input type="text" class="form-control" placeholder="Nama Aset" name="input2[' + count + '][asset_name]" required>	</div>	<div class="col-lg-4">		<label class="">Akun:</label>		<select name="input2[' + count + '][coa3_id]" class="pilihAkun form-control md-static" required><?= $arrakun ?></select>	</div>	<div class="col-lg-4">		<label class="">Tanggal Mulai Pemakaian:</label>		<input type="text" class="form-control date_picker" placeholder="Tanggal" autocomplete="off" name="input2[' + count + '][asset_date]" required>	</div></div><div class="form-group row">	<div class="col-lg-4">		<label>Umur Ekonomis (Bulan):</label>		<input type="number" class="form-control" placeholder="Umur Ekonomis" name="input2[' + count + '][asset_span]">	</div>	<div class="col-lg-4">		<label class="">Harga:</label>		<input type="text" class="form-control numeric" id="harga' + count + '" placeholder="Harga" name="input2[' + count + '][asset_value]">	</div>	<div class="col-lg-4">		<label class="">Total Pembayaran:</label>		<input type="text" class="form-control numeric" id="totalbayar' + count + '" placeholder="Total Bayar" name="input2[' + count + '][asset_paid]">	</div></div><div class="form-group row">	<div class="col-lg-4">		<label>Akun Pembayaran:</label>		<select id="asset_type' + count + '" name="input2[' + count + '][asset_type]" class="pilihAkun form-control md-static" onchange="cekCoa4(' + count + ');" required>			<option value=""></option>			<option value="1">KAS</option>			<option value="2">BANK</option>			<option value="3">Modal Disetor</option>			<option value="4">Aset Lainnya</option>		</select>	</div>	<div class="col-lg-4">		<label class="">Pilihan Akun:</label>		<select onchange="chkcode(' + count + ');" id="arrakuncoa4' + count + '" name="input2[' + count + '][coa4_pakai]" class="pilihAkun form-control md-static" required></select>	</div>	<div class="col-lg-4">		<label class="">Nama Pemilik:</label>		<input type="text" class="form-control" placeholder="Nama Pemilik" name="input2[' + count + '][asset_seller]">	</div></div><div class="col-lg-6 kt-align-right">	<button onclick="myDeleteFunction(' + count + ');chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></div></div>';
			$('#x').append(html);
			$('.pilihAkun' + count).select2({
				allowClear: true,
				placeholder: 'Pilih Akun',
			})
			$('#asset_type' + count).select2({
				allowClear: true,
				placeholder: 'Pilih Akun',
			})
			$('#arrakuncoa4' + count).select2({
				allowClear: true,
				placeholder: 'Pilih Akun',
			})
			$('.date_picker, #kt_datepicker_1_validate').datepicker({
				rtl: KTUtil.isRTL(),
				todayHighlight: true,
				orientation: "bottom left",
				format: "yyyy-mm-dd"
			});
			$(".numeric").mask("#,##0", {
				reverse: true
			});
		}

		function myDeleteFunction(id) {
			$('#body' + id).remove();
		}

		function cekCoa4(id) {
			$.ajax({
				url: '<?= base_url() ?>Aset_tetap/get_coa4/' + $('#asset_type' + id + ' option:selected').val(),
				success: function(result) {
					$("#arrakuncoa4" + id + "").html(result);
				}
			});
		}

		function chkcode(code) {
			if ($("#asset_type" + code).find(":selected").val() == 4) {
				$('#harga' + code).val(commafy($('#arrakuncoa4' + code + ' option:selected').attr('total')));
				$('#totalbayar' + code).val(commafy($('#arrakuncoa4' + code + ' option:selected').attr('total')));
				$('#harga' + code).attr('readonly', 'true');
				$('#totalbayar' + code).attr('readonly', 'true');
			} else {
				$('#harga' + code).removeAttr('readonly');
				$('#totalbayar' + code).removeAttr('readonly');
			}
		}

		function cekMax(i) {
			// if ($('#totalbayar' + i).val() == '') $('#totalbayar' + i).val(0);
			// if ($('#harga' + i).val() == '') $('#harga' + i).val(0);
			if (parseFloat($('#totalbayar' + i).val().replaceAll(",", "")) > parseFloat($('#harga' + i).val().replaceAll(",", ""))) {
				$('#totalbayar' + i).val($('#harga' + i).val());
			}
		}
	</script>