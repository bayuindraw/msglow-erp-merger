    <?php if ($this->session->flashdata('flash')) : ?>
        <div class="alert alert-success">
            <?= $this->session->flashdata('flash'); ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <form id="add-category-ticket" method="POST" action="<?= isset($coa) ? base_url() . 'COA/update_coa2' : base_url() . 'COA/store_coa2' ?>">
                        <?php if (isset($coa)) : ?>
                            <div class="form-group">
                                <label for="name"> Kode</label>
                                <input type="text" name="input[kode]" id="kode" class="form-control" placeholder="Kode COA" aria-describedby="kode" value="<?= isset($coa) ? $coa[0]['kode'] : '' ?>" <?= isset($coa) ? 'disabled' : '' ?>>
                            </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <label for="name"> Tipe COA1</label>
                            <select id="selectCOA" class="form-control" name="input[coa1_id]">
                                <?php
                                foreach ($coas1 as $coa1) {
                                ?>
                                    <option value="<?= $coa1['id'] ?>" <?= isset($coa) ? $coa[0]['coa1_id'] == $coa1['id'] ? 'selected' : '' : '' ?>><?= $coa1['kode'] ?> - <?= $coa1['nama'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name"> COA</label>
                            <input type="text" name="input[nama]" id="nama" class="form-control" placeholder="Nama COA" aria-describedby="nama" value="<?= isset($coa) ? $coa[0]['nama'] : '' ?>">
                            <input type="hidden" name="input[kode]" id="nama" class="form-control" placeholder="Nama COA" aria-describedby="nama" value="<?= isset($coa) ? $coa[0]['kode'] : '' ?>">
                            <input type="hidden" name="input[id]" id="nama" class="form-control" placeholder="Nama COA" aria-describedby="nama" value="<?= isset($coa) ? $coa[0]['id'] : '' ?>">
                        </div>
                        <div class="text-right">
                            <?php if (isset($coa)) : ?>
                                <a href="<?= base_url() ?>COA/coa2" class="btn btn-danger btn-sm waves-effect waves-light"><span class="btn-label"><i class="fas fa-ban"></i></span> Batal</a>
                            <?php endif; ?>
                            <button class="btn btn-success btn-sm waves-effect waves-light" type="submit"><span class="btn-label"><i class="fas fa-save"></i></span> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="listCoa" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th width="10%">Kode</th>
                                    <th width="">Nama</th>
                                    <th width="5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($coas as $coa) : ?>
                                    <tr>
                                        <td><?= $coa['kode'] ?></td>
                                        <td><?= $coa['nama'] ?></td>
                                        <td>
                                            <a href="<?= base_url() ?>COA/coa2/edit/<?= $coa['id'] ?>" class="btn btn-info btn-sm waves-effect waves-light del-product" type="submit"><span class="btn-label"><i class="fas fa-edit"></i></span></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#listCoa').DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });
    </script>