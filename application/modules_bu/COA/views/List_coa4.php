    <?php if ($this->session->flashdata('flash')) : ?>
        <div class="alert alert-success">
            <?= $this->session->flashdata('flash'); ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <form id="add-category-ticket" method="POST" action="<?= isset($coa) ? base_url() . 'COA/update_coa4' : base_url() . 'COA/store_coa4' ?>">
                        <?php if (isset($coa)) : ?>
                            <div class="form-group">
                                <label for="name"> Kode</label>
                                <input type="text" name="input[kode]" id="kode" class="form-control" placeholder="Kode COA" aria-describedby="kode" value="<?= isset($coa) ? $coa[0]['kode'] : '' ?>" <?= isset($coa) ? 'disabled' : '' ?>>
                            </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <label for="name"> Tipe coa3</label>
                            <select id="selectCOA" class="form-control" name="input[coa3_id]">
                                <?php
                                foreach ($coas3 as $coa3) {
                                ?>
                                    <option value="<?= $coa3['id'] ?>" <?= isset($coa) ? $coa[0]['coa3_id'] == $coa3['id'] ? 'selected' : '' : '' ?>><?= $coa3['kode'] ?> - <?= $coa3['nama'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name"> COA</label>
                            <input type="text" name="input[nama]" id="nama" class="form-control" placeholder="Nama COA" aria-describedby="nama" value="<?= isset($coa) ? $coa[0]['nama'] : '' ?>">
                            <input type="hidden" name="input[kode]" id="nama" class="form-control" placeholder="Nama COA" aria-describedby="nama" value="<?= isset($coa) ? $coa[0]['kode'] : '' ?>">
                            <input type="hidden" name="input[id]" id="nama" class="form-control" placeholder="Nama COA" aria-describedby="nama" value="<?= isset($coa) ? $coa[0]['id'] : '' ?>">
                        </div>
                        <div class="text-right">
                            <?php if (isset($coa)) : ?>
                                <a href="<?= base_url() ?>COA/coa3" class="btn btn-danger btn-sm waves-effect waves-light"><span class="btn-label"><i class="fas fa-ban"></i></span> Batal</a>
                            <?php endif; ?>
                            <button class="btn btn-success btn-sm waves-effect waves-light" type="submit"><span class="btn-label"><i class="fas fa-save"></i></span> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="listCoa" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th width="10%">Kode</th>
                                    <th width="">Nama</th>
                                    <th width="5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#listCoa').DataTable({
                "pagingType": "full_numbers",scrollY:        "300px",
                scrollX:        true,
                scrollCollapse: true,
                "processing": true,
                "serverSide": true,
                "ajax": "<?= site_url() ?>/<?= $url ?>/get_data_coa4"
                });
            });
    </script>