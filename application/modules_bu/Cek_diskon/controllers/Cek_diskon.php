<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Cek_diskon extends CI_Controller
{

	var $url_ = "Cek_diskon";
	var $ind_ = "Cek_diskon";

	function produk_api()
	{
		$arrdata = $this->db->query('SELECT id, kode, nama_produk FROM produk_global')->result_array();
		$data = array('data' => $arrdata, 'status' => 'success', 'code' => '200');
		echo json_encode($data);
	}

	function price_api()
	{
		$search = $_GET['kode'];
		$where = "WHERE produk_global_kd = '$search' AND schema_id = 1";
		$arrdata = $this->db->query("SELECT price_quantity, price_unit_price FROM m_price $where")->result_array();
		$data = array('data' => $arrdata, 'status' => 'success', 'code' => '200');
		echo json_encode($data);
	}

	function member_api()
	{
		$search = $_GET['search'];
		$where = "WHERE kode LIKE '%$search%' OR NAMA LIKE '%$search%'";
		$arrdata = $this->db->query("SELECT nama, kode FROM member $where LIMIT 10")->result_array();
		$data = array('data' => $arrdata, 'status' => 'success', 'code' => '200');
		echo json_encode($data);
	}

	function cek_php()
	{
		echo 'Current PHP version: ' . phpversion();
		die();
	}

	function cek_discount()
	{
		$rewardHarga = '';
		$rewardQuantity = '';
		$rewardPersen = '';
		$reward = '';
		$arrjoin = "";
		if (@count(@$_POST['kode']) > 0) {
			$arrjoin = join("', '", @$_POST['kode']);
		}
		$arrproduct = $this->db->query("SELECT kd_pd, id_produk FROM produk WHERE id_produk IN('$arrjoin')")->result_array();
		foreach ($arrproduct as $indexkd => $valuekd) {
			$arrdict[$valuekd['id_produk']] = $valuekd['kd_pd'];
		}
		if (@count(@$_POST['kode']) > 0) {
			foreach (@$_POST['kode'] as $index_kode => $value_kode) {
				if (@$hrgbrg[$arrdict[$value_kode]] != "") $hrgbrg[$arrdict[$value_kode]] += $_POST['price'][$index_kode];
				else @$hrgbrg[$arrdict[$value_kode]] = $_POST['price'][$index_kode];
				if (@$jumbrg[$arrdict[$value_kode]] != "") $jumbrg[$arrdict[$value_kode]] += $_POST['quantity'][$index_kode];
				else @$jumbrg[$arrdict[$value_kode]] = $_POST['quantity'][$index_kode];
			}
		}
		$arrdiscount = $this->db->query("SELECT * FROM m_discount WHERE discount_active = 1")->result_array();
		$arrdiscount_terms = $this->db->query("SELECT B.discount_id, C.* FROM m_discount_terms A JOIN m_discount B ON B.discount_id = A.discount_id JOIN m_discount_terms_product C ON C.discount_terms_id = A.discount_terms_id JOIN m_discount_member E ON E.discount_id = B.discount_id WHERE E.member_status = '" . $_POST['member_status'] . "' AND discount_active = 1")->result_array();
		foreach ($arrdiscount_terms as $indexterms => $valueterms) {
			$arrfdisc_terms[$valueterms['discount_id']][$valueterms['discount_terms_id']][$valueterms['discount_terms_product_id']] = $valueterms;
		}
		$arrdiscount_reward = $this->db->query("SELECT B.discount_id, C.*, D.nama_produk FROM m_discount_reward A JOIN m_discount B ON B.discount_id = A.discount_id JOIN m_discount_reward_product C ON C.discount_reward_id = A.discount_reward_id LEFT JOIN produk_global D ON D.kode = C.product_global_code JOIN m_discount_member E ON E.discount_id = B.discount_id WHERE E.member_status = '" . $_POST['member_status'] . "' AND discount_active = 1")->result_array();
		foreach ($arrdiscount_reward as $indexreward => $valuereward) {
			$arrfdisc_reward[$valuereward['discount_id']][$valuereward['discount_reward_id']][$valuereward['discount_reward_product_id']] = $valuereward;
		}
		if (@count(@$arrfdisc_terms) > 0) {
			foreach ($arrfdisc_terms as $index1 => $value1) {
				$total_terms[$index1] = count($value1);
				$accepted_terms[$index1] = 0;
				$min = 0;
				$sementara = "";
				foreach ($value1 as $index2 => $value2) {
					$ix = 0;
					$jum[$index1][$index2] = 0;
					foreach ($value2 as $index3 => $value3) {
						if ($value3['discount_terms_product_type'] == "2") {
							if (@$hrgbrg[$value3['product_global_code']] == "") $hrgbrg[$value3['product_global_code']] = 0;
							$jum[$index1][$index2] += $hrgbrg[$value3['product_global_code']];
						} else if ($value3['discount_terms_product_type'] == "1") {
							if (@$jumbrg[$value3['product_global_code']] == "") $jumbrg[$value3['product_global_code']] = 0;
							$jum[$index1][$index2] += $jumbrg[$value3['product_global_code']];
						}
					}
					if ($ix == 0) {
						if ($value3['discount_terms_product_type'] == "2") {
							if ($jum[$index1][$index2] >= $value3['discount_terms_product_price']) {
								$ix = 1;
								$sementara = intdiv($jum[$index1][$index2], $value3['discount_terms_product_price']);
								if ($min == 0 || $sementara < $min) $min = $sementara;
							}
						} else if ($value3['discount_terms_product_type'] == "1") {
							if ($jum[$index1][$index2] >= $value3['discount_terms_product_quantity']) {
								$ix = 1;
								$sementara = intdiv($jum[$index1][$index2], $value3['discount_terms_product_quantity']);
								if ($min == 0 || $sementara < $min) $min = $sementara;
							}
						}
					}
					if ($ix == 1) $accepted_terms[$index1]++;
				}


				if (@$accepted_terms[$index1] == $total_terms[$index1]) {
					$accepted_discount[$index1] = $index1;
					if ($min > 0) $multiply[$index1] = $min;
				}
			}
		}
		$total_potongan = 0;
		$total_potongan2 = 0;
		$i = 0;
		$x = 0;

		if (@count(@$accepted_discount) > 0) {
			foreach ($accepted_discount as $index_reward => $value_reward) {
				$jum_syarat = 0;
				foreach ($arrfdisc_terms[$value_reward] as $indexterms2 => $valueterms2) {
					//print_r($valueterms2);
					foreach ($valueterms2 as $indexterms3 => $valueterms3) {
						$jum_syarat += @$hrgbrg[$valueterms3['product_global_code']];
						//if($valueterms3['discount_terms_product_type'] == "2"){
						//echo $hrgbrg[$valueterms3['product_global_code']] * $multiply[$value_reward].'|';
						/*if(@$hrgbrg[$value3['product_global_code']] == "") $hrgbrg[$value3['product_global_code']] = 0;
							$jum += $hrgbrg[$value3['product_global_code']];*/
						//}else if($valueterms3['discount_terms_product_type'] == "1"){
						//echo $valueterms3['discount_terms_product_quantity'] * $multiply[$value_reward].'|';
						//if(@$jumbrg[$value3['product_global_code']] == "") $jumbrg[$value3['product_global_code']] = 0;
						//$jum += $jumbrg[$value3['product_global_code']];
						//}
					}
					//die();
				}
				if (@count(@$arrfdisc_reward[$value_reward]) > 0) {
					foreach ($arrfdisc_reward[$value_reward] as $index_reward2 => $value_reward2) {
						foreach ($value_reward2 as $index_reward3 => $value_reward3) {

							if ($value_reward3['discount_reward_product_type'] == 3) {
								if (@$reward_product[$value_reward3['product_global_code']] != "") {
									$reward_product[$value_reward3['product_global_code']] += $value_reward3['discount_reward_product_quantity'] * $multiply[$value_reward];
								} else {
									$reward_product[$value_reward3['product_global_code']] = $value_reward3['discount_reward_product_quantity'] * $multiply[$value_reward];
								}
								$reward_kd[$value_reward3['product_global_code']] = $value_reward3['nama_produk'];
								$reward .= '<input type="hidden" name="discount_id[]" value="' . $value_reward3['discount_id'] . '" />';
							} else if ($value_reward3['discount_reward_product_type'] == 2) {
								//$jum_syarat = ;
								$reward .= '<input type="hidden" name="discount_id[]" value="' . $value_reward3['discount_id'] . '" />';
								$total_potongan2 += $jum_syarat * $value_reward3['discount_reward_product_percentage'] / 100;
								$rewardPersen .= '<tr><td>' . ++$x . '</td><td>' . $value_reward3['discount_reward_product_percentage'] . '%</td><td>Rp ' . number_format($jum_syarat * $value_reward3['discount_reward_product_percentage'] / 100) . '</td></tr>';
							} else if ($value_reward3['discount_reward_product_type'] == 1) {

								foreach ($arrfdisc_terms[$value_reward] as $arrdisc_terms1) {
									foreach ($arrdisc_terms1 as $arrdisc_terms2) {
										if (isset($jumbrg[$arrdisc_terms2['product_global_code']]) && isset($hrgbrg[$arrdisc_terms2['product_global_code']])) {
										}
									}
								}
								$rewardHarga .= '<tr><td>' . ++$i . '</td><td>Rp ' . number_format($value_reward3['discount_reward_product_deduction_cost'] * $multiply[$value_reward]) . '</td></tr>';
								$total_potongan += $value_reward3['discount_reward_product_deduction_cost'] * $multiply[$value_reward];
								$reward .= '<input type="hidden" name="discount_id[]" value="' . $value_reward3['discount_id'] . '" />';
								//echo $value_reward3['discount_reward_product_deduction_cost']."|";
							}
						}
					}
				}
				//$leftovers[$value_reward] = '';
				//$arrfdisc_terms[$value_reward];
				//print_r($arrfdisc_reward[$value_reward]);
				//print_r($multiply[$value_reward]);
			}
			//die();
			if (@count(@$reward_kd) > 0) {
				$discount_produk = "";
				$discount_produk_input = "";
				foreach ($reward_kd as $index2x => $value2x) {
					//echo $arrreward_kd = join("', '", $reward_kd);
					$discount_produk .= $value2x . ' ' . $reward_product[$index2x] . ' pcs' . '<br>';
					// $rewardQuantity .= 'Gratis Produk ' . $value2x . ' ' . $reward_product[$index2x] . ' pcs' . '<br>';
					$rewardQuantity .= '<tr><td>1</td><td>' . $value2x . '</td><td>' . $reward_product[$index2x] . '</td></tr>';
					$discount_produk_input .= '<input type="hidden" name="promo_product[' . $index2x . ']" value="' . $reward_product[$index2x] . '" />';
				}
			}
			//if(@count(@$accepted_discount) > 0){

			//$this->
			//}
		}
		$data = array(
			'list_diskon_quantity' => @$rewardQuantity,
			'list_diskon_harga' => @$rewardHarga,
			'list_diskon_persen' => @$rewardPersen,
			'potongan' => @$total_potongan,
			'potongan2' => @$total_potongan2,
			'produk_gratis' => @$discount_produk,
			'produk_insert' => @$discount_produk_input,
			'reward' => $reward
		);
		echo json_encode($data);
		die();
	}

	function cek_discount_bayar($sales_id)
	{

		$rewardHarga = '';
		$rewardQuantity = '';
		$rewardPersen = '';
		$reward = '';
		$list_persen_product = '';
		$arrjoin = "";
		if (@count(@$_POST['kode']) > 0) {
			$arrjoin = join("', '", @$_POST['kode']);
		}

		$arrproduct = $this->db->query("SELECT kd_pd, id_produk FROM produk WHERE id_produk IN('$arrjoin')")->result_array();

		foreach ($arrproduct as $indexkd => $valuekd) {
			$arrdict[$valuekd['id_produk']] = $valuekd['kd_pd'];
		}
		if (@count(@$_POST['kode']) > 0) {
			foreach (@$_POST['kode'] as $index_kode => $value_kode) {
				if (@$hrgbrg[$arrdict[$value_kode]] != "") $hrgbrg[$arrdict[$value_kode]] += $_POST['price'][$index_kode];
				else $hrgbrg[$arrdict[$value_kode]] = $_POST['price'][$index_kode];
				if (@$jumbrg[$arrdict[$value_kode]] != "") $jumbrg[$arrdict[$value_kode]] += $_POST['quantity'][$index_kode];
				else $jumbrg[$arrdict[$value_kode]] = $_POST['quantity'][$index_kode];
			}
		}
		$arrdiscount = $this->db->query("SELECT * FROM t_sales_discount WHERE sales_id = '$sales_id' AND sales_discount_active = 1")->result_array();
		foreach ($arrdiscount as $indexdisc => $valuedisc) {
			$arrdiscount2[$valuedisc['sales_discount_id']] = $valuedisc;
		}
		$arrdiscount_terms = $this->db->query("SELECT B.sales_id, A.*, C.discount_terms_id, B.sales_discount_percentage_hoard, B.sales_discount_percentage_hoard_draft FROM t_sales_discount_terms A JOIN t_sales_discount B ON B.sales_discount_id = A.sales_discount_id JOIN m_discount_terms_product C ON C.discount_terms_product_id = A.discount_terms_product_id WHERE B.sales_id = '$sales_id' AND B.sales_discount_active = 1")->result_array();

		foreach ($arrdiscount_terms as $indexterms => $valueterms) {
			$arrfdisc_terms[$valueterms['sales_discount_id']][$valueterms['discount_terms_id']][$valueterms['sales_discount_terms_id']] = $valueterms;
		}
		/*print_r($arrfdisc_terms);
		die();
		$arrdiscount_reward = $this->db->query("SELECT B.discount_id, C.*, D.nama_produk FROM m_discount_reward A JOIN m_discount B ON B.discount_id = A.discount_id JOIN m_discount_reward_product C ON C.discount_reward_id = A.discount_reward_id LEFT JOIN produk_global D ON D.kode = C.product_kd JOIN m_discount_member E ON E.discount_id = B.discount_id WHERE E.member_status = '" . $_POST['member_status'] . "' AND discount_active = 1")->result_array();
		foreach ($arrdiscount_reward as $indexreward => $valuereward) {
			$arrfdisc_reward[$valuereward['discount_id']][$valuereward['discount_reward_id']][$valuereward['discount_reward_product_id']] = $valuereward;
		}*/
		if (@count(@$arrfdisc_terms) > 0) {
			foreach ($arrfdisc_terms as $index1 => $value1) {
				$total_terms[$index1] = count($value1);
				$accepted_terms[$index1] = 0;
				$min = 0;
				$sementara = "";
				foreach ($value1 as $index2 => $value2) {
					$ix = 0;
					$jum[$index1][$index2] = 0;
					$jum_all[$index1][$index2] = 0;
					$tot_harga[$index1] = 0;
					foreach ($value2 as $index3 => $value3) {
						if ($value3['sales_discount_terms_type'] == "2") {
							if (@$hrgbrg[$value3['product_global_code']] == "") $hrgbrg[$value3['product_global_code']] = 0;
							/*if($value3['sales_discount_terms_price'] < ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft'])){ 
								$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']) - $value3['sales_discount_terms_price'];
							}else{*/
							$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']);
							$tambah_price[$index1][$index2][$index3] = ($value3['sales_discount_percentage_hoard'] + $value3['sales_discount_percentage_hoard_draft']);
							//}
							$jum[$index1][$index2] += ($hrgbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
							$jum_all[$index1][$index2] = $value3['sales_discount_terms_price'];
						} else if ($value3['sales_discount_terms_type'] == "1") {
							if (@$jumbrg[$value3['product_global_code']] == "") $jumbrg[$value3['product_global_code']] = 0;
							/*if($value3['sales_discount_terms_quantity'] < ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft'])){ 
								$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']) - $value3['sales_discount_terms_quantity'];
							}else{*/
							$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']);
							$tambah_price[$index1][$index2][$index3] = ($value3['sales_discount_percentage_hoard'] + $value3['sales_discount_percentage_hoard_draft']);
							//}
							$jum[$index1][$index2] += ($jumbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
							$jum_all[$index1][$index2] = $value3['sales_discount_terms_quantity'];
						}
						$tot_harga[$index1] += ($hrgbrg[$value3['product_global_code']]);
					}
					if ($ix == 0) {
						if ($value3['sales_discount_terms_type'] == "2") {
							if ($jum[$index1][$index2] >= $value3['sales_discount_terms_price']) {
								$ix = 1;
								$sementara = intdiv($jum[$index1][$index2], $value3['sales_discount_terms_price']);
								if ($min == 0 || $sementara < $min) $min = $sementara;
							}
						} else if ($value3['sales_discount_terms_type'] == "1") {
							if ($jum[$index1][$index2] >= $value3['sales_discount_terms_quantity']) {
								$ix = 1;
								$sementara = intdiv($jum[$index1][$index2], $value3['sales_discount_terms_quantity']);
								if ($min == 0 || $sementara < $min) $min = $sementara;
							}
						}
					}
					if ($ix == 1) $accepted_terms[$index1]++;
				}

				//echo @$accepted_terms[$index1];
				//echo $total_terms[$index1];
				//echo "<hr>";
				if ($arrdiscount2[$index1]['sales_discount_percentage_active_draft'] == 1 && $arrdiscount2[$index1]['sales_discount_percentage'] != "") {
					$accepted_discount[$index1] = $index1;
					$multiply[$index1] = 1;
				} else if (@$accepted_terms[$index1] == $total_terms[$index1]) {
					$accepted_discount[$index1] = $index1;
					if ($min > 0) $multiply[$index1] = $min;
				}
			}
		}
		$total_potongan = 0;
		$total_potongan2 = 0;
		$i = 0;
		$x = 0;
		$y = 0;
		$jum_syarat = 0;
		if (@count(@$accepted_discount) > 0) {
			foreach ($accepted_discount as $index_reward => $value_reward) {
				$arrdatasales_discount = $this->db->query("SELECT A.*, B.sales_detail_id FROM t_sales_discount A JOIN t_sales_discount_detail B ON B.sales_discount_id = A.sales_discount_id WHERE A.sales_discount_id = '" . $index_reward . "'")->row_array();

				foreach ($arrfdisc_terms[$index_reward] as $index1x => $value1x) {
					foreach ($value1x as $index2x => $value2x) {
					}
				}
				if ($arrdatasales_discount['sales_discount_product'] == '1') {
					$arrdatasales_discount_product = $this->db->query("SELECT * FROM t_sales_discount_product WHERE sales_discount_id = '" . $index_reward . "'")->result_array();
					foreach ($arrdatasales_discount_product as $indexproduct => $valueproduct) {
						$rewardQuantity .= '<tr><td>' . ++$i . '<input type="hidden" name="dikson_produk[' . $sales_id . '][' . $valueproduct['sales_detail_id'] . ']" value="' . ($valueproduct['sales_discount_product_quantity'] * $multiply[$index_reward]) . '"/></td><td>' . $valueproduct['product_global_code'] . '</td><td>' . ($valueproduct['sales_discount_product_quantity'] * $multiply[$index_reward]) . ' pcs</td></tr>';
						$reward .= '<input type="hidden" name="discount_id[' . $sales_id . '][' . $valueproduct['sales_detail_id'] . ']" value="' . $arrdatasales_discount['discount_id'] . '"';
						// print_r($valueproduct);
					}
				}
				if ($arrdatasales_discount['sales_discount_price'] > 0) {
					$rewardHarga .= '<tr><td>' . ++$x . '<input type="hidden" name="diskon_harga[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" id="' . $sales_id . '" class="price_deduction" value="' . ($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]) . '"/></td><td>Rp ' . number_format($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]) . '</td></tr>';
					$total_potongan += ($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]);
					$reward .= '<input type="hidden" name="discount_id[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" value="' . $arrdatasales_discount['discount_id'] . '"/>';
				}

				if ($arrdatasales_discount['sales_discount_percentage'] > 0) {

					$val = 0;
					if ($arrdiscount2[$index_reward]['sales_discount_percentage_active'] == 0) {
						foreach ($arrfdisc_terms[$index_reward] as $index1xx => $val1) {
							foreach ($val1 as $index2xx => $val2) {

								$val += (($hrgbrg[$val2['product_global_code']] + $tambah_price[$index_reward][$index1xx][$index2xx]) * $arrdatasales_discount['sales_discount_percentage'] / 100);
								$list_persen_product .= '<input type="hidden" name="list_prod_diskon[' . $sales_id . '][' . $val2['product_global_code'] . ']" value="' . $arrdatasales_discount['sales_discount_percentage'] . '"/>';
							}
						}
					} else {
						foreach ($arrfdisc_terms[$index_reward] as $index1xx => $val1) {
							foreach ($val1 as $index2xx => $val2) {

								$val += (($hrgbrg[$val2['product_global_code']]) * $arrdatasales_discount['sales_discount_percentage'] / 100);
								$list_persen_product .= '<input type="hidden" name="list_prod_diskon[' . $sales_id . '][' . $val2['product_global_code'] . ']" value="' . $arrdatasales_discount['sales_discount_percentage'] . '"/>';
							}
						}
					}
					//print_r($tambah);


					$rewardPersen .= '<tr><td>' . ++$y . '<input type="hidden" name="diskon_persen[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" id="' . $sales_id . '" class="price_deduction" value="' . $val . '"/></td><td>' . $arrdatasales_discount['sales_discount_percentage']  . '%</td><td>Rp ' . number_format($val) . '</td></tr>';
					$total_potongan2 += $val;
					$reward .= '<input type="hidden" name="discount_id[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" value="' . $arrdatasales_discount['discount_id'] . '"/>';
				}
				foreach ($arrfdisc_terms[$value_reward] as $indexterms2 => $valueterms2) {
					//print_r($valueterms2);
					foreach ($valueterms2 as $indexterms3 => $valueterms3) {
						$jum_syarat += @$hrgbrg[$valueterms3['product_global_code']];
						//if($valueterms3['discount_terms_product_type'] == "2"){
						//echo $hrgbrg[$valueterms3['product_global_code']] * $multiply[$value_reward].'|';
						/*if(@$hrgbrg[$value3['product_global_code']] == "") $hrgbrg[$value3['product_global_code']] = 0;
							$jum += $hrgbrg[$value3['product_global_code']];*/
						//}else if($valueterms3['discount_terms_product_type'] == "1"){
						//echo $valueterms3['discount_terms_product_quantity'] * $multiply[$value_reward].'|';
						//if(@$jumbrg[$value3['product_global_code']] == "") $jumbrg[$value3['product_global_code']] = 0;
						//$jum += $jumbrg[$value3['product_global_code']];
						//}
					}
					//die();
				}
				// foreach ($arrfdisc_reward[$value_reward] as $index_reward2 => $value_reward2) {
				// 	foreach ($value_reward2 as $index_reward3 => $value_reward3) {

				// 		if ($value_reward3['discount_reward_product_type'] == 3) {
				// 			if (@$reward_product[$value_reward3['product_global_code']] != "") {
				// 				$reward_product[$value_reward3['product_global_code']] += $value_reward3['discount_reward_product_quantity'] * $multiply[$value_reward];
				// 			} else {
				// 				$reward_product[$value_reward3['product_global_code']] = $value_reward3['discount_reward_product_quantity'] * $multiply[$value_reward];
				// 			}
				// 			$reward_kd[$value_reward3['product_global_code']] = $value_reward3['nama_produk'];
				// 			$reward .= '<input type="hidden" name="discount_id[]" value="' . $value_reward3['discount_id'] . '" />';
				// 		} else if ($value_reward3['discount_reward_product_type'] == 2) {
				// 			//$jum_syarat = ;
				// 			$reward .= '<input type="hidden" name="discount_id[]" value="' . $value_reward3['discount_id'] . '" />';
				// 			$total_potongan2 += $jum_syarat * $value_reward3['discount_reward_product_percentage'] / 100;
				// 			$rewardPersen .= '<tr><td>' . ++$x . '</td><td>' . $value_reward3['discount_reward_product_percentage'] . '%</td><td>Rp ' . number_format($jum_syarat * $value_reward3['discount_reward_product_percentage'] / 100) . '</td></tr>';
				// 		} else if ($value_reward3['discount_reward_product_type'] == 1) {

				// 			foreach ($arrfdisc_terms[$value_reward] as $arrdisc_terms1) {
				// 				foreach ($arrdisc_terms1 as $arrdisc_terms2) {
				// 					if (isset($jumbrg[$arrdisc_terms2['product_global_code']]) && isset($hrgbrg[$arrdisc_terms2['product_global_code']])) {
				// 					}
				// 				}
				// 			}
				// 			$rewardHarga .= '<tr><td>' . ++$i . '</td><td>Rp ' . number_format($value_reward3['discount_reward_product_deduction_cost'] * $multiply[$value_reward]) . '</td></tr>';
				// 			$total_potongan += $value_reward3['discount_reward_product_deduction_cost'] * $multiply[$value_reward];
				// 			$reward .= '<input type="hidden" name="discount_id[]" value="' . $value_reward3['discount_id'] . '" />';
				// 			//echo $value_reward3['discount_reward_product_deduction_cost']."|";
				// 		}
				// 	}
				// }

				//$leftovers[$value_reward] = '';
				//$arrfdisc_terms[$value_reward];
				//print_r($arrfdisc_reward[$value_reward]);
				//print_r($multiply[$value_reward]);
			}
			//die();
			if (@count(@$reward_kd) > 0) {
				$discount_produk = "";
				$discount_produk_input = "";
				foreach ($reward_kd as $index2x => $value2x) {
					//echo $arrreward_kd = join("', '", $reward_kd);
					$discount_produk .= $value2x . ' ' . $reward_product[$index2x] . ' pcs' . '<br>';
					// $rewardQuantity .= 'Gratis Produk ' . $value2x . ' ' . $reward_product[$index2x] . ' pcs' . '<br>';
					$rewardQuantity .= '<tr><td>1</td><td>' . $value2x . '</td><td>' . $reward_product[$index2x] . '</td></tr>';
					$discount_produk_input .= '<input type="hidden" name="promo_product[' . $index2x . ']" value="' . $reward_product[$index2x] . '" />';
				}
			}
			//if(@count(@$accepted_discount) > 0){

			//$this->
			//}
		}
		$sisa = array();
		if (@count(@$arrfdisc_terms) > 0) {
			foreach ($arrfdisc_terms as $index1 => $value1) {
				if (@$multiply[$index1] != "") $xmultiply = $multiply[$index1];
				else $xmultiply = 0;
				foreach ($value1 as $index2 => $value2) {
					//$jum_sem_awal = $jum_all[$index1][$index2] * $xmultiply;
					$jum_sem = $jum_all[$index1][$index2] * $xmultiply;

					foreach ($value2 as $index3 => $value3) {
						//if($jum_sem > 0){
						if ($value3['sales_discount_terms_type'] == "2") {
							$jumx = ($hrgbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
						} else if ($value3['sales_discount_terms_type'] == "1") {
							$jumx = ($jumbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
						}
						if ($jum_sem <= $jumx) {
							$jum_sem_awal[$index2][$index3] = $jum_sem;
							$sisa[$index2][$index3] = $jumx - $jum_sem;
							$jum_sem = 0;
						} else {
							$jum_sem_awal[$index2][$index3] = $jumx;
							$jum_sem = $jum_sem - $jumx;
							$sisa[$index2][$index3] = 0;
						}
						$accumulation[$index2][$index3] = $jum_sem;
						//}
					}
				}
			}
		}
		$sisa_product = '';
		$tot_hoard = '';
		foreach ($sisa as $index1 => $val1) {
			foreach ($val1 as $index2 => $val2) {
				$sisa_product .= '<input type="hidden" name="sisa_produk[' . $index1 . '][' . $index2 . ']" value="' . $val2 . '" />';
				$sisa_product .= '<input type="hidden" name="produk_accum[' . $index1 . '][' . $index2 . ']" value="' . $accumulation[$index1][$index2] . '" />';
			}
		}
		if (@count(@$tot_harga) > 0) {
			foreach ($tot_harga as $index1xz => $val1xz) {
				$tot_hoard .= '<input type="hidden" name="tot_hoard[' . $index1xz . ']" value="' . $val1xz . '" />';
			}
		}
		$data = array(
			'list_diskon_quantity' => @$rewardQuantity,
			'list_diskon_harga' => @$rewardHarga,
			'list_diskon_persen' => @$rewardPersen,
			'potongan' => @$total_potongan,
			'potongan2' => @$total_potongan2,
			'produk_gratis' => @$discount_produk,
			'produk_insert' => @$discount_produk_input,
			'produk_persen' => @$list_persen_product,
			'reward' => $reward,
			'sisa_produk' => $sisa_product,
			'tot_hoard' => $tot_hoard
		);
		echo json_encode($data);
		die();
	}

	function cek_discount_bayar_clinic($sales_id, $kode, $price, $quantity)
	{
		$rewardHarga = '';
		$rewardQuantity = '';
		$rewardPersen = '';
		$reward = '';
		$list_persen_product = '';
		$arrjoin = "";
		if (@count(@$kode) > 0) {
			$arrjoin = join("', '", @$kode);
		}

		$arrproduct = $this->db->query("SELECT kd_pd, id_produk FROM produk WHERE id_produk IN('$arrjoin')")->result_array();

		foreach ($arrproduct as $indexkd => $valuekd) {
			$arrdict[$valuekd['id_produk']] = $valuekd['kd_pd'];
		}
		if (@count(@$kode) > 0) {
			foreach (@$kode as $index_kode => $value_kode) {
				if (@$hrgbrg[$arrdict[$value_kode]] != "") $hrgbrg[$arrdict[$value_kode]] += $price[$index_kode];
				else $hrgbrg[$arrdict[$value_kode]] = $price[$index_kode];
				if (@$jumbrg[$arrdict[$value_kode]] != "") $jumbrg[$arrdict[$value_kode]] += $quantity[$index_kode];
				else $jumbrg[$arrdict[$value_kode]] = $quantity[$index_kode];
			}
		}
		$arrdiscount = $this->db->query("SELECT * FROM t_sales_discount WHERE sales_id = '$sales_id' AND sales_discount_active = 1")->result_array();
		foreach ($arrdiscount as $indexdisc => $valuedisc) {
			$arrdiscount2[$valuedisc['sales_discount_id']] = $valuedisc;
		}
		$arrdiscount_terms = $this->db->query("SELECT B.sales_id, A.* FROM t_sales_discount_terms A JOIN t_sales_discount B ON B.sales_discount_id = A.sales_discount_id WHERE B.sales_id = '$sales_id' AND B.sales_discount_active = 1")->result_array();

		foreach ($arrdiscount_terms as $indexterms => $valueterms) {
			$arrfdisc_terms[$valueterms['sales_discount_id']][$valueterms['discount_terms_product_id']][$valueterms['sales_discount_terms_id']] = $valueterms;
		}
		/*print_r($arrfdisc_terms);
		die();
		$arrdiscount_reward = $this->db->query("SELECT B.discount_id, C.*, D.nama_produk FROM m_discount_reward A JOIN m_discount B ON B.discount_id = A.discount_id JOIN m_discount_reward_product C ON C.discount_reward_id = A.discount_reward_id LEFT JOIN produk_global D ON D.kode = C.product_kd JOIN m_discount_member E ON E.discount_id = B.discount_id WHERE E.member_status = '" . $_POST['member_status'] . "' AND discount_active = 1")->result_array();
		foreach ($arrdiscount_reward as $indexreward => $valuereward) {
			$arrfdisc_reward[$valuereward['discount_id']][$valuereward['discount_reward_id']][$valuereward['discount_reward_product_id']] = $valuereward;
		}*/
		if (@count(@$arrfdisc_terms) > 0) {
			foreach ($arrfdisc_terms as $index1 => $value1) {
				$total_terms[$index1] = count($value1);
				$accepted_terms[$index1] = 0;
				$min = 0;
				$sementara = "";
				foreach ($value1 as $index2 => $value2) {
					$ix = 0;
					$jum[$index1][$index2] = 0;
					$jum_all[$index1][$index2] = 0;
					foreach ($value2 as $index3 => $value3) {
						if ($value3['sales_discount_terms_type'] == "2") {
							/*if($value3['sales_discount_terms_price'] < ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft'])){ 
								$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']) - $value3['sales_discount_terms_price'];
							}else{*/
							$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']);
							if (@$hrgbrg[$value3['product_global_code']] == "") $hrgbrg[$value3['product_global_code']] = 0;
							//}
							$jum[$index1][$index2] += ($hrgbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
							$jum_all[$index1][$index2] = $value3['sales_discount_terms_price'];
						} else if ($value3['sales_discount_terms_type'] == "1") {
							if (@$jumbrg[$value3['product_global_code']] == "") $jumbrg[$value3['product_global_code']] = 0;
							/*if($value3['sales_discount_terms_quantity'] < ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft'])){ 
								$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']) - $value3['sales_discount_terms_quantity'];
							}else{*/
							$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']);
							//}
							$jum[$index1][$index2] += ($jumbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
							$jum_all[$index1][$index2] = $value3['sales_discount_terms_quantity'];
						}
					}
					if ($ix == 0) {
						if ($value3['sales_discount_terms_type'] == "2") {
							if ($jum[$index1][$index2] >= $value3['sales_discount_terms_price']) {
								$ix = 1;
								$sementara = intdiv($jum[$index1][$index2], $value3['sales_discount_terms_price']);
								if ($min == 0 || $sementara < $min) $min = $sementara;
							}
						} else if ($value3['sales_discount_terms_type'] == "1") {
							if ($jum[$index1][$index2] >= $value3['sales_discount_terms_quantity']) {
								$ix = 1;
								$sementara = intdiv($jum[$index1][$index2], $value3['sales_discount_terms_quantity']);
								if ($min == 0 || $sementara < $min) $min = $sementara;
							}
						}
					}
					if ($ix == 1) $accepted_terms[$index1]++;
				}

				//echo @$accepted_terms[$index1];
				//echo $total_terms[$index1];
				//echo "<hr>";
				if ($arrdiscount2[$index1]['sales_discount_percentage_active_draft'] == 1 && $arrdiscount2[$index1]['sales_discount_percentage'] != "") {
					$accepted_discount[$index1] = $index1;
					$multiply[$index1] = 1;
				} else if (@$accepted_terms[$index1] == $total_terms[$index1]) {
					$accepted_discount[$index1] = $index1;
					if ($min > 0) $multiply[$index1] = $min;
				}
			}
		}
		$total_potongan = 0;
		$total_potongan2 = 0;
		$i = 0;
		$x = 0;
		$y = 0;
		$jum_syarat = 0;
		if (@count(@$accepted_discount) > 0) {
			foreach ($accepted_discount as $index_reward => $value_reward) {
				$arrdatasales_discount = $this->db->query("SELECT A.*, B.sales_detail_id FROM t_sales_discount A JOIN t_sales_discount_detail B ON B.sales_discount_id = A.sales_discount_id WHERE A.sales_discount_id = '" . $index_reward . "'")->row_array();

				foreach ($arrfdisc_terms[$index_reward] as $index1x => $value1x) {
					foreach ($value1x as $index2x => $value2x) {
					}
				}
				if ($arrdatasales_discount['sales_discount_price'] > 0) {
					$rewardHarga .= '<tr><td>' . ++$x . '<input type="hidden" name="diskon_harga[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" id="' . $sales_id . '" class="price_deduction" value="' . ($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]) . '"/></td><td>Rp ' . number_format($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]) . '</td></tr>';
					$total_potongan += ($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]);
					$reward .= '<input type="hidden" name="discount_id[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" value="' . $arrdatasales_discount['discount_id'] . '"/>';
				}

				if ($arrdatasales_discount['sales_discount_percentage'] > 0) {
					if ($arrdiscount2[$index_reward]['sales_discount_percentage_active'] == 0) {
					}
					$val = 0;
					foreach ($arrfdisc_terms[$index_reward] as $val1) {
						foreach ($val1 as $val2) {
							$val += ($hrgbrg[$val2['product_global_code']] * $arrdatasales_discount['sales_discount_percentage'] / 100);
							$list_persen_product .= '<input type="hidden" name="list_prod_diskon[' . $sales_id . '][' . $val2['product_global_code'] . ']" value="' . $arrdatasales_discount['sales_discount_percentage'] . '"/>';
						}
					}
					$rewardPersen .= '<tr><td>' . ++$y . '<input type="hidden" name="diskon_persen[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" id="' . $sales_id . '" class="price_deduction" value="' . $val . '"/></td><td>' . $arrdatasales_discount['sales_discount_percentage']  . '%</td><td>Rp ' . number_format($val) . '</td></tr>';
					$total_potongan2 += $val;
					$reward .= '<input type="hidden" name="discount_id[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" value="' . $arrdatasales_discount['discount_id'] . '"/>';
				}
				foreach ($arrfdisc_terms[$value_reward] as $indexterms2 => $valueterms2) {
					foreach ($valueterms2 as $indexterms3 => $valueterms3) {
						$jum_syarat += @$hrgbrg[$valueterms3['product_global_code']];
					}
				}
			}
			if (@count(@$reward_kd) > 0) {
				$discount_produk = "";
				$discount_produk_input = "";
				foreach ($reward_kd as $index2x => $value2x) {
					$discount_produk .= $value2x . ' ' . $reward_product[$index2x] . ' pcs' . '<br>';
					$rewardQuantity .= '<tr><td>1</td><td>' . $value2x . '</td><td>' . $reward_product[$index2x] . '</td></tr>';
					$discount_produk_input .= '<input type="hidden" name="promo_product[' . $index2x . ']" value="' . $reward_product[$index2x] . '" />';
				}
			}
		}
		$sisa = array();
		if (@count(@$arrfdisc_terms) > 0) {
			foreach ($arrfdisc_terms as $index1 => $value1) {
				if (@$multiply[$index1] != "") $xmultiply = $multiply[$index1];
				else $xmultiply = 0;
				foreach ($value1 as $index2 => $value2) {
					//$jum_sem_awal = $jum_all[$index1][$index2] * $xmultiply;
					$jum_sem = $jum_all[$index1][$index2] * $xmultiply;

					foreach ($value2 as $index3 => $value3) {
						//if($jum_sem > 0){
						if ($value3['sales_discount_terms_type'] == "2") {
							$jumx = ($hrgbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
						} else if ($value3['sales_discount_terms_type'] == "1") {
							$jumx = ($jumbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
						}
						if ($jum_sem <= $jumx) {
							$jum_sem_awal[$index2][$index3] = $jum_sem;
							$sisa[$index2][$index3] = $jumx - $jum_sem;
							$jum_sem = 0;
						} else {
							$jum_sem_awal[$index2][$index3] = $jumx;
							$jum_sem = $jum_sem - $jumx;
							$sisa[$index2][$index3] = 0;
						}
						$accumulation[$index2][$index3] = $jum_sem;
						//}
					}
				}
			}
		}
		$sisa_product = '';
		foreach ($sisa as $index1 => $val1) {
			foreach ($val1 as $index2 => $val2) {
				//$sisa_product .= '<input type="hidden" name="sisa_produk[' . $index1 . '][' . $index2 . ']" value="' . $val2 . '" />';
				//$sisa_product .= '<input type="hidden" name="produk_accum[' . $index1 . '][' . $index2 . ']" value="' . $accumulation[$index1][$index2] . '" />';
				$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod = $val2, sales_discount_terms_accumulation = " . $accumulation[$index1][$index2] . " WHERE sales_discount_terms_id = '$index2'");
			}
		}
		$data = array(
			'potongan' => @$total_potongan,
			'potongan2' => @$total_potongan2,
		);
		echo json_encode($data);
		die();
	}

	function cek_discount_bayar_revisi($sales_id)
	{

		$rewardHarga = '';
		$rewardQuantity = '';
		$rewardPersen = '';
		$reward = '';
		$list_persen_product = '';
		$arrjoin = "";
		if (@count(@$_POST['kode']) > 0) {
			$arrjoin = join("', '", @$_POST['kode']);
		}

		$arrproduct = $this->db->query("SELECT kd_pd, id_produk FROM produk WHERE id_produk IN('$arrjoin')")->result_array();

		foreach ($arrproduct as $indexkd => $valuekd) {
			$arrdict[$valuekd['id_produk']] = $valuekd['kd_pd'];
		}
		if (@count(@$_POST['kode']) > 0) {
			foreach (@$_POST['kode'] as $index_kode => $value_kode) {
				if (@$hrgbrg[$arrdict[$value_kode]] != "") $hrgbrg[$arrdict[$value_kode]] += $_POST['price'][$index_kode];
				else $hrgbrg[$arrdict[$value_kode]] = $_POST['price'][$index_kode];
				if (@$jumbrg[$arrdict[$value_kode]] != "") $jumbrg[$arrdict[$value_kode]] += $_POST['quantity'][$index_kode];
				else $jumbrg[$arrdict[$value_kode]] = $_POST['quantity'][$index_kode];
			}
		}
		$arrdiscount = $this->db->query("SELECT * FROM t_sales_discount WHERE sales_id = '$sales_id' AND sales_discount_active = 1")->result_array();
		foreach ($arrdiscount as $indexdisc => $valuedisc) {
			$arrdiscount2[$valuedisc['sales_discount_id']] = $valuedisc;
		}
		$arrdiscount_terms = $this->db->query("SELECT B.sales_id, A.*, C.discount_terms_id, B.sales_discount_percentage_hoard, B.sales_discount_percentage_hoard_draft FROM t_sales_discount_terms A JOIN t_sales_discount B ON B.sales_discount_id = A.sales_discount_id JOIN m_discount_terms_product C ON C.discount_terms_product_id = A.discount_terms_product_id WHERE B.sales_id = '$sales_id' AND B.sales_discount_active = 1")->result_array();

		foreach ($arrdiscount_terms as $indexterms => $valueterms) {
			$arrfdisc_terms[$valueterms['sales_discount_id']][$valueterms['discount_terms_id']][$valueterms['sales_discount_terms_id']] = $valueterms;
		}
		/*print_r($arrfdisc_terms);
		die();
		$arrdiscount_reward = $this->db->query("SELECT B.discount_id, C.*, D.nama_produk FROM m_discount_reward A JOIN m_discount B ON B.discount_id = A.discount_id JOIN m_discount_reward_product C ON C.discount_reward_id = A.discount_reward_id LEFT JOIN produk_global D ON D.kode = C.product_kd JOIN m_discount_member E ON E.discount_id = B.discount_id WHERE E.member_status = '" . $_POST['member_status'] . "' AND discount_active = 1")->result_array();
		foreach ($arrdiscount_reward as $indexreward => $valuereward) {
			$arrfdisc_reward[$valuereward['discount_id']][$valuereward['discount_reward_id']][$valuereward['discount_reward_product_id']] = $valuereward;
		}*/
		if (@count(@$arrfdisc_terms) > 0) {
			foreach ($arrfdisc_terms as $index1 => $value1) {
				$total_terms[$index1] = count($value1);
				$accepted_terms[$index1] = 0;
				$min = 0;
				$sementara = "";
				foreach ($value1 as $index2 => $value2) {
					$ix = 0;
					$jum[$index1][$index2] = 0;
					$jum_all[$index1][$index2] = 0;
					$tot_harga[$index1] = 0;
					foreach ($value2 as $index3 => $value3) {
						if ($value3['sales_discount_terms_type'] == "2") {
							if (@$hrgbrg[$value3['product_global_code']] == "") $hrgbrg[$value3['product_global_code']] = 0;
							/*if($value3['sales_discount_terms_price'] < ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft'])){ 
								$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']) - $value3['sales_discount_terms_price'];
							}else{*/
							$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod']);
							$tambah_price[$index1][$index2][$index3] = ($value3['sales_discount_percentage_hoard']);
							//}
							$jum[$index1][$index2] += ($hrgbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
							$jum_all[$index1][$index2] = $value3['sales_discount_terms_price'];
						} else if ($value3['sales_discount_terms_type'] == "1") {
							if (@$jumbrg[$value3['product_global_code']] == "") $jumbrg[$value3['product_global_code']] = 0;
							/*if($value3['sales_discount_terms_quantity'] < ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft'])){ 
								$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod'] + $value3['sales_discount_terms_mod_draft']) - $value3['sales_discount_terms_quantity'];
							}else{*/
							$tambah[$index1][$index2][$index3] = ($value3['sales_discount_terms_mod']);
							$tambah_price[$index1][$index2][$index3] = ($value3['sales_discount_percentage_hoard']);
							//}
							$jum[$index1][$index2] += ($jumbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
							$jum_all[$index1][$index2] = $value3['sales_discount_terms_quantity'];
						}
						$tot_harga[$index1] += ($hrgbrg[$value3['product_global_code']]);
					}
					if ($ix == 0) {
						if ($value3['sales_discount_terms_type'] == "2") {
							if ($jum[$index1][$index2] >= $value3['sales_discount_terms_price']) {
								$ix = 1;
								$sementara = intdiv($jum[$index1][$index2], $value3['sales_discount_terms_price']);
								if ($min == 0 || $sementara < $min) $min = $sementara;
							}
						} else if ($value3['sales_discount_terms_type'] == "1") {
							if ($jum[$index1][$index2] >= $value3['sales_discount_terms_quantity']) {
								$ix = 1;
								$sementara = intdiv($jum[$index1][$index2], $value3['sales_discount_terms_quantity']);
								if ($min == 0 || $sementara < $min) $min = $sementara;
							}
						}
					}
					if ($ix == 1) $accepted_terms[$index1]++;
				}

				//echo @$accepted_terms[$index1];
				//echo $total_terms[$index1];
				//echo "<hr>";
				if ($arrdiscount2[$index1]['sales_discount_percentage_active'] == 1 && $arrdiscount2[$index1]['sales_discount_percentage'] != "") {
					$accepted_discount[$index1] = $index1;
					$multiply[$index1] = 1;
				} else if (@$accepted_terms[$index1] == $total_terms[$index1]) {
					$accepted_discount[$index1] = $index1;
					if ($min > 0) $multiply[$index1] = $min;
				}
			}
		}
		$total_potongan = 0;
		$total_potongan2 = 0;
		$i = 0;
		$x = 0;
		$y = 0;
		$jum_syarat = 0;
		if (@count(@$accepted_discount) > 0) {
			foreach ($accepted_discount as $index_reward => $value_reward) {
				$arrdatasales_discount = $this->db->query("SELECT A.*, B.sales_detail_id FROM t_sales_discount A JOIN t_sales_discount_detail B ON B.sales_discount_id = A.sales_discount_id WHERE A.sales_discount_id = '" . $index_reward . "'")->row_array();

				foreach ($arrfdisc_terms[$index_reward] as $index1x => $value1x) {
					foreach ($value1x as $index2x => $value2x) {
					}
				}
				if ($arrdatasales_discount['sales_discount_product'] == '1') {
					$arrdatasales_discount_product = $this->db->query("SELECT * FROM t_sales_discount_product WHERE sales_discount_id = '" . $index_reward . "'")->result_array();
					foreach ($arrdatasales_discount_product as $indexproduct => $valueproduct) {
						$rewardQuantity .= '<tr><td>' . ++$i . '<input type="hidden" name="dikson_produk[' . $sales_id . '][' . $valueproduct['sales_detail_id'] . ']" value="' . ($valueproduct['sales_discount_product_quantity'] * $multiply[$index_reward]) . '"/></td><td>' . $valueproduct['product_global_code'] . '</td><td>' . ($valueproduct['sales_discount_product_quantity'] * $multiply[$index_reward]) . ' pcs</td></tr>';
						$reward .= '<input type="hidden" name="discount_id[' . $sales_id . '][' . $valueproduct['sales_detail_id'] . ']" value="' . $arrdatasales_discount['discount_id'] . '"';
						// print_r($valueproduct);
					}
				}
				if ($arrdatasales_discount['sales_discount_price'] > 0) {
					$rewardHarga .= '<tr><td>' . ++$x . '<input type="hidden" name="diskon_harga[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" id="' . $sales_id . '" class="price_deduction" value="' . ($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]) . '"/></td><td>Rp ' . number_format($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]) . '</td></tr>';
					$total_potongan += ($arrdatasales_discount['sales_discount_price'] * $multiply[$index_reward]);
					$reward .= '<input type="hidden" name="discount_id[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" value="' . $arrdatasales_discount['discount_id'] . '"/>';
				}

				if ($arrdatasales_discount['sales_discount_percentage'] > 0) {

					$val = 0;
					if ($arrdiscount2[$index_reward]['sales_discount_percentage_active'] == 0) {
						foreach ($arrfdisc_terms[$index_reward] as $index1xx => $val1) {
							foreach ($val1 as $index2xx => $val2) {

								$val += (($hrgbrg[$val2['product_global_code']] + $tambah_price[$index_reward][$index1xx][$index2xx]) * $arrdatasales_discount['sales_discount_percentage'] / 100);
								$list_persen_product .= '<input type="hidden" name="list_prod_diskon[' . $sales_id . '][' . $val2['product_global_code'] . ']" value="' . $arrdatasales_discount['sales_discount_percentage'] . '"/>';
							}
						}
					} else {
						foreach ($arrfdisc_terms[$index_reward] as $index1xx => $val1) {
							foreach ($val1 as $index2xx => $val2) {

								$val += (($hrgbrg[$val2['product_global_code']]) * $arrdatasales_discount['sales_discount_percentage'] / 100);
								$list_persen_product .= '<input type="hidden" name="list_prod_diskon[' . $sales_id . '][' . $val2['product_global_code'] . ']" value="' . $arrdatasales_discount['sales_discount_percentage'] . '"/>';
							}
						}
					}
					//print_r($tambah);


					$rewardPersen .= '<tr><td>' . ++$y . '<input type="hidden" name="diskon_persen[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" id="' . $sales_id . '" class="price_deduction" value="' . $val . '"/></td><td>' . $arrdatasales_discount['sales_discount_percentage']  . '%</td><td>Rp ' . number_format($val) . '</td></tr>';
					$total_potongan2 += $val;
					$reward .= '<input type="hidden" name="discount_id[' . $sales_id . '][' . $arrdatasales_discount['sales_detail_id'] . ']" value="' . $arrdatasales_discount['discount_id'] . '"/>';
				}
				foreach ($arrfdisc_terms[$value_reward] as $indexterms2 => $valueterms2) {
					//print_r($valueterms2);
					foreach ($valueterms2 as $indexterms3 => $valueterms3) {
						$jum_syarat += @$hrgbrg[$valueterms3['product_global_code']];
						//if($valueterms3['discount_terms_product_type'] == "2"){
						//echo $hrgbrg[$valueterms3['product_global_code']] * $multiply[$value_reward].'|';
						/*if(@$hrgbrg[$value3['product_global_code']] == "") $hrgbrg[$value3['product_global_code']] = 0;
							$jum += $hrgbrg[$value3['product_global_code']];*/
						//}else if($valueterms3['discount_terms_product_type'] == "1"){
						//echo $valueterms3['discount_terms_product_quantity'] * $multiply[$value_reward].'|';
						//if(@$jumbrg[$value3['product_global_code']] == "") $jumbrg[$value3['product_global_code']] = 0;
						//$jum += $jumbrg[$value3['product_global_code']];
						//}
					}
					//die();
				}
				// foreach ($arrfdisc_reward[$value_reward] as $index_reward2 => $value_reward2) {
				// 	foreach ($value_reward2 as $index_reward3 => $value_reward3) {

				// 		if ($value_reward3['discount_reward_product_type'] == 3) {
				// 			if (@$reward_product[$value_reward3['product_global_code']] != "") {
				// 				$reward_product[$value_reward3['product_global_code']] += $value_reward3['discount_reward_product_quantity'] * $multiply[$value_reward];
				// 			} else {
				// 				$reward_product[$value_reward3['product_global_code']] = $value_reward3['discount_reward_product_quantity'] * $multiply[$value_reward];
				// 			}
				// 			$reward_kd[$value_reward3['product_global_code']] = $value_reward3['nama_produk'];
				// 			$reward .= '<input type="hidden" name="discount_id[]" value="' . $value_reward3['discount_id'] . '" />';
				// 		} else if ($value_reward3['discount_reward_product_type'] == 2) {
				// 			//$jum_syarat = ;
				// 			$reward .= '<input type="hidden" name="discount_id[]" value="' . $value_reward3['discount_id'] . '" />';
				// 			$total_potongan2 += $jum_syarat * $value_reward3['discount_reward_product_percentage'] / 100;
				// 			$rewardPersen .= '<tr><td>' . ++$x . '</td><td>' . $value_reward3['discount_reward_product_percentage'] . '%</td><td>Rp ' . number_format($jum_syarat * $value_reward3['discount_reward_product_percentage'] / 100) . '</td></tr>';
				// 		} else if ($value_reward3['discount_reward_product_type'] == 1) {

				// 			foreach ($arrfdisc_terms[$value_reward] as $arrdisc_terms1) {
				// 				foreach ($arrdisc_terms1 as $arrdisc_terms2) {
				// 					if (isset($jumbrg[$arrdisc_terms2['product_global_code']]) && isset($hrgbrg[$arrdisc_terms2['product_global_code']])) {
				// 					}
				// 				}
				// 			}
				// 			$rewardHarga .= '<tr><td>' . ++$i . '</td><td>Rp ' . number_format($value_reward3['discount_reward_product_deduction_cost'] * $multiply[$value_reward]) . '</td></tr>';
				// 			$total_potongan += $value_reward3['discount_reward_product_deduction_cost'] * $multiply[$value_reward];
				// 			$reward .= '<input type="hidden" name="discount_id[]" value="' . $value_reward3['discount_id'] . '" />';
				// 			//echo $value_reward3['discount_reward_product_deduction_cost']."|";
				// 		}
				// 	}
				// }

				//$leftovers[$value_reward] = '';
				//$arrfdisc_terms[$value_reward];
				//print_r($arrfdisc_reward[$value_reward]);
				//print_r($multiply[$value_reward]);
			}
			//die();
			if (@count(@$reward_kd) > 0) {
				$discount_produk = "";
				$discount_produk_input = "";
				foreach ($reward_kd as $index2x => $value2x) {
					//echo $arrreward_kd = join("', '", $reward_kd);
					$discount_produk .= $value2x . ' ' . $reward_product[$index2x] . ' pcs' . '<br>';
					// $rewardQuantity .= 'Gratis Produk ' . $value2x . ' ' . $reward_product[$index2x] . ' pcs' . '<br>';
					$rewardQuantity .= '<tr><td>1</td><td>' . $value2x . '</td><td>' . $reward_product[$index2x] . '</td></tr>';
					$discount_produk_input .= '<input type="hidden" name="promo_product[' . $index2x . ']" value="' . $reward_product[$index2x] . '" />';
				}
			}
			//if(@count(@$accepted_discount) > 0){

			//$this->
			//}
		}
		$sisa = array();
		if (@count(@$arrfdisc_terms) > 0) {
			foreach ($arrfdisc_terms as $index1 => $value1) {
				if (@$multiply[$index1] != "") $xmultiply = $multiply[$index1];
				else $xmultiply = 0;
				foreach ($value1 as $index2 => $value2) {
					//$jum_sem_awal = $jum_all[$index1][$index2] * $xmultiply;
					$jum_sem = $jum_all[$index1][$index2] * $xmultiply;

					foreach ($value2 as $index3 => $value3) {
						//if($jum_sem > 0){
						if ($value3['sales_discount_terms_type'] == "2") {
							$jumx = ($hrgbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
						} else if ($value3['sales_discount_terms_type'] == "1") {
							$jumx = ($jumbrg[$value3['product_global_code']] + $tambah[$index1][$index2][$index3]);
						}
						if ($jum_sem <= $jumx) {
							$jum_sem_awal[$index2][$index3] = $jum_sem;
							$sisa[$index2][$index3] = $jumx - $jum_sem;
							$jum_sem = 0;
						} else {
							$jum_sem_awal[$index2][$index3] = $jumx;
							$jum_sem = $jum_sem - $jumx;
							$sisa[$index2][$index3] = 0;
						}
						$accumulation[$index2][$index3] = $jum_sem;
						//}
					}
				}
			}
		}
		$sisa_product = '';
		$tot_hoard = '';
		foreach ($sisa as $index1 => $val1) {
			foreach ($val1 as $index2 => $val2) {
				$sisa_product .= '<input type="hidden" name="sisa_produk[' . $index1 . '][' . $index2 . ']" value="' . $val2 . '" />';
				$sisa_product .= '<input type="hidden" name="produk_accum[' . $index1 . '][' . $index2 . ']" value="' . $accumulation[$index1][$index2] . '" />';
			}
		}
		if (@count(@$tot_harga) > 0) {
			foreach ($tot_harga as $index1xz => $val1xz) {
				$tot_hoard .= '<input type="hidden" name="tot_hoard[' . $index1xz . ']" value="' . $val1xz . '" />';
			}
		}
		$data = array(
			'list_diskon_quantity' => @$rewardQuantity,
			'list_diskon_harga' => @$rewardHarga,
			'list_diskon_persen' => @$rewardPersen,
			'potongan' => @$total_potongan,
			'potongan2' => @$total_potongan2,
			'produk_gratis' => @$discount_produk,
			'produk_insert' => @$discount_produk_input,
			'produk_persen' => @$list_persen_product,
			'reward' => $reward,
			'sisa_produk' => $sisa_product,
			'tot_hoard' => $tot_hoard
		);
		echo json_encode($data);
		die();
	}

	function check_fifo_beta()
	{

		//$arrglobal = $this->db->query("SELECT * FROM produk A WHERE A.id_produk = '$product_id'")->row_array();
		//	$arrglobalkd = $arrglobal['kd_pd'];
		$arrin = $this->db->query("SELECT A.seller_id, B.*, C.kd_pd FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN produk C ON C.id_produk = B.product_id WHERE B.pending_quantity > 0")->result_array();
		$arrinfix = array();

		foreach ($arrin as $indexinawal => $valueinawal) {
			$arrinfix[$valueinawal['kd_pd']][$valueinawal['seller_id']][] = $valueinawal;
			$arrkd_pd2[$valueinawal['kd_pd']] = $valueinawal['kd_pd'];
		}
		//$arrout = $this->db->query("SELECT A.member_code, B.* FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN produk C ON C.id_produk = B.product_id WHERE C.kd_pd = '$arrglobalkd' AND B.pending_beta_quantity > 0")->result_array();
		//$arroutfix = array();
		//foreach ($arrout as $indexoutawal => $valueoutawal) {
		//	$arroutfix[$valueoutawal['member_code']][] = $valueoutawal;
		//}
		$arrout = $this->db->query("SELECT * FROM t_package_trial_detail_draft A JOIN produk B ON B.id_produk = A.product_id ORDER BY B.kd_pd")->result_array();
		$arroutfix = array();
		foreach ($arrout as $indexoutawal => $valueoutawal) {
			$arroutfix[$valueoutawal['kd_pd']][$valueoutawal['seller_id']][] = $valueoutawal;
			$arrkd_pd[$valueoutawal['kd_pd']] = $valueoutawal['kd_pd'];
		}
		$hitung = 0;
		foreach ($arrkd_pd as $index_pd => $value_pd) {
			$hitung = 0;
			foreach ($arrinfix[$value_pd] as $indexinfix => $valueinfix) {
				//echo @count(@$arroutfix[@$indexinfix]);
				//echo $value_pd;

				if (!empty(@$arroutfix[$value_pd][$indexinfix]) > 0) {
					foreach ($arroutfix[$value_pd][$indexinfix] as $indexout => $valueout) {
						if ($hitung == 0) {
							$hitung = $valueout['package_detail_quantity'];
							$valueout['package_detail_id'];
							$valueout['package_id'];
							foreach ($valueinfix as $indexin => $valuein) {

								if ($hitung != 0 && $valuein['pending_quantity'] != 0) {
									$xdata = array();
									$xupdate = array();
									if ($hitung < $valuein['pending_quantity']) {

										if (@$temp_in[$indexin]['pending_quantity'] == 0 || @$temp_in[$indexin]['pending_quantity'] == "") $temp_in[$indexin]['pending_quantity'] = $valuein['connected_quantity'];

										$xupdate['connected_quantity'] = $temp_in[$indexin]['pending_quantity'] + $hitung;
										$temp_in[$indexin]['pending_quantity'] = $xupdate['connected_quantity'];
										$arrin[$indexin]['pending_quantity'] = $valuein['pending_quantity'] - $temp_in[$indexin]['pending_quantity'] + $valuein['connected_quantity'];
										$alpha_quantity = $hitung;
										$xupdate['pending_quantity'] = $arrin[$indexin]['pending_quantity'];
										$hitung = 0;
									} else {
										$hitung = $hitung - $valuein['pending_quantity'];
										$alpha_quantity = $valuein['pending_quantity'];
										$arrin[$indexin]['pending_quantity'] = 0;
										$xupdate['pending_quantity'] = 0;
										$xupdate['connected_quantity'] = $valuein['connected_quantity'] + $valuein['pending_quantity'];
										$xupdate['connected_status'] = 2;
									}
									//print_r($xupdate);
									//$this->update_global('t_sales_detail', $xupdate, ['sales_detail_id' => $valuein['sales_detail_id']]);

									$xdata['sales_id'] = $valuein['sales_id'];
									$xdata['sales_detail_id'] = $valuein['sales_detail_id'];
									$xdata['package_detail_id'] = $valueout['package_detail_id'];
									$xdata['bridge_beta_quantity'] = $alpha_quantity;
									$xdata['bridge_beta_price'] = $valuein['sales_detail_price'];
									$xdata['product_global_code'] = $value_pd;
									$xdata['bridge_beta_pending_quantity'] = $alpha_quantity;
									print_r($xdata);

									//$this->insert_global('t_bridge_beta', $xdata);
								}
							}
							$xupdate2 = array();
							$xupdate2['connected_beta_quantity'] = 0 + $valueout['package_detail_quantity'] - $hitung;
							$xupdate2['package_detail_quantity'] = $hitung;
							if ($hitung == 0) $xupdate2['connected_beta_status'] = 2;
							//$this->update_global('t_package_trial_detail', $xupdate2, ['package_detail_id' => $valueout['package_detail_id']]);
						}
					}
				}
			}
		}
		die();
		//$arrin = $this->db->query("SELECT A.seller_id, B.* FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id JOIN produk C ON C.id_produk = B.product_id WHERE C.kd_pd = '$arrglobalkd' AND B.pending_quantity > 0")->result_array();
		//$arrout = $this->db->query("SELECT A.member_code, B.* FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN produk C ON C.id_produk = B.product_id WHERE C.kd_pd = '$arrglobalkd' AND B.pending_beta_quantity > 0")->result_array();
		return '';
	}

	public function test_xy()
	{
		$tgl1 = date('Y-') . '02-31';
		$tgl2 = date('Y-') . '02-28';
		$awal1 = substr(str_replace('-', '', $tgl1), 0, 6);
		$akhir1 = substr(str_replace('-', '', $tgl1), 6, 8);
		$awal2 = substr(str_replace('-', '', $tgl2), 0, 6);
		$akhir2 = substr(str_replace('-', '', $tgl2), 6, 8);
		$tgl_akhir1 = date("t", strtotime($tgl1));
		for ($i = ($awal1); $i <= $awal2; $i++) {
			if (substr($i, 4, 6) == 13) {
				$i = (substr($i, 0, 4) + 1) . '01';
			}
			$tgl_akhir = date("Y-m-t", strtotime(substr($i, 0, 4) . '-' . substr($i, 4, 6) . '-01'));
			if ($awal2 == $i) {

				if ($akhir1 > substr($tgl_akhir, 8, 10)) {
					if ($akhir1 <= $akhir2 || ($akhir1 == $tgl_akhir1 && $akhir2 == substr($tgl_akhir, 8, 10))) {
						echo $tgl_akhir;
					}
				} else {
					if ($akhir1 <= $akhir2) {
						echo substr($tgl_akhir, 0, 8) . $akhir1;
					}
				}
			} else {

				if ($akhir1 > substr($tgl_akhir, 8, 10)) {
					echo substr($tgl_akhir, 0, 8) . substr($tgl_akhir, 8, 10);
				} else {
					echo substr($tgl_akhir, 0, 8) . $akhir1;
				}
			}
			echo "<br>";
			if (substr($i, 4, 6) == 12) $i += 88;
		}
		//$d1 = new DateTime("$tgl");
		//$d2 = new DateTime('2020-02-29');
		//$x = $d1->diff($d2)->m + ($d1->diff($d2)->y*12);
		//for ($n=0; $n<$x; $n++) {
		//	echo $n;
		//}
		//echo date("Y-m-t", strtotime(date('Y-m-d')));
		//echo $x;
		//die();
		//$a_date = "2021-03-23";
		//echo date("Y-m-t", strtotime($a_date));
		//die();

	}

	public function test_xxy()
	{
		$tgl1 = date('Y-') . '02-31';
		$tgl2 = date('Y-') . '02-28';
		$awal1 = substr(str_replace('-', '', $tgl1), 0, 6);
		$akhir1 = substr(str_replace('-', '', $tgl1), 6, 8);
		$awal2 = substr(str_replace('-', '', $tgl2), 0, 6);
		$akhir2 = substr(str_replace('-', '', $tgl2), 6, 8);
		$tgl_akhir1 = date("t", strtotime($tgl1));
		for ($i = ($awal1); $i <= $awal2; $i++) {
			if (substr($i, 4, 6) == 13) {
				$i = (substr($i, 0, 4) + 1) . '01';
			}
			$tgl_akhir = date("Y-m-t", strtotime(substr($i, 0, 4) . '-' . substr($i, 4, 6) . '-01'));
			if ($awal2 == $i) {

				if ($akhir1 > substr($tgl_akhir, 8, 10)) {
					if ($akhir1 <= $akhir2 || ($akhir1 == $tgl_akhir1 && $akhir2 == substr($tgl_akhir, 8, 10))) {
						echo $tgl_akhir;
					}
				} else {
					if ($akhir1 <= $akhir2) {
						echo substr($tgl_akhir, 0, 8) . $akhir1;
					}
				}
			} else {

				if ($akhir1 > substr($tgl_akhir, 8, 10)) {
					echo substr($tgl_akhir, 0, 8) . substr($tgl_akhir, 8, 10);
				} else {
					echo substr($tgl_akhir, 0, 8) . $akhir1;
				}
			}
			echo "<br>";
			if (substr($i, 4, 6) == 12) $i += 88;
		}
		//$d1 = new DateTime("$tgl");
		//$d2 = new DateTime('2020-02-29');
		//$x = $d1->diff($d2)->m + ($d1->diff($d2)->y*12);
		//for ($n=0; $n<$x; $n++) {
		//	echo $n;
		//}
		//echo date("Y-m-t", strtotime(date('Y-m-d')));
		//echo $x;
		//die();
		//$a_date = "2021-03-23";
		//echo date("Y-m-t", strtotime($a_date));
		//die();

	}

	public function test_p()
	{
		$yourInt = 166666666.67;
		$divided = 3;
		$remainder = round($yourInt - (round($yourInt / $divided, 2) * $divided), 2);
		$third = round($yourInt / $divided, 2);
		$lastBit = round($third + $remainder, 2);
		for ($i = 1; $i <= $divided; $i++) {
			if ($i == $divided) {
				$data[] =  $lastBit;
			} else {
				$data[] = $third;
			}
		}
		print_r($data);
	}

	public function test_y()
	{
		$tgl1 = '2020-12-03';
		$tgl2 = date('Y-m-d');
		$awal1 = substr(str_replace('-', '', $tgl1), 0, 6);
		$akhir1 = substr(str_replace('-', '', $tgl1), 6, 8);
		$awal2 = substr(str_replace('-', '', $tgl2), 0, 6);
		$akhir2 = substr(str_replace('-', '', $tgl2), 6, 8);
		$tgl_akhir1 = date("t", strtotime($tgl1));
		for ($i = ($awal1 + 1); $i <= $awal2; $i++) {
			if (substr($i, 4, 6) == 13) {
				$i = (substr($i, 0, 4) + 1) . '01';
			}
			$tgl_akhir = date("Y-m-t", strtotime(substr($i, 0, 4) . '-' . substr($i, 4, 6) . '-01'));
			if ($awal2 == $i) {

				if ($akhir1 > substr($tgl_akhir, 8, 10)) {
					if ($akhir1 <= $akhir2 || ($akhir1 == $tgl_akhir1 && $akhir2 == substr($tgl_akhir, 8, 10))) {
						echo $tgl_akhir;
					}
				} else {
					if ($akhir1 <= $akhir2) {
						echo substr($tgl_akhir, 0, 8) . $akhir1;
					}
				}
			} else {

				if ($akhir1 > substr($tgl_akhir, 8, 10)) {
					echo substr($tgl_akhir, 0, 8) . substr($tgl_akhir, 8, 10);
				} else {
					echo substr($tgl_akhir, 0, 8) . $akhir1;
				}
			}
			echo "<br>";
			if (substr($i, 4, 6) == 12) $i += 88;
		}
		die();
		//$d1 = new DateTime("$tgl");
		//$d2 = new DateTime('2020-02-29');
		//$x = $d1->diff($d2)->m + ($d1->diff($d2)->y*12);
		//for ($n=0; $n<$x; $n++) {
		//	echo $n;
		//}
		//echo date("Y-m-t", strtotime(date('Y-m-d')));
		//echo $x;
		//die();
		//$a_date = "2021-03-23";
		//echo date("Y-m-t", strtotime($a_date));
		//die();

	}

	public function connection_test()
	{
		$arrdatakirim = '';
	}

	public function test_xx()
	{
		$arrdata = $this->db->query("SELECT A.id as id1, A.nama as nama1, A.kode as kode1, B.id as id2, B.nama as nama2, B.kode as kode2, C.id as id3, C.nama as nama3, C.kode as kode3, D.coa_total_debit as debit4, D.coa_total_credit as credit4, E.coa_total_debit as debit3, E.coa_total_credit as credit3 FROM coa_1 A JOIN coa_2 B ON B.coa1_id = A.id JOIN coa_3 C ON C.coa2_id = B.id LEFT JOIN (SELECT SUM(Y.coa_total_debit) as coa_total_debit, SUM(Y.coa_total_credit) as coa_total_credit, Z.coa3_id FROM coa_4 Z JOIN t_coa_total Y ON Y.coa_id = Z.id AND Y.coa_level = 4 GROUP BY Z.coa3_id) D ON D.coa3_id = C.id LEFT JOIN t_coa_total E ON E.coa_id = C.id AND E.coa_level = 3")->result_array();
		foreach ($arrdata as $index => $value) {
			if (@$arrdata2[$value['id1']]['id1'] == "") {
				$data1['id1'] = $value['id1'];
				$data1['nama1'] = $value['nama1'];
				$data1['kode1'] = $value['kode1'];
				$arrdata2[$value['id1']] = $data1;
			}
			if (@$arrdata2[$value['id1']][$value['id2']]['id2'] == "") {
				$data2['id2'] = $value['id2'];
				$data2['nama2'] = $value['nama2'];
				$data2['kode2'] = $value['kode2'];
				$arrdata2[$value['id1']][$value['id2']] = $data2;
			}
			if (@$arrdata2[$value['id1']][$value['id2']][$value['id3']]['id3'] == "") {
				$data3['id3'] = $value['id3'];
				$data3['nama3'] = $value['nama3'];
				$data3['kode3'] = $value['kode3'];
				$data3['debit3'] = $value['debit3'];
				$data3['credit3'] = $value['credit3'];
				$data3['debit4'] = $value['debit4'];
				$data3['credit4'] = $value['credit4'];
				$arrdata2[$value['id1']][$value['id2']][$value['id3']] = $data3;
			}
		}
		print_r($arrdata2);
		die();
		print_r($arrdata);
		die();
	}

	function go_getter()
	{
		die('xx');
	}

	public function test_xxx()
	{
		$arrbln = array(
			'01' => '01',
			'02' => '02',
			'03' => '03',
			'04' => '04',
			'05' => '05',
			'06' => '06',
			'07' => '07',
			'08' => '08',
			'09' => '09',
			'10' => '10',
			'11' => '11',
			'12' => '12'
		);
		$tahun = '2021';
		$arrdata = $this->db->query("SELECT A.id as id1, A.nama as nama1, A.kode as kode1, B.id as id2, B.nama as nama2, B.kode as kode2, C.id as id3, C.nama as nama3, C.kode as kode3, D.coa_total_debit as debit4, D.coa_total_credit as credit4, E.coa_total_debit as debit3, E.coa_total_credit as credit3 FROM coa_1 A JOIN coa_2 B ON B.coa1_id = A.id JOIN coa_3 C ON C.coa2_id = B.id LEFT JOIN (SELECT SUM(Y.coa_total_debit) as coa_total_debit, SUM(Y.coa_total_credit) as coa_total_credit, Z.coa3_id FROM coa_4 Z JOIN t_coa_total Y ON Y.coa_id = Z.id AND Y.coa_level = 4 GROUP BY Z.coa3_id) D ON D.coa3_id = C.id LEFT JOIN t_coa_total E ON E.coa_id = C.id AND E.coa_level = 3")->result_array();
		$arrtotal_history4 = $this->db->query("SELECT SUBSTR(coa_total_date, 6, 2) as coa_total_date, coa3_id, SUM(coa_total_debit) as coa_total_debit, SUM(coa_total_credit) as coa_total_credit FROM t_coa_total_history A JOIN coa_4 B ON B.id = A.coa_id WHERE SUBSTR(coa_total_date, 1, 4) = '$tahun' AND coa_level = 4 GROUP BY coa3_id, coa_total_date")->result_array();
		foreach ($arrtotal_history4 as $index2 => $value2) {
			$arrhistory4[$value2['coa3_id']][$value2['coa_total_date']] = $value2;
		}
		$arrtotal_history3 = $this->db->query("SELECT SUBSTR(coa_total_date, 6, 2) as coa_total_date, coa_id, coa_total_debit, coa_total_credit FROM t_coa_total_history WHERE SUBSTR(coa_total_date, 1, 4) = '2021' AND coa_level = 3")->result_array();
		foreach ($arrtotal_history3 as $index3 => $value3) {
			$arrhistory3[$value3['coa3_id']][$value3['coa_total_date']] = $value3;
		}
		foreach ($arrdata as $index => $value) {
			if (@$arrdata2[$value['id1']]['id1'] == "") {
				$data1['id1'] = $value['id1'];
				$data1['nama1'] = $value['nama1'];
				$data1['kode1'] = $value['kode1'];
				$arrdata2[$value['id1']] = $data1;
			}
			if (@$arrdata2[$value['id1']]['coa2'][$value['id2']]['id2'] == "") {
				$data2['id2'] = $value['id2'];
				$data2['nama2'] = $value['nama2'];
				$data2['kode2'] = $value['kode2'];
				$arrdata2[$value['id1']]['coa2'][$value['id2']] = $data2;
			}
			if (@$arrdata2[$value['id1']]['coa2'][$value['id2']]['coa3'][$value['id3']]['id3'] == "") {
				$data3['id3'] = $value['id3'];
				$data3['nama3'] = $value['nama3'];
				$data3['kode3'] = $value['kode3'];
				$data3['debit3'] = $value['debit3'];
				$data3['credit3'] = $value['credit3'];
				$data3['debit4'] = $value['debit4'];
				$data3['credit4'] = $value['credit4'];
				$datahistory4 = array();
				foreach ($arrbln as $indexx => $valuex) {
					$datahistory4[$indexx] = @$arrhistory4[@$data3['id3']][@$indexx];
				}
				$data3['history4'] = $datahistory4;
				$datahistory3 = array();
				foreach ($arrbln as $indexx => $valuex) {
					$datahistory3[$indexx] = @$arrhistory3[@$data3['id3']][@$indexx];
				}
				$data3['history3'] = $datahistory3;
				$arrdata2[$value['id1']]['coa2'][$value['id2']]['coa3'][$value['id3']] = $data3;
			}
		}
		print_r($arrdata2);
		die();
		print_r($arrdata);
		die();
	}

	public function String2Date($dTgl)
	{
		list($cMonth, $cDate, $cYear)	= explode("/", $dTgl);
		if (strlen($cYear) == 4) {
			$dTgl	= $cYear . "-" . $cMonth . "-" . $cDate;
		}
		return $dTgl;
	}

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function test_print()
	{
		$this->load->view('test_print');
	}

	public function chart()
	{
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$bulan = date('m');
		$tahun = date('Y');
		if (@$_POST['cBulan'] != "") $bulan = $_POST['cBulan'];
		if (@$_POST['cTahun'] != "") $tahun = $_POST['cTahun'];
		$datacontent['tahun'] = $tahun;
		$datacontent['bulan'] = $bulan;
		$datacontent['ind_tanggal'] = $arrbln[$bulan] . " " . $tahun;
		$datacontent['url'] = $this->url_;
		if ($_SESSION['role_id'] == "5") {
			$datacontent['title'] = 'Laporan Harian Barang Keluar';
			$data['file'] = 'Laporan';
			$tgl_terima2 = $tahun . '-' . $bulan;
			$arrdat_masuk = $this->db->query("SELECT SUM(jumlah) as jum, RIGHT(tgl_terima, 2) as tgl FROM terima_produk WHERE LEFT(tgl_terima, 7) = '$tgl_terima2' GROUP BY tgl_terima")->result_array();
			foreach ($arrdat_masuk as $index_masuk => $value_masuk) {
				$datacontent['arrdat_masuk'][$value_masuk['tgl']] = $value_masuk['jum'];
			}
			$arrdat_keluar = $this->db->query("SELECT SUM(A.package_detail_quantity) as jum, RIGHT(B.package_date, 2) as tgl FROM t_package_trial_detail A LEFT JOIN t_package_trial B ON B.package_id = A.package_id WHERE LEFT(B.package_date, 7) = '$tgl_terima2' GROUP BY B.package_date")->result_array();
			foreach ($arrdat_keluar as $index_keluar => $value_keluar) {
				$datacontent['arrdat_keluar'][$value_keluar['tgl']] = $value_keluar['jum'];
			}
			$datacontent['arrout_topten'] = $this->db->query("SELECT SUM(A.package_detail_quantity) as jum, nama_produk FROM t_package_trial_detail A LEFT JOIN t_package_trial B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = A.product_id WHERE LEFT(B.package_date, 7) = '$tgl_terima2' GROUP BY A.product_id ORDER BY SUM(A.package_detail_quantity) DESC LIMIT 10")->result_array();
			$datacontent['arrout_bottomten'] = $this->db->query("SELECT SUM(A.package_detail_quantity) as jum, nama_produk FROM t_package_trial_detail A LEFT JOIN t_package_trial B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = A.product_id WHERE LEFT(B.package_date, 7) = '$tgl_terima2' GROUP BY A.product_id ORDER BY SUM(A.package_detail_quantity) ASC LIMIT 10")->result_array();
			$datacontent['arrin_factory'] = $this->db->query("SELECT SUM(A.jumlah) as jum, C.nama_factory FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po LEFT JOIN factory C ON C.id_factory = B.factory WHERE LEFT(A.tgl_terima, 7) = '$tgl_terima2' GROUP BY B.factory")->result_array();
			$data['content'] = $this->load->view($this->url_ . '/chart_barang_jadi', $datacontent, TRUE);
		} else if ($_SESSION['role_id'] == "17" || $_SESSION['role_id'] == "7") {
			$datacontent['title'] = 'Laporan Harian Barang Keluar';
			$data['file'] = 'Laporan';
			$tgl_terima2 = $tahun . '-' . $bulan;
			$arrdat_masuk = $this->db->query("SELECT SUM(account_detail_sales_product_allow) as jum, RIGHT(A.account_detail_sales_date, 2) as tgl FROM t_account_detail_sales A LEFT JOIN t_account_detail_sales_product B ON B.account_detail_sales_id = A.account_detail_sales_id WHERE LEFT(account_detail_sales_date, 7) = '$tgl_terima2' GROUP BY A.account_detail_sales_date")->result_array();
			foreach ($arrdat_masuk as $index_masuk => $value_masuk) {
				$datacontent['arrdat_masuk'][$value_masuk['tgl']] = $value_masuk['jum'];
			}
			$arrdat_keluar = $this->db->query("SELECT SUM(A.package_detail_quantity) as jum, RIGHT(B.package_date, 2) as tgl FROM t_package_trial_detail A LEFT JOIN t_package_trial B ON B.package_id = A.package_id WHERE LEFT(B.package_date, 7) = '$tgl_terima2' GROUP BY B.package_date")->result_array();
			foreach ($arrdat_keluar as $index_keluar => $value_keluar) {
				$datacontent['arrdat_keluar'][$value_keluar['tgl']] = $value_keluar['jum'];
			}
			$datacontent['arrout_topten'] = $this->db->query("SELECT SUM(B.account_detail_sales_product_allow) as jum, C.nama_produk FROM t_account_detail_sales A JOIN t_account_detail_sales_product B ON B.account_detail_sales_id = A.account_detail_sales_id JOIN produk C ON C.id_produk = B.product_id WHERE LEFT(A.account_detail_sales_date, 7) = '$tgl_terima2' GROUP BY B.product_id ORDER BY SUM(B.account_detail_sales_product_allow) DESC LIMIT 10")->result_array();
			$data['content'] = $this->load->view($this->url_ . '/chart_penjualan', $datacontent, TRUE);
		}
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function monthly_all()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_all', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function journal()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Jurnal';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_journal', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function format_db($tgl)
	{
		$arrtgl = explode('/', $tgl);
		$new_format = $arrtgl['2'] . '-' . $arrtgl['0'] . '-' . $arrtgl['1'];
		return $new_format;
	}

	public function print_journal()
	{
		$from = $this->format_db($_POST['from']);
		$to = $this->format_db($_POST['to']);

		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);



		//$sheet->getStyle('C4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f8cbad');
		$arrdata = $this->db->query("SELECT A.*, B.kode as  kode_4, C.kode as  kode_3, D.kode as  kode2, E.kode as  kode_1 FROM t_coa_transaction A LEFT JOIN coa_4 B ON B.id = A.coa_id LEFT JOIN coa_3 C ON C.id = A.coa_id  LEFT JOIN coa_2 D ON D.id = A.coa_id  LEFT JOIN coa_1 E ON E.id = A.coa_id WHERE (REPLACE(A.coa_date, '-', '') BETWEEN REPLACE('$from', '-', '') AND REPLACE('$to', '-', ''))")->result_array();

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:F2')->applyFromArray($style);
		$sheet->getStyle('B2:F2')->getFont()->setSize(20);
		$sheet->getStyle('B2:F2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'JURNAL UMUM ' . $_POST['from'] . ' s/d ' . $_POST['to'] . '');
		$sheet->mergeCells('B2:F2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		//$sheet->mergeCells($abjad.$awal.':'.$abjad.($awal+1));
		$sheet->setCellValue($abjad++ . $awal, 'NO AKUN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'TANGGAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'DEBIT');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KREDIT');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'KETERANGAN');
		for ($x = $abjad_awal;; $x++) {
			$sheet->getStyle($x . ($awal) . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($x == $abjad) break;
		}
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		//$sheet->getStyle('E5:I1')->getAlignment()->setWrapText(true);
		$nawal = $awal;
		$i = 1;
		foreach ($arrdata as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			if ($value1['coa_level'] == 1) $coa_id = $value1['kode_1'];
			else if ($value1['coa_level'] == 2) $coa_id = $value1['kode_2'];
			else if ($value1['coa_level'] == 3) $coa_id = $value1['kode_3'];
			else if ($value1['coa_level'] == 4) $coa_id = $value1['kode_4'];
			$sheet->setCellValue($nabjad++ . $nawal, $coa_id);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['coa_date']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['coa_debit']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['coa_credit']);
			$sheet->setCellValue($nabjad . $nawal, $value1['coa_transaction_note']);
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
		}


		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"JURNAL UMUM " . $_POST['from'] . " s/d " . $_POST['to'] . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}


	public function division_daily()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan';
		$datacontent['product_type_list'] = "";
		foreach ($this->Model->get_product_type() as $key => $value) {
			$datacontent['product_type_list'] = $datacontent['product_type_list'] . '<option value="' . $value['product_type_name'] . '">' . $value['product_type_name'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_division_daily', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function mpdf_division_daily()
	{
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$tanggal = $_POST['cTanggal'];
		$product_type = $_POST['product_type'];
		if (@$_POST['draft']) {
			$arrdata = $this->db->query("SELECT B.product_id, B.seller_id, D.nama, C.nama_produk, B.package_detail_quantity FROM t_package_trial_detail_draft B LEFT JOIN produk C ON C.id_produk = B.product_id LEFT JOIN member D ON D.kode = B.seller_id WHERE B.package_id IS NULL AND C.klasifikasi = '$product_type' AND B.seller_id NOT IN('MSGLOWXX101', 'MSGLOWXX102')")->result_array();
			$arrreturn['type'] = 'draft';
			$tanggal = date('Y-m-d');
			$arrreturn['detail_type'] = 'DRAFT ';
		} else {
			$arrdata = $this->db->query("SELECT B.product_id, A.member_code, D.nama, C.nama_produk, B.package_detail_quantity FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = B.product_id LEFT JOIN member D ON D.kode = A.member_code WHERE A.package_date = '$tanggal' AND C.klasifikasi = '$product_type' AND A.member_code NOT IN('MSGLOWXX101', 'MSGLOWXX102')")->result_array();
			$arrreturn['type'] = 'realisasi';
		}
		foreach ($arrdata as $index => $value) {
			$arrreturn['arrdata'][$value['product_id']][] = $value;
			$arrreturn['arrproduk'][$value['product_id']] = $value['nama_produk'];
		}
		$arrreturn['tgl_format'] = (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4);
		$arrreturn['product_type'] = @$product_type;
		$data['content'] = $this->load->view($this->url_ . '/mpdf_division_daily', $arrreturn);
	}

	public function mpdf_pembagian()
	{
		$data['content'] = $this->load->view($this->url_ . '/mpdf_pembagian');
	}

	public function mpdf_test()
	{
		$data['content'] = $this->load->view($this->url_ . '/mpdf_v');
	}

	public function print_monthly_all()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);



		//$sheet->getStyle('C4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f8cbad');
		$arrdata = $this->db->query("SELECT * FROM tb_stock_produk_history A LEFT JOIN produk B ON B.id_produk = A.id_barang LEFT JOIN m_product_type C ON C.product_type_name = B.klasifikasi WHERE LEFT(tanggal, 7) = '" . $tahun . "-" . $bulan . "' ORDER BY C.product_type_order, B.id_produk")->result_array();

		$arrterima = $this->db->query("SELECT SUM(jumlah) jumlah, id_barang, tgl_terima FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po WHERE LEFT(tgl_terima, 7) = '" . $tahun . "-" . $bulan . "' AND factory <> '19' GROUP BY tgl_terima, id_barang")->result_array();
		foreach ($arrterima as $keyterima => $valueterima) {
			$dataterima[$valueterima['id_barang']][$valueterima['tgl_terima']] = $valueterima;
		}

		$arrrepair = $this->db->query("SELECT SUM(jumlah) jumlah, id_barang, tgl_terima FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po WHERE LEFT(tgl_terima, 7) = '" . $tahun . "-" . $bulan . "' AND factory = '19' GROUP BY tgl_terima, id_barang")->result_array();
		foreach ($arrrepair as $keyterima => $valueterima) {
			$datarepair[$valueterima['id_barang']][$valueterima['tgl_terima']] = $valueterima;
		}

		$arrkeluar = $this->db->query("SELECT SUM(package_detail_quantity) jumlah, product_id, package_date FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE LEFT(package_date, 7) = '" . $tahun . "-" . $bulan . "' AND member_code NOT IN('MSGLOWXX101', 'MSGLOWXX102') GROUP BY package_date, product_id")->result_array();
		foreach ($arrkeluar as $keykeluar => $valuekeluar) {
			$datakeluar[$valuekeluar['product_id']][$valuekeluar['package_date']] = $valuekeluar;
		}

		$arrrusak = $this->db->query("SELECT SUM(package_detail_quantity) jumlah, product_id, package_date FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE LEFT(package_date, 7) = '" . $tahun . "-" . $bulan . "' AND member_code = 'MSGLOWXX101' GROUP BY package_date, product_id")->result_array();
		foreach ($arrrusak as $keykeluar => $valuekeluar) {
			$datarusak[$valuekeluar['product_id']][$valuekeluar['package_date']] = $valuekeluar;
		}

		$arrsample = $this->db->query("SELECT SUM(package_detail_quantity) jumlah, product_id, package_date FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE LEFT(package_date, 7) = '" . $tahun . "-" . $bulan . "' AND member_code = 'MSGLOWXX102' GROUP BY package_date, product_id")->result_array();
		foreach ($arrsample as $keykeluar => $valuekeluar) {
			$datasample[$valuekeluar['product_id']][$valuekeluar['package_date']] = $valuekeluar;
		}
		$abjad = 'B';
		$awal = 5;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');
		$sheet->setCellValue($abjad++ . $awal, 'KLASIFIKASI');
		$sheet->setCellValue($abjad++ . $awal, 'STOK AWAL');
		$sheet->setCellValue($abjad++ . $awal, 'STOK BARANG RUSAK AWAL');

		$nawal = $awal;

		$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		for ($i = 1; $i <= $nJumlahHari; $i++) {
			if ($i > 9) {
				$cNol = "";
			} else {
				$cNol = "0";
			}
			$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
			$no = 0;
			$titik_awal = $abjad;

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG MASUK');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG REPAIR');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG KELUAR');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG RUSAK');
			$sheet->getStyle($titik_awal . $awal . ':' . $abjad . $awal)->applyFromArray($styleArray);
			$sheet->mergeCells($titik_awal . $awal . ':' . $abjad . $awal);
			$sheet->setCellValue($titik_awal . $awal, substr($arrbln[$bulan], 0, 3) . '-' . (int)($cNol . $i));

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG SAMPLE');
		}
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG DITERIMA');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG REPAIR');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG MASUK');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG TERKIRIM');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG RUSAK');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG SAMPLE');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG KELUAR');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad . $awal, 'SISA STOCK BAGUS');


		$sheet->mergeCells($abjad_awal . ($awal - 3) . ':' . $abjad . ($awal - 3));
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$sheet->setCellValue($abjad_awal . ($awal - 3), 'LAPORAN STOK BARANG JADI MS GLOW');
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setSize(20);
		$sheet->mergeCells($abjad_awal . ($awal - 2) . ':' . $abjad . ($awal - 2));
		$sheet->setCellValue($abjad_awal . ($awal - 2), 'PERIODE ' . strtoupper($arrbln[$bulan]));
		$sheet->setCellValue($abjad . ($awal - 1), date('d/m/Y H.i'));


		//$sheet->getStyle('A1:B2')->applyFromArray($styleArray);
		//$sheet ->getStyle($abjad_awal.$awal.':'.$abjad.($awal+1))->applyFromArray($styleArray);
		$sheet->freezePane('F7');
		$sheet->getSheetView()->setZoomScale(70);
		$sheet->getRowDimension($awal + 1)->setRowHeight(30);
		$irow = 1;
		$wrap_awal = "";
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal + 1))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = @$wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}
		$sheet->getStyle($wrap_awal . '5:' . $wrap_akhir . '6')->getAlignment()->setWrapText(true);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$nawal++;
		$produk_global = "";
		$produk_merge_akhir = "";

		foreach ($arrdata as $key => $value) {
			$nabjad = 'B';
			$paling_awal = $nabjad;
			$nawal++;
			if (@$produk_merge_awal == "") {
				$produk_merge_awal = $nawal;
			}

			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk']);
			//$xx = 1;
			if (@$rgb == "") $rgb = 'e9f7df';
			if ($produk_global != $value['nama_produk']) {
				//$xx = 0;
				$xabjad = $nabjad;
				if ($produk_merge_akhir != "") {

					$sheet->mergeCells($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_akhir);
					$sheet->getStyle($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_awal)->applyFromArray($style);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
					//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
				}
				$produk_merge_awal = $nawal;
			}
			$produk_global = $value['klasifikasi'];
			$sheet->setCellValue($nabjad++ . $nawal, $value['klasifikasi']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['jumlah']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['rusak']);
			$jumlah = $value['jumlah'];
			$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$jmlterima = 0;
			$jmlrepair = 0;
			$jmlmasuk = 0;
			$jmlkirim = 0;
			$jmlrusak = 0;
			$jmlsample = 0;
			$jmlkeluar = 0;
			for ($i = 1; $i <= $nJumlahHari; $i++) {
				if ($i > 9) {
					$cNol = "";
				} else {
					$cNol = "0";
				}
				$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
				$no = 0;

				$sheet->setCellValue($nabjad++ . $nawal, @$dataterima[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datarepair[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datakeluar[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datarusak[$value['id_barang']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, @$datasample[$value['id_barang']][$date]['jumlah']);
				$jmlterima += ((@$dataterima[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_barang']][$date]['jumlah']);
				$jmlrepair += ((@$datarepair[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarepair[$value['id_barang']][$date]['jumlah']);
				$jmlmasuk += ((@$dataterima[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_barang']][$date]['jumlah']) + ((@$datarepair[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarepair[$value['id_barang']][$date]['jumlah']);
				$jmlkirim += ((@$datakeluar[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datakeluar[$value['id_barang']][$date]['jumlah']);
				$jmlrusak += ((@$datarusak[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarusak[$value['id_barang']][$date]['jumlah']);
				$jmlsample += ((@$datasample[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datasample[$value['id_barang']][$date]['jumlah']);
				$jmlkeluar += ((@$datakeluar[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datakeluar[$value['id_barang']][$date]['jumlah']) + ((@$datarusak[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datarusak[$value['id_barang']][$date]['jumlah']) + ((@$datasample[$value['id_barang']][$date]['jumlah'] == "") ? 0 : $datasample[$value['id_barang']][$date]['jumlah']);
				//$jmlkeluar += $jmlkeluar + (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']);
				//$jumlah = $jumlah + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima[$value['id_kemasan']][$date]['jumlah']) - (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']); 
			}
			$jumlah = (int)$jumlah + (int)$jmlmasuk - (int)$jmlkeluar;
			$sheet->setCellValue($nabjad++ . $nawal, $jmlterima);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlrepair);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlmasuk);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlkirim);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlrusak);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlsample);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlkeluar);
			$sheet->setCellValue($nabjad . $nawal, $jumlah);
			$produk_merge_akhir = $nawal;
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
			$rgb = @$value['category_product_rgb'];
			if ($rgb == "") $rgb = 'e9f7df';
			if ($jumlah < 20000) {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
			} else {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
			}
		}

		//if($xx == 1){
		$sheet->mergeCells($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_akhir);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray2);
		$sheet->getStyle($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_awal)->applyFromArray($style);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
		//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
		//}

		$nawal++;
		$nawal++;
		$sheet->setCellValue($abjad_awal . $nawal++, 'NB:');
		$abjad_awal2 = $abjad_awal;
		$sheet->getStyle($abjad_awal2 . $nawal . ':' . $abjad_awal2++ . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
		$sheet->setCellValue($abjad_awal2 . $nawal++, 'STOK DIBAWAH 20.000 PCS');
		/*foreach($arrcategoryproduct as $keyproduct => $valueproduct){
			$abjad_awal2 = $abjad_awal;
			$sheet->getStyle($abjad_awal2.$nawal.':'.$abjad_awal2++.$nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($valueproduct['category_product_rgb']);
			$sheet->setCellValue($abjad_awal2.$nawal++, $valueproduct['category_product_name']);
		}*/

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"STOCK BARANG JADI MSGLOW " . strtoupper($arrbln[$bulan]) . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}


	public function print_monthly_all2()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$arrpackage_trial = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN member B ON B.kode = A.member_code WHERE A.package_date = '$tanggal'")->result_array();
		$arrpackage_trial_detail = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = B.product_id WHERE A.package_date = '$tanggal'")->result_array();
		$arrpackage_trial_employee = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_employee B ON B.package_id = A.package_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.package_date = '$tanggal'")->result_array();
		foreach ($arrpackage_trial_detail as $index2 => $value2) {
			$datpackage_trial_detail[$value2['package_id']][] = $value2;
		}
		foreach ($arrpackage_trial_employee as $index3 => $value3) {
			$datpackage_trial_employee[$value3['package_id']][] = $value3;
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:F2')->applyFromArray($style);
		$sheet->getStyle('B2:F2')->getFont()->setSize(20);
		$sheet->getStyle('B2:F2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'LAPORAN PENGIRIMAN ' . (int)substr($tanggal, 8, 2) . ' ' . $arrbln[substr($tanggal, 5, 2)] . ' ' . substr($tanggal, 0, 4));
		$sheet->mergeCells('B2:F2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PRODUK');
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PEGAWAI');

		$nawal = $awal;
		foreach ($arrpackage_trial as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value1['member_code']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama'] . " (" . $value1['kode'] . ")");
			$nawalx = $nawal;
			$abjadxawal = $nabjad;
			foreach ($datpackage_trial_detail[$value1['package_id']] as $indexd2 => $valued2) {
				$x = $nawalx;
				$abjadx = $abjadxawal;
				$sheet->setCellValue($abjadx++ . $nawalx, $valued2['nama_produk']);
				$sheet->setCellValue($abjadx . $nawalx++, $valued2['package_detail_quantity']);
			}
			$abjadx++;
			$nabjad = $abjadx;
			$nawaly = $nawal;
			foreach ($datpackage_trial_employee[$value1['package_id']] as $indexd3 => $valued3) {
				$y = $nawaly;
				$abjadx = $abjadxawal;
				$sheet->setCellValue($nabjad . $nawaly++, $valued3['nama_phl']);
			}
			if ($x > $y) $nawal = $x;
			else $nawal = $y;
		}

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENGIRIMAN " . (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4) . " .xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function print_monthly_all3()
	{
		ob_end_clean();
		$from = $this->String2Date($_POST['from']);
		$to = $this->String2Date($_POST['to']);
		$arrdata = $this->db->query("SELECT A.*, B.kode as  kode_4, C.kode as  kode_3, D.kode as  kode2, E.kode as  kode_1 FROM t_coa_transaction A LEFT JOIN coa_4 B ON B.id = A.coa_id LEFT JOIN coa_3 C ON C.id = A.coa_id  LEFT JOIN coa_2 D ON D.id = A.coa_id  LEFT JOIN coa_1 E ON E.id = A.coa_id WHERE A.coa_date BETWEEN '" . $from . "' AND '" . $to . "'")->result_array();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Hello World !');

		$writer = new Xlsx($spreadsheet);
		$writer->save('hello world.xlsx');
	}

	public function daily_out()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Harian Barang Keluar';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_daily_out', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_daily_out()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$tanggal = $_POST['cTanggal'];
		$arrpackage_trial = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN member B ON B.kode = A.member_code WHERE A.package_date = '$tanggal'")->result_array();
		$arrpackage_trial_detail = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = B.product_id WHERE A.package_date = '$tanggal'")->result_array();
		$arrpackage_trial_employee = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_employee B ON B.package_id = A.package_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.package_date = '$tanggal'")->result_array();
		foreach ($arrpackage_trial_detail as $index2 => $value2) {
			$datpackage_trial_detail[$value2['package_id']][] = $value2;
		}
		foreach ($arrpackage_trial_employee as $index3 => $value3) {
			$datpackage_trial_employee[$value3['package_id']][] = $value3;
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:F2')->applyFromArray($style);
		$sheet->getStyle('B2:F2')->getFont()->setSize(20);
		$sheet->getStyle('B2:F2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'LAPORAN PENGIRIMAN ' . (int)substr($tanggal, 8, 2) . ' ' . $arrbln[substr($tanggal, 5, 2)] . ' ' . substr($tanggal, 0, 4));
		$sheet->mergeCells('B2:F2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PRODUK');
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PEGAWAI');

		$nawal = $awal;
		foreach ($arrpackage_trial as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value1['member_code']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama'] . " (" . $value1['kode'] . ")");
			$nawalx = $nawal;
			$abjadxawal = $nabjad;
			foreach ($datpackage_trial_detail[$value1['package_id']] as $indexd2 => $valued2) {
				$x = $nawalx;
				$abjadx = $abjadxawal;
				$sheet->setCellValue($abjadx++ . $nawalx, $valued2['nama_produk']);
				$sheet->setCellValue($abjadx . $nawalx++, $valued2['package_detail_quantity']);
			}
			$abjadx++;
			$nabjad = $abjadx;
			$nawaly = $nawal;
			foreach ($datpackage_trial_employee[$value1['package_id']] as $indexd3 => $valued3) {
				$y = $nawaly;
				$abjadx = $abjadxawal;
				$sheet->setCellValue($nabjad . $nawaly++, $valued3['nama_phl']);
			}
			if ($x > $y) $nawal = $x;
			else $nawal = $y;
		}

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENGIRIMAN " . (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4) . " .xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function daily_all()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Harian Stok';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_daily_all', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_daily_all()
	{

		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$tanggal = $_POST['cTanggal'];
		//$tanggal = '2020-11-05';
		$tgl_before = (date('Y-m-d', strtotime($tanggal . ' - 1 days')) < '2020-11-01') ? substr($tanggal, 0, 7) . '-01' : date('Y-m-d', strtotime($tanggal . ' - 1 days'));
		$arrstock_produk_history = $this->db->query("SELECT * FROM tb_stock_produk_history A LEFT JOIN produk B ON B.id_produk = A.id_barang LEFT JOIN m_product_type C ON C.product_type_name = B.klasifikasi WHERE LEFT(tanggal, 7) = '" . substr($tanggal, 0, 7) . "' ORDER BY C.product_type_order")->result_array();
		$arrin_past = $this->db->query("SELECT SUM(jumlah) as jumlah, id_barang as id_produk FROM terima_produk WHERE tgl_terima BETWEEN '" . substr($tanggal, 0, 7) . '-01' . "' AND '$tgl_before' GROUP BY id_barang")->result_array();
		foreach ($arrin_past as $indexin => $valuein) {
			$datin_past[$valuein['id_produk']] = $valuein['jumlah'];
		}
		$arrout_past = $this->db->query("SELECT SUM(B.package_detail_quantity) as jumlah, B.product_id as id_produk FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE A.package_date BETWEEN '" . substr($tanggal, 0, 7) . '-01' . "' AND '$tgl_before' GROUP BY B.product_id")->result_array();
		foreach ($arrout_past as $indexout => $valueout) {
			$datout_past[$valueout['id_produk']] = $valueout['jumlah'];
		}
		$arrin_new = $this->db->query("SELECT SUM(jumlah) as jumlah, id_barang as id_produk FROM terima_produk WHERE tgl_terima = '" . $tanggal . "' GROUP BY id_barang")->result_array();
		foreach ($arrin_new as $indexin_new => $valuein_new) {
			$datin_new[$valuein_new['id_produk']] = $valuein_new['jumlah'];
		}
		$arrout_new = $this->db->query("SELECT SUM(B.package_detail_quantity) as jumlah, B.product_id as id_produk FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE A.package_date = '" . $tanggal . "' GROUP BY B.product_id")->result_array();
		foreach ($arrout_new as $indexout_new => $valueout_new) {
			$datout_new[$valueout_new['id_produk']] = $valueout_new['jumlah'];
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:I2')->applyFromArray($style);
		$sheet->getStyle('B2:I2')->getFont()->setSize(20);
		$sheet->getStyle('B2:I2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'LAPORAN HARIAN ' . (int)substr($tanggal, 8, 2) . ' ' . $arrbln[substr($tanggal, 5, 2)] . ' ' . substr($tanggal, 0, 4));
		$sheet->mergeCells('B2:I2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'KLASIFIKASI');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');
		$sheet->mergeCells($abjad . $awal . ':I' . $awal);
		$sheet->getStyle($abjad . $awal . ':I' . $awal)->applyFromArray($styleArray);
		$sheet->setCellValue($abjad . $awal++, (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4));
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STOCK AWAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BARANG DATANG');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BARANG KELUAR');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STOCK AKHIR');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'KETERANGAN');
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . ($awal - 1) . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($x == $abjad) break;
		}
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		//$sheet->getStyle('E5:I1')->getAlignment()->setWrapText(true);
		$nawal = $awal;
		$i = 1;
		$klasifikasi_chk = "";
		foreach ($arrstock_produk_history as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			$stockawal = ($value1['jumlah'] + ((@$datin_past[$value1['id_barang']] == "") ? 0 : @$datin_past[$value1['id_barang']]) - ((@$datout_past[$value1['id_barang']] == "") ? 0 : @$datout_past[$value1['id_barang']]));
			$sheet->setCellValue($nabjad++ . $nawal, $i++);
			if ($klasifikasi_chk != $value1['klasifikasi']) {
				if ($klasifikasi_chk != "") {
					$sheet->mergeCells($nabjad . $nawal_awal . ':' . $nabjad . $nawal);
				}
				$nawal_awal = $nawal;
			}
			$sheet->setCellValue($nabjad++ . $nawal, $value1['klasifikasi']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $stockawal);
			$sheet->setCellValue($nabjad++ . $nawal, @$datin_new[$value1['id_barang']]);
			$sheet->setCellValue($nabjad++ . $nawal, @$datout_new[$value1['id_barang']]);
			$sheet->setCellValue($nabjad++ . $nawal, ($stockawal + ((@$datin_new[$value1['id_barang']] == "") ? 0 : @$datin_new[$value1['id_barang']]) - ((@$datout_new[$value1['id_barang']] == "") ? 0 : $datout_new[$value1['id_barang']])));
			$klasifikasi_chk = $value1['klasifikasi'];
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
		}
		if ($klasifikasi_chk != $value1['klasifikasi']) {
			if ($klasifikasi_chk != "") {
				$sheet->mergeCells($nabjad . $nawal_awal . ':' . $nabjad . $nawal);
			}
			$klasifikasi_awal = $value1['klasifikasi'];
		}
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN HARIAN " . (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4) . " .xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function monthly_factory_in()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Barang Masuk';
		$datacontent['factory_list'] = "";
		foreach ($this->Model->get_factory() as $key => $value) {
			$datacontent['factory_list'] = $datacontent['factory_list'] . '<option value="' . $value['id_factory'] . '">' . $value['nama_factory'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_factory_in', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_monthly_factory_in()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$factory_id = $_POST['factory_id'];
		$arrfactory = $this->db->query("SELECT * FROM factory A WHERE id_factory = '$factory_id'")->row_array();
		$arrterima_produk = $this->db->query("SELECT id_barang, SUM(jumlah) as jumlah, tgl_terima, id_barang FROM terima_produk A LEFT JOIN po_produk B ON B.kode_po = A.kode_po WHERE B.factory = '$factory_id' AND LEFT(A.tgl_terima, 7) = '$tahun-$bulan' GROUP BY id_barang, tgl_terima ORDER BY tgl_terima")->result_array();
		$tanggal = array();
		$id_barang = array();
		$arrdata = array();
		foreach ($arrterima_produk as  $index => $value) {
			$tanggal[$value['tgl_terima']] = $value['tgl_terima'];
			$id_barang[$value['id_barang']] = $value['id_barang'];
			$arrdata[$value['id_barang']][$value['tgl_terima']] = $value;
		}
		$arrsisapo = $this->db->query("SELECT SUM(B.jumlah) as jumlah, IFNULL(SUM(C.jumlah), 0) as jumlah_terkirim, B.id_barang FROM po_produk A LEFT JOIN tb_detail_po_produk B ON B.kode_pb = A.kode_po LEFT JOIN terima_produk C ON C.kode_po = A.kode_po AND C.id_barang = B.id_barang AND LEFT(C.tgl_terima, 7) < '$tahun-$bulan' WHERE A.factory = '$factory_id' AND A.active = 'Y' AND LEFT(A.tanggal, 7) < '$tahun-$bulan' GROUP BY B.id_barang")->result_array();
		foreach ($arrsisapo as  $indexxx => $valuexx) {
			$sisa_po[$valuexx['id_barang']] = $valuexx;
			$id_barang[$valuexx['id_barang']] = $valuexx['id_barang'];
		}
		$arrsisapo2 = $this->db->query("SELECT SUM(B.jumlah) as jumlah, B.id_barang FROM po_produk A LEFT JOIN tb_detail_po_produk B ON B.kode_pb = A.kode_po WHERE A.factory = '$factory_id' AND LEFT(A.tanggal, 7) = '$tahun-$bulan' GROUP BY B.id_barang")->result_array();
		foreach ($arrsisapo2 as  $indexxx => $valuexx) {
			$sisa_po2[$valuexx['id_barang']] = $valuexx;
			$id_barang[$valuexx['id_barang']] = $valuexx['id_barang'];
		}
		$allid_barang = join("', '", $id_barang);
		$arrbarang = $this->db->query("SELECT * FROM produk WHERE id_produk IN('$allid_barang') ORDER BY id_produk")->result_array();
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$sheet->setCellValue('A1', 'REKAP BARANG DATANG ' . strtoupper($arrbln[$bulan]));
		$sheet->setCellValue('A2', $arrfactory['nama_factory']);
		$sheet->getStyle('A1:A2')->getFont()->setSize(20);
		$sheet->getStyle('A1:A2')->getFont()->setBold(true);
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$awal = 4;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'SISA PO');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'ORDER');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL ORDER');
		$sheet->setCellValue($abjad . $awal, 'PENERIMAAN BARANG');
		$sawal = $awal + 1;
		$start_tgl = $abjad;
		$akhir_tgl = $abjad;
		foreach (@$tanggal as $index2 => $value2) {
			$akhir_tgl = $abjad;
			$sheet->setCellValue($abjad++ . $sawal, (int)substr($value2, 8, 2) . "-" . substr($arrbln[$bulan], 0, 3));
		}
		$sheet->mergeCells($start_tgl . $awal . ':' . @$akhir_tgl . $awal);
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL KIRIM');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'KURANG KIRIM');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'HARGA');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad . $awal, 'TOTAL');
		for ($x = $abjad_awal;; $x++) {
			if ($x >= $start_tgl && $x <= $akhir_tgl) {
				$sheet->getStyle($x . ($awal + 1) . ':' . $x . ($awal + 1))->applyFromArray($styleArray);
			}
			$sheet->getStyle($x . ($awal) . ':' . $x . ($awal + 1))->applyFromArray($styleArray);
			if ($x == $abjad) break;
		}
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad . ($awal + 1))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad . ($awal + 1))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad . ($awal + 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$abjad_akhir = $abjad;
		$sheet->mergeCells('A1:' . $abjad_akhir . '1');
		$sheet->mergeCells('A2:' . $abjad_akhir . '2');
		$sheet->getStyle('A1:' . $abjad_akhir . '2')->applyFromArray($style);
		$nawal = $awal + 1;
		foreach (@$arrbarang as $index3 => $value3) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value3['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, ((int)@$sisa_po[$value3['id_produk']]['jumlah'] - (int)@$sisa_po[$value3['id_produk']]['jumlah_terkirim']));
			$sheet->setCellValue($nabjad++ . $nawal, ((int)@$sisa_po2[$value3['id_produk']]['jumlah']));
			$sheet->setCellValue($nabjad++ . $nawal, ((int)@$sisa_po[$value3['id_produk']]['jumlah'] - (int)@$sisa_po[$value3['id_produk']]['jumlah_terkirim']) + ((int)@$sisa_po2[$value3['id_produk']]['jumlah']));
			$total[$value3['id_produk']] = 0;
			foreach (@$tanggal as $index2 => $value2) {
				$akhir_tgl = $nabjad;
				$sheet->setCellValue($nabjad++ . $nawal, @$arrdata[$value3['id_produk']][$value2]['jumlah']);
				$total[$value3['id_produk']] += ((@$arrdata[$value3['id_produk']][$value2]['jumlah'] == "") ? 0 : @$arrdata[$value3['id_produk']][$value2]['jumlah']);
			}
			$sheet->setCellValue($nabjad++ . $nawal, $total[$value3['id_produk']]);
			$sheet->setCellValue($nabjad++ . $nawal, ((int)@$sisa_po[$value3['id_produk']]['jumlah'] - (int)@$sisa_po[$value3['id_produk']]['jumlah_terkirim']) + ((int)@$sisa_po2[$value3['id_produk']]['jumlah']) - $total[$value3['id_produk']]);
			/*foreach($tanggalx as $index2 => $value2){ 
				$sheet->setCellValue($nabjad++.$nawal, @$arrdata[$index3][$index2]);
				$total[$index2] = $total[$index2] + ((@$arrdata[$index3][$index2] != "")?@$arrdata[$index3][$index2]:0);
			}*/
		}
		//$nawal++;
		//$nabjad = $abjad_awal;
		/*$sheet->setCellValue($nabjad++.$nawal, 'Grand Total');
		foreach($tanggalx as $index2 => $value2){
				$abjad_akhir = $nabjad;
				$sheet->setCellValue($nabjad++.$nawal, $total[$index2]);
		}*/
		//$sheet->getStyle('A1:'.((@$abjad_akhir=="")?'A':$abjad_akhir).'2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F4B084');
		//$sheet->getStyle($abjad_awal.$awal.':'.((@$abjad_akhir=="")?'A':$abjad_akhir).$nawal)->applyFromArray($styleArray_thin);
		//$sheet->getStyle($abjad_awal.$nawal.':'.((@$abjad_akhir=="")?'A':$abjad_akhir).$nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('D9D9D9');
		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENERIMAAN PABRIK " . $arrfactory['nama_factory'] . " BULAN " . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function monthly_out()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Barang Keluar';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_out', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_monthly_out()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$product_id = $_POST['product_id'];
		$arrpackage_trial = $this->db->query("SELECT SUM(B.package_detail_quantity) AS jumlah, A.package_date, A.member_code, D.nama, C.nama_produk FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = B.product_id LEFT JOIN member D ON D.kode = A.member_code WHERE LEFT(A.package_date, 7) = '" . $tahun . "-" . $bulan . "' AND B.product_id = '$product_id' GROUP BY A.package_date, A.member_code ORDER BY A.package_date")->result_array();
		$tanggalx = array();
		$sellerx = array();
		$arrdata = array();
		foreach ($arrpackage_trial as $index => $value) {
			$tanggalx[$value['package_date']] = $value['package_date'];
			$sellerx[$value['member_code']] = $value['nama'];
			$arrdata[$value['member_code']][$value['package_date']] = $value['jumlah'];
			$barang = $value['nama_produk'];
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->setCellValue('A1', 'Laporan Bulanan');
		$sheet->setCellValue('A2', @$barang);
		$sheet->getStyle('A1:A2')->getFont()->setSize(20);
		$sheet->getStyle('A1:A2')->getFont()->setBold(true);
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$awal = 3;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NAMA');
		foreach (@$tanggalx as $index2 => $value2) {
			$sheet->setCellValue($abjad++ . $awal, (int)substr($value2, 8, 2) . "-" . substr($arrbln[$bulan], 0, 3) . "-" . substr($tahun, 2, 4));
			$total[$index2] = 0;
		}

		$nawal = $awal;
		foreach (@$sellerx as $index3 => $value3) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value3);
			foreach ($tanggalx as $index2 => $value2) {
				$sheet->setCellValue($nabjad++ . $nawal, @$arrdata[$index3][$index2]);
				$total[$index2] = $total[$index2] + ((@$arrdata[$index3][$index2] != "") ? @$arrdata[$index3][$index2] : 0);
			}
		}
		$nawal++;
		$nabjad = $abjad_awal;
		$sheet->setCellValue($nabjad++ . $nawal, 'Grand Total');
		foreach ($tanggalx as $index2 => $value2) {
			$abjad_akhir = $nabjad;
			$sheet->setCellValue($nabjad++ . $nawal, $total[$index2]);
		}
		$sheet->getStyle('A1:' . ((@$abjad_akhir == "") ? 'A' : $abjad_akhir) . '2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F4B084');
		$sheet->getStyle($abjad_awal . $awal . ':' . ((@$abjad_akhir == "") ? 'A' : $abjad_akhir) . $nawal)->applyFromArray($styleArray_thin);
		$writer = new Xlsx($spreadsheet);
		$sheet->getStyle($abjad_awal . $nawal . ':' . ((@$abjad_akhir == "") ? 'A' : $abjad_akhir) . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('D9D9D9');
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENGIRIMAN " . @$barang . " BULAN " . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function export_stock_monthly($bulan, $tahun)
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		//$sheet->getStyle('C4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f8cbad');
		$arrdata = $this->model->Query("SELECT *, C.jumlah, C.rusak FROM kemasan A LEFT JOIN produk B ON B.id_produk = A.produk LEFT JOIN tb_stock_kemasan_history C ON C.id_barang = A.id_kemasan AND C.tanggal = '" . $tahun . "-" . $bulan . "-01' LEFT JOIN m_category_product D ON D.category_product_id = B.produk_kategori ORDER BY B.produk_kategori, A.produk");
		$arrcategoryproduct = $this->model->Query("SELECT * FROM m_category_product");
		$arrterima = $this->model->Query("SELECT SUM(jumlah) as jumlah, SUM(rusak) as rusak, tgl_terima, id_barang FROM terima_kemasan  GROUP BY id_barang, tgl_terima");
		foreach ($arrterima as $keyterima => $valueterima) {
			$dataterima[$valueterima['id_barang']][$valueterima['tgl_terima']] = $valueterima;
		}
		$arrterima_print = $this->model->Query("SELECT SUM(jumlah) as jumlah, SUM(rusak) as rusak, tgl_terima, id_barang FROM terima_print  GROUP BY id_barang, tgl_terima");
		foreach ($arrterima_print as $keyterima_print => $valueterima_print) {
			$dataterima_print[$valueterima_print['id_barang']][$valueterima_print['tgl_terima']] = $valueterima_print;
		}
		$arrkeluar = $this->model->Query("SELECT SUM(jumlah) as jumlah, tanggal, id_barang FROM tb_detail_kluar_kemasan GROUP BY id_barang, tanggal");
		foreach ($arrkeluar as $keykeluar => $valuekeluar) {
			$datakeluar[$valuekeluar['id_barang']][$valuekeluar['tanggal']] = $valuekeluar;
		}

		$abjad = 'B';
		$awal = 5;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');
		$sheet->setCellValue($abjad++ . $awal, 'KLASIFIKASI');
		$sheet->setCellValue($abjad++ . $awal, 'STOK AWAL');
		$sheet->setCellValue($abjad++ . $awal, 'STOK BARANG RUSAK AWAL');

		$nawal = $awal;

		$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		for ($i = 1; $i <= $nJumlahHari; $i++) {
			if ($i > 9) {
				$cNol = "";
			} else {
				$cNol = "0";
			}
			$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
			$no = 0;
			$titik_awal = $abjad;

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG DATANG');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG KELUAR');
			$sheet->getStyle($titik_awal . $awal . ':' . $abjad . $awal)->applyFromArray($styleArray);
			$sheet->mergeCells($titik_awal . $awal . ':' . $abjad . $awal);
			$sheet->setCellValue($titik_awal . $awal, substr($arrbln[$bulan], 0, 3) . '-' . (int)($cNol . $i));

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG RUSAK');
		}

		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG DATANG');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG KELUAR');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad . $awal, 'SISA STOCK KEMASAN BAGUS');

		$sheet->mergeCells($abjad_awal . ($awal - 3) . ':' . $abjad . ($awal - 3));
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$sheet->setCellValue($abjad_awal . ($awal - 3), 'LAPORAN STOK BAHAN KEMAS MS GLOW');
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setSize(20);
		$sheet->mergeCells($abjad_awal . ($awal - 2) . ':' . $abjad . ($awal - 2));
		$sheet->setCellValue($abjad_awal . ($awal - 2), 'PERIODE ' . strtoupper($arrbln[$bulan]));
		$sheet->setCellValue($abjad . ($awal - 1), date('d/m/Y H.i'));


		//$sheet->getStyle('A1:B2')->applyFromArray($styleArray);
		//$sheet ->getStyle($abjad_awal.$awal.':'.$abjad.($awal+1))->applyFromArray($styleArray);
		$sheet->freezePane('F7');
		$sheet->getSheetView()->setZoomScale(70);
		$sheet->getRowDimension($awal + 1)->setRowHeight(30);
		$irow = 1;
		$wrap_awal = "";
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal + 1))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = $wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}
		$sheet->getStyle($wrap_awal . '5:' . $wrap_akhir . '6')->getAlignment()->setWrapText(true);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$nawal++;
		$produk_global = "";
		$produk_merge_akhir = "";

		foreach ($arrdata as $key => $value) {
			$nabjad = 'B';
			$paling_awal = $nabjad;
			$nawal++;
			if ($produk_merge_awal == "") {
				$produk_merge_awal = $nawal;
			}

			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_kemasan']);
			//$xx = 1;
			if ($rgb == "") $rgb = 'e9f7df';
			if ($produk_global != $value['nama_produk']) {
				//$xx = 0;
				$xabjad = $nabjad;
				if ($produk_merge_akhir != "") {

					$sheet->mergeCells($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_akhir);
					$sheet->getStyle($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_awal)->applyFromArray($style);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
					//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
				}
				$produk_merge_awal = $nawal;
			}
			$produk_global = $value['nama_produk'];
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['jumlah']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['rusak']);
			$jumlah = $value['jumlah'];
			$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$jmldatang = 0;
			$jmlkeluar = 0;
			for ($i = 1; $i <= $nJumlahHari; $i++) {
				if ($i > 9) {
					$cNol = "";
				} else {
					$cNol = "0";
				}
				$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
				$no = 0;

				$sheet->setCellValue($nabjad++ . $nawal, ($dataterima[$value['id_kemasan']][$date]['jumlah'] != "" || $dataterima_print[$value['id_kemasan']][$date]['jumlah'] != "") ? ((($dataterima[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_kemasan']][$date]['jumlah']) + (($dataterima_print[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima_print[$value['id_kemasan']][$date]['jumlah'])) : "");
				$sheet->setCellValue($nabjad++ . $nawal, $datakeluar[$value['id_kemasan']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, $dataterima[$value['id_kemasan']][$date]['rusak']);
				$jmldatang = $jmldatang + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_kemasan']][$date]['jumlah']) + (($dataterima_print[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima_print[$value['id_kemasan']][$date]['jumlah']);
				$jmlkeluar = $jmlkeluar + (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $datakeluar[$value['id_kemasan']][$date]['jumlah']);
				//$jumlah = $jumlah + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima[$value['id_kemasan']][$date]['jumlah']) - (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']); 
			}
			$jumlah = $jumlah + $jmldatang - $jmlkeluar;
			$sheet->setCellValue($nabjad++ . $nawal, $jmldatang);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlkeluar);
			$sheet->setCellValue($nabjad . $nawal, $jumlah);
			$produk_merge_akhir = $nawal;
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
			$rgb = $value['category_product_rgb'];
			if ($rgb == "") $rgb = 'e9f7df';
			if ($jumlah < 20000) {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
			} else {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
			}
		}
		//if($xx == 1){
		$sheet->mergeCells($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_akhir);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray2);
		$sheet->getStyle($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_awal)->applyFromArray($style);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
		//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
		//}

		$nawal++;
		$nawal++;
		$sheet->setCellValue($abjad_awal . $nawal++, 'NB:');
		$abjad_awal2 = $abjad_awal;
		$sheet->getStyle($abjad_awal2 . $nawal . ':' . $abjad_awal2++ . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
		$sheet->setCellValue($abjad_awal2 . $nawal++, 'STOK DIBAWAH 20.000 PCS');
		foreach ($arrcategoryproduct as $keyproduct => $valueproduct) {
			$abjad_awal2 = $abjad_awal;
			$sheet->getStyle($abjad_awal2 . $nawal . ':' . $abjad_awal2++ . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($valueproduct['category_product_rgb']);
			$sheet->setCellValue($abjad_awal2 . $nawal++, $valueproduct['category_product_name']);
		}

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"STOCK KEMASAN MSGLOW " . strtoupper($arrbln[$bulan]) . ".xls\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}
}
