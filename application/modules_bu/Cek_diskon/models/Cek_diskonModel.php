<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Cek_diskonModel extends CI_Model
{
	
	var $table_ = "t_sales";
	var $id_ = "sales_id";
	var $eng_ = "sales";
	var $url_ = "T_sales";
	
	function get_produk($id="")
	{
		$data = $this->db->get('produk');
		return $data->result_array();
	}
	
	function get_product_type()
	{
		$data = $this->db->get('m_product_type');
		return $data->result_array();
	}
	
	function get_factory()
	{
		$data = $this->db->get('factory');
		return $data->result_array();
	}

}
