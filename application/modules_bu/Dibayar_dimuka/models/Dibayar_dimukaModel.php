<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Dibayar_dimukaModel extends CI_Model
{

	var $table_ = "t_activities";
	var $id_ = "activities_id";
	var $eng_ = "Dibayar Dimuka";
	var $url_ = "Dibayar_dimuka";


	public function insert_coa_biaya($id3_biaya, $asset_name, $kode)
	{
		$coaBiaya = $this->db->get_where('coa_3', ['id' => 19])->row_array();
		$coaBeban = $this->db->get_where('coa_4', ['nama' => $coaBiaya['nama'] . " " . $asset_name])->row_array();
		if ($coaBeban == null) {
			$coaBeban = ([
				'nama' => $coaBiaya['nama'] . " " . $asset_name,
				'kode' => $coaBiaya['kode'] . "." . $kode,
				'coa3_id' => 19
			]);
			$this->db->insert('coa_4', $coaBeban);
			$coaBeban['id'] = $this->db->insert_id();


			$total = ([
				'coa_id' => $coaBeban['id'],
				'coa_code' => $coaBeban['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $coaBeban['nama'],
			]);
			$this->db->insert('t_coa_total', $total);
			$total['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $total);
		}
		return $coaBeban;
	}

	public function insertCoaBrand($coa_name, $brand, $koreId, $amortisasiId)
	{
		$akunBrand = $this->db->get_where('m_brand', ['brand_id' => $brand])->row_array();
		$coa4 = $this->db->get_where('coa_4', ['id' => $amortisasiId])->row_array();
		$coa5 = $this->db->get_where('coa_5', ['coa4_id' => $amortisasiId])->result_array();
		$coaBrand = $this->db->get_where('coa_5', ['nama' => $coa_name . " " . $akunBrand['brand_name'], 'coa4_id' => $amortisasiId])->row_array();
		if ($coaBrand == null) {
			$coaBrand = ([
				'nama' => $coa_name . " " . $akunBrand['brand_name'],
				'kode' => $coa4['kode'] . "." . (count($coa5) + 1),
				'coa4_id' => $amortisasiId,
				'brand_id' => $brand,
				'coa_corelation_id' => $koreId
			]);
			$this->db->insert('coa_5', $coaBrand);
			$coaBrand['id'] = $this->db->insert_id();

			$total = ([
				'coa_id' => $coaBrand['id'],
				'coa_code' => $coaBrand['kode'],
				'coa_level' => 5,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $coaBrand['nama'],
			]);
			$this->db->insert('t_coa_total', $total);
			$total['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $total);
		}
		return $coaBrand;
	}

	public function insertCoaProduk($coa_name, $brand, $produk, $koreId, $amortisasiId)
	{
		$akunProduk = $this->db->get_where('produk_global', ['id' => $produk])->row_array();
		$coa4 = $this->db->get_where('coa_4', ['id' => $amortisasiId])->row_array();
		$coa5 = $this->db->get_where('coa_5', ['coa4_id' => $amortisasiId])->result_array();
		$coaProduk = $this->db->get_where('coa_5', ['nama' => $coa_name . " " . $akunProduk['nama_produk'], 'coa4_id' => $amortisasiId])->row_array();
		if ($coaProduk == null) {
			$coaProduk = ([
				'nama' => $coa_name . " " . $akunProduk['nama_produk'],
				'kode' => $coa4['kode'] . "." . (count($coa5) + 1),
				'coa4_id' => $amortisasiId,
				'brand_id' => $brand,
				'coa_corelation_id' => $koreId
			]);
			$this->db->insert('coa_5', $coaProduk);
			$coaProduk['id'] = $this->db->insert_id();

			$total = ([
				'coa_id' => $coaProduk['id'],
				'coa_code' => $coaProduk['kode'],
				'coa_level' => 5,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $coaProduk['nama'],
			]);
			$this->db->insert('t_coa_total', $total);
			$total['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $total);
		}
		return $coaProduk;
	}

	public function insert_coa_akumulasi($asset_name, $koreId)
	{
		$coaAkum = $this->db->get_where('coa_4', ['nama' => "Akumulasi Amortisasi " . $asset_name, 'coa3_id' => 19])->row_array();
		if ($coaAkum == null) {
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 19])->result_array();
			$coaAkum = ([
				'nama' => "Akumulasi Amortisasi " . $asset_name,
				'kode' => "100.1.10." . (count($coa4) + 1),
				'coa3_id' => 19,
				'coa_corelation_id' => $koreId
			]);
			$this->db->insert('coa_4', $coaAkum);
			$coaAkum['id'] = $this->db->insert_id();
			$total = ([
				'coa_id' => $coaAkum['id'],
				'coa_code' => $coaAkum['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $coaAkum['nama'],
			]);
			$this->db->insert('t_coa_total', $total);
			$total['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $total);
		}
		return $coaAkum;
	}
	public function insert_coa_amortisasi($asset_name, $koreId, $cekMarketing)
	{
		if ($cekMarketing == 1) {
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 178])->result_array();
			$coa3 = $this->db->get_where('coa_3', ['id' => 178])->row_array();
		} else {
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 179])->result_array();
			$coa3 = $this->db->get_where('coa_3', ['id' => 179])->row_array();
		}
		$coaAmor = $this->db->get_where('coa_4', ['nama' => "Biaya Amortisasi " . $asset_name, 'coa3_id' => $coa3['id']])->row_array();
		if ($coaAmor == null) {
			$coaAmor = ([
				'nama' => "Biaya Amortisasi " . $asset_name,
				'kode' => $coa3['kode'] . "." . (count($coa4) + 1),
				'coa3_id' => $coa3['id'],
				'coa_corelation_id' => $koreId
			]);
			$this->db->insert('coa_4', $coaAmor);
			$coaAmor['id'] = $this->db->insert_id();

			$total = ([
				'coa_id' => $coaAmor['id'],
				'coa_code' => $coaAmor['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $coaAmor['nama'],
			]);
			$this->db->insert('t_coa_total', $total);
			$total['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $total);
		}
		return $coaAmor;
	}
	public function insert_coa_utang($name)
	{
		$coaUtang = $this->db->get_where('coa_4', ['nama' => "Utang " . $name])->row_array();
		if ($coaUtang == null) {
			$coa3 = $this->db->get_where('coa_3', ['id' => 27])->row_array();
			$coa4 = $this->db->get_where('coa_4', ['coa3_id' => 27])->result_array();
			$coaUtang = ([
				'nama' => "Utang " . $name,
				'kode' => $coa3['kode'] . "." . (count($coa4) + 1),
				'coa3_id' => 27
			]);
			$this->db->insert('coa_4', $coaUtang);
			$coaUtang['id'] = $this->db->insert_id();

			$total = ([
				'coa_id' => $coaUtang['id'],
				'coa_code' => $coaUtang['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $coaUtang['nama'],
			]);
			$this->db->insert('t_coa_total', $total);
			$total['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $total);
		}
		return $coaUtang;
	}

	public function input_old_coa($date, $t_activities, $coaAkum, $coaAmortisasi, $idHeader, $presentase, $cek)
	{
		$brands = $this->db->get_where('t_activities_brand', ['activities_id' => $t_activities['activities_id']])->result_array();
		if ($brands == null) {
			$akunAmor = ([
				'coa_name' => $coaAmortisasi['nama'],
				'coa_code' => $coaAmortisasi['kode'],
				'coa_date' => $date,
				'coa_level' => 4,
				'coa_debit' => ($t_activities['activities_value'] / $t_activities['activities_span']),
				'coa_transaction_note' => 'Pembayaran dibayar dimuka By ' . $_SESSION['user_fullname'],
				'date_create' => date('Y-m-d'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 13,
				'coa_transaction_source_id' => $t_activities['activities_id'],
				'coa_group_id' => $idHeader,
				'coa_id' => $coaAmortisasi['id'],
				'coa_transaction_realization' => 1,
			]);
			$this->db->insert('t_coa_transaction', $akunAmor);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $akunAmor[coa_debit] WHERE coa_id = '$akunAmor[coa_id]' AND coa_level = '$akunAmor[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $akunAmor[coa_debit] WHERE coa_id = '$akunAmor[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunAmor['coa_date'] . "', '-', '') AND coa_level = '$akunAmor[coa_level]'");

			$akunAkum = ([
				'coa_name' => $coaAkum['nama'],
				'coa_code' => $coaAkum['kode'],
				'coa_date' => $date,
				'coa_level' => 4,
				'coa_credit' => ($t_activities['activities_value'] / $t_activities['activities_span']),
				'coa_transaction_note' => 'Pembayaran dibayar dimuka By ' . $_SESSION['user_fullname'],
				'date_create' => date('Y-m-d'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 13,
				'coa_transaction_source_id' => $t_activities['activities_id'],
				'coa_group_id' => $idHeader,
				'coa_id' => $coaAkum['id'],
				'coa_transaction_realization' => 1,
			]);
			$this->db->insert('t_coa_transaction', $akunAkum);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $akunAkum[coa_credit] WHERE coa_id = '$akunAkum[coa_id]' AND coa_level = '$akunAkum[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $akunAkum[coa_credit] WHERE coa_id = '$akunAkum[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunAkum['coa_date'] . "', '-', '') AND coa_level = '$akunAkum[coa_level]'");
		} else {
			$produks = $this->db->get_where('t_activities_brand_product', ['activities_id' => $t_activities['activities_id']])->result_array();
			if ($produks == null) {
				$value = round($t_activities['activities_value'] / $t_activities['activities_span'], 2);
				if ($cek == $t_activities['activities_span']) {
					$value = ($t_activities['activities_value'] - ($value * ($t_activities['activities_span'] - 1)));
				}
				// $debit = $this->test_p($value, count($brands));
				$i = 0;
				$countProduk = count($brands);
				$totalDebit = 0;
				foreach ($brands as $brand) {
					if ($i == ($countProduk - 1)) {
						$debit = round($value - $totalDebit, 2);
					} else {
						$debit = round($value * $presentase[$i] / 100, 2);
						$totalDebit += $debit;
					}
					$akunBrand = $this->db->get_where('m_brand', ['brand_id' => $brand['brand_id']])->row_array();
					$coaBrand = $this->db->get_where('coa_5', ['nama' => $t_activities['activities_name'] . " " . $akunBrand['brand_name'], 'coa4_id' => $t_activities['coa_amortitation_id']])->row_array();
					$coa = ([
						'coa_name' => $coaBrand['nama'],
						'coa_code' => $coaBrand['kode'],
						'coa_id' => $coaBrand['id'],
						'coa_date' => $date,
						'coa_level' => 5,
						'coa_debit' => $debit,
						'coa_transaction_note' => 'Pembayaran dibayar dimuka By ' . $_SESSION['user_fullname'],
						'date_create' => date('Y-m-d H:i:s'),
						'user_create' => $_SESSION['user_id'],
						'coa_transaction_source' => 14,
						'coa_transaction_source_id' => $t_activities['activities_id'],
						'coa_transaction_realization' => 1,
						'coa_group_id' => $idHeader,
					]);
					$this->db->insert('t_coa_transaction', $coa);
					$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $coa[coa_debit] WHERE coa_id = '$coa[coa_id]' AND coa_level = '$coa[coa_level]'");
					$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $coa[coa_debit] WHERE coa_id = '$coa[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $coa['coa_date'] . "', '-', '') AND coa_level = '$coa[coa_level]'");
					$i++;
				}
			} else {
				$value = round($t_activities['activities_value'] / $t_activities['activities_span'], 2);
				if ($cek == $t_activities['activities_span']) {
					$value = ($t_activities['activities_value'] - ($value * ($t_activities['activities_span'] - 1)));
				}
				// $debit = $this->test_p($value, count($produks));
				$i = 0;
				$totalDebit = 0;
				$countProduk = count($produks);
				foreach ($produks as $produk) {
					if ($i == ($countProduk - 1)) {
						$debit = round($value - $totalDebit, 2);
					} else {
						$debit = round($value * $presentase[$i] / 100, 2);
						$totalDebit += $debit;
					}
					$akunProduk = $this->db->get_where('produk_global', ['id' => $produk['product_id']])->row_array();
					$coaProduk = $this->db->get_where('coa_5', ['nama' => $t_activities['activities_name'] . " " . $akunProduk['nama_produk'], 'coa4_id' => $t_activities['coa_amortitation_id']])->row_array();
					$coa = ([
						'coa_name' => $coaProduk['nama'],
						'coa_code' => $coaProduk['kode'],
						'coa_id' => $coaProduk['id'],
						'coa_date' => $date,
						'coa_level' => 5,
						'coa_debit' => $debit,
						'coa_transaction_note' => 'Pembayaran dibayar dimuka By ' . $_SESSION['user_fullname'],
						'date_create' => date('Y-m-d H:i:s'),
						'user_create' => $_SESSION['user_id'],
						'coa_transaction_source' => 14,
						'coa_transaction_source_id' => $t_activities['activities_id'],
						'coa_transaction_realization' => 1,
						'coa_group_id' => $idHeader,
					]);
					$this->db->insert('t_coa_transaction', $coa);
					$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $coa[coa_debit] WHERE coa_id = '$coa[coa_id]' AND coa_level = '$coa[coa_level]'");
					$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $coa[coa_debit] WHERE coa_id = '$coa[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $coa['coa_date'] . "', '-', '') AND coa_level = '$coa[coa_level]'");
					$i++;
				}
			}
			$akunAkum = ([
				'coa_name' => $coaAkum['nama'],
				'coa_code' => $coaAkum['kode'],
				'coa_id' => $coaAkum['id'],
				'coa_date' => $date,
				'coa_level' => 4,
				'coa_credit' => $value,
				'coa_transaction_note' => 'Pembayaran dibayar dimuka By ' . $_SESSION['user_fullname'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 14,
				'coa_transaction_source_id' => $t_activities['activities_id'],
				'coa_transaction_realization' => 1,
				'coa_group_id' => $idHeader,
			]);
			$this->db->insert('t_coa_transaction', $akunAkum);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $akunAkum[coa_credit] WHERE coa_id = '$akunAkum[coa_id]' AND coa_level = '$akunAkum[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $akunAkum[coa_credit] WHERE coa_id = '$akunAkum[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $akunAkum['coa_date'] . "', '-', '') AND coa_level = '$akunAkum[coa_level]'");
		}
	}

	public function test_p($value, $count)
	{
		$yourInt = $value;
		$divided = $count;
		$remainder = round($yourInt - (round($yourInt / $divided, 2) * $divided), 2);
		$third = round($yourInt / $divided, 2);
		$lastBit = round($third + $remainder, 2);
		for ($i = 1; $i <= $divided; $i++) {
			if ($i == $divided) {
				$data[] =  $lastBit;
			} else {
				$data[] = $third;
			}
		}
		return $data;
	}

	function get_data()
	{
		$table = $this->table_;
		$id = $this->id_;
		$field = array('activities_name', 'activities_date', 'FORMAT(asset_value, 0)');
		$url = $this->url_;
		$action = '<a href="' . site_url($url . "/detail/xid") . '" class="btn btn-info"> <i class="fa fa-list"></i></a>';
		$jfield = join(', ', $field);
		$arrorder[] = "asset_id DESC";
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}
	function get_data_detail($kode)
	{
		$table = "t_asset A";
		$arrjoin[] = "JOIN t_coa_transaction B ON B.coa_id = A.coa_shrink_id";
		$id = "A.asset_id";
		$field = array('B.coa_date', 'FORMAT(B.coa_credit, 0)');
		$fields = array('coa_date', 'FORMAT(B.coa_credit, 0)');
		$url = $this->url_;
		$jfield = join(', ', $field);
		$arrorder[] = "B.coa_date DESC";
		$arrwhere[] = "A.asset_id = " . $kode . "";
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($fields as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}
}
