<?php if ($discount['discount_active'] == 0) { ?>
<form method="post" action="<?= site_url($url . '/simpan_edit/' . $id); ?>" enctype="multipart/form-data" onsubmit="return confirm('Are you sure?')">
    <?php } ?>
    <div class="row">
        <div class="col-lg-12 col-xl-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Total Pesanan & Syarat Quantity
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label>Nama Diskon</label>
                        <input type="text" class="form-control" placeholder="Nama Diskon" name="input_diskon[discount_name]" value="<?= @$discount['discount_name'] ?>" required>
                    </div>
                    <div id="totalPesanan">
                        <?php $index1 = 0 ?>
                        <?php if (isset($discount_terms['harga'])) { 
                            foreach (@$discount_terms['harga'] as $index => $discount_terms) { ?>
                            <div class="form-group row" id="totalPesanan<?= $index ?>">
                                <div class="col-lg-6">
                                    <label>Produk</label>
                                    <select class="form-control pilihProduk" name="input1[<?= $index ?>][product][]" multiple="multiple">
                                        <?php foreach ($discount_terms as $discount_term) { ?>
                                        <option value="<?= $discount_term['product_kd'] ?>" selected><?= $discount_term['nama_produk'] ?></option>
                                        <?php } ?>
                                        <?= $arrproduct ?>
                                    </select>
                                </div>
                                <div class="col-lg-5">
                                    <label>Harga</label>
                                    <input type="text" class="form-control numeric" placeholder="input harga" name="input1[<?= $index ?>][price]" value="<?= @str_replace(".00", "", $discount_terms[0]['discount_terms_product_price']) ?>" />
                                </div>
                                <div class="col-lg-1">
                                    <br>
                                    <button onclick="deleteTotalPesanan(<?= $index ?>);" class="btn btn-danger">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                            </div>
                            <?php } 
                        } else { ?>
                        <div class="form-group row" id="totalPesanan0">
                            <div class="col-lg-6">
                                <label>Produk</label>
                                <select class="form-control pilihProduk" name="input1[0][product][]" multiple="multiple">
                                    <?= $arrproduct ?>
                                </select>
                            </div>
                            <div class="col-lg-5">
                                <label>Harga</label>
                                <input type="text" class="form-control numeric" placeholder="input harga" name="input1[0][price]" />
                            </div>
                            <div class="col-lg-1">
                                <br>
                                <button onclick="deleteTotalPesanan(0);" class="btn btn-danger">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="kt-form__actions">
                        <button type="button" class="btn btn-warning" onclick="addFormTotal();">Tambah Produk</button>
                    </div>
                    <br>
                    <h5>Syarat Quantity</h5>
                    <div id="syaratQuantity">
                        <?php $index2 = 0 ?>
                        <?php if (isset($discount_terms['quantity'])) {
                            foreach (@$discount_terms['quantity'] as $index => $discount_terms) { ?>
                            <div class="form-group row" id="syaratQuantity<?= $index ?>">
                                <div class="col-lg-6">
                                    <label>Produk</label>
                                    <select class="form-control pilihProduk" name="input2[<?= $index ?>][product][]" multiple="multiple">
                                        <?php foreach ($discount_terms as $discount_term) { ?>
                                        <option value="<?= $discount_term['product_kd'] ?>" selected><?= $discount_term['nama_produk'] ?></option>
                                        <?php } ?>
                                        <?= $arrproduct ?>
                                    </select>
                                </div>
                                <div class="col-lg-5">
                                    <label>Jumlah</label>
                                    <input type="text" class="form-control numeric" placeholder="input jumlah" name="input2[<?= $index ?>][quantity]" value="<?= @str_replace(".00", "", $discount_terms[0]['discount_terms_product_quantity']) ?>" />
                                </div>
                                <div class="col-lg-1">
                                    <br>
                                    <button onclick="deleteSyaratQuantity(<?= $index ?>);" class="btn btn-danger">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                            </div>
                            <?php }
                        } else { ?>
                        <div class="form-group row" id="syaratQuantity0">
                            <div class="col-lg-6">
                                <label>Produk</label>
                                <select class="form-control pilihProduk" name="input2[0][product][]" multiple="multiple">
                                    <?= $arrproduct ?>
                                </select>
                            </div>
                            <div class="col-lg-5">
                                <label>Jumlah</label>
                                <input type="text" class="form-control numeric" placeholder="input jumlah" name="input2[0][quantity]" />
                            </div>
                            <div class="col-lg-1">
                                <br>
                                <button onclick="deleteSyaratQuantity(0);" class="btn btn-danger">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="kt-form__actions">
                        <button type="button" class="btn btn-warning" onclick="addFormQuantity();">Tambah Produk</button>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Tanggal Mulai</label>
                            <!--<input type="text" id="tgl_awal" name="input[package_date]" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d', strtotime(date('Y-m-d') . ' - 1 days')) ?>" required>-->
                            <input type="text" id="tgl_awal" name="tgl_mulai" onchange="$('#tgl_akhir').val($(this).val());" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= $discount['discount_start_date'] ?>" required>
                        </div>
                        <div class="col-sm-6">
                            <label>Tanggal Selesai</label>
                            <input type="text" id="tgl_akhir" name="tgl_selesai" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= $discount['discount_end_date'] ?>" required>
                        </div>
                    </div>
                    <br>
                    <label>Promo Untuk:</label>
                    <div class="row">
                        <div class="col-lg-2">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                <input type="checkbox" value="KLINIK" name="for[KLINIK]" <?= (isset($member['KLINIK'])) ? 'checked' : '' ?>> Klinik
                                <span></span>
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                <input type="checkbox" value="OFFICE" name="for[OFFICE]" <?= (isset($member['OFFICE'])) ? 'checked' : '' ?>> Bali
                                <span></span>
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                <input type="checkbox" value="DISTRIBUTOR" name="for[DISTRIBUTOR]" <?= (isset($member['DISTRIBUTOR'])) ? 'checked' : '' ?>> Distributor
                                <span></span>
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                <input type="checkbox" value="AGEN" name="for[AGEN]" <?= (isset($member['AGEN'])) ? 'checked' : '' ?>> Agen
                                <span></span>
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                <input type="checkbox" value="MEMBER" name="for[MEMBER]" <?= (isset($member['MEMBER'])) ? 'checked' : '' ?>> Member
                                <span></span>
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                <input type="checkbox" value="RESELLER" name="for[RESELLER]" <?= (isset($member['RESELLER'])) ? 'checked' : '' ?>> Reseller
                                <span></span>
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                <input type="checkbox" value="ENDUSER" name="for[ENDUSER]" <?= (isset($member['ENDUSER'])) ? 'checked' : '' ?>> End User
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Promo
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Potongan Harga</label>
                            <input type="text" class="form-control numeric" placeholder="input potongan harga" name="promo[pot_harga]" value="<?= number_format(@$reward['harga']) ?>" />
                        </div>
                        <div class="col-lg-6">
                            <label>Diskon (%)</label>
                            <input type="number" class="form-control" placeholder="input diskon" max="100" name="promo[diskon]" value="<?= number_format(@$reward['persen']) ?>" />
                        </div>
                    </div>
                    <div id="freeProduk">
                        <?php $index3 = 0 ?>
                        <div class="col-lg-6">
                            <label>Free Produk</label>
                        </div>
                        <?php if (isset($reward['barang'])) {
                            foreach ($reward['barang'] as $index => $barangs) { ?>
                            <div class="form-group" id="freeProduk<?= $index ?>">
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <select class="form-control pilihProduk" name="input3[<?= $index ?>][product][]" multiple="multiple">
                                            <?php foreach ($barangs as $barang) { ?>
                                            <option value="<?= $barang['product_kd'] ?>" selected><?= $barang['nama_produk'] ?></option>
                                            <?php } ?>
                                            <?= $arrproduct ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-5">
                                        <input type="text" class="form-control numeric" placeholder="input jumlah" name="input3[<?= $index ?>][quantity]" value="<?= str_replace(".00", "", $barangs[0]['discount_reward_product_quantity']) ?>" />
                                    </div>
                                    <div class="col-lg-1">
                                        <button onclick="deleteFreeProduk(<?= $index ?>);" class="btn btn-danger">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <?php }
                        } else { ?>
                        <div class="form-group" id="freeProduk0">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <select class="form-control pilihProduk" name="input3[0][product][]" multiple="multiple">
                                        <?= $arrproduct ?>
                                    </select>
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control numeric" placeholder="input jumlah" name="input3[0][quantity]" />
                                </div>
                                <div class="col-lg-1">
                                    <button onclick="deleteFreeProduk(0);" class="btn btn-danger">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="kt-form__actions">
                        <button type="button" class="btn btn-warning" onclick="addFreeProduk();">Tambah Produk</button>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <?php if ($discount['discount_active'] == 0) { ?>
                        <button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                        <?php } else if ($discount['discount_active'] == 1) { ?>
                        <a type="button" href="<?= site_url($url . '/non_active/' . $id); ?>" name="non_active" value="non_active" class="btn btn-success">Non Active</a>
                        <?php } ?>
                        <a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <?php if ($discount['discount_active'] == 0) { ?>
</form>
<?php } ?>
<script>
    var index1 = <?= $index1 ?>;
    var index2 = <?= $index2 ?>;
    var index3 = <?= $index3 ?>;

    $('.pilihProduk').select2({
        placeholder: "Pilih Produk",
    });

    function addFormTotal() {
        index1 = index1 + 1;
        var html = '<div class="form-group row" id="totalPesanan' + index1 + '"><div class="col-lg-6">    <label>Produk</label>    <select class="form-control pilihProduk" name="input1[' + index1 + '][product][]" multiple="multiple">        <?= $arrproduct ?>    </select></div><div class="col-lg-5">    <label>Harga</label>    <input type="text" class="form-control numeric" placeholder="input harga" name="input1[' + index1 + '][price]" /></div><div class="col-lg-1"><br><button onclick="deleteTotalPesanan(' + index1 + ');" class="btn btn-danger">    <i class="fas fa-trash"></i></button></div></div>';
        $('#totalPesanan').append(html);
        $('.pilihProduk').select2({
            placeholder: "Pilih Produk",
        });
        $(".numeric").mask("#,##0", {
            reverse: true
        });
    }

    function deleteTotalPesanan(id) {
        $('#totalPesanan' + id).remove()
    }

    function addFormQuantity() {
        index2 = index2 + 1;
        var html = '<div class="form-group row" id="syaratQuantity' + index2 + '"><div class="col-lg-6">    <label>Produk</label>    <select class="form-control pilihProduk" name="input2[' + index2 + '][product][]" multiple="multiple">        <?= $arrproduct ?>    </select></div><div class="col-lg-5">    <label>Jumlah</label>    <input type="text" class="form-control numeric" placeholder="input jumlah" name="input2[' + index2 + '][quantity]" /></div><div class="col-lg-1"><br><button onclick="deleteSyaratQuantity(' + index2 + ');" class="btn btn-danger">    <i class="fas fa-trash"></i></button></div></div>';
        $('#syaratQuantity').append(html);
        $('.pilihProduk').select2({
            placeholder: "Pilih Produk",
        });
        $(".numeric").mask("#,##0", {
            reverse: true
        });
    }

    function deleteSyaratQuantity(id) {
        $('#syaratQuantity' + id).remove();
    }

    function addFreeProduk() {
        index3 = index3 + 1;
        var html = '<div class="form-group" id="freeProduk' + index3 + '"><div class="form-group row">    <div class="col-lg-6">        <select class="form-control pilihProduk" name="input3[' + index3 + '][product][]" multiple="multiple">            <?= $arrproduct ?>        </select>    </div>    <div class="col-lg-5">        <input type="text" class="form-control numeric" placeholder="input jumlah" name="input3[' + index3 + '][quantity]" />    </div>    <div class="col-lg-1"><button onclick="deleteFreeProduk(' + index3 + ');" class="btn btn-danger">            <i class="fas fa-trash"></i>        </button>    </div></div></div>';
        $('#freeProduk').append(html);
        $('.pilihProduk').select2({
            placeholder: "Pilih Produk",
        });
        $(".numeric").mask("#,##0", {
            reverse: true
        });
    }

    function deleteFreeProduk(id) {
        $('#freeProduk' + id).remove();
    }

    $(document).ready(function() {
        $('input[type="number"]').on('keyup', function() {
            v = parseInt($(this).val());
            min = parseInt($(this).attr('min'));
            max = parseInt($(this).attr('max'));

            /*if (v < min){
                $(this).val(min);
            } else */
            if (v > max) {
                $(this).val(max);
            }
        })
    })
</script>