<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Diskon extends CI_Controller
{
    var $url_ = "Diskon";
    var $id_  = "id_factory";
    var $eng_ = "diskon";
    var $ind_ = "diskon";

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            $this->url_ . 'Model'  =>  'Model',
        ));
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        $datacontent['url'] = $this->url_;
        $datacontent['title'] = 'Data ' . $this->ind_; 
		
		
        $datacontent['diskons'] = $this->db->get('m_discount')->result_array();
        $data['file'] = $this->ind_;
        $data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
        $data['title'] = $datacontent['title'];
        $this->load->view('Layout/home', $data);
    }

    public function get_data()
    {
        $datacontent['datatable'] = $this->Model->get_data();
    }

    public function form()
    {
        $datacontent['url'] = $this->url_;
        $datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['m_member_status'] = $this->db->get('m_member_status')->result_array();
        $products = $this->db->get('produk_global')->result_array();
        $datacontent['arrproduct'] = '';
        foreach ($products as $product) {
            $datacontent['arrproduct'] .= '<option value="' . $product['kode'] . '">' . $product['nama_produk'] . '</option>';
        }
        $data['file'] = $this->ind_;
        $data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
        $data['title'] = $datacontent['title'];
        $this->load->view('Layout/home', $data);
    }
    public function edit($id)
    {
        $datacontent['url'] = $this->url_;
        $datacontent['id'] = $id;
        $datacontent['title'] = 'Data ' . $this->ind_;
        $products = $this->db->get('produk_global')->result_array();
        $datacontent['arrproduct'] = '';
        $datacontent['arrproduct_harga'] = '';
        $datacontent['arrproduct_quantity'] = '';
        $datacontent['discount'] = $this->db->get_where('m_discount', ['discount_id' => $id])->row_array();
        $discount_terms = $this->db->query("SELECT B.* FROM m_discount_terms A JOIN m_discount_terms_product B ON A.discount_terms_id = B.discount_terms_id WHERE A.discount_id = $id")->result_array();
        $datacontent['discount_terms'] = array();
        foreach ($discount_terms as  $discount_term) {
            $product = $this->db->get_where('produk', ['kd_pd' => $discount_term['product_kd']])->row_array();
            if ($discount_term['discount_terms_product_type'] == 2) {
                $discount_term['nama_produk'] = $product['nama_produk'];
                $datacontent['discount_terms']['harga'][$discount_term['discount_terms_id']][] = $discount_term;
            } else {
                $discount_term['nama_produk'] = $product['nama_produk'];
                $datacontent['discount_terms']['quantity'][$discount_term['discount_terms_id']][] = $discount_term;
            }
        }
        $members = $this->db->get_where('m_discount_member', ['discount_id' => $id])->result_array();
        foreach ($members as $member) {
            $datacontent['member'][$member['member_status']] = $member;
        }
        foreach ($products as $product) {
            $datacontent['arrproduct'] .= '<option value="' . $product['kode'] . '">' . $product['nama_produk'] . '</option>';
        }
        $rewards = $this->db->query("SELECT B.* FROM m_discount_reward A JOIN m_discount_reward_product B ON A.discount_reward_id = B. discount_reward_id WHERE A.discount_id = $id")->result_array();

        foreach ($rewards as $reward) {
            if ($reward['discount_reward_product_type'] == 1) {
                $datacontent['reward']['harga'] = $reward['discount_reward_product_deduction_cost'];
            }
            if ($reward['discount_reward_product_type'] == 2) {
                $datacontent['reward']['persen'] = $reward['discount_reward_product_percentage'];
            }
            if ($reward['discount_reward_product_type'] == 3) {
                $product = $this->db->get_where('produk', ['kd_pd' => $reward['product_kd']])->row_array();
                $reward['nama_produk'] = $product['nama_produk'];
                $datacontent['reward']['barang'][$reward['discount_reward_id']][] = $reward;
            }
        }
        $data['file'] = $this->ind_;
        $data['content'] = $this->load->view($this->url_ . '/form_edit', $datacontent, TRUE);
        $data['title'] = $datacontent['title'];
        $this->load->view('Layout/home', $data);
    }

    public function simpan()
    {
        $this->db->trans_begin();
		
		
		if($_POST['discount_type'] == 2){
			
			$diskon = ([
				'discount_name' => $_POST['input_diskon']['discount_name'],
				'discount_start_date' => $_POST['tgl_mulai'],
				'discount_end_date' => $_POST['tgl_selesai'],
				'discount_create_user_id' => $_SESSION['user_id'],
				'discount_active' => ((date('Ymd') >= str_replace('-', '', $_POST['tgl_mulai']) && date('Ymd') <= str_replace('-', '', $_POST['tgl_selesai']))?'':''),
				'discount_type' => $_POST['discount_type'],
				'warehouse_id' => $_SESSION['warehouse_id'],
			]);
			
			if (isset($_POST['for'])) {
				foreach ($_POST['for'] as $index => $for) {
					$diskon_member = ([
						'discount_id' =>  $diskon['discount_id'],
						'member_status' => $index,
					]);
					//$this->db->insert('m_discount_member', $diskon_member);
				}
			}
			print_r($_POST['for']); 
			die();
		}
		print_r($_POST);
		die();
        $diskon = ([
            'discount_start_date' => $_POST['tgl_mulai'],
            'discount_end_date' => $_POST['tgl_selesai'],
            'discount_create_date' => date('Y-m-d H:i:s'),
            'discount_create_user_id' => $_SESSION['user_id'],
            'discount_active' => 1,
            'discount_name' => $_POST['input_diskon']['discount_name'],
        ]);
        if (isset($_POST['akumulasi'])) {
            $diskon['discount_accumulation'] = 1;
        }
        $this->db->insert('m_discount', $diskon);
        $diskon['discount_id'] = $this->db->insert_id();
        if (isset($_POST['for'])) {
            foreach ($_POST['for'] as $index => $for) {
                $diskon_member = ([
                    'discount_id' =>  $diskon['discount_id'],
                    'member_status' => $index,
                ]);
                $this->db->insert('m_discount_member', $diskon_member);
            }
        }
        if (isset($_POST['input1'][0]['product'])) {
            foreach ($_POST['input1'] as $input1) {
                $this->db->insert('m_discount_terms', ['discount_id' => $diskon['discount_id']]);
                $diskon_term['discount_terms_id'] = $this->db->insert_id();

                foreach ($input1['product'] as $product) {
                    $diskon_term_prod = array();
                    $diskon_term_prod = ([
                        'discount_terms_id' => $diskon_term['discount_terms_id'],
                        'discount_id' => $diskon['discount_id'],
                        'product_kd' => $product,
                        'discount_terms_product_price' => str_replace(",", "", $input1['price']),
                        'discount_terms_product_type' => 2,
                    ]);
                    $this->db->insert('m_discount_terms_product', $diskon_term_prod);
                }
            }
        }
        if (isset($_POST['input2'][0]['product'])) {
            foreach ($_POST['input2'] as $input2) {
                $this->db->insert('m_discount_terms', ['discount_id' => $diskon['discount_id']]);
                $diskon_term['discount_terms_id'] = $this->db->insert_id();

                foreach ($input2['product'] as $product) {
                    $diskon_term_prod = array();
                    $diskon_term_prod = ([
                        'discount_terms_id' => $diskon_term['discount_terms_id'],
                        'discount_id' => $diskon['discount_id'],
                        'product_kd' => $product,
                        'discount_terms_product_quantity' => str_replace(",", "", $input2['quantity']),
                        'discount_terms_product_type' => 1,
                    ]);
                    $this->db->insert('m_discount_terms_product', $diskon_term_prod);
                }
            }
        }
        $this->db->insert('m_discount_reward', ['discount_id' => $diskon['discount_id']]);
        $diskon_reward['discount_reward_id'] = $this->db->insert_id();

        if ($_POST['promo']['pot_harga'] !== '' || $_POST['promo']['diskon'] !== '') {
            $pot_harga = str_replace(",", "", $_POST['promo']['pot_harga']);
            if ($pot_harga > 0 || $pot_harga != '') {
                $diskon_reward_product = ([
                    'discount_reward_product_type' => 1,
                    'discount_reward_product_deduction_cost' => $pot_harga,
                ]);
            } else {
                $diskon_reward_product = ([
                    'discount_reward_product_type' => 2,
                    'discount_reward_product_percentage' => str_replace(",", "", $_POST['promo']['diskon']),
                ]);
            }
            $diskon_reward_product['discount_reward_id'] = $diskon_reward['discount_reward_id'];
            $this->db->insert('m_discount_reward_product', $diskon_reward_product);
        }

        foreach ($_POST['input3']  as $input3) {
            if (isset($input3['product'])) {
                $this->db->insert('m_discount_reward', ['discount_id' => $diskon['discount_id']]);
                $diskon_reward['discount_reward_id'] = $this->db->insert_id();
                foreach ($input3['product'] as $product) {
                    $diskon_reward_product = ([
                        'discount_reward_id' => $diskon_reward['discount_reward_id'],
                        'discount_reward_product_type' => 3,
                        'product_kd' => $product,
                        'discount_reward_product_quantity' => $input3['quantity'],
                    ]);
                    $this->db->insert('m_discount_reward_product', $diskon_reward_product);
                }
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        redirect(base_url() . 'Diskon');
    }
    public function simpan_edit($id)
    {
        $this->db->trans_begin();
        $this->db->query("DELETE FROM m_discount_terms A JOIN m_discount_terms_product B ON A.discount_terms_id = B.discount_terms_id WHERE A.discount_id = $id");
        $this->db->query("DELETE FROM m_discount_reward A JOIN m_discount_reward_product B ON A.discount_reward_id = B.discount_reward_id WHERE A.discount_id = $id");
        $this->db->query("DELETE FROM m_discount_member WHERE discount_id = $id");
        $diskon = $this->db->get_where('m_discount', ['discount_id' => $id])->row_array();
        if (isset($_POST['for'])) {
            foreach ($_POST['for'] as $index => $for) {
                $diskon_member = ([
                    'discount_id' =>  $diskon['discount_id'],
                    'member_status' => $index,
                ]);
                $this->db->insert('m_discount_member', $diskon_member);
            }
        }
        if (isset($_POST['input1'][0]['product'])) {
            foreach ($_POST['input1'] as $input1) {
                $this->db->insert('m_discount_terms', ['discount_id' => $diskon['discount_id']]);
                $diskon_term['discount_terms_id'] = $this->db->insert_id();

                foreach ($input1['product'] as $product) {
                    $diskon_term_prod = array();
                    $diskon_term_prod = ([
                        'discount_terms_id' => $diskon_term['discount_terms_id'],
                        'discount_id' => $diskon['discount_id'],
                        'product_kd' => $product,
                        'discount_terms_product_price' => str_replace(",", "", $input1['price']),
                        'discount_terms_product_type' => 2,
                    ]);
                    $this->db->insert('m_discount_terms_product', $diskon_term_prod);
                }
            }
        }
        if (isset($_POST['input2'][0]['product'])) {
            foreach ($_POST['input2'] as $input2) {
                $this->db->insert('m_discount_terms', ['discount_id' => $diskon['discount_id']]);
                $diskon_term['discount_terms_id'] = $this->db->insert_id();

                foreach ($input2['product'] as $product) {
                    $diskon_term_prod = array();
                    $diskon_term_prod = ([
                        'discount_terms_id' => $diskon_term['discount_terms_id'],
                        'discount_id' => $diskon['discount_id'],
                        'product_kd' => $product,
                        'discount_terms_product_quantity' => str_replace(",", "", $input2['quantity']),
                        'discount_terms_product_type' => 1,
                    ]);
                    $this->db->insert('m_discount_terms_product', $diskon_term_prod);
                }
            }
        }
        $this->db->insert('m_discount_reward', ['discount_id' => $diskon['discount_id']]);
        $diskon_reward['discount_reward_id'] = $this->db->insert_id();

        if ($_POST['promo']['pot_harga'] !== '' || $_POST['promo']['diskon'] !== '') {
            $pot_harga = str_replace(",", "", $_POST['promo']['pot_harga']);
            if ($pot_harga > 0 || $pot_harga != '') {
                $diskon_reward_product = ([
                    'discount_reward_product_type' => 1,
                    'discount_reward_product_deduction_cost' => $pot_harga,
                ]);
            } else {
                $diskon_reward_product = ([
                    'discount_reward_product_type' => 2,
                    'discount_reward_product_percentage' => str_replace(",", "", $_POST['promo']['diskon']),
                ]);
            }
            $diskon_reward_product['discount_reward_id'] = $diskon_reward['discount_reward_id'];
            $this->db->insert('m_discount_reward_product', $diskon_reward_product);
        }

        foreach ($_POST['input3']  as $input3) {
            if (isset($input3['product'])) {
                $this->db->insert('m_discount_reward', ['discount_id' => $diskon['discount_id']]);
                $diskon_reward['discount_reward_id'] = $this->db->insert_id();
                foreach ($input3['product'] as $product) {
                    $diskon_reward_product = ([
                        'discount_reward_id' => $diskon_reward['discount_reward_id'],
                        'discount_reward_product_type' => 3,
                        'product_kd' => $product,
                        'discount_reward_product_quantity' => $input3['quantity'],
                    ]);
                    $this->db->insert('m_discount_reward_product', $diskon_reward_product);
                }
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        redirect(base_url() . 'Diskon');
    }

    public function non_active($id)
    {
        $this->db->query("UPDATE m_discount SET discount_active = 2 WHERE discount_id = $id");
        redirect(base_url() . 'Diskon');
    }
}
