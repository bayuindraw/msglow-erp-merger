<?php
if ($parameter == 'ubah' && $id != '') {
	//$this->db->where('A.id_kemasan', $id);
	$row = $this->Model->get($id)->row_array();
}
?>
<div class="row"
								<div class="col-lg-12 col-xl-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Nama</label>
													<input type="text" class="form-control" placeholder="Nama" name="input[nama_kemasan]" value="<?= @$row['nama_kemasan'] ?>" required>
												</div>
												<div class="form-group">
													<label>Supplier</label>
													<select name="input[supplier]" id="pilihSupplier" class="md-form-control md-static subkategori" required> 
														<option></option>
														<?php
														$query = $this->Model->ViewAsc('supplier','nama_supplier');
														foreach ($query as $key => $vaKemasan) {
														?>
														<option value="<?=$vaKemasan['kode_sup']?>" <?= ($vaKemasan['kode_sup']==@$row['supplier'])?'selected':''; ?>><?=$vaKemasan['nama_supplier']?> (<?=$vaKemasan['kode_sup']?>)</option>
													<?php } ?>
													</select>
												</div>
												<div class="form-group">
													<div class="kt-radio-list">
														<label class="kt-radio">
															<input type="checkbox" name="transfer_fee_chk" onclick="$('.divtransferfee').toggle()" <?= (@$row['id_kemasan_print'] != "")?"checked":"";?>> Bahan Baku
															<span></span>
														</label>
													</div>
												</div>
												<div class="form-group divtransferfee" <?= (@$row['id_kemasan_print'] != "")?"":'style="display:none;"';?>>
													<label>Barang Jadi</label>
													<select name="input[id_kemasan_print]" id="pilihbahanbaku" class="md-form-control md-static subkategori"> 
														<option></option>
														<?php
														$query = $this->Model->get_kemasan_bahan();
														foreach ($query as $key => $vaKemasan) {  
														?>
														<option value="<?=$vaKemasan['id_kemasan']?>" <?= ($vaKemasan['id_kemasan']==@$row['id_kemasan_print'])?'selected':''; ?>><?=$vaKemasan['nama_kemasan']?></option>
													<?php } ?> 
													</select>
												</div>
												<div class="form-group">
													<label>Klasifikasi</label>
													<select name="input[product_classification_detail_id]" id="pilihklasifikasi" class="md-form-control md-static subkategori"> 
														<option></option>
														<?php
														$query = $this->Model->get_product_classification_detail();
														foreach ($query as $key => $vaKemasan) {
														?>
														<option value="<?=$vaKemasan['product_classification_detail_id']?>" <?= ($vaKemasan['product_classification_detail_id']==@$row['product_classification_detail_id'])?'selected':''; ?>><?=$vaKemasan['name']?></option>
													<?php } ?>
													</select>
												</div>
												<div class="form-group">
													<label>Kategori</label>
													<select name="input[kategori]" id="pilihKategori" class="md-form-control md-static subkategori" required> 
														<option></option>
														<?php
														$query = $this->Model->ViewAsc('m_category','category_id');
														foreach ($query as $key => $vaKemasan) {
														?>
														<option value="<?=$vaKemasan['category_code']?>" <?= ($vaKemasan['category_code']==@$row['kategori'])?'selected':''; ?>><?=$vaKemasan['category_code']?></option>
													<?php } ?>
													</select>
												</div>
												<div class="form-group">
													<label>Jumlah Min</label>
													<input type="text" class="form-control" placeholder="Jumlah Min" name="stock[min_jumlah]" value="<?= @$row['min_jumlah'] ?>" required>
												</div>
												<div class="form-group">
													<label>Jumlah Aman</label>
													<input type="text" class="form-control" placeholder="Jumlah Aman" name="stock[am_jumlah]" value="<?= @$row['am_jumlah'] ?>" required>
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>