<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Laporan extends CI_Controller
{

	var $url_ = "Laporan";
	var $ind_ = "Laporan";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function search_member($search = '')
	{
		$this->Model->search_member($search);
	}

	public function daily_product_out()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Barang Keluar Per Produk Harian';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_daily_product_out', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_daily_product_out()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$tanggal = $_POST['cTanggal'];
		$product_id = $_POST['product_id'];
		$arrproduct = $this->db->query("SELECT nama_produk FROM produk A WHERE A.id_produk = '$product_id'")->row_array();
		$arrpackage_trial = $this->db->query("SELECT nama, kode, package_detail_quantity FROM t_package_trial_detail A LEFT JOIN t_package_trial B ON B.package_id =  A.package_id LEFT JOIN member C ON C.kode = B.member_code WHERE A.product_id = '$product_id' AND B.package_date = '$tanggal'")->result_array();

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:D2')->applyFromArray($style);
		$sheet->getStyle('B2:D2')->getFont()->setSize(20);
		$sheet->getStyle('B2:D2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'LAPORAN PENGIRIMAN ' . $arrproduct['nama_produk'] . ' ' . (int)substr($tanggal, 8, 2) . ' ' . $arrbln[substr($tanggal, 5, 2)] . ' ' . substr($tanggal, 0, 4));
		$sheet->mergeCells('B2:D2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH');

		$nawal = $awal;
		$total = 0;
		foreach ($arrpackage_trial as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['kode']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['package_detail_quantity']);
			$total +=  $value1['package_detail_quantity'];
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$sheet->setCellValue($nabjad++ . $nawal, '');
		$sheet->setCellValue($nabjad++ . $nawal, 'TOTAL');
		$sheet->setCellValue($nabjad++ . $nawal, $total);

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENGIRIMAN " . $arrproduct['nama_produk'] . " " . (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4) . " .xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function daily_out()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Harian Barang Keluar';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_daily_out', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_daily_out()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$tanggal = $_POST['cTanggal'];
		$arrpackage_trial = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN member B ON B.kode = A.member_code WHERE A.package_date = '$tanggal'")->result_array();
		$arrpackage_trial_detail = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = B.product_id AND C.is_delete = 0 WHERE A.package_date = '$tanggal'")->result_array();
		$arrpackage_trial_employee = $this->db->query("SELECT * FROM t_package_trial A LEFT JOIN t_package_trial_employee_real B ON B.package_id = A.package_id LEFT JOIN tb_phl C ON C.id_phl = B.employee_id WHERE A.package_date = '$tanggal'")->result_array();
		foreach ($arrpackage_trial_detail as $index2 => $value2) {
			$datpackage_trial_detail[$value2['package_id']][] = $value2;
		}
		foreach ($arrpackage_trial_employee as $index3 => $value3) {
			$datpackage_trial_employee[$value3['package_id']][] = $value3;
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:F2')->applyFromArray($style);
		$sheet->getStyle('B2:F2')->getFont()->setSize(20);
		$sheet->getStyle('B2:F2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'LAPORAN PENGIRIMAN ' . (int)substr($tanggal, 8, 2) . ' ' . $arrbln[substr($tanggal, 5, 2)] . ' ' . substr($tanggal, 0, 4));
		$sheet->mergeCells('B2:F2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'KODE');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PRODUK');
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PEGAWAI');

		$nawal = $awal;
		foreach ($arrpackage_trial as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value1['member_code']);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama'] . " (" . $value1['kode'] . ")");
			$nawalx = $nawal;
			$abjadxawal = $nabjad;
			$abjadx = $abjadxawal;
			$x = $nawalx;
			if (@count($datpackage_trial_detail[$value1['package_id']]) > 0) {
				foreach ($datpackage_trial_detail[$value1['package_id']] as $indexd2 => $valued2) {
					$x = $nawalx;
					$abjadx = $abjadxawal;
					$sheet->setCellValue($abjadx++ . $nawalx, $valued2['nama_produk']);
					$sheet->setCellValue($abjadx . $nawalx++, $valued2['package_detail_quantity']);
				}
			}
			$abjadx++;
			$nabjad = $abjadx;
			$nawaly = $nawal;
			if (@count(@$datpackage_trial_employee[$value1['package_id']]) > 0) {
				foreach (@$datpackage_trial_employee[$value1['package_id']] as $indexd3 => $valued3) {
					$y = $nawaly;
					$abjadx = $abjadxawal;
					$sheet->setCellValue($nabjad . $nawaly++, $valued3['nama_phl']);
				}
			}
			if ($x > @$y) $nawal = $x;
			else $nawal = @$y;
		}

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENGIRIMAN " . (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4) . " .xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function daily_all()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Harian Stok';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_daily_all', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_daily_all()
	{

		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$tanggal = $_POST['cTanggal'];
		//$tanggal = '2020-11-05';
		$tgl_before = (date('Y-m-d', strtotime($tanggal . ' - 1 days')) < '2020-11-01') ? substr($tanggal, 0, 7) . '-01' : date('Y-m-d', strtotime($tanggal . ' - 1 days'));
		// $arrstock_produk_history = $this->db->query("SELECT * FROM tb_stock_produk_history A LEFT JOIN produk B ON B.id_produk = A.id_barang AND B.is_delete = 0 LEFT JOIN m_product_type C ON C.product_type_name = B.klasifikasi WHERE LEFT(tanggal, 7) = '" . substr($tanggal, 0, 7) . "' ORDER BY C.product_type_order, B.id_produk")->result_array();
		$arrstock_produk_history = $this->db->query("SELECT A.id_stock, A.tanggal, A.id_barang, A.jumlah, A.min_jumlah, A.rusak, B.klasifikasi, B.product_type, B.nama_produk, C.product_type_name FROM tb_stock_produk_history A LEFT JOIN produk B ON B.id_produk = A.id_barang AND B.is_delete IS NULL LEFT JOIN m_product_type C ON C.product_type_name = B.klasifikasi WHERE LEFT(A.tanggal, 7) = '" . substr($tanggal, 0, 7) . "' ORDER BY C.product_type_order, B.id_produk")->result_array();
		//query terbaru nunggu data dari alfa baru bisa coba SELECT A.id_stock, A.tanggal, A.id_barang, A.jumlah, A.min_jumlah, A.rusak, B.klasifikasi, B.product_type, B.nama_produk, C.product_type_name FROM tb_stock_produk_history A LEFT JOIN produk B ON B.id_produk = A.id_barang AND B.is_delete IS NULL LEFT JOIN m_product_type C ON C.product_type_name = B.klasifikasi LEFT JOIN terima_produk D ON D.id_barang = B.id_produk LEFT JOIN t_package_trial E ON D.invoice_date = E.package_date WHERE D.tgl_terima = '2021-06-29' ORDER BY C.product_type_order, B.id_produk
		$arrin_past = $this->db->query("SELECT SUM(jumlah) as jumlah, id_barang as id_produk FROM terima_produk WHERE tgl_terima BETWEEN '" . substr($tanggal, 0, 7) . '-01' . "' AND '$tgl_before' GROUP BY id_barang")->result_array();
		foreach ($arrin_past as $indexin => $valuein) {
			$datin_past[$valuein['id_produk']] = $valuein['jumlah'];
		}
		$arrout_past = $this->db->query("SELECT SUM(B.package_detail_quantity) as jumlah, B.product_id as id_produk FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE A.package_date BETWEEN '" . substr($tanggal, 0, 7) . '-01' . "' AND '$tgl_before' GROUP BY B.product_id")->result_array();
		foreach ($arrout_past as $indexout => $valueout) {
			$datout_past[$valueout['id_produk']] = $valueout['jumlah'];
		}
		$arrin_new = $this->db->query("SELECT SUM(jumlah) as jumlah, id_barang as id_produk FROM terima_produk WHERE tgl_terima = '" . $tanggal . "' GROUP BY id_barang")->result_array();
		foreach ($arrin_new as $indexin_new => $valuein_new) {
			$datin_new[$valuein_new['id_produk']] = $valuein_new['jumlah'];
		}
		$arrout_new = $this->db->query("SELECT SUM(B.package_detail_quantity) as jumlah, B.product_id as id_produk FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id WHERE A.package_date = '" . $tanggal . "' GROUP BY B.product_id")->result_array();
		foreach ($arrout_new as $indexout_new => $valueout_new) {
			$datout_new[$valueout_new['id_produk']] = $valueout_new['jumlah'];
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->getStyle('B2:I2')->applyFromArray($style);
		$sheet->getStyle('B2:I2')->getFont()->setSize(20);
		$sheet->getStyle('B2:I2')->getFont()->setBold(true);
		$sheet->setCellValue('B2', 'LAPORAN HARIAN ' . (int)substr($tanggal, 8, 2) . ' ' . $arrbln[substr($tanggal, 5, 2)] . ' ' . substr($tanggal, 0, 4));
		$sheet->mergeCells('B2:I2');

		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'KLASIFIKASI');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');
		$sheet->mergeCells($abjad . $awal . ':I' . $awal);
		$sheet->getStyle($abjad . $awal . ':I' . $awal)->applyFromArray($styleArray);
		$sheet->setCellValue($abjad . $awal++, (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4));
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STOCK AWAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BARANG DATANG');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BARANG KELUAR');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STOCK AKHIR');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'KETERANGAN');
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . ($awal - 1) . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($x == $abjad) break;
		}
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		//$sheet->getStyle('E5:I1')->getAlignment()->setWrapText(true);
		$nawal = $awal;
		$i = 1;
?>
		<table border="1">
			<thead>
				<tr>
					<td>KLASIFIKASI</td>
					<td>NAMA PRODUK</td>
					<td>STOK AWAL</td>
					<td>TANGGAL AWAL</td>
					<td>TANGGAL AKHIR</td>
					<td>SUM</td>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($arrstock_produk_history as $index1 => $value1) {
					// $nabjad = $abjad_awal;
					// $nawal++;
					// $stockawal = ($value1['jumlah'] + ((@$datin_past[$value1['id_barang']] == "") ? 0 : @$datin_past[$value1['id_barang']]) - ((@$datout_past[$value1['id_barang']] == "") ? 0 : @$datout_past[$value1['id_barang']]));
					// $sheet->setCellValue($nabjad++ . $nawal, $i++);
					// $sheet->setCellValue($nabjad++ . $nawal, $value1['klasifikasi']);
					// $sheet->setCellValue($nabjad++ . $nawal, $value1['nama_produk']);
					// $sheet->setCellValue($nabjad++ . $nawal, $stockawal);
					// $sheet->setCellValue($nabjad++ . $nawal, @$datin_new[$value1['id_barang']]);
					// $sheet->setCellValue($nabjad++ . $nawal, @$datout_new[$value1['id_barang']]);
					// $sheet->setCellValue($nabjad++ . $nawal, ($stockawal + ((@$datin_new[$value1['id_barang']] == "") ? 0 : @$datin_new[$value1['id_barang']]) - ((@$datout_new[$value1['id_barang']] == "") ? 0 : $datout_new[$value1['id_barang']])));
					// for ($x = $abjad_awal;; $x++) {
					// 	$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
					// 	if ($x == $nabjad) break;
					// }
					$stockawal = ($value1['jumlah'] + ((@$datin_past[$value1['id_barang']] == "") ? 0 : @$datin_past[$value1['id_barang']]) - ((@$datout_past[$value1['id_barang']] == "") ? 0 : @$datout_past[$value1['id_barang']]));
				?>

					<tr>
						<td>
							<?= $value1['klasifikasi'] ?>
						</td>
						<td>
							<?= $value1['nama_produk'] ?>
						</td>
						<td>
							<?= $stockawal ?>
						</td>
						<td>
							<?= @$datin_new[$value1['id_barang']] ?>
						</td>
						<td>
							<?= @$datout_new[$value1['id_barang']] ?>
						</td>
						<td>
							<?= ($stockawal + ((@$datin_new[$value1['id_barang']] == "") ? 0 : @$datin_new[$value1['id_barang']]) - ((@$datout_new[$value1['id_barang']] == "") ? 0 : $datout_new[$value1['id_barang']])) ?>
						</td>
					</tr>

				<?php
				}
				?>
			</tbody>
		</table>
<?php


		// $writer = new Xlsx($spreadsheet);
		// header("Content-Type: application/vnd.ms-excel charset=utf-8");
		// header("Content-Disposition: attachment; filename=\"LAPORAN HARIAN " . (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4) . " .xlsx\"");
		// header("Cache-Control: max-age=0");
		// $writer->save('php://output');
	}

	public function monthly_out()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Barang Keluar Per Produk Bulanan';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['kd_pd'] . '">' . $value['nama_produk'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_out', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_monthly_out()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$product_id = $_POST['product_id'];
		$arrpackage_trial = $this->db->query("SELECT SUM(B.package_detail_quantity) AS jumlah, A.package_date, A.member_code, D.nama, C.nama_produk FROM t_package_trial A LEFT JOIN t_package_trial_detail B ON B.package_id = A.package_id LEFT JOIN produk C ON C.id_produk = B.product_id LEFT JOIN member D ON D.kode = A.member_code LEFT JOIN tb_stock_produk E ON E.id_barang = C.id_produk WHERE LEFT(A.package_date, 7) = '" . $tahun . "-" . $bulan . "' AND C.kd_pd = '$product_id' AND E.warehouse_id ='1' GROUP BY A.package_date, A.member_code ORDER BY A.package_date")->result_array();
		$tanggalx = array();
		$sellerx = array();
		$arrdata = array();
		foreach ($arrpackage_trial as $index => $value) {
			$tanggalx[$value['package_date']] = $value['package_date'];
			$sellerx[$value['member_code']] = $value['nama'];
			$arrdata[$value['member_code']][$value['package_date']] = $value['jumlah'];
			$barang = $value['nama_produk'];
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$sheet->setCellValue('A1', 'Laporan Bulanan');
		$sheet->setCellValue('A2', @$barang);
		$sheet->getStyle('A1:A2')->getFont()->setSize(20);
		$sheet->getStyle('A1:A2')->getFont()->setBold(true);
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$awal = 3;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NAMA');
		foreach (@$tanggalx as $index2 => $value2) {
			$sheet->setCellValue($abjad++ . $awal, (int)substr($value2, 8, 2) . "-" . substr($arrbln[$bulan], 0, 3) . "-" . substr($tahun, 2, 4));
			$total[$index2] = 0;
		}

		$nawal = $awal;
		foreach (@$sellerx as $index3 => $value3) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value3);
			foreach ($tanggalx as $index2 => $value2) {
				$sheet->setCellValue($nabjad++ . $nawal, @$arrdata[$index3][$index2]);
				$total[$index2] = $total[$index2] + ((@$arrdata[$index3][$index2] != "") ? @$arrdata[$index3][$index2] : 0);
			}
		}
		$nawal++;
		$nabjad = $abjad_awal;
		$sheet->setCellValue($nabjad++ . $nawal, 'Grand Total');
		foreach ($tanggalx as $index2 => $value2) {
			$abjad_akhir = $nabjad;
			$sheet->setCellValue($nabjad++ . $nawal, $total[$index2]);
		}
		$sheet->getStyle('A1:' . ((@$abjad_akhir == "") ? 'A' : $abjad_akhir) . '2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F4B084');
		$sheet->getStyle($abjad_awal . $awal . ':' . ((@$abjad_akhir == "") ? 'A' : $abjad_akhir) . $nawal)->applyFromArray($styleArray_thin);
		$writer = new Xlsx($spreadsheet);
		$sheet->getStyle($abjad_awal . $nawal . ':' . ((@$abjad_akhir == "") ? 'A' : $abjad_akhir) . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('D9D9D9');
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENGIRIMAN " . @$barang . " BULAN " . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function monthly_sales_product_payment()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Produk Terbayar';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_sales_product_payment', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function preview_monthly_sales_product_payment(){
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$arrsales_member = $this->db->query("SELECT C.nama_produk, C.kode, SUM(A.account_detail_sales_product_allow) as 'total_barang', LEFT(A.date_created, 10) as tanggal, SUM(A.account_detail_sales_product_allow * D.sales_detail_price) as 'total_harga' , F.brand_name FROM t_account_detail_sales_product A JOIN produk B ON B.id_produk = A.product_id JOIN produk_global C ON C.kode = B.kd_pd JOIN t_sales_detail D ON D.sales_detail_id = A.sales_detail_id JOIN m_product_global_group E ON E.product_global_group_id = C.product_global_group_id JOIN m_brand F ON F.brand_id = E.brand_id WHERE LEFT(A.date_created, 7) = '$tahun-$bulan' AND E.brand_id IN(1,2,3) GROUP BY C.kode, LEFT(A.date_created, 10) ORDER BY LEFT(A.date_created, 10), brand_name ASC")->result_array();
		$arrreturn['bulanTahun'] = $arrbln[$bulan] . ' ' . $tahun;
		$produks = $this->db->get('produk')->result_array();
		$arrreturn['arrsales_member'] = [];
		foreach($arrsales_member as $index => $arr){
			$arrreturn['arrsales_member'][$arr['kode']] = $arr;
		}
		foreach($produks as $index => $produk){
			if(isset($arrreturn['arrsales_member'][$produk['kd_pd']])){
				$arrreturn['arrsales_member'][$produk['kd_pd']]['produks'][] = $produk;
			}
		}
		$data['content'] = $this->load->view($this->url_ . '/preview_monthly_sales_product_payment', $arrreturn);
	}

	public function monthly_sales_product()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Penjualan Produk';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_sales_product', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	public function monthly_sales_product_sales()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Penjualan Produk Per Sales';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$sales = $this->db->get_where('m_user', ['sales_category_id' => $_SESSION['sales_category_id']])->result_array();
		$datacontent['sales'] = "";
		foreach($sales as $sale){
			$datacontent['sales'] .= '<option value="'.$sale['user_id'].'">'.$sale['user_name'].'</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_sales_product_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function monthly_sales_product2()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Penjualan Produk';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_sales_product2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function daily_sales_product()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Penjualan Produk';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_daily_sales_product', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function monthly_sales_product_user()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Produk Per User';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$datacontent['arrpic'] = "";
		$arrpic = $this->db->query("SELECT * FROM m_user WHERE sales_category_id IS NOT NULL ORDER BY sales_category_id ASC")->result_array();
		foreach($arrpic as $pic){
			if($pic['sales_category_id'] == 1) $type = "Beauty";
			else if($pic['sales_category_id'] == 2) $type = "Men";
			else if($pic['sales_category_id'] == 3) $type = "MS Slim";
			else $type = "";
			$datacontent['arrpic'] .= '<option value="'.$pic['user_id'].'">'.$pic['user_name'].' - '.$type.'</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_sales_product_sales', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	public function monthly_sales()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Penjualan Seller';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_sales', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function monthly_sales_product_seller_pic()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Produk Per User';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_sales_product_picseller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}



	public function print_monthly_sales_product_sales()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$pic 	= $_POST['cPic'];

		$arrsales_member = $this->db->query("SELECT *  FROM v_report_sales_all WHERE LEFT(sales_date, 7) = '$tahun-$bulan' AND user_id = '$pic'")->result_array();
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$styleArray_thin_number = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			],
			'borders' => [
				'outline' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];




		$sheet->setCellValue('B2', 'LAPORAN PEMBELIAN BARANG PER PIC / SALES - ' . $arrbln[$bulan] . ' ' . $tahun);


		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'TANGGAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PIC A');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STATUS SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'HARGA SATUAN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'SUB TOTAL');


		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$irow = 1;
		$wrap_awal = "";
		$wrap_akhir = $abjad;
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = @$wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}

		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');


		$nawal = $awal;
		$jum_total = 0;
		foreach ($arrsales_member as $index => $value) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value['sales_date'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['user_name'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama'] . ' (' . $value['kode'] . ')');
			$sheet->setCellValue($nabjad++ . $nawal, $value['status'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, ($value['sales_detail_quantity']));
			$sheet->setCellValue($nabjad++ . $nawal, ($value['sales_detail_price']));
			$sheet->setCellValue($nabjad++ . $nawal, ($value['sales_detail_price'] * $value['sales_detail_quantity']));
			$jum_total += $value['sales_detail_quantity'];
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$nawal++;
		$sheet->setCellValue($nabjad++ . $nawal, 'TOTAL');
		$sheet->setCellValue($nabjad++ . $nawal, ($jum_total));
		$writer = new Xlsx($spreadsheet);

		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENJUALAN PER PIC / SALES -" . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function preview_monthly_sales_product_sales()
	{
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$pic 	= $_POST['cPic'];

		$arrreturn['arrsales_member'] = $this->db->query("SELECT *  FROM v_report_sales_all WHERE LEFT(sales_date, 7) = '$tahun-$bulan' AND user_id = '$pic'")->result_array();
		$data['content'] = $this->load->view($this->url_ . '/preview_monthly_sales_product_sales', $arrreturn);
	}

	public function print_monthly_sales()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$tipe 	= $_POST['cTipe'];
		$brand 	= $_POST['cBrand'];
		if(!empty($brand)){
			$arrsales_member = $this->db->query("SELECT A.nama, SUM(C.account_detail_credit) AS total FROM member A JOIN m_account B ON A.kode = B.seller_id JOIN t_account_detail C ON B.account_id = C.account_id AND SUBSTR(C.account_detail_date, 6, 2) = $bulan AND SUBSTR(C.account_detail_date, 1, 4) = $tahun AND C.coa_transaction_confirm = 1 WHERE A.status = '$tipe' AND A.sales_category_id = '$brand' GROUP BY B.account_id ORDER BY 2 DESC")->result_array();
		}else{
			$arrsales_member = $this->db->query("SELECT A.nama, SUM(C.account_detail_credit) AS total FROM member A JOIN m_account B ON A.kode = B.seller_id JOIN t_account_detail C ON B.account_id = C.account_id AND SUBSTR(C.account_detail_date, 6, 2) = $bulan AND SUBSTR(C.account_detail_date, 1, 4) = $tahun AND C.coa_transaction_confirm = 1 WHERE A.status = '$tipe' GROUP BY B.account_id ORDER BY 2 DESC")->result_array();
		}
		
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);

		$abjad = 'A';
		$awal2 = 1;
		$sheet->setCellValue($abjad . $awal2, "LAPORAN PENJUALAN " . $tipe . " - " . $arrbln[$bulan] . " " . $tahun);
		$sheet->mergeCells('A1:c1');
		$awal = 2;

		$abjad = 'A';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NO');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'JUMLAH');
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . $awal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$sheet->getStyle($abjad_awal . ($awal - 1) . ':' . $abjad . ($awal - 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFC000');
		$nawal = $awal;
		$i = 1;
		$total = 0;
		$xtanggal = "";
		$xno_rm = "";
		$xxabjad = 'C';

		$total = 0;
		foreach ($arrsales_member as $index1 => $value1) {
			$nabjad = $abjad_awal;
			$nawal++;

			$sheet->setCellValue($nabjad++ . $nawal, ++$index1);
			$sheet->setCellValue($nabjad++ . $nawal, $value1['nama']);
			$sheet->setCellValue($nabjad++ . $nawal, ($value1['total']));
			$total += $value1['total'];
		}
		$nawal++;
		$nabjad = $abjad_awal;
		$nabjad++;
		$sheet->setCellValue($nabjad++ . $nawal, 'Total');
		$sheet->setCellValue($xxabjad . $nawal, ($total));

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENJUALAN " . $tipe . " - " . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}
	public function preview_monthly_sales()
	{
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$tipe 	= $_POST['cTipe'];
		$brand 	= $_POST['cBrand'];
		$arrreturn['judul'] = $tipe . ' - ' . $arrbln[$bulan] . ' ' . $tahun;
		if (!empty($brand)) {
			$arrreturn['arrsales_member'] = $this->db->query("SELECT A.nama, SUM(C.account_detail_credit) AS total FROM member A JOIN m_account B ON A.kode = B.seller_id JOIN t_account_detail C ON B.account_id = C.account_id AND SUBSTR(C.account_detail_date, 6, 2) = $bulan AND SUBSTR(C.account_detail_date, 1, 4) = $tahun AND C.coa_transaction_confirm = 1 WHERE A.status = '$tipe' AND A.sales_category_id = '$brand' GROUP BY B.account_id ORDER BY 2 DESC")->result_array();
		}else{
			$arrreturn['arrsales_member'] = $this->db->query("SELECT A.nama, SUM(C.account_detail_credit) AS total FROM member A JOIN m_account B ON A.kode = B.seller_id JOIN t_account_detail C ON B.account_id = C.account_id AND SUBSTR(C.account_detail_date, 6, 2) = $bulan AND SUBSTR(C.account_detail_date, 1, 4) = $tahun AND C.coa_transaction_confirm = 1 WHERE A.status = '$tipe' GROUP BY B.account_id ORDER BY 2 DESC")->result_array();
		}
		
		$data['content'] = $this->load->view($this->url_ . '/preview_monthly_sales', $arrreturn);
	}

	public function print_monthlu_pic_sales()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan 		= $_POST['cBulan'];
		$tahun 		= $_POST['cTahun'];
		$pic 		= $_POST['cPic'];
		$seller 	= $_POST['seller_id'];

		$arrsales_member = $this->db->query("SELECT *  FROM v_report_sales_all WHERE LEFT(sales_date, 7) = '$tahun-$bulan' AND user_id = '$pic' AND seller_id = '$seller' ")->result_array();
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$styleArray_thin_number = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			],
			'borders' => [
				'outline' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];




		$sheet->setCellValue('B2', 'LAPORAN PEMBELIAN BARANG PER PIC / SALES - ' . $arrbln[$bulan] . ' ' . $tahun);


		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PIC');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STATUS SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'HARGA SATUAN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'SUB TOTAL');


		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$irow = 1;
		$wrap_awal = "";
		$wrap_akhir = $abjad;
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = @$wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}

		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');


		$nawal = $awal;
		$jum_total  = 0;
		$jum_harga	= 0;
		$jum_subtot	= 0;
		foreach ($arrsales_member as $index => $value) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value['user_name'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama'] . ' (' . $value['kode'] . ')');
			$sheet->setCellValue($nabjad++ . $nawal, $value['status'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, number_format(($value['sales_detail_quantity'])));
			$sheet->setCellValue($nabjad++ . $nawal, number_format(($value['sales_detail_price'])));
			$sheet->setCellValue($nabjad++ . $nawal, number_format(($value['sales_detail_price'] * $value['sales_detail_quantity'])));
			$jum_total 	+= $value['sales_detail_quantity'];
			$jum_harga 	+= $value['sales_detail_price'];
			$jum_subtot += $value['sales_detail_price'] * $value['sales_detail_quantity'];
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$nawal++;
		$sheet->setCellValue($nabjad++ . $nawal, '-');
		$sheet->setCellValue($nabjad++ . $nawal, '-');
		$sheet->setCellValue($nabjad++ . $nawal, '-');
		$sheet->setCellValue($nabjad++ . $nawal, 'TOTAL');
		$sheet->setCellValue($nabjad++ . $nawal, number_format(($jum_total)));
		$sheet->setCellValue($nabjad++ . $nawal, number_format(($jum_harga)));
		$sheet->setCellValue($nabjad++ . $nawal, number_format(($jum_subtot)));
		$writer = new Xlsx($spreadsheet);

		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PENJUALAN PER SALES - SELLER -" . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function print_monthly_sales_product()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		if(isset($_POST['product_id'])){
			$product_id = join("', '", $_POST['product_id']);
			$arrproduk = $this->db->query("SELECT nama_produk FROM produk WHERE id_produk IN ('$product_id')")->result_array();
			$arrsales_member = $this->db->query("SELECT SUM(B.sales_detail_quantity) as jumlah, C.nama, C.kode ,C.status, D.user_name, E.nama_produk, F.group_new , A.sales_date,B.sales_detail_price, (B.sales_detail_price * SUM( B.sales_detail_quantity )) AS kudubayar FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id LEFT JOIN member C ON C.kode = A.seller_id LEFT JOIN m_user D ON D.user_id = A.user_id LEFT JOIN produk E ON B.product_id = E.id_produk LEFT JOIN produk_global F ON E.kd_pd = F.kode WHERE LEFT(A.sales_date, 7) = '$tahun-$bulan' AND B.product_id IN ('$product_id') GROUP BY A.seller_id")->result_array();
		}else{
			$arrsales_member = $this->db->query("SELECT SUM(B.sales_detail_quantity) as jumlah, C.nama, C.kode ,C.status, D.user_name, F.nama_produk, F.group_new , A.sales_date,B.sales_detail_price, (B.sales_detail_price * SUM( B.sales_detail_quantity )) AS kudubayar FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id LEFT JOIN member C ON C.kode = A.seller_id LEFT JOIN m_user D ON D.user_id = A.user_id LEFT JOIN produk E ON B.product_id = E.id_produk LEFT JOIN produk_global F ON E.kd_pd = F.kode WHERE LEFT(A.sales_date, 7) = '$tahun-$bulan' GROUP BY A.seller_id")->result_array();
		}
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$styleArray_thin_number = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			],
			'borders' => [
				'outline' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];




		$sheet->setCellValue('B2', 'LAPORAN PEMBELIAN BARANG - ' . $arrbln[$bulan] . ' ' . $tahun);


		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'TANGGAL');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PIC');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'GROUP PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STATUS SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++. $awal, 'JUMLAH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++. $awal, 'HARGA');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'TOTAL HARGA');

		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$irow = 1;
		$wrap_awal = "";
		$wrap_akhir = $abjad;
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = @$wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}

		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');


		$nawal = $awal;
		$jum_total = 0;
		foreach ($arrsales_member as $index => $value) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, @$value['sales_date'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['user_name'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, @$value['group_new'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama'] . ' (' . $value['kode'] . ')');
			$sheet->setCellValue($nabjad++ . $nawal, $value['status'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['jumlah']);
			$sheet->setCellValue($nabjad++ . $nawal, @$value['sales_detail_price']);
			$sheet->setCellValue($nabjad++ . $nawal, @$value['kudubayar']);
			$jum_total += $value['jumlah'];
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$nawal++;
		$sheet->setCellValue($nabjad++ . $nawal, 'TOTAL');
		$sheet->setCellValue($nabjad++ . $nawal, ($jum_total));
		/*$jum = array();
		foreach($arrmember as $index_member => $value_member){
			
			$sheet->setCellValue($abjad++.$awal, strtoupper($value_member));
			$jum[$index_member] = 0;
		}
		$sheet->setCellValue($abjad.$awal, 'TOTAL TF HARIAN');
		$abjad_akhir = $abjad;
		for( $x = $abjad_awal; ; $x++) {			
				
			$sheet->getStyle($x.($awal).':'.$x.($awal))->applyFromArray($styleArray);
			if( $x == $abjad_akhir) break;
		}
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->applyFromArray($style);
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->getFont()->setSize(20);
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->getFont()->setBold( true );
		$sheet->mergeCells('B2:'.$abjad_akhir.'2');
		$nawal = $awal;
		$jum_tlg = array();
		foreach($arrtgl as $index_tgl => $value_tgl){
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++.$nawal, (int)substr($value_tgl, 8, 2)); 
			$nabjad2 = $nabjad;
			$jum_tgl[$index_tgl] = 0;
			foreach($arrmember as $index_member => $value_member){
				$jjum = (@$arrdata[$index_tgl][$index_member] == "")?'0':@$arrdata[$index_tgl][$index_member];
				$sheet->setCellValue($nabjad++.$nawal, number_format(($jjum)));
				$jum[$index_member] += $jjum;
				$jum_tgl[$index_tgl] += $jjum;
			}
			$sheet->setCellValue($nabjad.$nawal, number_format($jum_tgl[$index_tgl]));
			$sheet->getStyle($abjad_awal.$nawal.':'.$abjad_awal.$nawal)->applyFromArray($styleArray_thin);
			for( $x = $nabjad2; ; $x++) {
				$sheet->getStyle($x.$nawal.':'.$x.$nawal)->applyFromArray($styleArray_thin_number);
				if( $x == $nabjad) break;
			}
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$sheet->setCellValue($nabjad++.$nawal, 'TOTAL MUTASI');
		$jum_tgl['end'] = 0;
		foreach($arrmember as $index_member => $value_member){
			
			$sheet->setCellValue($nabjad++.$nawal, number_format(($jum[$index_member])));
			$jum_tgl['end'] += $jum[$index_member];
		}
		$sheet->setCellValue($nabjad++.$nawal, number_format($jum_tgl['end']));
		$sheet->getStyle($abjad_awal.$nawal.':'.$abjad_awal.$nawal)->applyFromArray($styleArray_thin);
		for( $x = $nabjad2; ; $x++) {
			$sheet->getStyle($x.$nawal.':'.$x.$nawal)->applyFromArray($styleArray_thin_number);
			if( $x == $nabjad) break;
		}*/
		$writer = new Xlsx($spreadsheet);

		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PEMBELIAN BARANG -" . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function print_monthly_sales_product_payment()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$arrsales_member = $this->db->query("SELECT C.nama_produk, C.kode, SUM(A.account_detail_sales_product_allow) as 'total_barang', LEFT(A.date_created, 10) as tanggal, SUM(A.account_detail_sales_product_allow * D.sales_detail_price) as 'total_harga' , F.brand_name FROM t_account_detail_sales_product A JOIN produk B ON B.id_produk = A.product_id JOIN produk_global C ON C.kode = B.kd_pd JOIN t_sales_detail D ON D.sales_detail_id = A.sales_detail_id JOIN m_product_global_group E ON E.product_global_group_id = C.product_global_group_id JOIN m_brand F ON F.brand_id = E.brand_id WHERE LEFT(A.date_created, 7) = '$tahun-$bulan' AND E.brand_id IN(1,2,3) GROUP BY C.kode, LEFT(A.date_created, 10) ORDER BY LEFT(A.date_created, 10), brand_name ASC")->result_array();
		
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$styleArray_thin_number = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			],
			'borders' => [
				'outline' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];




		$sheet->setCellValue('B2', 'LAPORAN PEMBAYARAN PRODUK - ' . $arrbln[$bulan] . ' ' . $tahun);


		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'TANGGAL PEMBAYARAN');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'BRAND');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SKU');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad. $awal, 'TOTAL PEMBAYARAN');
		

		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$irow = 1;
		$wrap_awal = "";
		$wrap_akhir = $abjad;
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = @$wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}

		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');


		$nawal = $awal;
		$jum_total = 0;
		foreach ($arrsales_member as $index => $value) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value['tanggal'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['brand_name'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['kode'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['total_barang'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['total_harga'] . '');
			$jum_total += $value['total_harga'];
			// $produks = $this->db->get_where('produk', ['kd_pd' => $value['kode']])->result_array();
			// foreach($produks as $produk){
			// 	$nabjad = $abjad_awal;
			// 	$nabjad++;
			// 	$sheet->setCellValue($nabjad++ . ++$nawal, $produk['nama_produk'] . '');
			// 	$sheet->setCellValue($nabjad++ . $nawal, $produk['kd_pd'] . '');
			// }
			// $nawal++;
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$nawal++;
		$sheet->setCellValue($nabjad++ . $nawal, 'TOTAL');
		$sheet->setCellValue($nabjad++ . $nawal, $jum_total);
		$writer = new Xlsx($spreadsheet);

		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PEMBELIAN BARANG -" . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function preview_monthly_sales_product()
	{
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		if($_POST['product_id'] != ""){
			$product_id = str_replace(",", "','", $_POST['product_id']);
			$arrproduk = $this->db->query("SELECT nama_produk FROM produk WHERE id_produk IN ('$product_id')")->result_array();
			$arrreturn['arrsales_member'] = $this->db->query("SELECT SUM(B.sales_detail_quantity) as jumlah, C.nama, C.kode ,C.status, D.user_name, E.nama_produk FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id LEFT JOIN member C ON C.kode = A.seller_id LEFT JOIN m_user D ON D.user_id = A.user_id LEFT JOIN produk E ON B.product_id = E.id_produk WHERE LEFT(A.sales_date, 7) = '$tahun-$bulan' AND B.product_id IN ('$product_id') GROUP BY A.seller_id")->result_array();
		}else{
			$arrreturn['arrsales_member'] = $this->db->query("SELECT SUM(B.sales_detail_quantity) as jumlah, C.nama, C.kode ,C.status, D.user_name, E.nama_produk FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id LEFT JOIN member C ON C.kode = A.seller_id LEFT JOIN m_user D ON D.user_id = A.user_id LEFT JOIN produk E ON B.product_id = E.id_produk WHERE LEFT(A.sales_date, 7) = '$tahun-$bulan' GROUP BY A.seller_id")->result_array();
		}
		$data['content'] = $this->load->view($this->url_ . '/preview_monthly_sales_product', $arrreturn);
		
	}

	public function print_monthly_sales_product_seller()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		if(isset($_POST['product_id'])){
			$product_id = join("', '", $_POST['product_id']);
			$arrproduk = $this->db->query("SELECT nama_produk FROM produk WHERE id_produk IN ('$product_id')")->result_array();
			$arrsales_member = $this->db->query("SELECT SUM(B.sales_detail_quantity) as jumlah, C.nama, C.kode ,C.status, D.user_name, E.nama_produk FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id LEFT JOIN member C ON C.kode = A.seller_id LEFT JOIN m_user D ON D.user_id = A.user_id LEFT JOIN produk E ON B.product_id = E.id_produk WHERE LEFT(A.sales_date, 7) = '$tahun-$bulan' AND D.user_id = ".$_POST['user_id']." AND B.product_id IN ('$product_id') GROUP BY A.seller_id")->result_array();
		}else{
			$arrsales_member = $this->db->query("SELECT SUM(B.sales_detail_quantity) as jumlah, C.nama, C.kode ,C.status, D.user_name, E.nama_produk FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id LEFT JOIN member C ON C.kode = A.seller_id LEFT JOIN m_user D ON D.user_id = A.user_id LEFT JOIN produk E ON B.product_id = E.id_produk WHERE LEFT(A.sales_date, 7) = '$tahun-$bulan' AND D.user_id = ".$_POST['user_id']." GROUP BY A.seller_id")->result_array();
		}
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$styleArray_thin_number = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			],
			'borders' => [
				'outline' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];




		$sheet->setCellValue('B2', 'LAPORAN PEMBELIAN BARANG - ' . $arrbln[$bulan] . ' ' . $tahun);


		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PIC');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'NAMA PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STATUS SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'JUMLAH');

		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$irow = 1;
		$wrap_awal = "";
		$wrap_akhir = $abjad;
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = @$wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}

		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');


		$nawal = $awal;
		$jum_total = 0;
		foreach ($arrsales_member as $index => $value) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value['user_name'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama'] . ' (' . $value['kode'] . ')');
			$sheet->setCellValue($nabjad++ . $nawal, $value['status'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, number_format(($value['jumlah'])));
			$jum_total += $value['jumlah'];
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$nawal++;
		$sheet->setCellValue($nabjad++ . $nawal, 'TOTAL');
		$sheet->setCellValue($nabjad++ . $nawal, number_format(($jum_total)));
		/*$jum = array();
		foreach($arrmember as $index_member => $value_member){
			
			$sheet->setCellValue($abjad++.$awal, strtoupper($value_member));
			$jum[$index_member] = 0;
		}
		$sheet->setCellValue($abjad.$awal, 'TOTAL TF HARIAN');
		$abjad_akhir = $abjad;
		for( $x = $abjad_awal; ; $x++) {			
				
			$sheet->getStyle($x.($awal).':'.$x.($awal))->applyFromArray($styleArray);
			if( $x == $abjad_akhir) break;
		}
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->applyFromArray($style);
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->getFont()->setSize(20);
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->getFont()->setBold( true );
		$sheet->mergeCells('B2:'.$abjad_akhir.'2');
		$nawal = $awal;
		$jum_tlg = array();
		foreach($arrtgl as $index_tgl => $value_tgl){
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++.$nawal, (int)substr($value_tgl, 8, 2)); 
			$nabjad2 = $nabjad;
			$jum_tgl[$index_tgl] = 0;
			foreach($arrmember as $index_member => $value_member){
				$jjum = (@$arrdata[$index_tgl][$index_member] == "")?'0':@$arrdata[$index_tgl][$index_member];
				$sheet->setCellValue($nabjad++.$nawal, number_format(($jjum)));
				$jum[$index_member] += $jjum;
				$jum_tgl[$index_tgl] += $jjum;
			}
			$sheet->setCellValue($nabjad.$nawal, number_format($jum_tgl[$index_tgl]));
			$sheet->getStyle($abjad_awal.$nawal.':'.$abjad_awal.$nawal)->applyFromArray($styleArray_thin);
			for( $x = $nabjad2; ; $x++) {
				$sheet->getStyle($x.$nawal.':'.$x.$nawal)->applyFromArray($styleArray_thin_number);
				if( $x == $nabjad) break;
			}
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$sheet->setCellValue($nabjad++.$nawal, 'TOTAL MUTASI');
		$jum_tgl['end'] = 0;
		foreach($arrmember as $index_member => $value_member){
			
			$sheet->setCellValue($nabjad++.$nawal, number_format(($jum[$index_member])));
			$jum_tgl['end'] += $jum[$index_member];
		}
		$sheet->setCellValue($nabjad++.$nawal, number_format($jum_tgl['end']));
		$sheet->getStyle($abjad_awal.$nawal.':'.$abjad_awal.$nawal)->applyFromArray($styleArray_thin);
		for( $x = $nabjad2; ; $x++) {
			$sheet->getStyle($x.$nawal.':'.$x.$nawal)->applyFromArray($styleArray_thin_number);
			if( $x == $nabjad) break;
		}*/
		$writer = new Xlsx($spreadsheet);

		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PEMBELIAN BARANG -" . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function print_monthly_sales_product2()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$product_id = $_POST['product_id'];
		echo $product_id;die();
		$arrproduk = $this->db->query("SELECT nama_produk FROM produk WHERE id_produk = '$product_id'")->row_array();
		// $arrsales_member = $this->db->query("SELECT SUM(B.sales_detail_quantity) as jumlah, C.nama, C.kode ,C.status, D.user_name FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id LEFT JOIN member C ON C.kode = A.seller_id LEFT JOIN m_user D ON D.user_id = A.user_id WHERE LEFT(A.sales_date, 7) = '$tahun-$bulan' AND B.product_id = '$product_id' GROUP BY A.seller_id")->result_array();
		$arrsales_member = $this->db->query("SELECT D.nama_produk, SUBSTR( A.account_detail_date, 1, 7 ) AS TGL, SUM(account_detail_sales_product_allow) FROM t_account_detail A JOIN t_account_detail_sales_product B ON B.account_detail_real_id = A.account_detail_real_id JOIN produk C ON C.id_produk = B.product_id JOIN produk_global D ON D.kode = C.kd_pd WHERE SUBSTR( A.account_detail_date, 1, 7 ) = '".$tahun."-".$bulan."' AND C.id_produk = '".$product_id."' GROUP BY D.kode,SUBSTR(A.account_detail_date,1,7)")->result_array();
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$styleArray_thin_number = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			],
			'borders' => [
				'outline' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];




		$sheet->setCellValue('B2', 'LAPORAN PEMBELIAN BARANG ' . $arrproduk['nama_produk'] . '- ' . $arrbln[$bulan] . ' ' . $tahun);


		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PIC');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'STATUS SELLER');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'JUMLAH');

		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$irow = 1;
		$wrap_awal = "";
		$wrap_akhir = $abjad;
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = @$wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}

		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');


		$nawal = $awal;
		$jum_total = 0;
		foreach ($arrsales_member as $index => $value) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value['user_name'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama'] . ' (' . $value['kode'] . ')');
			$sheet->setCellValue($nabjad++ . $nawal, $value['status'] . '');
			$sheet->setCellValue($nabjad++ . $nawal, number_format(($value['jumlah'])));
			$jum_total += $value['jumlah'];
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$nawal++;
		$sheet->setCellValue($nabjad++ . $nawal, 'TOTAL');
		$sheet->setCellValue($nabjad++ . $nawal, number_format(($jum_total)));
		/*$jum = array();
		foreach($arrmember as $index_member => $value_member){
			
			$sheet->setCellValue($abjad++.$awal, strtoupper($value_member));
			$jum[$index_member] = 0;
		}
		$sheet->setCellValue($abjad.$awal, 'TOTAL TF HARIAN');
		$abjad_akhir = $abjad;
		for( $x = $abjad_awal; ; $x++) {			
				
			$sheet->getStyle($x.($awal).':'.$x.($awal))->applyFromArray($styleArray);
			if( $x == $abjad_akhir) break;
		}
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->applyFromArray($style);
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->getFont()->setSize(20);
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->getFont()->setBold( true );
		$sheet->mergeCells('B2:'.$abjad_akhir.'2');
		$nawal = $awal;
		$jum_tlg = array();
		foreach($arrtgl as $index_tgl => $value_tgl){
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++.$nawal, (int)substr($value_tgl, 8, 2)); 
			$nabjad2 = $nabjad;
			$jum_tgl[$index_tgl] = 0;
			foreach($arrmember as $index_member => $value_member){
				$jjum = (@$arrdata[$index_tgl][$index_member] == "")?'0':@$arrdata[$index_tgl][$index_member];
				$sheet->setCellValue($nabjad++.$nawal, number_format(($jjum)));
				$jum[$index_member] += $jjum;
				$jum_tgl[$index_tgl] += $jjum;
			}
			$sheet->setCellValue($nabjad.$nawal, number_format($jum_tgl[$index_tgl]));
			$sheet->getStyle($abjad_awal.$nawal.':'.$abjad_awal.$nawal)->applyFromArray($styleArray_thin);
			for( $x = $nabjad2; ; $x++) {
				$sheet->getStyle($x.$nawal.':'.$x.$nawal)->applyFromArray($styleArray_thin_number);
				if( $x == $nabjad) break;
			}
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$sheet->setCellValue($nabjad++.$nawal, 'TOTAL MUTASI');
		$jum_tgl['end'] = 0;
		foreach($arrmember as $index_member => $value_member){
			
			$sheet->setCellValue($nabjad++.$nawal, number_format(($jum[$index_member])));
			$jum_tgl['end'] += $jum[$index_member];
		}
		$sheet->setCellValue($nabjad++.$nawal, number_format($jum_tgl['end']));
		$sheet->getStyle($abjad_awal.$nawal.':'.$abjad_awal.$nawal)->applyFromArray($styleArray_thin);
		for( $x = $nabjad2; ; $x++) {
			$sheet->getStyle($x.$nawal.':'.$x.$nawal)->applyFromArray($styleArray_thin_number);
			if( $x == $nabjad) break;
		}*/
		$writer = new Xlsx($spreadsheet);

		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN PEMBELIAN BARANG " . $arrproduk['nama_produk'] . "-" . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function monthly_sales_seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Penjualan Per Seller';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_sales_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_monthly_sales_seller()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$seller_id = $_POST['seller_id'];
		$arrmember = $this->db->query("SELECT nama, kode FROM member WHERE kode = '$seller_id'")->row_array();
		$arrsales_seller = $this->db->query("SELECT SUM(B.sales_detail_quantity) as jumlah, C.nama_produk , 	B.sales_detail_price, SUM(B.sales_detail_quantity*B.sales_detail_price) as TotalUang FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id LEFT JOIN produk C ON C.id_produk = B.product_id WHERE LEFT(A.sales_date, 7) = '$tahun-$bulan' AND A.seller_id = '$seller_id' GROUP BY B.product_id")->result_array();
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);





		$sheet->setCellValue('B2', 'LAPORAN BULANAN PEMBELIAN ' . $arrmember['nama'] . ' (' . $arrmember['kode'] . ') - ' . $arrbln[$bulan] . ' ' . $tahun);


		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'PRODUK');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'JUMLAH');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad++ . $awal, 'HARGA');
		$sheet->getColumnDimension($abjad)->setAutoSize(true);
		$sheet->setCellValue($abjad . $awal, 'TOTAL HARGA');

		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$irow = 1;
		$wrap_awal = "";
		$wrap_akhir = $abjad;
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = @$wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}

		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$nawal = $awal;
		$jum_total = 0;
		foreach ($arrsales_seller as $index => $value) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['jumlah']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['sales_detail_price']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['TotalUang']);
			$jum_total += $value['jumlah'];
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$nawal++;
		/*$sheet->setCellValue($nabjad++.$nawal, 'TOTAL');
		$sheet->setCellValue($nabjad++.$nawal, number_format(($jum_total)));*/
		/*$jum = array();
		foreach($arrmember as $index_member => $value_member){
			
			$sheet->setCellValue($abjad++.$awal, strtoupper($value_member));
			$jum[$index_member] = 0;
		}
		$sheet->setCellValue($abjad.$awal, 'TOTAL TF HARIAN');
		$abjad_akhir = $abjad;
		for( $x = $abjad_awal; ; $x++) {			
				
			$sheet->getStyle($x.($awal).':'.$x.($awal))->applyFromArray($styleArray);
			if( $x == $abjad_akhir) break;
		}
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal.($awal).':'.$abjad_akhir.($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->applyFromArray($style);
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->getFont()->setSize(20);
		$sheet->getStyle('B2:'.$abjad_akhir.'2')->getFont()->setBold( true );
		$sheet->mergeCells('B2:'.$abjad_akhir.'2');
		$nawal = $awal;
		$jum_tlg = array();
		foreach($arrtgl as $index_tgl => $value_tgl){
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++.$nawal, (int)substr($value_tgl, 8, 2)); 
			$nabjad2 = $nabjad;
			$jum_tgl[$index_tgl] = 0;
			foreach($arrmember as $index_member => $value_member){
				$jjum = (@$arrdata[$index_tgl][$index_member] == "")?'0':@$arrdata[$index_tgl][$index_member];
				$sheet->setCellValue($nabjad++.$nawal, number_format(($jjum)));
				$jum[$index_member] += $jjum;
				$jum_tgl[$index_tgl] += $jjum;
			}
			$sheet->setCellValue($nabjad.$nawal, number_format($jum_tgl[$index_tgl]));
			$sheet->getStyle($abjad_awal.$nawal.':'.$abjad_awal.$nawal)->applyFromArray($styleArray_thin);
			for( $x = $nabjad2; ; $x++) {
				$sheet->getStyle($x.$nawal.':'.$x.$nawal)->applyFromArray($styleArray_thin_number);
				if( $x == $nabjad) break;
			}
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$sheet->setCellValue($nabjad++.$nawal, 'TOTAL MUTASI');
		$jum_tgl['end'] = 0;
		foreach($arrmember as $index_member => $value_member){
			
			$sheet->setCellValue($nabjad++.$nawal, number_format(($jum[$index_member])));
			$jum_tgl['end'] += $jum[$index_member];
		}
		$sheet->setCellValue($nabjad++.$nawal, number_format($jum_tgl['end']));
		$sheet->getStyle($abjad_awal.$nawal.':'.$abjad_awal.$nawal)->applyFromArray($styleArray_thin);
		for( $x = $nabjad2; ; $x++) {
			$sheet->getStyle($x.$nawal.':'.$x.$nawal)->applyFromArray($styleArray_thin_number);
			if( $x == $nabjad) break;
		}*/
		$writer = new Xlsx($spreadsheet);

		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LAPORAN BULANAN PEMBELIAN " . $arrmember['nama'] . " (" . $arrmember['kode'] . ") - " . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function preview_monthly_sales_seller()
	{
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		$seller_id = $_POST['seller_id'];
		$arrreturn['arrmember'] = $this->db->query("SELECT nama, kode FROM member WHERE kode = '$seller_id'")->row_array();
		$arrreturn['bulanTahun'] = $arrbln[$bulan] . ' ' . $tahun;
		$arrreturn['arrsales_seller'] = $this->db->query("SELECT SUM(B.sales_detail_quantity) as jumlah, C.nama_produk FROM t_sales A LEFT JOIN t_sales_detail B ON B.sales_id = A.sales_id LEFT JOIN produk C ON C.id_produk = B.product_id WHERE LEFT(A.sales_date, 7) = '$tahun-$bulan' AND A.seller_id = '$seller_id' GROUP BY B.product_id")->result_array();
		$data['content'] = $this->load->view($this->url_ . '/preview_monthly_sales_seller', $arrreturn);
	}

	public function monthly_sales_paid()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan Barang Keluar';
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_monthly_sales_paid', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_monthly_sales_paid()
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);

		$bulan = $_POST['cBulan'];
		$tahun = $_POST['cTahun'];
		//'".$tahun."-".$bulan."'
		$arraccount_detail_date = $this->db->query("SELECT D.kode, D.nama, A.account_detail_date, SUM(A.account_detail_credit) as paid FROM t_account_detail A JOIN t_account_detail_sales B ON B.account_detail_real_id = A.account_detail_real_id JOIN t_sales C ON C.sales_id = B.sales_id JOIN member D ON D.kode = C.seller_id WHERE A.account_detail_user_id = '" . $_SESSION['user_id'] . "' AND LEFT(A.account_detail_date, 7) = '" . $tahun . "-" . $bulan . "' GROUP BY A.account_detail_date, C.seller_id")->result_array();
		$arrtgl = array();
		$arrmember = array();
		foreach ($arraccount_detail_date as $index => $value) {
			$arrtgl[$value['account_detail_date']] = $value['account_detail_date'];
			$arrmember[$value['kode']] = $value['nama'];
			$arrdata[$value['account_detail_date']][$value['kode']] = $value['paid'];
		}

		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		$styleArray_thin_number = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
			],
			'borders' => [
				'outline' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];
		/*array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
				)
			),
		);*/



		$sheet->setCellValue('B2', 'UPDATE MUTASI PERORANGAN - ' . $arrbln[$bulan] . ' ' . $tahun);


		$abjad = 'B';
		$awal = 4;

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'TANGGAL');
		$jum = array();
		foreach ($arrmember as $index_member => $value_member) {

			$sheet->setCellValue($abjad++ . $awal, strtoupper($value_member));
			$jum[$index_member] = 0;
		}
		$sheet->setCellValue($abjad . $awal, 'TOTAL TF HARIAN');
		$abjad_akhir = $abjad;
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . ($awal) . ':' . $x . ($awal))->applyFromArray($styleArray);
			if ($x == $abjad_akhir) break;
		}
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad_akhir . ($awal))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad_akhir . ($awal))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . ($awal) . ':' . $abjad_akhir . ($awal))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');

		$sheet->getStyle('B2:' . $abjad_akhir . '2')->applyFromArray($style);
		$sheet->getStyle('B2:' . $abjad_akhir . '2')->getFont()->setSize(20);
		$sheet->getStyle('B2:' . $abjad_akhir . '2')->getFont()->setBold(true);
		$sheet->mergeCells('B2:' . $abjad_akhir . '2');
		$nawal = $awal;
		$jum_tlg = array();
		foreach ($arrtgl as $index_tgl => $value_tgl) {
			$nabjad = $abjad_awal;
			$nawal++;
			$sheet->setCellValue($nabjad++ . $nawal, (int)substr($value_tgl, 8, 2));
			$nabjad2 = $nabjad;
			$jum_tgl[$index_tgl] = 0;
			foreach ($arrmember as $index_member => $value_member) {
				$jjum = (@$arrdata[$index_tgl][$index_member] == "") ? '0' : @$arrdata[$index_tgl][$index_member];
				$sheet->setCellValue($nabjad++ . $nawal, ($jjum));
				$jum[$index_member] += $jjum;
				$jum_tgl[$index_tgl] += $jjum;
			}
			$sheet->setCellValue($nabjad . $nawal, ($jum_tgl[$index_tgl]));
			$sheet->getStyle($abjad_awal . $nawal . ':' . $abjad_awal . $nawal)->applyFromArray($styleArray_thin);
			for ($x = $nabjad2;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin_number);
				if ($x == $nabjad) break;
			}
		}
		$nabjad = $abjad_awal;
		$nawal++;
		$sheet->setCellValue($nabjad++ . $nawal, 'TOTAL MUTASI');
		$jum_tgl['end'] = 0;
		foreach ($arrmember as $index_member => $value_member) {

			$sheet->setCellValue($nabjad++ . $nawal, ($jum[$index_member]));
			$jum_tgl['end'] += $jum[$index_member];
		}
		$sheet->setCellValue($nabjad++ . $nawal, ($jum_tgl['end']));
		$sheet->getStyle($abjad_awal . $nawal . ':' . $abjad_awal . $nawal)->applyFromArray($styleArray_thin);
		for ($x = $nabjad2;; $x++) {
			$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin_number);
			if ($x == $nabjad) break;
		}
		//$sheet->getStyle('E5:I1')->getAlignment()->setWrapText(true);
		/*$nawal = $awal;
		$i = 1;
		foreach($arrstock_produk_history as $index1 => $value1){
			$nabjad = $abjad_awal;
			$nawal++;
			$stockawal = ($value1['jumlah'] + ((@$datin_past[$value1['id_barang']]=="")?0:@$datin_past[$value1['id_barang']]) - ((@$datout_past[$value1['id_barang']]=="")?0:@$datout_past[$value1['id_barang']]));
			$sheet->setCellValue($nabjad++.$nawal, $i++);
			$sheet->setCellValue($nabjad++.$nawal, $value1['klasifikasi']);
			$sheet->setCellValue($nabjad++.$nawal, $value1['nama_produk']);
			$sheet->setCellValue($nabjad++.$nawal, $stockawal);
			$sheet->setCellValue($nabjad++.$nawal, @$datin_new[$value1['id_barang']]);
			$sheet->setCellValue($nabjad++.$nawal, @$datout_new[$value1['id_barang']]);
			$sheet->setCellValue($nabjad++.$nawal, ($stockawal + ((@$datin_new[$value1['id_barang']]=="")?0:@$datin_new[$value1['id_barang']]) - ((@$datout_new[$value1['id_barang']]=="")?0:$datout_new[$value1['id_barang']])));
			for( $x = $abjad_awal; ; $x++) {
				$sheet->getStyle($x.$nawal.':'.$x.$nawal)->applyFromArray($styleArray_thin);
				if( $x == $nabjad) break;
			}
		}*/
		/*$sheet->getStyle('A1:'.((@$abjad_akhir=="")?'A':$abjad_akhir).'2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F4B084');
		$sheet->getStyle($abjad_awal.$awal.':'.((@$abjad_akhir=="")?'A':$abjad_akhir).$nawal)->applyFromArray($styleArray_thin);
		$sheet->getStyle($abjad_awal.$nawal.':'.((@$abjad_akhir=="")?'A':$abjad_akhir).$nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('D9D9D9');*/
		$writer = new Xlsx($spreadsheet);

		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"LIST ORDER SELLER - " . $arrbln[$bulan] . " " . $tahun . ".xlsx\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function export_stock_monthly($bulan, $tahun)
	{
		ob_end_clean();
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		$spreadsheet = new Spreadsheet();
		//$sheet = $spreadsheet->getActiveSheet()->setShowGridLines(false); Mpdf
		$sheet = $spreadsheet->getActiveSheet();

		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$styleArray2 = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => '060606'),
				),
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				)
			),
		);
		$styleArray_thin = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => '060606'),
				),
			),
		);
		$style = array(
			'alignment' => array(
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			)
		);
		//$sheet->getStyle('C4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('f8cbad');
		$arrdata = $this->model->Query("SELECT *, C.jumlah, C.rusak FROM kemasan A LEFT JOIN produk B ON B.id_produk = A.produk LEFT JOIN tb_stock_kemasan_history C ON C.id_barang = A.id_kemasan AND C.tanggal = '" . $tahun . "-" . $bulan . "-01' LEFT JOIN m_category_product D ON D.category_product_id = B.produk_kategori ORDER BY B.produk_kategori, A.produk");
		$arrcategoryproduct = $this->model->Query("SELECT * FROM m_category_product");
		$arrterima = $this->model->Query("SELECT SUM(jumlah) as jumlah, SUM(rusak) as rusak, tgl_terima, id_barang FROM terima_kemasan  GROUP BY id_barang, tgl_terima");
		foreach ($arrterima as $keyterima => $valueterima) {
			$dataterima[$valueterima['id_barang']][$valueterima['tgl_terima']] = $valueterima;
		}
		$arrterima_print = $this->model->Query("SELECT SUM(jumlah) as jumlah, SUM(rusak) as rusak, tgl_terima, id_barang FROM terima_print  GROUP BY id_barang, tgl_terima");
		foreach ($arrterima_print as $keyterima_print => $valueterima_print) {
			$dataterima_print[$valueterima_print['id_barang']][$valueterima_print['tgl_terima']] = $valueterima_print;
		}
		$arrkeluar = $this->model->Query("SELECT SUM(jumlah) as jumlah, tanggal, id_barang FROM tb_detail_kluar_kemasan GROUP BY id_barang, tanggal");
		foreach ($arrkeluar as $keykeluar => $valuekeluar) {
			$datakeluar[$valuekeluar['id_barang']][$valuekeluar['tanggal']] = $valuekeluar;
		}

		$abjad = 'B';
		$awal = 5;
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));
		$sheet->mergeCells($abjad . $awal . ':' . $abjad++ . ($awal + 1));

		$abjad = 'B';
		$abjad_awal = $abjad;
		$sheet->setCellValue($abjad++ . $awal, 'NAMA BARANG');
		$sheet->setCellValue($abjad++ . $awal, 'KLASIFIKASI');
		$sheet->setCellValue($abjad++ . $awal, 'STOK AWAL');
		$sheet->setCellValue($abjad++ . $awal, 'STOK BARANG RUSAK AWAL');

		$nawal = $awal;

		$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		for ($i = 1; $i <= $nJumlahHari; $i++) {
			if ($i > 9) {
				$cNol = "";
			} else {
				$cNol = "0";
			}
			$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
			$no = 0;
			$titik_awal = $abjad;

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG DATANG');
			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG KELUAR');
			$sheet->getStyle($titik_awal . $awal . ':' . $abjad . $awal)->applyFromArray($styleArray);
			$sheet->mergeCells($titik_awal . $awal . ':' . $abjad . $awal);
			$sheet->setCellValue($titik_awal . $awal, substr($arrbln[$bulan], 0, 3) . '-' . (int)($cNol . $i));

			$sheet->setCellValue($abjad++ . ($awal + 1), 'BARANG RUSAK');
		}

		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG DATANG');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad++ . $awal, 'TOTAL BARANG KELUAR');
		$sheet->mergeCells($abjad . $awal . ':' . $abjad . ($awal + 1));
		$sheet->setCellValue($abjad . $awal, 'SISA STOCK KEMASAN BAGUS');

		$sheet->mergeCells($abjad_awal . ($awal - 3) . ':' . $abjad . ($awal - 3));
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 2))->getFont()->setBold(true);
		$sheet->getStyle($abjad_awal . ($awal - 3) . ':' . $abjad_awal . ($awal - 3))->getFont()->setSize(28);
		$sheet->setCellValue($abjad_awal . ($awal - 3), 'LAPORAN STOK BAHAN KEMAS MS GLOW');
		$sheet->getStyle($abjad_awal . ($awal - 2) . ':' . $abjad_awal . ($awal - 2))->getFont()->setSize(20);
		$sheet->mergeCells($abjad_awal . ($awal - 2) . ':' . $abjad . ($awal - 2));
		$sheet->setCellValue($abjad_awal . ($awal - 2), 'PERIODE ' . strtoupper($arrbln[$bulan]));
		$sheet->setCellValue($abjad . ($awal - 1), date('d/m/Y H.i'));


		//$sheet->getStyle('A1:B2')->applyFromArray($styleArray);
		//$sheet ->getStyle($abjad_awal.$awal.':'.$abjad.($awal+1))->applyFromArray($styleArray);
		$sheet->freezePane('F7');
		$sheet->getSheetView()->setZoomScale(70);
		$sheet->getRowDimension($awal + 1)->setRowHeight(30);
		$irow = 1;
		$wrap_awal = "";
		for ($x = $abjad_awal;; $x++) {

			$sheet->getStyle($x . $awal . ':' . $x . ($awal + 1))->applyFromArray($styleArray);
			if ($irow <= 2) {
				$sheet->getColumnDimension($x)->setAutoSize(true);
			} else {
				if ($wrap_awal == "") $wrap_awal = $x;
				$wrap_akhir_before = $wrap_akhir;
				$wrap_akhir = $x;
			}
			if ($x == $abjad) break;
			$irow++;
		}
		$sheet->getStyle($wrap_awal . '5:' . $wrap_akhir . '6')->getAlignment()->setWrapText(true);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->applyFromArray($style);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFont()->setSize(12);
		$sheet->getStyle($abjad_awal . $awal . ':' . $abjad . ($awal + 1))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('acb9ca');
		$nawal++;
		$produk_global = "";
		$produk_merge_akhir = "";

		foreach ($arrdata as $key => $value) {
			$nabjad = 'B';
			$paling_awal = $nabjad;
			$nawal++;
			if ($produk_merge_awal == "") {
				$produk_merge_awal = $nawal;
			}

			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_kemasan']);
			//$xx = 1;
			if ($rgb == "") $rgb = 'e9f7df';
			if ($produk_global != $value['nama_produk']) {
				//$xx = 0;
				$xabjad = $nabjad;
				if ($produk_merge_akhir != "") {

					$sheet->mergeCells($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_akhir);
					$sheet->getStyle($nabjad . $produk_merge_awal . ':' . $nabjad . $produk_merge_awal)->applyFromArray($style);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray);
					$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
					//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
				}
				$produk_merge_awal = $nawal;
			}
			$produk_global = $value['nama_produk'];
			$sheet->setCellValue($nabjad++ . $nawal, $value['nama_produk']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['jumlah']);
			$sheet->setCellValue($nabjad++ . $nawal, $value['rusak']);
			$jumlah = $value['jumlah'];
			$nJumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$jmldatang = 0;
			$jmlkeluar = 0;
			for ($i = 1; $i <= $nJumlahHari; $i++) {
				if ($i > 9) {
					$cNol = "";
				} else {
					$cNol = "0";
				}
				$date = $tahun . "-" . $bulan . "-" . $cNol . $i;
				$no = 0;

				$sheet->setCellValue($nabjad++ . $nawal, ($dataterima[$value['id_kemasan']][$date]['jumlah'] != "" || $dataterima_print[$value['id_kemasan']][$date]['jumlah'] != "") ? ((($dataterima[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_kemasan']][$date]['jumlah']) + (($dataterima_print[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima_print[$value['id_kemasan']][$date]['jumlah'])) : "");
				$sheet->setCellValue($nabjad++ . $nawal, $datakeluar[$value['id_kemasan']][$date]['jumlah']);
				$sheet->setCellValue($nabjad++ . $nawal, $dataterima[$value['id_kemasan']][$date]['rusak']);
				$jmldatang = $jmldatang + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima[$value['id_kemasan']][$date]['jumlah']) + (($dataterima_print[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $dataterima_print[$value['id_kemasan']][$date]['jumlah']);
				$jmlkeluar = $jmlkeluar + (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "") ? 0 : $datakeluar[$value['id_kemasan']][$date]['jumlah']);
				//$jumlah = $jumlah + (($dataterima[$value['id_kemasan']][$date]['jumlah'] == "")?0:$dataterima[$value['id_kemasan']][$date]['jumlah']) - (($datakeluar[$value['id_kemasan']][$date]['jumlah'] == "")?0:$datakeluar[$value['id_kemasan']][$date]['jumlah']); 
			}
			$jumlah = $jumlah + $jmldatang - $jmlkeluar;
			$sheet->setCellValue($nabjad++ . $nawal, $jmldatang);
			$sheet->setCellValue($nabjad++ . $nawal, $jmlkeluar);
			$sheet->setCellValue($nabjad . $nawal, $jumlah);
			$produk_merge_akhir = $nawal;
			for ($x = $abjad_awal;; $x++) {
				$sheet->getStyle($x . $nawal . ':' . $x . $nawal)->applyFromArray($styleArray_thin);
				if ($x == $nabjad) break;
			}
			$rgb = $value['category_product_rgb'];
			if ($rgb == "") $rgb = 'e9f7df';
			if ($jumlah < 20000) {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
			} else {
				$sheet->getStyle($nabjad . $nawal . ':' . $nabjad . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
			}
		}
		//if($xx == 1){
		$sheet->mergeCells($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_akhir);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir . $produk_merge_akhir)->applyFromArray($styleArray2);
		$sheet->getStyle($xabjad . $produk_merge_awal . ':' . $xabjad . $produk_merge_awal)->applyFromArray($style);
		$sheet->getStyle($paling_awal . $produk_merge_awal . ':' . $wrap_akhir_before . $produk_merge_akhir)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rgb);
		//$sheet->getStyle($paling_awal.$produk_merge_awal.':'.$paling_awal.$produk_merge_awal)->applyFromArray($style);
		//}

		$nawal++;
		$nawal++;
		$sheet->setCellValue($abjad_awal . $nawal++, 'NB:');
		$abjad_awal2 = $abjad_awal;
		$sheet->getStyle($abjad_awal2 . $nawal . ':' . $abjad_awal2++ . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ed7291');
		$sheet->setCellValue($abjad_awal2 . $nawal++, 'STOK DIBAWAH 20.000 PCS');
		foreach ($arrcategoryproduct as $keyproduct => $valueproduct) {
			$abjad_awal2 = $abjad_awal;
			$sheet->getStyle($abjad_awal2 . $nawal . ':' . $abjad_awal2++ . $nawal)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($valueproduct['category_product_rgb']);
			$sheet->setCellValue($abjad_awal2 . $nawal++, $valueproduct['category_product_name']);
		}

		$writer = new Xlsx($spreadsheet);
		header("Content-Type: application/vnd.ms-excel charset=utf-8");
		header("Content-Disposition: attachment; filename=\"STOCK KEMASAN MSGLOW " . strtoupper($arrbln[$bulan]) . ".xls\"");
		header("Cache-Control: max-age=0");
		$writer->save('php://output');
	}

	public function laporan_total_akun()
	{
		$arrbln = array(
			'01' => '01',
			'02' => '02',
			'03' => '03',
			'04' => '04',
			'05' => '05',
			'06' => '06',
			'07' => '07',
			'08' => '08',
			'09' => '09',
			'10' => '10',
			'11' => '11',
			'12' => '12'
		);
		$tahun = '2021';
		$arrdata = $this->db->query("SELECT A.id as id1, A.nama as nama1, A.kode as kode1, B.id as id2, B.nama as nama2, B.kode as kode2, C.id as id3, C.nama as nama3, C.kode as kode3, D.coa_total_debit as debit4, D.coa_total_credit as credit4, E.coa_total_debit as debit3, E.coa_total_credit as credit3 FROM coa_1 A JOIN coa_2 B ON B.coa1_id = A.id JOIN coa_3 C ON C.coa2_id = B.id LEFT JOIN (SELECT SUM(Y.coa_total_debit) as coa_total_debit, SUM(Y.coa_total_credit) as coa_total_credit, Z.coa3_id FROM coa_4 Z JOIN t_coa_total Y ON Y.coa_id = Z.id AND Y.coa_level = 4 GROUP BY Z.coa3_id) D ON D.coa3_id = C.id LEFT JOIN t_coa_total E ON E.coa_id = C.id AND E.coa_level = 3")->result_array();
		$arrtotal_history4 = $this->db->query("SELECT SUBSTR(coa_total_date, 6, 2) as coa_total_date, coa3_id, SUM(coa_total_debit) as coa_total_debit, SUM(coa_total_credit) as coa_total_credit FROM t_coa_total_history A JOIN coa_4 B ON B.id = A.coa_id WHERE SUBSTR(coa_total_date, 1, 4) = '$tahun' AND coa_level = 4 GROUP BY coa3_id, coa_total_date")->result_array();
		foreach ($arrtotal_history4 as $index2 => $value2) {
			$arrhistory4[$value2['coa3_id']][$value2['coa_total_date']] = $value2;
		}
		$arrtotal_history3 = $this->db->query("SELECT SUBSTR(coa_total_date, 6, 2) as coa_total_date, coa_id, coa_total_debit, coa_total_credit FROM t_coa_total_history WHERE SUBSTR(coa_total_date, 1, 4) = '2021' AND coa_level = 3")->result_array();
		foreach ($arrtotal_history3 as $index3 => $value3) {
			$arrhistory3[$value3['coa3_id']][$value3['coa_total_date']] = $value3;
		}
		foreach ($arrdata as $index => $value) {
			if (@$arrdata2[$value['id1']]['id1'] == "") {
				$data1['id1'] = $value['id1'];
				$data1['nama1'] = $value['nama1'];
				$data1['kode1'] = $value['kode1'];
				$arrdata2[$value['id1']] = $data1;
			}
			if (@$arrdata2[$value['id1']]['coa2'][$value['id2']]['id2'] == "") {
				$data2['id2'] = $value['id2'];
				$data2['nama2'] = $value['nama2'];
				$data2['kode2'] = $value['kode2'];
				$arrdata2[$value['id1']]['coa2'][$value['id2']] = $data2;
			}
			if (@$arrdata2[$value['id1']]['coa2'][$value['id2']]['coa3'][$value['id3']]['id3'] == "") {
				$data3['id3'] = $value['id3'];
				$data3['nama3'] = $value['nama3'];
				$data3['kode3'] = $value['kode3'];
				$data3['debit3'] = $value['debit3'];
				$data3['credit3'] = $value['credit3'];
				$data3['debit4'] = $value['debit4'];
				$data3['credit4'] = $value['credit4'];
				$datahistory4 = array();
				foreach ($arrbln as $indexx => $valuex) {
					$datahistory4[$indexx] = @$arrhistory4[@$data3['id3']][@$indexx];
				}
				$data3['history4'] = $datahistory4;
				$datahistory3 = array();
				foreach ($arrbln as $indexx => $valuex) {
					$datahistory3[$indexx] = @$arrhistory3[@$data3['id3']][@$indexx];
				}
				$data3['history3'] = $datahistory3;
				$arrdata2[$value['id1']]['coa2'][$value['id2']]['coa3'][$value['id3']] = $data3;
			}
		}

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Total Akun';
		$datacontent['akuns'] = $arrdata2;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/laporan_total_akun', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
}
