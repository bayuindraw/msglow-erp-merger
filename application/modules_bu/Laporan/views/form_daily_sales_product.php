<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN PENJUALAN PRODUK HARIAN
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/print_daily_all'); ?>" method="Post" target="_blank">
          <div class="form-group">
            <label>Pilih Tanggal</label>
            <input type="text" id="cTanggal" name="cTanggal" class="md-form-control md-static floating-label 4IDE-date date_picker form-control" value="<?= $this->session->userdata('tanggal') ?>" placeholder="Pilih Tanggal Penjualan">
            <input type="hidden" id="cIdOutlet" name="NamaOutlet" class="md-form-control md-static floating-label" value="1">
          </div>
          <div class="form-group">
            <label>Produk</label>
            <select name="product_id" class="PilihBarang form-control md-static" required>
              <option></option><?= $product_list ?>
            </select>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>


</div>