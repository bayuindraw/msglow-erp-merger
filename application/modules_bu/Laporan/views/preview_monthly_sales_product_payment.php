<?php
if (count($arrsales_member) > 0) {
?>
    <div class="center">
        <table>
            <thead>
                <tr>
                    <td rowspan="2" colspan="7">
                        <h5> LAPORAN PEMBAYARAN PRODUK - <?= $bulanTahun ?> </h5>
                    </td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <br />
    <?php
    $x = 1; ?>
        <table class="table table-striped table-bordered text-center" id='DataTable_preview_tb1'>
            <thead>
                <tr>
                    <td style="background-color:#BAB86C"> TANGGAL PEMBAYARAN </td>
                    <td style="background-color:#BAB86C"> BRAND </td>
                    <td style="background-color:#BAB86C"> SKU </td>
                    <td style="background-color:#BAB86C"> NAMA PRODUK </td>
                    <td style="background-color:#BAB86C"> JUMLAH PRODUK </td>
                    <td style="background-color:#BAB86C"> TOTAL PEMBAYARAN </td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($arrsales_member as $index => $value) {
                ?>
                    <tr onclick="showDetail('<?= $index ?>');">
                        <td> <?= $value['tanggal'] ?> </td>
                        <td> <?= $value['brand_name'] ?> </td>
                        <td> <?= $value['kode'] ?> </td>
                        <td> <?= $value['nama_produk'] ?> </td>
                        <td> <?= number_format($value['total_barang']) ?> </td>
                        <td> <?= number_format($value['total_harga'])?> </td>
                    </tr>
                    <?php foreach($value['produks'] as $produk) {?>
                        <tr style="display: none;" class="<?= $index ?>">
                            <td></td>
                            <td></td>
                            <td> <?= $produk['kd_pd'] ?> </td>
                            <td> <?= $produk['nama_produk'] ?> </td>
                            <td></td>
                            <td></td>
                        <tr>
                    <?php } ?>
                <?php
                }
                ?>
            </tbody>
        </table>
        <br />
        <br />
    <?php
} else {
    ?>
    <table class="table table-striped table-bordered text-center" id='DataTable_preview_tb2'>
        <thead>
                <tr>
                    <td rowspan="2" colspan="7" style="background-color:#8CD3FF;"> LAPORAN PEMBAYARAN PRODUK - <?= $bulanTahun ?> </td>
                </tr>

        </thead>
        <tbody>
        </tbody>
    </table>
    <br />
    <table class="table table-striped table-bordered text-center" id='DataTable_preview_tb3'>
        <thead>
            <tr>
                <td colspan="2" style="background-color:#FFE4C4"> DATA NOT FOUND</td>
            </tr>
            <tr>
                <td style="background-color:#BAB86C"> TANGGAL PEMBAYARAN </td>
                <td style="background-color:#BAB86C"> BRAND </td>
                <td style="background-color:#BAB86C"> SKU </td>
                <td style="background-color:#BAB86C"> NAMA PRODUK </td>
                <td style="background-color:#BAB86C"> JUMLAH PRODUK </td>
                <td style="background-color:#BAB86C"> TOTAL PEMBAYARAN </td>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
<?php
}
?>


<script type="text/javascript">
    function showDetail(kode){
        if($('.'+kode).attr('style') == 'display: none;'){
            $('.'+kode).show()
        }else{
            $('.'+kode).hide()
        }
    }

    $("#DataTable_preview_tb1").dataTable({
        scrollY: '50vh',
        scrollX: 'true',
        scrollCollapse: true,
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data per halaman",
            "sSearch": "Pencarian: ",
            "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
            "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
            "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
            "sInfoFiltered": "(di filter dari _MAX_ total data)",
            "oPaginate": {
                "sFirst": "Awal",
                "sLast": "Akhir",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya"
            }
        }
    });

    $("#DataTable_preview_tb2").dataTable({
        scrollY: '50vh',
        scrollX: 'true',
        scrollCollapse: true,
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data per halaman",
            "sSearch": "Pencarian: ",
            "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
            "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
            "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
            "sInfoFiltered": "(di filter dari _MAX_ total data)",
            "oPaginate": {
                "sFirst": "Awal",
                "sLast": "Akhir",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya"
            }
        }
    });

    $("#DataTable_preview_tb3").dataTable({
        scrollY: '50vh',
        scrollX: 'true',
        scrollCollapse: true,
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data per halaman",
            "sSearch": "Pencarian: ",
            "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
            "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
            "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
            "sInfoFiltered": "(di filter dari _MAX_ total data)",
            "oPaginate": {
                "sFirst": "Awal",
                "sLast": "Akhir",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya"
            }
        }
    });
</script>