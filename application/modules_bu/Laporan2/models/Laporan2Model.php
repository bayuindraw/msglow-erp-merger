<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Laporan2Model extends CI_Model
{
	
	var $table_ = "t_sales";
	var $id_ = "sales_id";
	var $eng_ = "sales";
	var $url_ = "Laporan2";
	
	function get_data_list_boleh_kirim_vs_kirim(){
		$arrjoin[] = "LEFT JOIN (SELECT SUM(connected_quantity + pending_quantity) as boleh_kirim, kd_pd FROM t_sales_detail ZA JOIN produk ZB ON ZB.id_produk = ZA.product_id GROUP BY ZB.kd_pd) Z ON Z.kd_pd = A.kode";
		$arrjoin[] = "LEFT JOIN (SELECT SUM(package_detail_quantity) as kirim, kd_pd FROM t_package_trial_detail YA JOIN produk YB ON YB.id_produk = YA.product_id GROUP BY YB.kd_pd) Y ON Y.kd_pd = A.kode";
		$table = "produk_global A";
		$id = 'A.id';
		//$arrgroup[] = "d.kode";
		//$arrwhere[] = "b.pending_quantity > 0";
		//if ($_SESSION['role_id'] == 12 || $_SESSION['role_id'] == 7) $arrwhere[] = "d.brand_id  = '" . $_SESSION['sales_category_id'] . "'";
		$arrorder[] = "kirim - boleh_kirim DESC";
		$field = array('A.kode', 'A.nama_produk', "IFNULL(Z.boleh_kirim, 0) boleh_kirim", "IFNULL(Y.kirim, 0) kirim");
		$rfield = array('kode', 'nama_produk', "boleh_kirim", "kirim");
		$searchField = array('A.kode', 'A.nama_produk');
		$url = $this->url_;
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($searchField as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			if($value2['column'] != ""){
				$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
			}
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT jum as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT jum as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {

			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($rfield as $keyfield) {
				if ($keyfield == 'kode' || $keyfield == 'nama_produk') {
					$datax[] = '<a href="' . site_url($url . "/list_boleh_kirim_vs_kirim_seller/" . $valuer['kode']) . '">' . $valuer[$keyfield] . '</a>';
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			// $datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}
	
	function get_data_list_boleh_kirim_vs_kirim_seller($id){
		$arrjoin[] = "LEFT JOIN (SELECT SUM(connected_quantity + pending_quantity) as boleh_kirim, ZB.kd_pd, ZC.seller_id FROM t_sales_detail ZA JOIN produk ZB ON ZB.id_produk = ZA.product_id JOIN t_sales ZC ON ZC.sales_id = ZA.sales_id WHERE ZB.kd_pd = '$id' GROUP BY ZC.seller_id) Z ON Z.seller_id = A.kode";
		$arrjoin[] = "LEFT JOIN (SELECT SUM(package_detail_quantity) as kirim, YB.kd_pd, YC.member_code FROM t_package_trial_detail YA JOIN produk YB ON YB.id_produk = YA.product_id JOIN t_package_trial YC ON YC.package_id = YA.package_id WHERE YB.kd_pd = '$id' GROUP BY YC.member_code) Y ON Y.member_code = A.kode";
		$arrjoin[] = "LEFT JOIN m_user B ON B.user_id = A.user_pic_id";
		$table = "member A";
		$id = 'A.id_member';
		//$arrgroup[] = "d.kode";
		$arrwhere[] = "(Y.member_code IS NOT NULL OR Z.seller_id IS NOT NULL)";
		//if ($_SESSION['role_id'] == 12 || $_SESSION['role_id'] == 7) $arrwhere[] = "d.brand_id  = '" . $_SESSION['sales_category_id'] . "'";
		$arrorder[] = "kirim - boleh_kirim DESC";
		$field = array('A.kode', 'A.nama', 'B.user_fullname', "IFNULL(Z.boleh_kirim, 0) boleh_kirim", "IFNULL(Y.kirim, 0) kirim");
		$rfield = array('kode', 'nama', 'user_fullname', "boleh_kirim", "kirim");
		$searchField = array('A.kode', 'A.nama', 'B.user_fullname');
		$url = $this->url_;
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($searchField as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			if($value2['column'] != ""){
				$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
			}
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT jum as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT jum as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $order $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {

			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($rfield as $keyfield) {
				
					$datax[] = $valuer[$keyfield];
				
			}
			// $datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}
	
	function get_produk($id="")
	{
		$data = $this->db->get('produk');
		return $data->result_array();
	}
	
	function get_product_type()
	{
		$data = $this->db->get('m_product_type');
		return $data->result_array();
	}
	
	function get_factory()
	{
		$data = $this->db->get('factory');
		return $data->result_array();
	}

	function get_data_list_pendingan()
	{
		$arrjoin[] = "JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd";
		$table = "t_sales B";
		$id = 'B.sales_id';
		$arrgroup[] = "E.kode";
		$arrwhere[] = "C.pending_quantity > 0";
		$arrorder[] = "";
		$field = array('E.kode', 'E.nama_produk', "FORMAT(SUM(C.sales_detail_quantity), 0) as 'Jumlah Pesanan'", "FORMAT((SUM(C.pending_quantity) + SUM(C.connected_quantity)), 0) as 'Boleh Dikirim'", "FORMAT(SUM(C.pending_quantity), 0) as 'Pending'", "FORMAT(SUM(C.connected_quantity), 0) as 'Terkirim'");
		$rfield = array('kode', 'nama_produk', "Jumlah Pesanan", "Boleh Dikirim", "Pending", "Terkirim");
		$searchField = array('E.kode', 'E.nama_produk');
		$url = $this->url_;
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($searchField as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();

		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {

			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($rfield as $keyfield) {
				if ($keyfield == 'kode' || $keyfield == 'nama_produk') {
					$datax[] = '<a href="' . site_url($url . "/detail_list_pendingan/" . $valuer['kode']) . '">' . $valuer[$keyfield] . '</a>';
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			// $datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_draft_total_pembagian()
	{
		$arrjoin[] = "JOIN produk B ON B.id_produk = A.product_id";
		$table = "t_package_trial_detail_draft A";
		$id = 'A.package_detail_id';
		$arrgroup[] = "A.product_id";
		$arrwhere[] = "A.package_detail_quantity <> 0";
		$arrorder[] = "";
		$field = array('B.nama_produk', 'SUM(A.package_detail_quantity) as jum_tot');
		$rfield = array('nama_produk', 'jum_tot');
		$searchField = array('B.nama_produk');
		$url = $this->url_;
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($searchField as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();

		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {

			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($rfield as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			// $datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	function get_data_realisasi_total_pembagian($tgl_awal, $tgl_akhir)
	{
		$arrjoin[] = "JOIN t_package_trial_detail B ON B.package_id = A.package_id JOIN produk C ON C.id_produk = B.product_id";
		$table = "t_package_trial A";
		$id = 'A.package_id';
		$arrgroup[] = "B.product_id, A.package_date";
		$arrwhere[] = "REPLACE(A.package_date, '-', '') BETWEEN '" . $tgl_awal . "' AND '" . $tgl_akhir . "'";
		$arrorder[] = "A.package_date";
		$field = array('A.package_date', 'C.nama_produk', 'SUM(B.package_detail_quantity) as jum_tot');
		$rfield = array('package_date', 'nama_produk', 'jum_tot');
		$searchField = array('C.nama_produk');
		$url = $this->url_;
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$group = "";
		if (@$arrgroup != "") {
			foreach ($arrgroup as $gkey => $gvalue) {
				$arrgroup2[] = $gvalue;
			}
			$group = "GROUP BY " . join(', ', $arrgroup2);
		}
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($searchField as $key => $value) {
				if ($value != 'COUNT(package_detail_id)') {
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		foreach (@$_GET['order'] as $key2 => $value2) {
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where2 $group) K")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT(jum) as jum FROM (SELECT COUNT($id) as jum FROM $table $join $where $group) K")->row_array();

		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $group $limit")->result_array();
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {

			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($rfield as $keyfield) {
				$datax[] = $valuer[$keyfield];
			}
			// $datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
	}

	public function Code($Query) {
		$Query = $this->db->query("  ".$Query."  ");
		return $Query->result_array();	
	}

}
