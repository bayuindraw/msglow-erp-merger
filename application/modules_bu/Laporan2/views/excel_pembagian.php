<?php
date_default_timezone_set('Asia/Jakarta');
$tgl_sekarang = date("Y-F-d");
$bulan_cetak = date("F");

// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=report_realisasi_total_pembagian" . $tgl_sekarang . ".xls");
?>
<table>
    <thead>
        <tr>
            <td rowspan="2" colspan="11" style="background-color:#8CD3FF; text-align: center;font-weight: bold;"> LAPORAN <?= strtoupper($type) ?> BARANG KELUAR PT. KOSMETIKA CANTIK INDONESIA </td>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<br />

<?php
$count = isset($arrdata) ? count($arrdata) : 0;
?>

<?php if ($count > 0) { ?>
    <?php
    $total_data = count($arrdata);
    $total_row = ceil($total_data / 4);

    $row_ke = 1;
    $data_index_from = 0;
    $data_index_to = 4;

    for ($i = 0; $i < $total_row; $i++) {
    ?>
        <table>
            <thead>
                <tr>
                    <?php
                    $no = 0;
                    foreach ($arrdata as $index => $value) {
                        if ($no >= $data_index_from && $no < $data_index_to) { ?>
                            <td colspan="2" style="background-color:#FFE4C4; text-align: center;padding:5px;border: 1px solid black;font-weight: bold;"> <?= $arrproduk[$index] ?></td>
                            <td style="width: 30px;"></td>
                    <?php }
                        $no++;
                    } ?>
                </tr>
                <tr>
                    <?php
                    $no = 0;
                    foreach ($arrdata as $index => $value) {
                        if ($no >= $data_index_from && $no < $data_index_to) { ?>
                            <td style="background-color:#BAB86C;width: 200px;padding:5px;border: 1px solid black;font-weight: bold;text-align: center;">NAMA</td>
                            <td style="background-color:#BAB86C;padding:5px;border: 1px solid black;font-weight: bold;text-align: center;">PEMBAGIAN</td>
                            <td style="width: 30px;"></td>
                    <?php }
                        $no++;
                    } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $max = 0;
                $no = 0;
                foreach ($arrdata as $key => $value) {
                    if ($no >= $data_index_from && $no < $data_index_to) {
                        $count = 0;
                        foreach ($value as $key2 => $value2) {
                            $count++;
                        }
                        if ($count > $max) {
                            $max = $count;
                        }
                    }
                    $no++;
                }

                for ($a = 0; $a < $max; $a++) { ?>
                    <tr>
                        <?php
                        $no = 0;
                        foreach ($arrdata as $index => $value) {
                            if ($no >= $data_index_from && $no < $data_index_to) { ?>
                                <td style="padding:5px;border: 1px solid black;"> <?= isset($value[$a]['nama']) ? $value[$a]['nama'] : null ?> </td>
                                <td style="padding:5px;border: 1px solid black;text-align:right;"> <?= isset($value[$a]['package_detail_quantity']) ? $value[$a]['package_detail_quantity'] : null; ?> </td>
                                <td style="width: 30px;"></td>
                        <?php }
                            $no++;
                        } ?>
                    </tr>
                <?php } ?>

                <tr>
                    <?php
                    $no = 0;
                    foreach ($arrdata as $index => $value) {
                        if ($no >= $data_index_from && $no < $data_index_to) {
                            $total = 0;
                            foreach ($value as $key2 => $value2) {
                                $total += $value2['package_detail_quantity'];
                            }
                    ?>
                            <td style="background-color:#BAB86C;width: 200px;padding:5px;border: 1px solid black;font-weight: bold;text-align: center;">TOTAL</td>
                            <td style="background-color:#BAB86C;width: 200px;padding:5px;border: 1px solid black;text-align:right;font-weight: bold;"><?= $total; ?></td>
                            <td style="width: 30px;"></td>
                    <?php }
                        $no++;
                    } ?>
                </tr>

            </tbody>
        </table>
    <?php
        $data_index_from += 4;
        $data_index_to += 4;
        echo '<br><br>';
    } ?>
    <br />
    <br />
<?php } else { ?>
    <table border="1">
        <thead>
            <tr>
                <td style="background-color:#BAB86C"> NAMA </td>
                <td style="background-color:#BAB86C"> PEMBAGIAN</td>
            </tr>
            <tr>
                <td colspan="2" style="background-color:#FFE4C4"> DATA NOT FOUND</td>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
<?php } ?>