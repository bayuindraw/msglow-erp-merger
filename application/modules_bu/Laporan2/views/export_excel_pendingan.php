<?php
date_default_timezone_set('Asia/Jakarta');
$tgl_sekarang = date("Y-F-d");
$bulan_cetak = date("F");
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data_pendingan_" . $tgl_sekarang . ".xls");

// Tambahkan table
?>

<!--begin: Datatable -->
<table border="1" style="width: 100%; border:black;">
    <thead>
        <tr>
            <td colspan="7" style="background-color:#8CD3FF;">REKAP PENDINGAN PT. KOSMETIKA CANTIK INDONESIA</td>
        </tr>
        <tr>
            <td colspan="7" style="background-color:#8CD3FF"><?= $tgl_sekarang ?></td>
        </tr>
        <tr>
            <td style="background-color:#BAB86C">No</td>
            <td style="background-color:#BAB86C">Kode</td>
            <td style="background-color:#BAB86C">Nama Produk</td>
            <td style="background-color:#BAB86C">Bali</td>
            <td style="background-color:#BAB86C">Distributor</td>
            <td style="background-color:#BAB86C">Seller Malang</td>
            <td style="background-color:#BAB86C">Total Pending</td>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0;
        $total_vert_bali = 0;
        $total_vert_mlg = 0;
        $total_vert_distributor = 0;
        $cTot_pend = 0;
        foreach ($data_list_pending as $vaData) {
            $pending_bali = 0;
            $pending_mlg = 0;
            $pending_distributor = 0;
        ?>
            <tr>
                <td><?= ++$no ?></td>
                <td> <?= $vaData['kode'] ?> </td>
                <td> <?= $vaData['nama_produk'] ?> </td>
                <td>
                    <?php
                    if ($vaData['team'] == "BALI") {
                        echo number_format($vaData['Pending'], 0, '.', '.');
                        $pending_bali = $vaData['Pending'];
                        $total_vert_bali = $total_vert_bali + $vaData['Pending'];
                        $vert_bali = $total_vert_bali;
                    } else {
                        $pending_bali = 0;
                        $total_vert_bali = 0;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($vaData['status'] == "DISTRIBUTOR") {
                        echo number_format($vaData['Pending'], 0, '.', '.');
                        $pending_distributor = $vaData['Pending'];
                        $total_vert_distributor = $total_vert_distributor + $vaData['Pending'];
                        $vert_distributor = $total_vert_distributor;
                    } else {
                        $pending_distributor = 0;
                        $total_vert_distributor = 0;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($vaData['team'] == "MLG") {
                        echo number_format($vaData['Pending'], 0, '.', '.');
                        $pending_mlg = $vaData['Pending'];
                        $total_vert_mlg = $total_vert_mlg + $vaData['Pending'];
                        $vert_mlg = $total_vert_mlg;
                    } else {
                        $pending_mlg = 0;
                        $total_vert_mlg = 0;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    $total_pending = $pending_bali + $pending_mlg + $pending_distributor;
                    $cTot_pend = $cTot_pend + $total_pending;
                    $sum_vert_pend = $cTot_pend;
                    echo number_format($total_pending, 0, '.', '.');
                    ?>
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>
<table border="1">
    <thead>
        <tr>
            <td colspan="3" align="center"><b>TOTAL</b></td>
            <td>
                <?php
                echo number_format($vert_bali, 0, '.', '.');
                ?>
            </td>
            <td>
                <?php
                echo number_format($vert_distributor, 0, '.', '.');
                ?>
            </td>
            <td>
                <?php
                echo number_format($vert_mlg, 0, '.', '.');
                ?>
            </td>
            <td>
                <?php
                echo number_format($sum_vert_pend, 0, '.', '.');
                ?>
            </td>
        </tr>
    </thead>
</table>
<!--end: Datatable -->
<br />
<br />
<br />
<!--begin: Datatable -->
<table border="1" style="width: 100%; border:black;">
    <thead>
        <tr>
            <td colspan="4" style="background-color:#9E9E9E;">REKAP PENDINGAN MSGLOW FOR MEN</td>
        </tr>
        <tr>
            <td colspan="4" style="background-color:#9E9E9E"><?= $tgl_sekarang ?></td>
        </tr>
        <tr>
            <td style="background-color:#BAB86C">No</td>
            <td style="background-color:#BAB86C">Kode</td>
            <td style="background-color:#BAB86C">Nama Produk</td>
            <td style="background-color:#BAB86C">Total Pending</td>
        </tr>
    </thead>
    <tbody>
        <?php $no_men = 0;
        $total_formen = 0;
        foreach ($data_list_pending_formen as $vaDataMen) {
        ?>
            <tr>
                <td><?= ++$no_men ?></td>
                <td> <?= $vaDataMen['kode'] ?> </td>
                <td> <?= $vaDataMen['nama_produk'] ?> </td>
                <td>
                    <?php
                    echo number_format($vaDataMen['Pending'], 0, ',', ',');
                    $total_formen = $total_formen + $vaDataMen['Pending'];
                    $sum_vert_formen = $total_formen;
                    ?>
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>
<table border="1">
    <thead>
        <tr>
            <td colspan="3" align="center"><b>TOTAL</b></td>
            <td>
                <?php
                echo number_format($sum_vert_formen, 0, '.', '.');
                ?>
            </td>
        </tr>
    </thead>
</table>
<!--end: Datatable -->
<br />
<br />
<br />
<!--begin: Datatable -->
<table border="1" style="width: 100%; border:black;">
    <thead>
        <tr>
            <td colspan="5" style="background-color:#9E9E9E;">REKAP PENDINGAN KLINIK</td>
        </tr>
        <tr>
            <td colspan="5" style="background-color:#9E9E9E"><?= $tgl_sekarang ?></td>
        </tr>
        <tr>
            <td style="background-color:#BAB86C">No</td>
            <td style="background-color:#BAB86C">Klinik</td>
            <td style="background-color:#BAB86C">Kode</td>
            <td style="background-color:#BAB86C">Nama Produk</td>
            <td style="background-color:#BAB86C">Total Pending</td>
        </tr>
    </thead>
    <tbody>
        <?php $no_klinik = 0;
        $total_klinik = 0;
        foreach ($data_list_pending_klinik as $vaDataKlinik) {
        ?>
            <tr>
                <td><?= ++$no_klinik ?></td>
                <td> <?= $vaDataKlinik['kode_tempat'] ?> </td>
                <td> <?= $vaDataKlinik['kode'] ?> </td>
                <td> <?= $vaDataKlinik['nama_produk'] ?> </td>
                <td>
                    <?php
                    echo number_format($vaDataKlinik['Pending'], 0, '.', '.');
                    $total_klinik = $total_klinik + $vaDataKlinik['Pending'];
                    $sum_vert_klinik = $total_klinik;
                    ?>
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>
<table border="1">
    <thead>
        <tr>
            <td colspan="4" align="center"><b>TOTAL</b></td>
            <td>
                <?php
                echo number_format($sum_vert_klinik, 0, '.', '.');
                ?>
            </td>
        </tr>
    </thead>
</table>
<!--end: Datatable -->