<?php
date_default_timezone_set('Asia/Jakarta');
$tgl_sekarang = date("Y-m-d");
$bulan_cetak = date("F");
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=Laporan_Terima_Barang_Bulanan_" . $tgl_sekarang . ".xls");

// Tambahkan table
$nama_sup = "";
foreach ($nama_supplier as $sup) {
    $nama_sup = $sup['nama_factory'];
}
?>
<!--begin: Datatable -->
<table>
    <thead>
        <tr>
            <td colspan="4" style="background-color:#8CD3FF;"><?= strtoupper($title) ?></td>
        </tr>
        <tr>
            <td style="background-color:#8CD3FF"> Bulan </td>
            <td style="background-color:#8CD3FF"> : </td>
            <td colspan="2" style="background-color:#8CD3FF"> <?= $tanggal ?></td>
        </tr>
        <tr>
            <td style="background-color:#8CD3FF"> Supplier </td>
            <td style="background-color:#8CD3FF"> : </td>
            <td colspan="2" style="background-color:#8CD3FF"> <?= $nama_sup ?></td>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<br />
<!--end: Datatable -->

<?php
if ($cek_data->num_rows() > 0) {
?>
    <!--begin: Datatable -->
    <table border="1" style="width: 100%; border:black;">
        <thead>
            <tr>
                <td style="background-color:#BAB86C">Tanggal</td>
                <td style="background-color:#BAB86C">SJ</td>
                <td style="background-color:#BAB86C">Nama Produk</td>
                <td style="background-color:#BAB86C">Total Terima</td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($data_terima_barang as $row) {
            ?>
                <tr>
                    <td> <?= $row['tgl_terima'] ?> </td>
                    <td> <?= $row['nomor_surat_jalan'] ?> </td>
                    <td> <?= $row['nama_produk'] ?> </td>
                    <td> <?= $row['jumlah'] ?> </td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <!--end: Datatable -->
<?php
} else {
?>
    <table border="1" style="width: 100%; border:black;">
        <thead>
            <tr>
                <td style="background-color:#BAB86C">Tanggal</td>
                <td style="background-color:#BAB86C">SJ</td>
                <td style="background-color:#BAB86C">Nama Produk</td>
                <td style="background-color:#BAB86C">Total Terima</td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($data_terima_barang as $row) {
            ?>
                <td colspan="4"> DATA NOT FOUND </td>
            <?php
            }
            ?>
        </tbody>
    </table>
<?php
}
?>