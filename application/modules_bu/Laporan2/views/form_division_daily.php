<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK <?= strtoupper($title) ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">
        <form id="main" class="form-horizontal" action="<?= site_url($url . '/division_daily_to_excel'); ?>" method="post" target="_blank">
          <div class="form-group">
            <label>Klasifkasi</label>
            <input type="hidden" id="url" value="<?= $url ?>">
            <select id="product_type" name="product_type" class="PilihKlasifikasi form-control md-static">
              <option></option><?= $product_type_list ?>
            </select>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <label>Tanggal Awal</label>
              <input type="text" id="tgl_awal" name="tgl_awal" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required>
            </div>
            <div class="col-sm-6">
              <label>Tanggal Akhir</label>
              <input type="text" id="tgl_akhir" name="tgl_akhir" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required>
            </div>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <div class="row">
                <div class="col-6 col-md-6 col-lg-6 text-left">
                  <button type="button" onclick="preview_draft()" class="btn btn-primary waves-effect waves-light " name="draft" value="draft" title="Cetak Laporan">
                    <span class="m-l-10">Preview Laporan Draft</span>
                  </button>
                  &nbsp;
                  <button type="button" onclick="preview_realisasi()" class="btn btn-warning waves-effect waves-light " name="realisasi" value="realisasi" title="Cetak Laporan">
                    <span class="m-l-10">Preview Laporan Realisasi</span>
                  </button>
                </div>
                <div class="col-6 col-md-6 col-lg-6 text-right">
                  <button type="submit" class="btn btn-primary waves-effect waves-light " name="draft" value="draft" title="Cetak Laporan">
                    <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan Draft</span>
                  </button>
                  &nbsp;
                  <button type="submit" class="btn btn-warning waves-effect waves-light " name="realisasi" value="realisasi" title="Cetak Laporan">
                    <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan Realisasi</span>
                  </button>
                </div>
              </div>
              <br />
              <br />
              <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                  <div id="preview_datatable"></div>
                </div>
              </div>

            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  function preview_draft() {
    // var cTanggal = $('#cTanggal').val();
    var product_type = document.getElementById('product_type');
    var p_type = product_type.value;
    if (p_type === "") {
      var value_p = "all";
    } else {
      var value_p = p_type;
    }
    // var url = $('#url').val();
    // if (cTanggal == "") {
    //   alert('Pilih Tanggal terlebih dahulu!');
    // } 
    // if (product_type == "") {
    //   alert('Pilih Klasifikasi terlebih dahulu!');
    // } else {
    $.ajax({
      type: "POST",
      // data: "cTanggal=" + cTanggal +
      //   "&product_type=" + product_type,
      data: "draft=draft" +
        "&product_type=" + value_p,
      url: "<?= site_url($url . '/preview_division_daily') ?>",
      cache: false,
      beforeSend: function() {
        $('#preview_datatable').html("Cek Data Ke Sistem .. ");
      },
      success: function(msg) {
        $("#preview_datatable").html(msg);
      }
    });
    // }
  }

  function preview_realisasi() {
    // var cTanggal = $('#cTanggal').val();
    var product_type = document.getElementById('product_type');
    var p_type = product_type.value;
    if (p_type === "") {
      var value_p = "all";
    } else {
      var value_p = p_type;
    }
    date = $('#tanggal').val()
    var tgl_awal = $('#tgl_awal').val();
    var tgl_akhir = $('#tgl_akhir').val();
    // if (cTanggal == "") {
    //   alert('Pilih Tanggal terlebih dahulu!');
    // } 
    // if (product_type == "") {
    //   alert('Pilih Klasifikasi terlebih dahulu!');
    // } else {
    $.ajax({
      type: "POST",
      // data: "cTanggal=" + cTanggal +
      //   "&product_type=" + product_type,
      data: "draft=realisasi" +
        "&product_type=" + value_p +
        "&tgl_awal=" + tgl_awal +
        "&tgl_akhir=" + tgl_akhir,
      url: "<?= site_url($url . '/preview_division_daily') ?>",
      cache: false,
      beforeSend: function() {
        $('#preview_datatable').html("Cek Data Ke Sistem .. ");
      },
      success: function(msg) {
        $("#preview_datatable").html(msg);
      }
    });
    // }
  }

  function search_date() {
    var product_type = $('#product_type option:selected').val();
    if (product_type == "") {
      var value_p = "all";
    } else {
      var value_p = product_type;
    }
    var tgl_awal = $('#tgl_awal').val();
    var tgl_akhir = $('#tgl_akhir').val();

    $.ajax({
      type: "POST",
      data: "draft=realisasi" +
        "&product_type=" + value_p +
        "&tgl_awal=" + tgl_awal +
        "&tgl_akhir=" + tgl_akhir,
      url: "<?= site_url('Laporan2/preview_division_daily') ?>",
      cache: false,
      beforeSend: function() {
        $('#preview_datatable').html("Cek Data Ke Sistem .. ");
      },
      success: function(msg) {
        $("#preview_datatable").html(msg);
      }
    });
  }
</script>