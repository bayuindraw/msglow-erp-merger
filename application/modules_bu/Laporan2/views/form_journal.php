<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN JURNAL
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/print_journal'); ?>" method="Post" target="_blank">
          <div class="form-group">
            <label>Tanggal</label>
            <div class="input-daterange input-group col-6" id="kt_datepicker_5">
              <input type="text" class="form-control" name="from" />
              <div class="input-group-append">
                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
              </div>
              <input type="text" class="form-control" name="to" />
            </div>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>


</div>