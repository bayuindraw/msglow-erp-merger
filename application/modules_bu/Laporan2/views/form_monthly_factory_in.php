<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK <?= strtoupper($title) ?>
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/print_monthly_factory_in'); ?>" method="Post" target="_blank">
          <div class="form-group">
            <label>Bulan</label>
            <select name="cBulan" id="pilihBulan" ng-model="cBulan" class="form-control" required>
              <option></option>
              <option value="01">Januari</option>
              <option value="02">Februari</option>
              <option value="03">Maret</option>
              <option value="04">April</option>
              <option value="05">Mei</option>
              <option value="06">Juni</option>
              <option value="07">Juli</option>
              <option value="08">Agustus</option>
              <option value="09">September</option>
              <option value="10">Oktober</option>
              <option value="11">November</option>
              <option value="12">Desember</option>
            </select>
          </div>
          <div class="form-group">
            <label>Tahun</label>
            <select name="cTahun" id="pilihTahun" class="form-control" required>
              <option></option>
              <option value="2019">2019</option>
              <option value="2020">2020</option>
              <option value="2021">2021</option>
            </select>
          </div>
          <div class="form-group">
            <label>Pabrik</label>
            <select name="factory_id" id="pilihPabrik" class="form-control md-static" required>
              <option></option><?= $factory_list ?>
            </select>
          </div>
          <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
              <div id="preview_datatable"></div>
            </div>
            <div class="kt-portlet__foot">
              <div class="kt-form__actions">
                <button type="button" onclick="preview_data()" class="btn btn-warning" title="Preview Data">
                  <span class="m-l-10"> Preview Data</span>
                </button>
                <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                  <i class="flaticon2-print"></i>
                  <span class="m-l-10"> Cetak Laporan</span>
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  function preview_data() {
    var pilih_bln = $('#pilihBulan').val();
    var pilih_thn = $('#pilihTahun').val();
    var pilih_pabrik = $('#pilihPabrik').val();

    if (pilih_bln == "") {
      alert('Pilih Bulan terlebih dahulu!');
    } else if (pilih_thn == "") {
      alert('Pilih Tahun terlebih dahulu!');
    } else if (pilih_pabrik == "") {
      alert('Pilih Pabrik terlebih dahulu!');
    } else {
      $.ajax({
        type: "POST",
        data: "cBulan=" + pilih_bln +
          "&cTahun=" + pilih_thn +
          "&factory_id=" + pilih_pabrik,
        url: "<?= site_url($url . '/preview_monthly_factory_in') ?>",
        cache: false,
        beforeSend: function() {
          $('#preview_datatable').html("Cek Data Ke Sistem .. ");
        },
        success: function(msg) {
          $("#preview_datatable").html(msg);
        }
      });
    }

  }
</script>