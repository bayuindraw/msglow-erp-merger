<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        CETAK <?= strtoupper($title) ?>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form id="main" class="form-horizontal" action="<?= site_url($url . '/pembagian_outstanding_to_excel'); ?>" method="post" target="_blank">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Tanggal Awal</label>
                                <input type="text" id="tgl_awal" name="tgl_awal" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required>
                            </div>
                            <div class="col-sm-6">
                                <label>Tanggal Akhir</label>
                                <input type="text" id="tgl_akhir" name="tgl_akhir" class="form-control md-static floating-label 4IDE-date date_picker" value="<?= date('Y-m-d') ?>" required>
                            </div>
                        </div>
                        <div class="row pt-4">
                            <div class="col-6 col-md-6 col-lg-6 text-left">
                                <button type="button" onclick="preview_draft()" class="btn btn-primary waves-effect waves-light " name="draft" value="draft" title="Cetak Laporan">
                                    <span class="m-l-10">Preview Laporan Draft</span>
                                </button>
                                &nbsp;
                                <button type="button" onclick="preview_realisasi()" class="btn btn-warning waves-effect waves-light " name="realisasi" value="realisasi" title="Cetak Laporan">
                                    <span class="m-l-10">Preview Laporan Realisasi</span>
                                </button>
                            </div>
                            <div class="col-6 col-md-6 col-lg-6 text-right">
                                <button type="submit" class="btn btn-primary waves-effect waves-light" name="draft" value="draft" title="Cetak Laporan">
                                    <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan Draft</span>
                                </button>
                                &nbsp;
                                <button type="submit" class="btn btn-warning waves-effect waves-light " name="realisasi" value="realisasi" title="Cetak Laporan">
                                    <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan Realisasi</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 id="report_name" class="kt-portlet__head-title">
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <div id="spinner" class="kt-spinner kt-spinner--v2 kt-spinner--lg kt-spinner--warning pt-5 pb-5"></div>
                    </div>
                    <div class="col-12">
                        <table id="table_draft_total_pembagian" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Produk</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                        <table id="table_realisasi_total_pembagian" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Produk</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#spinner').hide();
        preview_draft()
    });

    function preview_draft() {
        $('#report_name').text("PREVIEW LAPORAN DRAFT TOTAL PEMBAGIAN")
        $('#spinner').show();
        $('#table_realisasi_total_pembagian').DataTable().destroy();
        $('#table_realisasi_total_pembagian').hide();
        $('#table_draft_total_pembagian').show();
        $('#table_draft_total_pembagian').DataTable({
            "pagingType": "full_numbers",
            scrollY: "300px",
            scrollX: true,
            scrollCollapse: true,
            "processing": true,
            "serverSide": true,
            "bDestroy": true,
            "ajax": "<?= site_url('Laporan2/datatable_draft_total_pembagian') ?>",
        }, $('#spinner').hide());
    }

    function preview_realisasi() {
        $('#report_name').text("PREVIEW LAPORAN REALISASI TOTAL PEMBAGIAN")
        $('#spinner').show();
        $('#table_draft_total_pembagian').DataTable().destroy();
        $('#table_draft_total_pembagian').hide();
        $('#table_realisasi_total_pembagian').show();
        $('#table_realisasi_total_pembagian').DataTable({
            "pagingType": "full_numbers",
            scrollY: "300px",
            scrollX: true,
            scrollCollapse: true,
            "processing": true,
            "serverSide": true,
            "bDestroy": true,
            "ajax": {
                "type": "GET",
                "url": "<?= site_url('Laporan2/datatable_realisasi_total_pembagian') ?>",
                "data": {
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                },
            }
        }, $('#spinner').hide());
    }
</script>