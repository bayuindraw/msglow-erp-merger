<?php
date_default_timezone_set('Asia/Jakarta');
$tgl_sekarang = date("Y-m-d");
$bulan_cetak = date("F");

// Tambahkan table
$nama_sup = "";
foreach ($nama_supplier as $sup) {
    $nama_sup = $sup['nama_factory'];
}
?>
<!--begin: Datatable -->
<table>
    <thead>
        <tr>
            <td colspan="4" style="background-color:#8CD3FF;"><?= strtoupper($title) ?></td>
        </tr>
        <tr>
            <td style="background-color:#8CD3FF"> Bulan </td>
            <td style="background-color:#8CD3FF"> : </td>
            <td colspan="2" style="background-color:#8CD3FF"> <?= $tanggal ?></td>
        </tr>
        <tr>
            <td style="background-color:#8CD3FF"> Supplier </td>
            <td style="background-color:#8CD3FF"> : </td>
            <td colspan="2" style="background-color:#8CD3FF"> <?= $nama_sup ?></td>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<br />
<!--end: Datatable -->

<?php
if ($cek_data->num_rows() > 0) {
?>
    <!--begin: Datatable -->
    <table class="table table-striped table-bordered text-center" id='DataTable_preview_tm1'>
        <thead>
            <tr>
                <td style="background-color:#BAB86C">Tanggal</td>
                <td style="background-color:#BAB86C">SJ</td>
                <td style="background-color:#BAB86C">Nama Produk</td>
                <td style="background-color:#BAB86C">Total Terima</td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($data_terima_barang as $row) {
            ?>
                <tr>
                    <td> <?= $row['tgl_terima'] ?> </td>
                    <td> <?= $row['nomor_surat_jalan'] ?> </td>
                    <td> <?= $row['nama_produk'] ?> </td>
                    <td> <?= $row['jumlah'] ?> </td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

    <!--end: Datatable -->
<?php
} else {
?>
    <table class="table table-striped table-bordered text-center" id='DataTable_preview_tm2'>
        <thead>
            <tr>
                <td style="background-color:#BAB86C">Tanggal</td>
                <td style="background-color:#BAB86C">SJ</td>
                <td style="background-color:#BAB86C">Nama Produk</td>
                <td style="background-color:#BAB86C">Total Terima</td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($data_terima_barang as $row) {
            ?>
                <td colspan="4"> DATA NOT FOUND </td>
            <?php
            }
            ?>
        </tbody>
    </table>
<?php
}
?>

<script type="text/javascript">
    $("#DataTable_preview_tm1").dataTable({
        scrollY: '50vh',
        scrollX: 'true',
        scrollCollapse: true,
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data per halaman",
            "sSearch": "Pencarian: ",
            "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
            "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
            "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
            "sInfoFiltered": "(di filter dari _MAX_ total data)",
            "oPaginate": {
                "sFirst": "Awal",
                "sLast": "Akhir",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya"
            }
        }
    });

    $("#DataTable_preview_tm2").dataTable({
        scrollY: '50vh',
        scrollX: 'true',
        scrollCollapse: true,
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data per halaman",
            "sSearch": "Pencarian: ",
            "sZeroRecords": "Maaf, tidak ada data yang ditemukan",
            "sInfo": "Menampilkan _START_ s/d _END_ dari _TOTAL_ data",
            "sInfoEmpty": "Menampilkan 0 s/d 0 dari 0 data",
            "sInfoFiltered": "(di filter dari _MAX_ total data)",
            "oPaginate": {
                "sFirst": "Awal",
                "sLast": "Akhir",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya"
            }
        }
    });
</script>