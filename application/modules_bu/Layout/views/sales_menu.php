<?php
$perubahan_verifikasi_pembayaran_not_on_process = '';
if ($_SESSION['role_id'] == '12') {
    $perubahan_verifikasi_pembayaran_not_on_process = $this->db->query("select * from v_count_perubahan_verifikasi_pembayaran_not_on_process where sales_category_id = '" . $_SESSION['sales_category_id'] . "'")->num_rows();
}
if ($_SESSION['role_id'] == '7') {
    $perubahan_verifikasi_pembayaran_not_on_process = $this->db->query("select * from v_count_perubahan_verifikasi_pembayaran_not_on_process where user_pic_id = '" . $_SESSION['user_id'] . "'")->num_rows();
}
?>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-users"></i><span class="kt-menu__link-text">User</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <?php if ($_SESSION['role_id'] == 12) { ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_user/table_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">User Sales</span></a></li>
            <?php } ?>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Member') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Member</span></a></li>
			<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_member_status" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Status Seller</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-hand-holding-usd"></i><span class="kt-menu__link-text">Penjualan Lunas</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('T_sales/Form/Tambah') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Input Penjualan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('T_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Penjualan</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-money-check-alt"></i><span class="kt-menu__link-text">Penjualan Utang</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Penjualan/Penjualan_utang') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Penjualan Utang</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Penjualan/Penjualan_utang/List') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Penjualan Utang</span></a></li>
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-puzzle-piece"></i><span class="kt-menu__link-text">Penjualan Parsial</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Penjualan/Penjualan_parsial') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Penjualan Parsial</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Penjualan/Penjualan_parsial/List') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">List Penjualan Parsial</span></a></li>
        </ul>
    </div>
</li>
<?php if ($_SESSION['role_id'] == 12) { ?>
    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('Diskon') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-percentage"></i><span class="kt-menu__link-text">Diskon</span></a></li>
<?php } ?>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('Laporan2/list_boleh_kirim_vs_kirim') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-alt"></i><span class="kt-menu__link-text">List Pengiriman</span></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('T_package_trial2/list_pendingan') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-alt"></i><span class="kt-menu__link-text">List Pendingan Produk</span></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" style="font-weight: 900;letter-spacing: 0.5px;"><a href="<?= base_url('T_package_trial2/list_pendingan_seller') ?>" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-list-alt"></i><span class="kt-menu__link-text">List Pendingan Seller</span></a></li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cart-arrow-down"></i><span class="kt-menu__link-text">Pembayaran</span><span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em; padding-top: 0.7em;"><?php if ($perubahan_verifikasi_pembayaran_not_on_process > 0) echo $perubahan_verifikasi_pembayaran_not_on_process; ?></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_account/seller') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pembayaran</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_account/seller_parsial') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Pembayaran Parsial</span></a></li>
            <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('M_account/edit_verif_pembayaran') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Perubahan Verifikasi Pembayaran <span class="badge badge-warning badge-pill" style="padding-left: 0.7em;padding-right: 0.7em;"><?php if ($perubahan_verifikasi_pembayaran_not_on_process > 0) echo $perubahan_verifikasi_pembayaran_not_on_process; ?></span></span></a></li> -->
        </ul>
    </div>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-cubes"></i><span class="kt-menu__link-text">Produk</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Produk_global') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Master Produk</span></a></li>
			<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Produk_global" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>M_product_global_group" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">SKU Group</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Administrator/Stock_Produk/stock_opname_produk') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Stock Hari Ini</span></a></li>
            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Data Packing</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('T_package_trial2') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">History Pengiriman</span></a></li>
                    </ul>
                </div>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Gudang</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/monthly_all') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Bulanan</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/division_daily') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/monthly_factory_in') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pabrik Bulanan</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/daily_all') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/daily_out') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/daily_product_out') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Produk Harian</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_out') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Data Keluar Bulanan</span></a></li>
                        <?php if ($_SESSION['role_id'] == "16") { ?>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan2/chart') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Chart</span></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Laporan Sales</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-book"></i><span class="kt-menu__link-text">Sales</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Produk</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Bulanan</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Sales</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_payment') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Pembayaran Produk</span></a></li>
                    </ul>
                </div>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Sales</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_seller') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller Bulanan</span></a></li>
                        <!--<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_paid') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Mutasi Perorangan</span></a></li>-->
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales_product_user') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Sales</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Laporan/monthly_sales') ?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Bulanan</span></a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</li>
<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Laporan Inventory</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-plus-circle"></i><span class="kt-menu__link-text">Inventory</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Bulanan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/division_daily" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Pembagian</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/monthly_factory_in" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Terima Barang Bulanan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_all" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Stok Harian</span></a></li>
            <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Keluar Harian</span></a></li> -->
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/daily_product_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Harian</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_out" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Barang Keluar Per Produk Bulanan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Pendingan</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/laporan_pendingan_per_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Laporan Data Pendingan Per Seller</span></a></li>
            <?php if ($_SESSION['role_id'] == "16") { ?>
                <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan2/chart" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Chart</span></a></li>
            <?php } ?>
        </ul>
    </div>
</li>