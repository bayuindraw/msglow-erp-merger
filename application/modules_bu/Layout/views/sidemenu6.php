<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fas fa-handshake"></i><span class="kt-menu__link-text">Laporan Penjualan</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Produk</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Bulanan</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Produk Sales</span></a></li>
                    </ul>
                </div>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Seller</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_seller" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller Bulanan</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_paid" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Mutasi Perorangan</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales_product_user" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Per Seller</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url() ?>Laporan/monthly_sales" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span style="background-color: #fff;"></span></i><span class="kt-menu__link-text">Laporan Penjualan Bulanan</span></a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</li>