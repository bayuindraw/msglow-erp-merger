<?php
defined('BASEPATH') or exit('No direct script access allowed');


class M_account2 extends CI_Controller
{

	var $url_ = "M_account2";
	var $url2_ = "M_account/table_detail";
	var $id_ = "account_id";
	var $id2_ = "account_detail_id";
	var $eng_ = "account";
	var $eng2_ = "account_detail";
	var $ind_ = "Pembayaran Seller";
	var $ind2_ = "Akun Detail";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	function form_import_bank()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan';
		$datacontent['coas'] = $this->db->query("SELECT * FROM coa_4 WHERE nama LIKE '%bca%' OR nama LIKE '%mandiri%'")->result_array();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_import_bank', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	function import_csv_bank()
	{
		$cek_mandiri = $this->db->query("SELECT * FROM coa_4 WHERE id = '" . $_POST['coa_id'] . "' AND nama LIKE '%mandiri%'")->row_array();
		if (($handle		=	fopen($_FILES['csv']['tmp_name'], "r")) !== FALSE) {
			$n			=	0;
			$i = 0;
			if (@$cek_mandiri) {
				while (($row	=	fgetcsv($handle)) !== FALSE) {
					if ($n >= 1) {
						$data[$i]['coa_date'] = date_format(date_create($row[1]), 'd/m/Y');
						$data[$i]['coa_transaction_note'] = $row[4] . ' ' . $row[5];
						$data[$i]['coa_debit'] = str_replace(',', '', $row[8]);
						$data[$i]['coa_credit'] = str_replace(',', '', $row[7]);
						$i++;
					}
					$n++;
				}
			} else {
				while (($row	=	fgetcsv($handle)) !== FALSE) {
					if ($n >= 7 && isset($row[4])) {
						if ($row[0] == 'PEND') {
							$data[$i]['coa_date'] = date('d/m/Y');
						} else {
							$data[$i]['coa_date'] = date_format(date_create($row[0]), 'd/m/Y');
						}
						$data[$i]['coa_transaction_note'] = $row[1];
						if (substr($row[3], strlen($row[3]) - 2, 2) == 'CR') {
							$data[$i]['coa_debit'] = str_replace(',', '', substr($row[3], 0, strlen($row[3]) - 6));
							$data[$i]['coa_credit'] = 0;
						} else {
							$data[$i]['coa_debit'] = 0;
							$data[$i]['coa_credit'] = str_replace(',', '', substr($row[3], 0, strlen($row[3]) - 6));
						}
						$i++;
					}
					$n++;
				}
			}
		}
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Laporan Bulanan';

		$datacontent['datas'] = $data;
		$datacontent['coa'] = $this->db->get_where('coa_4', ['id' => $_POST['coa_id']])->row_array();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_import_bank', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_mutasi_bank()
	{
		$this->db->trans_begin();
		foreach ($_POST['input'] as $input) {
			$data_paid['coa_id'] = $_POST['coa_id'];
			$data_paid['coa_name'] = $_POST['coa_name'];
			$data_paid['coa_code'] = $_POST['coa_code'];
			$data_paid['coa_level'] = 4;
			$data_paid['coa_date'] = date_format(date_create($input['coa_date']), 'Y-m-d');
			$data_paid['coa_debit'] = str_replace(',', '', $input['coa_debit']);
			$data_paid['coa_credit'] = str_replace(',', '', $input['coa_credit']);
			$data_paid['coa_transaction_note'] = $input['coa_transaction_note'];
			$data_paid['user_create'] = $_SESSION['user_id'];
			$data_paid['coa_transaction_source'] = 2;
			$data_paid['coa_transaction_realization'] = 0;
			$this->Model->insert_coa_transaction($data_paid);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $data_paid[coa_credit],  coa_total_debit = coa_total_debit + $data_paid[coa_debit] WHERE coa_id = '$_POST[coa_id]' AND coa_level = '$data_paid[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $data_paid[coa_credit],  coa_total_debit = coa_total_debit + $data_paid[coa_debit] WHERE coa_id = '$_POST[coa_id]' AND REPLACE(coa_total_date, '-', '') < REPLACE('" . $data_paid['coa_date'] . "', '-', '') AND coa_level = '$data_paid[coa_level]'");
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('M_account2/form_import_bank');
	}


	public function xxx()
	{
		$this->db->trans_begin();
		$data = $this->db->query("SELECT * FROM m_user")->result_array();
		foreach ($data as $index => $value) {
			$this->db->query("UPDATE m_user SET user_password_sha = '" . hash('sha256', $value['user_password']) . "' WHERE user_id = '" . $value['user_id'] . "'");
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		die();
	}

	public function xx($id = '')
	{
		$this->db->trans_begin();
		$data = $this->db->query("SELECT A.*, B.nama, B.kode FROM m_account A JOIN member B ON B.kode = A.seller_id WHERE seller_id IS NOT NULL")->result_array();
		$i = 1;
		$tanggal = "2021-03-09";
		foreach ($data as $index => $value) {
			$this->db->query("UPDATE m_account SET account_code2 = '" . $i . "' WHERE account_id = '" . $value['account_id'] . "'");
			$data3['kode'] = '100.1.3.' . $i;
			$data3['nama'] = 'Piutang ' . str_replace("'", "", $value['nama']) . ' (' . $value['seller_id'] . ')';
			$data3['coa3_id'] = 12;
			$this->db->insert('coa_4', $data3);
			$id_coa_1 = $this->db->insert_id();
			$data3['kode'] = '200.1.2.' . $i;
			$data3['nama'] = 'Deposit ' . str_replace("'", "", $value['nama']) . ' (' . $value['seller_id'] . ')';
			$data3['coa3_id'] = 24;
			$this->db->insert('coa_4', $data3);
			$id_coa_2 = $this->db->insert_id();
			$data4 = array();
			$data4['coa_name'] = 'Piutang ' .  str_replace("'", "", $value['nama']) . ' (' . $value['seller_id'] . ')';
			$data4['coa_code'] = '100.1.3.' . $i;
			$data4['coa_level'] = 4;
			$data4['coa_id'] = $id_coa_1;
			$this->db->insert('t_coa_total', $data4);
			$data4['coa_total_date'] = substr($tanggal, 0, 7) . "-01";
			$this->db->insert('t_coa_total_history', $data4);
			$data4 = array();
			$data4['coa_name'] = 'Deposit ' .  str_replace("'", "", $value['nama']) . ' (' . $value['seller_id'] . ')';
			$data4['coa_code'] = '200.1.2.' . $i;
			$data4['coa_level'] = 4;
			$data4['coa_id'] = $id_coa_2;
			$this->db->insert('t_coa_total', $data4);
			$data4['coa_total_date'] = substr($tanggal, 0, 7) . "-01";
			$this->db->insert('t_coa_total_history', $data4);
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		die();
	}

	public function table_sales($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_sales', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function vendor()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_vendor', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function verification_transfer_seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$belum_real = $this->db->query("SELECT A.account_id, Y.seller_id, CONCAT('upload/TRANSFER/', A.account_detail_proof) as proof, Y.sales_category_id, A.account_detail_real_id, E.nama, C.account_name, A.account_detail_transfer_name, B.account_detail_date, B.account_detail_debit, A.account_detail_proof FROM t_account_detail A JOIN t_account_detail B ON B.account_detail_id = A.account_detail_id AND B.account_detail_real_id = (A.account_detail_real_id + 1) AND B.account_detail_debit = A.account_detail_credit JOIN m_account C ON C.account_id = B.account_id JOIN m_account D ON D.account_id = A.account_id JOIN member E ON E.kode = D.seller_id JOIN t_coa_transaction X ON X.coa_transaction_source_id = A.account_detail_real_id JOIN m_account Y ON Y.account_id = A.account_id WHERE A.account_detail_realization = '0' AND X.coa_transaction_realization = '0' AND X.coa_transaction_source = '1' AND X.coa_code LIKE '%100.1.3%'")->result_array();
		$datacontent['belum_real'] = count($belum_real);
		$sudah_real = $this->db->query("SELECT * FROM t_coa_transaction WHERE coa_transaction_source = 1 AND coa_transaction_realization = 1 AND coa_code LIKE '%100.1.3%'")->result_array();
		$datacontent['sudah_real'] = count($sudah_real);
		//$datacontent['datatable'] = $this->Model->get();
		$datacontent['mutasis'] = $this->db->query('SELECT * FROM t_coa_transaction WHERE coa_code LIKE "%100.1.2.%"')->result_array();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_verification_transfer_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function verification_transfer_seller2()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$belum_real = $this->db->query("SELECT A.account_id, Y.seller_id, CONCAT('upload/TRANSFER/', A.account_detail_proof) as proof, Y.sales_category_id, A.account_detail_real_id, E.nama, C.account_name, A.account_detail_transfer_name, B.account_detail_date, B.account_detail_debit, A.account_detail_proof FROM t_account_detail A JOIN t_account_detail B ON B.account_detail_id = A.account_detail_id AND B.account_detail_real_id = (A.account_detail_real_id + 1) AND B.account_detail_debit = A.account_detail_credit JOIN m_account C ON C.account_id = B.account_id JOIN m_account D ON D.account_id = A.account_id JOIN member E ON E.kode = D.seller_id JOIN t_coa_transaction X ON X.coa_transaction_source_id = A.account_detail_real_id JOIN m_account Y ON Y.account_id = A.account_id WHERE A.account_detail_realization = '0' AND X.coa_transaction_realization = '0' AND X.coa_transaction_source = '1' AND X.coa_code LIKE '%100.1.3%'")->result_array();
		$datacontent['belum_real'] = count($belum_real);
		$sudah_real = $this->db->query("SELECT * FROM t_coa_transaction WHERE coa_transaction_source = 1 AND coa_transaction_realization = 1 AND coa_code LIKE '%100.1.3%'")->result_array();
		$datacontent['sudah_real'] = count($sudah_real);
		//$datacontent['datatable'] = $this->Model->get();
		$datacontent['mutasis'] = $this->db->query('SELECT * FROM t_coa_transaction WHERE coa_code LIKE "%100.1.2.%"')->result_array();
		$rejecttotal = $this->db->query("SELECT COUNT(coa_transaction_id) as jum FROM t_coa_transaction_temp WHERE coa_transaction_source = 2")->row_array();
		$datacontent['rejecttotal'] = $rejecttotal['jum'];
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_verification_transfer_seller2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function factory()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_factory', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		//$datacontent['datatable'] = $this->Model->get();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_print', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function get_data_verification_transfer_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_verification_transfer_seller();
	}

	public function get_data_verification_transfer_seller2()
	{
		$datacontent['datatable'] = $this->Model->get_data_verification_transfer_seller2();
	}

	public function get_data_verification_transfer_seller3()
	{
		$datacontent['datatable'] = $this->Model->get_data_verification_transfer_seller3();
	}

	public function get_data_transfer($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_transfer($id);
	}

	public function get_data_sales($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_sales($id);
	}

	public function get_data_vendor()
	{
		$datacontent['datatable'] = $this->Model->get_data_vendor();
	}

	public function get_data_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_seller();
	}

	public function get_data_print()
	{
		$datacontent['datatable'] = $this->Model->get_data_print();
	}

	public function get_data_factory()
	{
		$datacontent['datatable'] = $this->Model->get_data_factory();
	}

	public function table_detail($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		//$datacontent['datatable'] = $this->Model->get_detail($id);
		$datacontent['id'] = $id;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_detail($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_detail($id);
	}

	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['id'] = $id;
		$datacontent['m_employee'] = $this->Model->get_list_employee();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail($parameter = '', $id = '', $id_detail = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account_detail_category'] = $this->Model->get_list_account_detail_category();
		$datacontent['id'] = $id;
		$datacontent['id_detail'] = $id_detail;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_sales_allow($id = '')
	{
		if ($this->Model->chk_account_detail_sales_product($id) != "") {
			redirect(site_url($this->url_ . "/view_transfer_seller/" . $id));
		}
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$arrdata = $this->Model->get_account_detail_sales($id);
		foreach ($arrdata as $index => $value) {
			$datacontent['arraccount_detail_sales'][$value['sales_id']][$value['sales_detail_id']] = $value;
			$datacontent['arraccount_detail_sales2'][$value['sales_id']] = $value;
		}
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_sales_allow', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_factory_invoice($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['arraccount_factory'] = $this->Model->get_account_factory($id);
		$datacontent['title'] = 'Input Invoice ' . $datacontent['arraccount_factory']['nama_factory'];
		$arrterima_produk = $this->Model->get_terima_produk($datacontent['arraccount_factory']['id_factory']);
		foreach ($arrterima_produk as $index => $value) {
			$datacontent['arrterima_produk'][$value['nomor_surat_jalan']][$value['id_barang'] . '|' . $value['tgl_terima']] = $value;
		}
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_factory_invoice', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function transfer_seller($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account_seller();
		$datacontent['arrlist_account_sales'] = $this->Model->get_list_account_sales($id);
		$arrdetail_sales = $this->Model->get_detail_sales($id);
		foreach ($arrdetail_sales as $index => $value) {
			$datacontent['arrdetail_sales'][$value['sales_id']][] = $value;
		}
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function transfer_seller2($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account_seller();
		$datacontent['arraccount'] = $this->Model->get_account($id);
		$datacontent['arrlist_account_sales'] = $this->Model->get_list_account_sales($id);
		$arrdetail_sales = $this->Model->get_detail_sales($id);
		foreach ($arrdetail_sales as $index => $value) {
			$datacontent['arrdetail_sales'][$value['sales_id']][] = $value;
		}
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer_seller2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function view_transfer_seller($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['arraccount_detail'] = $this->Model->get_account_detail($id);
		$datacontent['chk_account_detail'] = $this->Model->chk_account_detail_sales_product($id);
		$arrdata = $this->Model->get_account_detail_sales_product($id);
		foreach ($arrdata as $index => $value) {
			$datacontent['arraccount_detail_sales_product'][$value['sales_id']][$value['product_id']] = $value;
			$datacontent['arraccount_detail_sales'][$value['sales_id']] = $value;
		}
		$datacontent['id'] = $id;
		$data['file'] = 'Detail Pembayaran';
		$data['content'] = $this->load->view($this->url_ . '/view_transfer_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_detail_id'] = $id;
				$from['account_detail_debit'] = 0;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_id'] = $from['account_id'];
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($from['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}

				$to['account_detail_id'] = $id;
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_credit'] = 0;
				$to['account_detail_debit'] = $from['account_detail_credit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function set_transfer_seller()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			$id2 = $this->input->post('id');
			$sales = $this->input->post('sales');
			if ($_POST['parameter'] == "tambah") {
				$from['account_detail_credit'] = str_replace(',', '', $from['account_detail_credit']);
				$from['account_id'] = $id2;
				$from['account_detail_pic'] = $_SESSION['user_fullname'];
				$from['account_detail_id'] = $id;
				$from['account_detail_debit'] = 0;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
				$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
				$exec = $this->Model->insert_detail($from);
				$insert_id = $this->db->insert_id();

				$path = $_FILES['transfer']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				$config['upload_path']          = './upload/TRANSFER/';
				$config['allowed_types']        = '*';
				$config['file_name']        = $insert_id;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('transfer')) {
					$error = array('error' => $this->upload->display_errors());
				} else {
					$this->upload->data();
					$dataxy['account_detail_proof'] = $insert_id . "." . $ext;
					$this->Model->update_detail($dataxy, ['account_detail_real_id' => $insert_id]);
				}
				foreach ($_POST['paid'] as $index_paid => $value_paid) {
					if ($value_paid > 0) {
						$value_paid = str_replace(',', '', $value_paid);
						$data_paid['account_detail_real_id'] = $exec;
						$data_paid['sales_id'] = $sales[$index_paid];
						$data_paid['account_detail_target_real_id'] = $index_paid;
						$data_paid['account_detail_sales_amount'] = $value_paid;
						$data_paid['account_detail_sales_date'] = $from['account_detail_date'];
						$this->Model->insert_account_detal_sales($data_paid);
					}
				}
				$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');
				/*if($from['account_detail_deposit'] > 0){
					$datayyyx['account_detail_deposit_amount'] = $from['account_detail_deposit'];
					$datayyyx['account_detail_real_id'] = $insert_id;
					$datayyyx['account_id'] = $from['account_id'];
					$this->Model->insert_account_detail_deposit($datayyyx);
				}*/

				$to['account_detail_proof'] = $dataxy['account_detail_proof'];
				$to['account_detail_id'] = $id;
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_credit'] = 0;
				$to['account_detail_transfer_name'] = $from['account_detail_transfer_name'];
				$to['account_detail_debit'] = $from['account_detail_credit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_ . "/form_sales_allow2/" . $insert_id));
	}

	/*public function set_transfer_seller2()
	{
		$id = $this->Model->get_max_id();
		if ($this->input->post('simpan')) {
			$from2 = $this->input->post('from');
			$to = $this->input->post('to');
			$id2 = $this->input->post('id');
			$sales = $this->input->post('sales');
			$account_id = $this->input->post('account_id');
			if ($_POST['parameter'] == "tambah") {
				foreach($account_id as $index => $value){
					$from['account_detail_transfer_name'] = $from2['account_detail_transfer_name'][$index];  
					$from['account_detail_date'] = $from2['account_detail_date'][$index];  
					$from['account_detail_credit'] = $from2['account_detail_credit'][$index];  
					$from['account_detail_credit'] = str_replace(',', '', $from['account_detail_credit']);
					$from['account_id'] = $id2;
					$from['account_detail_pic'] = $_SESSION['user_fullname'];
					$from['account_detail_id'] = $id; 
					$from['account_detail_debit'] = 0;
					$from['account_detail_category_id'] = 1;
					$from['account_detail_type_id'] = 1;
					$from['account_detail_user_id'] = $_SESSION['user_id'];
					$from['account_detail_date_create'] = date('Y-m-d H:i:s');
					//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
					//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
					$exec = $this->Model->insert_detail($from);
					$insert_id = $this->db->insert_id();
					$path = $_FILES['transfer_'.$index]['name'];
					$ext = pathinfo($path, PATHINFO_EXTENSION);
					$config['upload_path']          = './upload/TRANSFER/';
					$config['allowed_types']        = '*';
					$config['file_name']        = $insert_id;
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('transfer_'.$index))
					{
							$error = array('error' => $this->upload->display_errors());
					}
					else
					{
							$this->upload->data();
							$dataxy['account_detail_proof'] = $insert_id.".".$ext;
							$this->Model->update_detail($dataxy, ['account_detail_real_id' => $insert_id]);
					}
					$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');
					
					$to['account_id'] = $value;
					$to['account_detail_proof'] = @$dataxy['account_detail_proof'];
					$to['account_detail_id'] = $id;
					$to['account_detail_pic'] = $from['account_detail_pic'];
					$to['account_detail_credit'] = 0;
					$to['account_detail_transfer_name'] = $from['account_detail_transfer_name'];
					$to['account_detail_debit'] = $from['account_detail_credit'];
					$to['account_detail_pic'] = $from['account_detail_pic'];
					$to['account_detail_date'] = $from['account_detail_date'];
					$to['account_detail_category_id'] = 2;
					$to['account_detail_type_id'] = 1;
					$to['account_detail_user_id'] = $from['account_detail_user_id'];
					$to['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($to);
					$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
				}
				
				foreach($_POST['paid'] as $index_paid => $value_paid){
					if($value_paid > 0){
						$value_paid = str_replace(',', '', $value_paid);
						$data_paid['account_detail_real_id'] = $insert_id;
						$data_paid['sales_id'] = $sales[$index_paid];
						$data_paid['account_detail_target_real_id'] = $index_paid;
						$data_paid['account_detail_sales_amount'] = $value_paid;
						$data_paid['account_detail_sales_date'] = $from['account_detail_date'];
						$data_paid['account_detail_id'] = $id;
						
						$this->Model->insert_account_detal_sales($data_paid);
					}
				}
				$arrx = $this->Model->get_account($id2);
				$account['account_deposit'] = ((int)@$arrx['account_deposit']) + str_replace(',', '', $_POST['deposit']);
				$this->Model->update($account, [$this->id_ => $id2]);
			} else {
				$data[$this->eng_.'_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_."/form_sales_allow/".$id));
	}*/

	public function set_transfer_seller2()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		if ($this->input->post('simpan')) {
			$from2 = $this->input->post('from');
			$to = $this->input->post('to');
			$id2 = $this->input->post('id');
			$sales = $this->input->post('sales');
			$account_id = $this->input->post('account_id');
			$account = $this->db->query("SELECT * FROM m_account WHERE account_id = '$id2'")->row_array();
			$piutang = $this->db->query("SELECT * FROM coa_4 WHERE kode = '100.1.3.$account[account_code2]'")->row_array();
			$deposit = $this->db->query("SELECT * FROM coa_4 WHERE kode = '200.1.2.$account[account_code2]'")->row_array();
			if ($_POST['parameter'] == "tambah") {
				foreach ($account_id as $index => $value) {
					$from['account_detail_transfer_name'] = $from2['account_detail_transfer_name'][$index];
					$from['account_detail_date'] = $from2['account_detail_date'][$index];
					$from['account_detail_credit'] = $from2['account_detail_credit'][$index];
					$from['account_detail_credit'] = str_replace(',', '', $from['account_detail_credit']);
					$from['account_id'] = $id2;
					$from['account_detail_pic'] = $_SESSION['user_fullname'];
					$from['account_detail_id'] = $id;
					$from['account_detail_debit'] = 0;
					$from['account_detail_category_id'] = 1;
					$from['account_detail_type_id'] = 1;
					$from['account_detail_user_id'] = $_SESSION['user_id'];
					$from['account_detail_date_create'] = date('Y-m-d H:i:s');
					//$from['account_detail_deposit'] = str_replace(',', '', $_POST['deposit']);
					//$from['account_detail_deposit_start'] = $from['account_detail_deposit'];
					$exec = $this->Model->insert_detail($from);

					$data_paid3['coa_id'] = $piutang['id'];
					$data_paid3['coa_name'] = $piutang['nama'];
					$data_paid3['coa_code'] = $piutang['kode'];
					$data_paid3['coa_level'] = 4;
					$data_paid3['coa_debit'] = $from['account_detail_credit'];
					$data_paid3['coa_credit'] = 0;
					$data_paid3['coa_transaction_note'] = 'Pembayaran ' . $from['account_detail_transfer_name'];
					$data_paid3['user_create'] = $_SESSION['user_id'];
					$data_paid3['coa_transaction_source'] = 1;
					$data_paid3['coa_transaction_source_id'] = $exec;
					$this->Model->insert_coa_transaction($data_paid3);

					$data_paid2['coa_id'] = $deposit['id'];
					$data_paid2['coa_name'] = $deposit['nama'];
					$data_paid2['coa_code'] = $deposit['kode'];
					$data_paid2['coa_level'] = 4;
					$data_paid2['coa_debit'] = 0;
					$data_paid2['coa_credit'] = $from['account_detail_credit'];
					$data_paid2['coa_transaction_note'] = 'Pembayaran ' . $from['account_detail_transfer_name'];
					$data_paid2['user_create'] = $_SESSION['user_id'];
					$data_paid2['coa_transaction_source'] = 1;
					$data_paid2['coa_transaction_source_id'] = $exec;
					$this->Model->insert_coa_transaction($data_paid2);

					//$coa_total1 = $this->db->get_where('t_coa_total', ['coa_code' => $deposit['kode']])->row_array();
					//$coa_total2 = $this->db->get_where('t_coa_total', ['coa_code' => $piutang['kode']])->row_array();
					//$coa_total1['coa_total_credit'] += str_replace(',', '', $from['account_detail_credit']);
					//$coa_total2['coa_total_debit'] += str_replace(',', '', $from['account_detail_credit']);
					$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $from[account_detail_credit] WHERE coa_id = '$deposit[id]' AND coa_level = '$data_paid3[coa_level]'");
					$this->db->query("UPDATE t_coa_total_history SET account_credit = account_credit + $from[account_detail_credit] WHERE coa_id = '$deposit[id]' AND REPLACE(coa_total_date, '-', '') < REPLACE($from[account_detail_date], '-', '') AND coa_level = '$data_paid3[coa_level]'");
					$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $from[account_detail_credit] WHERE coa_id = '$piutang[id]' AND coa_level = '$data_paid2[coa_level]'");
					$this->db->query("UPDATE t_coa_total_history SET account_debit = account_debit + $from[account_detail_credit] WHERE coa_id = '$piutang[id]' AND REPLACE(coa_total_date, '-', '') < REPLACE($from[account_detail_date], '-', '') AND coa_level = '$data_paid2[coa_level]'");
					//$this->db->update('t_coa_total', $coa_total2, ['coa_total_id' => $coa_total2['coa_total_id']]);

					$insert_id = $this->db->insert_id();
					$path = $_FILES['transfer_' . $index]['name'];
					$ext = pathinfo($path, PATHINFO_EXTENSION);
					$config['upload_path']          = './upload/TRANSFER/';
					$config['allowed_types']        = '*';
					$config['file_name']        = $insert_id;
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload('transfer_' . $index)) {
						$error = array('error' => $this->upload->display_errors());
					} else {
						$this->upload->data();
						$dataxy['account_detail_proof'] = $insert_id . "." . $ext;
						$this->Model->update_detail($dataxy, ['account_detail_real_id' => $insert_id]);
					}
					$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');
					/*if($from['account_detail_deposit'] > 0){
						$datayyyx['account_detail_deposit_amount'] = $from['account_detail_deposit'];
						$datayyyx['account_detail_real_id'] = $insert_id;
						$datayyyx['account_id'] = $from['account_id'];
						$this->Model->insert_account_detail_deposit($datayyyx);
					}*/
					$to['account_id'] = $value;
					$to['account_detail_proof'] = @$dataxy['account_detail_proof'];
					$to['account_detail_id'] = $id;
					$to['account_detail_pic'] = $from['account_detail_pic'];
					$to['account_detail_credit'] = 0;
					$to['account_detail_transfer_name'] = $from['account_detail_transfer_name'];
					$to['account_detail_debit'] = $from['account_detail_credit'];
					$to['account_detail_pic'] = $from['account_detail_pic'];
					$to['account_detail_date'] = $from['account_detail_date'];
					$to['account_detail_category_id'] = 2;
					$to['account_detail_type_id'] = 1;
					$to['account_detail_user_id'] = $from['account_detail_user_id'];
					$to['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($to);
					$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
				}

				foreach ($_POST['paid'] as $index_paid => $value_paid) {
					if ($value_paid > 0) {
						$value_paid = str_replace(',', '', $value_paid);
						$data_paid['account_detail_real_id'] = $exec;
						$data_paid['sales_id'] = $sales[$index_paid];
						$data_paid['account_detail_target_real_id'] = $index_paid;
						$data_paid['account_detail_sales_amount'] = $value_paid;
						$data_paid['account_detail_sales_date'] = $from['account_detail_date'];
						$data_paid['account_detail_id'] = $id;
						$this->Model->insert_account_detal_sales($data_paid);
					}
				}
				$arrx = $this->Model->get_account($id2);
				$account['account_deposit'] = ((int)@$arrx['account_deposit']) + str_replace(',', '', $_POST['deposit']);
				$this->Model->update($account, [$this->id_ => $id2]);
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_ . "/form_sales_allow/" . $id));
	}

	public function simpan_sales_allow()
	{
		$allow = $this->input->post('allow');
		$id = $this->input->post('id');
		$ids = $this->input->post('ids');
		$product_id = $this->input->post('product_id');
		foreach ($allow as $index => $arrvalue) {
			foreach ($arrvalue as $index2 => $value2) {
				//if($value2 > 0){
				$data['account_detail_id'] = $id;
				$data['sales_id'] = $index;
				$data['sales_detail_id'] = $index2;
				$data['product_id'] = $product_id[$index][$index2];
				$data['account_detail_sales_product_allow'] = $value2;
				$data['account_detail_sales_id'] = $ids[$index][$index2];
				$exec = $this->Model->insert_account_detail_sales_product($data);
				//} 
			}
		}
		redirect(site_url($this->url_ . "/seller"));
	}

	public function simpan()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$data[$this->eng_ . '_monthly_debit'] = $data[$this->eng_ . '_debit'];
				$data[$this->eng_ . '_monthly_credit'] = $data[$this->eng_ . '_credit'];
				$data[$this->eng_ . '_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert($data);
				$data2['account_id'] = $this->db->insert_id();
				$data2['account_reset_date'] = $data[$this->eng_ . '_date_reset'];
				$data2['account_reset_date_create'] = $data[$this->eng_ . '_date_create'];
				$data2['account_reset_debit'] = $data[$this->eng_ . '_monthly_debit'];
				$data2['account_reset_credit'] = $data[$this->eng_ . '_monthly_credit'];
				$exec = $this->Model->insert_reset($data2);
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}
	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}

	public function simpan_detail()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$id = $this->input->post('id');
				$id2 = $this->Model->get_max_id();
				$data[$this->id_] = $id;
				$data[$this->id2_] = $id2;
				$data[$this->eng2_ . '_type_id'] = 2;
				$data[$this->eng2_ . '_user_id'] = $_SESSION['user_id'];
				if ($_POST['realisasi'] == "on") {
					$data[$this->eng2_ . '_realization'] = 1;
				} else {
					$data[$this->eng2_ . '_realization'] = 0;
				}
				if ($_POST['transaction_type'] == "debit") {
					$type = "debit";
					$data[$this->eng2_ . '_debit'] = $_POST['transaction_amount'];
				} else if ($_POST['transaction_type'] == "credit") {
					$type = "credit";
					$data[$this->eng2_ . '_credit'] = $_POST['transaction_amount'];
				}
				$data[$this->eng2_ . '_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($data);
				$this->db->where('account_id', $id);
				$row = $this->Model->get()->row_array();
				if (@$row['account_date_reset'] > $data[$this->eng2_ . '_date']) {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_ . '_date']);
				} else {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
				}
			} else {
				$id = $this->input->post('id');
				$data[$this->eng2_ . '_user_id'] = $_SESSION['user_id'];
				if ($_POST['realisasi'] == "on") {
					$data[$this->eng2_ . '_realization'] = 1;
				} else {
					$data[$this->eng2_ . '_realization'] = 0;
				}
				if ($_POST['transaction_type'] == "debit") {
					$type = "debit";
					$data[$this->eng2_ . '_debit'] = $_POST['transaction_amount'];
					$data[$this->eng2_ . '_credit'] = 0;
				} else if ($_POST['transaction_type'] == "credit") {
					$type = "credit";
					$data[$this->eng2_ . '_credit'] = $_POST['transaction_amount'];
					$data[$this->eng2_ . '_debit'] = 0;
				}
				$data[$this->eng2_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update_detail($data, [$this->id_ => $this->input->post('id'), $this->id2_ => $this->input->post('id_detail')]);
				$this->db->where('account_id', $id);
				$row = $this->Model->get()->row_array();
				if (@$row['account_date_reset'] > $this->input->post('transaction_date_last')) {
					$this->Model->update_balance($id,  -1 * ($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'), @$row['account_type_id'], $this->input->post('transaction_date_last'));
				} else {
					$this->Model->update_balance($id,  -1 * ($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'));
				}
				if (@$row['account_date_reset'] > $data[$this->eng2_ . '_date']) {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_ . '_date']);
				} else {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url2_ . "/" . $id));
	}
	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();


		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();

		if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
		} else {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
		}



		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_ . "/$id"));
	}

	function approve_transfer_seller($id)
	{
		$this->Model->update_realization_detail($id, '1');
		redirect(site_url($this->url_ . "/verification_transfer_seller"));
	}

	function reject_transfer_seller($id)
	{
		$this->Model->update_realization_detail($id, '2');
		redirect(site_url($this->url_ . "/verification_transfer_seller"));
	}

	public function get_mutasi($id)
	{
		// $data['coa_transaction'] = $this->db->get_where('t_coa_transaction', ['coa_transaction_source_id' => $id, 'coa_debit' => 0])->row_array();
		// $data['mutasis'] = $this->db->query("SELECT C.* FROM t_account_detail X JOIN t_account_detail A ON A.account_detail_real_id = (X.account_detail_real_id + 1) JOIN m_account B ON B.account_id = A.account_id JOIN t_coa_transaction C ON C.coa_id = B.coa_id WHERE X.account_detail_real_id = $id AND C.coa_transaction_realization = 0")->result_array();
		// $data['piutang'] = $this->db->query("SELECT C.* FROM t_account_detail X JOIN m_account B ON B.account_id = X.account_id JOIN coa_4 C ON C.kode = CONCAT('100.1.3.', B.account_code2) WHERE X.account_detail_real_id = $id")->row_array();
		// $data['deposit'] = $this->db->query("SELECT C.* FROM t_account_detail X JOIN m_account B ON B.account_id = X.account_id JOIN coa_4 C ON C.kode = CONCAT('200.1.2.', B.account_code2) WHERE X.account_detail_real_id = $id")->row_array();
		$data['mutasis'] = $this->db->query("SELECT Z.user_name, A.account_detail_note_accounting, A.account_id, Y.seller_id, CONCAT('upload/TRANSFER/', A.account_detail_proof) as proof, Y.sales_category_id, A.account_detail_real_id, E.nama, C.account_name, A.account_detail_transfer_name, B.account_detail_date, B.account_detail_debit, A.account_detail_proof FROM t_account_detail A JOIN t_account_detail B ON B.account_detail_id = A.account_detail_id AND B.account_detail_real_id = (A.account_detail_real_id + 1) AND B.account_detail_debit = A.account_detail_credit JOIN m_account C ON C.account_id = B.account_id JOIN m_account D ON D.account_id = A.account_id JOIN member E ON E.kode = D.seller_id JOIN m_account Y ON Y.account_id = A.account_id JOIN m_user Z ON Z.user_id = A.account_detail_user_id WHERE A.account_detail_id = $id")->result_array();
		$data['id'] = $id;
		$this->load->view('modal_mutasi', $data);
	}

	public function get_verif($id, $acc)
	{
		// $data['coa_transaction'] = $this->db->get_where('t_coa_transaction', ['coa_transaction_source_id' => $id, 'coa_debit' => 0])->row_array();
		// $data['mutasis'] = $this->db->query("SELECT C.* FROM t_account_detail X JOIN t_account_detail A ON A.account_detail_real_id = (X.account_detail_real_id + 1) JOIN m_account B ON B.account_id = A.account_id JOIN t_coa_transaction C ON C.coa_id = B.coa_id WHERE X.account_detail_real_id = $id AND C.coa_transaction_realization = 0")->result_array();
		// $data['piutang'] = $this->db->query("SELECT C.* FROM t_account_detail X JOIN m_account B ON B.account_id = X.account_id JOIN coa_4 C ON C.kode = CONCAT('100.1.3.', B.account_code2) WHERE X.account_detail_real_id = $id")->row_array();
		// $data['deposit'] = $this->db->query("SELECT C.* FROM t_account_detail X JOIN m_account B ON B.account_id = X.account_id JOIN coa_4 C ON C.kode = CONCAT('200.1.2.', B.account_code2) WHERE X.account_detail_real_id = $id")->row_array();
		$data['mistake'] = $this->db->query("SELECT * FROM t_mistake WHERE mistake_id = $id")->row_array();
		$data['acc'] = $acc;
		$this->load->view('modal_verif', $data);
	}

	public function get_mutasi2($id)
	{
		$data['mutasis'] = $this->db->query("SELECT Z.user_name, A.account_detail_note_accounting, A.account_id, Y.seller_id, CONCAT('upload/TRANSFER/', A.account_detail_proof) as proof, Y.sales_category_id, A.account_detail_real_id, E.nama, C.account_name, A.account_detail_transfer_name, B.account_detail_date, B.account_detail_debit, A.account_detail_proof FROM t_account_detail A JOIN t_account_detail B ON B.account_detail_id = A.account_detail_id AND B.account_detail_real_id = (A.account_detail_real_id + 1) AND B.account_detail_debit = A.account_detail_credit JOIN m_account C ON C.account_id = B.account_id JOIN m_account D ON D.account_id = A.account_id JOIN member E ON E.kode = D.seller_id JOIN m_account Y ON Y.account_id = A.account_id JOIN m_user Z ON Z.user_id = A.account_detail_user_id WHERE A.account_detail_id = $id")->result_array();
		$this->load->view('modal_mutasi2', $data);
	}

	public function set_verification()
	{
		$this->db->trans_begin();
		date_default_timezone_set('Asia/Jakarta');
		$id = $_POST['id'];
		if(@$_POST['reject'] != ""){
			$arrdata_transaction_header = $this->db->query("SELECT * FROM t_coa_transaction_header WHERE account_detail_real_id = '$id'")->row_array();
			$this->db->query("DELETE FROM t_account_detail_real WHERE account_detail_real_id = '$id';");
			$this->db->query("DELETE FROM t_account_detail WHERE account_detail_id = '$id';");
			$this->db->query("DELETE FROM t_account_detail_sales WHERE account_detail_id = '$id';");
			$this->db->query("DELETE FROM t_account_detail_sales_product WHERE account_detail_id = '$id';");
			$this->db->query("DELETE FROM t_account_detail_delivery WHERE account_detail_id = '$id';");
			$this->db->query("DELETE FROM t_account_detail_delivery_product WHERE account_detail_id = '$id';");
			$this->db->query("DELETE FROM t_coa_transaction_header WHERE account_detail_real_id = '$id';");
			$this->db->query("DELETE FROM t_coa_transaction_temp WHERE coa_group_id = '".$arrdata_transaction_header['coa_transaction_header_id']."';");

			// data log
			$param = [
				'account_detail_real_id' => $id,
				'account_detail_real_status' => 2,
				'account_detail_real_log_detail' => 'Verifikasi 1 Pembayaran Seller',
				'account_detail_real_log_note' => 'Rejected by ' . $_SESSION['user_fullname'],
			];
			helper_log($param);
		}else{
			$this->db->query("UPDATE t_account_detail_real SET account_detail_real_status = 3 WHERE account_detail_real_id = '$id'");
			// data log
			$param = [
				'account_detail_real_id' => $id,
				'account_detail_real_status' => 3,
				'account_detail_real_log_detail' => 'Verifikasi 1 Pembayaran Seller',
				'account_detail_real_log_note' => 'Verification 1 by ' . $_SESSION['user_fullname'],
			];
			helper_log($param);
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('M_account2/verification_transfer_seller');
	}

	public function set_verification2()
	{
		foreach ($_POST['input'] as $input) {
			if (isset($input['is_verification'])) {
				$this->db->query("UPDATE t_coa_transaction_temp  SET coa_credit = $input[nominal] WHERE coa_transaction_source_id = $input[group_id] AND coa_credit > 0");
				$this->db->query("UPDATE t_coa_transaction_temp  SET coa_debit = $input[nominal] WHERE coa_transaction_source_id = $input[group_id] AND coa_debit > 0");
			}
		}
		redirect('M_account2/verification_transfer_seller2');
	}

	public function set_all_verif()
	{
		$this->db->trans_begin();
		$verif_groups = $this->db->query("SELECT * FROM t_account_detail_real WHERE account_detail_real_status = 3")->result_array();
		/*$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod = sales_discount_terms_mod_draft, sales_discount_terms_mod_draft = 0, sales_discount_terms_accumulation = sales_discount_terms_accumulation_draft, sales_discount_terms_accumulation_draft = 0, sales_discount_terms_mod_price = sales_discount_terms_mod_price_draft, sales_discount_terms_mod_price_draft = 0");
		$this->db->query("UPDATE t_sales_discount SET sales_discount_percentage_hoard = sales_discount_percentage_hoard_draft, sales_discount_percentage_hoard_draft = 0, sales_discount_percentage_active = sales_discount_percentage_active_draft");*/
		$arrupdseller = array();
		/*foreach ($edit_headers as $index_header => $edit_header) {
			$account = $this->db->query("SELECT * FROM t_account_detail WHERE account_detail_real_id = $edit_header[coa_transaction_source_id]")->row_array();
			$arrupdseller[$account['account_id']] = $account['account_id'];
			// if ($edit_header['coa_transaction_deposit'] !== null) {
			$this->db->query("UPDATE t_account_detail SET account_detail_deposit_paid = account_detail_deposit_paid_draft, account_detail_deposit_paid_draft = 0 WHERE account_detail_real_id =  $edit_header[coa_transaction_source_id]");
		}*/
		/*foreach($arrupdseller as $indexseller => $valueseller){
			$this->db->query("UPDATE m_account SET account_deposit = account_deposit_draft, account_deposit_draft = 0, account_deposit_draft_activation = 0 WHERE account_id =  '$valueseller'");
			$this->db->query("UPDATE t_account_detail SET account_detail_paid = account_detail_paid + account_detail_paid_draft, account_detail_paid_draft = 0 WHERE account_id =  '$valueseller' AND sales_id IS NOT NULL");
		}*/
		foreach ($verif_groups as $verif_group) {
				//$verif_group['coa_transaction_source_id']
				$verifs = $this->db->query("SELECT A.* FROM t_coa_transaction_temp A JOIN t_coa_transaction_header B ON B.coa_transaction_header_id = A.coa_group_id WHERE B.account_detail_real_id = '".$verif_group['account_detail_real_id']."'")->result_array();
				foreach ($verifs as $verif) {
					$idVerif = $verif['coa_transaction_id'];
					unset($verif['coa_transaction_temp_status']);
					unset($verif['coa_transaction_id']);
					unset($verif['coa_past_debit']);
					unset($verif['coa_past_credit']);
					$this->db->insert('t_coa_transaction', $verif);
					$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $verif[coa_debit], coa_total_credit = coa_total_credit + $verif[coa_credit] WHERE coa_id = $verif[coa_id] AND coa_level = '$verif[coa_level]'");
					$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $verif[coa_debit], coa_total_credit = coa_total_credit + $verif[coa_credit] WHERE coa_id = $verif[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($verif[coa_date], '-', '') AND coa_level = '$verif[coa_level]'");
					$this->db->delete('t_coa_transaction_temp', ['coa_transaction_id' => $idVerif]);
				}

				$idHeader = $verif['coa_group_id'];
				$this->db->query("UPDATE t_coa_transaction_header SET coa_transaction_debit = $verif[coa_credit], coa_transaction_credit = $verif[coa_credit], coa_transaction_payment = $verif[coa_credit], coa_transaction_temp_id = 0 WHERE coa_transaction_header_id = $verif[coa_group_id]");

				$this->db->query("UPDATE t_account_detail SET coa_transaction_confirm = 1 WHERE account_detail_id = '".$verif_group['account_detail_real_id']."'");
				$this->db->query("UPDATE t_account_detail_real B JOIN m_account A ON A.account_id = B.account_id SET A.account_deposit = B.account_detail_real_deposit_effect + A.account_deposit, B.account_detail_real_status = 4 WHERE B.account_detail_real_id = '".$verif_group['account_detail_real_id']."'");
				$this->db->query("UPDATE t_account_detail_sales A JOIN t_account_detail B ON B.sales_id = A.sales_id SET A.account_detail_sales_realization = 1, B.account_detail_paid = B.account_detail_paid + A.account_detail_sales_amount WHERE A.account_detail_id = '".$verif_group['account_detail_real_id']."'");
				$this->db->query("UPDATE t_account_detail_sales_product A JOIN t_sales_detail B ON B.sales_detail_id = A.sales_detail_id SET B.pending_quantity = B.pending_quantity + A.account_detail_sales_product_allow, A.account_detail_sales_product_connected = 1 WHERE A.account_detail_id = '".$verif_group['account_detail_real_id']."'");
				$this->db->query("UPDATE t_account_detail_delivery A SET A.account_detail_delivery_realization = 1 WHERE A.account_detail_id = '".$verif_group['account_detail_real_id']."'");
				$this->db->query("UPDATE t_account_detail_delivery_product A SET A.account_detail_delivery_product_status = 1, A.pending_quantity = A.account_detail_delivery_product_quantity WHERE A.account_detail_id = '".$verif_group['account_detail_real_id']."'");

				// data log
				$param = [
					'account_detail_real_id' => $verif_group['account_detail_real_id'],
					'account_detail_real_status' => 4,
					'account_detail_real_log_detail' => 'Verifikasi 2 Pembayaran Seller',
					'account_detail_real_log_note' => 'Verification 2 by ' . $_SESSION['user_fullname'],
				];
				helper_log($param);
				
				$updatePendings = $this->db->query("SELECT * FROM t_account_detail_delivery_product A JOIN t_sales B ON B.sales_id = A.sales_id WHERE A.account_detail_id = '".$verif_group['account_detail_real_id']."'")->result_array(); 
				foreach ($updatePendings as $updatePending) {
					/*$discount = $this->db->get_where('t_discount_total', ['account_detail_id' => $updatePending['account_detail_id']])->row_array();
					if ($discount['discount_total_price'] == null) $discount['discount_total_price'] = 0;
					$this->db->query("UPDATE t_sales_detail SET pending_quantity = pending_quantity + $updatePending[account_detail_sales_product_allow] WHERE product_id = $updatePending[product_id] AND sales_detail_id = $updatePending[sales_detail_id]");
					$this->db->query("UPDATE t_account_detail_sales_product SET account_detail_sales_product_connected = 1, pending_quantity = pending_quantity + $updatePending[account_detail_sales_product_allow], discount_price = discount_price + $discount[discount_total_price] WHERE product_id = $updatePending[product_id] AND sales_detail_id = $updatePending[sales_detail_id] AND account_detail_sales_product_id = $updatePending[account_detail_sales_product_id]");*/
					$this->Model->check_fifo_alpha($updatePending['product_id']);
					$this->Model->check_fifo_beta($updatePending['product_id']);
					$this->Model->check_fifo_main($updatePending['product_id'], $updatePending['seller_id']);
					$this->Model->set_sales_detail();
				}
		}
		/*$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod = MOD(sales_discount_terms_mod, sales_discount_terms_quantity) WHERE sales_discount_terms_type = 1 AND sales_discount_terms_mod > sales_discount_terms_quantity");
		$this->db->query("UPDATE t_sales_discount_terms SET sales_discount_terms_mod = MOD(sales_discount_terms_mod, sales_discount_terms_price) WHERE sales_discount_terms_type = 2 AND sales_discount_terms_mod > sales_discount_terms_price");
		$sisaTemps = $this->db->query("SELECT * FROM t_coa_transaction_temp WHERE coa_transaction_source <= 3 GROUP BY coa_transaction_source_id")->result_array();
		if (count($sisaTemps) > 0) {
			$totals = array();
			$sales_id = array();
			foreach ($sisaTemps as $index => $sisaTemp) {
				$account_sales = $this->db->get_where('t_account_detail_sales', ['account_detail_real_id' => $sisaTemp['coa_transaction_source_id']])->result_array();
				$account_detail = $this->db->get_where('t_account_detail', ['account_detail_real_id' => $sisaTemp['coa_transaction_source_id']])->row_array();
				$account_bank = $this->db->get_where('t_account_detail', ['account_detail_real_id' => ($sisaTemp['coa_transaction_source_id'] + 1)])->row_array();
				$this->db->delete('t_account_detail', ['account_detail_real_id' => $sisaTemp['coa_transaction_source_id']]);
				$this->db->delete('t_account_detail', ['account_detail_real_id' => ($sisaTemp['coa_transaction_source_id'] + 1)]);
				foreach ($account_sales as $indexx => $account_sale) {
					$sales_id[] = $account_sale['sales_id'];
					$this->db->delete('t_account_detail_sales', ['account_detail_sales_id' => $account_sale['account_detail_sales_id']]);
					$this->db->delete('t_account_detail_sales_product', ['account_detail_sales_id' => $account_sale['account_detail_sales_id']]);
				}
				$this->db->delete('t_coa_transaction_temp', ['coa_transaction_id' => $sisaTemp['coa_transaction_id']]);
				$this->db->delete('t_coa_transaction_header', ['coa_transaction_header_id' => $sisaTemp['coa_group_id']]);
				@$totals[$account_detail['account_id']] += $account_detail['account_detail_credit'];
				@$totals[$account_bank['account_id']] += $account_bank['account_detail_debit'];
			}
			if (count($sales_id) > 0) {
				$this->db->query("UPDATE t_account_detail A SET A.account_detail_paid = IFNULL((SELECT SUM(X.account_detail_sales_amount) FROM t_account_detail_sales X WHERE A.sales_id = X.sales_id), 0) WHERE A.sales_id IN(" . join(", ", $sales_id) . ")");
			}

			foreach ($totals as $index => $total) {
				if ($index !== '') {
					if ($index == 49 || $index == 50) {
						$this->db->query("UPDATE m_account SET account_debit = account_debit - $total WHERE account_id = $index");
					} else {
						$this->db->query("UPDATE m_account SET account_credit = account_credit - $total WHERE account_id = $index");
					}
				}
			}
		}*/
		// print_r($totals);
		// print_r($sisaTemps);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		redirect('M_account2/verification_transfer_seller2');
	}

	public function get_finish_rev()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['revisis'] = $this->db->query("SELECT A.*, SUM(C.coa_debit) as sum, IFNULL(SUM(C.coa_past_debit), 0) as past_sum, D.account_id, F.nama, F.kode FROM t_mistake A LEFT JOIN t_coa_transaction_mistake B ON B.mistake_id = A.mistake_id LEFT JOIN t_coa_transaction_temp C ON C.coa_transaction_id = B.coa_transaction_id AND C.coa_debit > 0 LEFT JOIN t_account_detail D ON D.account_detail_real_id = C.coa_transaction_source_id LEFT JOIN m_account E ON E.account_id = D.account_id LEFT JOIN member F ON F.kode = E.seller_id WHERE C.coa_transaction_source = 0 GROUP BY A.mistake_id")->result_array();
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_finish_verification', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_verif_rev()
	{
		$this->db->trans_begin();
		$mistake = $this->db->get_where('t_mistake', ['mistake_id' => $_POST['mistake_id']])->row_array();
		$mistake_type = $_POST['mistake_suspect_type'];
		if ($mistake_type == 1) {
			$account = $this->db->get_where('m_account', ['account_id' => $_POST['account_id']])->row_array();
			$member = $this->db->get_where('member', ['kode' => $account['seller_id']])->row_array();
			$t_mistake = ([
				'mistake_suspect_code' => $member['seller_id'],
				'mistake_suspect_name' => $member['account_name'],
				'mistake_suspect_type' => $mistake_type,
				'mistake_approved_user_id' => $_SESSION['user_id']
			]);
		} else if ($mistake_type == 2) {
			$account = $this->db->get_where('m_user', ['user_id' => $mistake['mistake_create_user_id']])->row_array();
			$t_mistake = ([
				'mistake_suspect_code' => $account['user_id'],
				'mistake_suspect_name' => $account['user_fullname'],
				'mistake_suspect_type' => $mistake_type,
				'mistake_approved_user_id' => $_SESSION['user_id']
			]);
		} else {
			$t_mistake = ([
				'mistake_suspect_code' => $_SESSION['user_id'],
				'mistake_suspect_name' => $_SESSION['user_fullname'],
				'mistake_suspect_type' => $mistake_type,
				'mistake_approved_user_id' => $_SESSION['user_id']
			]);
		}
		$t_mistake['mistake_note'] = $_POST['mistake_note'];
		$this->db->update('t_mistake', $t_mistake, ['mistake_id' => $_POST['mistake_id']]);

		$mistake_coas = $this->db->get_where('t_coa_transaction_mistake', ['mistake_id' => $mistake['mistake_id']])->result_array();

		foreach ($mistake_coas as $mistake_coa) {
			$data['coa_transaction_source'] = 1;
			$this->db->update('t_coa_transaction_temp', $data, ['coa_transaction_id' => $mistake_coa['coa_transaction_id']]);
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('M_account2/get_finish_rev');
	}

	public function fifoProduct()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Incomplete Sales Data';
		$data['file'] = 'Data Sales';
		$data['content'] = $this->load->view($this->url_ . '/v_fifo_product', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function getFifoProduct()
	{
		$datacontent['datatable'] = $this->Model->getFifoProduct();
	}
}
