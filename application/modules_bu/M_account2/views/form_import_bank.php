<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">


      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            CETAK LAPORAN BARANG KELUAR HARIAN
          </h3>
        </div>
      </div>
      <div class="kt-portlet__body"> 

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/import_csv_bank'); ?>" method="Post" target="_blank" enctype="multipart/form-data">
          <div class="form-group">
            <label>Bank</label>
            <select name="coa_id" id="pilihBulan" ng-model="coa_id" class="form-control">
				<option></option>
        <?php foreach($coas as $coa){ ?>
				<option value="<?= $coa['id'] ?>"><?= $coa['nama'] ?></option>
        <?php } ?>
			</select> 
          </div>
		  <div class="form-group">
            <label>File CSV</label>
            <input type="file" name="csv" accept="csv" required/>
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <button type="submit" class="btn btn-primary waves-effect waves-light " title="Cetak Laporan">
                <i class="flaticon2-print"></i><span class="m-l-10">Cetak Laporan</span>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>


</div>