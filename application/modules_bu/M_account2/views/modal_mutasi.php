<form id="formID" action="<?= base_url() ?>M_account2/set_verification" method="POST" enctype="multipart/form-data" onsubmit="return confirm('Are you sure?')">
    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
        <thead>
            <tr>
                <th>Rekening Tujuan</th>
                <th>Tanggal</th>
                <th>Nama Penginput</th>
                <th>Nama Pengirim</th>
                <th>Bukti Transfer</th>
                <th>Catatan</th>
                <th>Nominal (sales)</th>
                <!-- <th>Verif Nominal</th> -->
                <th>Verification</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mutasis as $key => $mutasi) { ?>
                <input type="hidden" name="input[<?= $key ?>][source_id]" value="<?= $mutasi['account_detail_real_id'] ?>" />
                <tr>
                    <td><input type="hidden" value="<?= $mutasi['account_name'] ?>" name="input[<?= $key ?>][account_name]" /><?= $mutasi['account_name'] ?></td>
                    <td><input type="hidden" value="<?= $mutasi['account_detail_date'] ?>" name="input[<?= $key ?>][account_detail_date]" /><?= $mutasi['account_detail_date'] ?></td>
                    <td><input type="hidden" value="<?= $mutasi['user_name'] ?>" /><?= $mutasi['user_name'] ?></td>
                    <td><?= $mutasi['account_detail_transfer_name'] ?></td>
                    <!-- <td><a href="<?= site_url() . $mutasi['proof'] ?>" target="_blank"><img src="<?= site_url() . $mutasi['proof'] ?>" style="max-width: 90%;" /></a></td> -->
                    <td>
                        <a href="<?= site_url() . $mutasi['proof'] ?>" data-lightbox="Image" data-title="<?= $mutasi['account_name'] ?>">
                            <img src="<?= site_url() . $mutasi['proof'] ?>" style="max-width: 90%;" />
                        </a>
                    </td>
                    <td>
                        <textarea name="input[<?= $key ?>][account_detail_note_accounting]" style="height:200px;" class="form-control" disabled><?= $mutasi['account_detail_note_accounting'] ?></textarea>
                    </td>
                    <td><input type="hidden" value="<?= $mutasi['account_detail_debit'] ?>" name="input[<?= $key ?>][account_detail_debit]" /><?= number_format($mutasi['account_detail_debit']) ?></td>
                    <!-- <td><input type="text" name="input[<?= $key ?>][nominal]" class="form-control numeric" value="<?= $mutasi['account_detail_debit'] ?>" /></td> -->
                    <td><input type="checkbox" name="input[<?= $key ?>][is_verification]" required></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
	<input type="hidden" name="id" value="<?= $id; ?>"> 
    <button type="submit" name="verif" class="btn btn-success waves-effect" value="approve">Verification</button>
	<button type="submit" name="reject" class="btn btn-danger waves-effect" value="reject">Reject</button>
</form>

<script>
    lightbox.option({
        'wrapAround': true,
        'albumLabel': '<?= $mutasi['account_detail_date'] ?>',
        'width': 900
    })
    $('button[type="submit"]').on('click', function() {
        this.disabled = true;
        this.value = 'Submitting...';
        this.disabled = false;
    });
    $(".numeric").mask("#,##0", {
        reverse: true
    });
</script>