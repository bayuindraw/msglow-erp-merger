<div class="kt-portlet">
	<div class="kt-portlet__body  kt-portlet__body--fit">
		<div class="row row-no-padding row-col-separator-lg">
			<div class="col-md-12 col-lg-6 col-xl-6">

				<!--begin::Total Profit-->
				<div class="kt-widget24">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Total Sudah Cek
							</h4>
						</div>
						<span class="kt-widget24__stats kt-font-brand">
							<?= $sudah_real ?>
						</span>
					</div>
				</div>

				<!--end::Total Profit-->
			</div>
			<div class="col-md-12 col-lg-6 col-xl-6">

				<!--begin::New Feedbacks-->
				<div class="kt-widget24">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Total Belum Cek
							</h4>
						</div>
						<span class="kt-widget24__stats kt-font-warning">
							<?= $belum_real ?>
						</span>
					</div>
				</div>

				<!--end::New Feedbacks-->
			</div>
		</div>
	</div>
</div>
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label" style="">
			<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
			<h3 class="kt-portlet__head-title">
				Verifikasi 2 Pembayaran Seller
			</h3>
			<?php if($rejecttotal == 0){ ?> 
				<?php if (date('H') >= 15) { ?>
					<a class="btn btn-success" href="<?= base_url() ?>M_account2/set_all_verif" style="position: absolute; right: 0; margin-right: 20px" onclick="return confirm('Are You Sure ?')">Verif Semua</a>
				<?php } ?>
				<a class="btn btn-success" href="<?= base_url() ?>M_account2/set_all_verif" style="position: absolute; right: 0; margin-right: 20px" onclick="return confirm('Are You Sure ?')">Verif Semua</a>
			<?php } ?>
		</div>
	</div>
	<div class="kt-portlet__body">
		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Seller</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<!--end: Datatable -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",
			scrollY: "300px",
			scrollX: true,
			scrollCollapse: true,
			"processing": true,
			"serverSide": true,
			"ajax": "<?= site_url() ?>/<?= $url ?>/get_data_verification_transfer_seller3"
		});
	});
</script>

<div class="modal fade" id="modal-paket" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">BUKTI TRANSFER</h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-form" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document" style="width: 90%;max-width:1200px;">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">SAMBUNGKAN KE MUTASI</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="loading" class="row">
					<div class="col-12">
						<div class="d-flex justify-content-center">
							<div class="spinner-border" role="status">
								<span class="sr-only">Loading...</span>
							</div>
						</div>
					</div>
				</div>
				<div id="data"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function showKartuStokA(file) {
		$("#modal-paket").modal('show');
		htmx = '<img src="<?= site_url() . "upload/TRANSFER/" ?>' + file + '" width="100%">';
		$(".modal-body").html(htmx);
	}

	function showKartuForm(file) {
		$("#data").empty()
		$("#loading").show()
		$("#modal-form").modal('show');
		$.ajax({
			type: "POST",

			url: "<?php echo base_url() ?>M_account2/get_mutasi2/" + file,
			cache: false,
			success: function(msg) {
				$("#loading").hide()
				$("#data").html(msg);
				xx();
			}
		});
	}

	function xx() {
		$('.ccc').select2({
			allowClear: true,
			placeholder: 'Pilih Mutasi',
		});
	}
</script>