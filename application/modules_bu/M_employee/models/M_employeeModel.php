<?php
defined('BASEPATH') or exit('No direct script access allowed');
class M_employeeModel extends CI_Model
{
	
	var $table_ = "m_employee";
	var $id_ = "employee_id";
	var $eng_ = "employee";
	var $url_ = "M_employee";
	
	function get()
	{
		$data = $this->db->get($this->table_);
		return $data;
	}
	
	function get_data()
	{
		$table = $this->table_;
		$id = $this->id_;
		$field = array('employee_name', 'employee_address', 'employee_phone', 'employee_email');
		$url = $this->url_;
		$action = '<a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . '/hapus/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
					  'recordsTotal' => (int)$jum_all['jum'],
					  'recordsFiltered' => (int)$jum_filter['jum'],
					  'data' => @$dataxy
					  );
		
		echo json_encode($data);
	}
	
	function get_max_id()
	{
		$data = $this->db->query("SELECT (IFNULL(MAX(".$this->id2_."), 0) + 1) as id FROM ".$this->table2_)->row_array();
		return $data['id'];
	}
	
	function update_balance($id, $balance, $type, $account_type = '', $monthly = '')
	{
		if($type == "credit"){
			if($monthly == ''){
				$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_credit = (".$this->eng_."_credit + $balance) WHERE ".$this->id_." = ".$id);
			}else{
				if($account_type == '1'){ 
					$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_debit = (".$this->eng_."_debit - ($balance)), ".$this->eng_."_monthly_debit = (".$this->eng_."_monthly_debit - ($balance)) WHERE ".$this->id_." = ".$id);
					$this->db->query("UPDATE ".$this->table_."_reset SET account_reset_debit = (account_reset_debit - ($balance)) WHERE account_reset_date > '$monthly'");
				}else{
					$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_credit = (".$this->eng_."_credit + $balance), ".$this->eng_."_monthly_credit = (".$this->eng_."_monthly_credit + $balance) WHERE ".$this->id_." = ".$id);
					$this->db->query("UPDATE ".$this->table_."_reset SET account_reset_credit = (account_reset_credit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}else if($type == "debit"){
			if($monthly == ''){
				$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_debit = (".$this->eng_."_debit + $balance) WHERE ".$this->id_." = ".$id);
			}else{
				if($account_type == '2'){
					$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_credit = (".$this->eng_."_credit - ($balance)), ".$this->eng_."_monthly_credit = (".$this->eng_."_monthly_credit - ($balance)) WHERE ".$this->id_." = ".$id);
					$this->db->query("UPDATE ".$this->table_."_reset SET account_reset_credit = (account_reset_credit - ($balance)) WHERE account_reset_date > '$monthly'");
				}else{
					$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_debit = (".$this->eng_."_debit + $balance), ".$this->eng_."_monthly_debit = (".$this->eng_."_monthly_debit + $balance) WHERE ".$this->id_." = ".$id);
					$this->db->query("UPDATE ".$this->table_."_reset SET account_reset_debit = (account_reset_debit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}
		return '';
	}
	
	function get_list_account()
	{
		return $this->db->get($this->table_)->result();
	}
	
	function get_list_account_detail_category()
	{
		return $this->db->get('m_account_detail_category')->result();
	}
	
	function insert($data = array())
	{
		$this->db->insert($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function update($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($this->table_);
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
}
