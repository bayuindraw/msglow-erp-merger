<?php
if ($parameter == 'ubah' && $id != '') {
	//$this->db->where('A.id_kemasan', $id);
	$row = $this->Model->get($id);
}
?>
<div class="row"
								<div class="col-lg-12 col-xl-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Username</label>
													<input type="text" class="form-control" placeholder="Nama" name="input[username]" value="<?= @$row['username'] ?>" required>
												</div>
												
												<div id="plus2"><label>Line</label><select class="form-control" name="line_id" id="line_id" required><?php 
												echo '<option></option>';
												foreach($arr_role_list as $index => $value){
													echo '<option value="'.$value['id'].'" '.((@$row['level'].'|'.@$row['line']==$value['id'])?'selected':'').'>'.$value['value'].'</option>';
												}
												?></select></div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>