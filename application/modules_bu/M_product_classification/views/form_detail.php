<?php
?>
<div class="row">
								
<div class="col-lg-12 col-xl-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Detail Kemasan
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan_detail'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
												<div class="kt-portlet__body">
										
										<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>Nama</th>
                                <th>Type</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id="x">
								<?php
								$i = 0;
								foreach($arrdata as $value){ 
								$i++;
								?>
									<tr id="<?= $i; ?>">
										<td><input type="text" class="form-control" placeholder="Nama" name="input2[<?= $i ?>][product_classification_detail_name]" value="<?= @$value['product_classification_detail_name'] ?>" required></td>
										<td><input type="text" class="form-control" placeholder="Nama" name="input2[<?= $i ?>][product_classification_detail_type]" value="<?= @$value['product_classification_detail_type'] ?>" required></td> 
										<td><button type="button" onclick="myDeleteFunction('<?= $i ?>')"></button></td>
									</tr>
								<?php } ?>
                            </tbody>
                         </table>
						 <button type="button" onclick="myCreateFunction()">Create row</button>
						 </div>
										
											
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>
<script>
it = <?= count($arrdata) ?>;
function myCreateFunction() {
	it++;
	var html = '<tr id="'+it+'"><td><input type="text" class="form-control" placeholder="Nama" name="input2['+it+'][product_classification_detail_name]" value="<?= @$row['product_classification_detail_name'] ?>" required></td><td><input type="text" class="form-control" placeholder="Nama" name="input2['+it+'][product_classification_detail_type]" value="<?= @$row['product_classification_detail_type'] ?>" required></td><td><button onclick="myDeleteFunction('+it+')"></button></td></tr>';
	$('#x').append(html);
}

function myDeleteFunction(id) {
	$('#'+id).remove();
}
</script>