<?php
defined('BASEPATH') or exit('No direct script access allowed');


class M_product_classification2 extends CI_Controller
{
	
	var $url_ = "M_product_classification2";
	var $id_ = "product_classification2_id";
	var $eng_ = "product_classification2";
	var $ind_ = "Klasifikasi Produk";
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_.'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}
	
	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_ ;
		if($id != ""){
			$datacontent['arrdata'] = $this->Model->get($id);
		}
		$data['content'] = $this->load->view($this->url_.'/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function form_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$datacontent['parameter'] = 'simpan';
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_ ;
		if($id != ""){
			$datacontent['arrdata'] = $this->Model->get_detail($id);
		}
		$data['content'] = $this->load->view($this->url_.'/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	
	public function get_data_report($search = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_report($search);
	}
	
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$exec = $this->Model->insert($data);
			} else {
				$this->Model->update($data, ['product_classification2_id' => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}
	
	public function simpan_detail()
	{
	
		if ($this->input->post('simpan')) {
			$id = $_POST['id'];
			$this->Model->delete_detail(["product_classification_id" => $id]);
			$data = $this->input->post('input2');
			foreach($data as $index => $value){
				$value['product_classification_id'] = $id;
				$this->Model->insert_detail($value);
			}
		}
		redirect(site_url($this->url_));
	}
}
