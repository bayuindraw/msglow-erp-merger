<div class="row">
  <div class="col-lg-12 col-xl-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            Form Input Klasifikasi Produk
          </h3>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-12 col-xl-12"> 
    <!--begin::Portlet-->
    <div class="kt-portlet">

      <div class="kt-portlet__body">

        <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan'); ?>" method="Post">
			<?= input_hidden('parameter', $parameter) ?>
			<?= input_hidden('id', @$arrdata['product_classification2_id']) ?>
          <div class="form-group">
            <label>Nama</label>
            <input type="text" id="cKodePo" name="input[product_classification2_name]" class="form-control md-form-control md-static" value="<?= @$arrdata['product_classification2_name'] ?>">
          </div>
          <div class="kt-portlet__foot">
            <div class="kt-form__actions">
              <input type="hidden" name="supplier" value="<?= $this->session->userdata('supplier') ?>">
              <button type="submit" name="simpan" class="btn btn-success waves-effect waves-light" value="simpan">
                SIMPAN
              </button>
              <a href="<?= site_url($url) ?>" class="btn btn-warning waves-effect waves-light">
                BATAL
              </a>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>



</div>
</div>
</div>