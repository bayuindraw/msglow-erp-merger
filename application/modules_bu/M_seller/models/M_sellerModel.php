<?php
defined('BASEPATH') or exit('No direct script access allowed');
class M_sellerModel extends CI_Model
{
	
	var $table_ = "m_seller";
	var $id_ = "seller_id";
	var $eng_ = "seller";
	var $url_ = "M_seller";
	
	function get()
	{
		$data = $this->db->get($this->table_);
		return $data;
	}
	
	function get_data()
	{
		$table = $this->table_;
		$id = $this->id_;
		$field = array('seller_name', 'seller_address', 'seller_code');
		$url = $this->url_;
		$action = '<a href="'.site_url($url."/form/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . "/transfer/tambah/xid").'" class="btn btn-info"> <i class="fa fa-list"></i></a> <a href="'.site_url($url . "/send/tambah/xid").'" class="btn btn-info"> <i class="fa fa-download"></i></a> <a href="'.site_url($url . '/hapus/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer[$id], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
					  'recordsTotal' => (int)$jum_all['jum'],
					  'recordsFiltered' => (int)$jum_filter['jum'],
					  'data' => @$dataxy
					  );
		
		echo json_encode($data);
	}
	
	
	
	function get_detail($id)
	{
		$this->db->where($this->id_, $id);
		$this->db->join('m_account_detail_category B', 'B.account_detail_category_id = A.account_detail_category_id');
		$data = $this->db->get($this->table2_.' A');
		return $data;
	}
	
	function get_data_detail($idxx)
	{
		$table = $this->table2_.' A';
		$id = "CONCAT(account_id, '/', account_detail_id) AS id";
		$field = array('account_detail_category_name', 'account_detail_pic', 'account_detail_note', 'account_detail_debit', 'account_detail_credit', 'account_detail_realization');
		$arrjoin[] = 'LEFT JOIN m_account_detail_category B ON B.account_detail_category_id = A.account_detail_category_id';
		$url = $this->url_;
		$arrwhere[] = "account_id = $idxx";
		$action = '<a href="'.site_url($url."/form_detail/ubah/xid").'" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="'.site_url($url . '/hapus_detail/xid').'" class="btn btn-danger" onclick="return confirm(\'Hapus data?\')"> <i class="fa fa-trash"></i></a>';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '')?0:$_GET['start'];
		$length = (@$_GET['length'] == '')?10:$_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if(@$arrjoin != ""){
			foreach($arrjoin as $jkey => $jvalue){
				$arrjoin2[] = $jvalue;
			}
			$join = join('', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if(@$arrwhere2 != '') $where2 = 'WHERE '.join(' AND ',$arrwhere2);
		if(@$search != ""){
			foreach($field as $key => $value){
				$arrfield[] = "$value LIKE '%$search%'";
			}
			$arrwhere[] = '('.join(' OR ', $arrfield).')';
		}
		if(@$arrwhere != '') $where = 'WHERE '.join(' AND ',$arrwhere);
		foreach(@$_GET['order'] as $key2 => $value2){
			$arrorder[] = ($value2['column'] + 1).' '.$value2['dir'];
		}
		$order = 'ORDER BY '.join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT(account_detail_id) as jum FROM $table $join $where2")->row_array(); 
		$jum_filter = $this->db->query("SELECT COUNT(account_detail_id) as jum FROM $table $join $where")->row_array();  
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array(); 
		$i = $start;
		$dataxy = array();
		foreach($result as $keyr => $valuer){
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach($field as $keyfield){
				$datax[] = $valuer[$keyfield];
			}
			$datax[] = str_replace('xid', $valuer['id'], $action);
			$dataxy[] = $datax;
		}
		$data = array('draw' => $_GET['draw'],
					  'recordsTotal' => (int)$jum_all['jum'],
					  'recordsFiltered' => (int)$jum_filter['jum'],
					  'data' => @$dataxy
					  );
		
		echo json_encode($data);
	}
	
	function get_max_id()
	{
		$data = $this->db->query("SELECT (IFNULL(MAX(account_detail_id), 0) + 1) as id FROM t_account_detail")->row_array();
		return $data['id'];
	}
	
	function get_account_id($id)
	{
		$data = $this->db->query("SELECT account_id FROM m_account WHERE seller_id = '$id'")->row_array();
		return $data['account_id'];
	}
	
	function update_balance($id, $balance, $type, $account_type = '', $monthly = '')
	{
		if($type == "credit"){
			if($monthly == ''){
				$this->db->query("UPDATE m_account SET account_credit = (account_credit + $balance) WHERE account_id = ".$id);
			}else{
				if($account_type == '1'){ 
					$this->db->query("UPDATE m_account SET account_debit = (account_debit - ($balance)), account_monthly_debit = (account_monthly_debit - ($balance)) WHERE account_id = ".$id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit - ($balance)) WHERE account_reset_date > '$monthly'");
				}else{
					$this->db->query("UPDATE m_account SET account_credit = (account_credit + $balance), account_monthly_credit = (account_monthly_credit + $balance) WHERE account_id = ".$id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}else if($type == "debit"){
			if($monthly == ''){
				$this->db->query("UPDATE m_account SET account_debit = (account_debit + $balance) WHERE account_id = ".$id);
			}else{
				if($account_type == '2'){
					$this->db->query("UPDATE m_account SET account_credit = (account_credit - ($balance)), account_monthly_credit = (account_monthly_credit - ($balance)) WHERE account_id = ".$id);
					$this->db->query("UPDATE m_account_reset SET account_reset_credit = (account_reset_credit - ($balance)) WHERE account_reset_date > '$monthly'");
				}else{
					$this->db->query("UPDATE m_account SET account_debit = (account_debit + $balance), account_monthly_debit = (account_monthly_debit + $balance) WHERE account_id = ".$id);
					$this->db->query("UPDATE m_account_reset SET account_reset_debit = (account_reset_debit + ($balance)) WHERE account_reset_date > '$monthly'");
				}
			}
		}
		return '';
	}
	
	/*function update_monthly_balance($id, $balance, $type)
	{
		if($type == "credit"){
			$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."_monthly_credit = (".$this->eng_."monthly_credit + $balance) WHERE ".$this->id_." = ".$id);
		}else if($type == "debit"){
			$this->db->query("UPDATE ".$this->table_." SET ".$this->eng_."monthly_debit = (".$this->eng_."monthly_debit + $balance) WHERE ".$this->id_." = ".$id);
		}
		return '';
	}*/
	
	function get_list_account()
	{
		return $this->db->get('m_account')->result();
	}
	
	function get_list_account_detail_category()
	{
		return $this->db->get('m_account_detail_category')->result();
	}
	
	function insert($data = array())
	{
		$this->db->insert($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function insert_account($data = array())
	{
		$this->db->insert('m_account', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function insert_reset($data = array())
	{
		$this->db->insert($this->table_."_reset", $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function update($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update($this->table_, $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete($this->table_);
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function insert_detail($data = array())
	{
		$this->db->insert('t_account_detail', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses ditambahkan! </div>';
		$this->session->set_flashdata('info', $info);
	}
	
	function update_detail($data = array(), $where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->update('t_account_detail', $data);
		$info = '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-check"></i> Sukses!</h4> Data Sukses diubah! </div>';
		$this->session->set_flashdata('info', $info);
	}
	function delete_detail($where = array())
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->delete('t_account_detail');
		$info = '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <h4><i class="icon fa fa-trash"></i> Sukses!</h4> Data Sukses dihapus </div>';
		$this->session->set_flashdata('info', $info);
	}
}
