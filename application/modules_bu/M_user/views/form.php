<?php
$name = "";
$email = "";
$remember_token = "";
$password = "";
$role_id = "";
if ($parameter == 'ubah' && $id != '') {
	$this->db->where('user_id', $id);
	$row = $this->Model->get()->row_array();
	extract($row);
}
?>
<div class="row">
								<div class="col-lg-12 col-xl-6">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Username</label>
													<input type="text" class="form-control" placeholder="Username" name="input[user_name]" value="<?= @$user_name ?>" required>
												</div>
												<div class="form-group">
													<label>Nama Lengkap</label>
													<input type="text" class="form-control" placeholder="Nama Lengkap" name="input[user_fullname]" value="<?= @$user_fullname ?>" required>
												</div>
												<div class="form-group">
													<label>Email</label>
													<input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Email" name="input[user_email]" value="<?= @$user_email ?>" required>
												</div>
												<div class="form-group">
													<label>Role</label>
													<select class="form-control" id="exampleSelect1" name="input[role_id]" required>
														<option value="">Pilih Role</option>
														<?php
														foreach ($m_role as $data) { // Lakukan looping pada variabel siswa dari controller
															echo "<option value='" . $data->role_id . "'".(($data->role_id == $role_id)?'selected':'').">" . $data->role_name . "</option>";
														}
														?>
													</select>
												</div>
												
												<?php if($parameter != 'ubah' && $id == ''){ ?>
													<div class="form-group">
														<label for="exampleInputPassword1">Password</label>
														<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="input[user_password]">
													</div>
												<?php }else{ ?>
													<div class="form-group">
														<div class="kt-radio-list">
															<label class="kt-radio">
																<input type="checkbox" name="change_password" onclick="$('#divpassword').toggle()"> Ubah Password
																<span></span>
															</label>
														</div>
													</div>
													<div class="form-group" style="display:none;" id="divpassword">
														<label for="exampleInputPassword1">Password</label>
														<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="user_password">
													</div>
												<?php } ?>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>