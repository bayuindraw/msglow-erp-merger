<?php
defined('BASEPATH') or exit('No direct script access allowed');


class M_vendor extends CI_Controller
{

	var $url_ = "M_vendor";
	var $id_ = "vendor_id";
	var $eng_ = "vendor";
	var $ind_ = "Vendor";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {

				$from['account_detail_id'] = $id;
				$from['account_detail_debit'] = 0;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_id'] = $from['account_id'];
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($from['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}

				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_debit'] = $from['account_detail_credit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_debit'], 'debit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_send()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_credit'], 'credit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function form_detail($parameter = '', $id = '', $id_detail = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind2_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account_detail_category'] = $this->Model->get_list_account_detail_category();
		$datacontent['id'] = $id;
		$datacontent['id_detail'] = $id_detail;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$data[$this->eng_ . '_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert($data);
				$data2['vendor_id'] = $this->db->insert_id();
				$data2['account_code'] = '7.' . $data2['vendor_id'];
				$data2['account_name'] = 'Akun ' . $data[$this->eng_ . '_name'] . ' (vendor)';
				$data2['account_type_id'] = 1;
				$data2['account_date_create'] = $data[$this->eng_ . '_date_create'];
				$data2['account_date_reset'] = $data[$this->eng_ . '_date_create'];
				$exec = $this->Model->insert_account($data2);
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}
	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}

	public function simpan_detail()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$id = $this->input->post('id');
				$id2 = $this->Model->get_max_id();
				$data[$this->id_] = $id;
				$data[$this->id2_] = $id2;
				$data[$this->eng2_ . '_type_id'] = 2;
				$data[$this->eng2_ . '_user_id'] = $_SESSION['user_id'];
				if ($_POST['realisasi'] == "on") {
					$data[$this->eng2_ . '_realization'] = 1;
				} else {
					$data[$this->eng2_ . '_realization'] = 0;
				}
				if ($_POST['transaction_type'] == "debit") {
					$type = "debit";
					$data[$this->eng2_ . '_debit'] = $_POST['transaction_amount'];
				} else if ($_POST['transaction_type'] == "credit") {
					$type = "credit";
					$data[$this->eng2_ . '_credit'] = $_POST['transaction_amount'];
				}
				$data[$this->eng2_ . '_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($data);
				$this->db->where('account_id', $id);
				$row = $this->Model->get()->row_array();
				if (@$row['account_date_reset'] > $data[$this->eng2_ . '_date']) {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_ . '_date']);
				} else {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
				}
			} else {
				$id = $this->input->post('id');
				$data[$this->eng2_ . '_user_id'] = $_SESSION['user_id'];
				if ($_POST['realisasi'] == "on") {
					$data[$this->eng2_ . '_realization'] = 1;
				} else {
					$data[$this->eng2_ . '_realization'] = 0;
				}
				if ($_POST['transaction_type'] == "debit") {
					$type = "debit";
					$data[$this->eng2_ . '_debit'] = $_POST['transaction_amount'];
					$data[$this->eng2_ . '_credit'] = 0;
				} else if ($_POST['transaction_type'] == "credit") {
					$type = "credit";
					$data[$this->eng2_ . '_credit'] = $_POST['transaction_amount'];
					$data[$this->eng2_ . '_debit'] = 0;
				}
				$data[$this->eng2_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update_detail($data, [$this->id_ => $this->input->post('id'), $this->id2_ => $this->input->post('id_detail')]);
				$this->db->where('account_id', $id);
				$row = $this->Model->get()->row_array();
				if (@$row['account_date_reset'] > $this->input->post('transaction_date_last')) {
					$this->Model->update_balance($id,  -1 * ($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'), @$row['account_type_id'], $this->input->post('transaction_date_last'));
				} else {
					$this->Model->update_balance($id,  -1 * ($this->input->post('transaction_amount_last')), $this->input->post('transaction_type_last'));
				}
				if (@$row['account_date_reset'] > $data[$this->eng2_ . '_date']) {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type, @$row['account_type_id'], $data[$this->eng2_ . '_date']);
				} else {
					$this->Model->update_balance($id,  $_POST['transaction_amount'], $type);
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url2_ . "/" . $id));
	}
	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();


		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();

		if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
		} else {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
		}



		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_ . "/$id"));
	}
}
