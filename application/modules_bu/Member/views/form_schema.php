<div class="row">
	<div class="col-lg-12 col-xl-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Form Input Member
					</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12 col-xl-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">

			<div class="kt-portlet__body">

				<form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_schema'); ?>" method="Post">
					<?= input_hidden('id', $id) ?>
					<div class="kt-portlet__body">
						<div class="form-group">
													<label>Schema</label>
													<select class="form-control" id="select2schema" name="input[schema_id]" required>
														<option value="">Pilih Role</option>
														<?php
														foreach ($m_schema as $data) { // Lakukan looping pada variabel siswa dari controller
															echo "<option value='" . $data->schema_id . "'".(($data->schema_id == $arrdata['schema_id'])?'selected':'').">" . $data->schema_name . "</option>";
														}
														?>
													</select>
												</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
							<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>



</div>
</div>
</div>