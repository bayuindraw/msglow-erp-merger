<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="kt-portlet">
            <form method="post" action="<?= site_url($url . '/simpan_pay_deposit'); ?>" enctype="multipart/form-data">

                <?= input_hidden('id', $id) ?>
                <?= input_hidden('account_detail_real_id', $arraccount_detail['account_detail_real_id']) ?>
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Pembayaran Deposit
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">


                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <table>
                                    <br>
                                    <tr>
                                        <td>Sales Order</td>
                                        <td>&nbsp;&nbsp;:</td>
                                        <td>&nbsp;&nbsp;<?= @$arraccount_detail['sales_code']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Total Tagihan</td>
                                        <td>&nbsp;&nbsp;:</td>
                                        <td>&nbsp;&nbsp;Rp <?= @number_format(@$arraccount_detail['account_detail_debit']); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Total Terbayar</td>
                                        <td>&nbsp;&nbsp;:</td>
                                        <td>&nbsp;&nbsp;Rp <?= @number_format($arraccount_detail['account_detail_paid']); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kurang Bayar</td>
                                        <td>&nbsp;&nbsp;:</td>
                                        <td>&nbsp;&nbsp;Rp <?= @number_format(@$arraccount_detail['account_detail_debit'] - $arraccount_detail['account_detail_paid']); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Deposit Tersedia</td>
                                        <td>&nbsp;&nbsp;:</td>
                                        <td>&nbsp;&nbsp;Rp <?= @number_format(@$deposit); ?></td>
                                    </tr>
                                </table>
                                <br>
                            </h3>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ekspedisi:</label>
                            <select class="form-control kt-select2 select_ekspedisi" name="input2[ekspedisi]">
                                <option value=""></option>
                                <?php foreach ($deliveries as $delivery) { ?>
                                    <option value="<?= $delivery['delivery_instance_id'] ?>"><?= $delivery['delivery_instance_name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label>Provinsi:</label>
                            <select class="form-control kt-select2 select_provinsi" name="input2[provinsi]" id="select_provinsi" onchange="get_kota()" required>
                                <?php foreach ($provinsis as $provinsi) { ?>
                                    <option value="<?= $provinsi['REGION_ID'] ?>"><?= $provinsi['NAME'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label class="">Kota/Kabupaten:</label>
                            <select class="form-control kt-select2 select_kota" id="select_kota" onchange="get_kecamatan()" name="input2[kota]" required>

                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Kecamatan:</label>
                            <select class="form-control kt-select2 select_kecamatan" id="select_kecamatan" name="input2[kecamatan]" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Alamat Lengkap:</label>
                            <textarea class="form-control" name="input2[full_address]"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Bayar</label>
                        <input type="hidden" name="max" value="<?= @$arraccount_detail['account_detail_debit'] - $arraccount_detail['account_detail_paid']; ?>">
                        <?php if (($arraccount_detail['account_detail_debit'] - $arraccount_detail['account_detail_paid']) > $deposit) { ?>
                            <input type="text" class="form-control numeric" <?= $arraccount_detail['schema_id'] == 1 ? 'readonly' : '' ?> placeholder="Nominal" deposit="<?= @$deposit; ?>" max="<?= @$deposit; ?>" name="paid" value="0" id="paid" required onkeyup="chk_total2()">
                        <?php } else { ?>
                            <input type="text" class="form-control numeric" <?= $arraccount_detail['schema_id'] == 1 ? 'readonly' : '' ?> placeholder="Nominal" deposit="<?= @$arraccount_detail['account_detail_debit'] - $arraccount_detail['account_detail_paid']; ?>" max="<?= @$deposit; ?>" name="paid" value="0" id="paid" required onkeyup="chk_total2()">
                        <?php } ?>
                        <input type="hidden" id="total_all" value=0>
                        <input type="hidden" name="schema_id" value="<?= $arraccount_detail['schema_id'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="date" value="<?= date('Y-m-d'); ?>">
                    </div>
                    <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NAMA PRODUK</th>
                                <th>JUMLAH PESANAN</th>
                                <th>BOLEH KIRIM</th>
                                <th>KURANG BOLEH KIRIM</th>

                                <th>JUMLAH KIRIM</th>
                                <th>TOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0;
                            foreach ($arrsales_detail as $key => $vaData) {
                            ?>
                                <tr>
                                    <td><?= ++$no ?></td>
                                    <td><?= $vaData['nama_produk'] ?> (@Rp <?= number_format($vaData['sales_detail_price']) ?>)</td>
                                    <td><?= $vaData['sales_detail_quantity'] ?></td>
                                    <td><?= $vaData['send_allow'] ?></td>
                                    <td><?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] ?></td>


                                    <td>
                                        <input type="text" name="allow[<?= $key ?>]" price="<?= $vaData['sales_detail_price']; ?>" max="<?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] ?>" id_dex="<?= $key ?>" class="form-control md-static sum_total numeric" value="0" onkeyup="chk_total()">
                                        <input type="hidden" name="product_id[<?= $key ?>]" value="<?= $vaData['id_produk'] ?>">
                                        <input type="hidden" name="product_max[<?= $key ?>]" value="<?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] ?>">
                                        <input type="hidden" name="sales_detail_id[<?= $key ?>]" value="<?= $vaData['sales_detail_id'] ?>">
                                    </td>
                                    <td>
                                        <div id="<?= $key ?>"></div>
                                        <input class="total" id="total<?= $key ?>" type="hidden" value="0" />
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                </div>




                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>
                        <a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
                    </div>
                </div>
            </form>


        </div>
    </div>
</div>

<script>
    function chk_total(val) {
        var paid = $('#paid').val();
        var harga = paid.replaceAll(",", "");
        var price = 0;
        var total_price = 0;
        var total_price2 = 0;
        var dex = "";
        var val_temp = "";
        $('.sum_total').each(function() {
            val_temp = $(this).val().replaceAll(",", "");
            if (val_temp == "") {
                val_temp = 0;
            }
            price = $(this).attr("price") * parseFloat(val_temp);
            total_price = total_price + price;
            dex = $(this).attr("id_dex");
            if (parseFloat(val_temp) > parseFloat($(this).attr("max"))) {
                $(this).val($(this).attr("max"));
                $('#' + dex).html('Rp ' + commafy($(this).attr("max") * $(this).attr("price")));
                $('#total' + dex).val($(this).attr("max") * $(this).attr("price"));
                total_price2 += parseFloat($(this).attr("max") * $(this).attr("price"));

            } else {
                $('#' + dex).html('Rp ' + commafy(total_price));
                $('#total' + dex).val(total_price);
                total_price2 += parseFloat(total_price);
            }
            total_price = "";
            dex = "";
            /*if(parseFloat($(this).val()) > parseFloat($(this).attr("max"))){
            	$(this).val($(this).attr("max"));
            }*/

        });
        $('#total_all').val(total_price2);
    }

    function chk_total2() {
        var paid = $('#paid').val();
        var total_all = $('#total_all').val();
        paid = paid.replaceAll(",", "");
        if (parseFloat($('#paid').attr("max")) > parseFloat($('#paid').attr("deposit"))) {
            var max = $('#paid').attr("deposit");
        } else {
            var max = $('#paid').attr("max");
        }
        if (parseFloat(paid) > parseFloat(max)) {
            $('#paid').val(commafy(max));
        }
        var paid2 = $('#paid').val();
        paid2 = paid2.replaceAll(",", "");
    }

    function get_kota() {
        var val = $('#select_provinsi option:selected').val();

        $.ajax({
            url: '<?= base_url() ?>M_account/get_kota/' + val.substring(0, 2),
            success: function(result) {
                $('#select_kota').html(result);
            }
        });
        $('#select_kecamatan').html('');
    }

    function get_kecamatan() {
        var val = $('#select_kota option:selected').val();
        $.ajax({
            url: '<?= base_url() ?>M_account/get_kecamatan/' + val.substring(0, 4),
            success: function(result) {
                $('#select_kecamatan').html(result);
            }
        });
    }

    $('.select_provinsi').select2({
        allowClear: true,
        placeholder: 'Pilih Provinsi',
    });
    $('.select_ekspedisi').select2({
        allowClear: true,
        placeholder: 'Pilih Ekspedisi',
    });
    $('.select_kota').select2({
        allowClear: true,
        placeholder: 'Pilih Kota/kabupaten',
    });
    $('.select_kecamatan').select2({
        allowClear: true,
        placeholder: 'Pilih Kota/kecamatan',
    });
</script>