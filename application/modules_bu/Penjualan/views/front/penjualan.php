
<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Subheader -->
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container kt-container--fluid">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title"><?=$file?></h3>
                <span class="kt-subheader__separator kt-hidden"></span>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        <?=$link?>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <div dir id="dir" content="table">
                    <table id="multi-colum-dt" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Seller</th>
                                <th>Nama Seller</th>
                                <th>Alamat Pengiriman</th>
                                <th>Telepon</th>
                                <th>Status Seller</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
                        		$no = 0;
                        		foreach ($seller as $key => $vaSeller) {
                        	?>
                        	<tr>
                        		<td><?=++$no?></td>
                        		<td><?=$vaSeller['kode']?></td>
                        		<td><?=$vaSeller['nama']?></td>
                                <?php 
                                    $alamatKirim = '';
                                    $query = $this->model->code("SELECT * FROM member_alamat WHERE kode = '".$vaSeller['kode']."' ORDER BY id_kirim ASC");
                                    foreach ($query as $key => $vaData) {
                                       $alamatKirim = $vaData['alamat'];
                                    }

                                ?>
                        		<td><?=$alamatKirim?></td>
                        		<td><?=$vaSeller['telepon']?></td>
                        		<td><?=$vaSeller['status']?></td>
                        		<td> 
                        			<a href="<?=base_url()?>Penjualan/edit_seller/<?=$vaSeller['kode']?>" class="btn btn-primary"><i class="fa fa-edit" style="margin-right: -5px;"></i></a>
                        			<a href="#" class="btn btn-warning"><i class="fa fa-tasks" style="margin-right: -5px;"></i></a>
                        			<a href="#" class="btn btn-success"><i class="fa fa-file-invoice-dollar" style="margin-right: -5px;"></i></a>
                        		</td>
                        	</tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <!--end: Datatable -->
            </div>
        </div>
    </div>

    <!-- end:: Content -->
</div>
