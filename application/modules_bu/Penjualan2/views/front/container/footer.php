<!-- begin:: Footer -->
					<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
						<div class="kt-container  kt-container--fluid ">
							<div class="kt-footer__copyright">
								
							</div>
							<div class="kt-footer__menu">
								2020&nbsp;&copy;&nbsp;<a href="#" target="_blank" class="kt-link">IT MSGLOW</a>
						</div>
					</div>

					<!-- end:: Footer -->
				</div>
			</div>
		</div>

		<!-- end:: Page -->


		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		<script>
			$(document).ready( function () {
				$('#mytable_barang').DataTable();
				$('#mytable_kemasan').DataTable();
			} ); 
		</script>
		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": [
							"#c5cbe3",
							"#a1a8c3",
							"#3d4465",
							"#3e4466"
						],
						"shape": [
							"#f0f3ff",
							"#d9dffa",
							"#afb4d4",
							"#646c9a"
						]
					}
				}
			};
		</script>
		<script type="text/javascript">
	$('#multi-colum-dt').DataTable({
        pageLength: 100,
        columnDefs: [{
            targets: [0],
            orderData: [0, 1]
        }, {
            targets: [1],
            orderData: [1, 0]
        },]
    });
    /*$('.4IDE-date').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true,
            format : 'DD-MM-YYYY'
        });*/
	$('#pilihKategori').select2({  
     allowClear: true,
     placeholder: 'Pilih Kategori',
    });
   	$('#cIdStock').select2({  
	   allowClear: true,
	   placeholder: 'Pilih Nama Stock Barang',
	  });
   	$('#pilihSupplier').select2({  
	   allowClear: true,
	   placeholder: 'Pilih Supplier',
	  });
    $('#pilihPabrik').select2({  
       allowClear: true,
       placeholder: 'Pilih Pabrik',
      });
    $('#pilihBulan').select2({  
       allowClear: true,
       placeholder: 'Pilih Bulan',
      });
    $('#pilihTahun').select2({  
       allowClear: true,
       placeholder: 'Pilih Tahun',
      });
    $('#pilihBulanDua').select2({  
       allowClear: true,
       placeholder: 'Pilih Bulan',
      });
    $('#pilihTahunDua').select2({  
       allowClear: true,
       placeholder: 'Pilih Tahun',
      });
    $('.pilihbulan').select2({  
       allowClear: true,
       placeholder: 'Pilih Bulan',
      });
    $('.pilihtahun').select2({  
       allowClear: true,
       placeholder: 'Pilih Tahun',
      });
    $('#pilihSeller').select2({  
       allowClear: true,
       placeholder: 'Pilih Seller Msglow',
      });
    $('#pilihrekening').select2({  
     allowClear: true,
     placeholder: 'Pilih Rekening Penerimaan',
    });
    $('#pilihproduk').select2({  
     allowClear: true,
     placeholder: 'Pilih Produk',
    });
	$('#pilihbahanbaku').select2({  
     allowClear: true,
     placeholder: 'Pilih Bahan Baku',
    });
	$('#pilihklasifikasi').select2({  
     allowClear: true,
     placeholder: 'Pilih Klasifikasi',
    });
    $('#pilihprodukJL').select2({  
     allowClear: true,
     placeholder: 'Pilih Produk',
    });
    
</script>
		<!-- plugin-->
		
		<script src="<?=base_url()?>web/plugins/general/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="<?=base_url()?>web/plugins/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		
		<script src="<?=base_url()?>web/plugins/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?=base_url()?>web/plugins/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>web/plugins/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="<?=base_url()?>web/plugins/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		
		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<!--<script src="<?=base_url()?>web/plugins/global/plugins.bundle.js" type="text/javascript"></script>-->
			
		<!--<script src="<?=base_url()?>web/js/pages/crud/datatables/basic/paginations.js" type="text/javascript"></script>-->
		<script src="<?=base_url()?>web/js/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		<script src="<?=base_url()?>web/plugins/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
		<script src="<?=base_url()?>web/plugins/custom/gmaps/gmaps.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<script src="<?=base_url()?>web/js/pages/dashboard.js" type="text/javascript"></script>
		
		
		<!-- widget-->
		<script src="<?=base_url()?>web/js/pages/crud/forms/widgets/summernote.js" type="text/javascript"></script>
		<script src="<?=base_url()?>web/js/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
		
		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>