<?php
if ($parameter == 'ubah' && $id != '') {
	//$this->db->where('A.id_kemasan', $id);
	$row = $this->Model->get($id)->row_array();
}
?>
<div class="row"
								<div class="col-lg-12 col-xl-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Nama</label>
													<input type="text" class="form-control" placeholder="Nama" name="input[nama_kemasan]" value="<?= @$row['nama_kemasan'] ?>" required>
												</div>
												<div class="form-group">
													<label>Klasifikasi</label>
													<select name="input[product_classification2_id]" id="cIdStock" class="form-control md-static" required><option></option><?= $product_list ?></select>
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>