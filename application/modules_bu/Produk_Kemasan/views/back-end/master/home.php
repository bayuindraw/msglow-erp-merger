<div class="kt-portlet kt-portlet--mobile">
  <div class="kt-portlet__head kt-portlet__head--lg">


    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
          <a href="<?= base_url() ?>Produk_Kemasan/Tambah" class="btn btn-brand btn-elevate btn-icon-sm" style="margin-top: 15px;">
            <i class="la la-plus"></i>
            TAMBAH
          </a>
          <a href="<?= base_url() ?>Produk_Kemasan/export_pdf<?php if (isset($_GET['dari_tanggal']) && isset($_GET['sampai_tanggal'])) {
                                                                echo "?dari_tanggal=" . $_GET['dari_tanggal'] . "&" . "sampai_tanggal=" . $_GET['sampai_tanggal'];
                                                              } ?>" class="btn btn-brand btn-danger btn-icon-sm" style="margin-top: 15px;">
            <i class="la la-print"></i>
            CETAK
          </a>
          <hr>
          <form class="mt-3 mb-3" action="" method="get">
            <div class="row">
              <div class="col-md-4">
                Dari
                <input type="date" name="dari_tanggal" id="dari_tanggal" value="<?php if (isset($_GET['dari_tanggal'])) {
                                                                                  echo $_GET['dari_tanggal'];
                                                                                } ?>" class="form-control" required="">
              </div>
              <div class="col-md-4">
                Sampai
                <input type="date" name="sampai_tanggal" id="sampai_tanggal" value="<?php if (isset($_GET['sampai_tanggal'])) {
                                                                                      echo $_GET['sampai_tanggal'];
                                                                                    } ?>" class="form-control" required="">
              </div>
              <div class="col-md-4">
                <button type="submit" class="btn btn-success waves-effect mt-4"><i class="la la-history"></i>HISTORY</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">

    <!--begin: Datatable -->
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="mytable_barang">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Barang</th>
          <th>QTY (PCS)</th>
          <th>Supplier</th>
          <th>Keterangan</th>
          <th>Opsi</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no = 1;
        foreach ($data as $key) {
        ?>
          <tr>
            <td width="10px"><?= $no++ ?></td>
            <td><?= $key->nama_barang ?></td>
            <td><?= $key->qty ?></td>
            <td><?= $key->supplier ?></td>
            <td><?= $key->keterangan ?></td>
            <td width="130px">
              <a href="<?= site_url('Produk_Kemasan/Edit') ?>?no=<?= $key->no ?>" class="btn btn-warning waves-effect"><i class="la la-edit"></i></a>
              <a href="<?= site_url('Produk_Kemasan/Hapus') ?>?no=<?= $key->no ?>" class="btn btn-danger waves-effect" onclick="return confirm('Anda yakin ingin menghapus data?')"><i class="la la-trash"></i></a>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>

    <!--end: Datatable -->
  </div>
</div>