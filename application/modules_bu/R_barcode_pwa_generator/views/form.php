
<div class="row"
								<div class="col-lg-12 col-xl-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post"  target="print_popup" action="<?= site_url($url . '/print'); ?>" enctype="multipart/form-data" onsubmit="window.open(<?= site_url($url . '/print'); ?>,'print_popup','width=1000,height=800');">
										
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Jumlah</label>
													<input type="number" class="form-control" placeholder="Jumlah" name="jumlah" value="1" required>
												</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="Print" id="print" value="print" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>