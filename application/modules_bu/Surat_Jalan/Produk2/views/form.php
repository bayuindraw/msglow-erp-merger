<?php
if ($parameter == 'ubah' && $id != '') {
	//$this->db->where('A.id_kemasan', $id);
	$row = $this->Model->get($id)->row_array();
}
?>
<div class="row"
								<div class="col-lg-12 col-xl-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
											<?= input_hidden('parameter', $parameter) ?>
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>Nama</label>
													<input type="text" class="form-control" placeholder="Nama" name="input[nama_produk]" value="<?= @$row['nama_produk'] ?>" required>
												</div>
												<div class="form-group">
													<label>Kode</label>
													<input type="text" class="form-control" placeholder="Kode" name="input[kode_produk]" value="<?= @$row['kode_produk'] ?>" required>
												</div>
												<div class="form-group">
													<label>Klasifikasi</label>
													<input type="text" class="form-control" placeholder="Klasifikasi" name="input[klasifikasi]" value="<?= @$row['klasifikasi'] ?>" required>
												</div>
												<?php if($id == ""){ ?>
												<div class="form-group">
													<label>Stok Awal</label>
													<input type="text" class="form-control" placeholder="Stok Awal" name="stock_awal" value="0" required>
												</div>
												<?php } ?>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>