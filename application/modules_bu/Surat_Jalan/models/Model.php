<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	// Syarat  :  
	
	// 1 . Select  = View
	// 2 . Insert  = Ins
	// 3 . Update  = Updt
	// 4 . Delete  = Del

class Model extends CI_Model {

	public function tampil_barang() {
		return $Query = $this->db->query("SELECT * FROM tb_suratjalanbarang")->result();	
	}
	public function tampil_kemasan(){
		return $Query = $this->db->query("SELECT * FROM tb_suratjalankemasan")->result();	
	}
	
	public function tambah($Table,$Value){
		$Query = $this->db->insert($Table,$Value);
		return $Query ;
	}

	public function edit($Table,$Where,$WhereValue,$Value){
		$this->db->where($Where,$WhereValue);
		$this->db->update($Table,$Value);
	}

	public function hapus($Table,$Where,$WhereValue){
		$this->db->where($Where,$WhereValue);
		$this->db->delete($Table);
	}

	
}