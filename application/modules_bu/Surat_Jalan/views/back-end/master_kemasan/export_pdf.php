<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="shortcut icon" href="https://msglow.app/upload/nitrogen.png" />
   <title>Print Surat Jalan Kemasan</title>
   <img src="<?php echo base_url('assets/1.PNG'); ?>" width="100px" height="100px">

   <style>
      table,
      th,
      td {
         border: 1px solid black;
         border-collapse: collapse;
      }

      table {
         width: 100%;
      }

      .right {
         text-align: right;
      }

      td {
         text-align: center;
      }
   </style>

</head>

<body id="ignorePDF">
   <center>
      <h1>MS GLOW</h1>
   </center>
   <h4>TANGGAL : <?= date('d F Y'); ?></h4>
   <h4 style="margin: 0px; text-align: left"><span style="margin-right: 30px;">Nomor : -SJ.KEM.KEL/<?php echo date("m"); ?>/<?php echo date("y"); ?></h3></span>
      <h4>NAMA : PT KOSMETIKA GLOBAL INDONESIA</h4>
      <p><b style="float:right;margin-top: -50px;"><?= date('H:i A'); ?></b></p>
      <table>
         <tr>
            <th>NO</th>
            <th>JUMLAH (pcs)</th>
            <th>NAMA</th>
            <th>JUMLAH KOLI</th>
         </tr>

         <tr>
            <?php
            $no = 1;
            foreach ($data as $key) {
            ?>
         <tr>
            <td width="10px"><?= $no++ ?></td>
            <td><?= $key->jumlah ?></td>
            <td style="text-align: left;"><?= $key->nama ?></td>
            <td><?= $key->jumlah_koli ?></td>
         </tr>
      <?php } ?>
      </tr>
      </table>
      <h4>CATATAN:</h4>
      <h4>- DIAMBIL DRIVER KOSME</h4>
      <br>
      <br>
      <br>
      <div class="row clearfix">
         <div class="ttd2" style="float:left;margin-top: -10px;">
            <p><b>
                  <center>HORMAT KAMI</center>
               </b></p>
            <br><br><br>
            <p style="font-weight: bold;">_________________</p>
         </div>
         <div class="ttd" style="float:right;margin-top: -10px;">
            <p><b>
                  <center>PENERIMA </center>
               </b></p>
            <br><br><br>
            <p style="font-weight: bold;">_________________</p>

            <img src="<?php echo base_url('assets/2.PNG'); ?>" width="100px" height="100px" style="float: right;">

         </div>
</body>
<script>
   var doc = new jsPDF();
   var elementHandler = {
      '#ignorePDF': function(element, renderer) {
         return true;
      }
   };
   var source = window.document.getElementsByTagName("body")[0];
   doc.fromHTML(
      source,
      15,
      15, {
         'width': 180,
         'elementHandlers': elementHandler
      });

   doc.output("dataurlnewwindow");
</script>

</html>