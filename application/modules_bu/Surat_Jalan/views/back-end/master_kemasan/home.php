<div class="kt-portlet kt-portlet--mobile">
  <div class="kt-portlet__head kt-portlet__head--lg">


    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <div class="kt-portlet__head-actions">
          <a href="<?= base_url() ?>Surat_Jalan/Surat_Jalan_Kemasan/Tambah" class="btn btn-brand btn-elevate btn-icon-sm" style="margin-top: 15px;">
            <i class="la la-plus"></i>
            TAMBAH
          </a>
          <a href="<?= base_url() ?>Surat_Jalan/Surat_Jalan_Kemasan/export_pdf" class="btn btn-brand btn-danger btn-icon-sm" style="margin-top: 15px;">
            <i class="la la-print"></i>
            CETAK
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="kt-portlet__body">

    <!--begin: Datatable -->
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="mytable_barang">
      <thead>
        <tr>
          <th>No</th>
          <th>Jumlah(pcs)</th>
          <th>Nama</th>
          <th>Jumlah Koli</th>
          <th>Opsi</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no = 1;
        foreach ($data as $key) {
        ?>
          <tr>
            <td width="10px"><?= $no++ ?></td>
            <td><?= $key->jumlah ?></td>
            <td><?= $key->nama ?></td>
            <td><?= $key->jumlah_koli ?></td>
            <td width="130px">
              <a href="<?= site_url('Surat_Jalan/Surat_Jalan_Kemasan/Edit') ?>?no=<?= $key->no ?>" class="btn btn-warning waves-effect"><i class="la la-edit"></i></a>
              <a href="<?= site_url('Surat_Jalan/Surat_Jalan_Kemasan/Hapus') ?>?no=<?= $key->no ?>" class="btn btn-danger waves-effect" onclick="return confirm('Anda yakin ingin menghapus data?')"><i class="la la-trash"></i></a>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>

    <!--end: Datatable -->
  </div>
</div>