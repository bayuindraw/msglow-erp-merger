<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Form Input Data Barang
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_detail'); ?>" method="Post">
						<?= input_hidden('id', $id) ?>
					<div class="form-group">
						<label>Produk</label>
						<select name="input[product_id]" id="cIdStock" class="form-control static">
			
							<option></option>
							<?php
							foreach ($arrproduct as $key => $value) {
							?>
								<option value="<?= $value['id_produk'] ?>"><?= $value['nama_produk'] ?></option>
							<?php } ?>
						</select>
					</div>
                    <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" id="cJumlah" name="input[sales_detail_quantity]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static">
                    </div>
					<div class="form-group">
                        <label>Harga</label>
                        <input type="text" id="cJumlah" name="input[sales_detail_price]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static">
                    </div>
                    <div class="md-input-wrapper">
                        <button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " title="Tambahkan Kemasan">
                            <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Tambahkan PO Kemasan</span>
                        </button>
                    </div>
                </form>

                <br>

                <div dir id="dir" content="table_stock">
                    <table class="table table-striped table-bordered nowrap" style="font-size: 13px">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Kemasan</th>
                                <th>Jumlah Order</th>
                                <th>Harga</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($arrsales_detail as $key => $vaData) {
                            ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $vaData['nama_produk'] ?></td>
                                    <td><?= $vaData['sales_detail_quantity'] ?></td>
                                    <td><?= $vaData['sales_detail_price'] ?></td>
                                    <td>
                                        <a type="button" class="btn btn-danger waves-effect waves-light" href="<?= base_url() ?>Administrator/Stock_Act/hapus_detail_po/<?= $vaData['sales_detail_id'] ?>"><i class="icofont icofont-ui-delete flaticon2-trash"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div id="btn-pb">
                    <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_detail_all'); ?>" method="Post">
                        	<?= input_hidden('id', $id) ?>
                        <button type="submit" class="btn btn-success waves-effect waves-light">
                            SIMPAN PO KEMASAN
                        </button>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>