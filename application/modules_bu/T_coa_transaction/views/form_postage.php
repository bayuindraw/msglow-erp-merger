<div class="row">
    <div class="col-lg-12 col-xl-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Form Input Data Barang
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_postage'); ?>" method="Post">
						<?= input_hidden('id', $id) ?>
					
                    <div class="form-group">
                        <label>Ongkos Kirim</label>
                        <input type="text" id="cJumlah" name="input[package_postage]" value="<?= $arrpackage['package_postage']?>" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static">
                    </div>
					<div class="form-group">
						<div class="kt-radio-list">
							<label class="kt-radio">
								<input type="checkbox" name="input[package_delivery_method]" value="1"> Diantar Mobil
								<span></span>
							</label>
						</div>
					</div>
                    <div class="md-input-wrapper">
                        <button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " title="Tambahkan Kemasan">
                            <i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan</span>
                        </button>
                    </div>
                </form>


            </div>
        </div>
    </div>
</div>