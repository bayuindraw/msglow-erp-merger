
<div class="row">
								<div class="col-lg-12 col-xl-12">

									<!--begin::Portlet-->
									<div class="kt-portlet"> 
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													<?= $title ?>
												</h3>
											</div>
										</div>
										<form method="post" action="<?= site_url($url . '/simpan_produk_hpp'); ?>" enctype="multipart/form-data">
											<?= input_hidden('id', $id) ?>
											<div class="kt-portlet__body">
												<form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_detail'); ?>" method="Post">
						<?= input_hidden('id', $id) ?>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>Produk</th>
                                <th>HPP</th>
                                <th></th>
                              </tr> 
                            </thead>
                            <tbody id="x"> 
								<?php if(@count(@$arrfactory_product) > 0){ 
									$i = 0;
									foreach($arrfactory_product as $indexx => $valuex){
									
								?>
									<tr id="<?= $i; ?>">
										
										<td><select name="input2[<?= $i; ?>][product_id]" class="PilihBarang form-control md-static" required><option></option>
								<?php
									foreach($arrproduct as $indexl => $valuel){
											echo '<option value="'.$valuel['id_produk'].'" '.(($valuex['product_id'] == $valuel['id_produk'])?'selected':'').'>'.$valuel['nama_produk'].' ('.$valuel['klasifikasi'].')</option>';
											//echo '<option value="'.$valuel['id_phl'].'" '.(($valuex['employee_id'] == $valuel['id_phl'])?'selected':'').'>'.$valuel['nama_phl'].'</option>';
								?>
								<?php	
										} ?>
									<td><input type="text" class="form-control numeric" placeholder="Harga" name="input2[<?= $i ?>][factory_product_hpp]" required value="<?= $valuex['factory_product_hpp']; ?>"></td>
																				
										<td><button onclick="myDeleteFunction('<?= $i ?>')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									<?php $i++;
									}
									
								}else{ ?>
									<tr id="0">
										<td><select name="input2[0][product_id]" class="PilihBarang form-control md-static" required><option></option><?= $product_list ?></select></td>
										<td><input type="text" id="cJumlah" name="input2[0][factory_product_hpp]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control static numeric"></td>										
										<td><button onclick="myDeleteFunction('0')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									</tr> 
								<?php } ?>
                            </tbody>    
                         </table> 
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
													<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>
<script> 
it = <?= ((@count(@$arrfactory_product) > 0)?count(@$arrfactory_product):1); ?>;
function myCreateFunction() {
	it++;
	var html = '<tr id="'+it+'"><td><select name="input2['+it+'][product_id]" class="PilihBarang form-control md-static" required><option></option><?= $product_list ?></select></td><td><input type="text" id="cJumlah" name="input2['+it+'][factory_product_hpp]" value="0" onkeyup="return getTotal();" onchange="getTotal();" class="form-control numeric static"></td><td><button onclick="myDeleteFunction('+it+')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
	$('#x').append(html);
	$('.PilihBarang').select2({  
	 allowClear: true,
	 placeholder: 'Pilih Barang',
	});
	$(".numeric").mask("#,##0", {reverse: true});
}	
function myDeleteFunction(id) {
	$('#'+id).remove();
}

</script>