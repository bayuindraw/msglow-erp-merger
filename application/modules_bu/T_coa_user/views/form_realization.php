<?php
if ($parameter == 'ubah' && $id != '') {
	$this->db->where('id_factory', $id);
	$akun = $this->Model->get()->row_array();
}
?>
<div class="row">
	<div class="col-lg-12 col-xl-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/set_realization'); ?>" enctype="multipart/form-data" novalidate>
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label class="col-2 col-form-label">PIC</label>
						<div class="col-4">
							<input type="text" class="form-control " readonly="" placeholder="PIC" name="input[pic]" value="<?= @$akunPic['coa_name'] ?>">
						</div>
						<label class="col-2 col-form-label">Tanggal</label>
						<div class="col-4">
							<input type="text" class="form-control date_picker" readonly="" placeholder="Select date" name="input[tanggal]" value="<?= date('Y-m-d') ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Biaya Admin</label>
						<div class="col-4">
							<label class="kt-checkbox kt-checkbox--success">
								<input type="checkbox" name="is_biaya_admin" onchange="hideBiaya()"> Biaya Admin
								<span></span>
							</label>
						</div>
						<div id="biaya_admin" class="col-6" style="display: none;">
							<input type="number" class="form-control" placeholder="Biaya Admin" name="input[biaya_admin]" id="biaya_admin2" onkeyup="chk_total(); chk_kembalian();">
						</div>
					</div>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Keterangan</th>
								<th>Akun</th>
								<th>Nominal</th>
								<th width="20%">Bukti</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="x">
							<?php
							$totalx = 0;
							$i = 0;
							foreach ($details as $index2 => $detail) {
							?>
								<tr id="tr<?= $i ?>">
									<td><input type="text" class="form-control" placeholder="keterangan" name="input2[<?= $i ?>][keterangan]" value="<?= $detail['coa_transaction_note'] ?>" required></td>
									<td><select name="input2[<?= $i ?>][akun_id]" class="pilihAkun form-control md-static" required><?= $arrakun[$index2] ?></select></td>
									<td><input type="number" class="form-control sum_total" placeholder="nominal" value="<?= $detail['coa_debit'] ?>" onkeyup="chk_total($(this).val()); chk_kembalian();" name="input2[<?= $i ?>][nominal]" required></td>

									<td><input type="file" class="form-control" placeholder="bukti" name="input2[<?= $i ?>][bukti]" value="<?= $detail['coa_transaction_file'] ?>"></td>
									<td><button onclick="myDeleteFunction(<?= $i ?>);chk_kembalian();chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
								</tr>
							<?php $i++;
								$totalx += $detail['coa_debit'];
							} ?>
						</tbody>
						<tbody>
							<tr>
								<td>Total Nominal : <span id="total"><?= $totalx ?></span></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();return false();">Tambah Barang</button>
					<div id="real">
						<div class="form-group row mt-4">
							<label class="col-2 col-form-label">Total Realisasi</label>
							<div class="col-4">
								<input type="number" class="form-control" placeholder="Total Realisasi" name="input[total_realisasi]" onkeyup="chk_kembalian()" id="realisasi" value="<?= @$akun['total_realisasi'] ?>">
							</div>
							<label class="col-2 col-form-label">Total Bayar</label>
							<div class="col-4">
								<input type="number" id="total2" class="form-control" placeholder="Total Bayar" name="input[total_bayar]" value="<?= $totalx ?>" readonly>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-2 col-form-label">Kembalian</label>
							<div class="col-4">
								<input type="number" class="form-control" placeholder="Kembalian" name="input[kembalian]" id="kembalian" value="<?= -$totalx ?>" disabled>
							</div>
							<div class="col-6" id="pilih_akun2">
								<select name="input[akun2_id]" class="pilihAkun form-control md-static" required><?= $arrakun2 ?></select>
							</div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<button type="submit" name="simpan" value="simpan" id="btn_submit" class="btn btn-primary" onsubmit="return confirm('Are you sure?')">Simpan</button>
							<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
						</div>
					</div>
			</form>
		</div>
	</div>
</div>

<script>
	var count = <?= $i; ?>;

	function hideBiaya() {
		var x = document.getElementById("biaya_admin");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}

	function myCreateFunction() {
		count = count + 1;
		var html = '<tr id="tr' + count + '"><td><input type="text" class="form-control" placeholder="keterangan" name="input2[' + count + '][keterangan]" required></td><td><select name="input2[' + count + '][akun_id]" class="pilihAkun' + count + ' form-control md-static" required><?= $arrakunx ?></select></td><td><input type="number" class="form-control sum_total" placeholder="nominal" onkeyup="chk_total($(this).val()); chk_kembalian();" name="input2[' + count + '][nominal]" required></td></td><td><input type="file" class="form-control" placeholder="bukti" name="input2[' + count + '][bukti]" required></td><td><button onclick="myDeleteFunction(' + count + '); chk_total();chk_kembalian();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$('.pilihAkun' + count).select2({
			allowClear: true,
			placeholder: 'Pilih Akun',
		})
	}

	function myDeleteFunction(id) {
		$('#tr' + id).remove();
	}

	function chk_total(val) {
		var sum = 0;
		var sum1 = 0;
		var val = 0;
		var val1 = $('#biaya_admin2').val();
		if (val1 == "") val1 = 0;
		$('.sum_total').each(function() {
			val = this.value;
			if (this.value == "") val = 0;
			sum += parseFloat(this.value);
		});
		sum1 = sum + parseFloat(val1);
		$('#total').html(sum1);
		$('#total2').val(sum1);
	}

	function chk_kembalian(val) {
		var sum = ($('#realisasi').val() - $('#total2').val());
		$('#kembalian').val(sum);
		if (sum == 0) {
			$('#pilih_akun2').hide();
		} else {
			$('#pilih_akun2').show();
		}
	}
</script>