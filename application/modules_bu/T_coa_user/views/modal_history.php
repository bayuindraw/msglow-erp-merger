<div class="form-group row">
	<label class="col-2 col-form-label">PIC</label>
	<div class="col-4">
		<?= @$akunPic['coa_name'] ?>
		</div>
		<label class="col-2 col-form-label">Tanggal</label>
		<div class="col-4">
			<?= substr($akunPic['date_update'], 0, 11) ?>
		</div>
</div>
				<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
					<thead>
						<tr>
							<th>keterangan</th>
							<th>Akun</th>
							<th>Nominal</th>
							<th>Bukti</th>
						</tr>
					</thead>
					<tbody>
						<?php $totalx = 0 ?>
						<?php foreach ($histories as $key => $history) { ?>
						<tr>
							<td>
								<?= $history['coa_transaction_note'] ?>
							</td>
							<td>
								<?= $history['nama_coa'] ?>
							</td>
							<td>
								<?= $history['coa_debit'] ?>
							</td>
							<td>
								<?= $history['coa_transaction_file'] ?>
							</td>
						</tr>
						<?php $totalx += $history['coa_debit'] ?>
						<?php } ?>
					</tbody>
					<tbody>
						<tr>
							<td>Total Nominal : 
								<span id="total">
									<?= $totalx ?>
								</span>
							</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>