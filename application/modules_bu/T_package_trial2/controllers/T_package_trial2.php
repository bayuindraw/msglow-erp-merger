<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;

class T_package_trial2 extends CI_Controller
{

	var $url_ = "T_package_trial2";
	var $id_ = "package_id";
	var $eng_ = "package";
	var $ind_ = "Pembagian";
	var $ind2_ = "Pengiriman Barang";

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	function testxx()
	{
		$this->db->trans_begin();


		$this->Model->check_fifo_main(99);


		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}

		die();
	}

	public function table_produk()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_ . ' Produk';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_produk', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_produk()
	{
		$datacontent['datatable'] = $this->Model->get_data_produk();
	}

	public function form_return_retur($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Pengembalian Retur';
		$datacontent['id'] = $id;
		$datacontent['factory_list'] = "";
		foreach ($this->Model->get_factory() as $key => $value) {
			$datacontent['factory_list'] = $datacontent['factory_list'] . '<option value="' . $value['id_factory'] . '">' . $value['nama_factory'] . '</option>';
		}
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Repair';
		$data['content'] = $this->load->view($this->url_ . '/form_return_retur', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_repair($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Repair';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Repair';
		$data['content'] = $this->load->view($this->url_ . '/form_repair', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_masuk_opname($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Adjustment Barang Masuk';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Adjustment';
		$data['content'] = $this->load->view($this->url_ . '/form_masuk_opname', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_keluar_opname($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Adjustment Barang Keluar';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Adjustment';
		$data['content'] = $this->load->view($this->url_ . '/form_keluar_opname', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_sample($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Sample';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		$datacontent['valin'] 	= $this->Model->code("SELECT t_package_trial.member_code, t_package_trial.package_date, t_package_trial_detail.package_detail_quantity, t_package_trial_detail.product_id,  produk.nama_produk FROM t_package_trial INNER JOIN t_package_trial_detail ON t_package_trial.package_id = t_package_trial_detail.package_id INNER JOIN produk ON t_package_trial_detail.product_id = produk.id_produk WHERE member_code = 'MSGLOWXX102' ORDER BY t_package_trial.package_id DESC LIMIT 20"); 
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		} 
		$data['file'] = 'Sample';
		$data['content'] = $this->load->view($this->url_ . '/form_sample', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	
	public function form_return_factory($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Retur Ke Factory';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['factory_list'] = "";
		foreach ($this->Model->get_factory() as $key => $value) {
			$datacontent['factory_list'] = $datacontent['factory_list'] . '<option value="' . $value['id_factory'] . '">' . $value['nama_factory'] . '</option>';
		} 
		$datacontent['arrproduct'] = $this->Model->get_produk();
		
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Retur Ke Factory';
		$data['content'] = $this->load->view($this->url_ . '/form_return_factory', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_rusak($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Rusak';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['arrproduct'] = $this->Model->get_produk();
		$datacontent['valin'] 	= $this->Model->code("SELECT t_package_trial.member_code, t_package_trial.package_date, t_package_trial_detail.package_detail_quantity, t_package_trial_detail.product_id,  produk.nama_produk , t_package_trial_detail.package_detail_batch, t_package_trial.keterangan_reject,t_package_trial.status_barang,t_package_trial.tanggal_incoming FROM t_package_trial INNER JOIN t_package_trial_detail ON t_package_trial.package_id = t_package_trial_detail.package_id INNER JOIN produk ON t_package_trial_detail.product_id = produk.id_produk WHERE member_code = 'MSGLOWXX101' ORDER BY t_package_trial.package_id DESC LIMIT 20"); 
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Rusak';
		$data['content'] = $this->load->view($this->url_ . '/form_rusak', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_produk_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrproduk_row'] = $this->Model->get_produk_row($id);
		$arrpackage_trial_detail = $this->Model->get_package_trial_detail3($id);
		$arrpembagian = $this->Model->get_pembagian($id);
		foreach($arrpembagian as $indexbagi => $valuebagi){
			$arrpembagian2[$valuebagi['seller_id']] = $valuebagi['jum'];
		}
		$datacontent['arrpackage_trial_employee'] = $this->Model->get_package_trial_employee3($id);
		//$datacontent['data_trial_employee'][] = array();
		foreach ($datacontent['arrpackage_trial_employee'] as $indeemp => $valueemp) {
			$datacontent['data_trial_employee'][$valueemp['package_detail_id']][] = $valueemp;
		}
		$datacontent['seller_list'] = "";
		$datacontent['arrmember'] = $this->Model->get_member();
		foreach ($datacontent['arrmember'] as $key => $value) {
			$datacontent['seller_list'] = $datacontent['seller_list'] . '<option value="' . $value['kode'] . '">' . $value['nama'] . ' (' . $value['kode'] . ')</option>';
		}
		$datacontent['product_list'] = "";
		$datacontent['arrproduk'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduk'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$datacontent['employee_list'] = "";
		$arrline_product = $this->db->query("SELECT B.* FROM m_line_product A JOIN m_line B ON B.line_id = A.line_id WHERE A.product_id = '$id'")->result_array();
		if (@$arrline_product[0]['line_id'] == "") {
			$datacontent['arrphl'] = $this->Model->get_phl();
		} else {
			$datacontent['arrphl'] = $arrline_product;
		}
		foreach ($datacontent['arrphl'] as $key => $value) {
			$datacontent['employee_list'] = $datacontent['employee_list'] . '<option value="' . $value['line_id'] . '">' . $value['line_name'] . '</option>';
		}
		$produk = $this->db->get_where('produk', ['id_produk' => $id])->row_array();
		$pembandings = $this->db->query("SELECT B.seller_id, A.nama, A.kota, SUM(C.pending_quantity) as 'package_detail_quantity' FROM member A JOIN t_sales B ON B.seller_id = A.kode JOIN t_sales_detail C ON C.sales_id = B.sales_id JOIN produk D ON D.id_produk = C.product_id JOIN produk_global E ON E.kode = D.kd_pd WHERE C.pending_quantity > 0 AND E.kode = '$produk[kd_pd]' GROUP BY E.kode, B.seller_id")->result_array();
		$datacontent['arrpackage_trial_detail'] = array();
		$arrjoin_sales_id = array();
		foreach ($pembandings as $indexb => $valueb) {
			$datacontent['arrpackage_trial_detail'][$valueb['seller_id']] = $valueb;
			$datacontent['arrpackage_trial_detail'][$valueb['seller_id']]['pending_quantity'] = $valueb['package_detail_quantity'];
			$datacontent['arrpackage_trial_detail'][$valueb['seller_id']]['pembagian_quantity'] = (@$arrpembagian2[$valueb['seller_id']] == "")?0:@$arrpembagian2[$valueb['seller_id']];
			$datacontent['arrpackage_trial_detail'][$valueb['seller_id']]['package_detail_quantity'] = 0; 
		}
		$blacklist = array();
		foreach ($arrpackage_trial_detail as $indexa => $valuea) {
			if (@$datacontent['arrpackage_trial_detail'][$valuea['seller_id']]['pending_quantity'] != '') {
				@$valuea['pending_quantity'] = $datacontent['arrpackage_trial_detail'][$valuea['seller_id']]['pending_quantity'];
			} else {
				@$valuea['pending_quantity'] = 0;
			}
			//@$valuea['package_detail_quantity'] = 0;
			$valuea['pembagian_quantity'] = (@$arrpembagian2[$valuea['seller_id']] == "")?0:@$arrpembagian2[$valuea['seller_id']];
			$datacontent['arrpackage_trial_detail2'][$valuea['seller_id']][] = $valuea;
			$blacklist[$valuea['seller_id']] = $valuea['seller_id'];
		}
		foreach ($pembandings as $indexb => $valueb) {
			if($valueb['seller_id'] != @$blacklist[$valueb['seller_id']]){
				$datacontent['arrpackage_trial_detail2'][$valueb['seller_id']][] = $datacontent['arrpackage_trial_detail'][$valueb['seller_id']];
			}
		}
		$datacontent['arrdelivery_product'] = $this->db->query("SELECT B.account_detail_delivery_product_id, B.pending_quantity, D.nama, D.kode, A.account_detail_delivery_address, B.account_detail_delivery_product_quantity, IFNULL(G.package_detail_quantity, 0) as pembagian FROM t_account_detail_delivery A JOIN t_account_detail_delivery_product B ON B.account_detail_delivery_id = A.account_detail_delivery_id JOIN t_sales C ON C.sales_id = B.sales_id JOIN member D ON D.kode = C.seller_id JOIN produk E ON E.id_produk = B.product_id JOIN produk F ON F.kd_pd = E.kd_pd LEFT JOIN t_package_trial_detail_draft G ON G.account_detail_delivery_product_id = B.account_detail_delivery_product_id WHERE F.produk_status = 1 AND F.id_produk = '$id' AND B.account_detail_delivery_product_status = 1")->result_array();
		
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_produk_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function delete_all_pembagian($id)
	{
		$this->db->trans_begin();
		$this->db->query("DELETE FROM t_package_trial_detail_draft WHERE product_id = '$id' AND package_detail_confirm_quantity = 0");
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		return redirect(site_url('T_package_trial2/form_produk_detail/'. $id));
	}

	public function simpan_repair()
	{
		$data = $this->input->post('input2');
		$data['user'] = $_SESSION['user_id'];
		$data['kode_po'] = 'MSGLOWPOFACTORY-2111054';
		$data['date_create'] = date('Y-m-d H:i:s');
		$data['factory_id'] = '19';
		$data['nomor_surat_jalan'] = '';
		$data['warehouse_id'] = $_SESSION['warehouse_id'];
		$exec = $this->Model->insert_terima_produk($data);
		$this->Model->update_masuk($data['jumlah'], $data['tgl_terima'], $data['id_barang']);
		redirect(site_url('T_package_trial2/form_repair'));
	}

	public function simpan_masuk_opname()
	{
		$data = $this->input->post('input2');
		$data['user'] = $_SESSION['user_id'];
		$data['kode_po'] = 'MSGLOWPOFACTORY-2111055';
		$data['date_create'] = date('Y-m-d H:i:s');
		$data['factory_id'] = '29';
		$data['nomor_surat_jalan'] = '';
		$data['warehouse_id'] = $_SESSION['warehouse_id'];
		$exec = $this->Model->insert_terima_produk($data);
		$this->Model->update_masuk($data['jumlah'], $data['tgl_terima'], $data['id_barang']);
		redirect(site_url('T_package_trial2/form_masuk_opname'));
	}

	public function simpan_return_retur()
	{
		$data = $this->input->post('input2');
		$data['user'] = $_SESSION['user_id'];
		$data['kode_po'] = 'MSGLOWPOFACTORY-2111056';
		$data['date_create'] = date('Y-m-d H:i:s');
		$data['factory_id'] = '28';
		$data['nomor_surat_jalan'] = '';
		$data['warehouse_id'] = $_SESSION['warehouse_id'];
		$exec = $this->Model->insert_terima_produk($data);
		$this->Model->update_masuk($data['jumlah'], $data['tgl_terima'], $data['id_barang']);
		redirect(site_url('T_package_trial2/form_repair'));
	}

	public function simpan_sample()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$value = $this->input->post('input2');
			//$id2 = $_POST['id'];
			$data['package_code'] = $this->generate_code();
			$data['package_status_id'] = '1';
			$data['member_code'] = 'MSGLOWXX102';
			$data['user_id'] = $_SESSION['user_id'];
			$data['package_date_create'] = date('Y-m-d H:i:s');
			$data['warehouse_id'] = $_SESSION['warehouse_id'];
			$exec = $this->Model->insert($data);
			$id = $this->Model->insert_id();

			//foreach($data2 as $key => $value){
			if (@$value['product_id'] != "") {
				$value['package_id'] = $id;
				$exec = $this->Model->insert_detail($value);
				$id2 = $this->Model->insert_id();

				$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
			}
			//}
		}
		redirect(site_url('T_package_trial2/form_sample'));
	}
	
	public function simpan_transfer()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$value = $this->input->post('input2');
			//$id2 = $_POST['id'];
			$data['package_code'] = $this->generate_code();
			$data['package_status_id'] = '1';
			$data['member_code'] = 'MSGLOWTRANSFER';
			$data['user_id'] = $_SESSION['user_id'];
			$data['package_date_create'] = date('Y-m-d H:i:s');
			$data['warehouse_id'] = $_SESSION['warehouse_id'];
			$exec = $this->Model->insert($data);
			$id = $this->Model->insert_id();

			//foreach($data2 as $key => $value){
			if (@$value['product_id'] != "") {
				$value['package_id'] = $id;
				$exec = $this->Model->insert_detail($value);
				$id2 = $this->Model->insert_id();
				$xwarehouse_product['warehouse_id'] = $data['warehouse_target_id'];
				$xwarehouse_product['product_id'] = $value['product_id'];
				$this->db->insert('m_warehouse_product', $xwarehouse_product);
				$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
			}
			//}
		}
		redirect(site_url('T_package_trial2/form_transfer'));
	}
	
	public function simpan_return_factory()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$value = $this->input->post('input2');
			//$id2 = $_POST['id'];
			$data['package_code'] = $this->generate_code();
			$data['package_status_id'] = '1';
			$data['member_code'] = 'MSGLOWXX103';
			$data['user_id'] = $_SESSION['user_id'];
			$data['package_date_create'] = date('Y-m-d H:i:s');
			$data['warehouse_id'] = $_SESSION['warehouse_id'];
			$exec = $this->Model->insert($data);
			$id = $this->Model->insert_id();

			//foreach($data2 as $key => $value){
			if (@$value['product_id'] != "") {
				$value['package_id'] = $id;
				$exec = $this->Model->insert_detail($value);
				$id2 = $this->Model->insert_id();

				$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
			}
			//}
		}
		redirect(site_url('T_package_trial2/form_return_factory'));
	}

	public function simpan_keluar_opname()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$value = $this->input->post('input2');
			//$id2 = $_POST['id'];
			$data['package_code'] = $this->generate_code();
			$data['package_status_id'] = '1';
			$data['member_code'] = 'MSGLOWXX104';
			$data['user_id'] = $_SESSION['user_id'];
			$data['package_date_create'] = date('Y-m-d H:i:s');
			$data['warehouse_id'] = $_SESSION['warehouse_id'];
			$exec = $this->Model->insert($data);
			$id = $this->Model->insert_id();

			//foreach($data2 as $key => $value){
			if (@$value['product_id'] != "") {
				$value['package_id'] = $id;
				$exec = $this->Model->insert_detail($value);
				$id2 = $this->Model->insert_id();

				$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
			}
			//}
		}
		redirect(site_url('T_package_trial2/form_sample'));
	}

	public function simpan_rusak()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$value = $this->input->post('input2');
			//$id2 = $_POST['id'];
			$data['package_code'] = $this->generate_code();
			$data['package_status_id'] = '1';
			$data['member_code'] = 'MSGLOWXX101';
			$data['user_id'] = $_SESSION['user_id'];
			$data['package_date_create'] = date('Y-m-d H:i:s');
			$data['warehouse_id'] = $_SESSION['warehouse_id'];
			$exec = $this->Model->insert($data);
			$id = $this->Model->insert_id();

			//foreach($data2 as $key => $value){
			if (@$value['product_id'] != "") {
				$value['package_id'] = $id;
				$exec = $this->Model->insert_detail($value);
				$id2 = $this->Model->insert_id();

				$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
			}
			//}
		}
		redirect(site_url('T_package_trial2/form_rusak'));
	}

	public function simpan_safeconduct()
	{
		if ($this->input->post('simpan')) {
			$id = $this->input->post('id');
			$data2 = $this->input->post('input2');
			$this->Model->delete_package_safeconduct(['package_id' => $id]);
			foreach ($data2 as $key => $value) {
				$value['package_id'] = $id;
				$exec = $this->Model->insert_package_safeconduct($value);
			}
		}
		redirect(site_url('T_package_trial2/table_safeconduct'));
	}

	public function simpan_produk_detail()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$id = $this->input->post('id');
			$data2 = $this->input->post('input2');
			$data3 = $this->input->post('input3');
			$exec = $this->Model->insert_reset3($id);
			foreach ($data2 as $key => $value) {
				if($value['package_detail_quantity'] > 0){
					$value['product_id'] = $id;
					$value['account_detail_delivery_product_id'] = $key;
					$value['date_created'] = date('Y-m-d H:i:s');
					$exec = $this->Model->insert_detail_draft($value);
					$id2 = $this->Model->insert_id();
					foreach ($data3[$key] as $key3 => $value3) {
						$value3['product_id'] = $id;
						$value3['package_detail_id'] = $id2;
						$exec = $this->Model->insert_employee($value3);
					}
				}
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_package_trial2/table_produk'));
	}

	public function table_seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Konfirmasi ' . $this->ind2_;
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/table_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_seller2()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_ . ' seller';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_seller2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_seller();
	}

	public function get_data_seller2()
	{
		$datacontent['datatable'] = $this->Model->get_data_seller2();
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_safeconduct()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_safeconduct', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	
	public function table_accept_transfer()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_accept_transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	
	public function form_transfer($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Form Transfer';
		$datacontent['id'] = $id;
		$datacontent['product_list'] = "";
		$datacontent['warehouse_list'] = "";
		$datacontent['arrwarehouse'] = $this->Model->get_warehouse();
		foreach ($datacontent['arrwarehouse'] as $key => $value) {
			$datacontent['warehouse_list'] = $datacontent['warehouse_list'] . '<option value="' . $value['warehouse_id'] . '">' . $value['warehouse_name']. ')</option>';
		}
		$datacontent['arrproduct'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$data['file'] = 'Transfer';
		$data['content'] = $this->load->view($this->url_ . '/form_transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail_draft($id)
	{
		$datacontent['url'] = $this->url_;
		$data['file'] = $this->ind_;
		$datacontent['arrpackage_trial_detail_draft'] = $this->Model->get_package_trial_detail_draft($id);
		$datacontent['arrdata'] = $this->Model->get_package_trial($id);
		$datacontent['title'] = 'Data ' . $this->ind_ . ' Detail : ' . $datacontent['arrdata']['package_code'];
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	
	public function form_upload($id = '')
	{

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_upload', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	
	public function simpan_upload($id = '')
	{
		$this->db->trans_begin();
		$id = $_POST['id'];
		$inputFileName = $_FILES['excel']['tmp_name'];
		$spreadsheet = IOFactory::load($inputFileName);
		$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
		foreach($sheetData as $index => $valuex){
			$value['product_id'] = $id;
			$value['date_created'] = date('Y-m-d H:i:s');
			$value['package_detail_quantity'] = $valuex['B'];
			$value['seller_id'] = $valuex['A'];
			$exec = $this->Model->insert_detail_draft($value);
			$id2 = $this->Model->insert_id();
			$value3['product_id'] = $id;
			$value3['package_detail_id'] = $id2;
			$value3['employee_id'] = $valuex['C'];
			$exec = $this->Model->insert_employee($value3);
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_package_trial2/form_produk_detail/'.$id));
	}

	public function form_detail2($id)
	{
		$datacontent['url'] = $this->url_;
		$data['file'] = $this->ind_;
		$datacontent['arrpackage_trial_detail_draft'] = $this->Model->get_package_trial_detail($id);
		$datacontent['arrdata'] = $this->Model->get_package_trial($id);
		$datacontent['title'] = 'Data ' . $this->ind_ . ' Detail : ' . $datacontent['arrdata']['nama'] . ' (' . $datacontent['arrdata']['package_code'] . ')';
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function cetaksuratjalankemasan($id = "")
	{
		$data['kode']	= $id;
		$data['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail($id);
		$data['arrpackage_trial'] = $this->Model->get_package_trial($id);

		$this->load->view('sjkemasan', $data);
	}

	public function cetaksuratjalankemasan2($id = "")
	{
		$data['kode']	= $id;
		$data['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail($id);
		$data['arrpackage_trial'] = $this->Model->get_package_trial($id);

		$this->load->view('test_print', $data);
	}

	public function cetaksuratjalankemasan4($id = "")
	{
		$data['kode']	= $id;
		$data['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail_sjx($id);
		$data['arrpackage_trial'] = $this->Model->get_package_trial($id);

		$this->load->view('test_print2', $data);
	}

	public function cetaksuratjalankemasan5($id = "")
	{
		$data['kode']	= $id;
		$data['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail_sj($id);
		$data['arrpackage_trial'] = $this->Model->get_package_trial5($id);

		$this->load->view('test_print5', $data);
	}

	public function cetaksuratjalandriver($id = "")
	{
		$data['kode']	= $id;
		$data['arrpackage_trial_detail'] = $this->Model->get_package_safeconduct($id);
		$data['arrpackage_trial'] = $this->Model->get_package_trial($id);

		$this->load->view('sjdriver2', $data);
	}

	public function cetaksuratjalankemasan3($id = "")
	{
		$data['kode']	= $id;
		$data['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail($id);
		$data['arrpackage_trial'] = $this->Model->get_package_trial($id);

		$this->load->view('sjdriver', $data);
	}


	public function get_data_sales()
	{
		$datacontent['datatable'] = $this->Model->get_data_sales();
	}

	public function table_sales()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Penjualan';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_sales', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function generate_code()
	{
		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWOUT-";
		$dbDate	=	$this->db->query("SELECT (MAX(RIGHT(package_code, 5)) + 1) as JumlahTransaksi FROM t_package_trial WHERE LEFT(package_date_create, 7) = '" . date('Y-m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 1;
			}
		} else {
			$nJumlahTransaksi = 1;
		}
		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0000' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '000' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 4) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 5) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}
		// echo $cKode;
		// die;
		return $cKode;
	}

	public function get_data_postage()
	{
		$datacontent['datatable'] = $this->Model->get_data_postage();
	}

	public function table_postage()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Ongkir';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_postage', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_postage($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Ongkir' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		//$datacontent['arrpackage'] = $this->Model->get_package($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_postage', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_safeconduct($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Ongkir' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		$datacontent['arrpackage_safeconduct'] = $this->Model->get_package_safeconduct($id);
		$datacontent['arrsafeconduct'] = $this->Model->get_safeconduct();
		$datacontent['safeconduct_list'] = "";
		foreach ($datacontent['arrsafeconduct'] as $key => $value) {
			$datacontent['safeconduct_list'] = $datacontent['safeconduct_list'] . '<option value="' . $value['safeconduct_id'] . '">' . $value['safeconduct_name'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_safeconduct', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function search_member($search = '')
	{
		$this->Model->search_member($search);
		/*echo '{
  "results": [
    {
      "id": 1,
      "text": "Option 1"
    },
    {
      "id": 2,
      "text": "Option 2"
    }
  ],
  "pagination": {
    "more": true
  }
}';*/
	}

	public function form($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		$datacontent['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail($id);
		$datacontent['seller_list'] = "";
		/*foreach ($this->Model->get_member() as $key => $value) {
			$datacontent['seller_list'] = $datacontent['seller_list'].'<option value="'.$value['kode'].'">'.$value['nama'].' ('.$value['kode'].')</option>';
		}*/
		$datacontent['product_list'] = "";
		foreach ($this->Model->get_produk() as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . (($value['tipe'] == "1") ? "Barang Jadi" : "Gudang") . ')</option>';
			//$datacontent['product_list'] = $datacontent['product_list'].'<option value="'.$value['id_produk'].'">'.$value['nama_produk'].' ('.(($value['tipe']=="1")?"Barang Jadi":"Gudang").')</option>';
		}
		$datacontent['employee_list'] = "";
		$datacontent['arrphl'] = $this->Model->get_phl();
		foreach ($datacontent['arrphl'] as $key => $value) {
			$datacontent['employee_list'] = $datacontent['employee_list'] . '<option value="' . $value['line_id'] . '">' . $value['line_name'] . '</option>';
		}
		$datacontent['arrsales'] = $this->Model->get_sales($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_confirmation($id = '')
	{
		$id = str_replace('%20', ' ', $id);
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id; 
		$datacontent['code'] = $this->generate_code();
		$datacontent['arrmember'] = $this->Model->get_member2($id);
		$datacontent['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail_draft2x($id); 
		$arrlist_account_detail_delivery = array();
		foreach($datacontent['arrpackage_trial_detail'] as $index => $value){
			$arrlist_account_detail_delivery[$value['account_detail_delivery_product_id']] = $value['account_detail_delivery_address'];
		}
		$datacontent['list_account_detail_delivery'] = "";
		foreach($arrlist_account_detail_delivery as $index => $value){
			$datacontent['list_account_detail_delivery'] .= '<option value="' . $index . '">' . $value . '</option>';
		}	
		$datacontent['arrpackage_trial_employee'] = $this->Model->get_package_trial_employee4($id);
		foreach ($datacontent['arrpackage_trial_employee'] as $indeemp => $valueemp) {
			$datacontent['data_trial_employee'][$valueemp['package_detail_id']][] = $valueemp;
		}
		$datacontent['seller_list'] = "";
		foreach ($this->Model->get_member() as $key => $value) {
			$datacontent['seller_list'] = $datacontent['seller_list'] . '<option value="' . $value['kode'] . '">' . $value['nama'] . ' (' . $value['kode'] . ')</option>';
		}
		$datacontent['product_list'] = "";
		$datacontent['arrproduk'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduk'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$datacontent['employee_list'] = "";
		$datacontent['arrphl'] = $this->Model->get_phl();
		foreach ($datacontent['arrphl'] as $key => $value) {
			$datacontent['employee_list'] = $datacontent['employee_list'] . '<option value="' . $value['line_id'] . '">' . $value['line_name'] . '</option>';
		}
		$datacontent['arrsales'] = $this->Model->get_sales($id);
		$data['file'] = $this->ind2_;
		$data['content'] = $this->load->view($this->url_ . '/form_confirmation', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_confirmation_adjustment($id = '')
	{

		//$datacontent['title'] = 'Data '.$this->ind_.' Detail : '.$datacontent['arrdata']['nama'].' ('.$datacontent['arrdata']['package_code'].')';
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;


		$datacontent['arrpackage_trial_detail_draft'] = $this->Model->get_package_trial_detail($id);
		$datacontent['arrdata'] = $this->Model->get_package_trial($id);


		//$datacontent['arrmember'] = $this->Model->get_member2($id); 



		$datacontent['product_list'] = "";
		$datacontent['arrproduk'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduk'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}


		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_adjustment', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_confirmation2($id = '')
	{
		$id = str_replace('%20', ' ', $id);
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['code'] = $this->generate_code();
		$datacontent['arrmember'] = $this->Model->get_member2($id);
		$datacontent['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail_draft2($id);
		$datacontent['arrpackage_trial_employee'] = $this->Model->get_package_trial_employee4($id);
		foreach ($datacontent['arrpackage_trial_employee'] as $indeemp => $valueemp) {
			$datacontent['data_trial_employee'][$valueemp['package_detail_id']][] = $valueemp;
		}
		$datacontent['seller_list'] = "";
		foreach ($this->Model->get_member() as $key => $value) {
			$datacontent['seller_list'] = $datacontent['seller_list'] . '<option value="' . $value['kode'] . '">' . $value['nama'] . ' (' . $value['kode'] . ')</option>';
		}
		$datacontent['product_list'] = "";
		$datacontent['arrproduk'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduk'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$datacontent['employee_list'] = "";
		$datacontent['arrphl'] = $this->Model->get_phl();
		foreach ($datacontent['arrphl'] as $key => $value) {
			$datacontent['employee_list'] = $datacontent['employee_list'] . '<option value="' . $value['id_phl'] . '">' . $value['nama_phl'] . '</option>';
		}
		$datacontent['arrsales'] = $this->Model->get_sales($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_confirmation2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_employee($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrpackage_trial_detail'] = $this->Model->get_package_trial_detail($id);
		$datacontent['arrpackage_trial_employee'] = $this->Model->get_package_trial_employee($id);
		foreach ($datacontent['arrpackage_trial_employee'] as $indeemp => $valueemp) {
			$datacontent['data_trial_employee'][$valueemp['package_detail_id']][] = $valueemp;
		}
		$datacontent['product_list'] = "";
		$datacontent['arrproduk'] = $this->Model->get_produk();
		foreach ($datacontent['arrproduk'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
		}
		$datacontent['employee_list'] = "";
		$datacontent['arrphl'] = $this->Model->get_phl();
		foreach ($datacontent['arrphl'] as $key => $value) {
			$datacontent['employee_list'] = $datacontent['employee_list'] . '<option value="' . $value['id_phl'] . '">' . $value['nama_phl'] . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_employee', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail($id = '')
	{

		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrproduct'] = $this->Model->get_product();
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan()
	{
		if ($this->input->post('simpan')) {

			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$data['user_id'] = $this->session->userdata('user_id');
				$data[$this->eng_ . '_date_create'] = date('Y-m-d H:i:s');

				/*if($data['package_postage'] != "" && $data['package_postage'] != "0"){
						$account_id = $this->Model->get_account_id($_POST['seller_id']);
						$id2 = $this->Model->get_max_id();
						$datax['account_id'] = $account_id;
						$datax['account_detail_id'] = $id2;
						$datax['account_detail_type_id'] = 3;
						$datax['account_detail_user_id'] = $_SESSION['user_id'];
						$datax['account_detail_pic'] = $_SESSION['user_fullname'];
						$datax['account_detail_date'] = $data['package_date'];
						$datax['account_detail_category_id'] = '82';
						$datax['account_detail_realization'] = 1;
						$datax['account_detail_price'] = $data['package_postage'];
						$datax['account_detail_note'] = 'Ongkos Kirim '.$data['package_code'];
						$type = "debit";
						$datax['account_detail_debit'] = $data['package_postage'];
						$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
						$id_account_detail = $this->Model->insert_account_detail($datax); 
						$this->db->where('account_id', $account_id);
						$row = $this->Model->get_m_account()->row_array();
						if(@$row['account_date_reset'] > $_POST['package_date']){
							$this->Model->update_balance($account_id,  $data['package_postage'], $type, @$row['account_type_id'], $datax['account_detail_date']);
						}else{
							$this->Model->update_balance($account_id,  $data['package_postage'], $type);
						}
						$data['account_detail_real_id'] = $id_account_detail;
				}*/
				$data['package_date_draft'] = $data['package_date'];
				$exec = $this->Model->insert($data);
				$id = $this->Model->insert_id();
			}
		}
		redirect(site_url('T_package_trial/form_employee/' . $id));
	}

	public function simpan_confirmation()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {

				$id2 = $_POST['id'];
				$member = $this->db->get_where('member', ['kode' => $id2])->row_array();
				$data['package_code'] = $this->generate_code();
				$data['package_status_id'] = '1';
				$data['member_code'] = $id2;
				$data['user_id'] = $_SESSION['user_id'];
				$data['package_date_create'] = date('Y-m-d H:i:s');

				$exec = $this->Model->insert($data);
				$id = $this->Model->insert_id();


				$data2 = $this->input->post('input2');
				//$data3 = $this->input->post('input3');
				//$exec = $this->Model->update_confirm($id, $id2);

				$data2 = $this->input->post('input2');
				foreach ($data2 as $key => $value) {
					if (@$value['product_id'] != "") {
						
						$value['package_id'] = $id;
						$value['package_detail_quantity'] = (($value['package_detail_quantity'] == "")?0:$value['package_detail_quantity']);
						$value['pending_beta_quantity'] = $value['package_detail_quantity'];
						$value['pending_alpha_quantity'] = $value['package_detail_quantity'];
						
						$this->db->query("UPDATE t_package_trial_detail_draft D SET D.package_detail_quantity = D.package_detail_quantity - '" . $value['package_detail_quantity'] . "', D.package_detail_confirm_quantity = D.package_detail_confirm_quantity + '" . $value['package_detail_quantity'] . "' WHERE D.package_detail_id = '" . $value['package_detail_id'] . "'");
						unset($value['package_detail_id']);
						$exec = $this->Model->insert_detail($value);
						$id2 = $this->Model->insert_id();

						/*foreach ($data3[$key] as $key3 => $value3) {
							$value3['package_id'] = $id;
							$value3['package_detail_id'] = $id2;
							$value3['product_id'] = $value['product_id'];
							$exec = $this->Model->insert_employee_real($value3);
						}*/
						$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
						$this->Model->check_fifo_alpha($value['product_id']);
						$this->Model->check_fifo_beta($value['product_id']);
						$this->Model->check_fifo_main($value['product_id'], $_POST['id'], $member['schema_id'], $member['kode']);
						$this->Model->set_sales_detail();
					}
				}
				$this->db->query("UPDATE t_package_trial_detail_draft D SET D.package_id = '$id' WHERE D.package_id IS NULL AND D.seller_id = '" . $data['member_code'] . "' AND D.package_detail_quantity = 0");
			}
		}
		//die('ada');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_package_trial2'));
	}

	public function simpan_confirmation_x()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {

				$id2 = $_POST['id'];
				$member = $this->db->get_where('member', ['kode' => $id2])->row_array();
				$data['package_code'] = $this->generate_code();
				$data['package_status_id'] = '1';
				$data['member_code'] = $id2;
				$data['user_id'] = $_SESSION['user_id'];
				$data['package_date_create'] = date('Y-m-d H:i:s');
				$data['warehouse_id'] = $_SESSION['warehouse_id'];
				$exec = $this->Model->insert($data);
				$id = $this->Model->insert_id();


				$data2 = $this->input->post('input2');
				//$data3 = $this->input->post('input3');
				//$exec = $this->Model->update_confirm($id, $id2);

				$data2 = $this->input->post('input2');
				foreach ($data2 as $key => $value) {
					if (@$value['product_id'] != "") {
						
						$value['package_id'] = $id;
						$value['pending_beta_quantity'] = $value['package_detail_quantity'];
						$value['pending_alpha_quantity'] = $value['package_detail_quantity'];

						$this->db->query("UPDATE t_package_trial_detail_draft D SET D.package_detail_quantity = D.package_detail_quantity - '" . $value['package_detail_quantity'] . "', D.package_detail_confirm_quantity = D.package_detail_confirm_quantity + '" . $value['package_detail_quantity'] . "' WHERE D.package_id IS NULL AND D.seller_id = '" . $data['member_code'] . "' AND D.product_id = '" . $value['product_id'] . "'");

						$exec = $this->Model->insert_detail($value);
						$id2 = $this->Model->insert_id();

						/*foreach ($data3[$key] as $key3 => $value3) {
							$value3['package_id'] = $id;
							$value3['package_detail_id'] = $id2;
							$value3['product_id'] = $value['product_id'];
							$exec = $this->Model->insert_employee_real($value3);
						}*/
						$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
						$this->Model->check_fifo_alpha($value['product_id']);
						$this->Model->check_fifo_beta($value['product_id']);
						$this->Model->check_fifo_main($value['product_id'], $_POST['id'], $member['schema_id'], $member['kode']);
						$this->Model->set_sales_detail();
					}
				}
				$this->db->query("UPDATE t_package_trial_detail_draft D SET D.package_id = '$id' WHERE D.package_id IS NULL AND D.seller_id = '" . $data['member_code'] . "' AND D.package_detail_quantity = 0");
			}
		}
		//die('ada');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_package_trial2'));
	}


	public function json_konfirmasi_scan($barcode)
	{
		// $arrdata = json_decode($_POST, true);
		$arrdata = $barcode;
		$this->db->trans_begin();
		$arrdatascan = $this->db->query("SELECT * FROM t_package_trial_scan WHERE package_barcode = '$barcode'")->row_array();
		//echo $barcode;
		if (@$arrdatascan['package_id'] != "") {
			$idxx = $arrdatascan['package_id'];
			//$id2 = $_POST['id'];
			$member = $this->db->get_where('member', ['kode' => $arrdatascan['member_code']])->row_array();
			$data['package_scan_id'] = $idxx;
			$data['member_code'] = $arrdatascan['member_code'];
			$data['package_code'] = $arrdatascan['package_code'];
			$data['package_date'] = date('Y-m-d');
			$data['package_date_create'] = date('Y-m-d H:i:s');
			$data['package_status_id'] = '1';
			$data['user_pwa_id'] = $arrdatascan['user_id'];
	
			$exec = $this->Model->insert($data);
			$id = $this->Model->insert_id();

			$data2 = $this->db->query("SELECT * FROM android_barcode_line WHERE confirmation = '$idxx'")->result_array();
			//$data2 = $this->input->post('input2');
			foreach ($data2 as $key => $value2) {
				$value['product_id'] = $value2['product_id'];
				if (@$value['product_id'] != "") {
					$this->db->query("UPDATE android_barcode_line D SET D.confirmation = '$id' WHERE D.shipping = 1 AND D.shipping_time IS NOT NULL AND D.sellerid = '" . $data['member_code'] . "' AND D.product_id = '" . $value['product_id'] . "' AND D.confirmation = 0");
					$value['package_id'] = $id;
					$value['package_detail_quantity'] = $value2['qty'];
					$value['pending_beta_quantity'] = $value['package_detail_quantity'];
					$value['pending_alpha_quantity'] = $value['package_detail_quantity'];

					$this->db->query("UPDATE t_package_trial_detail_draft D SET D.package_detail_quantity = D.package_detail_quantity - '" . $value['package_detail_quantity'] . "', D.package_detail_confirm_quantity = D.package_detail_confirm_quantity + '" . $value['package_detail_quantity'] . "' WHERE D.package_detail_id = '".$value2['id_detail']."' AND D.seller_id = '" . $data['member_code'] . "' AND D.product_id = '" . $value['product_id'] . "'");

					$exec = $this->Model->insert_detail($value);
					$id2 = $this->Model->insert_id();

					/*foreach ($data3[$key] as $key3 => $value3) {
						$value3['package_id'] = $id;
						$value3['package_detail_id'] = $id2;
						$value3['product_id'] = $value['product_id'];
						$exec = $this->Model->insert_employee_real($value3);
					}*/
					$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
					$this->Model->check_fifo_alpha($value['product_id']);
					$this->Model->check_fifo_beta($value['product_id']);
					$this->Model->check_fifo_main($value['product_id'], $data['member_code'], $member['schema_id'], $member['kode']);
					$this->Model->set_sales_detail();
					$this->db->query("UPDATE t_package_trial_detail_draft D SET D.package_id = '$id' WHERE D.package_detail_id = '".$value2['id_detail']."' AND D.seller_id = '" . $data['member_code'] . "' AND D.product_id = '" . $value['product_id'] . "' AND D.package_detail_quantity = 0");
				}
			} 
			$this->db->query("UPDATE t_package_trial_scan SET package_barcode_status = 1, package_date_update = NOW() WHERE package_barcode = '$barcode'");
			//die('ada');
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$arrreturn['code'] = '400';
				$arrreturn['status'] = 'fail';
			} else {
				$this->db->trans_commit();
				$arrreturn['code'] = '200';
				$arrreturn['status'] = 'success';
			}
		}else{
			$this->db->trans_rollback();
			$arrreturn['code'] = '400';
			$arrreturn['status'] = 'fail';
		}
		echo json_encode($arrreturn);
	}

	public function json_simpan_scan()
	{
		header("Access-Control-Allow-Origin: *");
		$arrdata = json_decode($_POST['data'], true);

		$this->db->trans_begin();
		$data = $arrdata['data'];
		$id_barcode = join("', '", $arrdata['id_barcode']);
		$arrbarcodeline = $this->db->query("SELECT sellerid FROM android_barcode_line WHERE id_barcode IN('$id_barcode') LIMIT 1")->row_array();
		if(@$arrbarcodeline['sellerid'] != ""){
			$data['member_code'] = $arrbarcodeline['sellerid'];
			$data['package_code'] = $this->generate_code();
			$data['package_date'] = date('Y-m-d');
			$data['package_date_create'] = date('Y-m-d H:i:s');
			$data['package_status_id'] = '0';
			$data['package_barcode'] = $this->get_barcode();
			$exec = $this->Model->insert_scan($data);
			$id = $this->Model->insert_id();
			$this->db->query("UPDATE android_barcode_line SET confirmation = $id WHERE id_barcode IN('$id_barcode') AND sellerid = '".$arrbarcodeline['sellerid']."'");
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$arrreturn['code'] = '400';
				$arrreturn['status'] = 'fail';
			} else {
				$this->db->trans_commit();
				$arrreturn['code'] = '200';
				$arrreturn['status'] = 'success';
			}
		}else{
			$arrreturn['code'] = '400';
			$arrreturn['status'] = 'fail';
		}
		echo json_encode($arrreturn);
	}

	public function table_delivery_order()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Surat Jalan';
		$data['file'] = 'Surat Jalan';
		$data['content'] = $this->load->view($this->url_ . '/table_delivery_order', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function print_sj($id)
	{
		$data['arrpackage_trial_scan'] = $this->db->query("SELECT X.*, AX.product_id, SUM(AX.qty) as qty, COUNT(AX.product_id) as jum_koli, AY.nama_produk FROM (SELECT GROUP_CONCAT(DISTINCT C.nama_produk ORDER BY C.nama_produk) as produk, A.sellerid, A.barcode FROM android_barcode_line A JOIN produk B ON B.id_produk = A.product_id JOIN produk_global C ON C.kode = B.kd_pd WHERE confirmation = '$id' GROUP BY A.barcode) X JOIN android_barcode_line AX ON AX.barcode = X.barcode JOIN produk AY ON AY.id_produk = AX.product_id JOIN produk_global AV ON AV.kode = AY.kd_pd GROUP BY X.produk, AX.product_id")->result_array();
		$totsj = array();
		foreach ($data['arrpackage_trial_scan'] as $index => $value) {
			if (@$colspan[$value['produk']] == "") $colspan[$value['produk']] = 1;
			else $colspan[$value['produk']]++;
			if (@$totsj[$value['produk']][$value['product_id']] == "") {
				$totsj[$value['produk']][$value['product_id']] = $value['qty'];
				$tokoli[$value['produk']][$value['product_id']] = $value['jum_koli'];
			} else {
				$totsj[$value['produk']][$value['product_id']] += $value['qty'];
				$tokoli[$value['produk']][$value['product_id']] += $value['jum_koli'];
			}
		}
		$data['colspan'] = $colspan;
		$data['arrpackage_trial'] = $this->Model->get_package_trial_scan($id);
		$this->load->view('print_sj', $data);
	}

	public function get_barcode()
	{
		$arrbarcode = $this->db->query("SELECT * FROM r_barcode_pwa_generator")->row_array();
		$barcode = str_replace('-', '', $arrbarcode['barcode_pwa_generator_date']) . str_pad(($arrbarcode['barcode_pwa_generator_counter'] + 1), 4, "0", STR_PAD_LEFT);
		$this->db->query("UPDATE r_barcode_pwa_generator SET barcode_pwa_generator_counter_last = barcode_pwa_generator_counter, barcode_pwa_generator_counter = (barcode_pwa_generator_counter + 1)");
		return $barcode;
	}


	public function simpan_confirmation2()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {


				$this->db->trans_begin();
				$data['update_user_id'] = $_SESSION['user_id'];
				$exec = $this->Model->update($data, [$this->id_ => $this->input->post('id')]);;

				$date_previous = $_POST['date_previous'];
				$arrdata = $this->Model->get_package_trial_detail_pure($this->input->post('id'));
				foreach ($arrdata as $key2 => $value2) {
					$this->Model->update_masuk($value2['package_detail_quantity'], $date_previous, $value2['product_id']);
				}


				$data2 = $this->input->post('input2');
				foreach ($data2 as $key => $value) {
					if (@$value['product_id'] != "") {
						$exec = $this->Model->update_table('t_package_trial_detail', $value, ['package_detail_id' => $value['package_detail_id']]);
						$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
					}
				}
				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}
			}
		}
		redirect(site_url('T_package_trial2'));
	}

	public function simpan_employee()
	{
		if ($this->input->post('simpan')) {
			$id = $this->input->post('id');
			$data2 = $this->input->post('input2');
			$data3 = $this->input->post('input3');
			$exec = $this->Model->insert_reset($id);
			foreach ($data2 as $key => $value) {
				if (@$value['product_id'] != "") {
					$value['package_id'] = $id;
					$exec = $this->Model->insert_detail_draft($value);
					$id2 = $this->Model->insert_id();
					foreach ($data3[$key] as $key3 => $value3) {
						$value3['package_id'] = $id;
						$value3['package_detail_id'] = $id2;
						$exec = $this->Model->insert_employee($value3);
					}
				}
			}
			//$id = $this->input->post('id');
			//$data2 = $this->input->post('input2');
			//foreach($data2 as $key => $value){
			//	$value['package_id'] = $id;
			//	$exec = $this->Model->insert_employee($value);
			//	//$this->Model->update_keluar($value['package_detail_quantity'], $data['package_date'], $value['product_id']);
			//}
		}
		redirect(site_url('T_package_trial'));
	}

	public function simpan_detail()
	{
		if ($this->input->post('simpan')) {
			$data = $this->input->post('input');
			$data['sales_id'] = $_POST['id'];
			$data['user_id'] = $this->session->userdata('user_id');
			$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
			$exec = $this->Model->insert_detail($data);
		}
		redirect(site_url('T_sales/form_detail/' . $_POST['id']));
	}

	public function simpan_postage()
	{
		$this->db->trans_begin();
		if ($this->input->post('simpan')) {
			$id = $this->input->post('id');
			$data = $this->input->post('input');
			if (@$data['package_delivery_method'] == "") {
				$arrdata = $this->Model->get_package_all($id);
				$account_id = $arrdata['account_id'];
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrdata['package_date'];
				$datax['account_detail_category_id'] = '82';
				$datax['account_detail_realization'] = 1;
				$datax['account_detail_price'] = $data['package_postage'];
				$datax['account_detail_note'] = 'Ongkos Kirim ' . $data['package_code'];
				$type = "debit";
				$datax['account_detail_debit'] = $data['package_postage'];
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');
				$id_account_detail = $this->Model->insert_account_detail($datax);
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account()->row_array();
				if (@$row['account_date_reset'] > $arrdata['package_date']) {
					$this->Model->update_balance($account_id,  $data['package_postage'], $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $data['package_postage'], $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			}
			$exec = $this->Model->update($data, [$this->id_ => $this->input->post('id')]);;
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url('T_sales/table_postage'));
	}

	public function simpan_detail_all()
	{
		redirect(site_url('T_sales'));
	}

	public function report()
	{
		if (@$_POST['export_pdf']) {
			$this->export_pdf();
		} else {
			$datacontent['url'] = $this->url_;
			$datacontent['search'] = $this->input->post('search');
			$datacontent['url2'] = '/' . $datacontent['search']['member_status'] . '|' . $datacontent['search']['all'];
			$datacontent['title'] = 'Member';
			$data['file'] = $this->ind_;
			$data['content'] = $this->load->view($this->url_ . '/table_report', $datacontent, TRUE);
			$data['title'] = $datacontent['title'];
			$this->load->view('Layout/home', $data);
		}
	}

	public function export_pdf()
	{
		ob_start();
		$datacontent['data'] = $this->Model->get_report()->result_array();
		$this->load->view('M_member/export_pdf', $datacontent);
		//$data['content'] = $this->load->view('m_member/table_report', $datacontent);
		//$data['siswa'] = $this->siswa_model->view_row();
		//$this->load->view('print', $data);
		$html = ob_get_contents();
		ob_end_clean();

		require './assets/html2pdf/autoload.php';

		$pdf = new Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
		$pdf->WriteHTML($html);
		$pdf->Output(date('Y-m-d') . '.pdf', 'D');
	}

	public function get_data($tgl_awal = '', $tgl_akhir = '')
	{
		$datacontent['datatable'] = $this->Model->get_data($tgl_awal, $tgl_akhir);
	}

	public function get_data_delivery_order($tgl_awal = '', $tgl_akhir = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_delivery_order($tgl_awal, $tgl_akhir);
	}

	public function get_data_safeconduct()
	{
		$datacontent['datatable'] = $this->Model->get_data_safeconduct();
	}
	
	public function get_data_accept_transfer()
	{
		$datacontent['datatable'] = $this->Model->get_data_accept_transfer();
	}
	
	public function get_data_report($search = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_report($search);
	}

	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}

	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}

	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}



	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {

				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_credit'] = $from['account_detail_debit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_credit'], 'credit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_send()
	{
		$this->db->trans_begin();
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect(site_url($this->url_));
	}

	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}

	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();


		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();

		if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
		} else {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
		}



		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_ . "/$id"));
	}

	public function list_kecamatan()
	{
		$id_kota = $this->input->post('id_kota');
		$stasiun = $this->Model->get_list_kecamatan($id_kota);
		$lists = "<option value=''>Pilih Kecamatan</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kecamatan . "'>" . $data->nama_kecamatan . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function list_kota()
	{
		$id_provinsi = $this->input->post('id_provinsi');
		$stasiun = $this->Model->get_list_kota($id_provinsi);
		$lists = "<option value=''>Pilih Kota</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kota . "'>" . $data->nama_kota . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function list_pendingan()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data List Pendingan';
		$data['file'] = 'List Pendingan';
		$data['content'] = $this->load->view($this->url_ . '/list_pendingan', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function detail_list_pendingan($kode)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data List Pendingan Per Produk';
		$datacontent['kode'] = $kode;
		$datacontent['produk'] = $this->db->get_where('produk_global', ['kode' => $kode])->row_array();
		$data['file'] = 'List Pendingan';
		$data['content'] = $this->load->view($this->url_ . '/detail_list_pendingan', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}
	public function detail_account_list_pendingan($product, $kode)
	{
		$kode = str_replace("%20", " ", $kode);
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data List Pendingan Per Produk';
		$datacontent['kode'] = $kode;
		$datacontent['product'] = $product;
		$datacontent['seller'] = $this->db->get_where('t_sales', ['seller_id' => $kode])->row_array();
		$data['file'] = 'List Pendingan';
		$data['content'] = $this->load->view($this->url_ . '/detail_account_list_pendingan', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_list_pendingan()
	{
		$datacontent['datatable'] = $this->Model->get_data_list_pendingan();
	}

	public function get_data_detail_list_pendingan($kode)
	{
		$datacontent['datatable'] = $this->Model->get_data_detail_list_pendingan($kode);
	}
	public function get_data_detail_account_list_pendingan($product, $kode)
	{
		$datacontent['datatable'] = $this->Model->get_data_detail_account_list_pendingan($product, $kode);
	}

	public function list_pendingan_seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data List Pendingan Per Seller';
		$data['file'] = 'List Pendingan';
		$data['content'] = $this->load->view($this->url_ . '/list_pendingan_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function detail_list_pendingan_seller($kode)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data List Pendingan Per Seller';
		$datacontent['kode'] = $kode;
		$datacontent['produk'] = $this->db->get_where('member', ['kode' => $kode])->row_array();
		$data['file'] = 'List Pendingan';
		$data['content'] = $this->load->view($this->url_ . '/detail_list_pendingan_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_list_pendingan_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_list_pendingan_seller();
	}

	public function get_data_detail_list_pendingan_seller($kode)
	{
		$datacontent['datatable'] = $this->Model->get_data_detail_list_pendingan_seller($kode);
	}

	public function salesOrderMonitoring($seller_id)
	{
		header("Access-Control-Allow-Origin: *");
		$this->db->trans_begin();

		$header_data = $this->db->get_where('member', ['kode' => $seller_id])->row();

		// $this->db->select('kode, product_name, product_type, product_classification, qty_order, qty_pending, qty_delivered');
		// $content_data = $this->db->get_where('v_sales_order_monitoring', ['seller_id' => $seller_id])->result_array();

		$query = "SELECT A.kd_pd kode, ";
		$query .= "B.nama_produk product_name, ";
		$query .= "A.product_type product_type, ";
		$query .= "A.klasifikasi product_classification, ";
		$query .= "FORMAT(IFNULL(SUM(X.sales_detail_quantity), 0),0,'de_DE') AS qty_order, ";
		$query .= "FORMAT(IFNULL(SUM(X.pending_quantity), 0),0,'de_DE')      AS qty_pending, ";
		$query .= "FORMAT(IFNULL(SUM(X.connected_quantity), 0),0,'de_DE')    AS qty_delivered ";
		$query .= "FROM produk A ";
		$query .= "JOIN produk_global B ON B.kode = A.kd_pd ";
		$query .= "LEFT JOIN ( ";
		$query .= " SELECT C.sales_id, ";
		$query .= "		E.nama, E.kode, ";
		$query .= "		C.product_id, ";
		$query .= "		C.connected_quantity, ";
		$query .= "		C.pending_quantity, ";
		$query .= "		C.sales_detail_quantity ";
		$query .= " FROM  t_sales_detail C ";
		$query .= "	 JOIN t_sales D ON D.sales_id = C.sales_id ";
		$query .= "	 JOIN member E ON E.kode = D.seller_id ";
		$query .= " WHERE D.seller_id = '" . $seller_id . "') X ON X.product_id = A.id_produk GROUP BY A.kd_pd ";

		$content_data = $this->db->query($query)->result_array();

		$return = array();

		if (isset($header_data)) {
			$return = [
				'seller_id' => $header_data->kode,
				'seller_name' => $header_data->nama,
				'product_detail' => $content_data
			];
		} else {
			$return = $header_data;
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$response['code'] = '400';
			$response['status'] = 'Failed';
		} else {
			$this->db->trans_commit();
			$response['data'] = $return;
			$response['code'] = '200';
			$response['status'] = 'Success';
		}
		echo json_encode($response);
	}

	public function countPoActiveByClassification()
	{
		header("Access-Control-Allow-Origin: *");
		$this->db->trans_begin();

		$data = $this->db->get('vw_count_po_based_on_classification')->row();

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$response['code'] = '400';
			$response['status'] = 'Failed';
		} else {
			$this->db->trans_commit();
			$response['data'] = $data;
			$response['code'] = '200';
			$response['status'] = 'Success';
		}
		echo json_encode($response);
	}
}
