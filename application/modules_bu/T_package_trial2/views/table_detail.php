<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											<?= $title ?>
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
										<thead>
											<tr>
											<th>No</th>
											<th>Nama Produk</th>
											<th>Jumlah</th>
											<th>Pegawai</th>  
											<th>Alamat</th>  
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 0;
											foreach ($arrpackage_trial_detail_draft as $key => $vaData) {
											?>
											<tr>
												<td><?= ++$no ?></td>
												<td><?= $vaData['nama_produk'] ?></td>
												<td><?= $vaData['package_detail_quantity'] ?></td>
												<td><?= $vaData['pegawai'] ?></td>
												<td><?= $vaData['address'] ?></td>
											</tr>
											<?php } ?>
								
										</tbody>
									</table>
								</div>
							</div> 
							