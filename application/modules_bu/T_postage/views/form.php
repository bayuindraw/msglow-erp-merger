<div class="row">
   <div class="col-lg-12 col-xl-12">
      <!--begin::Portlet-->
      <div class="kt-portlet">
         <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
               <h3 class="kt-portlet__head-title">
                  Form Input Sales Order
               </h3>
            </div>
         </div>
      </div>
   </div>
   <div class="col-lg-12 col-xl-12">
      <!--begin::Portlet-->
      <div class="kt-portlet">
         <div class="kt-portlet__body">
            <form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan'); ?>" method="Post">
               <?= input_hidden('parameter', $parameter) ?>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Jasa Ekspedisi</label>
                  <div class="col-4">
                     <select name="input[ekspedisi_id]" class="form-control pilihAkun" id="selectLevel">
                        <option></option>
                        <?php foreach ($arrpostage_instance as $row) { ?>
                           <option value="<?= $row['postage_instance_id'] ?>"><?= $row['postage_instance_name'] ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                  <thead>
                     <tr>
                        <th>No Resi</th>
                        <th>Tanggal Kirim</th>
                        <th>Nama Seller</th>
                        <th>Tagihan</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody id="x">
                     <?php
                     $totalx = 0;
                     $i = 0;
                     ?>
                     <tr id="tr0">
                        <td><input type="text" class="form-control" placeholder="no_resi" autocomplete="off" name="input2[0][sales_code]" required></td>
                        <td><input type="text" class="form-control date_picker" placeholder="Select date" autocomplete="off" name="input2[0][sales_date]" required></td>
                        <td>
                           <select name="input2[0][seller_id]" id="PilihDistributor" class="form-control md-static" required>
                              <option></option>
                           </select>
                        </td>
                        <td><input type="number" class="form-control tagihan" placeholder="Tagihan" name="input2[0][tagihan]" onkeyup="chk_total($(this).val());"></td>
                        <td><button onclick="myDeleteFunction(0);chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
                     </tr>
                     <?php $i++; ?>
                  </tbody>
                  <tbody>
                     <tr>
                        <td>Total Nominal : <span id="total"><?= $totalx ?></span></td>
                        <td><input type="hidden" name="input[total]" id="total1" /></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  </tbody>
               </table>
               <button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Data</button>
               <div class="kt-portlet__foot">
                  <div class="kt-form__actions">
                     <input type="hidden" name="supplier" value="<?= $this->session->userdata('supplier') ?>">
                     <button type="submit" name="simpan" class="btn btn-success waves-effect waves-light" value="simpan">
                        SIMPAN
                     </button>
                     <a href="<?= site_url($url); ?>" class="btn btn-warning waves-effect waves-light">
                        BATAL
                     </a>
                  </div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<script>
   var count = <?= $i; ?>;

   function myCreateFunction() {
      count = count + 1;
      var html = '<tr id="tr' + count + '"><td><input type="text" class="form-control" placeholder="no_resi" autocomplete="off" name="input2[' + count + '][sales_code]" required></td><td><input type="text" class="form-control date_picker' + count + '" placeholder="Select date" autocomplete="off" name="input2[' + count + '][sales_date]" required></td><td><select name="input2[' + count + '][seller_id]" id="PilihDistributor' + count + '" class="form-control md-static" required><option></option></select></td><td><input type="number" class="form-control tagihan" placeholder="Tagihan" name="input2[' + count + '][tagihan]" onkeyup="chk_total($(this).val());"></td><td><button onclick="myDeleteFunction(' + count + ');chk_total();" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
      $('#x').append(html);
      $('#PilihDistributor' + count).select2({
         minimumInputLength: 3,
         allowClear: true,
         placeholder: 'Input Nama Seller',
         ajax: {
            type: 'POST',
            url: function(params) {
               return '<?= base_url() . $url ?>/search_member/' + params.term;
            },
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',

            processResults: function(data, params) {
               params.page = params.page || 1;

               return {
                  results: data.results,
                  pagination: {
                     more: (params.page * 10) < data.count_filtered
                  }
               };
            }
         }
      });
      $('.date_picker' + count).datepicker({
         rtl: KTUtil.isRTL(),
         todayHighlight: true,
         orientation: "bottom left",
         format: "yyyy-mm-dd"
      });
   }

   function myDeleteFunction(id) {
      $('#tr' + id).remove();
   }

   function chk_total(val) {
      var sum = 0;
      $('.tagihan').each(function() {
         val = this.value;
         if (this.value == "") val = 0;
         sum += parseFloat(val);
      });
      $('#total').html(sum);
      $('#total1').val(sum);
   }
</script>