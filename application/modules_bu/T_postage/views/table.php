<div class="kt-portlet">
	<div class="kt-portlet__body  kt-portlet__body--fit">
		<div class="row row-no-padding row-col-separator-lg">
			<a href="<?= base_url() ?>T_postage/set_jurnal_tagihan" class="col-md-12 col-lg-12 col-xl-12">

				<!--begin::Total Profit-->
				<div class="kt-widget24">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Total Tagihan
							</h4>
						</div>
						<span class="kt-widget24__stats kt-font-brand">
							Rp <?= number_format($total_tagihan['tagihan']) ?>
						</span>
					</div>
					<div class="kt-widget24__action">
						<span class="kt-widget24__change">
							Click disini untuk pembayaran semua tagihan
						</span>
					</div>
				</div>

				<!--end::Total Profit-->
			</a>
		</div>
	</div>
</div>
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
			<h3 class="kt-portlet__head-title">
				<?= $title ?>
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					<a href="<?= site_url() . $url . "/form" ?>" class="btn btn-brand btn-elevate btn-icon-sm">
						<i class="la la-plus"></i>
						Input Resi
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
			<thead>
				<tr>
					<th>No</th>
					<th>Code</th>
					<th>Nama</th>
					<th>Deposit</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<!--end: Datatable -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",
			scrollY: "300px",
			scrollX: true,
			scrollCollapse: true,
			"processing": true,
			"serverSide": true,
			"ajax": "<?= site_url() ?>/<?= $url ?>/get_data"
		});
	});
</script>