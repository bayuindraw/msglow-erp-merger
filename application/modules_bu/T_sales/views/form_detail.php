<div class="row">
	<div class="col-lg-12 col-xl-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<table>
							<tr>
								<td>Nomor PR</td>
								<td>&nbsp;&nbsp;:</td>
								<td>&nbsp;&nbsp;<?= $kodeSO = $arrsales_member['sales_code'] ?></td>
							</tr>
							<tr>
								<td>Seller</td>
								<td>&nbsp;&nbsp;:</td>
								<td>&nbsp;&nbsp;<?= $arrsales_member['nama'] . ' (' . $arrsales_member['kode'] . ')' ?></td>
							</tr>
						</table>
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_detail'); ?>" method="Post">
					<?= input_hidden('id', $id) ?>
					<?= input_hidden('account_id', @$arrseller['account_id']) ?>
					<?= input_hidden('tanggal', $arrseller['sales_date']) ?>
					<input type="hidden" id="member_status" value="<?= $arrsales_member['status'] ?>" />
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Produk</th>
								<th>Jumlah</th>
								<th>Harga Satuan</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="x">
							<?php if (@count(@$arrsales_detail) > 0) {
								$i = 0;
								$total = 0;
								foreach ($arrsales_detail as $indexx => $valuex) {

							?>
									<tr id="<?= $i; ?>">

										<?php if (($valuex['connected_quantity'] + $valuex['pending_quantity']) > 0) echo '<input type="hidden" name="input2[' . $i . '][product_id]" value="' . $valuex['product_id'] . '" />'; ?>
										<td><select name="input2[<?= $i; ?>][product_id]" class="PilihBarang form-control md-static" <?= (($valuex['connected_quantity'] + $valuex['pending_quantity']) > 0) ? 'disabled' : '' ?> required>
												<option></option>

												<?php
												foreach ($arrproduct as $indexl => $valuel) {
													echo '<option value="' . $valuel['id_produk'] . '" ' . (($valuex['product_id'] == $valuel['id_produk']) ? 'selected' : '') . '>' . $valuel['nama_produk'] . ' (' . $valuel['klasifikasi'] . ') (stock: ' . $valuel['jumlah'] . ')</option>';
													//echo '<option value="'.$valuel['id_phl'].'" '.(($valuex['employee_id'] == $valuel['id_phl'])?'selected':'').'>'.$valuel['nama_phl'].'</option>';
												?>
												<?php
												} ?>
										<td>
											<input type="number" class="form-control" placeholder="Jumlah" name="input2[<?= $i ?>][sales_detail_quantity]" required id="quantity<?= $i ?>" onkeyup="getTotal(<?= $i ?>);" value="<?= $valuex['sales_detail_quantity']; ?>" min="<?= (($valuex['connected_quantity'] + $valuex['pending_quantity']) > 0) ? ($valuex['connected_quantity'] + $valuex['pending_quantity']) : 0 ?>">
										</td>
										<td><input type="text" class="form-control numeric" placeholder="Harga" name="input2[<?= $i ?>][sales_detail_price]" id="harga<?= $i ?>" onkeyup="getTotal(<?= $i ?>);" required value="<?= $valuex['sales_detail_price']; ?>" <?= (($valuex['connected_quantity'] + $valuex['pending_quantity']) > 0) ? 'readonly' : '' ?>><input type="hidden" id="total<?= $i ?>" class="form-control numeric sum_total" placeholder="Harga" value="<?= $valuex['sales_detail_price'] * $valuex['sales_detail_quantity'] ?>"></td>
										<?php if (($valuex['connected_quantity'] + $valuex['pending_quantity']) > 0) { ?>
											<td></td>
										<?php } else { ?>
											<td><button onclick="myDeleteFunction('<?= $i ?>');getTotal2('<?= $i ?>');" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
										<?php } ?>
									<?php $i++;
									$total += ($valuex['sales_detail_price'] * $valuex['sales_detail_quantity']);
								}
							} else { ?>
									<tr id="0">
										<td><select id="produk0" name="input2[0][product_id]" class="PilihBarang form-control md-static" required onchange="hideSimpan();cekOption();">
												<option></option><?= $product_list ?>
											</select></td>
										<td><input type="text" id="quantity0" name="input2[0][sales_detail_quantity]" value="0" min="0" onkeyup="hideSimpan()" onblur="cekOption();" schema_type="<?= $arrsales_member['schema_type'] ?>" schema_id="<?= $arrsales_member['schema_id'] ?>" produk="produk0" class="form-control static numeric inputQuantity" index="0"></td>
										<td><select id="harga0" name="input2[0][sales_detail_price]" onchange="hideSimpan();getTotal(0);" class="form-control pilihHarga" required></select><input type="hidden" id="total0" class="form-control numeric sum_total" placeholder="Harga" value="0"></td>
										<td><button onclick="myDeleteFunction('0');getTotal2('0');" class="btn btn-danger"><i class="fas fa-trash"></i></button></td>
									</tr>
								<?php } ?>
						</tbody>
						<tbody>
							<tr>
								<td>Total</td>
								<td></td>
								<td colspan="2"><input type="text" id="total" value="<?= @$total ?>" class="form-control static numeric" readonly></td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="btn btn-warning" onclick="myCreateFunction();">Tambah Barang</button>
					<div id="input_diskon"></div>
					<div id="insertHiddenProd"></div>
					<br>
					<table class="table table-striped table-bordered nowrap" id="tablePotHarga" style="font-size: 12px; display: none;">
						<thead>
							<tr>
								<th>No</th>
								<th>Potongan Harga</th>
							</tr>
						</thead>
						<tbody id="potonganHarga">
						</tbody>
					</table>
					<table class="table table-striped table-bordered nowrap" id="tablePotPersen" style="font-size: 12px; display: none;">
						<thead>
							<tr>
								<th>No</th>
								<th>Potongan Persen</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody id="potonganPersen">
						</tbody>
					</table>
					<table class="table table-striped table-bordered nowrap" id="tablePotProduk" style="font-size: 12px; display: none;">
						<thead>
							<tr>
								<th>No</th>
								<th>Produk</th>
								<th>Quantity</th>
							</tr>
						</thead>
						<tbody id="bonusProduk">
						</tbody>
					</table>
					<div class="kt-portlet__foot" style="margin-top: 20px;">
						<div class="kt-form__actions">
							<button type="submit" id="simpan" style="display:none" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;">
								<i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
							</button>
							<a id="diskon" name="diskon" value="diskon" style="display:none;color: #fff;" class="btn btn-success waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;" onclick="chk_diskon()">
								<i class="icofont icofont-ui-edit"></i><span class="m-l-10">Cek Diskon</span>
							</a>
						</div>
					</div>
				</form>


			</div>
		</div>
	</div>
</div>


<script>
	it = <?= ((@count(@$arrsales_detail) > 0) ? count(@$arrsales_detail) : 1); ?>;

	function myCreateFunction() {
		it++;
		var html = '<tr id="' + it + '"><td><select id="produk' + it + '" name="input2[' + it + '][product_id]" class="PilihBarang form-control md-static" required onchange="hideSimpan();cekOption(); getTotal(' + it + ');">		<option></option><?= $product_list ?>	</select></td><td><input type="text" id="quantity' + it + '" name="input2[' + it + '][sales_detail_quantity]" value="0" min="0" onchange="hideSimpan();" onblur="cekOption(); getTotal(' + it + ');" schema_type="<?= $arrsales_member['schema_type'] ?>" schema_id="<?= $arrsales_member['schema_id'] ?>" produk="produk' + it + '" class="form-control static numeric inputQuantity" index="' + it + '"></td><td><select id="harga' + it + '" name="input2[' + it + '][sales_detail_price]" onchange="hideSimpan();getTotal(' + it + ');" class="form-control pilihHarga" required></select><input type="hidden" id="total' + it + '" class="form-control numeric sum_total" placeholder="Harga" value="0"></td><td><button onclick="myDeleteFunction(' + it + ');getTotal2(' + it + ')" class="btn btn-danger"><i class="fas fa-trash"></i></button></td></tr>';
		$('#x').append(html);
		$('.PilihBarang').select2({
			allowClear: true,
			placeholder: 'Pilih Barang',
		});
		$('.pilihHarga').select2({
			allowClear: true,
			placeholder: 'Pilih Harga',
		});
		$(".numeric").mask("#,##0", {
			reverse: true
		});
	}

	function myDeleteFunction(id) {
		$('#' + id).remove();
	}
	var timer;

	function getTotal(i) {
		$('#diskon').hide();
		$('#simpan').hide();
		$('#tablePotProduk').hide();
		$('#tablePotHarga').hide();
		$('#tablePotPersen').hide();
		clearTimeout(timer);
		timer = setTimeout(function validate() {
			if ($('#harga' + i).val() == "") $('#harga' + i).val(0);
			if ($('#quantity' + i).val() == "") $('#quantity' + i).val(0);
			if ($('#harga' + i).val().replaceAll(",", "") > 0 && $('#quantity' + i).val().replaceAll(",", "") > 0) {
				var ttl = parseFloat($('#harga' + i).val().replaceAll(",", "")) * parseFloat($('#quantity' + i).val().replaceAll(",", ""));
			} else {
				var ttl = 0
			}
			$('#total' + i).val(ttl);
			var sum = 0;
			$('.sum_total').each(function() {
				nilai_awal = $(this).val();
				nilai = nilai_awal.replace(",", "");
				if (nilai == "") nilai = 0;
				sum += parseFloat(nilai);
			});
			$('#total').val(commafy(sum));
			$('#diskon').show();
		}, 1000);
	}

	function getTotalAll() {
		$('#diskon').hide();
		$('#simpan').hide();
		$('#tablePotProduk').hide();
		$('#tablePotHarga').hide();
		$('#tablePotPersen').hide();
		clearTimeout(timer);
		timer = setTimeout(function validate() {
			$('.inputQuantity').each(function() {
				i = $(this).attr('index');
				if ($('#harga' + i).val() == "") $('#harga' + i).val(0);
				if ($('#quantity' + i).val() == "") $('#quantity' + i).val(0);
				if ($('#harga' + i).val().replaceAll(",", "") > 0 && $('#quantity' + i).val().replaceAll(",", "") > 0) {
					var ttl = parseFloat($('#harga' + i).val().replaceAll(",", "")) * parseFloat($('#quantity' + i).val().replaceAll(",", ""));
				} else {
					var ttl = 0
				}
				$('#total' + i).val(ttl);
				var sum = 0;
				$('.sum_total').each(function() {
					nilai_awal = $(this).val();
					nilai = nilai_awal.replace(",", "");
					if (nilai == "") nilai = 0;
					sum += parseFloat(nilai);
				});
				$('#total').val(commafy(sum));
				$('#diskon').show();
			});
		}, 1000);
	}

	function getTotal2(i) {
		$('#diskon').hide();
		$('#simpan').hide();
		$('#tablePotProduk').hide();
		$('#tablePotHarga').hide();
		$('#tablePotPersen').hide();

		var sum = 0;
		$('.sum_total').each(function() {
			nilai_awal = $(this).val();
			nilai = nilai_awal.replace(",", "");
			if (nilai == "") nilai = 0;
			sum += parseFloat(nilai);
		});
		$('#total').val(commafy(sum));
		$('#diskon').show();

	}


	function cekOption() {
		var produk = [];
		var quantity = [];
		var schema_type = [];
		var schema_id = [];
		var index = [];
		$('.inputQuantity').each(function() {
			id = $(this).attr('index')
			produk.push($('#produk' + id + ' option:selected').val());
			index.push(id);
			if ($('#quantity' + id).val() == "") $('#quantity' + id).val(0);
			quantity.push($('#quantity' + id).val().replaceAll(",", ""))
			schema_type.push($(this).attr('schema_type'))
			schema_id.push($(this).attr('schema_id'))
		})
		$.ajax({
			type: 'post',
			url: '<?= base_url() ?>T_sales/get_harga',
			data: {
				produk: produk,
				quantity: quantity,
				schema_type: schema_type,
				schema_id: schema_id,
				index: index
			},
			success: function(result) {
				var result2 = JSON.parse(result);
				for (var i in result2) {
					$('#harga' + i).html(result2[i]);
				}
				getTotalAll();
			}
		});
	}

	function chk_diskon() {
		var kode = [];
		var quantity = [];
		var price = [];
		var member_status = $('#member_status').val();
		$('.PilihBarang').each(function() {
			kode.push(this.value);
		})
		$('.inputQuantity').each(function() {
			quantity.push(this.value.replaceAll(",", ""));
		})
		$('.pilihHarga').each(function(index, value) {
			value = quantity[index] * this.value;
			price.push(value);
		})
		$.ajax({
			type: 'post',
			url: '<?= base_url() ?>Cek_diskon/cek_discount',
			data: {
				kode: kode,
				price: price,
				quantity: quantity,
				member_status: member_status
			},
			success: function(result) {
				var result2 = JSON.parse(result);
				$('#input_diskon').html(result2.reward);
				if (result2.list_diskon_harga !== '') {
					$('#potonganHarga').html(result2.list_diskon_harga);
					$('#tablePotHarga').show();
				}
				if (result2.list_diskon_persen !== '') {
					$('#potonganPersen').html(result2.list_diskon_persen)
					$('#tablePotPersen').show();
				}
				if (result2.list_diskon_quantity !== '') {
					$('#bonusProduk').html(result2.list_diskon_quantity)
					$('#tablePotProduk').show();
				}
				if (result2.produk_insert !== '') {
					$('#insertHiddenProd').html(result2.produk_insert)
				} else {
					$('#insertHiddenProd').html('')
				}
				$('#simpan').show();
				$('#diskon').hide();
			}
		});
	}

	$(document).ready(function() {
		$('input[type="number"]').on('blur', function() {
			var v = $(this).val();
			if (v == '') v = 0;
			v = parseInt(v);
			min = parseInt($(this).attr('min'));
			max = parseInt($(this).attr('max'));

			if (v < min) {
				$(this).val(min);
			} else if (v > max) {
				$(this).val(max);
			} else if (v == 0) {
				$(this).val(0)
			}
		})
	})

	function hideSimpan() {
		$('#simpan').hide();
	}
</script>