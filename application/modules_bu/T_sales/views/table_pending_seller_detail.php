							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											Seller : <?= $arrmember['nama'].' ('.$arrmember['kode'].')' ?>
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
										<thead>
											<tr>
											<th>No</th>
											<th>Nama Produk</th>
											<th>Total Pembelian</th> 
											<th>Total Kirim</th> 
											<th>Kurang Kirim / Pendingan</th>  
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 0;
											foreach ($arrpending_seller_detail as $key => $vaData) {
											
											?>
											<tr>
												<td><?= ++$no ?></td>
												<td><?= strtoupper($vaData['nama_produk']) ?></td>
												<td>
													<a href="<?=base_url()?>T_sales/detail_so_seller_detail/<?=$id?>/<?=$vaData['kode']?>"><?= number_format($pendingan = $vaData['jum1'])?></a>
												</td>
												<td>
													<a href="<?=base_url()?>T_sales/detail_pending_seller_detail/<?=$id?>/<?=$vaData['kode']?>"><?=number_format($kirim = $vaData['jum2'])?></a>
												</td>
												<td>
													<?php echo number_format($total = $pendingan - $kirim) ?>
												</td>
											</tr>
											<?php 

											
											  }

											?>
								
										</tbody>
									</table>
								</div>
							</div> 
							