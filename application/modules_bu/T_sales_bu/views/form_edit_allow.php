
							<div class="row">
								<div class="col-lg-12 col-xl-12">
									<div class="kt-portlet">
										<form method="post" action="<?= site_url($url . '/simpan_edit_allow'); ?>" enctype="multipart/form-data">
											<?= input_hidden('id', $id) ?>
											<?= input_hidden('sales_id', $arraccount_detail_sales['sales_id']) ?>
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title"> 
													<table>
														<br>
														<tr>
															<td>Bayar</td>
															<td>&nbsp;&nbsp;:</td>
															<td>&nbsp;&nbsp;Rp <?= @number_format(@$arraccount_detail_sales['account_detail_sales_amount']); ?><input type="hidden" id="paid" value="<?= @$arraccount_detail_sales['account_detail_sales_amount'] ?>"></td>
														</tr>
													</table>
													<br>
												</h3>  
											</div>
										</div>
											<div class="kt-portlet__body">
												
										<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
							<th>No</th>
							<th>NAMA PRODUK</th>
							<th>JUMLAH PESANAN</th>
							<th>BOLEH KIRIM</th>
							<!--<th>KURANG BOLEH KIRIM</th>-->
							
							<th>JUMLAH KIRIM</th>
							<th>TOTAL</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 0;
							foreach ($arrsales_detail as $key => $vaData) {
							?>
							<tr>
								<td><?= ++$no ?></td>
								<td><?= $vaData['nama_produk'] ?> (@<?= number_format($vaData['sales_detail_price']) ?>)</td>
								<td><?= $vaData['sales_detail_quantity'] ?></td>
								<td><?= $vaData['send_allow'] - $arraccount_detail_sales_product[$vaData['sales_detail_id']] ?></td>
								<!--<td><?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] + $arraccount_detail_sales_product[$vaData['sales_detail_id']] ?></td>-->
									
								<td><?= ((@$arraccount_detail_sales_product[$vaData['sales_detail_id']]=="")?0:$arraccount_detail_sales_product[$vaData['sales_detail_id']]); ?>
									<!--<input type="text" name="allow[<?= $vaData['sales_detail_id'] ?>]" price="<?= $vaData['sales_detail_price']; ?>" max="<?= $vaData['sales_detail_quantity'] - $vaData['send_allow'] + $arraccount_detail_sales_product[$vaData['sales_detail_id']] ?>" class="form-control md-static sum_total" value="<?= ((@$arraccount_detail_sales_product[$vaData['sales_detail_id']]=="")?0:$arraccount_detail_sales_product[$vaData['sales_detail_id']]); ?>" id_dex="<?= $key ?>" onkeyup="chk_total()" <?= $arraccount_detail_sales['coa_transaction_confirm'] == 1 ? 'readonly' : ''?>>-->
									<input type="hidden" name="product_id[<?= $vaData['sales_detail_id'] ?>]" value="<?= $vaData['id_produk'] ?>">
								</td>
								<td><div id="<?= $key ?>">Rp <?= number_format($vaData['sales_detail_price']*((@$arraccount_detail_sales_product[$vaData['sales_detail_id']]=="")?0:$arraccount_detail_sales_product[$vaData['sales_detail_id']])) ?></div></td>
							</tr>
							<?php } ?>
				
						</tbody>
					</table>
												
					</div>
										
										

											
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
												<!--<?= $arraccount_detail_sales['coa_transaction_confirm'] == 1 ? '' : '<button type="submit" name="simpan" id="simpan" value="simpan" class="btn btn-primary">Simpan</button>'?>-->
													<a href="<?= site_url($url."/table_detail/".$arraccount_detail_sales['sales_id']); ?>"><span type="reset" class="btn btn-secondary">Kembali</span></a>
												</div>
											</div>
										</form>


</div>
</div>
</div>

<script>
	function chk_total(val){
		var harga = $('#paid').val();
		var price = 0;
		var total_price = 0;
		var total_price2 = 0;
		var dex = "";
		var val_temp = "";
		$('.sum_total').each(function(){			
			val_temp = $(this).val();
			if(val_temp==""){
				val_temp = 0;
			}
			price = $(this).attr("price") * parseFloat(val_temp);
			total_price = total_price + price;
			dex = $(this).attr("id_dex");
			if(parseFloat(val_temp) > parseFloat($(this).attr("max"))){
				$(this).val($(this).attr("max"));
				$('#'+dex).html('Rp '+commafy($(this).attr("max") * $(this).attr("price")));
				total_price2 += parseFloat($(this).attr("max") * $(this).attr("price"));
				
			}else{
				$('#'+dex).html('Rp '+commafy(total_price));
				total_price2 += parseFloat(total_price);
			}
			total_price = "";
			dex = "";
			/*if(parseFloat($(this).val()) > parseFloat($(this).attr("max"))){
				$(this).val($(this).attr("max"));
			}*/
				
		});
		if(total_price2 > harga){
			$('#simpan').hide();
		}else{
			$('#simpan').show();
		}
	}
</script>