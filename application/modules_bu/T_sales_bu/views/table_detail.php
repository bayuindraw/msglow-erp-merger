							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											<table>
												<tr>
													<td>Nomor SO</td>
													<td>&nbsp;&nbsp;:</td>
													<td>&nbsp;&nbsp;<?= $kodeSO = $arrsales_member['sales_code'] ?></td>
												</tr>
												<tr>
													<td>Seller</td>
													<td>&nbsp;&nbsp;:</td>
													<td>&nbsp;&nbsp;<?= $arrsales_member['nama'].' ('.$arrsales_member['kode'].')' ?></td>
												</tr>
											</table>
											
										</h3>
									</div>
									<!--<div class="kt-portlet__head-toolbar xhide">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<a href="<?= site_url().$url."/form_pay_deposit/".$id ?>" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-money"></i>
													Deposit (Rp <?= @number_format($deposit) ?>)
												</a>
											</div>
										</div>
									</div>-->
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
										<thead>
											<tr>
											<th>No</th>
											<th>Nama Produk</th>
											<th>Jumlah</th>
											<th>Boleh Kirim</th>
											<th>Sudah Kirim</th>
											<th>Harga Satuan</th>  
											<th>Sub Total</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$total[] = 0;
											$no = 0;
											foreach ($arrsales_detail as $key => $vaData) {
											?>
											<tr>
												<td><?= ++$no ?></td>
												<td><?= $vaData['nama_produk'] ?></td>
												<td><?= $vaData['sales_detail_quantity'] ?> Pcs</td>
												<td><?= $vaData['connected_quantity'] + $vaData['pending_quantity'] ?> Pcs</td>
												<td><?= $vaData['connected_quantity'] ?> Pcs</td>
												<td align="right">Rp. <?= number_format($vaData['sales_detail_price']) ?> </td>
												<td align="right">Rp. <?= number_format($total[] = $vaData['sales_detail_price']*$vaData['sales_detail_quantity']) ?></td>
											</tr>
											<?php } ?>
									
										</tbody>
										<tfoot>
											<tr>
												<td colspan="4" align="right" style="font-weight: 900">Total Nominal Sales Order</td>
												<td style="font-weight: 900" align="right">Rp. <?=number_format(array_sum($total))?></td>
											</tr>
										</tfoot>
									</table>
									<a href="<?=base_url()?>T_sales/cetak_invoice/<?=$kodeSO?>" target="_blank" class="btn btn-primary waves-effect waves-light"> <i class="fa fa-print"></i> Cetak Invoice</a>	
								</div>
							</div> 
							<?php $total_discount_sales = ((@count($arrsales_discount) != "")?count($arrsales_discount):0);
							if($total_discount_sales > 0){ ?>
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											Diskon 
											
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">
									* B = Boleh Kirim, T = Terkirim, P = Pending
									<!--begin: Datatable -->
									<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
										<thead>
											<tr>
											<th>Nama</th>
											<th>Gratis Barang</th>
											<th>Detail Diskon Harga</th>
											<th>Total Diskon Harga</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$total[] = 0;
											$no = 0;
											foreach ($arrsales_discount as $key => $vaData2) {
											?>
											<tr> 
												<td><?= $vaData2['discount_name'] ?></td>
												<td>
												<?php if($vaData2['sales_discount_product'] == "1"){ 
													foreach($arrsales_discount_product[$vaData2['sales_discount_id']] as $indexsales_discount_product => $valuesales_discount_product){ ?>
													- <?= $valuesales_discount_product['sales_discount_product_target'].' '.$valuesales_discount_product['nama_produk']." (B = ".($valuesales_discount_product['connected_quantity']+$valuesales_discount_product['pending_quantity'])." | T = ".$valuesales_discount_product['connected_quantity']." | P = ".$valuesales_discount_product['pending_quantity']."".")" ?> <br>
												<?php } 
												} ?>
												</td>
											</tr>
											<?php } ?>
									
										</tbody>
									</table>	
								</div>
							</div> 
							<?php } ?>
							<?php 
							$total_detail_sales = ((@count($arraccount_detail_sales) != "")?count($arraccount_detail_sales):0);
							if($total_detail_sales > 0){  
							?> 
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											History Pembayaran
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
										<thead>
											<tr>
											<th>No</th>
											<th>Tanggal</th>
											<!-- <th>Nama Seller</th> -->
											<th>Jumlah</th>
											<th>History Pembayaran</th>
											<th class="xhide">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$total[] = 0;
											$no = 0;
											foreach ($arraccount_detail_sales as $key => $vaData2) {
											?>
											<tr>
												<td><?= ++$no ?></td>
												<td><?= $vaData2['account_detail_sales_date'] ?></td>
												<!-- <td><?= $vaData2['account_detail_sales_date'] ?></td> -->
												<td align="right">Rp. <?= number_format($total2[] = $vaData2['account_detail_sales_amount']) ?></td>
												<td class="xhide"><a href="<?= site_url().$url."/history_pembayaran/".$id."/".$vaData2['account_detail_id'] ?>" class="btn btn-xs btn-success"> <i class="fa fa-list"></i></a></td>
												<td class="xhide"><a href="<?= site_url().$url."/form_edit_allow/".$vaData2['account_detail_sales_id'] ?>" class="btn btn-xs btn-info"> <i class="fa fa-search"></i></a></td>
											</tr>
											<?php } ?>
									
										</tbody>
										<tfoot>
											<tr>
												<td colspan="2" align="right" style="font-weight: 900">Total Bayar</td>
												<td style="font-weight: 900" align="right">Rp. <?=number_format(array_sum($total2))?></td>
												
											</tr>	
											<tr>
												<td align="right" style="font-weight: 900">Kurang Bayar</td>
												<td style="font-weight: 900" align="right">Rp. <?=number_format(array_sum($total)) ?> - Rp. <?=number_format(array_sum($total2))?></td>
												<td style="font-weight: 900" align="right">Rp. <?=number_format(array_sum($total)-array_sum($total2))?></td>
											</tr>
										</tfoot>
									</table>	
								</div>
							</div> 
							<?php } ?>
							
							<!--<script>
								<?php if((array_sum($total)-array_sum($total2)) == 0){ ?>
									$('.xhide').hide();
								<?php } ?>
							</script>-->