<?php
defined('BASEPATH') or exit('No direct script access allowed');


class T_salesd extends CI_Controller
{
	
	var $url_ = "T_salesd";
	var $id_ = "sales_id";
	var $eng_ = "sales";
	var $ind_ = "Penjualan";
	
	public function __construct()
	{
		parent::__construct();
		ob_start();
		$this->load->library('m_pdf');
		$this->load->model('model');

		$this->load->model(array(
			$this->url_.'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}
	
	public function search_member($search = '')
	{
		$this->Model->search_member($search);
	}
	
	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function table_pending_product()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table_pending_product', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function table_pending_product_detail($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table_pending_product_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function table_pending_seller()
	{ 
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['content'] = $this->load->view($this->url_.'/table_pending_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title']; 
		$this->load->view('Layout/home',$data);
	}
	
	public function table_pending_product2($product_id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$datacontent['product_id'] = $product_id;	
		$data['content'] = $this->load->view($this->url_.'/table_pending_product2', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];  
		$this->load->view('Layout/home',$data);
	}
	
	public function get_data_pending_product()
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_product();
	}
	
	public function get_data_pending_product2($product_id)
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_product2($product_id);
	}
	
	public function get_data_pending_product_detail($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_product_detail($id);
	}
	
	public function get_data_pending_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_seller();
	}
	
	public function table_pending_seller_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$datacontent['id'] = $id; 
		$datacontent['arrmember'] = $this->Model->get_member($id);
		$datacontent['arrpending_seller_detail'] = $this->Model->get_pending_seller_detail_newAFIF($id);
		$data['file'] = 'Data Pendingan Seller';
		$data['content'] = $this->load->view($this->url_.'/table_pending_seller_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}

	public function detail_pending_seller_detail($id = '',$idproduk='')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$datacontent['id'] = $id; 
		$datacontent['arrpending_seller_detail'] = $this->Model->detail_pending_seller_detail($id,$idproduk);
		$data['file'] = 'Data Delivery Order';
		$data['content'] = $this->load->view($this->url_.'/table_pending_seller_detail_all', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}

	public function detail_so_seller_detail($id = '',$idproduk='')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$datacontent['id'] = $id; 
		$datacontent['arrpending_seller_detail'] = $this->Model->detail_so_sales_seller_detail($id,$idproduk);
		$data['file'] = 'Data Sales Order ';
		$data['content'] = $this->load->view($this->url_.'/table_so_seller_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data '.$this->ind_;
		$datacontent['parameter'] 	= $parameter;
		$datacontent['id']		 	= $id;
		$datacontent['code'] 		= $this->generate_code();
		$datacontent['arrseller'] 	= $this->Model->get_seller();
		$data['file'] 				= $this->ind_ ;
		$data['content'] 			= $this->load->view($this->url_.'/form', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function form_pay_deposit($id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data '.$this->ind_;
		$datacontent['id']		 	= $id;
		$datacontent['deposit'] 	= $this->Model->get_deposit($id);
		$datacontent['arraccount_detail'] 	= $this->Model->get_account_detail($id);
		$datacontent['arrsales_detail'] 	= $this->Model->get_sales_detail2($id);
		$data['file'] 				= $this->ind_ ;
		$data['content'] 			= $this->load->view($this->url_.'/form_pay_deposit', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function form_edit_allow($id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data '.$this->ind_;
		$datacontent['id']		 	= $id;
		$datacontent['arraccount_detail_sales'] = $this->Model->get_account_detail_sales3($id);
		$arraccount_detail_sales_product = $this->Model->get_account_detail_sales_product($id);
		foreach($arraccount_detail_sales_product as $index => $value){
			$datacontent['arraccount_detail_sales_product'][$value['sales_detail_id']] = $value['account_detail_sales_product_allow'];
			//$datacontent['sales_id'] = $value['sales_id'];
		}
		$datacontent['arrsales_detail'] 	= $this->Model->get_sales_detail2($datacontent['arraccount_detail_sales']['sales_id']);
		$data['file'] 				= $this->ind_ ;
		$data['content'] 			= $this->load->view($this->url_.'/form_edit_allow', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function form_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$datacontent['id'] = $id; 
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		$datacontent['arrseller'] = $this->Model->get_seller($id);
		$datacontent['arrseller'] = $datacontent['arrseller'][0];
		$datacontent['arrproduct'] = $this->Model->get_produk();
		$datacontent['product_list'] = "";
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'].'<option value="'.$value['kode'].'">'.$value['nama_produk'].'</option>';
			//$datacontent['product_list'] = $datacontent['product_list'].'<option value="'.$value['id_produk'].'">'.$value['nama_produk'].' ('.(($value['tipe']=="1")?"Barang Jadi":"Gudang").')</option>';
		}
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_.'/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function table_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$datacontent['id'] = $id; 
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail2($id);
		//$datacontent['deposit'] 	= $this->Model->get_deposit($id);
		//$datacontent['arraccount_detail_sales'] = $this->Model->get_account_detail_sales($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_.'/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function generate_code(){

		 $cFormatTahun  = substr(date('Y'),2,2);
	     $cFormatBulan  = date('m');
	     $cBsdpnp 		= "MSGLOWSO-";
	     $dbDate	=	$this->db->query("SELECT COUNT(sales_id) as JumlahTransaksi FROM t_salesd WHERE LEFT(sales_date_create, 7) = '".date('Y-m')."'");
	     if($dbDate->num_rows() > 0){
	     	foreach ($dbDate->result_array() as $key => $vaDbDate) {
	     		$nJumlahTransaksi = $vaDbDate['JumlahTransaksi']+3;
	     	}
	     }else{
	     		$nJumlahTransaksi = 1;
	     }
	     $panjang = strlen($nJumlahTransaksi);
	     if($panjang == 1){
	     	$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'0000'.$nJumlahTransaksi;
	     }elseif($panjang == 2){
	     	$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'000'.$nJumlahTransaksi;
	     }elseif($panjang == 3){
	     	$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'00'.$nJumlahTransaksi;
		 }elseif($panjang == 4){
	     	$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.'0'.$nJumlahTransaksi;
		 }elseif($panjang == 5){
	     	$cKode = $cBsdpnp.$cFormatTahun.$cFormatBulan.$nJumlahTransaksi;
	     }
	     return $cKode;     
	}
	
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			
			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$data['user_id'] = $this->session->userdata('user_id');
				$data['sales_date_create'] = date('Y-m-d H:i:s');
				$data['sales_category_id'] = $_SESSION['sales_category_id'];
				//if($data['sales_address'] == "") $data['sales_address'] = $this->Model->get_seller_address($data['seller_id']); 
				//$this->Model->chk_account($data['seller_id']); 
				$exec = $this->Model->insert($data);
				$id = $this->Model->insert_id();
			}
		}
		redirect(site_url('T_salesd/form_detail/'.$id));
	}
	
	public function simpan_pay_deposit()
	{
		if ($this->input->post('simpan')) {
			$id = $_POST['id'];
			$account_detail_real_id = $_POST['account_detail_real_id'];
			$paid = str_replace(',', '', $_POST['paid']);
			$date = $_POST['date'];
			$allow = $this->input->post('allow');
			$max = $this->input->post('max');
			$product_max = $this->input->post('product_max');
			$sales_detail_id = $this->input->post('sales_detail_id');
			$product_id = $this->input->post('product_id');
			$data_detail_sales['sales_id'] = $id;
			$data_detail_sales['account_detail_target_real_id'] = $account_detail_real_id;
			$data_detail_sales['account_detail_sales_amount'] = $paid;
			$data_detail_sales['account_detail_sales_date'] = $date;
			//$data_detail_sales['account_detail_id'] = $date;
			$this->Model->insert_account_detail_sales($data_detail_sales);
			$detail_sales_id = $this->Model->insert_id();
			foreach($allow as $index => $value){
				//if($value > 0){
					$data_detail_sales_product['sales_id'] = $id;
					$data_detail_sales_product['product_id'] = $product_id[$index];
					$data_detail_sales_product['sales_detail_id'] = $sales_detail_id[$index];
					if($max == $paid) $data_detail_sales_product['account_detail_sales_product_allow'] = $product_max[$index];
					else $data_detail_sales_product['account_detail_sales_product_allow'] = $value;
					$data_detail_sales_product['account_detail_sales_id'] = $detail_sales_id;
					$this->Model->insert_account_detail_sales_product($data_detail_sales_product);
				//}
			}
			$this->Model->update_deposit($id, $paid);
		}
		redirect(site_url('T_sales/table_detail/'.$id));
	}
	
	public function simpan_edit_allow()
	{
		if ($this->input->post('simpan')) {
			$sales_id = $_POST['sales_id'];
			$id = $_POST['id'];
			$allow = $this->input->post('allow');
			foreach($allow as $index => $value){
				$data['account_detail_sales_product_allow'] = $value;
				$this->Model->update_account_detail_sales_product($data, ['account_detail_sales_id' => $id, 'sales_id' => $sales_id, 'sales_detail_id' => $index]);
			}
		}
		redirect(site_url('T_sales/table_detail/'.$sales_id)); 
	}
	
	public function simpan_detail()
	{
		if ($this->input->post('simpan')) {		
		
			//$data = $this->input->post('input');
			$data2 = $this->input->post('input2');
			$id = $_POST['id'];
			$pending = $this->Model->reset_detail($_POST['id']);
			foreach($data2 as $index => $value){
				$value['sales_detail_price'] = str_replace(',', '', $value['sales_detail_price']);
				$value['sales_detail_quantity'] = str_replace(',', '', $value['sales_detail_quantity']);
				$data = $value;
				$data['sales_id'] = $_POST['id']; 
				$data['user_id'] = $this->session->userdata('user_id');
				$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
				if(@$pending[$value['product_kd']]['sales_detail_id'] != ""){
					if($pending[$value['product_kd']]['sales_detail_quantity_send'] < $value['sales_detail_quantity']){
						$exec = $this->Model->update_detail_quantity($pending[$value['product_kd']]['sales_detail_id'], $value['sales_detail_quantity']);
					}
					$dataxx['sales_detail_price'] = $value['sales_detail_price'];
					$this->Model->update_detail2($dataxx, ['sales_detail_id' => $pending[$value['product_kd']]['sales_detail_id']]);
				}else{
					$exec = $this->Model->insert_detail($data);
				}
			}
		}
		redirect(site_url('T_salesd/table_detail/'.$id));
	}
	
	/*public function simpan_detail()
	{
		if ($this->input->post('simpan')) {		
			$data = $this->input->post('input');
			$data['sales_id'] = $_POST['id'];
			$data['user_id'] = $this->session->userdata('user_id');
			$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
			$arrproduct = $this->Model->get_product($data['product_id']);			
			
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $_POST['tanggal'];
				$datax['account_detail_category_id'] = '81';
				$datax['account_detail_realization'] = 1;
				$datax['account_detail_quantity'] = $data['sales_detail_quantity'];
				$datax['account_detail_price'] = $data['sales_detail_price'];
				$datax['account_detail_note'] = 'Pembelian '.$arrproduct['nama_produk'].' Rp. '.$data['sales_detail_price'].' * '.$data['sales_detail_quantity'].' Pcs (Rp. '.$data['sales_detail_price']*$data['sales_detail_quantity'].')';
				$type = "debit";
				$datax['account_detail_debit'] = $data['sales_detail_price']*$data['sales_detail_quantity'];
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
				$id_account_detail = $this->Model->insert_account_detail($datax); 
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account()->row_array();
				if(@$row['account_date_reset'] > $_POST['tanggal']){
					$this->Model->update_balance($account_id,  $data['sales_detail_price']*$data['sales_detail_quantity'], $type, @$row['account_type_id'], $datax['account_detail_date']);
				}else{
					$this->Model->update_balance($account_id,  $data['sales_detail_price']*$data['sales_detail_quantity'], $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			
			$exec = $this->Model->insert_detail($data);
		}
		redirect(site_url('T_sales/form_detail/'.$_POST['id']));
	}*/
	
	public function simpan_detail_all()
	{
		redirect(site_url('T_sales'));
	}
	
	public function report()
	{
		if(@$_POST['export_pdf']){
			$this->export_pdf();
		}else{
			$datacontent['url'] = $this->url_;
			$datacontent['search'] = $this->input->post('search');
			$datacontent['url2'] = '/'.$datacontent['search']['member_status'].'|'.$datacontent['search']['all'];
			$datacontent['title'] = 'Member';
			$data['file'] = $this->ind_ ;	
			$data['content'] = $this->load->view($this->url_.'/table_report', $datacontent, TRUE);
			$data['title'] = $datacontent['title']; 
			$this->load->view('Layout/home',$data);
		}
	}
	
	public function export_pdf()
	{
		ob_start();
		$datacontent['data'] = $this->Model->get_report()->result_array();
		$this->load->view('M_member/export_pdf', $datacontent);
		//$data['content'] = $this->load->view('m_member/table_report', $datacontent);
		//$data['siswa'] = $this->siswa_model->view_row();
		//$this->load->view('print', $data);
		$html = ob_get_contents();
			ob_end_clean();
			
		require './assets/html2pdf/autoload.php';
		
		$pdf = new Spipu\Html2Pdf\Html2Pdf('P','A4','en');
		$pdf->WriteHTML($html);
		$pdf->Output(date('Y-m-d').'.pdf', 'D');
	}
	
	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}
	
	public function get_data_report($search = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_report($search);
	}
	
	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}
	
	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}
	
	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}
	
	
	
	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer '.$this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data);
	}
	
	public function set_transfer()
	{
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');
				
				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_credit'] = $from['account_detail_debit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2; 
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_credit'], 'credit');
			} else {
				$data[$this->eng_.'_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}
	
	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang '.$this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_ ;
		$data['content'] = $this->load->view($this->url_.'/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home',$data); 
	}
	
	public function set_send()
	{
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');
				
				if($this->input->post('transfer_fee_chk') == "on"){
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_.'_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}
	
	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}
	
	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();
		
		
		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();
		
		if(@$row['account_date_reset'] > $row2[$this->eng2_.'_date']){
			$this->Model->update_balance($id, -1*(($row2['account_detail_debit'] > 0)?$row2['account_detail_debit']:$row2['account_detail_credit']), ($row2['account_detail_debit'] > 0)?'debit':'credit', @$row['account_type_id'], $row2[$this->eng2_.'_date']);
		}else{
			$this->Model->update_balance($id, -1*(($row2['account_detail_debit'] > 0)?$row2['account_detail_debit']:$row2['account_detail_credit']), ($row2['account_detail_debit'] > 0)?'debit':'credit');
		}
		
		
		
		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_."/$id"));
	}
	
		public function list_kecamatan()
	{
		$id_kota = $this->input->post('id_kota');
		$stasiun = $this->Model->get_list_kecamatan($id_kota);
		$lists = "<option value=''>Pilih Kecamatan</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kecamatan . "'>" . $data->nama_kecamatan . "</option>"; 
		}
		$callback = array('list_stasiun' => $lists); 
		echo json_encode($callback); 
	}
	
	public function list_kota()
	{
		$id_provinsi = $this->input->post('id_provinsi');
		$stasiun = $this->Model->get_list_kota($id_provinsi);
		$lists = "<option value=''>Pilih Kota</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kota . "'>" . $data->nama_kota . "</option>"; 
		}
		$callback = array('list_stasiun' => $lists); 
		echo json_encode($callback); 
	}

	public function cetak_invoice($Aksi="")
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$data['aksi'] = $Aksi ;
		$data['title'] = $datacontent['title'];
		$data['arraccount_detail_sales'] = $this->Model->get_account_detail_sales2($Aksi);
		$this->load->view($this->url_.'/inv-msglow',$data);
	}

	public function cetak_invoice_d($Aksi="")
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data '.$this->ind_;
		$data['file'] = $this->ind_ ;	
		$query = $this->model->code("SELECT * FROM t_sales WHERE sales_id = '".$Aksi."'");
		foreach ($query as $key => $vaData) {
			$kodeSales = $vaData['sales_code'];
		}
		$data['arraccount_detail_sales'] = $this->Model->get_account_detail_sales2($kodeSales);
		$data['aksi'] = $kodeSales ;
		$data['title'] = $datacontent['title']; 
		$this->load->view($this->url_.'/inv-msglow',$data);
	}
}
