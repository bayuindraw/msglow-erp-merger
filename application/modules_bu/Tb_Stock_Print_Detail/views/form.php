<?php
?>
<div class="row">
	<div class="col-lg-12 col-xl-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title ?>
					</h3>
				</div>
			</div>

			<div class="kt-portlet__body">
				<div class="form-group">
					<label>Nama Barang : </label> <?= $data_terima_kemasan['nama_kemasan']; ?>
				</div>
				<div class="form-group">
					<label>Supplier : </label> <?= $data_terima_kemasan['nama_supplier']; ?>
				</div>
				<div class="form-group">
					<label>Tanggal : </label> <?= $data_terima_kemasan['tgl_terima']; ?>
				</div>
				<div class="form-group">
					<label>Jumlah Baik : </label> <?= $data_terima_kemasan['jumlah'] - (($data_terima_kemasan['rusak'] == "") ? 0 : $data_terima_kemasan['rusak']); ?>
				</div>
				<div class="form-group">
					<label>Jumlah Rusak : </label> <?= (($data_terima_kemasan['rusak'] == "") ? 0 : $data_terima_kemasan['rusak']) ?>
				</div>
				<div class="form-group">
					<label>Jumlah Total : </label> <?= $data_terima_kemasan['jumlah']; ?>
				</div>
			</div>


		</div>
	</div>
	<div class="col-lg-12 col-xl-12">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Detail Print
					</h3>
				</div>
			</div>
			<form method="post" action="<?= site_url($url . '/simpan'); ?>" enctype="multipart/form-data">
				<?= input_hidden('parameter', $parameter) ?>
				<?= input_hidden('id', $id) ?>
				<?= input_hidden('tanggal', $data_terima_kemasan['tgl_terima']) ?>
				<?= input_hidden('id_barang', $data_terima_kemasan['id_barang']) ?>
				<?= input_hidden('account_id', $data_terima_kemasan['account_id']) ?>
				<div class="kt-portlet__body">
					<div calss="form-group">
						<label>No. Invoice</label>
						<?= input_text('invoice', @$data_terima_kemasan['invoice'], '', 'placeholder = "Invoice"') ?>
					</div><br>
					<div calss="form-group">
						<label>Jumlah Tagihan</label>
						<?= input_text('total_tagihan', @$data_terima_kemasan['total_tagihan'], '', 'placeholder = "Tagihan"') ?>
					</div><br>
					<div calss="form-group">
						<label>HPP</label>
						<?= input_text('input2[1][hpp]', @$data_terima_kemasan['hpp'], '', 'placeholder = "Hpp"') ?>
					</div><br>
					<div calss="form-group">
						<label>Tipe</label>
						<select class="form-control" id="tipe" name="tipe" required>
							<option value="">-Pilih Kategori-</option>
							<option value="tipe" <?= @$data_terima_kemasan['tipe'], ''; ?>>PRINT</option>
						</select>
					</div><br>
					<?= input_hidden('input2[1][baik]', ($data_terima_kemasan['jumlah'] - (($data_terima_kemasan['rusak'] == "") ? 0 : $data_terima_kemasan['rusak']))) ?>
					<?= input_hidden('input2[1][rusak]', (($data_terima_kemasan['rusak'] == "") ? 0 : $data_terima_kemasan['rusak'])) ?>
					<!--<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
                            <thead>
                              <tr>
                                <th>Baik</th>
                                <th>Rusak</th>
                                <th>Hpp</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id="x">
								<?php
								$i = 0;
								foreach ($row as $value) {
									$i++;
								?>
									<tr id="<?= $i; ?>">
										<td><input type="text" class="form-control" placeholder="Nama" name="input2[<?= $i ?>][baik]" value="<?= @$value['baik'] ?>" required></td>
										<td><input type="text" class="form-control" placeholder="Nama" name="input2[<?= $i ?>][rusak]" value="<?= @$value['rusak'] ?>" required></td>
										<td><input type="text" class="form-control" placeholder="Nama" name="input2[<?= $i ?>][hpp]" value="<?= @$value['hpp'] ?>" required></td> 
										<td><button onclick="myDeleteFunction('1')"></button></td>
									</tr>
								<?php } ?>
                            </tbody>
                         </table>
						 <button onclick="myCreateFunction()">Create row</button>-->
				</div>


				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<button type="submit" name="simpan" value="simpan" class="btn btn-primary">Simpan</button>
						<a href="<?= site_url($url); ?>"><span type="reset" class="btn btn-secondary">Batalkan</span></a>
					</div>
				</div>
			</form>


		</div>
	</div>
</div>
<script>
	it = <?= count($row) ?>;

	function myCreateFunction() {
		it++;
		var html = '<tr id="' + it + '"><td><input type="text" class="form-control" placeholder="Nama" name="input2[' + it + '][baik]" value="<?= @$row['telepon'] ?>" required></td><td><input type="text" class="form-control" placeholder="Nama" name="input2[' + it + '][rusak]" value="<?= @$row['telepon'] ?>" required></td><td><input type="text" class="form-control" placeholder="Nama" name="input2[' + it + '][hpp]" value="<?= @$row['telepon'] ?>" required></td><td><button onclick="myDeleteFunction(' + it + ')"></button></td></tr>';
		$('#x').append(html);
	}

	function myDeleteFunction(id) {
		$('#' + id).remove();
	}
</script>