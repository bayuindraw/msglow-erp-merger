<style type="text/css">
    table{width:100%;}
    table, table td, table th{
        border-collapse: collapse;
        border: solid 1px #ababab;
    }
    .text-dark, table td{
        color: #343a40;
    }
    .text-white{
        color: #ffffff;
    }
    table td,
    table th {
        font-size: 11px;
        padding: 3px;
        line-height: 1.2;
        font-family:arial;
    }

    .bg-dark {
        background-color: #343a40;
    }
    .bg-secondary {
        background-color: #6c757d;
    }
    .bg-white {
        background-color: #ffffff;
    }
    .text-left {
        text-align: left;
    }
    .text-right {
        text-align: right;
    }
    .text-center {
        text-align: center;
    }
    </style>
	<table>
		<thead>
			<tr>
				<th class="bg-dark text-white" align="center">No</th>
				<th align="center" class="bg-dark text-white">ID Member</th>
				<th align="center" class="bg-dark text-white">Nama Member</th>
				<th align="center" class="bg-dark text-white">Telepon</th>
				<th align="center" class="bg-dark text-white">Kota</th>
				<th align="center" class="bg-dark text-white">Status</th>
				<th align="center" class="bg-dark text-white">Approval Date</th>
				<th align="center" class="bg-dark text-white">Update Membership</th>
				<th align="center" class="bg-dark text-white">Update Date</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no = 1;
			foreach($data as $key => $value){ ?>				
				<?php if(fmod($no, 2)){ ?><tr>
				<?php }else{ ?><tr style="background-color: #f2f2f2;">
				<?php } ?>
					<td align="center"><?= $no ?></td>
					<td align="center"><?= $value['member_code'] ?></td>
					<td align="center"><?= $value['member_name'] ?></td>
					<td align="center"><?= $value['member_phone'] ?></td>					
					<td align="center"><?= $value['nama_kota'] ?></td>
					<td align="center"><?= $value['member_status_name'] ?></td>
					<td align="center"><?= $value['member_date_approve'] ?></td>
					<td align="center"><?= $value['member_date_membership'] ?></td>	
					<td align="center"><?= $value['member_date_update'] ?></td>	
				</tr>
				
			<?php
				$no++;
			} ?>
		</tbody>
	</table>
