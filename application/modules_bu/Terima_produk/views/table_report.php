							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											DATA PRIMER DAFTAR MEMBER
										</h3>
									</div>
									
								</div>
								<div class="x_content">
									<form method="post" action="<?= site_url($url . '/report'); ?>" enctype="multipart/form-data">
										<div class="kt-portlet__body">
			
	<div class="form-group">
		<label>PILIH STATUS SELLER</label>
											<select class="custom-select form-control" name="search[member_status]" id="member_status">
												<option value="">Pilih Status Seller</option> 
												<option value="1" <?= (('1' == @$search['member_status'])?'selected':'') ?>>Distributor</option>
												<option value="2"<?= (('2' == @$search['member_status'])?'selected':'') ?>>Agen</option>
												<option value="3"<?= (('3' == @$search['member_status'])?'selected':'') ?>>Member</option>
												<option value="4"<?= (('4' == @$search['member_status'])?'selected':'') ?>>Seller</option>
												<?php foreach ($kecamatan as $key2 => $value2) { // Lakukan looping pada variabel siswa dari controller
														echo "<option value='" . $key2 . "'".(($key2 == @$search['member_status'])?'selected':'').">" . $value2 . "</option>";
												} ?>
											</select>
	</div>
	<div class="form-group">
		<label>PENCARIAN DATA SELLER</label>
											<?= input_text('search[all]', @$search['all'], '', 'placeholder = "Masukkan id seller, nama, telepon atau alamat"') ?>
	</div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" name="simpan" value="true" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
													<button type="submit" name="export_pdf" value="true" class="btn btn-warning"><i class="fa fa-print"></i> Cetak</button>
												</div>
											</div>
									</form>
								  </div>
							</div>
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<!--<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>-->
										<h3 class="kt-portlet__head-title">
											<?= $title ?>
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<a href="<?= site_url().$url."/form/tambah" ?>" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Tambah
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>No</th>
												<th>ID Member</th>
												<th>Nama Member</th>
												<th>Telepon</th>
												<th>Kota</th>
												<th>Status</th>
												<th>Induk</th>
												<th>Tanggal Pengajuan</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>							
							
<script type="text/javascript">
	$(document).ready(function() {
		$('#kt_table_1').DataTable({
			"pagingType": "full_numbers",scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
		"processing": true,
		"serverSide": true,
		"ajax": "<?= site_url() ?>/<?= $url ?>/get_data_report<?= $url2 ?>"
		});
	});
</script>