<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Terima_produk_accounting extends CI_Controller
{

	var $url_ = "Terima_produk_accounting";
	var $id_ = "id_terima_kemasan";
	var $eng_ = "Terima Produk";
	var $ind_ = "Terima Produk";

	public function __construct()
	{
		parent::__construct();
		ob_start();
		$this->load->library('m_pdf');
		$this->load->model('model');

		$this->load->model(array(
			$this->url_ . 'Model'  =>  'Model',
		));
		date_default_timezone_set("Asia/Jakarta");
	}

	function formatin_date($tanggal)
	{
		$arrbln = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		);
		return (int)substr($tanggal, 8, 2) . " " . $arrbln[substr($tanggal, 5, 2)] . " " . substr($tanggal, 0, 4);
	}

	public function search_member($search = '')
	{
		$this->Model->search_member($search);
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Draft ' . $this->ind_;
		$sudah_terima = $this->db->query("SELECT * FROM terima_produk WHERE invoice IS NOT NULL")->result_array();
		$datacontent['sudah_terima'] = count($sudah_terima);
		$belum_terima = $this->db->query("SELECT * FROM terima_produk WHERE invoice IS NULL")->result_array();
		$datacontent['belum_terima'] = count($belum_terima);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_non_sj($id)
	{
		$datacontent['id'] = $id;
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data Persetujuan Surat Jalan';
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_non_sj', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_realisasi($xid = '')
	{
		$arrid = explode('%7C', $xid);
		$arrid[0] = str_replace('~', ',', str_replace(':', '/', str_replace('%60', '`', str_replace('%20', ' ', $arrid[0]))));
		$datacontent['url'] 		= $this->url_;
		$datacontent['id_factory']		 	= $arrid['1'];
		$datacontent['sj']		 	= $arrid['0'];
		$datacontent['arrfactory'] 	= $this->Model->get_factory($arrid['1']);
		$datacontent['funct'] 	= $this;
		$datacontent['title'] 		= 'Form Realisasi ' . $arrid['0'] . ' (' . $datacontent['arrfactory']['nama_factory'] . ')';
		$arrterima_produk 	= $this->Model->get_terima_produk($arrid['1'], $arrid['0']);
		foreach ($arrterima_produk as $index => $value) {
			$datacontent['arrterima_produk'][$value['id_barang']] = $value;
			$datacontent['arrdetail_terima_produk'][$value['id_barang']][$value['id_terima_kemasan']] = $value;
		}
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view($this->url_ . '/form_realisasi', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data()
	{
		$datacontent['datatable'] = $this->Model->get_data();
	}

	public function get_data_non_sj($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_non_sj($id);
	}

	public function table_pending_product()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_pending_product', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_pending_product_detail($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_pending_product_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_pending_seller()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_pending_seller', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function get_data_pending_product()
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_product();
	}

	public function get_data_pending_product_detail($id)
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_product_detail($id);
	}

	public function get_data_pending_seller()
	{
		$datacontent['datatable'] = $this->Model->get_data_pending_seller();
	}

	public function table_pending_seller_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrmember'] = $this->Model->get_member($id);
		$datacontent['arrpending_seller_detail'] = $this->Model->get_pending_seller_detail($id);
		$data['file'] = 'Data Pendingan Seller';
		$data['content'] = $this->load->view($this->url_ . '/table_pending_seller_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form($parameter = '', $id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['parameter'] 	= $parameter;
		$datacontent['id']		 	= $id;
		$datacontent['code'] 		= $this->generate_code();
		$datacontent['arrseller'] 	= $this->Model->get_seller();
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_pay_deposit($id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['id']		 	= $id;
		$datacontent['deposit'] 	= $this->Model->get_deposit($id);
		$datacontent['arraccount_detail'] 	= $this->Model->get_account_detail($id);
		$datacontent['arrsales_detail'] 	= $this->Model->get_sales_detail2($id);
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view($this->url_ . '/form_pay_deposit', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_edit_allow($id = '')
	{
		$datacontent['url'] 		= $this->url_;
		$datacontent['title'] 		= 'Data ' . $this->ind_;
		$datacontent['id']		 	= $id;
		$datacontent['arraccount_detail_sales'] = $this->Model->get_account_detail_sales3($id);
		$arraccount_detail_sales_product = $this->Model->get_account_detail_sales_product($id);
		foreach ($arraccount_detail_sales_product as $index => $value) {
			$datacontent['arraccount_detail_sales_product'][$value['sales_detail_id']] = $value['account_detail_sales_product_allow'];
			//$datacontent['sales_id'] = $value['sales_id'];
		}
		$datacontent['arrsales_detail'] 	= $this->Model->get_sales_detail2($datacontent['arraccount_detail_sales']['sales_id']);
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view($this->url_ . '/form_edit_allow', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function form_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		$datacontent['arrseller'] = $this->Model->get_seller($id);
		$datacontent['arrseller'] = $datacontent['arrseller'][0];
		$datacontent['arrproduct'] = $this->Model->get_produk();
		$datacontent['product_list'] = "";
		foreach ($datacontent['arrproduct'] as $key => $value) {
			$datacontent['product_list'] = $datacontent['product_list'] . '<option value="' . $value['id_produk'] . '">' . $value['nama_produk'] . ' (' . $value['klasifikasi'] . ') (stock: ' . $value['jumlah'] . ')</option>';
			//$datacontent['product_list'] = $datacontent['product_list'].'<option value="'.$value['id_produk'].'">'.$value['nama_produk'].' ('.(($value['tipe']=="1")?"Barang Jadi":"Gudang").')</option>';
		}
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function table_detail($id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$datacontent['id'] = $id;
		$datacontent['arrsales_member'] = $this->Model->get_sales_member($id);
		$datacontent['arrsales_detail'] = $this->Model->get_sales_detail2($id);
		$datacontent['deposit'] 	= $this->Model->get_deposit($id);
		$datacontent['arraccount_detail_sales'] = $this->Model->get_account_detail_sales($id);
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/table_detail', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function generate_code()
	{

		$cFormatTahun  = substr(date('Y'), 2, 2);
		$cFormatBulan  = date('m');
		$cBsdpnp 		= "MSGLOWSO-";
		$dbDate	=	$this->db->query("SELECT COUNT(sales_id) as JumlahTransaksi FROM t_sales WHERE LEFT(sales_date, 7) = '" . date('Y-m') . "'");
		if ($dbDate->num_rows() > 0) {
			foreach ($dbDate->result_array() as $key => $vaDbDate) {
				$nJumlahTransaksi = $vaDbDate['JumlahTransaksi'] + 1;
			}
		} else {
			$nJumlahTransaksi = 1;
		}
		$panjang = strlen($nJumlahTransaksi);
		if ($panjang == 1) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0000' . $nJumlahTransaksi;
		} elseif ($panjang == 2) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '000' . $nJumlahTransaksi;
		} elseif ($panjang == 3) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '00' . $nJumlahTransaksi;
		} elseif ($panjang == 4) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . '0' . $nJumlahTransaksi;
		} elseif ($panjang == 5) {
			$cKode = $cBsdpnp . $cFormatTahun . $cFormatBulan . $nJumlahTransaksi;
		}
		return $cKode;
	}

	public function simpan()
	{
		if ($this->input->post('simpan')) {

			$data = $this->input->post('input');
			if ($_POST['parameter'] == "tambah") {
				$data['user_id'] = $this->session->userdata('user_id');
				$data['sales_date_create'] = date('Y-m-d H:i:s');
				$data['sales_category_id'] = $_SESSION['sales_category_id'];
				//if($data['sales_address'] == "") $data['sales_address'] = $this->Model->get_seller_address($data['seller_id']); 
				$this->Model->chk_account($data['seller_id']);
				$exec = $this->Model->insert($data);
				$id = $this->Model->insert_id();
			}
		}
		redirect(site_url('T_sales/form_detail/' . $id));
	}

	public function simpan_realisasi()
	{
		if ($this->input->post('simpan')) {
			$tgl = $this->input->post('tgl');
			$tgl_asli = $this->input->post('tgl_asli');
			$jml = $this->input->post('jml');
			$jml_asli = $this->input->post('jml_asli');
			$id_barang = $this->input->post('id_barang');
			foreach ($jml as $index => $value) {
				$data['tgl_terima'] = $tgl[$index];
				$data['realisasi_date'] = date('Y-m-d H:i:s');
				if ($jml[$index] == "") $jml[$index] = 0;
				if ($tgl[$index] == "") $tgl[$index] = date('Y-m-d');
				//$this->Model->update_draft($data, [$this->id_ => $index]);
				$this->Model->set_realisasi($index, $id_barang[$index], $jml[$index], $tgl[$index], $jml_asli[$index], $tgl_asli[$index]);
			}
		}
		redirect(site_url('Terima_produk'));
	}

	public function simpan_pay_deposit()
	{
		if ($this->input->post('simpan')) {
			$id = $_POST['id'];
			$account_detail_real_id = $_POST['account_detail_real_id'];
			$paid = str_replace(',', '', $_POST['paid']);
			$date = $_POST['date'];
			$allow = $this->input->post('allow');
			$max = $this->input->post('max');
			$product_max = $this->input->post('product_max');
			$sales_detail_id = $this->input->post('sales_detail_id');
			$product_id = $this->input->post('product_id');
			$data_detail_sales['sales_id'] = $id;
			$data_detail_sales['account_detail_target_real_id'] = $account_detail_real_id;
			$data_detail_sales['account_detail_sales_amount'] = $paid;
			$data_detail_sales['account_detail_sales_date'] = $date;
			//$data_detail_sales['account_detail_id'] = $date;
			$this->Model->insert_account_detail_sales($data_detail_sales);
			$detail_sales_id = $this->Model->insert_id();
			foreach ($allow as $index => $value) {
				//if($value > 0){
				$data_detail_sales_product['sales_id'] = $id;
				$data_detail_sales_product['product_id'] = $product_id[$index];
				$data_detail_sales_product['sales_detail_id'] = $sales_detail_id[$index];
				if ($max == $paid) $data_detail_sales_product['account_detail_sales_product_allow'] = $product_max[$index];
				else $data_detail_sales_product['account_detail_sales_product_allow'] = $value;
				$data_detail_sales_product['account_detail_sales_id'] = $detail_sales_id;
				$this->Model->insert_account_detail_sales_product($data_detail_sales_product);
				//}
			}
			$this->Model->update_deposit($id, $paid);
		}
		redirect(site_url('T_sales/table_detail/' . $id));
	}

	public function simpan_edit_allow()
	{
		if ($this->input->post('simpan')) {
			$sales_id = $_POST['sales_id'];
			$id = $_POST['id'];
			$allow = $this->input->post('allow');
			foreach ($allow as $index => $value) {
				$data['account_detail_sales_product_allow'] = $value;
				$this->Model->update_account_detail_sales_product($data, ['account_detail_sales_id' => $id, 'sales_id' => $sales_id, 'sales_detail_id' => $index]);
			}
		}
		redirect(site_url('T_sales/table_detail/' . $sales_id));
	}

	public function simpan_detail()
	{
		if ($this->input->post('simpan')) {

			//$data = $this->input->post('input');
			$data2 = $this->input->post('input2');
			$id = $_POST['id'];
			$pending = $this->Model->reset_detail($_POST['id']);
			foreach ($data2 as $index => $value) {
				$value['sales_detail_price'] = str_replace(',', '', $value['sales_detail_price']);
				$value['sales_detail_quantity'] = str_replace(',', '', $value['sales_detail_quantity']);
				$data = $value;
				$data['sales_id'] = $_POST['id'];
				$data['user_id'] = $this->session->userdata('user_id');
				$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
				if (@$pending[$value['product_id']]['sales_detail_id'] != "") {
					if ($pending[$value['product_id']]['sales_detail_quantity_send'] < $value['sales_detail_quantity']) {
						$exec = $this->Model->update_detail_quantity($pending[$value['product_id']]['sales_detail_id'], $value['sales_detail_quantity']);
					}
					$dataxx['sales_detail_price'] = $value['sales_detail_price'];
					$this->Model->update_detail2($dataxx, ['sales_detail_id' => $pending[$value['product_id']]['sales_detail_id']]);
				} else {
					$exec = $this->Model->insert_detail($data);
				}
			}
			$total_price = $this->Model->get_sales_detail_total_price($id);
			$chk_account_detail = $this->Model->get_account_detail($id);
			if ($chk_account_detail['account_id'] != "") {
				$arrsales = $this->Model->get_sales($id);
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();

				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account($account_id)->row_array();
				$type = "debit";
				if (@$row['account_date_reset'] > $chk_account_detail['account_detail_date']) {
					$this->Model->reset_balance($account_id,  $chk_account_detail['account_detail_' . $type], $type, @$row['account_type_id'], $chk_account_detail['account_detail_date']);
				} else {
					$this->Model->reset_balance($account_id,  $chk_account_detail['account_detail_' . $type], $type);
				}
				//$this->Model->delete_account_detail($chk_account_detail['account_detail_real_id']);

				//$datax['account_id'] = $account_id;
				//$datax['account_detail_id'] = $id2;
				//$datax['account_detail_type_id'] = 3;
				//$datax['account_detail_user_id'] = $_SESSION['user_id'];
				//$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrsales['sales_date'];
				//$datax['account_detail_category_id'] = '81';
				//$datax['account_detail_realization'] = 1;
				//$datax['account_detail_quantity'] = ;
				//$datax['sales_id'] = $id;
				$datax['account_detail_price'] = $total_price;
				//$datax['account_detail_note'] = 'Pembelian dengan sales order : '.$arrsales['sales_code'];
				//$type = "debit";
				$datax['account_detail_debit'] = $total_price;
				//$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
				$id_account_detail = $this->Model->update_account_detail($datax, ['account_detail_real_id' => $chk_account_detail['account_detail_real_id']]);

				if (@$row['account_date_reset'] > $arrsales['sales_date']) {
					$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $total_price, $type);
				}
				//$data['account_detail_real_id'] = $id_account_detail;
			} else {
				$arrsales = $this->Model->get_sales($id);
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $arrsales['sales_date'];
				$datax['account_detail_category_id'] = '81';
				$datax['account_detail_realization'] = 1;
				//$datax['account_detail_quantity'] = ;
				$datax['sales_id'] = $id;
				$datax['account_detail_price'] = $total_price;
				$datax['account_detail_note'] = 'Pembelian dengan sales order : ' . $arrsales['sales_code'];
				$type = "debit";
				$datax['account_detail_debit'] = $total_price;
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');
				$id_account_detail = $this->Model->insert_account_detail($datax);
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account($account_id)->row_array();
				if (@$row['account_date_reset'] > $arrsales['sales_date']) {
					$this->Model->update_balance($account_id,  $total_price, $type, @$row['account_type_id'], $datax['account_detail_date']);
				} else {
					$this->Model->update_balance($account_id,  $total_price, $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			}
		}
		redirect(site_url('T_sales/table_detail/' . $id));
	}

	/*public function simpan_detail()
	{
		if ($this->input->post('simpan')) {		
			$data = $this->input->post('input');
			$data['sales_id'] = $_POST['id'];
			$data['user_id'] = $this->session->userdata('user_id');
			$data['sales_detail_date_create'] = date('Y-m-d H:i:s');
			$arrproduct = $this->Model->get_product($data['product_id']);			
			
				$account_id = $this->input->post('account_id');
				$id2 = $this->Model->get_max_id();
				$datax['account_id'] = $account_id;
				$datax['account_detail_id'] = $id2;
				$datax['account_detail_type_id'] = 3;
				$datax['account_detail_user_id'] = $_SESSION['user_id'];
				$datax['account_detail_pic'] = $_SESSION['user_fullname'];
				$datax['account_detail_date'] = $_POST['tanggal'];
				$datax['account_detail_category_id'] = '81';
				$datax['account_detail_realization'] = 1;
				$datax['account_detail_quantity'] = $data['sales_detail_quantity'];
				$datax['account_detail_price'] = $data['sales_detail_price'];
				$datax['account_detail_note'] = 'Pembelian '.$arrproduct['nama_produk'].' Rp. '.$data['sales_detail_price'].' * '.$data['sales_detail_quantity'].' Pcs (Rp. '.$data['sales_detail_price']*$data['sales_detail_quantity'].')';
				$type = "debit";
				$datax['account_detail_debit'] = $data['sales_detail_price']*$data['sales_detail_quantity'];
				$datax['account_detail_date_create'] = date('Y-m-d H:i:s');  
				$id_account_detail = $this->Model->insert_account_detail($datax); 
				$this->db->where('account_id', $account_id);
				$row = $this->Model->get_m_account()->row_array();
				if(@$row['account_date_reset'] > $_POST['tanggal']){
					$this->Model->update_balance($account_id,  $data['sales_detail_price']*$data['sales_detail_quantity'], $type, @$row['account_type_id'], $datax['account_detail_date']);
				}else{
					$this->Model->update_balance($account_id,  $data['sales_detail_price']*$data['sales_detail_quantity'], $type);
				}
				$data['account_detail_real_id'] = $id_account_detail;
			
			$exec = $this->Model->insert_detail($data);
		}
		redirect(site_url('T_sales/form_detail/'.$_POST['id']));
	}*/

	public function simpan_detail_all()
	{
		redirect(site_url('T_sales'));
	}

	public function report()
	{
		if (@$_POST['export_pdf']) {
			$this->export_pdf();
		} else {
			$datacontent['url'] = $this->url_;
			$datacontent['search'] = $this->input->post('search');
			$datacontent['url2'] = '/' . $datacontent['search']['member_status'] . '|' . $datacontent['search']['all'];
			$datacontent['title'] = 'Member';
			$data['file'] = $this->ind_;
			$data['content'] = $this->load->view($this->url_ . '/table_report', $datacontent, TRUE);
			$data['title'] = $datacontent['title'];
			$this->load->view('Layout/home', $data);
		}
	}

	public function export_pdf()
	{
		ob_start();
		$datacontent['data'] = $this->Model->get_report()->result_array();
		$this->load->view('M_member/export_pdf', $datacontent);
		//$data['content'] = $this->load->view('m_member/table_report', $datacontent);
		//$data['siswa'] = $this->siswa_model->view_row();
		//$this->load->view('print', $data);
		$html = ob_get_contents();
		ob_end_clean();

		require './assets/html2pdf/autoload.php';

		$pdf = new Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
		$pdf->WriteHTML($html);
		$pdf->Output(date('Y-m-d') . '.pdf', 'D');
	}


	public function get_data_report($search = '')
	{
		$datacontent['datatable'] = $this->Model->get_data_report($search);
	}

	function get_list_kecamatan($id_kota)
	{
		$result = $this->db->query("SELECT * FROM mst_kecamatan A WHERE A.id_kota = '$id_kota'")->result();
		return $result;
	}

	function get_list_provinsi()
	{
		$result = $this->db->query("SELECT * FROM mst_provinsi A")->result();
		return $result;
	}

	function get_list_kota($id_provinsi)
	{
		$result = $this->db->query("SELECT * FROM mst_kota A WHERE A.id_provinsi = '$id_provinsi'")->result();
		return $result;
	}



	public function transfer($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Transfer ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/transfer', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_transfer()
	{
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {

				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				$to['account_detail_id'] = $from['account_detail_id'];
				$to['account_detail_pic'] = 'Budi';
				$to['account_detail_credit'] = 0;
				$to['account_id'] = $account_id;
				$to['account_detail_credit'] = $from['account_detail_debit'];
				$to['account_detail_pic'] = $from['account_detail_pic'];
				$to['account_detail_note'] = $from['account_detail_note'];
				$to['account_detail_date'] = $from['account_detail_date'];
				$to['account_detail_category_id'] = 2;
				$to['account_detail_type_id'] = 1;
				$to['account_detail_user_id'] = $from['account_detail_user_id'];
				$to['account_detail_date_create'] = $from['account_detail_date_create'];
				$exec = $this->Model->insert_detail($to);
				$this->Model->update_balance($to['account_id'], $to['account_detail_credit'], 'credit');
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}

	public function send($parameter = '', $id = '')
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Terima Barang ' . $this->ind_;
		$datacontent['parameter'] = $parameter;
		$datacontent['m_account'] = $this->Model->get_list_account();
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/send', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_send()
	{
		$id = $this->Model->get_max_id();
		$account_id = $this->Model->get_account_id($_POST['id']);
		if ($this->input->post('simpan')) {
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($_POST['parameter'] == "tambah") {
				$from['account_id'] = $account_id;
				$from['account_detail_id'] = $id;
				$from['account_detail_category_id'] = 1;
				$from['account_detail_type_id'] = 1;
				$from['account_detail_user_id'] = $_SESSION['user_id'];
				$from['account_detail_date_create'] = date('Y-m-d H:i:s');
				$exec = $this->Model->insert_detail($from);
				$this->Model->update_balance($from['account_id'], $from['account_detail_debit'], 'debit');

				if ($this->input->post('transfer_fee_chk') == "on") {
					$transfer_fee = $this->input->post('transfer_fee');
					$transfer_fee['account_detail_id'] = $from['account_detail_id'];
					$transfer_fee['account_detail_category_id'] = 3;
					$transfer_fee['account_detail_type_id'] = 1;
					$transfer_fee['account_detail_pic'] = $from['account_detail_pic'];
					$transfer_fee['account_detail_date'] = $from['account_detail_date'];
					$transfer_fee['account_detail_user_id'] = $from['account_detail_user_id'];
					$transfer_fee['account_detail_date_create'] = $from['account_detail_date_create'];
					$exec = $this->Model->insert_detail($transfer_fee);
					$this->Model->update_balance($transfer_fee['account_id'], $transfer_fee['account_detail_credit'], 'credit');
				}
			} else {
				$data[$this->eng_ . '_date_update'] = date('Y-m-d H:i:s');
				$this->Model->update($data, [$this->id_ => $this->input->post('id')]);
			}
		}

		redirect(site_url($this->url_));
	}

	public function hapus($id = '')
	{
		$this->Model->delete([$this->id_ => $id]);
		redirect($this->url_);
	}

	public function hapus_detail($id = '', $id_detail = '')
	{
		$this->db->where('account_id', $id);
		$row = $this->Model->get()->row_array();


		$this->db->where('account_detail_id', $id_detail);
		$row2 = $this->Model->get_detail($id)->row_array();

		if (@$row['account_date_reset'] > $row2[$this->eng2_ . '_date']) {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit', @$row['account_type_id'], $row2[$this->eng2_ . '_date']);
		} else {
			$this->Model->update_balance($id, -1 * (($row2['account_detail_debit'] > 0) ? $row2['account_detail_debit'] : $row2['account_detail_credit']), ($row2['account_detail_debit'] > 0) ? 'debit' : 'credit');
		}



		$this->Model->delete_detail([$this->id_ => $id, $this->id2_ => $id_detail]);
		redirect(site_url($this->url2_ . "/$id"));
	}

	public function list_kecamatan()
	{
		$id_kota = $this->input->post('id_kota');
		$stasiun = $this->Model->get_list_kecamatan($id_kota);
		$lists = "<option value=''>Pilih Kecamatan</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kecamatan . "'>" . $data->nama_kecamatan . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function list_kota()
	{
		$id_provinsi = $this->input->post('id_provinsi');
		$stasiun = $this->Model->get_list_kota($id_provinsi);
		$lists = "<option value=''>Pilih Kota</option>";
		foreach ($stasiun as $data) {
			$lists .= "<option value='" . $data->id_kota . "'>" . $data->nama_kota . "</option>";
		}
		$callback = array('list_stasiun' => $lists);
		echo json_encode($callback);
	}

	public function cetak_invoice($Aksi = "")
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$data['aksi'] = $Aksi;
		$data['title'] = $datacontent['title'];
		$data['arraccount_detail_sales'] = $this->Model->get_account_detail_sales2($Aksi);
		$this->load->view($this->url_ . '/inv-msglow', $data);
	}

	public function cetak_invoice_d($Aksi = "")
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Data ' . $this->ind_;
		$data['file'] = $this->ind_;
		$query = $this->model->code("SELECT * FROM t_sales WHERE sales_id = '" . $Aksi . "'");
		foreach ($query as $key => $vaData) {
			$kodeSales = $vaData['sales_code'];
		}
		$data['arraccount_detail_sales'] = $this->Model->get_account_detail_sales2($kodeSales);
		$data['aksi'] = $kodeSales;
		$data['title'] = $datacontent['title'];
		$this->load->view($this->url_ . '/inv-msglow', $data);
	}

	public function form_bill($sj)
	{
		$arrid = explode('%7C', $sj);
		$arrid[0] = str_replace('~', ',', str_replace(':', '/', str_replace('%60', '`', str_replace('%20', ' ', $arrid[0]))));
		$datacontent['url'] 		= $this->url_;
		$datacontent['id_factory']	= $arrid['1'];
		$datacontent['sj']		 	= $arrid['0'];
		$datacontent['arrfactory'] 	= $this->Model->get_factory($arrid['1']);
		$datacontent['funct'] 	= $this;
		$datacontent['title'] 		= 'Form Tagihan ' . $arrid['0'] . ' (' . $datacontent['arrfactory']['nama_factory'] . ')';
		$arrterima_produk 	= $this->Model->get_terima_produk($arrid['1'], $arrid['0']);
		foreach ($arrterima_produk as $index => $value) {
			$datacontent['arrterima_produk'][$value['id_barang']] = $value;
			$datacontent['arrdetail_terima_produk'][$value['id_barang']][$value['id_terima_kemasan']] = $value;
		}
		$data['file'] 				= $this->ind_;
		$data['content'] 			= $this->load->view($this->url_ . '/form_bill', $datacontent, TRUE);
		$data['title'] 				= $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan_tagihan()
	{
		$this->db->trans_begin();
		$total_harga = 0;
		foreach ($_POST['id_terima_kemasan'] as $index => $id) {
			$terima_produk = $this->db->get_where('terima_produk', ['nomor_surat_jalan' => $_POST['sj'], 'id_terima_kemasan' => $id])->row_array();
			$terima_produk = ([
				'hpp' => str_replace(",", "", $_POST['harga_satuan'][$index]), //'hpp' => $_POST['harga_satuan'][$index] |code sebelumnya|
				'total_tagihan' => str_replace(",", "", $_POST['harga_total'][$index]), //'total_tagihan' => $_POST['harga_total'][$index] |code sebelumnya|
				'invoice' => $_POST['invoice'],
				'connected_status' => 1,
				'invoice_date' => $_POST['tanggal'],
				'pending_quantity' => $terima_produk['jumlah']
			]);
			$this->db->update('terima_produk', $terima_produk, ['nomor_surat_jalan' => $_POST['sj'], 'id_terima_kemasan' => $id]);
			$total_harga += str_replace(",", "", $_POST['harga_total'][$index]); //$_POST['harga_total'][$index] |code sebelumnya|
			$this->Model->check_fifo_alpha($_POST['id_barang'][$index]);
			$this->Model->check_fifo_beta($_POST['id_barang'][$index]);
			$this->Model->check_fifo_main($_POST['id_barang'][$index]);
			$this->Model->set_sales_detail();
		}
		$dataHeader = ([
			'coa_transaction_date' => date('Y-m-d'),
			'date_create' => date('Y-m-d'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_realization' => 1,
			'coa_transaction_realization_date' => date('Y-m-d'),
			// 'coa_transaction_debit' => $total_harga,
			'coa_transaction_credit' => $total_harga,
		]);
		$this->db->insert('t_coa_transaction_header', $dataHeader);
		$idHeader = $this->db->insert_id();
		$factory 	= $this->Model->get_factory($_POST['id_factory']);
		$utangCoa = $this->db->get_where('coa_4', ['coa3_id' => 23, 'nama' => 'Utang Supplier ' . $factory['nama_factory'] . ''])->row_array();
		$piutangCoa = $this->db->get_where('coa_4', ['coa3_id' => 12, 'nama' => 'Piutang Supplier ' . $factory['nama_factory'] . ''])->row_array();
		$m_acc = $this->db->get_where('m_account', ['factory_id' => $_POST['id_factory']])->row_array();
		if ($utangCoa == null) {
			$countCoa = $this->db->get_where('coa_4', ['coa3_id' => 23])->result_array();
			$utangCoa = ([
				'kode' => '200.1.1.' . (count($countCoa) + 1),
				'nama' => 'Utang Supplier ' . $factory['nama_factory'] . '',
				'coa3_id' => 23,
			]);
			$this->db->insert('coa_4', $utangCoa);
			$utangCoa['id'] = $this->db->insert_id();

			$totalCoa = ([
				'coa_id' => $utangCoa['id'],
				'coa_code' => $utangCoa['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d'),
				'coa_name' => $utangCoa['nama'],
			]);
			$this->db->insert('t_coa_total', $totalCoa);
			$totalCoa['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $totalCoa);

			$upd_acc = ([
				'account_code2' => (count($countCoa) + 1),
				'coa_id' => $utangCoa['id'],
				'coa_level' => 4,
			]);
			$this->db->update('m_account', $upd_acc, ['account_id' => $m_acc['account_id']]);
		}
		if ($piutangCoa == null) {
			$countCoa = $this->db->get_where('coa_4', ['coa3_id' => 12])->result_array();
			$piutangCoa = ([
				'kode' => '100.1.3.' . (count($countCoa) + 1),
				'nama' => 'Piutang Supplier ' . $factory['nama_factory'],
				'coa3_id' => 12
			]);
			$this->db->insert('coa_4', $piutangCoa);
			$piutangCoa['id'] = $this->db->insert_id();
			$akunTotal = ([
				'coa_id' => $piutangCoa['id'],
				'coa_code' => $piutangCoa['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $piutangCoa['nama'],
			]);
			$this->db->insert('t_coa_total', $akunTotal);
			$akunTotal['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $akunTotal);

			$countDeposit = $this->db->get_where('coa_4', ['coa3_id' => 16])->result_array();
			$akunDeposit = ([
				'kode' => '100.1.7.' . (count($countDeposit) + 1),
				'nama' => 'Deposit Supplier ' . $factory['nama_factory'],
				'coa3_id' => 16
			]);
			$this->db->insert('coa_4', $akunDeposit);
			$akunDeposit['id'] = $this->db->insert_id();
			$akunTotal = ([
				'coa_id' => $akunDeposit['id'],
				'coa_code' => $akunDeposit['kode'],
				'coa_level' => 4,
				'date_create' => date('Y-m-d H:i:s'),
				'coa_name' => $akunDeposit['nama'],
			]);
			$this->db->insert('t_coa_total', $akunTotal);
			$akunTotal['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $akunTotal);
		}

		$real_id = $this->db->query("SELECT (IFNULL(MAX(account_detail_id), 0) + 1) as id FROM t_account_detail")->row_array();
		$t_account_detail = ([
			'account_id' => $m_acc['account_id'],
			'account_detail_id' => $real_id['id'],
			'account_detail_credit' => $total_harga,
			'account_detail_note' => 'Tagihan ' . $utangCoa['nama'] . ' SJ ' . $_POST['sj'],
			'account_detail_price' => $total_harga,
			'account_detail_category_id' => 72,
			'account_detail_date' => date('Y-m-d'),
			'account_detail_user_id' => $_SESSION['user_id'],
			'account_detail_date_create' => date('Y-m-d H:i:s'),
			'account_detail_type_id' => 4,
			'account_detail_pic' => $_SESSION['user_fullname'],
			'account_detail_realization' => 0,
			'account_detail_pic_id' => $_SESSION['user_id'],
			'account_detail_transfer_name' => $_POST['invoice'],
		]);
		$this->db->insert('t_account_detail', $t_account_detail);
		$idDetail = $this->db->insert_id();

		$totalPiutang = $this->db->get_where('t_coa_total', ['coa_id' => $piutangCoa['id']])->row_array();
		$jumlahPiutang = $totalPiutang['coa_total_credit'] - $totalPiutang['coa_total_debit'];
		if ($jumlahPiutang > 0) {
			$dataPiutang = ([
				'coa_name' => $piutangCoa['nama'],
				'coa_code' => $piutangCoa['kode'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_transaction_note' => 'Tagihan ' . $utang['nama'] . ' SJ ' . $_POST['sj'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 9,
				'coa_transaction_source_id' => $idDetail,
				'coa_id' => $piutangCoa['id'],
				'coa_transaction_realization' => 0,
				'coa_group_id' => $idHeader,
			]);
			if ($jumlahPiutang > $total_harga) {
				$dataPiutang['coa_credit'] = $total_harga;
				$total_harga = 0;
			} else {
				$dataPiutang['coa_credit'] = $jumlahPiutang;
				$total_harga -= $jumlahPiutang;
			}
			$this->db->insert('t_coa_transaction', $dataPiutang);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $dataPiutang[coa_credit] WHERE coa_id = $dataPiutang[coa_id]");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $dataPiutang[coa_credit] WHERE coa_id = $dataPiutang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataPiutang[coa_date], '-', '')");
		}

		foreach ($_POST['id_terima_kemasan'] as $index => $id) {
			if ($_POST['harga_total'][$index] > 0) {
				$produk = $this->db->get_where('produk', ['id_produk' => $_POST['id_barang'][$index]])->row_array();
				$persediaanCOA = $this->db->get_where('coa_4', ['nama' => 'Persediaan ' . $produk['nama_produk'], 'coa3_id' => 17])->row_array();
				if ($persediaanCOA == null) {
					$countCoa = $this->db->get_where('coa_4', ['coa3_id' => 17])->result_array();
					$persediaanCOA = ([
						'kode' => '100.1.8.' . (count($countCoa) + 1),
						'nama' => 'Persediaan ' . $produk['nama_produk'],
						'coa3_id' => 17
					]);
					$this->db->insert('coa_4', $persediaanCOA);
					$persediaanCOA['id'] = $this->db->insert_id();

					$totalCoa = ([
						'coa_id' => $persediaanCOA['id'],
						'coa_code' => $persediaanCOA['kode'],
						'coa_level' => 4,
						'date_create' => date('Y-m-d H:i:s'),
						'coa_name' => $persediaanCOA['nama']
					]);
					$this->db->insert('t_coa_total', $totalCoa);
					$totalCoa['coa_total_date'] = date('Y-m') . '-01';
					$this->db->insert('t_coa_total_history', $totalCoa);
				}

				$dataPersediaan = ([
					'coa_name' => $persediaanCOA['nama'],
					'coa_code' => $persediaanCOA['kode'],
					'coa_date' => date('Y-m-d'),
					'coa_level' => 4,
					'coa_debit' => str_replace(",", "", $_POST['harga_total'][$index]), //$_POST['harga_total'][$index] |code sebelumnya|
					'coa_transaction_note' => 'Tagihan ' . $utangCoa['nama'] . ' SJ ' . $_POST['sj'],
					'date_create' => date('Y-m-d H:i:s'),
					'user_create' => $_SESSION['user_id'],
					'coa_transaction_source' => 9,
					'coa_transaction_source_id' => $idDetail,
					'coa_id' => $persediaanCOA['id'],
					'coa_transaction_realization' => 0,
					'coa_group_id' => $idHeader,
				]);

				$this->db->insert('t_coa_transaction', $dataPersediaan);
				$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $dataPersediaan[coa_debit] WHERE coa_id = $dataPersediaan[coa_id] AND coa_level = '$dataPersediaan[coa_level]'");
				$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $dataPersediaan[coa_debit] WHERE coa_id = $dataPersediaan[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataPersediaan[coa_date], '-', '') AND coa_level = '$dataPersediaan[coa_level]'");
			}
		}

		if ($total_harga > 0) {
			$dataUtang = ([
				'coa_name' => $utangCoa['nama'],
				'coa_code' => $utangCoa['kode'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_credit' => $total_harga,
				'coa_transaction_note' => 'Tagihan ' . $utangCoa['nama'] . ' SJ ' . $_POST['sj'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 9,
				'coa_transaction_source_id' => $idDetail,
				'coa_id' => $utangCoa['id'],
				'coa_transaction_realization' => 0,
				'coa_group_id' => $idHeader,
			]);

			$this->db->insert('t_coa_transaction', $dataUtang);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $dataUtang[coa_credit] WHERE coa_id = $dataUtang[coa_id] AND coa_level = '$dataUtang[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $dataUtang[coa_credit] WHERE coa_id = $dataUtang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataUtang[coa_date], '-', '') AND coa_level = '$dataUtang[coa_level]'");
		}




		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Terima_produk_accounting');
	}

	public function table_tagihan($id)
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = 'Tabel Tagihan';
		// $datacontent['parameter'] = $parameter;
		$datacontent['id'] = $id;
		$data['file'] = $this->ind_;
		$datacontent['factory'] = $this->db->get_where('factory', ['id_factory' => $id])->row_array();
		$datacontent['m_account'] 	= $this->Model->get_list_account_seller();
		$datacontent['arrlist_account_sales'] 	= $this->Model->get_list_account_sales($id);
		$data['content'] = $this->load->view($this->url_ . '/table_tagihan', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function set_pembayaran()
	{
		$this->db->trans_begin();
		$detail_id = $this->db->query("SELECT (IFNULL(MAX(account_detail_id), 0) + 1) as id FROM t_account_detail")->row_array();
		$factory = $this->db->get_where('factory', ['id_factory' => $_POST['id']])->row_array();
		$dataTransaction = ([
			'account_id' => 49,
			'account_detail_id' => $detail_id['id'],
			'account_detail_credit' => str_replace(",", "", $_POST['nominal']),
			'account_detail_note' => 'Pembayaran Tagihan ' . $factory['nama_factory'],
			'account_detail_deposit' => str_replace(",", "", $_POST['deposit']),
			'account_detail_deposit_start' => str_replace(",", "", $_POST['deposit']),
			'account_detail_transfer_name' => $_SESSION['user_fullname'],
			'account_detail_category_id' => 73,
			'account_detail_date' => date('Y-m-d'),
			'account_detail_user_id' => $_SESSION['user_id'],
			'account_detail_realization' => 1,
			'account_detail_type_id' => 5
		]);
		$this->db->insert('t_account_detail', $dataTransaction);
		$idDetailTrans = $this->db->insert_id();

		$coaBank = $this->db->get_where('coa_4', ['id' => 11])->row_array();
		$coaUtang = $this->db->get_where('coa_4', ['nama' => 'Utang Supplier ' . $factory['nama_factory']])->row_array();

		$dataHeader = ([
			'coa_transaction_date' => date('Y-m-d'),
			'date_create' => date('Y-m-d H:i:s'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_debit' => str_replace(",", "", $_POST['nominal']),
			'coa_transaction_credit' => str_replace(",", "", $_POST['nominal']),
			'coa_transaction_payment' => str_replace(",", "", $_POST['nominal']),
			'coa_transaction_realization' => 1,
			'coa_transaction_realization_date' => date('Y-m-d'),
		]);
		$this->db->insert('t_coa_transaction_header', $dataHeader);
		$idHeader = $this->db->insert_id();

		foreach ($_POST['from'] as $index => $from) {
			$dataUtang = ([
				'coa_name' => $coaUtang['nama'],
				'coa_code' => $coaUtang['kode'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_debit' => str_replace(',', '', $from['account_detail_credit']),
				'coa_transaction_note' => 'Pembayaran Tagihan ' . $factory['nama_factory'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 10,
				'coa_transaction_source_id' => $idDetailTrans,
				'coa_id' => $coaUtang['id'],
				'coa_transaction_realization' => 1,
				'coa_transaction_source_type' => 9,
				'coa_group_id' => $idHeader,
			]);
			$this->db->insert('t_coa_transaction', $dataUtang);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $dataUtang[coa_debit] WHERE coa_id = $dataUtang[coa_id] AND coa_level = '$dataUtang[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $dataUtang[coa_debit] WHERE coa_id = $dataUtang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataUtang[coa_date], '-', '') AND coa_level = '$dataUtang[coa_level]'");

			$dataBank = ([
				'coa_name' => $coaBank['nama'],
				'coa_code' => $coaBank['kode'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_credit' => str_replace(",", "", $from['account_detail_credit']),
				'coa_transaction_note' => 'Pembayaran Tagihan ' . $factory['nama_factory'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 10,
				'coa_transaction_source_id' => $idDetailTrans,
				'coa_id' => $coaBank['id'],
				'coa_transaction_realization' => 1,
				'coa_transaction_source_type' => 9,
				'coa_group_id' => $idHeader,
			]);
			$this->db->insert('t_coa_transaction', $dataBank);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $dataBank[coa_credit] WHERE coa_id = $dataBank[coa_id] AND coa_level = '$dataBank[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $dataBank[coa_credit] WHERE coa_id = $dataBank[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataBank[coa_date], '-', '') AND coa_level = '$dataBank[coa_level]'");
		}

		foreach ($_POST['paid'] as $index => $paid) {
			if ($paid > 0) {
				$dataPayment = ([
					'account_detail_real_id' => $idDetailTrans,
					'coa_header_id' => $idHeader,
					'account_detail_target_real_id' => $index,
					'account_detail_payment_amount' => str_replace(",", "", $paid),
					'account_detail_payment_date' => date('Y-m-d'),
					'account_detail_id' => $detail_id['id']
				]);
				$this->db->insert('t_account_detail_payment', $dataPayment);

				if (str_replace(",", "", $paid) == $_POST['sisa'][$index]) {
					$this->db->update('t_coa_transaction', ['coa_transaction_realization' => 1], ['coa_transaction_source' => $index]);
					$this->db->update('t_account_detail', ['account_detail_realization' => 1], ['account_detail_real_id' => $index]);
				}
				$this->db->query("UPDATE t_account_detail SET account_detail_paid = account_detail_paid + REPLACE('$paid',',','') WHERE account_detail_real_id = $index");
			}
		}

		$piutangCoa = $this->db->get_where('coa_4', ['nama' => 	'Piutang Supplier ' . $factory['nama_factory']])->row_array();
		$totalCoaUtang = $this->db->get_where('t_coa_total', ['coa_id' => $utangCoa['id']])->row_array();
		$totalUtang = $totalCoaUtang['coa_total_credit'] - $totalCoaUtang['coa_total_debit'];
		if (str_replace(",", "", $_POST['nominal']) > $totalUtang) {
			$dataPiutang = ([
				'coa_name' => $piutangCoa['nama'],
				'coa_code' => $piutangCoa['kode'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_debit' => (str_replace(",", "", $_POST['nominal']) - $totalUtang),
				'coa_transaction_note' => $dataBank['coa_transaction_note'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 10,
				'coa_transaction_source_id' => $idDetailTrans,
				'coa_id' => $piutangCoa['id'],
				'coa_transaction_realization' => 1,
				'coa_transaction_source_type' => 9,
				'coa_group_id' => $idHeader,
			]);
			$this->db->insert('t_coa_transaction', $dataPiutang);
			$this->db->query("UPDATE t_coa_total SET coa_total_debit = coa_total_debit + $dataPiutang[coa_debit] WHERE coa_id = $dataPiutang[coa_id] AND coa_level = '$dataPiutang[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_debit = coa_total_debit + $dataPiutang[coa_debit] WHERE coa_id = $dataPiutang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataPiutang[coa_date], '-', '') AND coa_level = '$dataPiutang[coa_level]'");
		} else if (str_replace(",", "", $_POST['nominal']) < $totalUtang) {
			$dataPiutang = ([
				'coa_name' => $piutangCoa['nama'],
				'coa_code' => $piutangCoa['kode'],
				'coa_date' => date('Y-m-d'),
				'coa_level' => 4,
				'coa_credit' => ($totalUtang - str_replace(",", "", $_POST['nominal'])),
				'coa_transaction_note' => $dataBank['coa_transaction_note'],
				'date_create' => date('Y-m-d H:i:s'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 10,
				'coa_transaction_source_id' => $idDetailTrans,
				'coa_id' => $piutangCoa['id'],
				'coa_transaction_realization' => 1,
				'coa_transaction_source_type' => 9,
				'coa_group_id' => $idHeader,
			]);
			$this->db->insert('t_coa_transaction', $dataPiutang);
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $dataPiutang[coa_credit] WHERE coa_id = $dataPiutang[coa_id] AND coa_level = '$dataPiutang[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $dataPiutang[coa_credit] WHERE coa_id = $dataPiutang[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataPiutang[coa_date], '-', '') AND coa_level = '$dataPiutang[coa_level]'");
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Terima_produk_accounting');
	}
}
