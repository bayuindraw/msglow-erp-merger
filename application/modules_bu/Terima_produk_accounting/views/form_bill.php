<div class="row">
	<div class="col-lg-12 col-xl-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?= $title; ?>
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<form id="main" class="form-horizontal" action="<?= site_url($url . '/simpan_tagihan'); ?>" method="Post">
					<div class="form-group row">
						<label class="col-2 col-form-label">Invoice</label>
						<div class="col-4">
							<input type="text" name="invoice" class="form-control static" required />
						</div>
						<label class="col-2 col-form-label">Tanggal</label>
						<div class="col-4">
							<input type="text" name="tanggal" value="<?= date('Y-m-d') ?>" readonly class="form-control static" />
						</div>
					</div>
					<?= input_hidden('id_factory', $id_factory) ?>
					<?= input_hidden('sj', $sj) ?>
					<table class="table table-striped table-bordered nowrap" style="font-size: 12px">
						<thead>
							<tr>
								<th>Produk</th>
								<th>Kode PO</th>
								<th>Jumlah Sesuai SJ</th>
								<th>Jumlah Barang Datang</th>
								<th>Harga Satuan</th>
								<th>Harga Total</th>
							</tr>
						</thead>
						<tbody id="x">
							<?php foreach ($arrterima_produk as $index => $value) { ?>
								<tr>
									<td><?= $value['nama_produk'] ?></td>
									<td>

										<?php foreach ($arrdetail_terima_produk[$index] as $index2 => $value2) { ?>
											<div style="height:39px;"><?= $value2['kode_po'] ?></div>
											<hr>
										<?php } ?>
									</td>
									<td>

										<?php foreach ($arrdetail_terima_produk[$index] as $index2 => $value2) { ?>
											<div style="height:39px;">
												<?= number_format($value2['jumlah_sj'], 0, ',-', ','); ?>
											</div>
											<hr>
										<?php } ?>
									</td>
									<td>

										<?php
										$jum = 0;
										foreach ($arrdetail_terima_produk[$index] as $index2 => $value2) {
											$jum += $value2['jumlah'];
										?>
											<input type="hidden" name="id_barang[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['id_barang'] ?>">
											<input type="text" id="xJumlah<?= $index2 ?>" value="<?= number_format($value2['jumlah'], 0, ',-', ','); ?>" readonly class="form-control static sum_jumlah<?= $index2 ?>">
											<input type="text" id="cJumlah<?= $index2 ?>" name="jml[<?= $index2 ?>]" value="<?= $value2['jumlah'] ?>" readonly class="form-control static sum_jumlah<?= $index2 ?>" hidden>
											<hr>
											<input type="hidden" name="jml_asli[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['jumlah']; ?>">
											<input type="hidden" name="tgl_asli[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['tgl_terima'] ?>">
											<input type="hidden" name="id_terima_kemasan[<?= $value2['id_terima_kemasan'] ?>]" value="<?= $value2['id_terima_kemasan'] ?>">
										<?php } ?>
									</td>
									<td>
										<?php
										$jum = 0;
										foreach ($arrdetail_terima_produk[$index] as $index2 => $value2) {
											$jum += $value2['jumlah'];
										?>
											<input type="text" id="cSatuan<?= $index2 ?>" name="harga_satuan[<?= $index2 ?>]" value="0" onkeyup="get_total(<?= $index2 ?>);" class="form-control static money sum_satuan<?= $index2 ?>" required>
											<hr>

										<?php } ?>
									</td>
									<td>
										<?php
										$jum = 0;
										foreach ($arrdetail_terima_produk[$index] as $index2 => $value2) {
											$jum += $value2['jumlah'];
										?>
											<input type="text" id="cTotal<?= $index2 ?>" name="harga_total[<?= $index2 ?>]" value="0" onkeyup="get_satuan(<?= $index2 ?>)" class="form-control static money sum_total<?= $index2 ?>" required>
											<hr>
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>

					<div class="kt-portlet__foot" style="margin-top: 20px;">
						<div class="kt-form__actions">
							<button type="submit" name="simpan" value="simpan" class="btn btn-primary waves-effect waves-light " data-toggle="tooltip" data-placement="top" title="Simpan Penerima Produk" style="margin-left: -22px;" onsubmit="return confirm('Are you sure?')">
								<i class="icofont icofont-ui-edit"></i><span class="m-l-10">Simpan Data</span>
							</button>
						</div>
					</div>
				</form>


			</div>
		</div>
	</div>
</div>


<script>
	// function get_total($id) {
	// 	$('#cTotal' + $id).val(($('#cSatuan' + $id).val() * $('#cJumlah' + $id).val()));
	// }

	// function get_satuan($id) {
	// 	$('#cSatuan' + $id).val(Math.ceil(($('#cTotal' + $id).val() / $('#cJumlah' + $id).val())));
	// }

	function get_total($id) {
		$(".money").mask("#,##0", {
			reverse: true
		});
		$('#cTotal' + $id).val(($('#cSatuan' + $id).val().replaceAll(",", "") * $('#cJumlah' + $id).val().replaceAll(",", "")));

	}

	function get_satuan($id) {
		$(".money").mask("#,##0", {
			reverse: true
		});
		$('#cSatuan' + $id).val(Math.ceil(($('#cTotal' + $id).val().replaceAll(",", "") / $('#cJumlah' + $id).val().replaceAll(",", ""))));

	}
</script>