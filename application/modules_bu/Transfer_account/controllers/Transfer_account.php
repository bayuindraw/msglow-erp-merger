<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Transfer_account extends CI_Controller
{

	var $url_ = "Transfer_account";
	// var $url2_ = "Transfer_account/table_detail";
	var $id_ = "account_id";
	var $id2_ = "account_detail_id";
	var $eng_ = "account";
	var $eng2_ = "account_detail";
	var $ind_ = "Transfer Akun";
	var $ind2_ = "Akun Detail";

	public function __construct()
	{
		parent::__construct();
		// $this->load->model(array(
		// 	$this->url_ . 'Model'  =>  'Model',
		// ));
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		$datacontent['url'] = $this->url_;
		$datacontent['title'] = $this->ind_;
		$datacontent['arrakun'] = "<option></option>";
		foreach ($this->db->get('coa_1')->result_array() as $coa) {
			$datacontent['arrakun'] .= '<option level="1" value="' . $coa['id'] . '">' . str_replace("'", "", $coa['nama']) . '</option>';
		}
		foreach ($this->db->get('coa_2')->result_array() as $coa) {
			$datacontent['arrakun'] .= '<option level="2" value="' . $coa['id'] . '">' . str_replace("'", "", $coa['nama']) . '</option>';
		}
		foreach ($this->db->get('coa_3')->result_array() as $coa) {
			$datacontent['arrakun'] .= '<option level="3" value="' . $coa['id'] . '">' . str_replace("'", "", $coa['nama']) . '</option>';
		}
		foreach ($this->db->get('coa_4')->result_array() as $coa) {
			$datacontent['arrakun'] .= '<option level="4" value="' . $coa['id'] . '">' . str_replace("'", "", $coa['nama']) . '</option>';
		}
		$data['file'] = $this->ind_;
		$data['content'] = $this->load->view($this->url_ . '/form', $datacontent, TRUE);
		$data['title'] = $datacontent['title'];
		$this->load->view('Layout/home', $data);
	}

	public function simpan()
	{
		$this->db->trans_begin();
		$dataHeader = ([
			'coa_transaction_date' => $_POST['input']['tanggal'],
			'date_create' => date('Y-m-d'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_payment' => $_POST['input']['total'],
			'coa_transaction_realization' => 1,
			'coa_transaction_realization_date' => date('Y-m-d'),
			'coa_transaction_type' => 1,
		]);
		$this->db->insert('t_coa_transaction_header', $dataHeader);
		$idHeader = $this->db->insert_id();

		$sumber = $this->db->get_where('coa_' . $_POST['input']['coa_level_1'] . '', ['id' => $_POST['input']['akun']])->row_array();
		$dataSumber = ([
			'coa_name' => $sumber['nama'],
			'coa_code' => $sumber['kode'],
			'coa_date' => $_POST['input']['tanggal'],
			'coa_level' => $_POST['input']['coa_level_1'],
			'coa_transaction_note' => 'Transfer antar Akun sumber ' . $sumber['nama'],
			'date_create' => date('Y-m-d'),
			'user_create' => $_SESSION['user_id'],
			'coa_transaction_source' => 6,
			'coa_transaction_source_id' => $idHeader,
			'coa_id' => $sumber['id'],
			'coa_transaction_realization' => 1,
			'coa_group_id' => $idHeader,
		]);
		if (substr($sumber['kode'], 0, 1) == 1 || substr($sumber['kode'], 0, 1) == 5 || substr($sumber['kode'], 0, 1) == 6 || substr($sumber['kode'], 0, 1) == 7) {
			$dataSumber['coa_credit'] = $_POST['input']['total'];
			$dataSumber['coa_debit'] = 0;
		} else {
			$dataSumber['coa_debit'] = $_POST['input']['total'];
			$dataSumber['coa_credit'] = 0;
		}
		$this->db->insert('t_coa_transaction', $dataSumber);
		$this->db->query("UPDATE t_coa_transaction_header SET coa_transaction_credit = $dataSumber[coa_credit] + coa_transaction_credit, coa_transaction_debit = $dataSumber[coa_debit] + coa_transaction_debit WHERE coa_transaction_header_id = $idHeader");
		if ($this->db->get_where('t_coa_total', ['coa_id' => $dataSumber['coa_id']])->row_array() == null) {
			$dataCoaTot = ([
				'coa_id' => $dataSumber['coa_id'],
				'coa_code' => $dataSumber['coa_code'],
				'coa_level' => $dataSumber['coa_level'],
				'date_create' => $dataSumber['date_create'],
				'coa_name' => $dataSumber['coa_name'],
			]);
			$this->db->insert('t_coa_total', $dataCoaTot);
			$dataCoaTot['coa_total_date'] = date('Y-m') . '-01';
			$this->db->insert('t_coa_total_history', $dataCoaTot);
		}
		$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $dataSumber[coa_credit], coa_total_debit = coa_total_debit + $dataSumber[coa_debit] WHERE coa_id = $dataSumber[coa_id] AND coa_level = '$dataSumber[coa_level]'");
		$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $dataSumber[coa_credit], coa_total_debit = coa_total_debit + $dataSumber[coa_debit] WHERE coa_id = $dataSumber[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataSumber[coa_date], '-', '') AND coa_level = '$dataSumber[coa_level]'");

		foreach ($_POST['input2'] as $input) {
			$akun = $this->db->get_where('coa_' . $input['coa_level_2'] . '', ['id' => $input['akun_id']])->row_array();
			$dataAkun = ([
				'coa_name' => $akun['nama'],
				'coa_code' => $akun['kode'],
				'coa_date' => $_POST['input']['tanggal'],
				'coa_level' => $input['coa_level_2'],
				'coa_transaction_note' => 'Transfer antar Akun sumber ' . $sumber['nama'],
				'date_create' => date('Y-m-d'),
				'user_create' => $_SESSION['user_id'],
				'coa_transaction_source' => 6,
				'coa_transaction_source_id' => $idHeader,
				'coa_id' => $akun['id'],
				'coa_transaction_realization' => 1,
				'coa_group_id' => $idHeader,
			]);
			if (substr($akun['kode'], 0, 1) == 1 || substr($akun['kode'], 0, 1) == 5 || substr($akun['kode'], 0, 1) == 6 || substr($akun['kode'], 0, 1) == 7) {
				$dataAkun['coa_debit'] = $input['nominal'];
				$dataAkun['coa_credit'] = 0;
			} else {
				$dataAkun['coa_credit'] = $input['nominal'];
				$dataAkun['coa_debit'] = 0;
			}
			$this->db->insert('t_coa_transaction', $dataAkun);
			$this->db->query("UPDATE t_coa_transaction_header SET coa_transaction_credit = $dataAkun[coa_credit] + coa_transaction_credit, coa_transaction_debit = $dataAkun[coa_debit] + coa_transaction_debit WHERE coa_transaction_header_id = $idHeader");
			if ($this->db->get_where('t_coa_total', ['coa_id' => $dataAkun['coa_id']])->row_array() == null) {
				$dataCoaTot = ([
					'coa_id' => $dataAkun['coa_id'],
					'coa_code' => $dataAkun['coa_code'],
					'coa_level' => $dataAkun['coa_level'],
					'date_create' => $dataAkun['date_create'],
					'coa_name' => $dataAkun['coa_name'],
				]);
				$this->db->insert('t_coa_total', $dataCoaTot);
				$dataCoaTot['coa_total_date'] = date('Y-m') . '-01';
				$this->db->insert('t_coa_total_history', $dataCoaTot);
			}
			$this->db->query("UPDATE t_coa_total SET coa_total_credit = coa_total_credit + $dataAkun[coa_credit], coa_total_debit = coa_total_debit + $dataAkun[coa_debit] WHERE coa_id = $dataAkun[coa_id] AND coa_level = '$dataAkun[coa_level]'");
			$this->db->query("UPDATE t_coa_total_history SET coa_total_credit = coa_total_credit + $dataAkun[coa_credit], coa_total_debit = coa_total_debit + $dataAkun[coa_debit] WHERE coa_id = $dataAkun[coa_id] AND REPLACE(coa_total_date, '-', '') < REPLACE($dataAkun[coa_date], '-', '') AND coa_level = '$dataAkun[coa_level]'");
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		redirect('Transfer_account');
	}
}
