<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chart extends CI_Controller {

	public function __construct(){
		
		parent::__construct();

		$this->load->model('model');
		$this->load->model('relasi');
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('download'); 

	}
	public function Date2String($dTgl){
			//return 2012-11-22
		list($cDate,$cMount,$cYear)	= explode("-",$dTgl) ;
		if(strlen($cDate) == 2){
			$dTgl	= $cYear . "-" . $cMount . "-" . $cDate ;
		}
		return $dTgl ; 
	}

	public function String2Date($dTgl){
			//return 22-11-2012  
		list($cYear,$cMount,$cDate)	= explode("-",$dTgl) ;
		if(strlen($cYear) == 4){
			$dTgl	= $cDate . "-" . $cMount . "-" . $cYear ;
		} 
		return $dTgl ; 	
	}

	public function StringExplode($cString){

		$cString = explode(",", $cString);
		return $cString ; 	
	}

	public function TimeStamp() {
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("H:i:s");
		return $Data ;
	}

	public function DateStamp() {
		date_default_timezone_set("Asia/Jakarta");
		$Data =  date('Y-m-d', strtotime('-1 days'));
		return $Data ;
	}

	public function DateStampNow() {
		date_default_timezone_set("Asia/Jakarta");
		$Data =  date('Y-m-d');
		return $Data ;
	}  

	public function DateTimeStamp() {
		date_default_timezone_set("Asia/Jakarta");
		$Data = date("Y-m-d h:i:s");
		return $Data ;
	} 

	public function index($Aksi=""){
		date_default_timezone_set("Asia/Jakarta");
		$bulan 						= date("m");
		$tahun 						= date("Y");
		$dataHeader['menu']   		= '#';
		$dataHeader['file']   		= 'Index' ;
		$dataHeader['link']			= 'index';
		$data['kategori'] 			= $this->model->ViewKategori() ;
		$data['brand'] 				= $this->model->ViewBrand() ;

		$data['PoUnpaidMonthly']				= $this->relasi->PoUnpaidMonthly($bulan,$tahun);
		$data['PoUnpaidYearly']				= $this->relasi->PoUnpaidYearly($tahun);

		$data['PoPaidMonthly']				= $this->relasi->PoPaidMonthly($bulan,$tahun);
		$data['PoPaidYearly']				= $this->relasi->PoPaidYearly($tahun);

		$data['SoByQtyMonthly']				= $this->relasi->SoByQtyMonthly($bulan,$tahun);
		$data['SoByQtyYearly']				= $this->relasi->SoByQtyYearly($tahun);

		$data['SoByKategoriMonthlyBar']				= $this->relasi->SoByKategoriMonthly($bulan,$tahun);
		$data['SoByBrandMonthlyBar']				= $this->relasi->SoByBrandMonthly($bulan,$tahun);
		

		$data['SoByKategoriMonthlyLine']				= $this->relasi->SoByKategoriMonthly($bulan,$tahun);
		$data['SoByBrandMonthlyLine']				= $this->relasi->SoByBrandYearly($bulan,$tahun);

		$data['member']			= $this->relasi->SoByMemberMonthly($bulan,$tahun);
		$data['reseller']		= $this->relasi->SoByResellerMonthly($bulan,$tahun);
		$data['agen']			= $this->relasi->SoByAgenMonthly($bulan,$tahun);
		$data['distributor']	= $this->relasi->SoByDistributorMonthly($bulan,$tahun);

		$data['SoBySellerLine']			= $this->relasi->SoBySellerLine($bulan,$tahun,'MEMBER');	

		$data['FastMoving']	= $this->relasi->FastMoving($bulan,$tahun);	
		$data['SlowMoving']	= $this->relasi->SlowMoving($bulan,$tahun);	

		$data['TopSeller2']	= $this->relasi->TopSeller2($bulan,$tahun);


		$this->load->view('home',$data);
	}

	function GetPoUnpaidMonthly($bulan,$tahun,$kategori){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		if($kategori == 'bulan'){
			$data['row']	= $this->relasi->PoUnpaidMonthly($bulan,$tahun);
			$this->load->view('chart/GetPoUnpaid',$data);
		}else{
			$data['row']	= $this->relasi->PoUnpaidYearly($tahun);
			$this->load->view('chart/GetPoUnpaidYearly',$data);
		}
		
	}

	function GetPoUnpaidYearly($tahun){
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->PoUnpaidYearly($tahun);
		$this->load->view('chart/GetPoUnpaid',$data);
	}

	function GetPoPaidMonthly($bulan,$tahun,$kategori){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		if($kategori == 'bulan'){
			$data['row']	= $this->relasi->PoPaidMonthly($bulan,$tahun);
			$this->load->view('chart/GetPoPaid',$data);
		}else{
			$data['row']	= $this->relasi->PoPaidYearly($tahun);
			$this->load->view('chart/GetPoPaidYearly',$data);
		}
		
	}

	function GetPoPaidYearly($tahun){
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->PoPaidYearly($tahun);
		$this->load->view('chart/GetPoPaid',$data);
	}

	function GetSoByQtyMonthly($bulan,$tahun,$kategori){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		if($kategori == 'bulan'){
			$data['row']	= $this->relasi->SoByQtyMonthly($bulan,$tahun);
			$this->load->view('chart/GetSoByQty',$data);
		}else{
			$data['row']	= $this->relasi->SoByQtyYearly($tahun);
			$this->load->view('chart/GetSoByQtyYearly',$data);
		}
		
		
	}

	function GetSoByQtyYearly($tahun){;
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->SoByQtyYearly($tahun);
		$this->load->view('chart/GetSoByQty',$data);
	}

	function GetSoByCategoryMonthlyBar($bulan,$tahun){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->SoByKategoriMonthly($bulan,$tahun);
		$this->load->view('chart/GetSoByCategoryBar',$data);
	}

	function GetSoByCategoryMonthlyLine($bulan,$tahun){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->SoByKategoriMonthly($bulan,$tahun);
		$this->load->view('chart/GetSoByCategoryLine',$data);
	}

	function GetSoByCategoryYearly($tahun){
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->SoByKategoriYearly($tahun);
		$this->load->view('chart/GetSoByCategory',$data);
	}

	function GetSoByBrandMonthlyBar($bulan,$tahun){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->SoByBrandMonthly($bulan,$tahun);
		$this->load->view('chart/GetSoByBrandBar',$data);
	}

	function GetSoByBrandMonthlyLine($bulan,$tahun){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->SoByBrandMonthly($bulan,$tahun);
		$this->load->view('chart/GetSoByBrandBrandLine',$data);
	}

	function GetSoByBrandYearly($tahun){
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->SoByBrandYearly($tahun);
		$this->load->view('chart/GetSoByBrand',$data);
	}
	

	function GetSoSellerBar($bulan,$tahun){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;

		
			$data['member']	= $this->relasi->SoByMemberMonthly($bulan,$tahun);
		
			$data['reseller']	= $this->relasi->SoByResellerMonthly($bulan,$tahun);
		
			$data['agen']	= $this->relasi->SoByAgenMonthly($bulan,$tahun);
	
			$data['distributor']	= $this->relasi->SoByDistributorMonthly($bulan,$tahun);
	
		$this->load->view('chart/GetSoSellerBar',$data);
	}

	function GetSoSellerLine($bulan,$tahun,$seller){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->SoBySellerLine($bulan,$tahun,$seller);	
		$this->load->view('chart/GetSoSellerLine',$data);
	}

	function GetFastMoving($bulan,$tahun){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->FastMoving($bulan,$tahun);	
		$this->load->view('chart/GetFastMoving',$data);
	}

	function GetSlowMoving($bulan,$tahun){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		$data['row']	= $this->relasi->SlowMoving($bulan,$tahun);	
		$this->load->view('chart/GetSlowMoving',$data);
	}

	function TopSeller($bulan,$tahun,$seller){
		$data['bulan']	= $bulan;
		$data['tahun']	= $tahun;
		if($seller == 'All'){
			$data['row']	= $this->relasi->TopSeller2($bulan,$tahun);
		}else{
		$data['row']	= $this->relasi->TopSeller($bulan,$tahun,$seller);
		}	
		$this->load->view('chart/GetTopSeller',$data);
	}


}
