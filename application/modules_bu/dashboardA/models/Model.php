<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	// Syarat  :  
	
	// 1 . Select  = View 
	// 2 . Insert  = Ins
	// 3 . Update  = Updt
	// 4 . Delete  = Del

class Model extends CI_Model {

	////// MASTER //////
	
	public function ViewKategori() {
		
		$Query = $this->db->query("SELECT * FROM m_product_type order by product_type_order ASC ");
		return $Query->result_array();
	}

	public function ViewBrand() {
		
		$Query = $this->db->query("SELECT * FROM m_brand where brand_active = 1");
		return $Query->result_array();
	}

	
	
}