<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relasi extends CI_Model {

public function PoUnpaidMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT account_detail_sales_date,
		SUM(account_detail_sales_amount) as amount FROM 
		v_pr_dashboard_arga where month(account_detail_sales_date) = '".$Bulan."' and 
		year(account_detail_sales_date) = '".$Tahun."' and account_detail_sales_amount IS 
		NULL group by account_detail_sales_date");
	return $Query;	
}

public function PoPaidMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT account_detail_sales_date,
		SUM(account_detail_sales_amount) as amount FROM 
		v_pr_dashboard_arga where month(account_detail_sales_date) = '".$Bulan."' and 
		year(account_detail_sales_date) = '".$Tahun."' and account_detail_sales_amount IS 
		NOT NULL group by account_detail_sales_date");
	return $Query;	
}

public function SoByQtyMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT account_detail_sales_date,COUNT(account_detail_sales_id) as so_qty FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and month(account_detail_sales_date) = '".$Bulan."' and year(account_detail_sales_date) = '".$Tahun."' group by account_detail_sales_date");
	return $Query;	
}

public function SoByBrandMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT brand_name,COUNT(account_detail_sales_id) as so_by_brand  FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and month(account_detail_sales_date) = '".$Bulan."' and year(account_detail_sales_date) = '".$Tahun."' group by brand_name");
	return $Query;	
}

public function SoByKategoriMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT kategori,COUNT(account_detail_sales_id) as so_by_kategori FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and month(account_detail_sales_date) = '".$Bulan."' and year(account_detail_sales_date) = '".$Tahun."' group by kategori");
	return $Query;	
}

public function SoByMemberMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT `status`,COUNT(account_detail_sales_id) as member FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and month(account_detail_sales_date) = '".$Bulan."' and year(account_detail_sales_date) = '".$Tahun."'  and `status` = 'MEMBER' group by `status`  ");
	return $Query;	
}

public function SoByResellerMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT `status`,COUNT(account_detail_sales_id) as member FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and month(account_detail_sales_date) = '".$Bulan."' and year(account_detail_sales_date) = '".$Tahun."'  and `status` = 'RESELLER' group by `status`");
	return $Query;	
}

public function SoByAgenMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT `status`,COUNT(account_detail_sales_id) as member FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and month(account_detail_sales_date) = '".$Bulan."' and year(account_detail_sales_date) = '".$Tahun."'  and `status` = 'AGEN' group by `status`");
	return $Query;	
}

public function SoByDistributorMonthly($Bulan="",$Tahun="") {
	$Query = $this->db->query("SELECT `status`,COUNT(account_detail_sales_id) as member FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and month(account_detail_sales_date) = '".$Bulan."' and year(account_detail_sales_date) = '".$Tahun."'  and `status` = 'DISTRIBUTOR' group by `status`");
	return $Query;	
}


public function PoUnpaidYearly($Tahun="") {
	$Query = $this->db->query("SELECT account_detail_sales_date,
		SUM(account_detail_sales_amount) as amount FROM 
		v_pr_dashboard_arga where  
		year(account_detail_sales_date) = '".$Tahun."' and account_detail_sales_amount IS 
		NULL group by month(account_detail_sales_date)");
	return $Query;	
}

public function PoPaidYearly($Tahun="") {
	$Query = $this->db->query("SELECT account_detail_sales_date,
		SUM(account_detail_sales_amount) as amount FROM 
		v_pr_dashboard_arga where  
		year(account_detail_sales_date) = '".$Tahun."' and account_detail_sales_amount IS 
		NOT NULL group by month(account_detail_sales_date)");
	return $Query;	
}

public function SoByQtyYearly($Tahun="") {
	$Query = $this->db->query("SELECT account_detail_sales_date,COUNT(account_detail_sales_id) as so_qty FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and year(account_detail_sales_date) = '".$Tahun."' group by month(account_detail_sales_date)");
	return $Query;	
}

public function SoByBrandYearly($Tahun="") {
	$Query = $this->db->query("SELECT brand_name,COUNT(account_detail_sales_id) as so_by_brand  FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and year(account_detail_sales_date) = '".$Tahun."' group by brand_name");
	return $Query;	
}

public function SoByKategoriYearly($Tahun="") {
	$Query = $this->db->query("SELECT kategori,COUNT(account_detail_sales_id) as so_by_kategori FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and year(account_detail_sales_date) = '".$Tahun."' group by kategori");
	return $Query;	
}

public function SoBySellerLine($Bulan,$Tahun,$Seller) {
	$Query = $this->db->query("SELECT account_detail_sales_date,`status`,COUNT(account_detail_sales_id) as member FROM v_so_count_dashboard_arga where account_detail_sales_product_allow > 0 and 
		month(account_detail_sales_date) = '".$Bulan."' and year(account_detail_sales_date) = '".$Tahun."'  and `status` = '".$Seller."' group by `status`,account_detail_sales_date");
	return $Query;	
}

public function FastMoving($Bulan,$Tahun) {
	$Query = $this->db->query("SELECT a.nama_produk, (SELECT SUM(s.account_detail_sales_product_allow)  FROM t_account_detail_sales_product s,t_account_detail_sales p where s.product_id = a.id_produk  and s.account_detail_sales_product_allow > 0 and s.sales_id = p.sales_id and  month(p.account_detail_sales_date) = '".$Bulan."' and year(p.account_detail_sales_date) = '".$Tahun."') as jumlah_terjual
FROM v_produk_dashboard_arga a  ORDER BY jumlah_terjual DESC ");
	return $Query;	
}

public function SlowMoving($Bulan,$Tahun) {
	$Query = $this->db->query("SELECT a.nama_produk, (SELECT SUM(s.account_detail_sales_product_allow)  FROM t_account_detail_sales_product s,t_account_detail_sales p where s.product_id = a.id_produk  and s.account_detail_sales_product_allow > 0 and s.sales_id = p.sales_id and  month(p.account_detail_sales_date) = '".$Bulan."' and year(p.account_detail_sales_date) = '".$Tahun."') as jumlah_terjual
FROM v_produk_dashboard_arga a  ORDER BY jumlah_terjual DESC ");
	return $Query;	
}

public function TopSeller($Bulan,$Tahun,$Seller) {
	$Query = $this->db->query("SELECT a.account_detail_sales_date,a.seller_id,a.nama_member,a.status,SUM(a.account_detail_sales_amount) as amount
FROM v_pr_dashboard_arga a where MONTH(account_detail_sales_date) = '".$Bulan."' and YEAR(account_detail_sales_date) = '".$Tahun."' and status = '".$Seller."' GROUP BY nama_member
");
	return $Query;	
}


public function TopSeller2($Bulan,$Tahun) {
	$Query = $this->db->query("SELECT a.account_detail_sales_date,a.seller_id,a.nama_member,a.status,SUM(a.account_detail_sales_amount) as amount
FROM v_pr_dashboard_arga a where MONTH(account_detail_sales_date) = '".$Bulan."' and YEAR(account_detail_sales_date) = '".$Tahun."'  GROUP BY a.`status`
");
	return $Query;	
}

}