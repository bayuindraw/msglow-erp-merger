<?php
function convertDate($tgl){
    $cDate = explode("-", $tgl);

    $nTahun = $cDate[0];
    $nBulan = $cDate[1];
    $nHari  = $cDate[2];

    if($nBulan == "01"){
        $cBulan = "Jan";
    }else if($nBulan == "02"){
        $cBulan = "Feb";
    }else if($nBulan == "03"){
        $cBulan = "Mar";
    }else if($nBulan == "04"){
        $cBulan = "Apr";
    }else if($nBulan == "05"){
        $cBulan = "Mei";
    }else if($nBulan == "06"){
        $cBulan = "Juni";
    }else if($nBulan == "07"){
        $cBulan = "Juli";
    }else if($nBulan == "08"){
        $cBulan = "Agst";
    }else if($nBulan == "09"){
        $cBulan = "Spt";
    }else if($nBulan == "10"){
        $cBulan = "Okt";
    }else if($nBulan == "11"){
        $cBulan = "Nov";
    }else if($nBulan == "12"){
        $cBulan = "Des";
    }

    $cFix = $nHari." ".$cBulan;
    return $cFix;

}
?>
<link href="<?=base_url()?>web/plugins/general/morris.js/morris.css" rel="stylesheet" type="text/css" />


<div id="kt_morris_4" style="height:500px;"></div>


<script src="<?=base_url()?>web/plugins/jquery.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/plugins/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script src="<?=base_url()?>web/plugins/general/raphael/raphael.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/plugins/general/morris.js/morris.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/plugins/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>


<script type="text/javascript">
"use strict";
// Class definition
var KTMorrisChartsDemo = function() {
  

    var data = function() {
        // LINE CHART
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_4',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                <?php 
                if($row->num_rows() > 0){
                     foreach ($row->result_array() as $key => $vaData) {               
                ?>
                {
                    y: '<?=convertDate($vaData['account_detail_sales_date'])?>',
                    a: '<?=$vaData['amount']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'None',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['PR Paid'],
            lineColors: ['#007bff']
        });
    }

    return {
        // public functions
        init: function() {
            data();
        }
    };
}();

jQuery(document).ready(function() {
    KTMorrisChartsDemo.init();
});
                            </script>