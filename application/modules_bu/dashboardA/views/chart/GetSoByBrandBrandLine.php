<link href="<?=base_url()?>web/plugins/general/morris.js/morris.css" rel="stylesheet" type="text/css" />


<div id="kt_morris_6" style="height:500px;"></div>


<script src="<?=base_url()?>web/plugins/jquery.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/plugins/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script src="<?=base_url()?>web/plugins/general/raphael/raphael.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/plugins/general/morris.js/morris.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/plugins/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>


<script type="text/javascript">
"use strict";
// Class definition
var KTMorrisChartsDemo = function() {
  

    var data = function() {
        // LINE CHART
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'kt_morris_6',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                <?php 
                if($row->num_rows() > 0){
                     foreach ($row->result_array() as $key => $vaData) {               
                ?>
                {
                    y: '<?=$vaData['brand_name']?>',
                    a: '<?=$vaData['so_by_brand']?>',
                    
                },
            <?php } ?>
            <?php }else{ ?>
                {
                    y: 'None',
                    a: '0',
                    
                },
            <?php } ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'y',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['a'],
            parseTime: false,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Qty'],
            lineColors: ['#007bff']
        });
    }

    return {
        // public functions
        init: function() {
            data();
        }
    };
}();

jQuery(document).ready(function() {
    KTMorrisChartsDemo.init();
});
                            </script>